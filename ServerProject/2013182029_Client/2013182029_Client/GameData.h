#pragma once
#include <Windows.h>

struct PlayerInfo {
	WCHAR loginID[20];

	int		xPos;
	int		zPos;
	int		level;
	int		exp;
	int		hp;
	int		mp;
	int		viewDir;
	
	DWORD	attackTime;
	DWORD	attackAniTime;
	DWORD	moveTime;
};

struct Rock {
	int x;
	int z;
};

//#define ATTACKSPEED		1000
//#define MOVESPEED		500
#define ATTACKSPEED		1000
#define MOVESPEED		300