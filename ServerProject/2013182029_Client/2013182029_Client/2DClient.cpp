// PROG14_1_16b.CPP - DirectInput keyboard demo

// INCLUDES ///////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN  
#define INITGUID
#define USE_CONSOLE
#include <WinSock2.h>
#include <windows.h>   // include important windows stuff
#include <windowsx.h>
#include <stdio.h>
#include <iostream>
using namespace std;

#include <d3d9.h>     // directX includes
#include "d3dx9tex.h"     // directX includes
#include "gpdumb1.h"
#include "../../protocol.h"

#pragma comment (lib, "ws2_32.lib")

// DEFINES ////////////////////////////////////////////////

#define MAX(a,b)	((a)>(b))?(a):(b)
#define	MIN(a,b)	((a)<(b))?(a):(b)

// defines for windows 
#define WINDOW_CLASS_NAME L"WINXCLASS"  // class name

#define WINDOW_WIDTH    660   // size of window
#define WINDOW_HEIGHT   720

#define	BUF_SIZE				1024
#define	WM_SOCKET				WM_USER + 1

// PROTOTYPES /////////////////////////////////////////////

// game console
int Game_Init(void *parms = NULL);
int Game_Shutdown(void *parms = NULL);
int Game_Main(void *parms = NULL);

// GLOBALS ////////////////////////////////////////////////

HWND main_window_handle = NULL; // save the window handle
HINSTANCE main_instance = NULL; // save the instance
char buffer[80];                // used to print text

								// demo globals
BOB		player;							// 플레이어 Unit
BOB		monsterArray[NUM_OF_MONSTER];	// Monster Unit
BOB		otherPlayer[MAX_USER];			// Other player Unit

BOB		attackEffect[4];
Rock	rockArray[NUM_OF_ROCK];
BITMAP_IMAGE reactor;      // the background   
BITMAP_IMAGE black_tile;
BITMAP_IMAGE white_tile;
BITMAP_IMAGE indicateImage;
BITMAP_IMAGE playerImage;

BITMAP_IMAGE rockImage;

#define TILE_WIDTH 30

#define UNIT_TEXTURE  0
#define PLAYER_TEXTURE  1
#define ATTACK_TEXTURE	2
#define OTHERPLAYER_TEXTURE	3

SOCKET g_mysocket;
WSABUF	send_wsabuf;
char 	send_buffer[BUF_SIZE];
WSABUF	recv_wsabuf;
char	recv_buffer[BUF_SIZE];
char	packet_buffer[BUF_SIZE];
DWORD		in_packet_size = 0;
int		saved_packet_size = 0;
int		g_myid;

int		g_left_x = 0;
int     g_top_y = 0;
WCHAR	wLoginID[100];

void LoginServer()
{
	cout << "Enter ID." << endl;
	wcin >> wLoginID;

	cs_packet_Login *my_packet = reinterpret_cast<cs_packet_Login*>(send_buffer);
	DWORD iobyte;
	my_packet->size = sizeof(cs_packet_Login);
	my_packet->type = CS_LOGIN;
	wcscpy(my_packet->loginID, wLoginID);

	send_wsabuf.len = sizeof(cs_packet_Login);

	int ret = WSASend(g_mysocket, &send_wsabuf, 1, &iobyte, 0, NULL, NULL);
	if (ret) {
		int error_code = WSAGetLastError();
		printf("Error while sending packet [%d]", error_code);
	}
}

// FUNCTIONS //////////////////////////////////////////////
void ProcessPacket(char *ptr)
{
	static bool first_time = true;
	switch (ptr[1]) {
	case SC_LOGIN_OK:
	{
		cout << "로그인 성공!" << endl;
		sc_packet_Login_OK *my_packet = reinterpret_cast<sc_packet_Login_OK*>(ptr);

		if (my_packet->controlID == g_myid) {
			g_left_x = my_packet->xPos - 10;
			g_top_y = my_packet->zPos - 10;

			PlayerInfo data;
			wcscpy(data.loginID, wLoginID);
			data.xPos = my_packet->xPos;
			data.zPos = my_packet->zPos;
			data.level = my_packet->level;
			data.exp = my_packet->exp;
			data.hp = my_packet->hp;
			data.mp = my_packet->mp;

			player.InitData(data);
		}
	}
	break;
	case SC_LOGIN_FAIL:
	{
		cout << "로그인 실패! - 이미 접속중인 회원" << endl;
		cout << "아이디를 다시 입력해주세요. " << endl;
		LoginServer();
	}
	break;
	case SC_POSITION_INFO:
	{
		sc_packet_Position_Info *my_packet = reinterpret_cast<sc_packet_Position_Info *>(ptr);
		int updateID = my_packet->id;
		int objectType = my_packet->objectType;

		switch (objectType) {
		case Object_Type::ePlayer:
			// 자기 자신
			if (updateID == g_myid) {
				g_left_x = my_packet->x - 10;
				g_top_y = my_packet->y - 10;
				player.data.xPos = my_packet->x;
				player.data.zPos = my_packet->y;
				player.data.viewDir = my_packet->viewDir;
			}
			else {
				otherPlayer[updateID].data.xPos = my_packet->x;
				otherPlayer[updateID].data.zPos = my_packet->y;
				otherPlayer[updateID].data.viewDir = my_packet->viewDir;
			}
			break;
		case Object_Type::eMonster_Peace:
			monsterArray[updateID].data.xPos = my_packet->x;
			monsterArray[updateID].data.zPos = my_packet->y;
			monsterArray[updateID].data.viewDir = my_packet->viewDir;
			break;
		case Object_Type::eMonster_War:

			break;
		case 0:
			cout << "지정하지 않은 Object Type!" << endl;
			break;
		default:
			cout << "존재하지 않은 Object Type!" << endl;
			break;
		}
	}
	break;
	case SC_CHAT:
	{
		sc_packet_chat *my_packet = reinterpret_cast<sc_packet_chat *>(ptr);
		int updateID = my_packet->id;
		int objectType = my_packet->objectType;

		switch (objectType) {
		case Object_Type::ePlayer:
			// 자기 자신
			if (updateID == g_myid) {
				//wcsncpy_s(player.message, my_packet->message, 256);
				//player.message_time = GetTickCount();

				wcsncpy_s(player.systemMessage[player.systemMessageCount], my_packet->message, 256);
				player.systemMessage_time[player.systemMessageCount++] = GetTickCount();

			}
			else {
				wcsncpy_s(otherPlayer[updateID].message, my_packet->message, 256);
				otherPlayer[updateID].message_time = GetTickCount();
			}
			break;
		case Object_Type::eMonster_Peace:
			wcsncpy_s(monsterArray[updateID].message, my_packet->message, 256);
			monsterArray[updateID].message_time = GetTickCount();
			break;
		case Object_Type::eMonster_War:

			break;
		case 0:
			cout << "지정하지 않은 Object Type!" << endl;
			break;
		default:
			cout << "존재하지 않은 Object Type!" << endl;
			break;
		}
	}
	break;
	case SC_ADD_OBJECT:
	{
		sc_packet_Add_Object *my_packet = reinterpret_cast<sc_packet_Add_Object *>(ptr);
		int updateID = my_packet->id;
		int objectType = my_packet->objectType;

		switch (objectType) {
		case Object_Type::ePlayer:
			if (first_time) {
				first_time = false;
				g_myid = updateID;
			}
			if (updateID == g_myid) {
				player.data.xPos = my_packet->x;
				player.data.zPos = my_packet->y;
				player.data.viewDir = eDown;
				g_left_x = my_packet->x - 10;
				g_top_y = my_packet->y - 10;
				player.attr |= BOB_ATTR_VISIBLE;
			}
			else {
				otherPlayer[updateID].data.xPos = my_packet->x;
				otherPlayer[updateID].data.zPos = my_packet->y;
				otherPlayer[updateID].attr |= BOB_ATTR_VISIBLE;

				otherPlayer[updateID].data.viewDir = my_packet->viewDir;
				otherPlayer[updateID].data.level = my_packet->level;
				otherPlayer[updateID].data.exp = my_packet->exp;
				otherPlayer[updateID].data.hp = my_packet->hp;
				otherPlayer[updateID].data.mp = my_packet->mp;
			}
			break;
		case Object_Type::eMonster_Peace:
			monsterArray[updateID].data.xPos = my_packet->x;
			monsterArray[updateID].data.zPos = my_packet->y;
			monsterArray[updateID].attr |= BOB_ATTR_VISIBLE;

			monsterArray[updateID].data.viewDir = my_packet->viewDir;
			monsterArray[updateID].data.level = my_packet->level;
			monsterArray[updateID].data.exp = my_packet->exp;
			monsterArray[updateID].data.hp = my_packet->hp;
			monsterArray[updateID].data.mp = my_packet->mp;
			break;
		case Object_Type::eMonster_War:
			cout << "SC_REMOVE_OBJECT :: 정의하지 않음" << endl;
			break;
		case 0:
			cout << "지정하지 않은 Object Type!" << endl;
			break;
		default:
			cout << "존재하지 않은 Object Type!" << endl;
			break;
		}
		break;
	}
	case SC_REMOVE_OBJECT:
	{
		sc_packet_Remove_Object *my_packet = reinterpret_cast<sc_packet_Remove_Object *>(ptr);
		int updateID = my_packet->id;
		int objectType = my_packet->objectType;

		switch (objectType) {
		case Object_Type::ePlayer:
			// 자기 자신
			if (updateID == g_myid) {
				player.attr &= ~BOB_ATTR_VISIBLE;
			}
			else {
				otherPlayer[updateID].attr &= ~BOB_ATTR_VISIBLE;
			}
			break;
		case Object_Type::eMonster_Peace:
			monsterArray[updateID].attr &= ~BOB_ATTR_VISIBLE;

			break;
		case Object_Type::eMonster_War:
			cout << "SC_REMOVE_OBJECT :: 정의하지 않음" << endl;
			break;
		case 0:
			cout << "지정하지 않은 Object Type!" << endl;
			break;
		default:
			cout << "존재하지 않은 Object Type!" << endl;
			break;
		}
		break;
	}
	case SC_STAT_CHANGE:
	{
		sc_packet_Stat_Change *my_packet = reinterpret_cast<sc_packet_Stat_Change*>(ptr);
		int updateID = my_packet->id;
		int objectType = my_packet->objectType;

		switch (objectType) {
		case Object_Type::ePlayer:
			// 자기 자신
			if (updateID == g_myid) {
				player.SetData(my_packet->level, my_packet->exp, my_packet->hp, my_packet->mp);
			}
			else {
				otherPlayer[updateID].SetData(my_packet->level, my_packet->exp, my_packet->hp, my_packet->mp);
			}
			break;
		case Object_Type::eMonster_Peace:
			monsterArray[updateID].SetData(my_packet->level, my_packet->exp, my_packet->hp, my_packet->mp);

			break;
		case Object_Type::eMonster_War:

			break;
		case 0:
			cout << "지정하지 않은 Object Type!" << endl;
			break;
		default:
			cout << "존재하지 않은 Object Type!" << endl;
			break;
		}
		break;
	}
	case SC_MAPDATA:
	{
		static int rockCount = 0;
		sc_packet_MapData *my_packet = reinterpret_cast<sc_packet_MapData *>(ptr);

		rockArray[rockCount].x = my_packet->x;
		rockArray[rockCount++].z = my_packet->y;
	}
	break;
	default:
		printf("Unknown PACKET type [%d]\n", ptr[1]);
	}
}

void ReadPacket(SOCKET sock)
{
	DWORD iobyte, ioflag = 0;

	int ret = WSARecv(sock, &recv_wsabuf, 1, &iobyte, &ioflag, NULL, NULL);
	if (ret) {
		int err_code = WSAGetLastError();
		printf("Recv Error [%d]\n", err_code);
	}

	BYTE *ptr = reinterpret_cast<BYTE *>(recv_buffer);

	while (0 != iobyte) {
		if (0 == in_packet_size) in_packet_size = ptr[0];
		if (iobyte + saved_packet_size >= in_packet_size) {
			memcpy(packet_buffer + saved_packet_size, ptr, in_packet_size - saved_packet_size);
			ProcessPacket(packet_buffer);
			ptr += in_packet_size - saved_packet_size;
			iobyte -= in_packet_size - saved_packet_size;
			in_packet_size = 0;
			saved_packet_size = 0;
		}
		else {
			memcpy(packet_buffer + saved_packet_size, ptr, iobyte);
			saved_packet_size += iobyte;
			iobyte = 0;
		}
	}
}

void clienterror()
{
	exit(-1);
}

LRESULT CALLBACK WindowProc(HWND hwnd,
	UINT msg,
	WPARAM wparam,
	LPARAM lparam)
{
	// this is the main message handler of the system
	PAINTSTRUCT	ps;		   // used in WM_PAINT
	HDC			hdc;	   // handle to a device context

						   // what is the message 
	switch (msg)
	{
	case WM_KEYDOWN:
	{
		int x = 0, y = 0;
		int move = false;
		switch (wparam) {
		case VK_RIGHT:
			if (GetTickCount() - player.data.moveTime > MOVESPEED) {

				x += 1;
				move = true;
				player.data.moveTime = GetTickCount();
				Load_Frame_BOB32(&player, PLAYER_TEXTURE, 0, 4, 0, BITMAP_EXTRACT_MODE_CELL);
			}
			break;
		case VK_LEFT:
			if (GetTickCount() - player.data.moveTime > MOVESPEED) {
				x -= 1;
				move = true;
				player.data.moveTime = GetTickCount();
				Load_Frame_BOB32(&player, PLAYER_TEXTURE, 0, 6, 0, BITMAP_EXTRACT_MODE_CELL);
			}
			break;
		case VK_UP:
			if (GetTickCount() - player.data.moveTime > MOVESPEED) {
				y -= 1;
				move = true;
				player.data.moveTime = GetTickCount();
				Load_Frame_BOB32(&player, PLAYER_TEXTURE, 0, 2, 0, BITMAP_EXTRACT_MODE_CELL);
			}
			break;
		case VK_DOWN:
			if (GetTickCount() - player.data.moveTime > MOVESPEED) {
				y += 1;
				move = true;
				player.data.moveTime = GetTickCount();
				Load_Frame_BOB32(&player, PLAYER_TEXTURE, 0, 0, 0, BITMAP_EXTRACT_MODE_CELL);
			}
			break;
		case VK_SPACE:
		{
			if (GetTickCount() - player.data.attackTime > ATTACKSPEED) {
				cs_packet_Attack *my_packet = reinterpret_cast<cs_packet_Attack *>(send_buffer);
				my_packet->type = CS_ATTACK;
				my_packet->size = sizeof(my_packet);
				send_wsabuf.len = sizeof(my_packet);
				DWORD iobyte;

				player.data.attackTime = GetTickCount();

				int ret = WSASend(g_mysocket, &send_wsabuf, 1, &iobyte, 0, NULL, NULL);
				if (ret) {
					int error_code = WSAGetLastError();
					printf("Error while sending packet [%d]", error_code);
				}
			}

			break;
		}
		}
		if (move) {
			cs_packet_Move *my_packet = reinterpret_cast<cs_packet_Move *>(send_buffer);
			my_packet->type = CS_MOVE;
			my_packet->size = sizeof(my_packet);
			send_wsabuf.len = sizeof(my_packet);
			DWORD iobyte;
			if (0 != x) {
				if (1 == x) my_packet->dir = Direction_Type::eRight;
				else my_packet->dir = Direction_Type::eLeft;
				int ret = WSASend(g_mysocket, &send_wsabuf, 1, &iobyte, 0, NULL, NULL);
				if (ret) {
					int error_code = WSAGetLastError();
					printf("Error while sending packet [%d]", error_code);
				}
			}
			if (0 != y) {
				if (1 == y) my_packet->dir = Direction_Type::eDown;
				else my_packet->dir = Direction_Type::eUp;
				WSASend(g_mysocket, &send_wsabuf, 1, &iobyte, 0, NULL, NULL);
			}
		}
	}
	break;
	case WM_CREATE:
	{
		// do initialization stuff here
		return(0);
	} break;

	case WM_PAINT:
	{
		// start painting
		hdc = BeginPaint(hwnd, &ps);

		// end painting
		EndPaint(hwnd, &ps);
		return(0);
	} break;

	case WM_DESTROY:
	{
		// kill the application			
		PostQuitMessage(0);
		return(0);
	} break;
	case WM_SOCKET:
	{
		if (WSAGETSELECTERROR(lparam)) {
			closesocket((SOCKET)wparam);
			clienterror();
			break;
		}
		switch (WSAGETSELECTEVENT(lparam)) {
		case FD_READ:
			ReadPacket((SOCKET)wparam);
			break;
		case FD_CLOSE:
			closesocket((SOCKET)wparam);
			clienterror();
			break;
		}
	}

	default:break;

	} // end switch

	  // process any messages that we didn't take care of 
	return (DefWindowProc(hwnd, msg, wparam, lparam));

} // end WinProc

  // WINMAIN ////////////////////////////////////////////////

int WINAPI WinMain(HINSTANCE hinstance,
	HINSTANCE hprevinstance,
	LPSTR lpcmdline,
	int ncmdshow)
{
	// this is the winmain function

	WNDCLASS winclass;	// this will hold the class we create
	HWND	 hwnd;		// generic window handle
	MSG		 msg;		// generic message


						// first fill in the window class stucture
	winclass.style = CS_DBLCLKS | CS_OWNDC |
		CS_HREDRAW | CS_VREDRAW;
	winclass.lpfnWndProc = WindowProc;
	winclass.cbClsExtra = 0;
	winclass.cbWndExtra = 0;
	winclass.hInstance = hinstance;
	winclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	winclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	winclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	winclass.lpszMenuName = NULL;
	winclass.lpszClassName = WINDOW_CLASS_NAME;

	// register the window class
	if (!RegisterClass(&winclass))
		return(0);

	// create the window, note the use of WS_POPUP
	if (!(hwnd = CreateWindow(WINDOW_CLASS_NAME, // class
		L"Client",	 // title
		WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		0, 0,	   // x,y
		WINDOW_WIDTH,  // width
		WINDOW_HEIGHT, // height
		NULL,	   // handle to parent 
		NULL,	   // handle to menu
		hinstance,// instance
		NULL)))	// creation parms
		return(0);

	// save the window handle and instance in a global
	main_window_handle = hwnd;
	main_instance = hinstance;

	// perform all game console specific initialization
	Game_Init();

	// enter main event loop
	while (1)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// test if this is a quit
			if (msg.message == WM_QUIT)
				break;

			// translate any accelerator keys
			TranslateMessage(&msg);

			// send the message to the window proc
			DispatchMessage(&msg);
		} // end if

		  // main game processing goes here
		Game_Main();

	} // end while

	  // shutdown game and release all resources
	Game_Shutdown();

	/*
	cs_packet_exit *my_packet = reinterpret_cast<cs_packet_exit *>(send_buffer);
	my_packet->size = sizeof(my_packet);
	send_wsabuf.len = sizeof(my_packet);
	DWORD iobyte;
	my_packet->exitPlayer = true;
	int ret = WSASend(g_mysocket, &send_wsabuf, 1, &iobyte, 0, NULL, NULL);
	if (ret) {
	int error_code = WSAGetLastError();
	printf("Error while sending packet [%d]", error_code);
	}
	*/

	// return to Windows like this
	return(msg.wParam);

} // end WinMain

  ///////////////////////////////////////////////////////////

  // WINX GAME PROGRAMMING CONSOLE FUNCTIONS ////////////////

int Game_Init(void *parms)
{
#ifdef USE_CONSOLE
	AllocConsole();
	freopen("CONOUT$", "wt", stdout);
	freopen("CONIN$", "rt", stdin);
#endif
	// this function is where you do all the initialization 
	// for your game

	// set up screen dimensions
	screen_width = WINDOW_WIDTH;
	screen_height = WINDOW_HEIGHT;
	screen_bpp = 32;

	// initialize directdraw
	DD_Init(screen_width, screen_height, screen_bpp);


	// create and load the reactor bitmap image
	Create_Bitmap32(&reactor, 0, 0, 531, 532);
	Create_Bitmap32(&black_tile, 0, 0, 531, 532);
	Create_Bitmap32(&white_tile, 0, 0, 531, 532);
	Load_Image_Bitmap32(&reactor, L"map.BMP", 0, 0, BITMAP_EXTRACT_MODE_ABS);
	Load_Image_Bitmap32(&black_tile, L"map.BMP", 0, 0, BITMAP_EXTRACT_MODE_ABS);
	black_tile.x = 69;
	black_tile.y = 3;
	black_tile.height = TILE_WIDTH;
	black_tile.width = TILE_WIDTH;
	Load_Image_Bitmap32(&white_tile, L"map.BMP", 0, 0, BITMAP_EXTRACT_MODE_ABS);
	white_tile.x = 3;
	white_tile.y = 3;
	white_tile.height = TILE_WIDTH;
	white_tile.width = TILE_WIDTH;

	Load_Image_Bitmap32(&indicateImage, L"Image.png", 0, 0, BITMAP_EXTRACT_MODE_ABS);
	indicateImage.x = 5;
	indicateImage.y = 5;
	indicateImage.height = 20;
	indicateImage.width = 20;

	Load_Image_Bitmap32(&rockImage, L"tree2.png", 0, 0, BITMAP_EXTRACT_MODE_ABS);
	rockImage.x = 0;
	rockImage.y = 0;
	rockImage.height = 30;
	rockImage.width = 30;

	// now let's load in all the frames for the otherPlayer!!!

	Load_Texture(L"CHESS2.PNG", UNIT_TEXTURE, 192, 32);
	//	Load_Texture(L"Player.png", UNIT_TEXTURE, 258, 36);
	Load_Texture(L"Player.png", PLAYER_TEXTURE, 250, 36);
	Load_Texture(L"attack.png", ATTACK_TEXTURE, 128, 32);
	//다른플레이어의 텍스쳐 
	Load_Texture(L"Player2.png", OTHERPLAYER_TEXTURE, 250, 36);

	if (!Create_BOB32(&player, 0, 0, 30, 30, 1, BOB_ATTR_SINGLE_FRAME | BOB_ATTR_MULTI_FRAME)) return(0);
	Load_Frame_BOB32(&player, UNIT_TEXTURE, 0, 2, 0, BITMAP_EXTRACT_MODE_CELL);
	Load_Frame_BOB32(&player, PLAYER_TEXTURE, 0, 0, 0, BITMAP_EXTRACT_MODE_CELL);

	for (int i = 0; i < 4; ++i) {
		if (!Create_BOB32(&attackEffect[i], 0, 0, 30, 30, 1, BOB_ATTR_SINGLE_FRAME | BOB_ATTR_MULTI_FRAME)) return(0);
		Load_Frame_BOB32(&attackEffect[i], ATTACK_TEXTURE, 0, 0, 0, BITMAP_EXTRACT_MODE_CELL);
	}
	// set up stating state of otherPlayer
	Set_Animation_BOB32(&player, 1);
	Set_Anim_Speed_BOB32(&player, 4);
	Set_Vel_BOB32(&player, 0, 0);
	Set_Pos_BOB32(&player, 0, 0);


	// create otherPlayer bob
	for (int i = 0; i < MAX_USER; ++i) {
		if (!Create_BOB32(&otherPlayer[i], 0, 0, 30, 30, 1, BOB_ATTR_SINGLE_FRAME))
			return(0);
		Load_Frame_BOB32(&otherPlayer[i], OTHERPLAYER_TEXTURE, 0, 0, 0, BITMAP_EXTRACT_MODE_CELL);

		// set up stating state of otherPlayer
		Set_Animation_BOB32(&otherPlayer[i], 1);
		Set_Anim_Speed_BOB32(&otherPlayer[i], 4);
		Set_Vel_BOB32(&otherPlayer[i], 0, 0);
		Set_Pos_BOB32(&otherPlayer[i], 0, 0);
	}

	// create Monster bob
	for (int i = 0; i < NUM_OF_MONSTER; ++i) {
		if (!Create_BOB32(&monsterArray[i], 0, 0, 30, 30, 1, BOB_ATTR_SINGLE_FRAME))
			return(0);
		Load_Frame_BOB32(&monsterArray[i], UNIT_TEXTURE, 0, 4, 0, BITMAP_EXTRACT_MODE_CELL);

		// set up stating state of otherPlayer
		Set_Animation_BOB32(&monsterArray[i], 0);
		Set_Anim_Speed_BOB32(&monsterArray[i], 4);
		Set_Vel_BOB32(&monsterArray[i], 0, 0);
		Set_Pos_BOB32(&monsterArray[i], 0, 0);
	}




	// set clipping rectangle to screen extents so mouse cursor
	// doens't mess up at edges
	//RECT screen_rect = {0,0,screen_width,screen_height};
	//lpddclipper = DD_Attach_Clipper(lpddsback,1,&screen_rect);

	// hide the mouse
	//ShowCursor(FALSE);


	WSADATA	wsadata;
	WSAStartup(MAKEWORD(2, 2), &wsadata);

	g_mysocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0);

	char ipAddr[20];
	cout << "접속할 서버의 IP주소를 입력하세요." << endl;	
	cout <<	"루프백 서버 접속 : '127.0.0.1'->접속하려면 start를 입력해주세요. " << endl;
	cin >> ipAddr;

	if (strcmp(ipAddr, "start") == 0) {
		cout << "루프백 서버 접속" << endl;
		strcpy(ipAddr, "127.0.0.1");
	}

	SOCKADDR_IN ServerAddr;
	ZeroMemory(&ServerAddr, sizeof(SOCKADDR_IN));
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons(MY_SERVER_PORT);
#ifdef USE_CONSOLE
	ServerAddr.sin_addr.s_addr = inet_addr(ipAddr);
#else
	ServerAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
#endif
	int Result = WSAConnect(g_mysocket, (sockaddr *)&ServerAddr, sizeof(ServerAddr), NULL, NULL, NULL, NULL);

	WSAAsyncSelect(g_mysocket, main_window_handle, WM_SOCKET, FD_CLOSE | FD_READ);

	send_wsabuf.buf = send_buffer;
	send_wsabuf.len = BUF_SIZE;
	recv_wsabuf.buf = recv_buffer;
	recv_wsabuf.len = BUF_SIZE;

	if (Result != SOCKET_ERROR) {
		cout << "서버 연결 완료" << endl;

		LoginServer();
	}
	else
		cout << "서버 연결 실패" << endl;

	// return success
	return(1);

} // end Game_Init

  ///////////////////////////////////////////////////////////

int Game_Shutdown(void *parms)
{
#ifdef USE_CONSOLE
	FreeConsole();
#endif
	// this function is where you shutdown your game and
	// release all resources that you allocated

	// kill the reactor
	Destroy_Bitmap32(&black_tile);
	Destroy_Bitmap32(&white_tile);
	Destroy_Bitmap32(&reactor);
	Destroy_Bitmap32(&indicateImage);
	Destroy_Bitmap32(&rockImage);

	// kill otherPlayer
	for (int i = 0; i < MAX_USER; ++i) Destroy_BOB32(&otherPlayer[i]);

	// shutdonw directdraw
	DD_Shutdown();

	WSACleanup();

	// return success
	return(1);
} // end Game_Shutdown

  ///////////////////////////////////////////////////////////

int Game_Main(void *parms)
{
	// this is the workhorse of your game it will be called
	// continuously in real-time this is like main() in C
	// all the calls for you game go here!
	// check of user is trying to exit

	// start the timing clock
	Start_Clock();

	// clear the drawing surface
	DD_Fill_Surface(D3DCOLOR_ARGB(255, 0, 0, 0));

	// get player input

	g_pd3dDevice->BeginScene();
	g_pSprite->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_TEXTURE);

	// draw the background reactor image
	for (int i = 0; i < 21; ++i)
		for (int j = 0; j < 21; ++j)
		{
			int tile_x = i + g_left_x;
			int tile_y = j + g_top_y;
			if ((tile_x < 0) || (tile_y < 0)) continue;
			if ((tile_x >= BOARD_WIDTH) || (tile_y >= BOARD_HEIGHT)) continue;
			if (((tile_x >> 2) % 2) == ((tile_y >> 2) % 2))
				Draw_Bitmap32(&white_tile, TILE_WIDTH * i + 7, TILE_WIDTH * j + 7);
			else
				Draw_Bitmap32(&black_tile, TILE_WIDTH * i + 7, TILE_WIDTH * j + 7);
		}
	//	Draw_Bitmap32(&reactor);

	g_pSprite->End();

	// Draw Indicator
	g_pSprite->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_TEXTURE);

	for (auto rock : rockArray) 
	{
		D3DXVECTOR3 pos = D3DXVECTOR3((rock.x - g_left_x) * 30 + 5, (rock.z - g_top_y) * 30 + 6, 0.0);
		Draw_Bitmap32(&rockImage, pos.x, pos.y);
	}

	g_pSprite->End();

	g_pSprite->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_TEXTURE);

	// draw the otherPlayer
	Draw_BOB32(&player);
	for (int i = 0; i < NUM_OF_MONSTER; ++i) {
		Draw_BOB32(&monsterArray[i]);
	}
	for (int i = 0; i < MAX_USER; ++i)
		Draw_BOB32(&otherPlayer[i]);

	for (int i = 0; i < 4; ++i)
		Draw_BOB32(&attackEffect[i]);

	// --------------------------- Draw User Data ------------------------------ //
	wchar_t text[300];
	wsprintf(text, L"ID : %s", player.data.loginID);
	Draw_Text_D3D(text, 10, screen_height - 80, D3DCOLOR_ARGB(255, 255, 255, 255));

	wsprintf(text, L"Pos : (%3d, %3d)", player.data.xPos, player.data.zPos);
	Draw_Text_D3D(text, 10, screen_height - 60, D3DCOLOR_ARGB(255, 255, 255, 255));

	wsprintf(text, L"Level :%2d", player.data.level);
	Draw_Text_D3D(text, 200, screen_height - 80, D3DCOLOR_ARGB(255, 255, 255, 255));

	wsprintf(text, L"Exp  :%3d", player.data.exp);
	Draw_Text_D3D(text, 200, screen_height - 60, D3DCOLOR_ARGB(255, 255, 255, 255));

	wsprintf(text, L"HP : %3d", player.data.hp);
	Draw_Text_D3D(text, 340, screen_height - 80, D3DCOLOR_ARGB(255, 255, 255, 255));

	wsprintf(text, L"MP : %3d", player.data.mp);
	Draw_Text_D3D(text, 340, screen_height - 60, D3DCOLOR_ARGB(255, 255, 255, 255));
	// -------------------------------------------------------------------------- //
	// Draw monster Data
	for (auto& monster : monsterArray) {
		if (monster.attr & BOB_ATTR_VISIBLE) {
			wsprintf(text, L"HP : %d", monster.data.hp);

			D3DXVECTOR3 pos = D3DXVECTOR3((monster.data.xPos - g_left_x) * 30 + 5, (monster.data.zPos - g_top_y) * 30 + 6, 0.0);
			Draw_Text_D3D(text, pos.x, pos.y, D3DCOLOR_ARGB(255, 255, 30, 30));
		}
	}

	g_pSprite->End();
	g_pd3dDevice->EndScene();

	// flip the surfaces
	DD_Flip();
	// return success
	return(1);

} // end Game_Main

  //////////////////////////////////////////////////////////

