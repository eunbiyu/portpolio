#include "Player.h"

Player::Player()
{
}

Player::~Player()
{
}

void Player::UpdateExp()
{
	// Exp Update
	int needExp = pow(2, this->level - 1) * 100;

	if (this->exp >= needExp) {
		// Level Up!
		this->level++;
		this->exp -= needExp;
		this->max_Hp = this->level * 100;
		this->now_Hp = this->max_Hp;
		this->now_Mp = this->max_Mp;
	}
}

void Player::UpdateHP()
{
}

void Player::UpdateMP()
{
}