#pragma once
#pragma comment(lib, "ws2_32")
#define WIN32_LEAN_AND_MEAN
#define INITGUID

#include <windows.h>
#include <WinSock2.h>
#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <math.h>
#include <unordered_set>
#include <set>
#include <mutex>
#include <queue>

// --- User Header --- //
#include "../../protocol.h"
#include "Database.h"
#include "Player.h"
#include "Monster.h"

//extern "C" {
//	#include "lua.h"
//	#include "lauxlib.h"
//	#include "lualib.h"
//}

using namespace std;
#define NUM_OF_THREAD		8

#define RECOVERY_HP			10			// 리젠되는 HP %
#define RECOVERY_HPTIME		5000		// HP 리젠되는 시간
#define RECOVERY_MP			20			// 리젠되는 MP %
#define RECOVERY_MPTIME		500			// MP 리젠되는 시간
#define ATTACK_DAMAGE		20

enum InitPlayerInfo { eLevel = 1, eExp = 0, eHp = 100, eMp = 100, eStart_xPos = 1, eStart_zPos = 1 };
enum OperationType { OP_RECV = 1, OP_SEND, OP_MOVE, OP_HPRecovery, OP_MPRecovery, OP_Revival };

struct Event_timer {
	int objectID;
	Object_Type objectType;
	unsigned wakeup_time;
	OperationType eventType;
};

class mycomparison {
	bool reverse;
public:
	mycomparison() {}
	bool operator() (const Event_timer lhs, const Event_timer rhs) const
	{
		return (lhs.wakeup_time > rhs.wakeup_time);
	}
};

typedef struct LOGINPLAYER {
	// Accept
	int controlID;
	SOCKET controlSocket;
}LOGINPLAYER;

typedef struct OverlapEx {
	WSAOVERLAPPED	original_overlap;
	int				operation;
	WSABUF			recv_buff;
	unsigned char	socket_buff[MAX_BUFF_SIZE];
	int				packet_size;

}OverlapEx;

typedef struct CLIENT {
	int				id;
	bool			is_connected;
	SOCKET			sock;
	Player			player;
	OverlapEx		recv_overlap;
	int				previous_data_size;
	set<int>		view_list_OtherPlayer;
	set<int>		view_list_Monster;
	mutex			vl_lock;
	unsigned char	packet[MAX_BUFF_SIZE];      //조각난 패킷들이 조합되는 공간
}CLIENT;

struct Rock {
	int x;
	int z;
};

CLIENT clientArray[MAX_USER];
Monster monsterArray[NUM_OF_MONSTER];
//NonPlayer npcs[NUM_OF_NPC];
Rock rockArray[NUM_OF_ROCK];

bool g_isShutDown = false;
HANDLE g_hIOCP;

mutex					g_MonsterLock;
CRITICAL_SECTION		g_CriticalSection;
mutex					timer_lock;

CDataBase*				g_Database;

LOGINPLAYER				g_LoginPlayer;
priority_queue<Event_timer, vector<Event_timer>, mycomparison> p_queue;



void error_display(char *msg, int err_num)
{
	WCHAR *lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, err_num,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);

	printf("%s", msg); wprintf(L"에러%s\n", lpMsgBuf);
	LocalFree(lpMsgBuf);
}

//void my_lua_error(lua_State *L)
//{
//	cout << lua_tostring(L, -1) << endl;
//	lua_pop(L, 1);
//}