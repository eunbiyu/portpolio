#pragma once
#include "Character.h"
#include <Windows.h>
#include <math.h>
#include <iostream>

using namespace std;

#define MAX_ID_SIZE		20

class Player :public Character
{
public :
	Player();
	~Player();

//private:
	WCHAR		loginID[MAX_ID_SIZE];
public:
	void UpdateExp();
	void UpdateHP();
	void UpdateMP();
};