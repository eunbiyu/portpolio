#include "Monster.h"

Monster::Monster()
{
}

Monster::~Monster()
{
}

void Monster::InitData()
{
	level = 1;
	//exp = level * 5;
	exp = 20;
	max_Hp = 100;
	max_Mp = 100;
	now_Hp = 100;
	now_Mp = 100;
	isAlive = true;
	isActive = false;

	attackRange = 5;
	isTarget = false;
	targetPlayerID = 0;
	isAttack = false;
}

bool Monster::Damaged_InPos(int xPos, int zPos)
{
	if (this->xPos == xPos && this->zPos == zPos) {
		return true;
	}
	// Left
	else if (this->xPos == xPos - 1 && this->zPos == zPos) {
		return true;
	}
	//Right
	else if (this->xPos == xPos + 1 && this->zPos == zPos) {
		return true;
	}
	// Up
	else if (this->zPos == zPos - 1 && this->xPos == xPos) {
		return true;
	}
	// Down
	else if (this->zPos == zPos + 1 && this->xPos == xPos) {
		return true;
	}


	else
		return false;
}

bool Monster::IsInViewRange(int player_xPos, int player_zPos) //시야처리 시야안에들어오면 
{
	int dist = (this->xPos - player_xPos) * (this->xPos - player_xPos);
	dist += (this->zPos - player_zPos) * (this->zPos - player_zPos);
	
	return dist <= attackRange * attackRange;
}

bool Monster::IsInAttackRange(int player_xPos, int player_zPos)
{
	int dist = (this->xPos - player_xPos) * (this->xPos - player_xPos);
	dist += (this->zPos - player_zPos) * (this->zPos - player_zPos);

	return dist <= 3;
}