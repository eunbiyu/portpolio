#pragma once
#include <iostream>
#include <windows.h>
#include <sqlext.h>
#include <string>
#include <vector>
#include <algorithm>
#include "Player.h"
using namespace std;

class CDataBase {

public:
	CDataBase();
	~CDataBase();

	void HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode);
	Player FindPlayerData_FromDB(WCHAR loginID[]);

	bool FindPlayerID_FromDB(WCHAR loginID[]);
	void ShowData();
	void UpdateData(Player);
	void AddPlayer(Player);

private:
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLHSTMT hstmt;
	SQLRETURN retcode;
};