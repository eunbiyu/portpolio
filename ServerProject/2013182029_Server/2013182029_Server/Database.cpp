#include "Database.h"
#define Default_LEN 10

CDataBase::CDataBase()
{
	// Allocate environment handle
	retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);

	// Set the ODBC version environment attribute
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0);

		// Allocate connection handle
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

			// Set login timeout to 5 seconds
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

				// Connect to data source
				retcode = SQLConnect(hdbc, (SQLWCHAR*)L"test", SQL_NTS, (SQLWCHAR*)NULL, 0, NULL, 0);
			}
		}
	}
}

CDataBase::~CDataBase()
{
	SQLDisconnect(hdbc);
	SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
	SQLFreeHandle(SQL_HANDLE_ENV, henv);
}



//
void CDataBase::HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode)
{
	SQLSMALLINT iRec = 0;
	SQLINTEGER  iError;
	WCHAR       wszMessage[1000];
	WCHAR       wszState[SQL_SQLSTATE_SIZE + 1];


	if (RetCode == SQL_INVALID_HANDLE)
	{
		fwprintf(stderr, L"Invalid handle!\n");
		return;
	}

	while (SQLGetDiagRec(hType,
		hHandle,
		++iRec,
		wszState,
		&iError,
		wszMessage,
		(SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)),
		(SQLSMALLINT *)NULL) == SQL_SUCCESS)
	{
		// Hide data truncated..
		if (wcsncmp(wszState, L"01004", 5))
		{
			fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
		}
	}

}
//
Player CDataBase::FindPlayerData_FromDB(WCHAR loginID[])
{
	Player findPlayer;

	retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);

	// Process data
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		TCHAR szSQL[256];
		swprintf(szSQL, L"SELECT [ID], [xPos], [yPos], [Lv], [Exp], [HP], [MP]FROM[GAME_DB_2013182029 ].[dbo].[UserData] WHERE ID = '%s'", loginID);
		if (SQLExecDirect(hstmt, (SQLWCHAR*)szSQL, SQL_NTS) == SQL_ERROR) {
			HandleDiagnosticRecord(hdbc, SQL_HANDLE_DBC, retcode);
			cout << "에러 발생 " << endl;
		}
		else {
			SQLINTEGER pIndicators[7];
			Player tempData;

			SQLBindCol(hstmt, 1, SQL_C_WCHAR, (SQLPOINTER)&tempData.loginID, sizeof(tempData.loginID), &pIndicators[0]);
			SQLBindCol(hstmt, 2, SQL_C_LONG, (SQLPOINTER)&tempData.xPos, sizeof(tempData.xPos), &pIndicators[1]);
			SQLBindCol(hstmt, 3, SQL_C_LONG, (SQLPOINTER)&tempData.zPos, sizeof(tempData.zPos), &pIndicators[2]);
			SQLBindCol(hstmt, 4, SQL_C_LONG, (SQLPOINTER)&tempData.level, sizeof(tempData.level), &pIndicators[3]);
			SQLBindCol(hstmt, 5, SQL_C_LONG, (SQLPOINTER)&tempData.exp, sizeof(tempData.exp), &pIndicators[4]);
			SQLBindCol(hstmt, 6, SQL_C_LONG, (SQLPOINTER)&tempData.now_Hp, sizeof(tempData.now_Hp), &pIndicators[5]);
			SQLBindCol(hstmt, 7, SQL_C_LONG, (SQLPOINTER)&tempData.now_Mp, sizeof(tempData.now_Mp), &pIndicators[6]);

			while (SQLFetch(hstmt) == SQL_SUCCESS) {
				// 공백 제거
				for (int i = 0; i < 10; ++i) {
					if (tempData.loginID[i] == ' ') {
						tempData.loginID[i] = '\0';
						break;
					}
				}
				wcscpy(findPlayer.loginID, tempData.loginID);
				findPlayer.xPos = tempData.xPos;
				findPlayer.zPos = tempData.zPos;
				findPlayer.now_Hp = tempData.now_Hp;
				findPlayer.now_Mp = tempData.now_Mp;
				findPlayer.level = tempData.level;
				findPlayer.exp = tempData.exp;

				return findPlayer;
			}
		}
		SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
	}

	return findPlayer;
}

bool CDataBase::FindPlayerID_FromDB(WCHAR loginID[])
{
	retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);

	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		TCHAR szSQL[256];
		swprintf(szSQL, L"SELECT [ID], [xPos], [yPos], [Lv], [Exp], [HP], [MP]FROM[GAME_DB_2013182029 ].[dbo].[UserData] WHERE ID = '%s'", loginID);
		if (SQLExecDirect(hstmt, (SQLWCHAR*)szSQL, SQL_NTS) == SQL_ERROR) {
			HandleDiagnosticRecord(hdbc, SQL_HANDLE_DBC, retcode);
			cout << "에러 발생 " << endl;
		}
		else {
			SQLINTEGER pIndicators[7];
			Player tempData;

			SQLBindCol(hstmt, 1, SQL_C_WCHAR, (SQLPOINTER)&tempData.loginID, sizeof(tempData.loginID), &pIndicators[0]);
			SQLBindCol(hstmt, 2, SQL_C_LONG, (SQLPOINTER)&tempData.xPos, sizeof(tempData.xPos), &pIndicators[1]);
			SQLBindCol(hstmt, 3, SQL_C_LONG, (SQLPOINTER)&tempData.zPos, sizeof(tempData.zPos), &pIndicators[2]);
			SQLBindCol(hstmt, 4, SQL_C_LONG, (SQLPOINTER)&tempData.level, sizeof(tempData.level), &pIndicators[3]);
			SQLBindCol(hstmt, 5, SQL_C_LONG, (SQLPOINTER)&tempData.exp, sizeof(tempData.exp), &pIndicators[4]);
			SQLBindCol(hstmt, 6, SQL_C_LONG, (SQLPOINTER)&tempData.now_Hp, sizeof(tempData.now_Hp), &pIndicators[5]);
			SQLBindCol(hstmt, 7, SQL_C_LONG, (SQLPOINTER)&tempData.now_Mp, sizeof(tempData.now_Mp), &pIndicators[6]);

			while (SQLFetch(hstmt) == SQL_SUCCESS) {
				return true;
			}
		}
		SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
	}
	return false;
}
//
void CDataBase::ShowData()
{
	cout << " --------------------------------------------------------------- " << endl;
	cout << " - DB Player List" << endl;
	cout << " --------------------------------------------------------------- " << endl;
	cout << " Player ID	xPos	yPos	Level	Exp	HP	MP" << endl;
	cout << " --------------------------------------------------------------- " << endl;
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);

		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			TCHAR szSQL[256];
			swprintf(szSQL, L"SELECT [ID], [xPos], [yPos], [Lv], [Exp], [HP], [MP]FROM[position].[dbo].[GAME_DB]");

			if (SQLExecDirect(hstmt, (SQLWCHAR*)szSQL, SQL_NTS) == SQL_ERROR) {
				HandleDiagnosticRecord(hdbc, SQL_HANDLE_DBC, retcode);
				cout << "에러 발생 " << endl;
			}
			else {
				SQLINTEGER pIndicators[7];
				Player tempData;

				SQLBindCol(hstmt, 1, SQL_C_WCHAR, (SQLPOINTER)&tempData.loginID, sizeof(tempData.loginID), &pIndicators[0]);
				SQLBindCol(hstmt, 2, SQL_C_LONG, (SQLPOINTER)&tempData.xPos, sizeof(tempData.xPos), &pIndicators[1]);
				SQLBindCol(hstmt, 3, SQL_C_LONG, (SQLPOINTER)&tempData.zPos, sizeof(tempData.zPos), &pIndicators[2]);
				SQLBindCol(hstmt, 4, SQL_C_LONG, (SQLPOINTER)&tempData.level, sizeof(tempData.level), &pIndicators[3]);
				SQLBindCol(hstmt, 5, SQL_C_LONG, (SQLPOINTER)&tempData.exp, sizeof(tempData.exp), &pIndicators[4]);
				SQLBindCol(hstmt, 6, SQL_C_LONG, (SQLPOINTER)&tempData.now_Hp, sizeof(tempData.now_Hp), &pIndicators[5]);
				SQLBindCol(hstmt, 7, SQL_C_LONG, (SQLPOINTER)&tempData.now_Mp, sizeof(tempData.now_Mp), &pIndicators[6]);

				while (SQLFetch(hstmt) == SQL_SUCCESS) {
					// 공백 제거
					for (int i = 0; i < 10; ++i) {
						if (tempData.loginID[i] == ' ') {
							tempData.loginID[i] = '\0';
							break;
						}
					}
					wcout << tempData.loginID << L"\t\t (" << tempData.xPos << L"\t" << tempData.zPos << L")\t" << tempData.level << L"\t" << tempData.exp << L"\t" << tempData.now_Hp << L"\t" << tempData.now_Mp << endl;
				}

			}
			SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
		}
	}
	else {
		HandleDiagnosticRecord(hdbc, SQL_HANDLE_DBC, retcode);
	}
	cout << " --------------------------------------------------------------- " << endl;
}

void CDataBase::UpdateData(Player data)
{
	retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);

	TCHAR szSQL[256];
	swprintf(szSQL, L"UPDATE [position].[dbo].[GAME_DB] SET xPos = %d, yPos = %d, Lv = %d, Exp = %d, HP = %d, MP = %d WHERE id = '%s'", data.xPos, data.zPos, data.level, data.exp, data.now_Hp, data.now_Mp,	data.loginID);
	if (SQLExecDirect(hstmt, (SQLWCHAR*)szSQL, SQL_NTS) == SQL_ERROR) {
		HandleDiagnosticRecord(hdbc, SQL_HANDLE_DBC, retcode);
		cout << "에러 발생 " << endl;
	}
	else {
		wcout << "Player " << data.loginID << " Disconnected.		Position : (" << data.xPos << ", " << data.zPos << ")" << endl;
	}
	SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
}

void CDataBase::AddPlayer(Player player)
{
	retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);

	TCHAR szSQL[256];
	swprintf(szSQL, L"INSERT INTO[position].[dbo].[GAME_DB] ([ID], [xPos], [yPos], [Lv], [Exp], [HP], [MP]) VALUES( '%s', %d, %d, %d, %d, %d, %d)",player.loginID, player.xPos, player.zPos, player.level, player.exp, player.now_Hp, player.now_Mp);

	if (SQLExecDirect(hstmt, (SQLWCHAR*)szSQL, SQL_NTS) == SQL_ERROR) {
		HandleDiagnosticRecord(hdbc, SQL_HANDLE_DBC, retcode);
		cout << "에러 발생 " << endl;
	}
	else {
		wcout << "Add Player " << player.loginID << "	Position : (" << player.xPos << ", " << player.zPos << ")" << endl;
	}
	SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
}