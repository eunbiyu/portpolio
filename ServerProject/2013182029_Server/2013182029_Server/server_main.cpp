#include "Server.h"
//#pragma comment (lib, "lua53.lib")
//
//extern "C" {
//	#include "lua.h"
//	#include "lauxlib.h"
//	#include "lualib.h"
//}

bool Is_InRange(int i, int j)
{
	int dist = (clientArray[i].player.xPos - clientArray[j].player.xPos)
		* (clientArray[i].player.xPos - clientArray[j].player.xPos);
	dist += (clientArray[i].player.zPos - clientArray[j].player.zPos)
		* (clientArray[i].player.zPos - clientArray[j].player.zPos);

	return dist <= VIEW_RADIUS * VIEW_RADIUS;
}

bool Is_InRange_Monster(int clientID, int monsterID)
{
	int dist = (clientArray[clientID].player.xPos - monsterArray[monsterID].xPos) * (clientArray[clientID].player.xPos - monsterArray[monsterID].xPos);
	dist += (clientArray[clientID].player.zPos - monsterArray[monsterID].zPos) * (clientArray[clientID].player.zPos - monsterArray[monsterID].zPos);

	return dist <= VIEW_RADIUS * VIEW_RADIUS;
}

void add_timer(int objectID, Object_Type type, int m_sec, OperationType eventType)
{
	Event_timer event_object = { objectID, type, m_sec + GetTickCount(), eventType};
	timer_lock.lock();
	p_queue.push(event_object);
	timer_lock.unlock();
}

void Timer_Thread()
{
	while (true) {
		Sleep(1); // 바로 타이머쓰면 부하걸려서 걸어준거. 
		timer_lock.lock();
		while (!p_queue.empty()) {
			Event_timer top_object = p_queue.top();

			if (top_object.wakeup_time > GetTickCount())
				break;

			p_queue.pop();

			OverlapEx *overlapped = new OverlapEx;
			ZeroMemory(&overlapped->original_overlap, sizeof(overlapped->original_overlap));
			overlapped->operation = top_object.eventType;

			switch (top_object.objectType) {
			case ePlayer:
				//GetQueue에 쓰도록 저장. Post는 시간값을 넣어서 이벤트가 어떤시간에 이런일을 처리해야되 이런거를 워커쓰레드에 정보로 넘겨줌. 
				//Post는 일을 목록에 추가, GetQueue는 그일을 처리 
				PostQueuedCompletionStatus(g_hIOCP, 1, clientArray[top_object.objectID].id, reinterpret_cast<LPOVERLAPPED>(overlapped));
				break;
			case eMonster_Peace:
				PostQueuedCompletionStatus(g_hIOCP, 1, monsterArray[top_object.objectID].controlID, reinterpret_cast<LPOVERLAPPED>(overlapped));
				break;
			case eMonster_War:
				cout << " Timer_Thread::eMonster_War 아직 정의하지 않음 " << endl;
				break;
			}
		}
		timer_lock.unlock();
	}
}

void Create_Monster()
{
	int i = 0;

	int x = 0, z = 0;
	for (auto& monster : monsterArray){
		monster.controlID = i++;
		//몬스터 좌표생성 ---수정수정 
		monster.xPos = rand() % (BOARD_WIDTH*2) + 3;
		monster.zPos = rand() % (BOARD_HEIGHT * 2)+ 3 ;
		
	/*	monster.xPos = x;
		monster.zPos = z++;
		if (z % BOARD_WIDTH == 0) {
			z = 0;
			x++;
		}*/

		monster.InitData();
		monster.type = Object_Type::eMonster_Peace;

//		add_timer(monster.controlID, Object_Type::eMonster_Peace, 1000, OP_MOVE);
		//cout << "Pos : (" << monster.xPos << ", " << monster.zPos << ") \t";// << endl;
	}
}

void Create_Map()
{
	for (auto& rock : rockArray) {
		rock.x = rand() % BOARD_WIDTH;
		rock.z = rand() % BOARD_HEIGHT;
	}
}

void Initialize_Server()
{
	//_wsetlocale(LC_ALL, L"korean");
	WSADATA   wsadata;
	WSAStartup(MAKEWORD(2, 2), &wsadata);

	InitializeCriticalSection(&g_CriticalSection);
	g_hIOCP = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0); // IOCP 객체생성 

	for (int i = 0; i < MAX_USER; ++i) {
		clientArray[i].recv_overlap.recv_buff.buf = reinterpret_cast<CHAR *>(clientArray[i].recv_overlap.socket_buff);
		clientArray[i].recv_overlap.recv_buff.len = MAX_BUFF_SIZE;
		clientArray[i].recv_overlap.operation = OP_RECV;
		clientArray[i].is_connected = false;
	}

	Create_Monster();
	Create_Map();


	cout << "▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼" << endl;
	cout << "----------------------서 버 S T A R T-----------------------" << endl;
	cout << "-----------------------접 속 대 기 중-----------------------" << endl; 
	cout << "▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲" << endl;
	// DataBase Init
	g_Database = new CDataBase;
	g_LoginPlayer.controlID = 0;

	g_Database->ShowData();
}

void SendPacket(int id, unsigned char* packet) {

	// 아래처럼 지역변수로 놔두면 안된다. 데이터 다 보내기도 전에 소멸되므로
	// OverlapEx send_over;

	OverlapEx* send_over = new OverlapEx;
	memset(send_over, 0, sizeof(OverlapEx));			// 꼭 해줘야한다!!!!!!!!!!!
	send_over->operation = OP_SEND;
	send_over->recv_buff.buf = reinterpret_cast<CHAR*>(send_over->socket_buff);
	send_over->recv_buff.len = packet[0];
	memcpy(send_over->socket_buff, packet, packet[0]);

	int result = WSASend(clientArray[id].sock, &send_over->recv_buff, 1, NULL, 0, &send_over->original_overlap, NULL);
	if ((0 != result) && (WSA_IO_PENDING != result)) {
		int error_num = WSAGetLastError();

		if (WSA_IO_PENDING != error_num)
			error_display("SendPacket : WSASend", error_num);
		while (true);
	}
}

// --------------------------------------------------------------------------------------- //
void SendChatPacket(int sendClientID, Object_Type type, int objectID, WCHAR *message)
{
	sc_packet_chat chat_packet;
	chat_packet.id = objectID;
	chat_packet.size = sizeof(chat_packet);
	chat_packet.type = SC_CHAT;
	chat_packet.objectType = type;

	wcscpy_s(chat_packet.message, message);
	SendPacket(sendClientID, reinterpret_cast<unsigned char *>(&chat_packet));
}

void SendAddObjectPacket(int sendClientID, Object_Type type, int objectID)
{
	sc_packet_Add_Object packet;

	packet.size = sizeof(packet);
	packet.type = SC_ADD_OBJECT;

	packet.id = objectID;
	packet.objectType = type;

	switch (type) {
	case Object_Type::ePlayer:
		packet.x = clientArray[objectID].player.xPos;
		packet.y = clientArray[objectID].player.zPos;

		packet.viewDir = clientArray[objectID].player.viewDir;
		packet.level = clientArray[objectID].player.level;
		packet.exp = clientArray[objectID].player.exp;
		packet.hp = clientArray[objectID].player.now_Hp;
		packet.mp = clientArray[objectID].player.now_Mp;
		break;
	case Object_Type::eMonster_Peace:
		packet.x = monsterArray[objectID].xPos;
		packet.y = monsterArray[objectID].zPos;

		packet.viewDir = monsterArray[objectID].viewDir;
		packet.level = monsterArray[objectID].level;
		packet.exp = monsterArray[objectID].exp;
		packet.hp = monsterArray[objectID].now_Hp;
		packet.mp = monsterArray[objectID].now_Mp;
		break;
	case Object_Type::eMonster_War:
		cout << "SendAddObjectPacket :: 정의하지 않음" << endl;
		break;
	case 0:
		cout << "SendAddObjectPacket :: 지정하지 않은 Object Type!!" << endl;
		break;
	default:
		cout << "SendAddObjectPacket :: 존재하지 않은 Object Type!!" << endl;
		break;
	}

	SendPacket(sendClientID, reinterpret_cast<unsigned char*>(&packet));
}

void SendRemoveObjectPacket(int sendClientID, Object_Type type, int objectID)
{
	sc_packet_Remove_Object packet;

	packet.size = sizeof(packet);
	packet.type = SC_REMOVE_OBJECT;

	packet.id = objectID;
	packet.objectType = type;

	SendPacket(sendClientID, reinterpret_cast<unsigned char*>(&packet));
}

void SendMoveObjectPacket(int sendClientID, Object_Type type, int objectID)
{
	sc_packet_Position_Info packet;

	packet.size = sizeof(packet);
	packet.type = SC_POSITION_INFO;

	packet.id = objectID;
	packet.objectType = type;
	
	switch (type) {
	case Object_Type::ePlayer:
		packet.x = clientArray[objectID].player.xPos;
		packet.y = clientArray[objectID].player.zPos;
		packet.viewDir = clientArray[objectID].player.viewDir;
		break;
	case Object_Type::eMonster_Peace:
		packet.x = monsterArray[objectID].xPos;
		packet.y = monsterArray[objectID].zPos;
		packet.viewDir = monsterArray[objectID].viewDir;
		break;
	case Object_Type::eMonster_War:
		cout << "SendMoveObjectPacket :: 정의하지 않음" << endl;
		break;
	case 0:
		cout << "SendMoveObjectPacket :: 지정하지 않은 Object Type!!" << endl;
		break;
	default:
		cout << "SendMoveObjectPacket :: 존재하지 않는 Object Type!!" << endl;
		break;
	}

	SendPacket(sendClientID, reinterpret_cast<unsigned char*>(&packet));
}

void SendStatChangeObjectPacket(int sendClientID, Object_Type type, int objectID, int level, int exp, int hp, int mp)
{
	sc_packet_Stat_Change packet;
	packet.size = sizeof(packet);
	packet.type = SC_STAT_CHANGE;
	packet.objectType = type;

	switch (type) {
	case Object_Type::ePlayer:
		packet.id = clientArray[objectID].id;
		packet.level = clientArray[objectID].player.level;
		packet.exp = clientArray[objectID].player.exp;
		packet.hp = clientArray[objectID].player.now_Hp;
		packet.mp = clientArray[objectID].player.now_Mp;
		break;
	case Object_Type::eMonster_Peace:
		packet.id = monsterArray[objectID].controlID;
		packet.level = monsterArray[objectID].level;
		packet.exp = monsterArray[objectID].exp;
		packet.hp = monsterArray[objectID].now_Hp;
		packet.mp = monsterArray[objectID].now_Mp;
		break;
	case Object_Type::eMonster_War:
		cout << "SendStatChangeObjectPacket :: 정의하지 않음 " << endl;
		break;
	case 0:
		cout << "SendStatChangeObjectPacket :: 지정하지 않은 Object Type!!" << endl;
		break;
	default:
		cout << "SendStatChangeObjectPacket :: 존재하지 않는 Object Type!!" << endl;
		break;
	}

	SendPacket(sendClientID, reinterpret_cast<unsigned char*>(&packet));
}
// --------------------------------------------------------------------------------------- //

void ProcessPacket(int id, unsigned char *packet)
{
	// 패킷 종류별로 처리가 달라진다.
	// [0] : size, [1] : type
	int dx = clientArray[id].player.xPos;
	int dz = clientArray[id].player.zPos;

	unsigned char packet_type = packet[1];

	switch (packet_type) {
	case CS_LOGIN:
	{
		clientArray[id].vl_lock.lock();
		clientArray[id].sock = g_LoginPlayer.controlSocket;
		clientArray[id].id = g_LoginPlayer.controlID;
		clientArray[id].vl_lock.unlock();

		cs_packet_Login* recv_login_packet = reinterpret_cast<cs_packet_Login*>(packet);

		// 접속 확인
		for (int i = 0; i < MAX_USER; ++i) {
			if (true == clientArray[i].is_connected) {
				if (wcscmp(recv_login_packet->loginID, clientArray[i].player.loginID) == 0) {
					// --------------------------------------------- //
					sc_packet_Login_FAIL send_login_packet;
					send_login_packet.size = sizeof(send_login_packet);
					send_login_packet.type = SC_LOGIN_FAIL;
					// --------------------------------------------- //

					SendPacket(id, reinterpret_cast<unsigned char*>(&send_login_packet));
					return;
				}	
			}
		}

		Player playerData;
		// no exist Data in DB
		if (false == g_Database->FindPlayerID_FromDB(recv_login_packet->loginID)) {
			wcout << "새로운 아이디 생성 : " << recv_login_packet->loginID << endl;

			wcscpy(playerData.loginID,recv_login_packet->loginID);
			playerData.xPos = InitPlayerInfo::eStart_xPos;
			playerData.zPos = InitPlayerInfo::eStart_zPos;
			playerData.max_Hp = InitPlayerInfo::eHp;
			playerData.max_Mp = InitPlayerInfo::eMp;
			playerData.now_Hp = InitPlayerInfo::eHp;
			playerData.now_Mp = InitPlayerInfo::eMp;
			playerData.exp = InitPlayerInfo::eExp;
			playerData.level = InitPlayerInfo::eLevel;

			// Insert Data in DB
			g_Database->AddPlayer(playerData);
		}
		// exist Data in DB
		else
			playerData = g_Database->FindPlayerData_FromDB(recv_login_packet->loginID);

		// 접속시 플레이어 초기화
		clientArray[id].vl_lock.lock();
		clientArray[id].is_connected = true;
		clientArray[id].player.isActive = true;
		clientArray[id].player.isAlive = true;
	
		wcscpy(clientArray[id].player.loginID, playerData.loginID);
		clientArray[id].player.xPos = playerData.xPos;
		clientArray[id].player.zPos = playerData.zPos;
		clientArray[id].player.level = playerData.level;
		clientArray[id].player.exp = playerData.exp;
		clientArray[id].player.now_Hp = playerData.now_Hp;
		clientArray[id].player.now_Mp = playerData.now_Mp;
		clientArray[id].player.max_Hp = clientArray[id].player.level * 100;
		clientArray[id].player.max_Mp = InitPlayerInfo::eMp;
		add_timer(id, Object_Type::ePlayer, RECOVERY_HPTIME, OP_HPRecovery);
		add_timer(id, Object_Type::ePlayer, RECOVERY_MPTIME, OP_MPRecovery);

		clientArray[id].recv_overlap.operation = OP_RECV;
		clientArray[id].recv_overlap.packet_size = 0;
		clientArray[id].previous_data_size = 0;
		clientArray[id].vl_lock.unlock();
		wcout << "Player " << playerData.loginID << " Connected.		Position : (" << clientArray[id].player.xPos << ", " << clientArray[id].player.zPos << ")" << endl;


		// Send Map Date 
		sc_packet_MapData packet;
		packet.type = SC_MAPDATA;
		packet.size = sizeof(packet);

		for (auto rock : rockArray) {
			packet.x = rock.x;
			packet.y = rock.z;

			SendPacket(id, reinterpret_cast<unsigned char *>(&packet));
		}
		//


		sc_packet_Add_Object put_player_pacekt;
		put_player_pacekt.size = sizeof(put_player_pacekt);
		put_player_pacekt.type = SC_ADD_OBJECT;

		put_player_pacekt.objectType = Object_Type::ePlayer;
		put_player_pacekt.id = id;
		put_player_pacekt.x = playerData.xPos;
		put_player_pacekt.y = playerData.zPos;
	
		// Veiw List Clear
		clientArray[id].vl_lock.lock();
		clientArray[id].view_list_OtherPlayer.clear();
		clientArray[id].vl_lock.unlock();

		clientArray[id].vl_lock.lock();
		clientArray[id].view_list_Monster.clear();
		clientArray[id].vl_lock.unlock();

		// 플레이어 접속 알림
		for (int i = 0; i < MAX_USER; ++i) {
			if (true == clientArray[i].is_connected) {
				if (false == Is_InRange(id, i)) 
					continue;

				clientArray[i].vl_lock.lock();
				if (id != i)
					clientArray[i].view_list_OtherPlayer.insert(id);
				clientArray[i].vl_lock.unlock();

				SendPacket(i, reinterpret_cast<unsigned char*>(&put_player_pacekt));
			}
		}

		// 시야 처리
		for (int i = 0; i < MAX_USER; ++i) {
			if (false == clientArray[i].is_connected) continue;
			if (i == id) continue;
			if (false == Is_InRange(id, i)) continue;

			clientArray[id].vl_lock.lock();
			clientArray[id].view_list_OtherPlayer.insert(i);
			clientArray[id].vl_lock.unlock();

			put_player_pacekt.id = i;
			put_player_pacekt.x = clientArray[i].player.xPos;
			put_player_pacekt.y = clientArray[i].player.zPos;
			SendPacket(id, reinterpret_cast<unsigned char *> (&put_player_pacekt));
		}

		// NPC 처리
		for(auto& monster : monsterArray) {
			if (false == monster.isAlive)
				continue;
			if (false == Is_InRange_Monster(id, monster.controlID))
				continue;
			
			monster.isActive = true;
			add_timer(monster.controlID, Object_Type::eMonster_Peace, 1000, OP_MOVE );
			SendAddObjectPacket(id, Object_Type::eMonster_Peace, monster.controlID);
		}

		// --------------------------------------------- //
		sc_packet_Login_OK send_login_packet;
		send_login_packet.size = sizeof(send_login_packet);
		send_login_packet.type = SC_LOGIN_OK;
		
		send_login_packet.controlID = id;
		send_login_packet.xPos = playerData.xPos;
		send_login_packet.zPos = playerData.zPos;
		send_login_packet.hp = playerData.now_Hp;
		send_login_packet.mp = playerData.now_Mp;
		send_login_packet.level = playerData.level;
		send_login_packet.exp = playerData.exp;
		// --------------------------------------------- //

		SendPacket(id, reinterpret_cast<unsigned char*>(&send_login_packet));
		return;
	}
		break;
	case CS_MOVE:
		if (true == clientArray[id].player.isAlive) {
			switch (packet[2]) {
			case Direction_Type::eUp:
				dz--;
				for (auto rock : rockArray)
					if (rock.x == dx && rock.z == dz) {
						dz++;
						return;
					}

				break;
			case Direction_Type::eDown:
				dz++;
				for (auto rock : rockArray)
					if (rock.x == dx && rock.z == dz) {
						dz--;
						return;
					}
				break;
			case Direction_Type::eLeft:
				dx--;
				for (auto rock : rockArray)
					if (rock.x == dx && rock.z == dz) {
						dx++;
						return;
					}
				break;
			case Direction_Type::eRight:
				dx++;
				for (auto rock : rockArray)
					if (rock.x == dx && rock.z == dz) {
						dx--;
						return;
					}
				break;
			}
		}
		break;
	case CS_ATTACK:
	{
		int attackPosX = clientArray[id].player.xPos;
		int attackPosZ = clientArray[id].player.zPos;

		for (auto& monsterID : clientArray[id].view_list_Monster) {
			if (monsterArray[monsterID].Damaged_InPos(attackPosX, attackPosZ)) {
				g_MonsterLock.lock();
				monsterArray[monsterID].now_Hp -= ATTACK_DAMAGE;
				g_MonsterLock.unlock();
				TCHAR str[300];
				swprintf(str, L"'%s'가 몬스터 '%d'에게 데미지 %d을 입혔습니다.", clientArray[id].player.loginID, monsterID, ATTACK_DAMAGE);
				SendChatPacket(id, Object_Type::ePlayer, id, str);
				SendChatPacket(id, Object_Type::eMonster_Peace, monsterID, L"Damage!");

				// die Monster
				if (monsterArray[monsterID].now_Hp <= 0) {
					g_MonsterLock.lock();
					monsterArray[monsterID].now_Hp = 0;
					monsterArray[monsterID].isAlive = false;
					g_MonsterLock.unlock();

					clientArray[id].vl_lock.lock();
					clientArray[id].player.exp += monsterArray[monsterID].exp;
					clientArray[id].player.UpdateExp();
					clientArray[id].vl_lock.unlock();
					SendRemoveObjectPacket(id, Object_Type::eMonster_Peace, monsterID);
					SendStatChangeObjectPacket(id, Object_Type::ePlayer, id, clientArray[id].player.level, clientArray[id].player.exp, 0, 0);
				}

				SendStatChangeObjectPacket(id, Object_Type::eMonster_Peace, monsterID, 0, 0, monsterArray[monsterID].now_Hp, 0);
			}
		}
	}
		break;
	default: 
		cout << "존재하지 않은 패킷 타입!!\n";
		//exit(0);
		break;
	}
	if (dz < 0) dz = 0;
	if (dz >= BOARD_HEIGHT) dz = BOARD_HEIGHT - 1;
	if (dx < 0) dx = 0;
	if (dx >= BOARD_WIDTH) dx = BOARD_WIDTH - 1;

	// 이동 처리
	clientArray[id].player.xPos = dx;
	clientArray[id].player.zPos = dz;

	SendMoveObjectPacket(id, Object_Type::ePlayer, id);

#pragma region [Update view PlayerList]
	// Update view PlayerList 
	unordered_set <int> new_list_Player;
	for (auto i = 0; i < MAX_USER; ++i) {
		if (false == clientArray[i].player.isAlive) continue;
		if (false == clientArray[i].is_connected) continue;
		if (i == id) continue;
		if (false == Is_InRange(i, id)) continue;
		new_list_Player.insert(i);
	}

	// Apply Player viewList
	for (auto i : new_list_Player) {
		clientArray[id].vl_lock.lock();
		// 새로 뷰리스트에 들어오는 객체 처리
		if (0 == clientArray[id].view_list_OtherPlayer.count(i)) {
			clientArray[id].view_list_OtherPlayer.insert(i);
			clientArray[id].vl_lock.unlock();
			SendAddObjectPacket(id, Object_Type::ePlayer, i);

			clientArray[id].vl_lock.lock();
			if (0 == clientArray[i].view_list_OtherPlayer.count(id)) {
				clientArray[i].view_list_OtherPlayer.insert(id);
				clientArray[id].vl_lock.unlock();
				SendAddObjectPacket(i, Object_Type::ePlayer, id);
			}
			else {
				clientArray[id].vl_lock.unlock();
				SendMoveObjectPacket(i, Object_Type::ePlayer, id);
			}
		}
		// 뷰리스트에 계속 유지되어 있는 객체 처리
		else {
			clientArray[id].vl_lock.unlock();
			
			clientArray[i].vl_lock.lock();
			if (1 == clientArray[i].view_list_OtherPlayer.count(id)) {
				clientArray[i].vl_lock.unlock();
				SendMoveObjectPacket(i, Object_Type::ePlayer, id);
			}
			else {
				clientArray[i].view_list_OtherPlayer.insert(id);
				clientArray[i].vl_lock.unlock();
				SendAddObjectPacket(i, Object_Type::ePlayer, id);
			}
		}
	}

	// delete Player Object
	vector <int> remove_list_Player;

	clientArray[id].vl_lock.lock();
	for (auto playerID : clientArray[id].view_list_OtherPlayer) {
		if (0 != new_list_Player.count(playerID))
			continue;
		remove_list_Player.push_back(playerID);
	}

	for (auto playerID : remove_list_Player)
		clientArray[id].view_list_OtherPlayer.erase(playerID);
	clientArray[id].vl_lock.unlock();

	for (auto playerID : remove_list_Player) {
		SendRemoveObjectPacket(id, Object_Type::ePlayer, playerID);
	}

	for (auto playerID : remove_list_Player) {
		clientArray[playerID].vl_lock.lock();
		if (0 != clientArray[playerID].view_list_OtherPlayer.count(id)) {
			clientArray[playerID].view_list_OtherPlayer.erase(id);
			clientArray[playerID].vl_lock.unlock();
			SendRemoveObjectPacket(playerID, Object_Type::ePlayer, id);
		}
		else
			clientArray[playerID].vl_lock.unlock();
	}
#pragma endregion

#pragma region [Update view MonsterList]
	// Update view MonsterList
	unordered_set <int> new_list_Monster;
	for (auto i = 0; i < NUM_OF_MONSTER; ++i) {
		if (false == monsterArray[i].isAlive) continue;
		if (false == Is_InRange_Monster(id, i)) continue;
		new_list_Monster.insert(i);
	}

	// Apply Monster viewList 
	for (auto monsterID : new_list_Monster) {
		clientArray[id].vl_lock.lock();
		// 새로 뷰리스트에 들어오는 객체 처리
		if (0 == clientArray[id].view_list_Monster.count(monsterID)) {
			clientArray[id].view_list_Monster.insert(monsterID);
			clientArray[id].vl_lock.unlock();

			monsterArray[monsterID].isActive = true;
			add_timer(monsterID, Object_Type::eMonster_Peace, 1000, OP_MOVE);
			SendAddObjectPacket(id, Object_Type::eMonster_Peace, monsterID);
		}
		// 뷰리스트에 계속 유지되어 있는 객체 처리
		else {
			clientArray[id].vl_lock.unlock();
			SendMoveObjectPacket(id, Object_Type::eMonster_Peace, monsterID);
		}
	}

	// delete Monster Object
	unordered_set <int> remove_list_Monster;

	clientArray[id].vl_lock.lock();
	for (auto monsterID : clientArray[id].view_list_Monster) {
		if (0 != new_list_Monster.count(monsterID))
			continue;
		remove_list_Monster.insert(monsterID);
	}
	
	for (auto monsterID : remove_list_Monster) 
			clientArray[id].view_list_Monster.erase(monsterID);
	clientArray[id].vl_lock.unlock();

	for (auto monsterID : remove_list_Monster) {
		g_MonsterLock.lock();
		monsterArray[monsterID].isActive = false;
		g_MonsterLock.unlock();

		SendRemoveObjectPacket(id, Object_Type::eMonster_Peace, monsterID);
	}
#pragma endregion
}

void Do_Move(int i)
{
	if (false == monsterArray[i].isAlive) return;
	if (false == monsterArray[i].isActive) return;

	int x = monsterArray[i].xPos;
	int y = monsterArray[i].zPos;

	unordered_set<int> view_list;
	for (auto pl = 0; pl < MAX_USER; pl++) {
		if (false == clientArray[pl].is_connected) continue;
		if (false == clientArray[pl].player.isAlive) continue;
		if (false == Is_InRange_Monster(pl, i)) continue;
		view_list.insert(pl);
	}
	// 타겟이 없으면 랜덤 이동
	if (false == monsterArray[i].isAttack) {
		if (false == monsterArray[i].isTarget) {
			switch (rand() % 4) {
			case 0: if (x < BOARD_WIDTH - 1) x++; break;
			case 1: if (x > 0) x--; break;
			case 2: if (y < BOARD_HEIGHT - 1) y++; break;
			case 3: if (y > 0) y--; break;
			}
		}
		// 타겟이 있으면 타겟 방향으로 이동
		else {
			if (clientArray[monsterArray[i].targetPlayerID].player.xPos - monsterArray[i].xPos > 0) {
				if (x < BOARD_WIDTH - 1) {
					x++;
					monsterArray[i].viewDir = eRight;
				}
			}
			else if (clientArray[monsterArray[i].targetPlayerID].player.xPos - monsterArray[i].xPos < 0) {
				if (x > 0) {
					x--;
					monsterArray[i].viewDir = eLeft;
				}
			}

			if (clientArray[monsterArray[i].targetPlayerID].player.zPos - monsterArray[i].zPos > 0) {
				if (y < BOARD_HEIGHT - 1) {
					y++;
					monsterArray[i].viewDir = eUp;
				}
			}
			else if (clientArray[monsterArray[i].targetPlayerID].player.zPos - monsterArray[i].zPos < 0) {
				if (y > 0) {
					y--;
					monsterArray[i].viewDir = eDown;
				}
			}
		}
	}
	else {
		int playerID = monsterArray[i].targetPlayerID;
		clientArray[playerID].vl_lock.lock();
		clientArray[playerID].player.now_Hp -= ATTACK_DAMAGE;
		
		// die Player
		if (clientArray[playerID].player.now_Hp <= 0) {
			clientArray[playerID].player.now_Hp = 0;
			clientArray[playerID].player.isAlive = false;
			clientArray[playerID].player.exp /= 2;

			SendRemoveObjectPacket(playerID, Object_Type::ePlayer, playerID);
			add_timer(playerID, Object_Type::ePlayer, 3000, OP_Revival);
		}
		SendStatChangeObjectPacket(playerID, Object_Type::ePlayer, playerID, clientArray[playerID].player.level, clientArray[playerID].player.exp, clientArray[playerID].player.now_Hp, clientArray[playerID].player.now_Mp);
		clientArray[playerID].vl_lock.unlock();
		
		g_MonsterLock.lock();
		if (false == monsterArray[i].IsInAttackRange(clientArray[monsterArray[i].targetPlayerID].player.xPos, clientArray[monsterArray[i].targetPlayerID].player.zPos)) {
			monsterArray[i].isAttack = false;
//			cout << "공격 취소" << endl;
		}
		g_MonsterLock.unlock();
	}

	g_MonsterLock.lock();
	monsterArray[i].xPos = x;
	monsterArray[i].zPos = y;
	g_MonsterLock.unlock();


	for(int playerID = 0; playerID < MAX_USER; ++playerID) {
		if (monsterArray[i].IsInAttackRange(clientArray[playerID].player.xPos, clientArray[playerID].player.zPos)) {
			monsterArray[i].isAttack = true;
			monsterArray[i].targetPlayerID = playerID;
//			cout << "공격 시작" << endl;
			break;
		}
		if (monsterArray[i].IsInViewRange(clientArray[playerID].player.xPos, clientArray[playerID].player.zPos)) {
			monsterArray[i].targetPlayerID = playerID;
			monsterArray[i].isTarget = true;
			break;
		}
	}



	// Update Player ViewList 
	unordered_set <int> new_list;
	for (auto pl = 0; pl < MAX_USER; pl++) {
		if (false == clientArray[pl].is_connected) continue;
		if (false == clientArray[pl].player.isAlive) continue;
		if (false == Is_InRange_Monster(pl, i)) continue;
		new_list.insert(pl);
	}

	for (auto pl : view_list) {
		if (0 == new_list.count(pl)) {
			// 몬스터가 이동한 후 새로운 뉴 리스트에 없으면 객체 제거
			clientArray[pl].vl_lock.lock();

			// 이거 의심하기
			clientArray[pl].view_list_Monster.erase(i);
			// ----------------

			clientArray[pl].vl_lock.unlock();

			SendRemoveObjectPacket(pl, Object_Type::eMonster_Peace, i);
		}
		else {
			SendMoveObjectPacket(pl, Object_Type::eMonster_Peace, i);
		}
	}

	for (auto pl : new_list) {
		if (0 != view_list.count(pl))
			continue;

		monsterArray[i].isActive = true;
		add_timer(i, Object_Type::eMonster_Peace, 1000, OP_MOVE);
		SendAddObjectPacket(pl, Object_Type::eMonster_Peace, i);
	}
	
	add_timer(i, Object_Type::eMonster_Peace, 1000, OP_MOVE);

	if (true == new_list.empty())
		monsterArray[i].isActive = false;
}

void Accept_Thread()
{
	sockaddr_in listen_addr;
	// Overlapped 소켓으로 받을 것이므로 옵션 추가
	SOCKET accept_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);

	ZeroMemory(&listen_addr, sizeof(listen_addr));
	listen_addr.sin_family = AF_INET;
	listen_addr.sin_addr.s_addr = htonl(INADDR_ANY);      //모든 클라이언트의 주소에서 받아라
	listen_addr.sin_port = htons(MY_SERVER_PORT);
	ZeroMemory(&listen_addr.sin_zero, 8);

	//C++ 의 bind와 socket programing의 bind가 충돌한다
	::bind(accept_socket, reinterpret_cast<SOCKADDR*>(&listen_addr), sizeof(listen_addr));

	listen(accept_socket, 10);

	while (true) {
		sockaddr_in client_addr;
		int add_size = sizeof(client_addr);

		SOCKET new_client = ::WSAAccept(accept_socket, reinterpret_cast<SOCKADDR*>(&client_addr), &add_size, NULL, NULL);

		// 새로운 아이디 할당
		int new_id = -1;
		for (int i = 0; i < MAX_USER; ++i) {
			if (false == clientArray[i].is_connected) {
				new_id = i;
				break;
			}
		}

		if (-1 == new_id) {
			cout << "The Server is Full of User" << endl;
			closesocket(new_client);
			continue;
		}

		// 입출력 포트와 클라이언트 연결
		CreateIoCompletionPort(reinterpret_cast<HANDLE>(new_client), g_hIOCP, new_id, 0);
		g_LoginPlayer.controlID = new_id;
		g_LoginPlayer.controlSocket = new_client;

		DWORD flags = 0;
		int result = WSARecv(new_client, &clientArray[new_id].recv_overlap.recv_buff, 1, NULL, &flags, &clientArray[new_id].recv_overlap.original_overlap, NULL);

		if (0 != result) {
			int error_num = WSAGetLastError();
			if (WSA_IO_PENDING != error_num) {
				error_display("AcceptThread : WSARecv ", error_num);
			}
		}
	}
}

void Worker_Thread() {
	DWORD io_Size = 0;
	DWORD key = 0;
	OverlapEx* overlap = nullptr;
	bool bResult = true;

	while (true) {
		bResult = GetQueuedCompletionStatus(g_hIOCP, &io_Size, &key, reinterpret_cast<LPOVERLAPPED*>(&overlap), INFINITE);
		if (false == bResult) {
			//Error 처리
		}
		if (0 == io_Size) {
			closesocket(clientArray[key].sock);

			sc_packet_Remove_Object rp_packet;
			rp_packet.id = key;
			rp_packet.size = sizeof(rp_packet);
			rp_packet.type = SC_REMOVE_OBJECT;

			for (auto i = 0; i < MAX_USER; ++i) {
				if (false == clientArray[i].is_connected) continue;
				if (key == i) continue;

				clientArray[i].vl_lock.lock();
				if (0 != clientArray[i].view_list_OtherPlayer.count(key)) {
					clientArray[i].view_list_OtherPlayer.erase(key);
					clientArray[i].vl_lock.unlock();
					SendPacket(i, reinterpret_cast<unsigned char*>(&rp_packet));
				}
				else {
					clientArray[i].vl_lock.unlock();
				}
			}
			clientArray[key].is_connected = false;
			clientArray[key].player.isAlive = false;

			// DB에 데이터 저장
			Player playerData;
			wcscpy(playerData.loginID, clientArray[key].player.loginID);
			playerData.xPos = clientArray[key].player.xPos;
			playerData.zPos = clientArray[key].player.zPos;
			playerData.now_Hp = clientArray[key].player.now_Hp;
			playerData.now_Mp = clientArray[key].player.now_Mp;
			playerData.exp = clientArray[key].player.exp;
			playerData.level = clientArray[key].player.level;

			g_Database->UpdateData(playerData);
			continue;
		}
		if (overlap != NULL) {
			switch (overlap->operation) {
			case OP_RECV: {
				unsigned char* pBuff = overlap->socket_buff;
				int remained = io_Size;

				//남은 데이터 사이즈만큼 순회하면서 처리해라.
				while (0 < remained) {
					if (clientArray[key].recv_overlap.packet_size == 0) {
						clientArray[key].recv_overlap.packet_size = pBuff[0];      //모든 패킷의 맨 앞 칸은 사이즈이다.
					}

					// 이전에 받은 패킷과 총량을 비교하여 남은 데이터 조사
					int required = clientArray[key].recv_overlap.packet_size - clientArray[key].previous_data_size;

					// 패킷 완성
					if (remained >= required) {
						//지난번에 받은 데이터 뒷부분에 복사
						memcpy(clientArray[key].packet + clientArray[key].previous_data_size, pBuff, required);
						ProcessPacket(key, clientArray[key].packet);
						remained -= required;
						pBuff += required;
						clientArray[key].recv_overlap.packet_size = 0;
						clientArray[key].previous_data_size = 0;
					}
					else {
						memcpy(clientArray[key].packet + clientArray[key].previous_data_size, pBuff, remained);
						//미완성 패킷의 사이즈가 remained만큼 증가했다.
						clientArray[key].previous_data_size += remained;
						remained = 0;
						pBuff++;
					}
				}
				DWORD flags = 0;
				WSARecv(clientArray[key].sock, &clientArray[key].recv_overlap.recv_buff, 1, NULL, &flags, reinterpret_cast<LPWSAOVERLAPPED>(&clientArray[key].recv_overlap), NULL);

				break;
			}
			case OP_SEND:
				// ioSize하고 실제 보낸 크기 비교문 넣으라함 if()
				// 비교 후 소켓 접속 끊기

				//IO 사이즈와 실제 보낸 크기 비교
				delete overlap;
				break;
			case OP_MOVE:
				Do_Move(key);
				delete overlap;
				break;
			case OP_HPRecovery:
				if (clientArray[key].is_connected) {
					if (clientArray[key].player.isAlive) {
						clientArray[key].vl_lock.lock();
						clientArray[key].player.now_Hp += (clientArray[key].player.max_Hp / RECOVERY_HP);
						if (clientArray[key].player.now_Hp > clientArray[key].player.max_Hp)
							clientArray[key].player.now_Hp = clientArray[key].player.max_Hp;

						clientArray[key].vl_lock.unlock();

						SendStatChangeObjectPacket(key, Object_Type::ePlayer, key, clientArray[key].player.level, clientArray[key].player.exp, clientArray[key].player.now_Hp, clientArray[key].player.now_Mp);
						add_timer(key, Object_Type::ePlayer, RECOVERY_HPTIME, OP_HPRecovery);
					}
				}

				delete overlap;
				break;
			case OP_MPRecovery:
				if (clientArray[key].is_connected) {
					if (clientArray[key].player.isAlive) {
						clientArray[key].vl_lock.lock();
						clientArray[key].player.now_Mp += (RECOVERY_MP / clientArray[key].player.max_Mp);
						if (clientArray[key].player.now_Mp > clientArray[key].player.max_Mp)
							clientArray[key].player.now_Mp = clientArray[key].player.max_Mp;

						clientArray[key].vl_lock.unlock();


						add_timer(key, Object_Type::ePlayer, RECOVERY_MPTIME, OP_MPRecovery);
					}
				}
				delete overlap;
				break;
			case OP_Revival:
				if (clientArray[key].is_connected) {
					clientArray[key].vl_lock.lock();
					clientArray[key].player.isAlive = true;
					clientArray[key].player.now_Hp = clientArray[key].player.max_Hp;
					clientArray[key].player.xPos = eStart_xPos;
					clientArray[key].player.zPos = eStart_zPos;
					clientArray[key].vl_lock.unlock();

					SendAddObjectPacket(key, Object_Type::ePlayer, key);
					SendStatChangeObjectPacket(key, Object_Type::ePlayer, key, clientArray[key].player.level, clientArray[key].player.exp, clientArray[key].player.now_Hp, clientArray[key].player.now_Mp);
					add_timer(key, Object_Type::ePlayer, RECOVERY_HPTIME, OP_HPRecovery);
				}
				break;
			default:
				cout << "Unknown Event on Worker_Thread" << endl;
				while (true);
				break;
			}
		}
	}
}

int main() {
	thread* pAcceptThread;
	thread* pTimerThread;
	vector<thread*> vpThread;

	Initialize_Server();

	for (int i = 0; i < NUM_OF_THREAD; ++i)
		vpThread.push_back(new thread{ Worker_Thread });

	pAcceptThread = new thread{ Accept_Thread };
	pTimerThread = new thread{ Timer_Thread };

	while (g_isShutDown == false) {
		Sleep(1000);
	}

	delete g_Database;

	for (thread* pThread : vpThread) {
		pThread->join();
		delete pThread;
	}

	pAcceptThread->join();
	delete pAcceptThread;

	pTimerThread->join();
	delete pTimerThread;

	DeleteCriticalSection(&g_CriticalSection);

	WSACleanup();
}
