#pragma once
#include "Character.h"

class Monster : public Character
{
	
public :
	Monster();
	virtual ~Monster();

public:
	int type;

	int startXPos, startZPos;
	int attackRange;

	bool isTarget;
	int targetPlayerID;
	bool isAttack;
public:
	void InitData();
	bool Damaged_InPos(int xPos, int zPos);
	bool IsInViewRange(int player_xPos, int player_zPos);
	bool IsInAttackRange(int player_xPos, int player_zPos);
};