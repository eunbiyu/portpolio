#pragma once
#include <Windows.h>


#define MAX_BUFF_SIZE   4000
#define MAX_PACKET_SIZE  255
#define MAX_ID_SIZE		20

#define BOARD_WIDTH   300
#define BOARD_HEIGHT  300
//#define BOARD_WIDTH   20
//#define BOARD_HEIGHT  20
//
#define VIEW_RADIUS				7
#define MONSTER_VIEW_RADIUS		10

#define MAX_USER 100
#define NUM_OF_MONSTER	10000
#define NUM_OF_ROCK		10000
//#define NUM_OF_MONSTER	10
//#define NUM_OF_ROCK		1

#define MY_SERVER_PORT  4000
#define MAX_STR_SIZE	100


// ------------ Protocol -------------- //
#define CS_LOGIN			1
#define CS_MOVE				2
#define CS_ATTACK			3
#define CS_CHAT				4
#define CS_LOGOUT			5

#define SC_LOGIN_OK			1
#define SC_LOGIN_FAIL		2
#define SC_POSITION_INFO	3
#define SC_CHAT				4
#define SC_STAT_CHANGE		5
#define SC_REMOVE_OBJECT	6
#define SC_ADD_OBJECT		7
#define SC_MAPDATA			8

enum Direction_Type {eUp, eDown, eLeft, eRight};
enum Object_Type { ePlayer = 1, eMonster_Peace, eMonster_War};

#pragma pack (push, 1)
// ------------------------ Client -> Server -------------------------- //
struct cs_packet_Login {
	BYTE size;
	BYTE type;

	WCHAR loginID[MAX_ID_SIZE];
};

struct cs_packet_Move {
	BYTE size;
	BYTE type;
	BYTE dir;
};

struct cs_packet_Attack {
	BYTE size;
	BYTE type;
};

struct cs_packet_chat 
{
	BYTE size;
	BYTE type;
	WCHAR message[MAX_STR_SIZE];
};

// ------------------------ Server -> Client -------------------------- //
struct sc_packet_Login_OK {
	BYTE size; // 패킷사이즈
	BYTE type; // 패킷타입 

	WORD controlID;
	WORD xPos;
	WORD zPos;
	WORD level;
	DWORD exp; //경험치
	WORD hp;
	WORD mp;	//마력. 스킬쓸때 필요한 에너지
};

struct sc_packet_Login_FAIL {
	BYTE size;
	BYTE type;
};

struct sc_packet_Position_Info {
	BYTE size;
	BYTE type;

	BYTE objectType; //오브젝트타입. 플레이어냐 적이냐 ... 
	BYTE viewDir;
	WORD id;
	WORD x;
	WORD y;
};

struct sc_packet_Stat_Change {	//레벨오르거나 경험치오르거나 hp, mp 깎이거나 이런거하면 패킷보내줌 
	BYTE size;
	BYTE type;

	BYTE objectType;
	WORD id;
	WORD level;
	DWORD exp;
	WORD hp;
	WORD mp;
};

struct sc_packet_Add_Object { // 접속시  시야에넣어준다 
	BYTE size;
	BYTE type;

	BYTE objectType;
	BYTE viewDir;
	WORD id;
	WORD x;
	WORD y;
	WORD level;
	DWORD exp;
	WORD hp;
	WORD mp;
};

struct sc_packet_Remove_Object { // 시야에서 사라지는것
	BYTE size;
	BYTE type;

	BYTE objectType;
	WORD id;
};

struct sc_packet_chat { //채팅창. 몬스터 때리거나 하면 출력됨 
	BYTE size;
	BYTE type;

	BYTE objectType;
	WORD id;
	WCHAR message[MAX_STR_SIZE];
};

struct sc_packet_MapData { //맵에 장애물어디있는지 .. 
	BYTE size;
	BYTE type;

	WORD x;
	WORD y;
};

#pragma pack (pop)
