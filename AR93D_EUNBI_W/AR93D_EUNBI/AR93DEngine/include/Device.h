#pragma once

#include "Engine.h"

AR3D_BEGIN

class DLL CDevice
{
private:
	ID3D11Device*			m_pDevice;
	ID3D11DeviceContext*	m_pContext;
	IDXGISwapChain*			m_pSwapChain;
	ID3D11RenderTargetView*	m_pRTView;
	ID3D11Texture2D*		m_pDepthBuffer;
	ID3D11DepthStencilView*	m_pDepthView;
	float		m_fClearColor[4];
	unordered_map<string, ID3D11DeviceContext*>	m_mapDeferredContext;

public:
	ID3D11Device* GetDevice()	const;
	ID3D11DeviceContext* GetContext()	const;
	IDXGISwapChain* GetSwapChain()	const;

public:
	void SetClearColor(float fColor[4]);
	bool CreateDeferredContext(const string& strKey);
	ID3D11DeviceContext* FindContext(const string& strKey);

public:
	bool Init(HWND hWnd, unsigned int iWidth, unsigned int iHeight,
		bool bWindowMode);
	void Clear();
	void Present();

private:
	// 2D �� ������ Ÿ��
	ID2D1RenderTarget*	m_pD2DTarget;
	ID2D1Factory*		m_pD2DFactory;

public:
	ID2D1Factory* GetD2DFactory()	const;
	ID2D1RenderTarget* GetD2DRenderTarget()	const;

	DECLARE_SINGLE(CDevice)
};

AR3D_END
