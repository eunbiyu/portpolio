#include "GameObject.h"
#include "../Component/Transform.h"
#include "../Core/CollisionManager.h"

AR3D_USING

unordered_map<string, CGameObject*> CGameObject::m_mapPrototype;

CGameObject::CGameObject() :
	m_pScene(NULL),
	m_pLayer(NULL),
	m_pTransform(NULL),
	m_pParent(NULL),
	m_bPicked(false)
{
	SetTypeName("CGameObject");
	SetTypeID<CGameObject>();

}

CGameObject::CGameObject(const CGameObject & obj)
{
	*this = obj;

	m_pTransform = obj.m_pTransform->Clone();

	m_ComList.clear();

	list<CComponent*>::const_iterator	iter;
	list<CComponent*>::const_iterator	iterEnd = obj.m_ComList.end();

	for (iter = obj.m_ComList.begin(); iter != iterEnd; ++iter)
	{
		CComponent*	pCom = (*iter)->Clone();
		AddComponent(pCom);
		SAFE_RELEASE(pCom);
	}

	list<CGameObject*>::const_iterator	iter1;
	list<CGameObject*>::const_iterator	iter1End = obj.m_ChildList.end();

	m_ChildList.clear();
	for (iter1 = obj.m_ChildList.begin(); iter1 != iter1End; ++iter1)
	{
		CGameObject*	pObj = (*iter1)->Clone();
		AddChild(pObj);
		SAFE_RELEASE(pObj);
	}

	m_FindComList.clear();
}

CGameObject::~CGameObject()
{
	Safe_Release_VecList(m_ChildList);
	Safe_Release_VecList(m_FindComList);
	SAFE_RELEASE(m_pTransform);
	Safe_Release_VecList(m_ComList);
}

CGameObject * CGameObject::Create(const string & strTag, bool bPrototype)
{
	CGameObject*	pObj = new CGameObject;

	if (!pObj->Init())
	{
		SAFE_RELEASE(pObj);
		return NULL;
	}

	pObj->SetTag(strTag);

	if (bPrototype)
	{
		pObj->AddRef();
		m_mapPrototype.insert(make_pair(strTag, pObj));
	}

	return pObj;
}

CGameObject * CGameObject::CreateClone(const string & strKey)
{
	unordered_map<string, CGameObject*>::iterator	iter = m_mapPrototype.find(strKey);

	if (iter == m_mapPrototype.end())
		return NULL;

	CGameObject*	pObj = iter->second->Clone();

	return pObj;
}

void CGameObject::DestroyPrototype()
{
	Safe_Release_Map(m_mapPrototype);
}

CTransform * CGameObject::GetTransform()
{
	m_pTransform->AddRef();
	return m_pTransform;
}

CGameObject * CGameObject::GetParent()
{
	if (m_pParent)
		m_pParent->AddRef();
	return m_pParent;
}

CTransform * CGameObject::GetParentTransform()
{
	return m_pTransform->GetParentTransform();
}

void CGameObject::SetParent(CGameObject * pParent)
{
	m_pParent = pParent;
	CTransform*	pTr = pParent->GetTransform();
	m_pTransform->SetParentTransform(pTr);
	SAFE_RELEASE(pTr);
}

void CGameObject::SetScene(CScene * pScene)
{
	m_pScene = pScene;

	m_pTransform->SetScene(pScene);

	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		(*iter)->SetScene(pScene);
	}

	list<CGameObject*>::iterator	iter1;
	list<CGameObject*>::iterator	iter1End = m_ChildList.end();

	for (iter1 = m_ChildList.begin(); iter1 != iter1End; ++iter1)
	{
		(*iter1)->SetScene(pScene);
	}
}

void CGameObject::SetLayer(CLayer * pLayer)
{
	m_pLayer = pLayer;

	m_pTransform->SetLayer(pLayer);

	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		(*iter)->SetLayer(pLayer);
	}

	list<CGameObject*>::iterator	iter1;
	list<CGameObject*>::iterator	iter1End = m_ChildList.end();

	for (iter1 = m_ChildList.begin(); iter1 != iter1End; ++iter1)
	{
		(*iter1)->SetLayer(pLayer);
	}
}

void CGameObject::ChildTransformActiveUpdate()
{
	list<CGameObject*>::iterator	iter;
	list<CGameObject*>::iterator	iterEnd = m_ChildList.end();

	for (iter = m_ChildList.begin(); iter != iterEnd; ++iter)
	{
		CTransform*	pTransform = (*iter)->GetTransform();
		pTransform->ActiveUpdate();
		SAFE_RELEASE(pTransform);
	}
}

void CGameObject::AddChild(CGameObject * pChild)
{
	pChild->AddRef();
	pChild->SetParent(this);
	pChild->SetScene(m_pScene);
	pChild->SetLayer(m_pLayer);
	CTransform*	pTransform = pChild->GetTransform();
	pTransform->ComputeWorldMatrix();
	SAFE_RELEASE(pTransform);
	m_ChildList.push_back(pChild);
}

list<CGameObject*>* CGameObject::GetChildList()
{
	return &m_ChildList;
}

void CGameObject::AddComponent(CComponent * pCom)
{
	pCom->AddRef();
	pCom->SetScene(m_pScene);
	pCom->SetLayer(m_pLayer);
	pCom->SetGameObject(this);
	pCom->SetTransform(m_pTransform);

	m_ComList.push_back(pCom);
}

bool CGameObject::Init()
{
	m_pTransform = new CTransform;

	if (!m_pTransform->Init())
	{
		SAFE_RELEASE(m_pTransform);
		return false;
	}

	m_pTransform->SetGameObject(this);

	return true;
}

CComponent * CGameObject::FindComponentFromTag(const string & strTag)
{
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetTag() == strTag)
		{
			(*iter)->AddRef();
			return *iter;
		}
	}

	return NULL;
}

CComponent * CGameObject::FindComponentFromTypeName(const string & strTypeName)
{
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetTypeName() == strTypeName)
		{
			(*iter)->AddRef();
			return *iter;
		}
	}

	return NULL;
}

CComponent * CGameObject::FindComponentFromType(COMPONENT_TYPE eType)
{
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetComponentType() == eType)
		{
			(*iter)->AddRef();
			return *iter;
		}
	}

	return NULL;
}

bool CGameObject::CheckComponentFromTag(const string & strTag)
{
	list<class CComponent*>::iterator	iter;
	list<class CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetTag() == strTag)
		{
			return true;
		}
	}

	return false;
}

bool CGameObject::CheckComponentFromTypeName(const string & strTypeName)
{
	list<class CComponent*>::iterator	iter;
	list<class CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetTypeName() == strTypeName)
		{
			return true;
		}
	}

	return false;
}

bool CGameObject::CheckComponentFromType(COMPONENT_TYPE eType)
{
	list<class CComponent*>::iterator	iter;
	list<class CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetComponentType() == eType)
		{
			return true;
		}
	}

	return false;
}

list<CComponent*>* CGameObject::FindComponentsFromTag(const string & strTag)
{
	Safe_Release_VecList(m_FindComList);
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetTag() == strTag)
		{
			(*iter)->AddRef();
			m_FindComList.push_back(*iter);
		}
	}

	return &m_FindComList;
}

list<CComponent*>* CGameObject::FindComponentsFromTypeName(const string & strTypeName)
{
	Safe_Release_VecList(m_FindComList);
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetTypeName() == strTypeName)
		{
			(*iter)->AddRef();
			m_FindComList.push_back(*iter);
		}
	}

	return &m_FindComList;
}

list<CComponent*>* CGameObject::FindComponentsFromType(COMPONENT_TYPE eType)
{
	Safe_Release_VecList(m_FindComList);
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetComponentType() == eType)
		{
			(*iter)->AddRef();
			m_FindComList.push_back(*iter);
		}
	}

	return &m_FindComList;
}

void CGameObject::Input(float fTime)
{
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		(*iter)->Input(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_ComList.erase(iter);
		}

		else
			++iter;
	}

	// Child
	list<CGameObject*>::iterator	iter1;
	list<CGameObject*>::iterator	iter1End = m_ChildList.end();

	for (iter1 = m_ChildList.begin(); iter1 != iter1End;)
	{
		if (!(*iter1)->GetEnable())
		{
			++iter1;
			continue;
		}

		(*iter1)->Input(fTime);

		if (!(*iter1)->GetAlive())
		{
			SAFE_RELEASE((*iter1));
			iter1 = m_ChildList.erase(iter1);
		}

		else
			++iter1;
	}
}

void CGameObject::Update(float fTime)
{
	m_pTransform->Update(fTime);

	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		(*iter)->Update(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_ComList.erase(iter);
		}

		else
			++iter;
	}

	// Child
	list<CGameObject*>::iterator	iter1;
	list<CGameObject*>::iterator	iter1End = m_ChildList.end();

	for (iter1 = m_ChildList.begin(); iter1 != iter1End;)
	{
		if (!(*iter1)->GetEnable())
		{
			++iter1;
			continue;
		}

		(*iter1)->Update(fTime);

		if (!(*iter1)->GetAlive())
		{
			SAFE_RELEASE((*iter1));
			iter1 = m_ChildList.erase(iter1);
		}

		else
			++iter1;
	}
}

void CGameObject::LateUpdate(float fTime)
{
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		(*iter)->LateUpdate(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_ComList.erase(iter);
		}

		else
			++iter;
	}

	m_pTransform->LateUpdate(fTime);

	// Child
	list<CGameObject*>::iterator	iter1;
	list<CGameObject*>::iterator	iter1End = m_ChildList.end();

	for (iter1 = m_ChildList.begin(); iter1 != iter1End;)
	{
		if (!(*iter1)->GetEnable())
		{
			++iter1;
			continue;
		}

		(*iter1)->LateUpdate(fTime);

		if (!(*iter1)->GetAlive())
		{
			SAFE_RELEASE((*iter1));
			iter1 = m_ChildList.erase(iter1);
		}

		else
			++iter1;
	}
}

void CGameObject::Collision(float fTime)
{
	// 충돌관리자에 추가한다.
	GET_SINGLE(CCollisionManager)->AddObject(this);

	/*list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd;)
	{
	if (!(*iter)->GetEnable())
	{
	++iter;
	continue;
	}

	(*iter)->Collision(fTime);

	if (!(*iter)->GetAlive())
	{
	SAFE_RELEASE((*iter));
	iter = m_ComList.erase(iter);
	}

	else
	++iter;
	}*/

	// Child
	list<CGameObject*>::iterator	iter1;
	list<CGameObject*>::iterator	iter1End = m_ChildList.end();

	for (iter1 = m_ChildList.begin(); iter1 != iter1End;)
	{
		if (!(*iter1)->GetEnable())
		{
			++iter1;
			continue;
		}

		(*iter1)->Collision(fTime);

		if (!(*iter1)->GetAlive())
		{
			SAFE_RELEASE((*iter1));
			iter1 = m_ChildList.erase(iter1);
		}

		else
			++iter1;
	}
}

void CGameObject::Render(float fTime)
{
	m_pTransform->Render(fTime);

	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		(*iter)->Render(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_ComList.erase(iter);
		}

		else
			++iter;
	}

	// Child
	list<CGameObject*>::iterator	iter1;
	list<CGameObject*>::iterator	iter1End = m_ChildList.end();

	for (iter1 = m_ChildList.begin(); iter1 != iter1End;)
	{
		if (!(*iter1)->GetEnable())
		{
			++iter1;
			continue;
		}

		(*iter1)->Render(fTime);

		if (!(*iter1)->GetAlive())
		{
			SAFE_RELEASE((*iter1));
			iter1 = m_ChildList.erase(iter1);
		}

		else
			++iter1;
	}
}

CGameObject * CGameObject::Clone()
{
	return new CGameObject(*this);
}

void CGameObject::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		(*iter)->OnCollisionEnter(pSrc, pDest, fTime);
	}
}

void CGameObject::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		(*iter)->OnCollision(pSrc, pDest, fTime);
	}
}

void CGameObject::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		(*iter)->OnCollisionLeave(pSrc, pDest, fTime);
	}
}


bool CGameObject::GetPicked()
{
	return m_bPicked;
}

void CGameObject::SetPicked(bool bpicked)
{
	m_bPicked = bpicked;
}

void CGameObject::EraseComponentFromTag(const string & strTypeName)
{
	list<CComponent*>::iterator	iter;
	list<CComponent*>::iterator	iterEnd = m_ComList.end();

	for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetTag() == strTypeName)
		{
			//m_ComList.erase((iter);
		}
	}
}
