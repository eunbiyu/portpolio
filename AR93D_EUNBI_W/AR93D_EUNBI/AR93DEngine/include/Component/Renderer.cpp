#include "Renderer.h"
#include "../Rendering/Shader.h"
#include "../Rendering/ShaderManager.h"
#include "../Resources/Mesh.h"
#include "../Resources/ResourcesManager.h"
#include "../Device.h"
#include "../Core.h"
#include "../Scene/Scene.h"
#include "../GameObject/GameObject.h"
#include "Camera.h"
#include "Transform.h"
#include "../Rendering/RenderState.h"
#include "../Rendering/RenderManager.h"
#include "Material.h"
#include "Light.h"
#include "Effect.h"
#include "Animation2D.h"
#include "Animation3D.h"
#include "../Resources/Texture.h"

AR3D_USING

CRenderer::CRenderer() :
	m_pMesh(NULL),
	m_pShader(NULL),
	m_pInputLayout(NULL),
	m_pBoneTex(NULL),
	m_bBlend(false),
	m_bOutLine(false),
	m_eRenderFlag(RF_CARTOON_ONLY)
{
	SetTypeName("CRenderer");
	SetTypeID<CRenderer>();
	SetTag("Renderer");
	m_eComType = CT_RENDERER;
	for (int i = 0; i < RST_END; ++i)
	{
		m_pRenderState[i] = NULL;
	}
	m_vOutlineColor = DxVector3(0.f, 0.f, 0.f);

	AddConstantBuffer("Rendering", 3, sizeof(RENDERINGCBUFFER), CUT_PIXEL);
}

CRenderer::CRenderer(const CRenderer & renderer) :
	CComponent(renderer)
{
	*this = renderer;

	if (m_pMesh)
		m_pMesh->AddRef();

	if (m_pShader)
		m_pShader->AddRef();

	m_pBoneTex = NULL;

	//CheckAnimation();

	for (int i = 0; i < RST_END; ++i)
	{
		if (m_pRenderState[i])
			m_pRenderState[i]->AddRef();
	}

	m_vecMaterial.clear();

	for (size_t i = 0; i < renderer.m_vecMaterial.size(); ++i)
	{
		vector<CMaterial*>	vecMtrl;
		m_vecMaterial.push_back(vecMtrl);
		for (int j = 0; j < renderer.m_vecMaterial[i].size(); ++j)
		{
			CMaterial*	pMaterial = renderer.m_vecMaterial[i][j]->Clone();

			m_vecMaterial[i].push_back(pMaterial);
		}
	}

	m_mapCBuffer.clear();

	unordered_map<string, PRENDERERCBUFFER>::const_iterator	iter;
	unordered_map<string, PRENDERERCBUFFER>::const_iterator	iterEnd = renderer.m_mapCBuffer.end();

	for (iter = renderer.m_mapCBuffer.begin(); iter != iterEnd; ++iter)
	{
		PRENDERERCBUFFER	pBuffer = new RENDERERCBUFFER;

		pBuffer->iRegister = iter->second->iRegister;
		pBuffer->iSize = iter->second->iSize;
		pBuffer->pData = new char[pBuffer->iSize];

		m_mapCBuffer.insert(make_pair(iter->first, pBuffer));
	}
}

CRenderer::~CRenderer()
{
	SAFE_RELEASE(m_pBoneTex);
	for (int i = 0; i < RST_END; ++i)
	{
		SAFE_RELEASE(m_pRenderState[i]);
	}
	SAFE_RELEASE(m_pShader);
	SAFE_RELEASE(m_pMesh);

	for (size_t i = 0; i < m_vecMaterial.size(); ++i)
	{
		Safe_Release_VecList(m_vecMaterial[i]);
	}

	unordered_map<string, PRENDERERCBUFFER>::iterator	iter;
	unordered_map<string, PRENDERERCBUFFER>::iterator	iterEnd = m_mapCBuffer.end();

	for (iter = m_mapCBuffer.begin(); iter != iterEnd; ++iter)
	{
		SAFE_DELETE_ARRAY(iter->second->pData);
	}
	Safe_Delete_Map(m_mapCBuffer);
}

void CRenderer::SetBoneTexture(CTexture * pBoneTex)
{
	if (pBoneTex)
		pBoneTex->AddRef();
	SAFE_RELEASE(m_pBoneTex);
	m_pBoneTex = pBoneTex;
}

bool CRenderer::IsBoneTexture() const
{
	return m_pBoneTex != NULL;
}

CMaterial * CRenderer::GetMaterial(int iContainer, int iSubset)
{
	if (iContainer >= m_vecMaterial.size())
		return NULL;

	else if (iSubset >= m_vecMaterial[iContainer].size())
		return NULL;

	m_vecMaterial[iContainer][iSubset]->AddRef();

	return m_vecMaterial[iContainer][iSubset];
}

CMesh * CRenderer::GetMesh() const
{
	m_pMesh->AddRef();
	return m_pMesh;
}

bool CRenderer::BlendEnable() const
{
	return m_bBlend;
}

void CRenderer::SetOutlineColor(DxVector3 OutlineColor)
{
	m_vOutlineColor = OutlineColor;
}

bool CRenderer::Init()
{
	CMaterial*	pMaterial = CreateMaterial("Linear", "");

	SAFE_RELEASE(pMaterial);
	m_vOutlineColor = DxVector3(0.f, 0.f, 0.f);
	return true;
}

void CRenderer::Input(float fTime)
{
}

void CRenderer::Update(float fTime)
{
}

void CRenderer::LateUpdate(float fTime)
{
}

void CRenderer::Collision(float fTime)
{
}

void CRenderer::Render(float fTime)
{
	UpdateTransform();

 	RENDERINGCBUFFER	tBuffer = {};
	tBuffer.iCartoonRenderFlag = m_eRenderFlag;
	tBuffer.vEdgeColor = m_vOutlineColor;
	UpdateCBuffer("Rendering", &tBuffer);

	for (int i = 0; i < RST_END; ++i)
	{
		if (m_pRenderState[i])
			m_pRenderState[i]->SetState();
	}

	if (m_pBoneTex)
		m_pBoneTex->SetTexture(3, CUT_VERTEX);

	else
	{
		/*CheckAnimation();

		if (m_pBoneTex)
		m_pBoneTex->SetTexture(3, CUT_VERTEX);*/

		//if (!m_pBoneTex)
		CONTEXT->VSSetShaderResources(3, 0, NULL);
	}

	CONTEXT->IASetInputLayout(m_pInputLayout);
	m_pShader->SetShader();

	// 조명 정보를 얻어온다.
	/*const list<CGameObject*>* pLightList = m_pScene->GetLightList();

	if (!pLightList->empty())
	{
	list<CGameObject*>::const_iterator	iter = pLightList->begin();

	CLight*	pLight = (CLight*)(*iter)->FindComponentFromType(CT_LIGHT);

	pLight->SetLight();

	SAFE_RELEASE(pLight);
	}*/

	// 상수버퍼들을 셰이더에 업데이트한다.
	unordered_map<string, PRENDERERCBUFFER>::iterator	iter;
	unordered_map<string, PRENDERERCBUFFER>::iterator	iterEnd = m_mapCBuffer.end();

	for (iter = m_mapCBuffer.begin(); iter != iterEnd; ++iter)
	{
		GET_SINGLE(CShaderManager)->UpdateConstantBuffer(iter->first,
			iter->second->pData, iter->second->iShaderType);
	}

	for (UINT i = 0; i < m_vecMaterial.size(); ++i)
	{
		for (UINT j = 0; j < m_vecMaterial[i].size(); ++j)
		{
			// 재질 설정
			m_vecMaterial[i][j]->SetMaterial();

			m_pMesh->Render(i, j);
		}
	}

	for (int i = 0; i < RST_END; ++i)
	{
		if (m_pRenderState[i])
			m_pRenderState[i]->ResetState();
	}
}

CRenderer * CRenderer::Clone()
{
	return new CRenderer(*this);
}

void CRenderer::UpdateTransform()
{
	TRANSFORMCBUFFER	tTransform = {};

	CCamera*	pCamera = m_pScene->GetMainCamera();

	tTransform.matWorld = m_pTransform->GetLocalMatrix().mat * m_pTransform->GetWorldMatrix().mat;
	tTransform.matView = pCamera->GetViewMatrix();
	tTransform.matProj = pCamera->GetProjMatrix();
	tTransform.matWV = tTransform.matWorld * tTransform.matView;
	tTransform.matWVP = tTransform.matWV * tTransform.matProj;
	tTransform.matVP = tTransform.matView * tTransform.matProj;
	tTransform.matInvProj = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matProj),
		tTransform.matProj);
	tTransform.matInvView = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matView),
		tTransform.matView);
	tTransform.matInvVP = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matVP),
		tTransform.matVP);
	tTransform.vPivot = m_pTransform->GetPivot();
	tTransform.vMeshSize = m_pMesh->GetMeshSize();
	tTransform.vMeshMin = m_pMesh->GetMeshMin();
	tTransform.vMeshMax = m_pMesh->GetMeshMax();

	SAFE_RELEASE(pCamera);

	tTransform.matWorld = XMMatrixTranspose(tTransform.matWorld);
	tTransform.matView = XMMatrixTranspose(tTransform.matView);
	tTransform.matProj = XMMatrixTranspose(tTransform.matProj);
	tTransform.matWV = XMMatrixTranspose(tTransform.matWV);
	tTransform.matWVP = XMMatrixTranspose(tTransform.matWVP);
	tTransform.matVP = XMMatrixTranspose(tTransform.matVP);
	tTransform.matInvProj = XMMatrixTranspose(tTransform.matInvProj);
	tTransform.matInvView = XMMatrixTranspose(tTransform.matInvView);
	tTransform.matInvVP = XMMatrixTranspose(tTransform.matInvVP);

	GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Transform", &tTransform, CUT_VERTEX | CUT_GEOMETRY | CUT_PIXEL);
}

void CRenderer::SetMesh(const string & strKey)
{
	SAFE_RELEASE(m_pMesh);
	m_pMesh = GET_SINGLE(CResourcesManager)->FindMesh(strKey);

	//CheckAnimation();
}

void CRenderer::SetMesh(const string & strKey, void * pVertices, unsigned int iVtxCount,
	unsigned int iVtxSize, D3D11_USAGE eVtxUsage, D3D11_PRIMITIVE_TOPOLOGY ePrimitive,
	void * pIndices, unsigned int iIdxCount, unsigned int iIdxSize,
	D3D11_USAGE eIdxUsage, DXGI_FORMAT eFormat)
{
	SAFE_RELEASE(m_pMesh);
	m_pMesh = GET_SINGLE(CResourcesManager)->CreateMesh(strKey, pVertices, iVtxCount,
		iVtxSize, eVtxUsage, ePrimitive, pIndices, iIdxCount, iIdxSize,
		eIdxUsage, eFormat);

	//CheckAnimation();
}

void CRenderer::SetMesh(const string & strKey, const TCHAR * pFileName, FBX_LOAD_TYPE eLoadType,
	const string & strPathKey)
{
	SAFE_RELEASE(m_pMesh);
	m_pMesh = GET_SINGLE(CResourcesManager)->LoadMesh(strKey,
		pFileName, eLoadType, strPathKey);

	for (size_t i = 0; i < m_vecMaterial.size(); ++i)
	{
		Safe_Release_VecList(m_vecMaterial[i]);
	}

	m_vecMaterial.clear();

	for (UINT i = 0; i < m_pMesh->GetContainerCount(); ++i)
	{
		if (m_vecMaterial.size() == i)
			AddContainerMaterial();

		for (UINT j = 0; j < m_pMesh->GetSubsetCount(i); ++j)
		{
			CMaterial*	pMaterial = m_pMesh->CloneMaterial(i, j);

			m_vecMaterial[i].push_back(pMaterial);
		}
	}

	//CheckAnimation();
}

void CRenderer::SetMeshFromFullPath(const string & strKey,
	const TCHAR * pFullPath, FBX_LOAD_TYPE eLoadType)
{
	SAFE_RELEASE(m_pMesh);
	m_pMesh = GET_SINGLE(CResourcesManager)->LoadMeshFromFullPath(strKey,
		pFullPath, eLoadType);

	for (size_t i = 0; i < m_vecMaterial.size(); ++i)
	{
		Safe_Release_VecList(m_vecMaterial[i]);
	}

	m_vecMaterial.clear();

	for (UINT i = 0; i < m_pMesh->GetContainerCount(); ++i)
	{
		if (m_vecMaterial.size() == i)
			AddContainerMaterial();

		for (UINT j = 0; j < m_pMesh->GetSubsetCount(i); ++j)
		{
			CMaterial*	pMaterial = m_pMesh->CloneMaterial(i, j);

			m_vecMaterial[i].push_back(pMaterial);
		}
	}

	//CheckAnimation();
}

void CRenderer::SetMesh(CMesh * pMesh)
{
	SAFE_RELEASE(m_pMesh);
	m_pMesh = pMesh;
	m_pMesh->AddRef();

	//CheckAnimation();
}

void CRenderer::SetShader(const string & strKey)
{
	SAFE_RELEASE(m_pShader);
	m_pShader = GET_SINGLE(CShaderManager)->FindShader(strKey);
}

void CRenderer::SetShader(const string & strKey, TCHAR * pFileName, char* pEntry[ST_END],
	bool bStreamOut, const string & strPathKey)
{
	SAFE_RELEASE(m_pShader);
	m_pShader = GET_SINGLE(CShaderManager)->LoadShader(strKey, pFileName, pEntry, bStreamOut, strPathKey);
}

void CRenderer::SetInputLayout(const string & strKey)
{
	m_pInputLayout = GET_SINGLE(CShaderManager)->FindInputLayout(strKey);
}

void CRenderer::SetRenderState(const string & strKey)
{
	CRenderState*	pState = GET_SINGLE(CRenderManager)->FindRenderState(strKey);

	if (!pState)
		return;

	m_pRenderState[pState->GetType()] = pState;

	if (pState->GetType() == RST_BLEND)
		m_bBlend = true;
}

CMaterial * CRenderer::CreateMaterial(const string & strSamplerKey,
	const string & strTextureKey, int iSmpRegister, int iTexRegister,
	TCHAR * pFileName, const string & strPathKey,
	int iContainer)
{
	if (m_vecMaterial.empty())
	{
		vector<CMaterial*>	vecMtrl;
		m_vecMaterial.push_back(vecMtrl);
	}

	else if (m_vecMaterial.size() <= iContainer)
		return NULL;

	CMaterial*	pMaterial = new CMaterial;

	if (!pMaterial->Init())
	{
		SAFE_RELEASE(pMaterial);
		return NULL;
	}

	pMaterial->SetDiffuseTexture(strSamplerKey, strTextureKey,
		pFileName, strPathKey);
	pMaterial->SetDiffuseRegister(iSmpRegister, iTexRegister);

	pMaterial->AddRef();
	m_vecMaterial[iContainer].push_back(pMaterial);

	return pMaterial;
}

void CRenderer::AddMaterial(CMaterial * pMaterial, int iContainer)
{
	if (m_vecMaterial.empty())
	{
		vector<CMaterial*>	vecMtrl;
		m_vecMaterial.push_back(vecMtrl);
	}

	else if (m_vecMaterial.size() <= iContainer)
		return;

	pMaterial->AddRef();
	m_vecMaterial[iContainer].push_back(pMaterial);
}

void CRenderer::AddContainerMaterial()
{
	vector<CMaterial*>	vecMtrl;
	m_vecMaterial.push_back(vecMtrl);
}

void CRenderer::AddConstantBuffer(const string & strKey, int iRegister,
	int iSize, int iShaderType)
{
	if (FindConstantBuffer(strKey))
		return;

	PRENDERERCBUFFER	pBuffer = new RENDERERCBUFFER;

	pBuffer->iRegister = iRegister;
	pBuffer->iSize = iSize;
	pBuffer->iShaderType = iShaderType;
	pBuffer->pData = new char[iSize];

	m_mapCBuffer.insert(make_pair(strKey, pBuffer));
}

bool CRenderer::UpdateCBuffer(const string & strKey, void * pData)
{
	PRENDERERCBUFFER	pBuffer = FindConstantBuffer(strKey);

	if (!pBuffer)
		return false;

	memcpy(pBuffer->pData, pData, pBuffer->iSize);

	return true;
}

PRENDERERCBUFFER CRenderer::FindConstantBuffer(const string & strKey)
{
	unordered_map<string, PRENDERERCBUFFER>::iterator	iter = m_mapCBuffer.find(strKey);

	if (iter == m_mapCBuffer.end())
		return NULL;

	return iter->second;
}

void CRenderer::CheckAnimation()
{
	SAFE_RELEASE(m_pBoneTex);

	CAnimation3D*	pAnimation = NULL;

	if (CheckComponentFromType(CT_ANIMATION3D))
	{
		pAnimation = (CAnimation3D*)FindComponentFromType(CT_ANIMATION3D);
	}
	else
	{
		pAnimation = m_pMesh->CloneAnimation();

		if (!pAnimation)
			return;

		m_pGameObject->AddComponent((CComponent*)pAnimation);
	}

	m_pBoneTex = pAnimation->GetBoneTexture();

	SAFE_RELEASE(pAnimation);
}

void CRenderer::SetRenderFlag(RENDERING_FLAG eFlag)
{
	m_eRenderFlag = eFlag;
}
