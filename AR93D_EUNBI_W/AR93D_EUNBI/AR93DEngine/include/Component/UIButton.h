#pragma once
#include "UI.h"

AR3D_BEGIN

enum BUTTON_STATE
{
	BS_NONE,
	BS_MOUSEON,
	BS_CLICK
};

class DLL CUIButton :
	public CUI
{
protected:
	friend class CGameObject;

protected:
	CUIButton();
	CUIButton(const CUIButton& button);
	~CUIButton();

protected:
	BUTTON_STATE	m_eBtnState;	
	DxVector4		m_vNormalCol;
	DxVector4		m_vMouseOnCol;
	DxVector4		m_vClickCol;
	function<void(class CGameObject*, float)> m_Callback;
	bool			m_bCallback;
	bool			m_bCallbackonce;
	bool			m_bCallbackNone;

public:
	void SetNormalColor(float r, float g, float b, float a);
	void SetMouseOnColor(float r, float g, float b, float a);
	void SetClickColor(float r, float g, float b, float a);
	template <typename T>
	void SetCallback(T* pObj, void(T::*pFunc)(class CGameObject*, float))
	{
		m_bCallback = true;
		m_Callback = bind(pFunc, pObj, placeholders::_1, placeholders::_2);
	}
	void OnOffCallBack(bool bCallback);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CUIButton* Clone();
	virtual void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
};

AR3D_END
