#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CLampLight :
	public CComponent
{
private:
	friend class CGameObject;

public:
	CLampLight();
	CLampLight(const CLampLight& particle);
	~CLampLight();

private:
	DxVector3 m_LightPos;
	ID3D11Buffer*	m_pDrawVB;
	class CShader*	m_pLightShader;
	ID3D11InputLayout*	m_pInputLayout;
	class CRenderState*	m_pDepthDisable;
	class CRenderState*	m_pDepthWriteDisable;
	bool			m_bLight;
	float			m_fLightRange;

public:
	bool GetLampLight()	const;

public:
	void SetLightRange(float fRange);
	void SetInputLayout(const string& strKey);
	void SetLampLight(bool bParticleLight);
	void SetLampLightPos(const DxVector3& Lightpos);
	void SetLampLightInfo(DxVector3 pos);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CLampLight* Clone();

public:
	void RenderLight(float fTime);
};

AR3D_END


