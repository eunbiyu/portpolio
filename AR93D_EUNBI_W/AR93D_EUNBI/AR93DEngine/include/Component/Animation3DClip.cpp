#include "Animation3DClip.h"

AR3D_USING

CAnimation3DClip::CAnimation3DClip()
{
}

CAnimation3DClip::CAnimation3DClip(const CAnimation3DClip & clip)
{
	*this = clip;
	m_iRefCount = 1;

	m_tInfo.vecCallback.clear();

	m_vecKeyFrame.clear();

	for (size_t i = 0; i < clip.m_vecKeyFrame.size(); ++i)
	{
		PBONEKEYFRAME	pBoneKeyFrame = new BONEKEYFRAME;

		pBoneKeyFrame->iBoneIndex = clip.m_vecKeyFrame[i]->iBoneIndex;

		m_vecKeyFrame.push_back(pBoneKeyFrame);
		for (size_t j = 0; j < clip.m_vecKeyFrame[i]->vecKeyFrame.size(); ++j)
		{
			PKEYFRAME	pFrame = new KEYFRAME;
			*pFrame = *clip.m_vecKeyFrame[i]->vecKeyFrame[j];
			pBoneKeyFrame->vecKeyFrame.push_back(pFrame);
		}
	}
}

CAnimation3DClip::~CAnimation3DClip()
{
	for (size_t i = 0; i < m_vecKeyFrame.size(); ++i)
	{
		Safe_Delete_VecList(m_vecKeyFrame[i]->vecKeyFrame);
	}

	Safe_Delete_VecList(m_vecKeyFrame);

	Safe_Delete_VecList(m_tInfo.vecCallback);
}

bool CAnimation3DClip::IsEmptyKeyFrame(int iBoneIndex) const
{
	return m_vecKeyFrame[iBoneIndex]->vecKeyFrame.empty();
}

PKEYFRAME CAnimation3DClip::GetKeyFrame(int iBoneIndex, int iFrameIndex) const
{
	return m_vecKeyFrame[iBoneIndex]->vecKeyFrame[iFrameIndex];
}

ANIMATION3DCLIP CAnimation3DClip::GetInfo() const
{
	return m_tInfo;
}

void CAnimation3DClip::AddCallback(int iFrame, void(*pFunc)(float))
{
	PANIMATIONCALLBACK	pCallback = new ANIMATIONCALLBACK;

	pCallback->iAnimationProgress = iFrame;
	pCallback->fAnimationProgress = (iFrame - m_tInfo.iStartFrame) /
		(float)m_tInfo.iEndFrame;
	pCallback->func = bind(pFunc, placeholders::_1);
	pCallback->bCall = false;

	m_tInfo.vecCallback.push_back(pCallback);
}

void CAnimation3DClip::AddCallback(float fProgress, void(*pFunc)(float))
{
	PANIMATIONCALLBACK	pCallback = new ANIMATIONCALLBACK;

	pCallback->iAnimationProgress = (fProgress * m_tInfo.fTimeLength + m_tInfo.fStartTime) *
		m_iAnimationLimitFrame;
	pCallback->fAnimationProgress = fProgress;
	pCallback->func = bind(pFunc, placeholders::_1);
	pCallback->bCall = false;

	m_tInfo.vecCallback.push_back(pCallback);
}

void CAnimation3DClip::SetClipInfo(const string & strName,
	ANIMATION_OPTION eOption, int iAnimationLimitFrame, int iStartFrame,
	int iEndFrame, float fStartTime, float fEndTime)
{
	m_tInfo.eOption = eOption;
	m_tInfo.strName = strName;
	m_tInfo.iStartFrame = iStartFrame;
	m_tInfo.iEndFrame = iEndFrame;
	m_tInfo.iFrameLength = iEndFrame - iStartFrame;
	m_tInfo.fStartTime = fStartTime;
	m_tInfo.fEndTime = fEndTime;
	m_tInfo.fTimeLength = fEndTime - fStartTime;
}

void CAnimation3DClip::SetClipInfo(ANIMATION_OPTION eOption,
	PFBXANIMATIONCLIP pClip)
{
	m_tInfo.eOption = eOption;
	m_tInfo.strName = pClip->strName;
	m_tInfo.iStartFrame = pClip->tStart.GetFrameCount(pClip->eTimeMode);
	m_tInfo.iEndFrame = pClip->tEnd.GetFrameCount(pClip->eTimeMode);
	m_tInfo.iFrameLength = m_tInfo.iEndFrame - m_tInfo.iStartFrame;

	m_tInfo.fStartTime = pClip->tStart.GetSecondDouble();
	m_tInfo.fEndTime = pClip->tEnd.GetSecondDouble();
	m_tInfo.fTimeLength = m_tInfo.fEndTime - m_tInfo.fStartTime;

	for (size_t i = 0; i < pClip->vecBoneKeyFrame.size(); ++i)
	{
		PBONEKEYFRAME	pBoneKeyFrame = new BONEKEYFRAME;

		pBoneKeyFrame->iBoneIndex = pClip->vecBoneKeyFrame[i].iBoneIndex;

		m_vecKeyFrame.push_back(pBoneKeyFrame);

		pBoneKeyFrame->vecKeyFrame.reserve(pClip->vecBoneKeyFrame[i].vecKeyFrame.size());

		for (size_t j = 0; j < pClip->vecBoneKeyFrame[i].vecKeyFrame.size(); ++j)
		{
			PKEYFRAME	pKeyFrame = new KEYFRAME;

			pKeyFrame->dTime = pClip->vecBoneKeyFrame[i].vecKeyFrame[j].dTime;

			FbxAMatrix	mat = pClip->vecBoneKeyFrame[i].vecKeyFrame[j].matTransform;

			FbxVector4	vPos, vScale;
			FbxQuaternion	qRot;

			vPos = mat.GetT();
			vScale = mat.GetS();
			qRot = mat.GetQ();

			pKeyFrame->vScale = DxVector3(vScale.mData[0], vScale.mData[1],
				vScale.mData[2]);
			pKeyFrame->vPos = DxVector3(vPos.mData[0], vPos.mData[1],
				vPos.mData[2]);
			pKeyFrame->vRot = DxVector4(qRot.mData[0], qRot.mData[1],
				qRot.mData[2], qRot.mData[3]);

			pBoneKeyFrame->vecKeyFrame.push_back(pKeyFrame);
		}
	}
}

void CAnimation3DClip::Save(FILE * pFile)
{
	fwrite(&m_tInfo.eOption, 4, 1, pFile);

	int	iLength = m_tInfo.strName.length();
	fwrite(&iLength, 4, 1, pFile);
	fwrite(m_tInfo.strName.c_str(), 1, iLength, pFile);

	fwrite(&m_tInfo.fStartTime, 4, 1, pFile);
	fwrite(&m_tInfo.fEndTime, 4, 1, pFile);
	fwrite(&m_tInfo.fTimeLength, 4, 1, pFile);
	fwrite(&m_tInfo.iStartFrame, 4, 1, pFile);
	fwrite(&m_tInfo.iEndFrame, 4, 1, pFile);
	fwrite(&m_tInfo.iFrameLength, 4, 1, pFile);

	size_t iCount = m_vecKeyFrame.size();
	fwrite(&iCount, 4, 1, pFile);

	for (size_t i = 0; i < m_vecKeyFrame.size(); ++i)
	{
		fwrite(&m_vecKeyFrame[i]->iBoneIndex, 4, 1, pFile);

		iCount = m_vecKeyFrame[i]->vecKeyFrame.size();
		fwrite(&iCount, 4, 1, pFile);

		for (int j = 0; j < iCount; ++j)
		{
			fwrite(m_vecKeyFrame[i]->vecKeyFrame[j], sizeof(KEYFRAME), 1,
				pFile);
		}
	}
}

void CAnimation3DClip::Load(FILE * pFile)
{
	fread(&m_tInfo.eOption, 4, 1, pFile);

	int	iLength;
	fread(&iLength, 4, 1, pFile);
	char	strName[256] = {};
	fread(strName, 1, iLength, pFile);
	m_tInfo.strName = strName;

	fread(&m_tInfo.fStartTime, 4, 1, pFile);
	fread(&m_tInfo.fEndTime, 4, 1, pFile);
	fread(&m_tInfo.fTimeLength, 4, 1, pFile);
	fread(&m_tInfo.iStartFrame, 4, 1, pFile);
	fread(&m_tInfo.iEndFrame, 4, 1, pFile);
	fread(&m_tInfo.iFrameLength, 4, 1, pFile);

	size_t iCount = 0;
	fread(&iCount, 4, 1, pFile);

	for (size_t i = 0; i < m_vecKeyFrame.size(); ++i)
	{
		Safe_Delete_VecList(m_vecKeyFrame[i]->vecKeyFrame);
	}
	Safe_Delete_VecList(m_vecKeyFrame);

	for (size_t i = 0; i < iCount; ++i)
	{
		PBONEKEYFRAME	pBoneKeyFrame = new BONEKEYFRAME;

		m_vecKeyFrame.push_back(pBoneKeyFrame);

		fread(&pBoneKeyFrame->iBoneIndex, 4, 1, pFile);

		int iKeyCount = 0;
		fread(&iKeyCount, 4, 1, pFile);

		pBoneKeyFrame->vecKeyFrame.reserve(iKeyCount);

		for (int j = 0; j < iKeyCount; ++j)
		{
			PKEYFRAME	pFrame = new KEYFRAME;

			fread(pFrame, sizeof(KEYFRAME), 1,
				pFile);

			pBoneKeyFrame->vecKeyFrame.push_back(pFrame);
		}
	}
}

bool CAnimation3DClip::Init()
{
	return true;
}

int CAnimation3DClip::Update(float fTime)
{
	return 0;
}

int CAnimation3DClip::LateUpdate(float fTime)
{
	return 0;
}

CAnimation3DClip * CAnimation3DClip::Clone()
{
	return new CAnimation3DClip(*this);
}
