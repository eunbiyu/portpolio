#include "Transform.h"
#include "../GameObject/GameObject.h"
#include "../Core/Debug.h"

AR3D_USING

CTransform::CTransform()
{
	SetTypeName("CTransform");
	SetTypeID<CTransform>();
	SetTag("Transform");
	m_eComType = CT_TRANSFORM;
	m_bUpdate = true;
	m_bMove = false;
	m_pParent = NULL;
}

CTransform::CTransform(const CTransform & transform) :
	CComponent(transform)
{
	*this = transform;
	m_bUpdate = true;
	m_bMove = false;
	m_pParent = NULL;

	m_matLocalScale = new MATRIX;
	m_matLocalRotX = new MATRIX;
	m_matLocalRotY = new MATRIX;
	m_matLocalRotZ = new MATRIX;
	m_matLocalRot = new MATRIX;
	m_matLocalPos = new MATRIX;
	m_matLocal = new MATRIX;

	m_matWorldScale = new MATRIX;
	m_matWorldRotX = new MATRIX;
	m_matWorldRotY = new MATRIX;
	m_matWorldRotZ = new MATRIX;
	m_matWorldRot = new MATRIX;
	m_matWorldPos = new MATRIX;
	m_matParent = new MATRIX;
	m_matWorld = new MATRIX;

	*m_matLocalScale = *transform.m_matLocalScale;
	*m_matLocalRotX = *transform.m_matLocalRotX;
	*m_matLocalRotY = *transform.m_matLocalRotY;
	*m_matLocalRotZ = *transform.m_matLocalRotZ;
	*m_matLocalRot = *transform.m_matLocalRot;
	*m_matLocalPos = *transform.m_matLocalPos;
	*m_matLocal = *transform.m_matLocal;
	*m_matWorldScale = *transform.m_matWorldScale;
	*m_matWorldRotX = *transform.m_matWorldRotX;
	*m_matWorldRotY = *transform.m_matWorldRotY;
	*m_matWorldRotZ = *transform.m_matWorldRotZ;
	*m_matWorldRot = *transform.m_matWorldRot;
	*m_matWorldPos = *transform.m_matWorldPos;
	//*m_matParent = *transform.m_matParent;
	*m_matWorld = *transform.m_matWorld;
}


CTransform::~CTransform()
{
	SAFE_DELETE(m_matLocalScale);
	SAFE_DELETE(m_matLocalRotX);
	SAFE_DELETE(m_matLocalRotY);
	SAFE_DELETE(m_matLocalRotZ);
	SAFE_DELETE(m_matLocalRot);
	SAFE_DELETE(m_matLocalPos);
	SAFE_DELETE(m_matLocal);
	SAFE_DELETE(m_matWorldScale);
	SAFE_DELETE(m_matWorldRotX);
	SAFE_DELETE(m_matWorldRotY);
	SAFE_DELETE(m_matWorldRotZ);
	SAFE_DELETE(m_matWorldRot);
	SAFE_DELETE(m_matWorldPos);
	SAFE_DELETE(m_matParent);
	SAFE_DELETE(m_matWorld);
}

MATRIX CTransform::GetLocalMatrix() const
{
	return *m_matLocal;
}

MATRIX CTransform::GetWorldRotMatrix() const
{
	return *m_matWorldRot;
}

MATRIX CTransform::GetWorldMatrix() const
{
	return *m_matWorld;
}

MATRIX CTransform::GetParentMatrix() const
{
	if (m_pParent)
		*m_matParent = m_pParent->GetWorldMatrix();
	return *m_matParent;
}

MATRIX CTransform::GetParentRotMatrix() const
{
	MATRIX	matParent;
	if (m_pParent)
		matParent = m_pParent->GetParentRotMatrix();

	return *m_matWorldRot * matParent;
}

void CTransform::Forward(float fSpeed, float fTime)
{
	m_vWorldPos += m_vWorldAxis[AXIS_Z] * fSpeed * fTime;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	m_bMove = true;

	ActiveUpdate();
}

void CTransform::Forward(float fSpeed)
{
	m_vWorldPos += m_vWorldAxis[AXIS_Z] * fSpeed;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	m_bMove = true;

	ActiveUpdate();
}

void CTransform::Right(float fSpeed, float fTime)
{
	m_vWorldPos += m_vWorldAxis[AXIS_X] * fSpeed * fTime;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	m_bMove = true;

	ActiveUpdate();
}

void CTransform::Right(float fSpeed)
{
	m_vWorldPos += m_vWorldAxis[AXIS_X] * fSpeed;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	m_bMove = true;

	ActiveUpdate();
}

void CTransform::Up(float fSpeed, float fTime)
{
	m_vWorldPos += m_vWorldAxis[AXIS_Y] * fSpeed * fTime;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	m_bMove = true;

	ActiveUpdate();
}

void CTransform::Up(float fSpeed)
{
	m_vWorldPos += m_vWorldAxis[AXIS_Y] * fSpeed;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	m_bMove = true;

	ActiveUpdate();
}

void CTransform::Move(const DxVector3 & vDir, float fSpeed)
{
	m_vWorldPos += vDir * fSpeed;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	m_bMove = true;

	ActiveUpdate();
}

void CTransform::Move(const DxVector3 & vDir, float fSpeed, float fTime)
{
	m_vWorldPos += vDir * fSpeed * fTime;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	m_bMove = true;

	ActiveUpdate();
}

void CTransform::Move(const DxVector3 & vMove)
{
	m_vWorldPos += vMove;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	m_bMove = true;

	ActiveUpdate();
}

void CTransform::Rotate(const DxVector3 & vRot, bool bLocal)
{
	if (bLocal)
	{
		SetLocalRot(m_vLocalRot + vRot);
	}

	else
	{
		SetWorldRot(m_vWorldRot + vRot);
	}
}

void CTransform::Rotate(float x, float y, float z, bool bLocal)
{
	if (bLocal)
	{
		SetLocalRot(m_vLocalRot + DxVector3(x, y, z));
	}

	else
	{
		SetWorldRot(m_vWorldRot + DxVector3(x, y, z));
	}
}

void CTransform::RotateX(float x, bool bLocal)
{
	if (bLocal)
	{
		SetLocalRotX(m_vLocalRot.x + x);
	}

	else
	{
		SetWorldRotX(m_vWorldRot.x + x);
	}
}

void CTransform::RotateY(float y, bool bLocal)
{
	if (bLocal)
	{
		SetLocalRotY(m_vLocalRot.y + y);
	}

	else
	{
		SetWorldRotY(m_vWorldRot.y + y);
	}
}

void CTransform::RotateZ(float z, bool bLocal)
{
	if (bLocal)
	{
		SetLocalRotZ(m_vLocalRot.z + z);
	}

	else
	{
		SetWorldRotZ(m_vWorldRot.z + z);
	}
}

void CTransform::Rotate(const DxVector3 & vRot, float fTime, bool bLocal)
{
	if (bLocal)
	{
		SetLocalRot(m_vLocalRot + vRot * fTime);
	}

	else
	{
		SetWorldRot(m_vWorldRot + vRot * fTime);
	}
}

void CTransform::Rotate(float x, float y, float z, float fTime, bool bLocal)
{
	if (bLocal)
	{
		SetLocalRot(m_vLocalRot + DxVector3(x, y, z) * fTime);
	}

	else
	{
		SetWorldRot(m_vWorldRot + DxVector3(x, y, z) * fTime);
	}
}

void CTransform::RotateX(float x, float fTime, bool bLocal)
{
	if (bLocal)
	{
		SetLocalRotX(m_vLocalRot.x + x * fTime);
	}

	else
	{
		SetWorldRotX(m_vWorldRot.x + x * fTime);
	}
}

void CTransform::RotateY(float y, float fTime, bool bLocal)
{
	if (bLocal)
	{
		SetLocalRotY(m_vLocalRot.y + y * fTime);
	}

	else
	{
		SetWorldRotY(m_vWorldRot.y + y * fTime);
	}
}

void CTransform::RotateZ(float z, float fTime, bool bLocal)
{
	if (bLocal)
	{
		SetLocalRotZ(m_vLocalRot.z + z * fTime);
	}

	else
	{
		SetWorldRotZ(m_vWorldRot.z + z * fTime);
	}
}

void CTransform::LookAt(CTransform * pTransform, AXIS axis)
{
	DxVector3	vPos = pTransform->GetWorldPos();

	DxVector3	vView = vPos - m_vWorldPos;
	if (vView == DxVector3(0.f, 0.f, 0.f))
	{
		return;
	}

	vView = vView.Normalize();

	DxVector3	vAxis = WORLDAXIS[axis];

	DxVector3	vRotAxis = vAxis.Cross(vView);
	float	fAngle = vAxis.GetAngle(vView);

	if (fAngle != 0.f)
	{
		*m_matWorldRot = XMMatrixRotationAxis(vRotAxis.Convert(), fAngle);

		ComputeWorldAxis();

		ActiveUpdate();
	}
}

void CTransform::LookAt(CGameObject * pObject, AXIS axis)
{
	CTransform * pTransform = pObject->GetTransform();

	DxVector3	vPos = pTransform->GetWorldPos();

	DxVector3	vView = vPos - m_vWorldPos;

	if (vView == DxVector3(0.f, 0.f, 0.f))
	{
		return;
	}

	vView = vView.Normalize();

	DxVector3	vAxis = WORLDAXIS[axis];

	DxVector3	vRotAxis = vAxis.Cross(vView);

	float	fAngle = vAxis.GetAngle(vView);

	if (fAngle != 0.f)
	{

		*m_matWorldRot = XMMatrixRotationAxis(vRotAxis.Convert(), fAngle);

		ComputeWorldAxis();

		ActiveUpdate();
	}

	SAFE_RELEASE(pTransform);
}

void CTransform::CopyTransform(CTransform * pTransform)
{
	m_vLocalScale = pTransform->m_vLocalScale;
	m_vLocalRot = pTransform->m_vLocalRot;
	m_vLocalPos = pTransform->m_vLocalPos;

	m_vWorldScale = pTransform->m_vWorldScale;
	SetWorldRot(pTransform->GetWorldHierarchyRot());
	m_vWorldPos = pTransform->m_vWorldPos;

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vLocalAxis[i] = pTransform->m_vLocalAxis[i];
		//m_vWorldAxis[i] = pTransform->m_vWorldAxis[i];
	}

	*m_matLocalScale = *pTransform->m_matLocalScale;
	*m_matLocalRotX = *pTransform->m_matLocalRotX;
	*m_matLocalRotY = *pTransform->m_matLocalRotY;
	*m_matLocalRotZ = *pTransform->m_matLocalRotZ;
	*m_matLocalRot = *pTransform->m_matLocalRot;
	*m_matLocalPos = *pTransform->m_matLocalPos;
	*m_matLocal = *pTransform->m_matLocal;

	*m_matWorldScale = *pTransform->m_matWorldScale;
	//*m_matWorldRotX = *pTransform->m_matWorldRotX;
	//*m_matWorldRotY = *pTransform->m_matWorldRotY;
	//*m_matWorldRotZ = *pTransform->m_matWorldRotZ;
	//*m_matWorldRot = *pTransform->m_matWorldRot;
	*m_matWorldPos = *pTransform->m_matWorldPos;
	//*m_matParent = *pTransform->m_matParent;
	*m_matWorld = *pTransform->m_matWorld;

	// 최종 월드상에서의 위치를 구한다.
	m_vWorldPos = m_vWorldPos.TransformCoord(*pTransform->m_matParent);
}

void CTransform::ComputeWorldMatrix()
{
	if (m_pParent)
		*m_matParent = m_pParent->GetWorldMatrix();

	*m_matLocal = *m_matLocalScale * *m_matLocalRot * *m_matLocalPos;
	*m_matWorld = *m_matWorldScale * *m_matWorldRot * *m_matWorldPos * *m_matParent;
}

bool CTransform::Init()
{
	m_matLocalScale = new MATRIX;
	m_matLocalRotX = new MATRIX;
	m_matLocalRotY = new MATRIX;
	m_matLocalRotZ = new MATRIX;
	m_matLocalRot = new MATRIX;
	m_matLocalPos = new MATRIX;
	m_matLocal = new MATRIX;

	m_matWorldScale = new MATRIX;
	m_matWorldRotX = new MATRIX;
	m_matWorldRotY = new MATRIX;
	m_matWorldRotZ = new MATRIX;
	m_matWorldRot = new MATRIX;
	m_matWorldPos = new MATRIX;
	m_matParent = new MATRIX;
	m_matWorld = new MATRIX;

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vLocalAxis[i] = WORLDAXIS[i];
		m_vWorldAxis[i] = WORLDAXIS[i];
	}

	m_vLocalScale = DxVector3(1.f, 1.f, 1.f);
	m_vWorldScale = DxVector3(1.f, 1.f, 1.f);
	m_vPivot = DxVector3(0.5f, 0.5f, 0.5f);

	return true;
}

void CTransform::Input(float fTime)
{
}

void CTransform::Update(float fTime)
{
	if (!m_bUpdate)
		return;

	if (m_pParent)
		*m_matParent = m_pParent->GetWorldMatrix();

	*m_matLocal = *m_matLocalScale * *m_matLocalRot * *m_matLocalPos;
	*m_matWorld = *m_matWorldScale * *m_matWorldRot * *m_matWorldPos * *m_matParent;

	m_bUpdate = false;
}

void CTransform::LateUpdate(float fTime)
{
	if (!m_bUpdate)
		return;

	if (m_pParent)
		*m_matParent = m_pParent->GetWorldMatrix();

	*m_matLocal = *m_matLocalScale * *m_matLocalRot * *m_matLocalPos;
	*m_matWorld = *m_matWorldScale * *m_matWorldRot * *m_matWorldPos * *m_matParent;

	if (m_strTag == "ChildTr")
	{
		char	strDebug[256] = {};
		sprintf_s(strDebug, "41 : %f 42 : %f\n", m_matWorld->_41, m_matWorld->_42);
		CDebug::OutputConsole(strDebug);
	}

	m_bUpdate = false;
}

void CTransform::Collision(float fTime)
{
}

void CTransform::Render(float fTime)
{
	m_bMove = false;
}

CTransform * CTransform::Clone()
{
	return new CTransform(*this);
}

bool CTransform::GetMove() const
{
	return m_bMove;
}

DxVector3 CTransform::GetLocalScale() const
{
	return m_vLocalScale;
}

DxVector3 CTransform::GetLocalRot() const
{
	return m_vLocalRot;
}

DxVector3 CTransform::GetLocalPos() const
{
	return m_vLocalPos;
}

DxVector3 CTransform::GetPivot() const
{
	return m_vPivot;
}

DxVector3 CTransform::GetWorldScale() const
{
	return m_vWorldScale;
}

DxVector3 CTransform::GetWorldRot() const
{
	return m_vWorldRot;
}

DxVector3 CTransform::GetWorldPos() const
{
	return m_vWorldPos;
}

DxVector3 CTransform::GetWorldHierarchyRot() const
{
	if (!m_pParent)
		return m_vWorldRot;

	return m_vWorldRot + m_pParent->GetWorldHierarchyRot();
}

CTransform * CTransform::GetParentTransform() const
{
	if (m_pParent)
		m_pParent->AddRef();
	return m_pParent;
}

DxVector3 CTransform::GetWorldAxis(AXIS axis) const
{
	return m_vWorldAxis[axis];
}

void CTransform::SetParentTransform(CTransform * pParent)
{
	m_pParent = pParent;
}

void CTransform::ActiveUpdate()
{
	m_bUpdate = true;

	m_pGameObject->ChildTransformActiveUpdate();
}

void CTransform::ComputeWorldAxis()
{
	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vWorldAxis[i] = WORLDAXIS[i].TransformNormal(GetParentRotMatrix()).Normalize();
	}

	list<CGameObject*>*	pChildList = m_pGameObject->GetChildList();

	list<CGameObject*>::iterator	iter;
	list<CGameObject*>::iterator	iterEnd = pChildList->end();

	for (iter = pChildList->begin(); iter != iterEnd; ++iter)
	{
		CTransform*	pTransform = (*iter)->GetTransform();

		pTransform->ComputeWorldAxis();

		SAFE_RELEASE(pTransform);
	}
}

void CTransform::SetLocalScale(const Vector3 & v)
{
	m_vLocalScale = v;

	*m_matLocalScale = XMMatrixScaling(m_vLocalScale.x, m_vLocalScale.y,
		m_vLocalScale.z);

	ActiveUpdate();
}

void CTransform::SetLocalScale(const DxVector3 & v)
{
	m_vLocalScale = v;

	*m_matLocalScale = XMMatrixScaling(m_vLocalScale.x, m_vLocalScale.y,
		m_vLocalScale.z);

	ActiveUpdate();
}

void CTransform::SetLocalScale(const XMVECTOR & v)
{
	m_vLocalScale = v;

	*m_matLocalScale = XMMatrixScaling(m_vLocalScale.x, m_vLocalScale.y,
		m_vLocalScale.z);

	ActiveUpdate();
}

void CTransform::SetLocalScale(float x, float y, float z)
{
	m_vLocalScale = Vector3(x, y, z);

	*m_matLocalScale = XMMatrixScaling(m_vLocalScale.x, m_vLocalScale.y,
		m_vLocalScale.z);

	ActiveUpdate();
}

void CTransform::SetLocalScale(float f[3])
{
	m_vLocalScale = f;

	*m_matLocalScale = XMMatrixScaling(m_vLocalScale.x, m_vLocalScale.y,
		m_vLocalScale.z);

	ActiveUpdate();
}

void CTransform::SetLocalScaleX(float x)
{
	m_vLocalScale.x = x;

	*m_matLocalScale = XMMatrixScaling(m_vLocalScale.x, m_vLocalScale.y,
		m_vLocalScale.z);

	ActiveUpdate();
}

void CTransform::SetLocalScaleY(float y)
{
	m_vLocalScale.y = y;

	*m_matLocalScale = XMMatrixScaling(m_vLocalScale.x, m_vLocalScale.y,
		m_vLocalScale.z);

	ActiveUpdate();
}

void CTransform::SetLocalScaleZ(float z)
{
	m_vLocalScale.z = z;

	*m_matLocalScale = XMMatrixScaling(m_vLocalScale.x, m_vLocalScale.y,
		m_vLocalScale.z);

	ActiveUpdate();
}

void CTransform::SetLocalRot(const Vector3 & v)
{
	m_vLocalRot = v;

	*m_matLocalRotX = XMMatrixRotationX(m_vLocalRot.x);
	*m_matLocalRotY = XMMatrixRotationY(m_vLocalRot.y);
	*m_matLocalRotZ = XMMatrixRotationZ(m_vLocalRot.z);

	*m_matLocalRot = *m_matLocalRotX * *m_matLocalRotY * *m_matLocalRotZ;

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vLocalAxis[i] = WORLDAXIS[i].TransformNormal(*m_matLocalRot).Normalize();
	}

	ActiveUpdate();
}

void CTransform::SetLocalRot(const DxVector3 & v)
{
	m_vLocalRot = v;

	*m_matLocalRotX = XMMatrixRotationX(m_vLocalRot.x);
	*m_matLocalRotY = XMMatrixRotationY(m_vLocalRot.y);
	*m_matLocalRotZ = XMMatrixRotationZ(m_vLocalRot.z);

	*m_matLocalRot = *m_matLocalRotX * *m_matLocalRotY * *m_matLocalRotZ;

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vLocalAxis[i] = WORLDAXIS[i].TransformNormal(*m_matLocalRot).Normalize();
	}

	ActiveUpdate();
}

void CTransform::SetLocalRot(const XMVECTOR & v)
{
	m_vLocalRot = v;

	*m_matLocalRotX = XMMatrixRotationX(m_vLocalRot.x);
	*m_matLocalRotY = XMMatrixRotationY(m_vLocalRot.y);
	*m_matLocalRotZ = XMMatrixRotationZ(m_vLocalRot.z);

	*m_matLocalRot = *m_matLocalRotX * *m_matLocalRotY * *m_matLocalRotZ;

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vLocalAxis[i] = WORLDAXIS[i].TransformNormal(*m_matLocalRot).Normalize();
	}

	ActiveUpdate();
}

void CTransform::SetLocalRot(float x, float y, float z)
{
	m_vLocalRot = Vector3(x, y, z);

	*m_matLocalRotX = XMMatrixRotationX(m_vLocalRot.x);
	*m_matLocalRotY = XMMatrixRotationY(m_vLocalRot.y);
	*m_matLocalRotZ = XMMatrixRotationZ(m_vLocalRot.z);

	*m_matLocalRot = *m_matLocalRotX * *m_matLocalRotY * *m_matLocalRotZ;

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vLocalAxis[i] = WORLDAXIS[i].TransformNormal(*m_matLocalRot).Normalize();
	}

	ActiveUpdate();
}

void CTransform::SetLocalRot(float f[3])
{
	m_vLocalRot = f;

	*m_matLocalRotX = XMMatrixRotationX(m_vLocalRot.x);
	*m_matLocalRotY = XMMatrixRotationY(m_vLocalRot.y);
	*m_matLocalRotZ = XMMatrixRotationZ(m_vLocalRot.z);

	*m_matLocalRot = *m_matLocalRotX * *m_matLocalRotY * *m_matLocalRotZ;

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vLocalAxis[i] = WORLDAXIS[i].TransformNormal(*m_matLocalRot).Normalize();
	}

	ActiveUpdate();
}

void CTransform::SetLocalRotX(float x)
{
	m_vLocalRot.x = x;

	*m_matLocalRotX = XMMatrixRotationX(m_vLocalRot.x);

	*m_matLocalRot = *m_matLocalRotX * *m_matLocalRotY * *m_matLocalRotZ;

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vLocalAxis[i] = WORLDAXIS[i].TransformNormal(*m_matLocalRot).Normalize();
	}

	ActiveUpdate();
}

void CTransform::SetLocalRotY(float y)
{
	m_vLocalRot.y = y;

	*m_matLocalRotY = XMMatrixRotationY(m_vLocalRot.y);

	*m_matLocalRot = *m_matLocalRotX * *m_matLocalRotY * *m_matLocalRotZ;

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vLocalAxis[i] = WORLDAXIS[i].TransformNormal(*m_matLocalRot).Normalize();
	}

	ActiveUpdate();
}

void CTransform::SetLocalRotZ(float z)
{
	m_vLocalRot.z = z;

	*m_matLocalRotZ = XMMatrixRotationZ(m_vLocalRot.z);

	*m_matLocalRot = *m_matLocalRotX * *m_matLocalRotY * *m_matLocalRotZ;

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vLocalAxis[i] = WORLDAXIS[i].TransformNormal(*m_matLocalRot).Normalize();
	}

	ActiveUpdate();
}

void CTransform::SetLocalPos(const Vector3 & v)
{
	m_vLocalPos = v;

	*m_matLocalPos = XMMatrixTranslation(m_vLocalPos.x, m_vLocalPos.y,
		m_vLocalPos.z);

	ActiveUpdate();
}

void CTransform::SetLocalPos(const DxVector3 & v)
{
	m_vLocalPos = v;

	*m_matLocalPos = XMMatrixTranslation(m_vLocalPos.x, m_vLocalPos.y,
		m_vLocalPos.z);

	ActiveUpdate();
}

void CTransform::SetLocalPos(const XMVECTOR & v)
{
	m_vLocalPos = v;

	*m_matLocalPos = XMMatrixTranslation(m_vLocalPos.x, m_vLocalPos.y,
		m_vLocalPos.z);

	ActiveUpdate();
}

void CTransform::SetLocalPos(float x, float y, float z)
{
	m_vLocalPos = Vector3(x, y, z);

	*m_matLocalPos = XMMatrixTranslation(m_vLocalPos.x, m_vLocalPos.y,
		m_vLocalPos.z);

	ActiveUpdate();
}

void CTransform::SetLocalPos(float f[3])
{
	m_vLocalPos = f;

	*m_matLocalPos = XMMatrixTranslation(m_vLocalPos.x, m_vLocalPos.y,
		m_vLocalPos.z);

	ActiveUpdate();
}

void CTransform::SetLocalPosX(float x)
{
	m_vLocalPos.x = x;

	*m_matLocalPos = XMMatrixTranslation(m_vLocalPos.x, m_vLocalPos.y,
		m_vLocalPos.z);

	ActiveUpdate();
}

void CTransform::SetLocalPosY(float y)
{
	m_vLocalPos.y = y;

	*m_matLocalPos = XMMatrixTranslation(m_vLocalPos.x, m_vLocalPos.y,
		m_vLocalPos.z);

	ActiveUpdate();
}

void CTransform::SetLocalPosZ(float z)
{
	m_vLocalPos.z = z;

	*m_matLocalPos = XMMatrixTranslation(m_vLocalPos.x, m_vLocalPos.y,
		m_vLocalPos.z);

	ActiveUpdate();
}

void CTransform::SetPivot(const DxVector3 & vPivot)
{
	m_vPivot = vPivot;
}

void CTransform::SetPivot(float x, float y, float z)
{
	m_vPivot = DxVector3(x, y, z);
}

void CTransform::SetWorldScale(const Vector3 & v)
{
	m_vWorldScale = v;

	*m_matWorldScale = XMMatrixScaling(m_vWorldScale.x, m_vWorldScale.y,
		m_vWorldScale.z);

	ActiveUpdate();
}

void CTransform::SetWorldScale(const DxVector3 & v)
{
	m_vWorldScale = v;

	*m_matWorldScale = XMMatrixScaling(m_vWorldScale.x, m_vWorldScale.y,
		m_vWorldScale.z);

	ActiveUpdate();
}

void CTransform::SetWorldScale(const XMVECTOR & v)
{
	m_vWorldScale = v;

	*m_matWorldScale = XMMatrixScaling(m_vWorldScale.x, m_vWorldScale.y,
		m_vWorldScale.z);

	ActiveUpdate();
}

void CTransform::SetWorldScale(float x, float y, float z)
{
	m_vWorldScale = Vector3(x, y, z);

	*m_matWorldScale = XMMatrixScaling(m_vWorldScale.x, m_vWorldScale.y,
		m_vWorldScale.z);

	ActiveUpdate();
}

void CTransform::SetWorldScale(float f[3])
{
	m_vWorldScale = f;

	*m_matWorldScale = XMMatrixScaling(m_vWorldScale.x, m_vWorldScale.y,
		m_vWorldScale.z);

	ActiveUpdate();
}

void CTransform::SetWorldScaleX(float x)
{
	m_vWorldScale.x = x;

	*m_matWorldScale = XMMatrixScaling(m_vWorldScale.x, m_vWorldScale.y,
		m_vWorldScale.z);

	ActiveUpdate();
}

void CTransform::SetWorldScaleY(float y)
{
	m_vWorldScale.y = y;

	*m_matWorldScale = XMMatrixScaling(m_vWorldScale.x, m_vWorldScale.y,
		m_vWorldScale.z);

	ActiveUpdate();
}

void CTransform::SetWorldScaleZ(float z)
{
	m_vWorldScale.z = z;

	*m_matWorldScale = XMMatrixScaling(m_vWorldScale.x, m_vWorldScale.y,
		m_vWorldScale.z);

	ActiveUpdate();
}

void CTransform::SetWorldRot(const Vector3 & v)
{
	m_vWorldRot = v;

	*m_matWorldRotX = XMMatrixRotationX(m_vWorldRot.x);
	*m_matWorldRotY = XMMatrixRotationY(m_vWorldRot.y);
	*m_matWorldRotZ = XMMatrixRotationZ(m_vWorldRot.z);

	*m_matWorldRot = *m_matWorldRotX * *m_matWorldRotY * *m_matWorldRotZ;

	ComputeWorldAxis();

	ActiveUpdate();
}

void CTransform::SetWorldRot(const DxVector3 & v)
{
	m_vWorldRot = v;

	*m_matWorldRotX = XMMatrixRotationX(m_vWorldRot.x);
	*m_matWorldRotY = XMMatrixRotationY(m_vWorldRot.y);
	*m_matWorldRotZ = XMMatrixRotationZ(m_vWorldRot.z);

	*m_matWorldRot = *m_matWorldRotX * *m_matWorldRotY * *m_matWorldRotZ;

	ComputeWorldAxis();

	ActiveUpdate();
}

void CTransform::SetWorldRot(const XMVECTOR & v)
{
	m_vWorldRot = v;

	*m_matWorldRotX = XMMatrixRotationX(m_vWorldRot.x);
	*m_matWorldRotY = XMMatrixRotationY(m_vWorldRot.y);
	*m_matWorldRotZ = XMMatrixRotationZ(m_vWorldRot.z);

	*m_matWorldRot = *m_matWorldRotX * *m_matWorldRotY * *m_matWorldRotZ;

	ComputeWorldAxis();

	ActiveUpdate();
}

void CTransform::SetWorldRot(float x, float y, float z)
{
	m_vWorldRot = Vector3(x, y, z);

	*m_matWorldRotX = XMMatrixRotationX(m_vWorldRot.x);
	*m_matWorldRotY = XMMatrixRotationY(m_vWorldRot.y);
	*m_matWorldRotZ = XMMatrixRotationZ(m_vWorldRot.z);

	*m_matWorldRot = *m_matWorldRotX * *m_matWorldRotY * *m_matWorldRotZ;

	ComputeWorldAxis();

	ActiveUpdate();
}

void CTransform::SetWorldRot(float f[3])
{
	m_vWorldRot = f;

	*m_matWorldRotX = XMMatrixRotationX(m_vWorldRot.x);
	*m_matWorldRotY = XMMatrixRotationY(m_vWorldRot.y);
	*m_matWorldRotZ = XMMatrixRotationZ(m_vWorldRot.z);

	*m_matWorldRot = *m_matWorldRotX * *m_matWorldRotY * *m_matWorldRotZ;

	ComputeWorldAxis();

	ActiveUpdate();
}

void CTransform::SetWorldRotX(float x)
{
	m_vWorldRot.x = x;

	*m_matWorldRotX = XMMatrixRotationX(m_vWorldRot.x);

	*m_matWorldRot = *m_matWorldRotX * *m_matWorldRotY * *m_matWorldRotZ;

	ComputeWorldAxis();

	ActiveUpdate();
}

void CTransform::SetWorldRotY(float y)
{
	m_vWorldRot.y = y;

	*m_matWorldRotY = XMMatrixRotationY(m_vWorldRot.y);

	*m_matWorldRot = *m_matWorldRotX * *m_matWorldRotY * *m_matWorldRotZ;

	ComputeWorldAxis();

	ActiveUpdate();
}

void CTransform::SetWorldRotZ(float z)
{
	m_vWorldRot.z = z;

	*m_matWorldRotZ = XMMatrixRotationZ(m_vWorldRot.z);

	*m_matWorldRot = *m_matWorldRotX * *m_matWorldRotY * *m_matWorldRotZ;

	ComputeWorldAxis();

	ActiveUpdate();
}

void CTransform::SetWorldPos(const Vector3 & v)
{
	m_vWorldPos = v;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	ActiveUpdate();
}

void CTransform::SetWorldPos(const DxVector3 & v)
{
	m_vWorldPos = v;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	ActiveUpdate();
}

void CTransform::SetWorldPos(const XMVECTOR & v)
{
	m_vWorldPos = v;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	ActiveUpdate();
}

void CTransform::SetWorldPos(float x, float y, float z)
{
	m_vWorldPos = Vector3(x, y, z);

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	ActiveUpdate();
}

void CTransform::SetWorldPos(float f[3])
{
	m_vWorldPos = f;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	ActiveUpdate();
}

void CTransform::SetWorldPosX(float x)
{
	m_vWorldPos.x = x;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	ActiveUpdate();
}

void CTransform::SetWorldPosY(float y)
{
	m_vWorldPos.y = y;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	ActiveUpdate();
}

void CTransform::SetWorldPosZ(float z)
{
	m_vWorldPos.z = z;

	*m_matWorldPos = XMMatrixTranslation(m_vWorldPos.x, m_vWorldPos.y,
		m_vWorldPos.z);

	ActiveUpdate();
}

void CTransform::SetLookAt( class CGameObject* TargetObj)
{
	//#3
	// y축 기준으로 
	// 1. 전방 기준과 y축과의 외적을 통해 우향 기준을 구한다.
	// 2. 구한 우향 기준과 전방 기준을 통해 상향 기준을 구한다.
	DxVector3 _vLook = TargetObj->GetTransform()->GetWorldPos();

	m_vWorldAxis[AXIS_MAX];

	//Vec3 vForward = XMVector3Normalize(m_vWorldDir[(int)DIR_TYPE::FORWARD].Convert());
	DxVector3 vUp = DxVector3(0.f, 1.f, 0.f);

	DxVector3 vLookNormal = XMVector3Normalize(TargetObj->GetTransform()->GetWorldAxis(AXIS_Z).Convert());
	//if (TargetObj->GetTag() == "FarmObject")
		vLookNormal *= -1;
	//vLookNormal *= -1;
	// 1. 전방 기준과 y축과의 외적을 통해 우향 기준을 구한다.
	DxVector3 vRight = XMVector3Cross(vUp.Convert(), vLookNormal.Convert());

	if (vRight.x == 0 && vRight.y == 0 && vRight.z == 0)
	{
		vRight = DxVector3(1.f, 0.f, 0.f);
	}
	vRight = XMVector3Normalize(vRight.Convert());

	// 2. 구한 우향 기준과 전방 기준을 통해 상향 기준을 구한다.
	vUp = XMVector3Cross(vLookNormal.Convert(), vRight.Convert());

	if (vUp.x == 0 && vUp.y == 0 && vUp.z == 0)
		return;

	vUp = XMVector3Normalize(vUp.Convert());

	DxVector3 NewRot = DecomposeRatationAxis(vRight, vUp, vLookNormal);

	SetWorldRotX(NewRot.x);
	SetWorldRotY(NewRot.y);
	SetWorldRotZ(NewRot.z);

	ComputeWorldAxis();

	ActiveUpdate();
}

bool CTransform::closeEnough(const float & a, const float & b, const float & epsilon)
{
	return (epsilon > std::abs(a - b));
}

DxVector3 CTransform::DecomposeRatationAxis(const DxVector3 & _vRight, const DxVector3 & _vUp, const DxVector3 & _vForward)
{
	// _mat 을 분해 후 다시 행렬 만들기   
	DxVector3 vMat[3];
	vMat[0] = _vRight;
	vMat[1] = _vUp;
	vMat[2] = _vForward;
	DxVector3 vNewRot;
	if (closeEnough(vMat[0].z, -1.0f))
	{
		float x = 0; //gimbal lock, value of x doesn't matter
		float y = XM_PI / 2.f;
		float z = (x + atan2(vMat[1].x, vMat[2].x))*-1.f;      // 여기 수정할 필요 있을수도있어 17.10.11_P(수정함)
		vNewRot = DxVector3{ x, y, z };
	}
	else if (closeEnough(vMat[0].z, 1.0f))
	{
		float x = 0;
		float y = -XM_PI / 2.f;
		float z = -x + atan2(-vMat[1].x, -vMat[2].x);         // 여기 수정할 필요 있을수도있어  17.10.11_P
		vNewRot = DxVector3{ x, y, z };
	}
	else { //two solutions exist
		float y1 = -asin(vMat[0].z);
		float y2 = XM_PI - y1;

		float x1 = atan2f(vMat[1].z / cos(y1), vMat[2].z / cos(y1));
		float x2 = atan2f(vMat[1].z / cos(y2), vMat[2].z / cos(y2));

		float z1 = atan2f(vMat[0].y / cos(y1), vMat[0].x / cos(y1));
		float z2 = atan2f(vMat[0].y / cos(y2), vMat[0].x / cos(y2));

		//choose one solution to return
		//for example the "shortest" rotation
		if ((std::abs(x1) + std::abs(y1) + std::abs(z1)) <= (std::abs(x2) + std::abs(y2) + std::abs(z2)))
		{
			vNewRot = DxVector3{ x1, y1, z1 };
		}
		else {
			vNewRot = DxVector3{ x2, y2, z2 };
		}
	}
	return vNewRot;
}