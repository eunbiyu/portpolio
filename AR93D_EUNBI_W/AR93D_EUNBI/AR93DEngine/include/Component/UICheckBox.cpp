#include "UICheckBox.h"
#include "UIRadioButton.h"
#include "../GameObject/GameObject.h"
#include "Renderer2D.h"
#include "Transform.h"
#include "ColliderSphere.h"
#include "Material.h"
#include "../Scene/Layer.h"
#include "../Scene/Scene.h"
#include "ColliderRect.h"

AR3D_USING

CUICheckBox::CUICheckBox()
{
	SetTag("UICheckBox");
	SetTypeName("CUICheckBox");
	SetTypeID<CUICheckBox>();
	m_eUIType = UT_CHECKBOX;
	m_eBtnState = BS_NONE;
	
	m_bCallback = false;
	m_bClickOn = false;

	m_Check = false;
}


CUICheckBox::CUICheckBox(const CUICheckBox & button)
	:CUIButton(button)
{
}

CUICheckBox::~CUICheckBox()
{
}

void CUICheckBox::SetClick(bool bClick)
{
	CRenderer2D*	pRenderer = m_pGameObject->FindComponentFromTypeID<CRenderer2D>();

	CMaterial*	pMaterial = pRenderer->GetMaterial(0, 0);

	m_bClickOn = bClick;
	if (m_bClickOn)
	{
		pMaterial->SetDiffuseTexture("Linear", "CheckBoxOn", L"UIBox/CheckBox_On.png");
	}
	else
	{
		pMaterial->SetDiffuseTexture("Linear", "CheckBoxOff", L"UIBox/CheckBox_Off.png");
		
	}
	SAFE_RELEASE(pMaterial);
	SAFE_RELEASE(pRenderer);
}

void CUICheckBox::LateUpdate(float fTime)
{
	CRenderer2D*	pRenderer = m_pGameObject->FindComponentFromTypeID<CRenderer2D>();

	CMaterial*	pMaterial = pRenderer->GetMaterial(0, 0);

	pMaterial->SetDiffuseColor(m_vClickCol);

	switch (m_eBtnState)
	{
	case BS_NONE:
	case BS_MOUSEON:
		m_Check = false;
		break;

	case BS_CLICK:
		if (m_Check == false) {
			if (m_bClickOn)
				SetClick(false);
			else
				SetClick(true);
			if (m_bCallback)
				m_Callback(m_pGameObject, fTime);
			m_Check = true;
		}
		break;
	}
	SAFE_RELEASE(pMaterial);
	SAFE_RELEASE(pRenderer);
	
}

void CUICheckBox::Render(float fTime)
{
}

CUICheckBox * CUICheckBox::Clone()
{
	return new CUICheckBox(*this);
}
