#include "UIButton.h"
#include "ColliderRect.h"
#include "../GameObject/GameObject.h"
#include "Transform.h"
#include "../Core/Input.h"
#include "Material.h"
#include "Renderer2D.h"

AR3D_USING

CUIButton::CUIButton()
{
	SetTag("Button");
	SetTypeName("CUIButton");
	SetTypeID<CUIButton>();
	m_eUIType = UT_BUTTON;
	m_eBtnState = BS_NONE;

	m_bCallback = false;
	m_bCallbackonce = true;
}

CUIButton::CUIButton(const CUIButton & button) :
	CUI(button)
{
	m_eBtnState = button.m_eBtnState;
	m_vNormalCol = button.m_vNormalCol;
	m_vMouseOnCol = button.m_vMouseOnCol;
	m_vClickCol = button.m_vClickCol;

	m_bCallback = false;
}


CUIButton::~CUIButton()
{
}

void CUIButton::SetNormalColor(float r, float g, float b, float a)
{
	m_vNormalCol = DxVector4(r, g, b, a);
}

void CUIButton::SetMouseOnColor(float r, float g, float b, float a)
{
	m_vMouseOnCol = DxVector4(r, g, b, a);
}

void CUIButton::SetClickColor(float r, float g, float b, float a)
{
	m_vClickCol = DxVector4(r, g, b, a);
}

void CUIButton::OnOffCallBack(bool bCallback)
{
	m_bCallbackNone = bCallback;
	m_bCallbackonce = true;
}

bool CUIButton::Init()
{
	SetNormalColor(0.6f, 0.6f, 0.6f, 1.f);
	SetMouseOnColor(1.f, 1.f, 1.f, 1.f);
	SetClickColor(0.3f, 0.3f, 0.3f, 1.f);

	CColliderRect*	pRC = m_pGameObject->AddComponent<CColliderRect>("ColliderRect");

	DxVector3	vPos = m_pTransform->GetWorldPos();
	DxVector3	vScale = m_pTransform->GetWorldScale();
	DxVector3	vPivot = m_pTransform->GetPivot();

	DxVector3 vLeft = vPivot * vScale * -1.f; 

	pRC->SetRectInfo(vLeft.x, vLeft.y, vLeft.x + vScale.x , vLeft.y + vScale.y );

	SAFE_RELEASE(pRC);


	m_bCallbackonce = true;

	return true;
}

void CUIButton::Input(float fTime)
{
}

void CUIButton::Update(float fTime)
{
}

void CUIButton::LateUpdate(float fTime)
{
	CRenderer2D*	pRenderer = m_pGameObject->FindComponentFromTypeID<CRenderer2D>();

	CMaterial*	pMaterial = pRenderer->GetMaterial(0, 0);

	switch (m_eBtnState)
	{
	case BS_NONE:
		pMaterial->SetDiffuseColor(m_vNormalCol);
		m_bCallbackonce = true;
		break;
	case BS_MOUSEON:
		pMaterial->SetDiffuseColor(m_vMouseOnCol);
		break;
	case BS_CLICK:
		pMaterial->SetDiffuseColor(m_vClickCol);
		if (m_bCallback)
		{
			if (m_bCallbackonce)
			{
				m_Callback(m_pGameObject, fTime);
				m_bCallbackonce = false;
			}
		}
		break;
	}

	SAFE_RELEASE(pMaterial);
	SAFE_RELEASE(pRenderer);
}

void CUIButton::Collision(float fTime)
{
}

void CUIButton::Render(float fTime)
{
}

CUIButton * CUIButton::Clone()
{
	return new CUIButton(*this);
}

void CUIButton::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
	if (pDest->GetTag() == "MousePoint")
	{
		if (KEYPRESS("MouseLButton") || KEYPUSH("MouseLButton"))
			m_eBtnState = BS_CLICK;

		else
			m_eBtnState = BS_MOUSEON;
	}
}

void CUIButton::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
	if (pDest->GetTag() == "MousePoint")
	{
		if (KEYPRESS("MouseLButton") || KEYPUSH("MouseLButton"))
			m_eBtnState = BS_CLICK;

		else
			m_eBtnState = BS_MOUSEON;
	}
}

void CUIButton::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
	if (pDest->GetTag() == "MousePoint")
	{
		m_eBtnState = BS_NONE;
	}
}