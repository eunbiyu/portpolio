#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CLight :
	public CComponent
{
protected:
	CLight();
	CLight(const CLight& light);
	~CLight();

protected:
	LIGHTINFO m_tInfo;
	LIGHTCBUFFER m_tCBuffer;
	
public:
	LIGHTINFO GetLightInfo() const;
	void SetLightRange(float fRange);

public:
	void SetLightInfo(const LIGHTINFO& tInfo);
	void SetLightInfo(const DxVector4& vDif, const DxVector4& vAmb, const DxVector4& vSpc);
	void SetLightInfo(const DxVector3& vDif, const DxVector3& vAmb, const DxVector3& vSpc);

public:
	virtual bool Init() = 0;
	virtual void Input(float fTime) = 0;
	virtual void Update(float fTime) = 0;
	virtual void LateUpdate(float fTime) = 0;
	virtual void Collision(float fTime) = 0;
	virtual void Render(float fTime) = 0;
	virtual CComponent* Clone() = 0;
	virtual void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);

public:
	virtual void SetLight();

};

AR3D_END