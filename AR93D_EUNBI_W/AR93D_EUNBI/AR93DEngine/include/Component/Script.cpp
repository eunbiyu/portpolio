#include "Script.h"

AR3D_USING

CScript::CScript()
{
}

CScript::CScript(const CScript & script) :
	CComponent(script)
{
}

CScript::~CScript()
{
}

void CScript::Input(float fTime)
{
}

void CScript::Update(float fTime)
{
}

void CScript::LateUpdate(float fTime)
{
}

void CScript::Collision(float fTime)
{
}

void CScript::Render(float fTime)
{
}

CScript * CScript::Clone()
{
	return nullptr;
}

void CScript::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CScript::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CScript::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}
