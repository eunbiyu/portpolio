#include "CameraArm.h"
#include "../Core/Input.h"
#include "Transform.h"
#include "../GameObject/GameObject.h"
#include "Camera.h"
#include "../Core/Input.h"

AR3D_USING

CCameraArm::CCameraArm()
{
	SetTag("CameraArm");
	SetTypeName("CCameraArm");
	SetTypeID<CCameraArm>();
	m_eComType = CT_CAMERAARM;
}


CCameraArm::CCameraArm(const CCameraArm & camera) :
	CComponent(camera)
{
}

CCameraArm::~CCameraArm()
{
}

void CCameraArm::SetZoomDistance(float fMin, float fMax)
{
	m_fMinDist = fMin;
	m_fMaxDist = fMax;
}

void CCameraArm::SetZoomSpeed(float fSpeed)
{
	m_fZoomSpeed = fSpeed;
}

bool CCameraArm::Init()
{
	m_fMinDist = 1.f;
	m_fMaxDist = 5.f;
	m_fZoomSpeed = 10.f;

	return true;
}

void CCameraArm::Input(float fTime)
{
}

void CCameraArm::Update(float fTime)
{
	Zoom(fTime);
	RotationDrag(fTime);
}

void CCameraArm::LateUpdate(float fTime)
{
}

void CCameraArm::Collision(float fTime)
{
}

void CCameraArm::Render(float fTime)
{
}

CCameraArm * CCameraArm::Clone()
{
	return new CCameraArm(*this);
}

void CCameraArm::Zoom(float fTime)
{
	// 카메라에 붙어있는 오브젝트와 현재 카메라의 위치의 거리를 구한다.
	CCamera*	pCamera = m_pGameObject->FindComponentFromTypeID<CCamera>();

	float	fSpeed = WHEELDIR * m_fZoomSpeed * fTime;
	DxVector3	vPos = m_pTransform->GetWorldPos();
	DxVector3	vAxisZ = pCamera->GetAxis(AXIS_Z);

	vPos += vAxisZ * fSpeed;
	m_pTransform->SetWorldPos(vPos);

	CGameObject*	pAttachObj = pCamera->GetAttachObject();

	SAFE_RELEASE(pCamera);

	if (pAttachObj)
	{
		CTransform*	pTr = pAttachObj->GetTransform();

		DxVector3	vAttachPos = pTr->GetWorldPos();

		float	fDist = vAttachPos.Distance(vPos);

		if (fDist < m_fMinDist)
		{
			fDist -= m_fMinDist;
			vPos += vAxisZ * fDist;
			m_pTransform->SetWorldPos(vPos);
		}

		else if (fDist > m_fMaxDist)
		{
			fDist -= m_fMaxDist;
			vPos += vAxisZ * fDist;
			m_pTransform->SetWorldPos(vPos);
		}

		SAFE_RELEASE(pTr);

		SAFE_RELEASE(pAttachObj);
	}

}

void CCameraArm::RotationDrag(float fTime)
{
	CCamera*	pCamera = m_pGameObject->FindComponentFromTypeID<CCamera>();
	CGameObject*	pAttachObj = pCamera->GetAttachObject();

	if (pAttachObj)
	{
		CTransform*	pTr = pAttachObj->GetTransform();
		DxVector3	vAttachPos = pTr->GetWorldPos();

		// 마우스가 RButton Drag 상태인지를 체크한다.
		if (KEYPUSH("MouseRButton"))
		{
			// 마우스 이동양을 얻어온다.
			POINT	tMouseMove = MOUSEMOVE;

			if (tMouseMove.x != 0)
			{
				DxVector3	vAxis[AXIS_MAX];
				DxVector3	vPos = m_pTransform->GetWorldPos();

				for (int i = 0; i < AXIS_MAX; ++i)
				{
					vAxis[i] = pCamera->GetAxis((AXIS)i);
				}

				// 마우스가 이동한 양을 이용해서 각도를 구해준다.
				float	fAngle = tMouseMove.x * fTime;

				MATRIX	matRot = XMMatrixRotationY(fAngle);

				// 축을 구한다.
				for (int i = 0; i < AXIS_MAX; ++i)
				{
					vAxis[i] = vAxis[i].TransformNormal(matRot);
					vAxis[i] = vAxis[i].Normalize();

					pCamera->SetAxis(vAxis);
				}

				// 행렬의 회전 중점을 AttachObject의 위치로 설정한다.
				matRot._41 = vAttachPos.x;
				matRot._42 = vAttachPos.y;
				matRot._43 = vAttachPos.z;

				// AttachObject로부터의 상대적인 위치를 구한다.
				DxVector3	vDist = vPos - vAttachPos;

				vPos = vDist.TransformCoord(matRot);

				m_pTransform->SetWorldPos(vPos);
			}

			if (tMouseMove.y != 0)
			{
				DxVector3	vAxis[AXIS_MAX];
				DxVector3	vPos = m_pTransform->GetWorldPos();

				for (int i = 0; i < AXIS_MAX; ++i)
				{
					vAxis[i] = pCamera->GetAxis((AXIS)i);
				}

				// 마우스가 이동한 양을 이용해서 각도를 구해준다.
				float	fAngle = tMouseMove.y * fTime;

				MATRIX	matRot = XMMatrixRotationAxis(vAxis[AXIS_X].Convert(), fAngle);

				// 축을 구한다.
				for (int i = AXIS_Y; i < AXIS_MAX; ++i)
				{
					vAxis[i] = vAxis[i].TransformNormal(matRot);
					vAxis[i] = vAxis[i].Normalize();

					pCamera->SetAxis(vAxis);
				}

				// 행렬의 회전 중점을 AttachObject의 위치로 설정한다.
				matRot._41 = vAttachPos.x;
				matRot._42 = vAttachPos.y;
				matRot._43 = vAttachPos.z;

				// AttachObject로부터의 상대적인 위치를 구한다.
				DxVector3	vDist = vPos - vAttachPos;

				vPos = vDist.TransformCoord(matRot);

				m_pTransform->SetWorldPos(vPos);
			}
		}

		SAFE_RELEASE(pTr);
		SAFE_RELEASE(pAttachObj);
	}

	SAFE_RELEASE(pCamera);
}
