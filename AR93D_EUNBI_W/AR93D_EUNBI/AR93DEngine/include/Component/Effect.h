#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CEffect :
	public CComponent
{
private:
	friend class CGameObject;

private:
	CEffect();
	CEffect(const CEffect& effect);
	~CEffect();

private:
	EFFECTCBUFFER m_tEffectCBuffer;

public:
	void SetEffectCBuffer();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CEffect* Clone();
};

AR3D_END

