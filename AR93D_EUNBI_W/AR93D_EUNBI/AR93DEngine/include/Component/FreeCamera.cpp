#include "FreeCamera.h"
#include "Transform.h"
#include "../Core/Input.h"
#include "Transform.h"
#include "../GameObject/GameObject.h"
#include "Camera.h"
#include "../Core/Input.h"

AR3D_USING

CFreeCamera::CFreeCamera()
{
	SetTag("FreeCamera");
	SetTypeName("CFreeCamera");
	SetTypeID<CFreeCamera>();
	m_eComType = CT_CAMERAARM;
}


CFreeCamera::~CFreeCamera()
{
}

bool CFreeCamera::Init()
{
	GET_SINGLE(CInput)->CreateKey("MoveLeft", 'A');
	GET_SINGLE(CInput)->CreateKey("MoveRight", 'D');
	GET_SINGLE(CInput)->CreateKey("MoveUp", 'W');
	GET_SINGLE(CInput)->CreateKey("MoveDown", 'S');

	CTransform*	pCameraTr = m_pGameObject->GetTransform();

	pCameraTr->SetWorldPos(350.f, 100.f, 210.f);
	pCameraTr->SetWorldRotX(AR3D_PI / 4.f);

	SAFE_RELEASE(pCameraTr);

	return true;
}

void CFreeCamera::Input(float fTime)
{

	CTransform*	pTransform = m_pGameObject->GetTransform();

	CCamera* pCamera = (CCamera*)m_pGameObject->FindComponentFromTypeID<CCamera>();

	DxVector3 forward = pCamera->GetAxis(AXIS_Z);
	DxVector3 back = DxVector3(forward.x * -1, forward.y * -1, forward.z * -1);

	DxVector3 right = pCamera->GetAxis(AXIS_X);
	DxVector3 left = DxVector3(right.x * -1, right.y * -1, right.z * -1);

	if (KEYPRESS("MoveLeft") || KEYPUSH("MoveLeft"))
	{
		m_pTransform->Move(left, 0.5f);
	}

	if (KEYPRESS("MoveRight") || KEYPUSH("MoveRight"))
	{
		m_pTransform->Move(right, 0.5f);
	}

	if (KEYPRESS("MoveUp") || KEYPUSH("MoveUp"))
	{
		m_pTransform->Move(forward, 0.5f);
	}

	if (KEYPRESS("MoveDown") || KEYPUSH("MoveDown"))
	{
		m_pTransform->Move(back, 0.5f);
	}
	SAFE_RELEASE(pCamera);
	SAFE_RELEASE(pTransform);
}

void CFreeCamera::Update(float fTime)
{
	RotationDrag(fTime);
}

void CFreeCamera::LateUpdate(float fTime)
{
}

void CFreeCamera::Collision(float fTime)
{
}

void CFreeCamera::Render(float fTime)
{
}

CFreeCamera * CFreeCamera::Clone()
{
	return new CFreeCamera(*this);
}

void CFreeCamera::RotationDrag(float fTime)
{
	if (KEYPUSH("MouseRButton"))
	{
		// 마우스 이동양을 얻어온다.
		POINT	tMouseMove = MOUSEMOVE;

		float	fAngleX = tMouseMove.x * fTime;
		CTransform*	pTransform = m_pGameObject->GetTransform();
		pTransform->RotateY(fAngleX);

		float	fAngleY = tMouseMove.y * fTime;
		pTransform->RotateX(fAngleY);
		SAFE_RELEASE(pTransform);
	}

}
