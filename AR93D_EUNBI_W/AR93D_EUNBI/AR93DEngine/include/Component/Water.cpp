#include "Water.h"
#include "Terrain.h"
#include "../Resources/ResourcesManager.h"
#include "../Resources/Mesh.h"
#include "Renderer.h"
#include "Material.h"
#include "../GameObject/GameObject.h"
#include "../Core/PathManager.h"
#include "ColliderTerrain.h"
#include "../Scene/Scene.h"
#include "../Scene/SceneManager.h"
#include "../Device.h"
#include "../Resources/Texture.h"
#include "../Component/Transform.h"

AR3D_USING

CWater::CWater():
	m_iVtxSizeX(0),
	m_iVtxSizeZ(0)
{
	m_eComType = CT_WATER;
	SetTag("Water");
	SetTypeName("CWater");
	SetTypeID<CWater>();
	m_WaterInfo.fWaterTime = 0.f;
}


CWater::CWater(const CWater & water) :
	CComponent(water)
{
	m_iVtxSizeX = water.m_iVtxSizeX;
	m_iVtxSizeZ = water.m_iVtxSizeZ;
	m_WaterInfo = water.m_WaterInfo;
}
CWater::~CWater()
{
}

bool CWater::CreateWater(const string & strKey, unsigned int iVtxSizeX, unsigned int iVtxSizeZ, const string & strPathKey)
{
	m_strKey = strKey;

	m_vecPos.clear();

	m_iVtxSizeX = iVtxSizeX;
	m_iVtxSizeZ = iVtxSizeZ;

	int WaterRect = 4;

	float fx = iVtxSizeX*0.5f;
	float fz = iVtxSizeZ*0.5f;

	//버텍스 제작 

	VERTEXNORMAL	tVtx = {};
	//1
	tVtx.vPos = DxVector3(-fx, 0.f,
		fz);
	tVtx.vPos.y = 1;
	tVtx.vNormal = DxVector3(0.f, 1.f, 0.f);
	tVtx.vUV = DxVector2(0,
		1.f);
	m_vecVtx.push_back(tVtx);
	//2
	tVtx.vPos = DxVector3(fx, 0.f,
		fz);
	tVtx.vPos.y = 1;
	tVtx.vNormal = DxVector3(0.f, 1.f, 0.f);
	tVtx.vUV = DxVector2(1.f,
		1.f);
	m_vecVtx.push_back(tVtx);
	//3
	tVtx.vPos = DxVector3(fx, 0.f,
		-fz);
	tVtx.vPos.y = 1;
	tVtx.vNormal = DxVector3(0.f, 1.f, 0.f);
	tVtx.vUV = DxVector2(1.f,
		0.f);
	m_vecVtx.push_back(tVtx);

	//6
	tVtx.vPos = DxVector3(fx, 0.f,
		-fz);
	tVtx.vPos.y = 1;
	tVtx.vNormal = DxVector3(0.f, 1.f, 0.f);
	tVtx.vUV = DxVector2(1.f,
		0.f);
	m_vecVtx.push_back(tVtx);

	
	//5
	tVtx.vPos = DxVector3(-fx, 0.f,
		-fz);
	tVtx.vPos.y = 1;
	tVtx.vNormal = DxVector3(0.f, 1.f, 0.f);
	tVtx.vUV = DxVector2(0,
		0.f);
	m_vecVtx.push_back(tVtx);
	
	//4
	tVtx.vPos = DxVector3(-fx, 0.f,
		fz);
	tVtx.vPos.y = 1;
	tVtx.vNormal = DxVector3(0.f, 1.f, 0.f);
	tVtx.vUV = DxVector2(0,
		1.f);
	m_vecVtx.push_back(tVtx);

	CMesh*	pMesh = GET_SINGLE(CResourcesManager)->CreateMesh(strKey,
		&m_vecVtx[0], m_vecVtx.size(), sizeof(VERTEXNORMAL),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	pRenderer->SetMesh(pMesh);

	SAFE_RELEASE(pRenderer);

	SAFE_RELEASE(pMesh);


	return true;
}

void CWater::SetBaseTexture(const string & strKey, TCHAR * pFileName, const string & strPathKey)
{
	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", strKey, pFileName, strPathKey);

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

void CWater::SetDetailTexture(const string & strKey, TCHAR * pFileName, const string & strPathKey)
{
	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial();

	pMaterial->SetNormalMapTexture("Linear", strKey, pFileName, strPathKey); // 디테일텍스쳐를 노말맵에 저장해서 불러씀. 

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

void CWater::UpdateTerrainTexture(void)
{

}

bool CWater::Init()
{
	CRenderer*	pRenderer = m_pGameObject->AddComponent<CRenderer>("Renderer");

	assert(pRenderer);

	pRenderer->SetShader(WATER_SHADER);
	pRenderer->SetInputLayout("WaterInputLayout");
	pRenderer->SetRenderState(ALPHAOBJ);

	pRenderer->AddConstantBuffer("WaterInfo", 10, sizeof(WATERINFO),
		CUT_VERTEX | CUT_PIXEL);

	SAFE_RELEASE(pRenderer);

	CTransform* pTransform = m_pGameObject->GetTransform();

	pTransform->SetWorldPos(322.5f, 20.f, 322.5f);

	SAFE_RELEASE(pTransform);



	return true;
}

void CWater::Input(float fTime)
{
}

void CWater::Update(float fTime)
{
}

void CWater::LateUpdate(float fTime)
{
	WaterTime += fTime;

	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	//if (((int)WaterTime % 2) == 0)
	//{
		m_WaterInfo.fWaterTime += fTime;
		pRenderer->UpdateCBuffer("WaterInfo", &m_WaterInfo);
		//if(WaterTime >= 100000)
			//WaterTime = 0.f;
	//}
}

void CWater::Collision(float fTime)
{
}

void CWater::Render(float fTime)
{
}

CWater * CWater::Clone()
{
	return new CWater(*this);
}
