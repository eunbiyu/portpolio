#pragma once
#include "Collider.h"


AR3D_BEGIN

class DLL CColliderSphere :
	public CCollider
{
private:
	friend class CGameObject;

private:
	CColliderSphere();
	CColliderSphere(const CColliderSphere& collider);
	virtual ~CColliderSphere();


public:
	void* operator new (size_t size)
	{
		return _aligned_malloc(size, 16);
	}

	void operator delete (void* p)
	{
		_aligned_free(p);
	}


private:
	SPHEREINFO		m_tInfo;

public:
	SPHEREINFO GetSphereInfo()	const;

public:
	void SetSphereInfo(const DxVector3& vCenter, float fRadius);
	void SetSphereInfo(float x, float y, float z, float fRadius);
	void SetSphereInfo(const SPHEREINFO& tSphere);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CColliderSphere* Clone();
	virtual bool Collision(CCollider* pCollider);
};

AR3D_END
