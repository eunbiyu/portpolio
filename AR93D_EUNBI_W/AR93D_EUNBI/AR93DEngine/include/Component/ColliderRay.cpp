#include "ColliderRay.h"
#include "Camera.h"
#include "../Scene/Scene.h"
#include "ColliderSphere.h"
#include "Transform.h"
#include "ColliderTerrain.h"
#include "../Core/Input.h"
#include "../ObjInformation.h"
#include "../GameObject/GameObject.h"
#include "../Scene/Scene.h"
#include "../Scene/Layer.h"
#include "../Component/Renderer.h"
#include "../Component/Transform.h"
#include "../Scene/SceneManager.h"
#include "../Component/Terrain.h"
#include "../Rendering/RenderManager.h"
#include "../Core/Input.h"
#include "../Core/GameTotalManager.h"

AR3D_USING

CColliderRay::CColliderRay()
	: m_bPick(false)
{
	m_eCollType = CT_RAY;
	m_eCollGroup = CG_RAY;
	SetTag("ColliderRay");
	SetTypeName("CColliderRay");
	SetTypeID<CColliderRay>();

	m_tRay.bIntersect = false;
}

CColliderRay::CColliderRay(const CColliderRay & collider) :
	CCollider(collider)
{
}

CColliderRay::~CColliderRay()
{
}

RAY CColliderRay::GetRay() const
{
	return m_tRay;
}

DxVector3 CColliderRay::GetRayToTerrainPickPos() const
{
	return m_vPickPos;
}


void CColliderRay::SetRay(const RAY & ray)
{
	m_tRay = ray;
}

void CColliderRay::SetRay(const DxVector3 & vPos, const DxVector3 & vDir)
{
	m_tRay.vPos = vPos;
	m_tRay.vDir = vDir;
}


bool CColliderRay::Init()
{
	return true;
}

void CColliderRay::Input(float fTime)
{
}

void CColliderRay::Update(float fTime)
{
}

void CColliderRay::LateUpdate(float fTime)
{
	m_tRay.bIntersect = false;
}

void CColliderRay::Collision(float fTime)
{
}

void CColliderRay::Render(float fTime)
{
}

CColliderRay * CColliderRay::Clone()
{
	return new CColliderRay(*this);
}

bool CColliderRay::Collision(CCollider * pCollider)
{
	
	switch (pCollider->GetColliderType())
	{
	case CT_SPHERE:
		if (KEYPRESS("MouseLButton") || KEYPUSH("MouseLButton")) 
		{
			GET_SINGLE(CInput)->m_MouseClickOn = true;

			CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
			if (pScene->GetTag() == "ToolScene")
			{
				if (pScene->GetDialogType() == DT_OBJECT) 
				{
					CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
					m_TerrainInfo = pScene->GetTerrainColInfo();

					DDTPicking();
					SetRayToTerrainCollPos(m_vPickPos);
					return CollisionRayToSphere(m_tRay, ((CColliderSphere*)pCollider)->GetSphereInfo());
				}
			}

			else
			{
				return CollisionRayToSphere(m_tRay,
					((CColliderSphere*)pCollider)->GetSphereInfo());
			}
		}
		return 0;

	//case CT_TERRAIN_COLL:
	//	if (KEYPRESS("MouseLButton") || KEYPUSH("MouseLButton"))
	//	{
	//		//GET_SINGLE(CInput)->m_MouseClickOn = true;

	//		CScene*  pScene2 = GET_SINGLE(CSceneManager)->GetCurrentScene();
	//		if (pScene2->GetTag() == "ToolScene")
	//		{
	//			if (pScene2->GetDialogType() == DT_TERRAIN)
	//			{
	//				m_TerrainInfo = ((CColliderTerrain*)pCollider)->GetInfo();
	//				DDTPicking();
	//				if (m_bPick)
	//				{
	//					SetRayToTerrainCollPos(m_vPickPos);
	//					((CColliderTerrain*)pCollider)->SetPickPosInfo(m_vPickPos);
	//				}

	//				m_bPick = false;
	//				return true;
	//			}
	//		}
	//		else
	//		{
	//			//GET_SINGLE(CInput)->m_MouseClickOn = true;

	//			pScene2 = GET_SINGLE(CSceneManager)->GetCurrentScene();

	//			m_TerrainInfo = ((CColliderTerrain*)pCollider)->GetInfo();

	//			DDTPicking();
	//			if (m_bPick)
	//			{
	//				SetRayToTerrainCollPos(m_vPickPos);
	//				((CColliderTerrain*)pCollider)->SetPickPosInfo(m_vPickPos);
	//				
	//				
	//				GET_SINGLE(CInput)->SetPlayerMovePos(m_vPickPos);
	//			}

	//			m_bPick = false;

	//		}
	//	}
	//	

	}
	return false;
}


//지형 DDT 픽킹 알고리즘 

void CColliderRay::DDTPicking()
{
	m_bPick = false;

	//Layer의 카메라 얻어오기
	CCamera* pCamera = GET_SINGLE(CSceneManager)->GetCurrentScene()->GetMainCamera();

	if (NULL == pCamera)
		return;

	//
	DxVector3 vDir = m_tRay.vDir;

	//Start Vector
	DxVector3 vCameraPos = m_tRay.vPos;

	//방향 노말라이즈
	vDir = XMVector3Normalize(vDir.Convert());

	//증가할 방향
	float DX = vDir.x;
	float DZ = vDir.z;

	//시작 기준점
	DxVector3 StartPos = vCameraPos;
	StartPos.y = 0.0f;

	DxVector3 vLength = { 0.f,0.f,0.f };

	//시작점을 계산해 준다
	if (false == IsValidPoint(StartPos))
	{
		while (true)
		{
			StartPos.x += DX*0.05f;
			StartPos.z += DZ*0.05f;

			//시작점을 찾으면 break;
			if (true == IsValidPoint(StartPos))
					break;

			vLength = StartPos - vCameraPos;
			vLength = XMVector3Length(vLength.Convert());

			//1000 을 Max 찾는 거리 변수화 시켜야함
			//이상한 곳을 클릭
			if (vLength.x >= 2000)
				return;
		}
	}

	//이 백터는 내가 찾을 유효한 애들의 좌표를 관리하는 백터
	m_vecIndex.push_back(PostoIndex(StartPos));

	//기울기 많큼 증가하면서 인덱스 저장
	while (true)
	{
		StartPos.x += DX* 0.1f;
		StartPos.z += DZ* 0.1f;

		if (false == IsValidPoint(StartPos))
			break;

		//모든 기준을 통과하면 유효한 인덱스에 좌표들을 넣어줌
		if (false == IsBeforeIdx(StartPos))
			m_vecIndex.push_back(PostoIndex(StartPos));
	}

	int Size = m_vecIndex.size();

	float MinDist = 999999999.f;
	float fDist = 0.0f;

	vector<DxVector3>	pVtx = m_TerrainInfo.vecPos;

	for (int i = 0; i < Size; ++i)
	{
		if (m_vecIndex[i].iRightTop >(m_TerrainInfo.iNumH + 1) * (m_TerrainInfo.iNumW + 1) ||
			m_vecIndex[i].iLeftBot < 0)
			continue;

		if (true == DirectX::TriangleTests::Intersects(m_tRay.vPos.Convert()
			, m_tRay.vDir.Convert()
			, pVtx[m_vecIndex[i].iLeftTop].Convert()							//내가 유효하게 모아놓은 애들의 윗 삼각형이 Ray와 충돌하는지 체크
			, pVtx[m_vecIndex[i].iRightTop].Convert()
			, pVtx[m_vecIndex[i].iRightBot].Convert(), fDist))
		{
			if (MinDist >= fDist)
			{
				MinDist = fDist;													//최소 인 애들을 찾아냄
				m_bPick = true;
			}
		}

		if (true == DirectX::TriangleTests::Intersects(m_tRay.vPos.Convert()
			, m_tRay.vDir.Convert()
			, pVtx[m_vecIndex[i].iLeftTop].Convert()
			, pVtx[m_vecIndex[i].iRightBot].Convert()							//내가 유효하게 모아놓은 애들의 아랫 삼각형이 Ray와 충돌하는지 체크
			, pVtx[m_vecIndex[i].iLeftBot].Convert(), fDist))
		{
			if (MinDist >= fDist)
			{
				MinDist = fDist;
				m_bPick = true;
			}
		}
	}

	//PickPos 는 Local Pos 여서 나중에 월드로 쓸려면 지형의 World 행렬을 곱해줘야함
	if (true == m_bPick) 
	{
		m_vPickPos = m_tRay.vPos + m_tRay.vDir * MinDist;
	}

	m_vecIndex.clear();

	SAFE_RELEASE(pCamera);
}

DxVector3 CColliderRay::DDTPickingY(const DxVector3 & vPos)
{
	return DxVector3();
}

bool CColliderRay::IsValidPoint(const DxVector3 & _vPos)
{
	//터레인 안을 잘 픽킹하고 있는 지 찾는 함수
	//현재의 점이 터레인의 밖에 있을 경우 return true
	if (_vPos.x <= (m_TerrainInfo.iNumW* m_TerrainInfo.iSizeW) && _vPos.x >= 0 && _vPos.z <=
		(m_TerrainInfo.iNumH* m_TerrainInfo.iSizeH) && _vPos.z >= 0)
	{
		return true;
	}

	return false;
}

tAreaInfo CColliderRay::PostoIndex(const DxVector3 & _Pos)
{
	//현재 내가 찾은 위치들을 현재 내가 지나는 사각형들로 바꿔줌
	//Terrain의 정점으로 바꿔줌 
	//내가 지나는 Pos 들을 근삿 값으로 바꿈
	DxVector3 TempPos;

	TempPos.x = floorf(_Pos.x);
	TempPos.z = floorf(_Pos.z);

	//현재 내가 지나는 사각형의 네 정점

	int	idx = (m_TerrainInfo.iNumH - 1 - ((int)(TempPos.z/m_TerrainInfo.iSizeH) + 1)) * 
		m_TerrainInfo.iNumW + (int)(TempPos.x / m_TerrainInfo.iSizeH);

	tAreaInfo tArea;
	tArea.fRadius = 0.0f;

	/*DxVector3	vRcPos[4];
	vRcPos[0] = tTerrain.vecPos[idx];
	vRcPos[1] = tTerrain.vecPos[idx + 1];
	vRcPos[2] = tTerrain.vecPos[idx + tTerrain.iNumW];
	vRcPos[3] = tTerrain.vecPos[idx + tTerrain.iNumW + 1];*/

	tArea.iLeftBot = idx;
	tArea.iLeftTop = idx+1;
	tArea.iRightTop = idx + m_TerrainInfo.iNumW;
	tArea.iRightBot = idx + m_TerrainInfo.iNumW + 1;

	return tArea;
}


bool CColliderRay::IsBeforeIdx(const DxVector3& _vPos)
{
	//이전의 인덱스와 겹치는지 확인해주는 함수
	//DDT 를 찾을때 근삿값이기 떄문에 같은점이 두번 들어갈 수있는 것을 방지

	UINT Size = m_vecIndex.size();

	if (Size == 0)
		return false;

	tAreaInfo tCurArea = PostoIndex(_vPos);
	tAreaInfo tBeforeArea = m_vecIndex[Size - 1];

	if (tCurArea.iLeftBot == tBeforeArea.iLeftBot && 
		tCurArea.iLeftTop == tBeforeArea.iLeftTop &&
		tCurArea.iRightBot == tBeforeArea.iRightBot &&
		tCurArea.iRightTop == tBeforeArea.iRightTop)
		return true;

	return false;
}

DxVector3 CColliderRay::GetPickWorldPos()
{
	return XMVector3TransformCoord(m_vPickPos.Convert(), m_pTransform->GetWorldMatrix());
}