#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CRenderer2D :
	public CComponent
{
private:
	friend class CGameObject;

private:
	CRenderer2D();
	CRenderer2D(const CRenderer2D& renderer);
	~CRenderer2D();

private:
	class CMesh*		m_pMesh;
	class CShader*		m_pShader;
	ID3D11InputLayout*	m_pInputLayout;
	class CRenderState*	m_pRenderState[RST_END];
	vector<vector<class CMaterial*>>	m_vecMaterial;
	unordered_map<string, PRENDERERCBUFFER>	m_mapCBuffer;

public:
	class CMaterial* GetMaterial(int iContainer = 0, int iSubset = 0);
	class CMesh* GetMesh()	const;

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CRenderer2D* Clone();

private:
	void UpdateTransform();

public:
	void SetMesh(const string& strKey);
	void SetMesh(const string& strKey, void* pVertices, unsigned int iVtxCount,
		unsigned int iVtxSize, D3D11_USAGE eVtxUsage,
		D3D11_PRIMITIVE_TOPOLOGY ePrimitive, void* pIndices = NULL,
		unsigned int iIdxCount = 0, unsigned int iIdxSize = 0,
		D3D11_USAGE eIdxUsage = D3D11_USAGE_DEFAULT,
		DXGI_FORMAT eFormat = DXGI_FORMAT_R32_UINT);
	void SetMesh(const string& strKey, const TCHAR* pFileName,
		const string& strPathKey = MESH_PATH);
	void SetShader(const string& strKey);
	void SetShader(const string& strKey, TCHAR* pFileName, char* pEntry[ST_END],
		bool bStreamOut = false, const string& strPathKey = SHADER_PATH);
	void SetInputLayout(const string& strKey);
	void SetRenderState(const string& strKey);

public:
	class CMaterial* CreateMaterial(const string& strSamplerKey,
		const string& strTextureKey, int iSmpRegister = 0, int iTexRegister = 0,
		TCHAR* pFileName = NULL,
		const string& strPathKey = TEXTURE_PATH,
		int iContainer = 0);
	void AddMaterial(class CMaterial* pMaterial, int iContainer = 0);
	void AddContainerMaterial();

public:
	void AddConstantBuffer(const string& strKey, int iRegister, int iSize,
		int iShaderType);
	bool UpdateCBuffer(const string& strKey, void* pData);
	PRENDERERCBUFFER FindConstantBuffer(const string& strKey);


	bool m_bUIback;
};

AR3D_END
