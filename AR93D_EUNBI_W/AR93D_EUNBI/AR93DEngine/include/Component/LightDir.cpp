#include "LightDir.h"
#include "Transform.h"
#include "../Rendering/ShaderManager.h"

AR3D_USING

CLightDir::CLightDir()
{
	SetTag("LightDir");
	SetTypeName("CLightDir");
	SetTypeID<CLightDir>();
	m_tInfo.eType = LT_DIR;
	m_axis = AXIS_Z;
}

CLightDir::CLightDir(const CLightDir & light) :
	CLight(light)
{
}

CLightDir::~CLightDir()
{
}

bool CLightDir::Init()
{
	return true;
}

void CLightDir::Input(float fTime)
{
}

void CLightDir::Update(float fTime)
{
}

void CLightDir::LateUpdate(float fTime)
{
}

void CLightDir::Collision(float fTime)
{
}

void CLightDir::Render(float fTime)
{
}

CLightDir * CLightDir::Clone()
{
	return new CLightDir(*this);
}

void CLightDir::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CLightDir::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CLightDir::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CLightDir::SetLight()
{
	CLight::SetLight();

	m_tCBuffer.vDir = m_pTransform->GetWorldAxis(m_axis) * -1;

	GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Light", &m_tCBuffer, CUT_VERTEX | CUT_PIXEL);
}

void CLightDir::SetLightDir(AXIS axis)
{
	m_axis = axis;
}
