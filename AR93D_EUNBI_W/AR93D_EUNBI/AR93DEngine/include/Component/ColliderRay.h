#pragma once
#include "Collider.h"

AR3D_BEGIN


class DLL CColliderRay :
	public CCollider
{
private:
	friend class CGameObject;

public:
	CColliderRay();
	CColliderRay(const CColliderRay& collider);
	~CColliderRay();


public:
	void* operator new (size_t size)
	{
		return _aligned_malloc(size, 16);
	}

	void operator delete (void* p)
	{
		_aligned_free(p);
	}


	//DDT Pick������ 
private:
	RAY m_tRay;
	bool m_bPick; 
	vector<tAreaInfo>			m_vecIndex;
	DxVector3					m_vPickPos;

public:
	RAY GetRay() const;
	DxVector3 GetRayToTerrainPickPos() const;
	void SetRay(const RAY& ray);
	void SetRay(const DxVector3& vPos, const DxVector3& vDir);

	void DDTPicking();
	DxVector3 DDTPickingY(const DxVector3& vPos);
	bool IsValidPoint(const DxVector3& _vPos);
	tAreaInfo PostoIndex(const DxVector3& _Pos);
	bool IsBeforeIdx(const DxVector3& _vPos);
	DxVector3 GetPickWorldPos();
	//================

private:
	TERRAINCOLLINFO	m_TerrainInfo;

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CColliderRay* Clone();
	virtual bool Collision(CCollider* pCollider);


};

AR3D_END
