#include "UIBack.h"

AR3D_USING

CUIBack::CUIBack()
{
	SetTag("Back");
	SetTypeName("CUIBack");
	SetTypeID<CUIBack>();
	m_eUIType = UT_BACK;
}


CUIBack::CUIBack(const CUIBack& back)
	:CUI(back)
{
}


CUIBack::~CUIBack()
{
}

bool CUIBack::Init()
{
	return true;
}

void CUIBack::Input(float fTime)
{
}

void CUIBack::Update(float fTime)
{
}

void CUIBack::LateUpdate(float fTime)
{
}

void CUIBack::Collision(float fTime)
{
}

void CUIBack::Render(float fTime)
{
}

CUIBack * CUIBack::Clone()
{
	return new CUIBack(*this);
}

void CUIBack::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CUIBack::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CUIBack::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}
