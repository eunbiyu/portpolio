#include "Light.h"

AR3D_USING

CLight::CLight()
{
	m_eComType = CT_LIGHT;
	m_tInfo.vDif = DxVector4(1.f, 1.f, 1.f, 1.f);
	m_tInfo.vAmb = DxVector4(0.3f, 0.3f, 0.3f, 1.f);
	m_tInfo.vSpc = DxVector4(0.3f, 0.3f, 0.3f, 1.f);
}

CLight::CLight(const CLight & light) :
	CComponent(light)
{
	m_tInfo = light.m_tInfo;
}

CLight::~CLight()
{
}

LIGHTINFO CLight::GetLightInfo() const
{
	return m_tInfo;
}
void CLight::SetLightRange(float fRange)
{
	m_tCBuffer.fRange = fRange;
}


void CLight::SetLightInfo(const LIGHTINFO & tInfo)
{
	LIGHT_TYPE	eType = m_tInfo.eType;
	m_tInfo = tInfo;
	m_tInfo.eType = eType;
}

void CLight::SetLightInfo(const DxVector4 & vDif, const DxVector4 & vAmb, const DxVector4 & vSpc)
{
	m_tInfo.vDif = vDif;
	m_tInfo.vAmb = vAmb;
	m_tInfo.vSpc = vSpc;
}

void CLight::SetLightInfo(const DxVector3 & vDif, const DxVector3 & vAmb, const DxVector3 & vSpc)
{
	m_tInfo.vDif = vDif;
	m_tInfo.vAmb = vAmb;
	m_tInfo.vSpc = vSpc;
}

void CLight::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CLight::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CLight::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CLight::SetLight()
{
	m_tCBuffer.eType = m_tInfo.eType;
	m_tCBuffer.vDif = m_tInfo.vDif;
	m_tCBuffer.vAmb = m_tInfo.vAmb;
	m_tCBuffer.vSpc = m_tInfo.vSpc;
}
