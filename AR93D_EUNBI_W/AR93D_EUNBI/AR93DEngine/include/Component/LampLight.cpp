#include "LampLight.h"
#include "../Device.h"
#include "../Resources/ResourcesManager.h"
#include "../Rendering/RenderManager.h"
#include "../Rendering/RenderState.h"
#include "../Rendering/Shader.h"
#include "../Rendering/ShaderManager.h"
#include "Transform.h"
#include "Camera.h"
#include "../Scene/Scene.h"
#include "../Core/GameTotalManager.h"
#include "LightPoint.h"

AR3D_USING

CLampLight::CLampLight() :
	m_pDrawVB(NULL),
	m_pInputLayout(NULL),
	m_pLightShader(NULL),
	m_pDepthWriteDisable(NULL),
	m_fLightRange(20.f),
	m_bLight(false)
{
	SetTag("LampLight");
	SetTypeName("CLampLight");
	SetTypeID<CLampLight>();
	m_eComType = CT_LAMPLIGHT;

	SetInputLayout("LampInputLayout");

	m_pLightShader = GET_SINGLE(CShaderManager)->FindShader(LAMPLIGHT_SHADER);

	m_pDepthDisable = GET_SINGLE(CRenderManager)->FindRenderState(DEPTH_DISABLE);
}

CLampLight::CLampLight(const CLampLight & particle) :
	CComponent(particle)
{
	*this = particle;

	m_pLightShader = particle.m_pLightShader;

	if (m_pLightShader)
		m_pLightShader->AddRef();

	if (m_pDepthDisable)
		m_pDepthDisable->AddRef();

	if (m_pDepthWriteDisable)
		m_pDepthWriteDisable->AddRef();

	m_pInputLayout = particle.m_pInputLayout;
}


CLampLight::~CLampLight()
{
	SAFE_RELEASE(m_pLightShader);
	SAFE_RELEASE(m_pDepthDisable);
	SAFE_RELEASE(m_pDrawVB);
}

bool CLampLight::GetLampLight() const
{
	return m_bLight;
}

void CLampLight::SetLightRange(float fRange)
{
	m_fLightRange = fRange;
}


void CLampLight::SetInputLayout(const string & strKey)
{
	m_pInputLayout = GET_SINGLE(CShaderManager)->FindInputLayout(strKey);
}

void CLampLight::SetLampLight(bool bParticleLight)
{
	m_bLight = bParticleLight;
	SAFE_RELEASE(m_pDepthWriteDisable);
	if (bParticleLight)
	{
		m_pDepthWriteDisable = GET_SINGLE(CRenderManager)->FindRenderState(DEPTH_WRITE_DISABLE);
	}
}

void CLampLight::SetLampLightPos(const DxVector3 & Lightpos)
{
}

void CLampLight::SetLampLightInfo(DxVector3 pos)
{
	D3D11_BUFFER_DESC	tDesc = {};

	tDesc.Usage = D3D11_USAGE_DEFAULT;
	tDesc.ByteWidth = sizeof(VERTEXLAMPLIGHT);
	tDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	VERTEXLAMPLIGHT	vtx = {};
	vtx.fLightRange = 20.f;
	vtx.vPos = DxVector3(pos.x, pos.y+ 1.f, pos.z);


	D3D11_SUBRESOURCE_DATA	tData = {};
	tData.pSysMem = &vtx;

	if (FAILED(DEVICE->CreateBuffer(&tDesc, &tData, &m_pDrawVB)))
		return;

	tDesc.ByteWidth = sizeof(VERTEXLAMPLIGHT) ;
	tDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER ;

}

bool CLampLight::Init()
{
	return true;
}

void CLampLight::Input(float fTime)
{
}

void CLampLight::Update(float fTime)
{
}

void CLampLight::LateUpdate(float fTime)
{
}

void CLampLight::Collision(float fTime)
{
}

void CLampLight::Render(float fTime)
{
}

CLampLight * CLampLight::Clone()
{
	return new CLampLight(*this);
}

void CLampLight::RenderLight(float fTime)
{
	if ((GET_SINGLE(CGameTotalManager)->GetNight()))
	{
		SetLampLightInfo(m_pTransform->GetWorldPos());
		LAMPLIGHTCBUFFER	tCBuffer = {};

		CCamera*	pCamera = m_pScene->GetMainCamera();
		CTransform*	pCameraTr = pCamera->GetTransform();

		tCBuffer.vCamAxisY = pCamera->GetAxis(AXIS_Y);
		tCBuffer.vCamAxisX = pCamera->GetAxis(AXIS_X) * -1.f;

		TRANSFORMCBUFFER	tTransform;

		tTransform.matView = pCamera->GetViewMatrix();
		tTransform.matVP = pCamera->GetViewMatrix() * pCamera->GetProjMatrix();
		tTransform.matProj = pCamera->GetProjMatrix();
		tTransform.matInvProj = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matProj),
			tTransform.matProj);

		tTransform.matView = XMMatrixTranspose(tTransform.matView);
		tTransform.matProj = XMMatrixTranspose(tTransform.matProj);
		tTransform.matInvProj = XMMatrixTranspose(tTransform.matInvProj);
		tTransform.matVP = XMMatrixTranspose(tTransform.matVP);

		GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Transform", &tTransform, CUT_GEOMETRY);
		SAFE_RELEASE(pCameraTr);
		SAFE_RELEASE(pCamera);

		GET_SINGLE(CShaderManager)->UpdateConstantBuffer("LampLightCBuffer",
			&tCBuffer, CUT_GEOMETRY);

		UINT	iStride = sizeof(VERTEXLAMPLIGHT);
		UINT	iOffset = 0;

		m_pLightShader->SetShader();

		m_pDepthWriteDisable->SetState();

		CONTEXT->IASetInputLayout(m_pInputLayout);
		CONTEXT->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
		CONTEXT->IASetVertexBuffers(0, 1, &m_pDrawVB, &iStride, &iOffset);

		CONTEXT->Draw(1, 0);

		m_pDepthWriteDisable->ResetState();
	}
}
