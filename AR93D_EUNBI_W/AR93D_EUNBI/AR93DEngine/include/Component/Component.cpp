#include "Component.h"
#include "../GameObject/GameObject.h"
#include "../Scene/Scene.h"
#include "../Scene/Layer.h"
#include "Transform.h"

AR3D_USING

CComponent::CComponent() :
	m_pScene(NULL),
	m_pLayer(NULL),
	m_pGameObject(NULL),
	m_pTransform(NULL)
{
}

CComponent::CComponent(const CComponent & com)
{
	*this = com;
}

CComponent::~CComponent()
{
}

COMPONENT_TYPE CComponent::GetComponentType() const
{
	return m_eComType;
}

CTransform * CComponent::GetTransform() const
{
	m_pTransform->AddRef();
	return m_pTransform;
}

CGameObject * CComponent::GetGameObject() const
{
	m_pGameObject->AddRef();
	return m_pGameObject;
}

void CComponent::SetComponentType(COMPONENT_TYPE ct)
{
	m_eComType = ct;
}

void CComponent::SetScene(CScene * pScene)
{
	m_pScene = pScene;
}

void CComponent::SetLayer(CLayer * pLayer)
{
	m_pLayer = pLayer;
}

void CComponent::SetGameObject(CGameObject * pObj)
{
	m_pGameObject = pObj;
}

CComponent * CComponent::FindComponentFromTag(const string & strTag)
{
	return m_pGameObject->FindComponentFromTag(strTag);
}

CComponent * CComponent::FindComponentFromTypeName(const string & strTypeName)
{
	return m_pGameObject->FindComponentFromTypeName(strTypeName);
}

CComponent * CComponent::FindComponentFromType(COMPONENT_TYPE eType)
{
	return m_pGameObject->FindComponentFromType(eType);
}

bool CComponent::CheckComponentFromTag(const string & strTag)
{
	return m_pGameObject->CheckComponentFromTag(strTag);
}

bool CComponent::CheckComponentFromTypeName(const string & strTypeName)
{
	return m_pGameObject->CheckComponentFromTypeName(strTypeName);
}

bool CComponent::CheckComponentFromType(COMPONENT_TYPE eType)
{
	return m_pGameObject->CheckComponentFromType(eType);
}

list<class CComponent*>* CComponent::FindComponentsFromTag(const string & strTag)
{
	return m_pGameObject->FindComponentsFromTag(strTag);
}

list<class CComponent*>* CComponent::FindComponentsFromTypeName(const string & strTypeName)
{
	return m_pGameObject->FindComponentsFromTypeName(strTypeName);
}

list<class CComponent*>* CComponent::FindComponentsFromType(COMPONENT_TYPE eType)
{
	return m_pGameObject->FindComponentsFromType(eType);
}


void CComponent::SetTransform(CTransform * pTransform)
{
	m_pTransform = pTransform;
}

void CComponent::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CComponent::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CComponent::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}
