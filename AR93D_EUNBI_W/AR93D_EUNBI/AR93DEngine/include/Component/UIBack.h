#pragma once
#include "UI.h"

AR3D_BEGIN

class DLL CUIBack :
	public CUI
{
protected:
	friend class CGameObject;

protected:
	CUIBack();
	CUIBack(const CUIBack& back);
	~CUIBack();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CUIBack* Clone();
	virtual void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
};

AR3D_END

