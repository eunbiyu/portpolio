#pragma once
#include "Component.h"

AR3D_BEGIN

class CMesh;
class CShader;
class CRenderState;

class DLL CCollider :
	public CComponent
{
protected:
	CCollider();
	CCollider(const CCollider& collider);
	virtual ~CCollider() = 0;

protected:
	COLLIDER_TYPE	m_eCollType;
	COLLIDER_GROUP	m_eCollGroup;
	DxVector3		m_vRayToTerrainPickPos;
	DxVector3		m_vPrevPos;
	DxVector3		m_vMove;
	list<CCollider*>	m_CollList;
	bool			m_bCollision;
	bool			m_bTerrainCollision;

//#ifdef _DEBUG
	CMesh*		m_pMesh;
	CShader*		m_pShader;
	CRenderState* m_pWireFrame;
	ID3D11InputLayout*	m_pLayout;
	TRANSFORMCBUFFER	m_tTCBuffer;
	DxVector4			m_vColor;
	DxVector4			m_vCollColor;
	DxVector4			m_vTerrainCollColor;
//#endif // _DEBUG

public:
	void AddCollList(CCollider* pColl);
	void EraseCollList(CCollider* pColl);
	bool CheckCollList(CCollider* pColl);
	void SetMesh(const string& strKey);
	void SetShader(const string& strKey);
	void SetInputLayout(const string& strKey);
	void SetColor(const DxVector4& vColor);
	void SetColor(float r, float g, float b, float a);
	void SetCollColor(const DxVector4& vColor);
	void SetCollColor(float r, float g, float b, float a);
	void SetTerrainCollColor(float r, float g, float b, float a);
	void SetRayToTerrainCollPos(const DxVector3& Pos);
	DxVector3 GetRayToTerrainCollPos();
	void CollisionEnable();
	void TerrainCollisionEnable();

public:
	COLLIDER_TYPE GetColliderType()	const;
	COLLIDER_GROUP GetColliderGroup()	const;
	void SetColliderGroup(COLLIDER_GROUP eCollGroup);

public:
	virtual bool Init() = 0;
	virtual void Input(float fTime) = 0;
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CCollider* Clone() = 0;
	virtual bool Collision(CCollider* pCollider) = 0;

protected:
	bool CollisionSphereToSphere(const SPHEREINFO& tSrc, const SPHEREINFO& tDest);
	bool CollisionRectToRect(const RECTINFO& tSrc, const RECTINFO& tDest);
	bool CollisionRectToPoint(const RECTINFO& tSrc, const DxVector3& vPos);
	bool CollisionRayToSphere(RAY & tRay, const SPHEREINFO & tSphere);
	bool CollisionTerrainToPosition(const TERRAINCOLLINFO& tTerrain,
		class CTransform* pDestTr,
		const DxVector3& vTerrainScale);
	bool CollisionCameraTerrainToPosition(const TERRAINCOLLINFO& tTerrain,
		class CTransform* pDestTr,
		const DxVector3& vTerrainScale);
	bool CollisionRayToTriangle(RAY & tRay, const DxVector3& v0, const DxVector3& v1,
		const DxVector3& v2, float& dist);
	bool CollisionAABBToAABB(const AABBBOXINFO& tSrc, const AABBBOXINFO& tDest);


};

AR3D_END
