#include "Animation2D.h"
#include "../Resources/Texture.h"
#include "../Resources/ResourcesManager.h"
#include "Renderer.h"
#include "Renderer2D.h"
#include "Material.h"
#include "../GameObject/GameObject.h"
#include "../Rendering/ShaderManager.h"

AR3D_USING

CAnimation2D::CAnimation2D() :
	m_iCurrentAnimation(0),
	m_iDefaultAnimation(0)
{
	SetTag("Animation2D");
	SetTypeName("CAnimation2D");
	SetTypeID<CAnimation2D>();
	m_eComType = CT_ANIMATION2D;
}

CAnimation2D::CAnimation2D(const CAnimation2D & anim) :
	CComponent(anim)
{
	m_iCurrentAnimation = anim.m_iCurrentAnimation;
	m_iDefaultAnimation = anim.m_iDefaultAnimation;

	m_vecAnimationClip.clear();
	for (size_t i = 0; i < anim.m_vecAnimationClip.size(); ++i)
	{
		PANIMATIONCLIP2D	pClip = new ANIMATIONCLIP2D;

		*pClip = *anim.m_vecAnimationClip[i];

		if (pClip->pTexture)
			pClip->pTexture->AddRef();

		m_vecAnimationClip.push_back(pClip);
	}
}

CAnimation2D::~CAnimation2D()
{
	for (size_t i = 0; i < m_vecAnimationClip.size(); ++i)
	{
		if (m_vecAnimationClip[i]->pTexture)
			SAFE_RELEASE(m_vecAnimationClip[i]->pTexture);
	}

	Safe_Delete_VecList(m_vecAnimationClip);
}

void CAnimation2D::AddAnimationClip(const string & strName,
	ANIMATION2D_TYPE eType, ANIMATION_OPTION eOption,
	int iFrameMaxX, int iFrameMaxY, float fLimitTime, int iLoopCount,
	float fLoopTime, const string & strTexKey, int iTexRegister, TCHAR * pFileName,
	const string & strPathKey)
{
	PANIMATIONCLIP2D	pClip = new ANIMATIONCLIP2D;

	pClip->strName = strName;
	pClip->eType = eType;
	pClip->eOption = eOption;
	pClip->iFrameMaxX = iFrameMaxX;
	pClip->iFrameMaxY = iFrameMaxY;
	pClip->fLimitTime = fLimitTime;
	pClip->iLoopCount = iLoopCount;
	pClip->fLoopTime = fLoopTime;
	pClip->iFrameX = 0;
	pClip->iFrameY = 0;
	pClip->fFrameTime = 0.f;
	pClip->iTexRegister = iTexRegister;

	if (pFileName)
	{
		pClip->pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(
			strTexKey, pFileName, strPathKey);
	}

	else
	{
		pClip->pTexture = GET_SINGLE(CResourcesManager)->FindTexture(
			strTexKey);
	}

	m_vecAnimationClip.push_back(pClip);
}

void CAnimation2D::AddAnimationClip(const string & strName, ANIMATION2D_TYPE eType,
	ANIMATION_OPTION eOption, int iFrameMaxX, int iFrameMaxY, float fLimitTime, int iLoopCount,
	float fLoopTime, const string & strTexKey, int iTexRegister, const vector<wstring>* vecFileName,
	const string & strPathKey)
{
	PANIMATIONCLIP2D	pClip = new ANIMATIONCLIP2D;

	pClip->strName = strName;
	pClip->eType = eType;
	pClip->eOption = eOption;
	pClip->iFrameMaxX = iFrameMaxX;
	pClip->iFrameMaxY = iFrameMaxY;
	pClip->fLimitTime = fLimitTime;
	pClip->iLoopCount = iLoopCount;
	pClip->fLoopTime = fLoopTime;
	pClip->iFrameX = 0;
	pClip->iFrameY = 0;
	pClip->fFrameTime = 0.f;
	pClip->iTexRegister = iTexRegister;

	if (vecFileName)
	{
		pClip->pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(
			strTexKey, *vecFileName, strPathKey);
	}

	else
	{
		pClip->pTexture = GET_SINGLE(CResourcesManager)->FindTexture(
			strTexKey);
	}

	m_vecAnimationClip.push_back(pClip);
}

void CAnimation2D::ChangeAnimation(int iAnimation)
{
	if (m_iCurrentAnimation == iAnimation)
		return;

	// 기존 동작 모션을 초기화한다.
	m_vecAnimationClip[m_iCurrentAnimation]->iFrameX = 0;
	m_vecAnimationClip[m_iCurrentAnimation]->iFrameY = 0;
	m_vecAnimationClip[m_iCurrentAnimation]->fFrameTime = 0.f;

	m_iCurrentAnimation = iAnimation;

	CRenderer*	pRenderer = m_pGameObject->FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial(0, 0);

	pMaterial->SetDiffuseTexture(m_vecAnimationClip[m_iCurrentAnimation]->pTexture,
		m_vecAnimationClip[m_iCurrentAnimation]->iTexRegister);

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

void CAnimation2D::SetDefaultAnimation(int iAnimation)
{
	m_iDefaultAnimation = iAnimation;
}

void CAnimation2D::SetShader()
{
	GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Animation2D",
		&m_tCBuffer, CUT_VERTEX | CUT_GEOMETRY | CUT_PIXEL);
}

void CAnimation2D::Start()
{
	CRenderer*	pRenderer = m_pGameObject->FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial(0, 0);

	pMaterial->SetDiffuseTexture(m_vecAnimationClip[m_iCurrentAnimation]->pTexture,
		m_vecAnimationClip[m_iCurrentAnimation]->iTexRegister);

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

bool CAnimation2D::Init()
{
	if (CheckComponentFromTypeID<CRenderer>())
	{
		CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

		pRenderer->AddConstantBuffer("Animation2D", 12, sizeof(ANIMATION2DCBUFFER),
			CUT_VERTEX | CUT_GEOMETRY | CUT_PIXEL);

		SAFE_RELEASE(pRenderer);
	}

	else if (CheckComponentFromTypeID<CRenderer2D>())
	{
		CRenderer2D*	pRenderer = FindComponentFromTypeID<CRenderer2D>();

		pRenderer->AddConstantBuffer("Animation2D", 12, sizeof(ANIMATION2DCBUFFER),
			CUT_VERTEX | CUT_GEOMETRY | CUT_PIXEL);

		SAFE_RELEASE(pRenderer);
	}

	return true;
}

void CAnimation2D::Input(float fTime)
{
}

void CAnimation2D::Update(float fTime)
{
}

void CAnimation2D::LateUpdate(float fTime)
{
	PANIMATIONCLIP2D	pClip = m_vecAnimationClip[m_iCurrentAnimation];

	pClip->fFrameTime += fTime;

	int	iMaxFrame = pClip->iFrameMaxX * pClip->iFrameMaxY;

	float	fFrameLimitTime = pClip->fLimitTime / iMaxFrame;

	if (pClip->fFrameTime >= fFrameLimitTime)
	{
		pClip->fFrameTime -= fFrameLimitTime;

		++pClip->iFrameX;

		if (pClip->iFrameX == pClip->iFrameMaxX)
		{
			++pClip->iFrameY;
			pClip->iFrameX = 0;

			if (pClip->iFrameY == pClip->iFrameMaxY)
			{
				pClip->iFrameY = 0;

				switch (pClip->eOption)
				{
				case AO_LOOP:
					break;
				case AO_ONCE_RETURN:
					ChangeAnimation(m_iDefaultAnimation);
					break;
				case AO_ONCE_DESTROY:
					m_pGameObject->Death();
					break;
				case AO_COUNT_RETURN:
					break;
				case AO_COUNT_DESTROY:
					break;
				case AO_TIME_RETURN:
					break;
				case AO_TIME_DESTROY:
					break;
				}
			}
		}
	}

	m_tCBuffer.eType = pClip->eType;
	m_tCBuffer.iFrameX = pClip->iFrameX;
	m_tCBuffer.iFrameY = pClip->iFrameY;
	m_tCBuffer.iFrameMaxX = pClip->iFrameMaxX;
	m_tCBuffer.iFrameMaxY = pClip->iFrameMaxY;

	if (CheckComponentFromTypeID<CRenderer>())
	{
		CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

		pRenderer->UpdateCBuffer("Animation2D", &m_tCBuffer);

		SAFE_RELEASE(pRenderer);
	}

	else if (CheckComponentFromTypeID<CRenderer2D>())
	{
		CRenderer2D*	pRenderer = FindComponentFromTypeID<CRenderer2D>();

		pRenderer->UpdateCBuffer("Animation2D", &m_tCBuffer);

		SAFE_RELEASE(pRenderer);
	}
}

void CAnimation2D::Collision(float fTime)
{
}

void CAnimation2D::Render(float fTime)
{
}

CAnimation2D * CAnimation2D::Clone()
{
	return new CAnimation2D(*this);
}
