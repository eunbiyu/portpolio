#include "ColliderTerrain.h"
#include "Transform.h"
#include "ColliderRay.h"

AR3D_USING

CColliderTerrain::CColliderTerrain()
{
	m_eCollGroup = CG_TERRAIN;
	m_eCollType = CT_TERRAIN_COLL;
	SetTag("ColliderTerrain");
	SetTypeName("CColliderTerrain");
	SetTypeID<CColliderTerrain>();
}

CColliderTerrain::CColliderTerrain(const CColliderTerrain & collider) :
	CCollider(collider)
{
	m_tInfo = collider.m_tInfo;
}


CColliderTerrain::~CColliderTerrain()
{
}

TERRAINCOLLINFO CColliderTerrain::GetInfo() const
{
	return m_tInfo;
}

void CColliderTerrain::SetInfo(const vector<DxVector3>& vecPos, unsigned int iNumW, unsigned int iNumH, unsigned int iSizeW, 
	unsigned int iSizeH)
{
	m_tInfo.vecPos = vecPos;
	m_tInfo.iNumW = iNumW;
	m_tInfo.iNumH = iNumH;
	m_tInfo.iSizeH = iSizeH;
	m_tInfo.iSizeW = iSizeW;
}

void CColliderTerrain::SetPickPosInfo(const DxVector3 vPos)
{
	m_PickPos = vPos;
}


DxVector3 CColliderTerrain::GetPickInfo(void)
{
	return m_PickPos;
}

bool CColliderTerrain::Init()
{
	return true;
}

void CColliderTerrain::Input(float fTime)
{
}

void CColliderTerrain::Update(float fTime)
{
}

void CColliderTerrain::LateUpdate(float fTime)
{
}

void CColliderTerrain::Collision(float fTime)
{
}

void CColliderTerrain::Render(float fTime)
{
}

CColliderTerrain * CColliderTerrain::Clone()
{
	return nullptr;
}

bool CColliderTerrain::Collision(CCollider * pCollider)
{
	switch (pCollider->GetColliderType())
	{
	case CT_SPHERE:
	{
		CTransform* pTransform = pCollider->GetTransform();
		bool bReturn = CollisionTerrainToPosition(m_tInfo, pTransform, m_pTransform->GetWorldScale());
		SAFE_RELEASE(pTransform);
		return bReturn;
	}

	}
}
