#include "UI.h"

AR3D_USING

CUI::CUI()
{
	SetTag("ui");
	SetTypeName("CUI");
	SetTypeID<CUI>();
	m_eComType = CT_UI;
}

CUI::CUI(const CUI & ui)
: CComponent(ui)
{
	m_eUIType = ui.m_eUIType;
}


CUI::~CUI()
{
}

UI_TYPE CUI::GetUIType() const
{
	return m_eUIType;
}

void CUI::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CUI::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CUI::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}
