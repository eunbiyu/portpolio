#include "Camera.h"
#include "Transform.h"
#include "../Core.h"
#include "../GameObject/GameObject.h"
#include "CameraArm.h"
#include "FreeCamera.h"
#include "../Core/Input.h"
#include "../Component/Renderer.h"
#include "../Resources/Mesh.h"

AR3D_USING

CCamera::CCamera() :
	m_matView(NULL),
	m_matProj(NULL),
	m_pAttachObj(NULL),
	m_pAttachTr(NULL), 
	inTarget(false)
{
	SetTypeName("CCamera");
	SetTypeID<CCamera>();
	m_eComType = CT_CAMERA;
}

CCamera::CCamera(const CCamera & camera) :
	CComponent(camera)
{
	m_matView = new MATRIX;
	m_matProj = new MATRIX;

	*m_matView = *camera.m_matView;
	*m_matProj = *camera.m_matProj;
}

CCamera::~CCamera()
{
	SAFE_RELEASE(m_pAttachTr);
	SAFE_RELEASE(m_pAttachObj);
	SAFE_DELETE(m_matView);
	SAFE_DELETE(m_matProj);
}

DxVector3 CCamera::GetAxis(AXIS axis) const
{
	return m_vAxis[axis];
}

void CCamera::SetTargetObject(CGameObject * Target)
{
	m_pTargetObj = Target;
}

CGameObject * CCamera::GetTargetObject()
{
	return m_pTargetObj;
}

void CCamera::SetAxis(DxVector3 vAxis[AXIS_MAX])
{
	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vAxis[i] = vAxis[i];
	}

	ComputeViewMatrix();
}

void CCamera::SetAxis(const DxVector3 & vAxis, AXIS axis)
{
	m_vAxis[axis] = vAxis;

	ComputeViewMatrix();
}

void CCamera::Attach(CGameObject * pObj, const DxVector3& vDist)
{
	SAFE_RELEASE(m_pAttachTr);
	SAFE_RELEASE(m_pAttachObj);
	pObj->AddRef();
	m_pAttachObj = pObj;

	m_pAttachTr = pObj->GetTransform();

	m_vPrevPos = m_pAttachTr->GetWorldPos();

	// 거리를 이용한 카메라의 위치를 구해준다.
	m_pTransform->SetWorldPos(vDist + m_vPrevPos);

	// 대상을 바라보게 회전시킨다.
	m_pTransform->LookAt(m_pAttachTr);

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vAxis[i] = m_pTransform->GetWorldAxis((AXIS)i);
	}
}

void CCamera::Dettach()
{
	SAFE_RELEASE(m_pAttachTr);
	SAFE_RELEASE(m_pAttachObj);
}

CGameObject * CCamera::GetAttachObject()
{
	if (m_pAttachObj)
		m_pAttachObj->AddRef();

	return m_pAttachObj;
}

XMMATRIX CCamera::GetViewMatrix() const
{
	return m_matView->mat;
}

XMMATRIX CCamera::GetProjMatrix() const
{
	return m_matProj->mat;
}

void CCamera::CreateProjection(float fAngle, float fWidth, float fHeight, float fNear,
	float fFar)
{
	*m_matProj = XMMatrixPerspectiveFovLH(fAngle, fWidth / fHeight, fNear, fFar);
}

void CCamera::CreateOrthoProjection(float fWidth, float fHeight, float fNear, float fFar)
{
	*m_matProj = XMMatrixOrthographicOffCenterLH(0.f, fWidth, fHeight, 0.f,
		fNear, fFar);
}

void CCamera::ComputeViewMatrix()
{
	*m_matView = m_matView->Identity();

	DxVector3	vPos = m_pTransform->GetWorldPos();

	DxVector3	vAxis[AXIS_MAX];
	bool	bCheckArm = m_pGameObject->CheckComponentFromTypeID<CCameraArm>();

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		if (!bCheckArm)
		{
			vAxis[i] = m_pTransform->GetWorldAxis((AXIS)i);
			m_vAxis[i] = vAxis[i];
		}

		else
			vAxis[i] = m_vAxis[i];


		memcpy(&m_matView->m[i][0], &vAxis[i], sizeof(DxVector3));
	}

	*m_matView = m_matView->Transpose();

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_matView->m[3][i] = -vPos.Dot(vAxis[i]);
	}
}

bool CCamera::Init()
{
	m_matView = new MATRIX;
	m_matProj = new MATRIX;

	CreateProjection(AR3D_PI / 2.f, _RESOLUTION.iWidth, _RESOLUTION.iHeight);

	for (int i = 0; i < AXIS_MAX; ++i)
	{
		m_vAxis[i] = WORLDAXIS[i];
	}
	GET_SINGLE(CInput)->CreateKey("TargetNUL", 'A');
	inTarget = true;

	return true;
}

void CCamera::Input(float fTime)
{

}

void CCamera::Update(float fTime)
{
	// 붙어있는 오브젝트가 움직였을 경우 이동시켜준다.
	if (m_pAttachObj)
	{
		DxVector3	vAttachPos = m_pAttachTr->GetWorldPos();
		DxVector3	vMove = vAttachPos - m_vPrevPos;

		m_pTransform->Move(vMove);

		m_vPrevPos = vAttachPos;
	}
	
	// 타겟이 있을 때 이동시켜준다. 
	if (m_pTargetObj)
	{
		if (inTarget)
		{
			DxVector3 Dir = m_pTargetObj->GetTransform()->GetWorldAxis(AXIS_Z) ;
			DxVector3 Pos = m_pTargetObj->GetTransform()->GetWorldPos() + Dir* 20.f;
			m_pTransform->SetWorldPos(Pos.x, Pos.y + 15.f , Pos.z + 40.f);
			m_pTransform->SetLookAt(m_pTargetObj);
		}


		inTarget = false;

		DxVector3	vTargetPos = m_pTargetObj->GetTransform()->GetWorldPos();

		vTargetPos.y += 10.f;

		DxVector3	vMyPos = m_pTransform->GetWorldPos();

		DxVector3	vMoveDir = vTargetPos - vMyPos ;

		vMoveDir = XMVector3Normalize(vMoveDir.Convert());

		float distance = vTargetPos.Distance(vMyPos);

		distance = abs(distance);
		CRenderer* pRenderer = (CRenderer*)m_pTargetObj->FindComponentFromTag("Renderer");
		float sizeZ = pRenderer->GetMesh()->GetMeshSize().z * 0.2;
		if (distance >  30.f)
		{
			m_pTransform->Move(vMoveDir, distance, fTime);
			//m_pTransform->LookAt(m_pTargetObj);

			m_pTransform->SetLookAt(m_pTargetObj);
		}

		else
		{
			m_pTargetObj = NULL;
			inTarget = true;
		}
		
	}
}

void CCamera::LateUpdate(float fTime)
{
	ComputeViewMatrix();
}

void CCamera::Collision(float fTime)
{
}

void CCamera::Render(float fTime)
{
}

CCamera * CCamera::Clone()
{
	return new CCamera(*this);
}
