#pragma once
#include "Component.h"

AR3D_BEGIN

class CTexture;

class DLL CTerrain :
	public CComponent
{
private:
	friend class CGameObject;

private:
	CTerrain();
	CTerrain(const CTerrain& terrain);
	~CTerrain();

private:
	unsigned int	m_iVtxNumX;
	unsigned int	m_iVtxNumZ;
	unsigned int	m_iVtxSizeX;
	unsigned int	m_iVtxSizeZ;
	vector<DxVector3>	m_vecPos;
	float			m_fDetailLevel;
	TERRAINCBUFFER	m_tTerrainCBuffer;
	vector<DxVector3>	m_vecFaceNormal;
	string				m_strKey;
	vector<VERTEXBUMP>	m_vecVtx;
	CTexture*			m_Texture[MAXTEXNUM];				//스프레팅텍스쳐 객체
	int					m_iNowSplatNumInTool;				//현재 수정중인 스플래팅 카운트 

	//툴용 
private:
	bool m_bTool; // 맵툴인지 아닌지 
	PICKTERRAINCBUFFER	m_tPickTerrainCBuffer;

public:
	void ToolOn(bool bTool);
	void SetPickRadInfo(float fOuterRad, float fInnerRad);
	PICKTERRAINCBUFFER GetPickRadInfo(void);

public:
	bool CreateTerrain(const string& strKey, unsigned int iVtxNumX, unsigned int iVtxNumZ,
		unsigned int iVtxSizeX, unsigned int iVtxSizeZ,
		char* pHeightMap = NULL, const string& strPathKey = TEXTURE_PATH);

	void SetBaseTexture(const string& strKey, TCHAR* pFileName,
		const string& strPathKey = TEXTURE_PATH);
	void SetNormalTexture(const string& strKey, TCHAR* pFileName,
		const string& strPathKey = TEXTURE_PATH);
	void SetSpecularTexture(const string& strKey, TCHAR* pFileName,
		const string& strPathKey = TEXTURE_PATH);
	// 스플래팅 관련 함수 
	void SetSplatTexture(const string& strKey, vector<wstring> vecFileName,
		const string& strPathKey = TEXTURE_PATH);
	void SetSplatNormalTexture(const string& strKey, vector<wstring> vecFileName,
		const string& strPathKey = TEXTURE_PATH);
	void SetSplatSpecularTexture(const string& strKey, vector<wstring> vecFileName,
		const string& strPathKey = TEXTURE_PATH);
	void SetSplatAlphaTexture(const string& strKey, vector<wstring> vecFileName,
		const string& strPathKey = TEXTURE_PATH);

	void SetSplatCount(int iCount);
	void SetDetailLevel(float fLevel);
	void UpdateTerrainTexture(void);

public:
	vector<DxVector3> GetVecPos();
	float GetHeight(int idxX, int idxZ);
	void SetNowSetSplatNum(int iNowSplatNumInTool);
	int GetNowSetSplatNum();

private:
	void ComputeNormal(vector<VERTEXBUMP>& vecVtx, const vector<UINT>& vecIndex);
	void ComputeTangent(vector<VERTEXBUMP>& vecVtx, const vector<UINT>& vecIndex);


public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CTerrain* Clone();
	virtual void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);

};

AR3D_END