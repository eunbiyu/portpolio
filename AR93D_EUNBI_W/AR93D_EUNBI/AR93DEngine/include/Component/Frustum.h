#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CFrustum :
	public CComponent
{
private:
	friend class CGameObject;

private:
	CFrustum();
	CFrustum(const CFrustum& frustum);
	~CFrustum();

private:
	DxVector3		m_vPos[8];
	DxVector4		m_Plane[FP_END];

public:
	bool FrustumInPoint(const DxVector3& vPos);
	bool FrustumInSphere(const SPHEREINFO& tSphere);
	bool FrustumInSphere(const DxVector3& vCenter, float fRadius);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CFrustum* Clone();
};

AR3D_END
