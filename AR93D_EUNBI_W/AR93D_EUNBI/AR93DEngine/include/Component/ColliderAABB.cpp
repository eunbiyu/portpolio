#include "ColliderAABB.h"
#include "Transform.h"
#include "Camera.h"
#include "../Scene/Scene.h"
#include "ColliderRay.h"
#include "ColliderTerrain.h"
#include "../GameObject/GameObject.h"
#include "../Core/Input.h"

AR3D_USING

CColliderAABB::CColliderAABB()
{
	m_eCollGroup = CG_NORMAL;
	m_eCollType = CT_AABB;
	SetTag("ColliderAABB");
	SetTypeName("CColliderAABB");
	SetTypeID<CColliderAABB>();
}


CColliderAABB::CColliderAABB(const CColliderAABB & collider)
	:CColliderAABB (collider)
{
	m_tInfo = collider.m_tInfo;
}

CColliderAABB::~CColliderAABB()
{
}

void CColliderAABB::SetSphereInfo(const DxVector3 & vCenter, float fX, float fY, float fZ)
{
	m_tInfo.vCenter = vCenter;
	m_tInfo.fXSize = fX;
	m_tInfo.fYSize = fY;
	m_tInfo.fZSize = fZ;

	m_vPrevPos = m_pTransform->GetWorldPos();
}


void CColliderAABB::SetSphereInfo(const AABBBOXINFO & tSphere)
{
	m_tInfo = tSphere;

	m_vPrevPos = m_pTransform->GetWorldPos();
}


bool CColliderAABB::Init()
{
	return true;
}

void CColliderAABB::Input(float fTime)
{
}

void CColliderAABB::Update(float fTime)
{
	CCollider::Update(fTime);

	m_tInfo.vCenter += m_vMove;
}

void CColliderAABB::LateUpdate(float fTime)
{
}

void CColliderAABB::Collision(float fTime)
{
}

void CColliderAABB::Render(float fTime)
{

	//#ifdef _DEBUG
	// Transform ����

	CCamera*	pCamera = m_pScene->GetMainCamera();

	XMMATRIX	matScale, matPos;
	matScale = XMMatrixScaling(m_tInfo.fXSize, m_tInfo.fYSize * 2.f,
		m_tInfo.fZSize * 2.f);
	matPos = XMMatrixTranslation(m_tInfo.vCenter.x, m_tInfo.vCenter.y,
		m_tInfo.vCenter.z);
	m_tTCBuffer.matWorld = matScale * matPos;
	m_tTCBuffer.matView = pCamera->GetViewMatrix();
	m_tTCBuffer.matProj = pCamera->GetProjMatrix();
	m_tTCBuffer.matWV = m_tTCBuffer.matWorld * m_tTCBuffer.matView;
	m_tTCBuffer.matWVP = m_tTCBuffer.matWV * m_tTCBuffer.matProj;

	SAFE_RELEASE(pCamera);

	m_tTCBuffer.matWorld = XMMatrixTranspose(m_tTCBuffer.matWorld);
	m_tTCBuffer.matView = XMMatrixTranspose(m_tTCBuffer.matView);
	m_tTCBuffer.matProj = XMMatrixTranspose(m_tTCBuffer.matProj);
	m_tTCBuffer.matWV = XMMatrixTranspose(m_tTCBuffer.matWV);
	m_tTCBuffer.matWVP = XMMatrixTranspose(m_tTCBuffer.matWVP);

	//#endif // _DEBUG

	CCollider::Render(fTime);
}


CColliderAABB * CColliderAABB::Clone()
{
	return new CColliderAABB(*this);
}

bool CColliderAABB::Collision(CCollider * pCollider)
{
	switch (pCollider->GetColliderType())
	{
	case CT_AABB:
		return CollisionAABBToAABB(m_tInfo, ((CColliderAABB*)pCollider)->m_tInfo);

	case CT_TERRAIN_COLL:
		CTransform*	pTransform = pCollider->GetTransform();
		bool bReturn = CollisionTerrainToPosition(((CColliderTerrain*)pCollider)->GetInfo(),
			m_pTransform, pTransform->GetWorldScale());
		SAFE_RELEASE(pTransform);
		return bReturn;

	//case CT_RAY:
	//	if (KEYPRESS("MouseLButton") || KEYPUSH("MouseLButton"))
	//	{
	//		return CollisionRayToSphere(((CColliderRay*)pCollider)->GetRay(),
	//			m_tInfo);
	//	}
	}

	return false;
}
