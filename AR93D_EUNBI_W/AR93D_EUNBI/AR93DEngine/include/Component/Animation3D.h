#pragma once
#include "Component.h"
#include "../Resources/FbxLoader.h"
#include "Animation3DClip.h"

AR3D_BEGIN

typedef struct DLL _tagBone
{
	string		strName;
	int			iDepth;
	int			iParentIndex;
	PMATRIX		matOffset;
	PMATRIX		matBone;
	//vector<PKEYFRAME>	vecKeyFrame;
}BONE, *PBONE;

class DLL CAnimation3D :
	public CComponent
{
private:
	friend class CGameObject;
	friend class CMesh;

private:
	CAnimation3D();
	CAnimation3D(const CAnimation3D& anim);
	~CAnimation3D();

private:
	vector<PBONE>		m_vecBones;
	class CTexture*		m_pBoneTex;
	unordered_map<string, class CAnimation3DClip*>	m_mapClip;
	int		m_iAnimationLimitFrame;
	class CAnimation3DClip*	m_pLastInsertClip;
	class CAnimation3DClip*	m_pCurClip;
	class CAnimation3DClip*	m_pNextClip;
	string	m_strDefaultClip;
	string	m_strCurClip;
	string	m_strNextClip;
	bool	m_bChange;
	float	m_fChangeTime;
	float	m_fChangeLimitTime;
	float	m_fAnimationTime;
	float	m_fFrameTime;
	float	m_fAnimationProgress;
	bool	m_bEnd;

public:
	const unordered_map<string, class CAnimation3DClip*>* GetClips()	const;
	class CAnimation3DClip* GetLastInsertClip()	const;
	bool GetAnimationEnd()	const;
	float GetAnimationProgress()	const;
	int GetAnimationProgressFrame()	const;
	string GetCurrentClipName()	const;
	int GetCurrentClipStartFrame()	const;
	int GetCurrentClipEndFrame()	const;
	ANIMATION_OPTION GetCurrentClipOption()	const;
	int GetNextClipStartFrame()	const;
	int GetNextClipEndFrame()	const;
	ANIMATION_OPTION GetNextClipOption()	const;
	PBONE FindBone(const string& strName)	const;

public:
	bool LoadFbxClip(const wchar_t* pFileName, const string& strPathKey = MESH_PATH);
	bool LoadFbxClip(const char* pFileName, const string& strPathKey = MESH_PATH);
	bool LoadFbxClipFromFullPath(const wchar_t* pFileName);
	bool LoadFbxClipFromFullPath(const char* pFileName);
	void AddClipCallback(const string& strName, int iFrame, void(*pFunc)(float));
	template <typename T>
	void AddClipCallback(const string& strName, int iFrame, T* pObj, void(T::*pFunc)(float))
	{
		CAnimation3DClip*	pClip = FindClip(strName);

		if (!pClip)
			return;

		pClip->AddCallback(iFrame, pObj, pFunc);
	}

	void AddClipCallback(const string& strName, float fProgress, void(*pFunc)(float));
	template <typename T>
	void AddClipCallback(const string& strName, float fProgress, T* pObj, void(T::*pFunc)(float))
	{
		CAnimation3DClip*	pClip = FindClip(strName);

		if (!pClip)
			return;

		pClip->AddCallback(fProgress, pObj, pFunc);
	}

public:
	void ReturnToDefaultClip();
	void ClearClip();
	class CTexture* GetBoneTexture()	const;
	void SetChangeTime(float fTime);
	void AddBone(PBONE pBone);
	void AddClip(ANIMATION_OPTION eOption,
		PFBXANIMATIONCLIP pClip);
	void AddClip(const string& strKey, ANIMATION_OPTION eOption,
		int iAnimationLimitFrame, int iStartFrame, int iEndFrame,
		float fStartTime, float fEndTime);
	void AddClip(const string& strKey, ANIMATION_OPTION eOption,
		int iStartFrame, int iEndFrame);
	void ChangeClipInfo(const string& strKey, ANIMATION_OPTION eOption,
		int iStartFrame, int iEndFrame);
	void ChangeClipInfo(const string& strKey, const string& strName, ANIMATION_OPTION eOption,
		int iStartFrame, int iEndFrame);
	class CAnimation3DClip* FindClip(const string& strKey);
	void SetDefaultClipName(const string& strName);
	void SetCurClipName(const string& strName);
	void ChangeClip(const string& strName);
	bool CreateBoneTexture();

	bool Save(const char* pFileName,
		const string& strPathKey = MESH_PATH);
	bool Save(FILE* pFile);
	bool SaveFromFullPath(const char* pFileName);
	bool SaveClipFromFullPath(const char* pFullPath);
	bool SaveClipFromFullPath(const string& strClipName, const char* pFullPath);
	bool SaveClip(FILE* pFile);
	bool SaveBoneFromFullPath(const char* pFullPath);
	bool Load(const char* pFileName, const string& strPathKey = MESH_PATH);
	bool Load(FILE* pFile);
	bool LoadFromFullPath(const char* pFileName);
	bool LoadClipFromFullPath(const char* pFullPath);
	bool LoadClipFromOne(const char* pFullPath);
	bool LoadClip(FILE* pFile);
	bool LoadBoneFromFullPath(const char* pFullPath);
	bool LoadBone(const char* pFileName, const string& strPathKey = MESH_PATH);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CAnimation3D* Clone();
};

AR3D_END
