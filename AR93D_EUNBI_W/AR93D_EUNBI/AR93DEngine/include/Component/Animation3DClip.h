#pragma once
#include "../BaseObj.h"
#include "../Resources/FbxLoader.h"

AR3D_BEGIN

typedef struct DLL _tagKeyFrame
{
	double	dTime;
	DxVector3	vPos;
	DxVector3	vScale;
	DxVector4	vRot;
}KEYFRAME, *PKEYFRAME;

typedef struct DLL _tagBoneKeyFrame
{
	int		iBoneIndex;
	vector<PKEYFRAME>	vecKeyFrame;
}BONEKEYFRAME, *PBONEKEYFRAME;

typedef struct DLL _tagAnimationCallback
{
	int		iAnimationProgress;
	float	fAnimationProgress;
	function<void(float)> func;
	bool	bCall;
}ANIMATIONCALLBACK, *PANIMATIONCALLBACK;

typedef struct DLL _tagAnimation3DClip
{
	ANIMATION_OPTION	eOption;
	string		strName;
	float		fStartTime;
	float		fEndTime;
	float		fTimeLength;
	int			iStartFrame;
	int			iEndFrame;
	int			iFrameLength;
	vector<PANIMATIONCALLBACK>	vecCallback;

	_tagAnimation3DClip() :
		eOption(AO_LOOP),
		strName(""),
		fStartTime(0),
		fEndTime(0),
		fTimeLength(0),
		iStartFrame(0),
		iEndFrame(0),
		iFrameLength(0)
	{
	}
}ANIMATION3DCLIP, *PANIMATION3DCLIP;

class DLL CAnimation3DClip :
	public CBaseObj
{
private:
	friend class CAnimation3D;

private:
	CAnimation3DClip();
	CAnimation3DClip(const CAnimation3DClip& clip);
	~CAnimation3DClip();

private:
	ANIMATION3DCLIP		m_tInfo;
	int		m_iAnimationLimitFrame;
	vector<PBONEKEYFRAME>	m_vecKeyFrame;

public:
	bool IsEmptyKeyFrame(int iBoneIndex)	const;
	PKEYFRAME GetKeyFrame(int iBoneIndex, int iFrameIndex)	const;
	ANIMATION3DCLIP GetInfo()	const;
	void AddCallback(int iFrame, void(*pFunc)(float));
	template <typename T>
	void AddCallback(int iFrame, T* pObj, void(T::*pFunc)(float))
	{
		PANIMATIONCALLBACK	pCallback = new ANIMATIONCALLBACK;

		pCallback->iAnimationProgress = iFrame;
		pCallback->fAnimationProgress = (iFrame - m_tInfo.iStartFrame) /
			(float)m_tInfo.iEndFrame;
		pCallback->func = bind(pFunc, pObj, placeholders::_1);
		pCallback->bCall = false;

		m_tInfo.vecCallback.push_back(pCallback);
	}

	void AddCallback(float fProgress, void(*pFunc)(float));
	template <typename T>
	void AddCallback(float fProgress, T* pObj, void(T::*pFunc)(float))
	{
		PANIMATIONCALLBACK	pCallback = new ANIMATIONCALLBACK;

		pCallback->iAnimationProgress = (fProgress * m_tInfo.fTimeLength + m_tInfo.fStartTime) *
			m_iAnimationLimitFrame;
		pCallback->fAnimationProgress = fProgress;
		pCallback->func = bind(pFunc, pObj, placeholders::_1);
		pCallback->bCall = false;

		m_tInfo.vecCallback.push_back(pCallback);
	}

public:
	void SetClipInfo(const string& strName, ANIMATION_OPTION eOption,
		int iAnimationLimitFrame, int iStartFrame, int iEndFrame,
		float fStartTime, float fEndTime);
	void SetClipInfo(ANIMATION_OPTION eOption, PFBXANIMATIONCLIP pClip);
	void Save(FILE* pFile);
	void Load(FILE* pFile);

public:
	bool Init();
	int Update(float fTime);
	int LateUpdate(float fTime);
	CAnimation3DClip* Clone();
};

AR3D_END
