#pragma once
#include "../BaseObj.h"

AR3D_BEGIN

class DLL CComponent :
	public CBaseObj
{
protected: 
	friend class CGameObject;

protected:
	CComponent();
	CComponent(const CComponent& com);
	virtual ~CComponent() = 0;

protected:
	class CScene*	m_pScene;
	class CLayer*	m_pLayer;
	class CGameObject*	m_pGameObject;
	class CTransform*	m_pTransform;
	COMPONENT_TYPE		m_eComType;

public:
	COMPONENT_TYPE GetComponentType() const;
	class CTransform* GetTransform() const;
	class CGameObject* GetGameObject() const;
	void SetComponentType(COMPONENT_TYPE ct) ;

public:
	void SetScene(class CScene* pScene);
	void SetLayer(class CLayer* pLayer);
	void SetGameObject(class CGameObject* pObj);

public:
	class CComponent* FindComponentFromTag(const string& strTag);
	class CComponent* FindComponentFromTypeName(const string& strTypeName);
	class CComponent* FindComponentFromType(COMPONENT_TYPE eType);
	bool CheckComponentFromTag(const string& strTag);
	bool CheckComponentFromTypeName(const string& strTypeName);
	bool CheckComponentFromType(COMPONENT_TYPE eType);
	template <typename T>
	bool CheckComponentFromTypeID()
	{
		return m_pGameObject->CheckComponentFromTypeID<T>();
	}

	list<class CComponent*>* FindComponentsFromTag(const string& strTag);
	list<class CComponent*>* FindComponentsFromTypeName(const string& strTypeName);
	list<class CComponent*>* FindComponentsFromType(COMPONENT_TYPE eType);

	template <typename T>
	T* FindComponentFromTypeID()
	{
		return m_pGameObject->FindComponentFromTypeID<T>();
	}

	template <typename T>
	list<class CComponent*>* FindComponentsFromTypeID()
	{
		return m_pGameObject->FindComponentsFromTypeID<T>();
	}


private:
	void SetTransform(class CTransform* pTransform);

public:
	//여기는 모두 순수가상함수로 만들어준다 . 
	virtual bool Init() = 0;
	virtual void Input(float fTime) = 0;
	virtual void Update(float fTime) = 0;
	virtual void LateUpdate(float fTime) = 0;
	virtual void Collision(float fTime) = 0;
	virtual void Render(float fTime) = 0;

	virtual CComponent* Clone() = 0;

	virtual void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
};

AR3D_END