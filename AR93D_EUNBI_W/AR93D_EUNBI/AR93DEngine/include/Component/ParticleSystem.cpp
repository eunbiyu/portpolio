#include "ParticleSystem.h"
#include "../Device.h"
#include "../Rendering/Shader.h"
#include "../Rendering/ShaderManager.h"
#include "../Resources/Texture.h"
#include "../Resources/ResourcesManager.h"
#include "../Rendering/RenderManager.h"
#include "../Rendering/RenderState.h"
#include "Transform.h"
#include "Camera.h"
#include "../Scene/Scene.h"
#include "LightPoint.h"

AR3D_USING

CParticleSystem::CParticleSystem() :
	m_pInitVB(NULL),
	m_pStreamVB(NULL),
	m_pDrawVB(NULL),
	m_pShader(NULL),
	m_pStreamShader(NULL),
	m_pTexture(NULL),
	m_pInputLayout(NULL),
	m_pAlphaBlend(NULL),
	m_pLightShader(NULL),
	m_pDepthWriteDisable(NULL),
	m_iMaxParticles(0),
	m_fParticleTime(0.f),
	m_fLightRange(1.5f),
	m_bFirstRun(true),
	m_bLight(false)
{
	SetTag("ParticleSystem");
	SetTypeName("CParticleSystem");
	SetTypeID<CParticleSystem>();
	m_eComType = CT_PARTICLE;

	SetShader(PARTICLE_SHADER);
	SetStreamShader(PARTICLE_STREAMOUT_SHADER);
	SetInputLayout("Particle");

	m_pLightShader = GET_SINGLE(CShaderManager)->FindShader(PARTICLE_LIGHT_SHADER);

	m_pDepthDisable = GET_SINGLE(CRenderManager)->FindRenderState(DEPTH_DISABLE);
	m_pAlphaBlend = GET_SINGLE(CRenderManager)->FindRenderState(ALPHABLEND);
}

CParticleSystem::CParticleSystem(const CParticleSystem & particle) :
	CComponent(particle)
{
	*this = particle;
	m_fParticleTime = 0.f;
	m_bFirstRun = true;
	SetParticleInfo(particle.m_iMaxParticles);

	m_pShader = particle.m_pShader;

	if (m_pShader)
		m_pShader->AddRef();

	if (m_pStreamShader)
		m_pStreamShader->AddRef();

	if (m_pTexture)
		m_pTexture->AddRef();

	if (m_pDepthDisable)
		m_pDepthDisable->AddRef();

	if (m_pAlphaBlend)
		m_pAlphaBlend->AddRef();

	if (m_pLightShader)
		m_pLightShader->AddRef();

	if (m_pDepthWriteDisable)
		m_pDepthWriteDisable->AddRef();

	m_pInputLayout = particle.m_pInputLayout;
}

CParticleSystem::~CParticleSystem()
{
	SAFE_RELEASE(m_pDepthWriteDisable);
	SAFE_RELEASE(m_pLightShader);
	SAFE_RELEASE(m_pAlphaBlend);
	SAFE_RELEASE(m_pDepthDisable);
	SAFE_RELEASE(m_pTexture);
	SAFE_RELEASE(m_pStreamShader);
	SAFE_RELEASE(m_pShader);

	SAFE_RELEASE(m_pInitVB);
	SAFE_RELEASE(m_pStreamVB);
	SAFE_RELEASE(m_pDrawVB);
}

bool CParticleSystem::GetParticleLight() const
{
	return m_bLight;
}

void CParticleSystem::SetLightRange(float fRange)
{
	m_fLightRange = fRange;
}

void CParticleSystem::SetShader(const string & strKey)
{
	SAFE_RELEASE(m_pShader);
	m_pShader = GET_SINGLE(CShaderManager)->FindShader(strKey);
}

void CParticleSystem::SetStreamShader(const string & strKey)
{
	SAFE_RELEASE(m_pStreamShader);
	m_pStreamShader = GET_SINGLE(CShaderManager)->FindShader(strKey);

	//m_pInputLayout = GET_SINGLE(CShaderManager)->FindInputLayout("Particle");
}

void CParticleSystem::SetInputLayout(const string & strKey)
{
	m_pInputLayout = GET_SINGLE(CShaderManager)->FindInputLayout(strKey);
}

void CParticleSystem::SetParticleInfo(int iMaxParticles)
{
	// 
	m_iMaxParticles = iMaxParticles;

	D3D11_BUFFER_DESC	tDesc = {};

	tDesc.Usage = D3D11_USAGE_DEFAULT;
	tDesc.ByteWidth = sizeof(VERTEXPARTICLE);
	tDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	VERTEXPARTICLE	vtx = {};
	vtx.fCreateTime = 0.05f;
	vtx.fLifeTime = 2.5f;
	vtx.vSize = DxVector2(1.5f, 1.5f);
	vtx.fLightRange = 1.5f;

	D3D11_SUBRESOURCE_DATA	tData = {};
	tData.pSysMem = &vtx;

	if (FAILED(DEVICE->CreateBuffer(&tDesc, &tData, &m_pInitVB)))
		return;

	tDesc.ByteWidth = sizeof(VERTEXPARTICLE) * iMaxParticles;
	tDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER | D3D11_BIND_STREAM_OUTPUT;

	if (FAILED(DEVICE->CreateBuffer(&tDesc, NULL, &m_pStreamVB)))
		return;

	if (FAILED(DEVICE->CreateBuffer(&tDesc, NULL, &m_pDrawVB)))
		return;
}

void CParticleSystem::SetParticleTexture(const string & strKey, TCHAR * pFileName, const string & strPathKey)
{
	SAFE_RELEASE(m_pTexture);
	m_pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(strKey, pFileName, strPathKey);
}

void CParticleSystem::SetParticleLight(bool bParticleLight)
{
	m_bLight = bParticleLight;

	SAFE_RELEASE(m_pDepthWriteDisable);
	if (bParticleLight)
	{
		m_pDepthWriteDisable = GET_SINGLE(CRenderManager)->FindRenderState(DEPTH_WRITE_DISABLE);
	}
}

float CParticleSystem::GetParticleTime()
{
	return m_fTotalTime;
}

bool CParticleSystem::Init()
{
	m_bFirstRun = true;

	return true;
}

void CParticleSystem::Input(float fTime)
{
}

void CParticleSystem::Update(float fTime)
{
	
}

void CParticleSystem::LateUpdate(float fTime)
{
}

void CParticleSystem::Collision(float fTime)
{
}

void CParticleSystem::Render(float fTime)
{
	
		PARTICLECBUFFER	tCBuffer = {};

		tCBuffer.vPos = m_pTransform->GetWorldPos();
		tCBuffer.fDeltaTime = fTime;

		CCamera*	pCamera = m_pScene->GetMainCamera();
		CTransform*	pCameraTr = pCamera->GetTransform();

		tCBuffer.vCamAxisY = pCamera->GetAxis(AXIS_Y);
		tCBuffer.vCamAxisX = pCamera->GetAxis(AXIS_X) * -1.f;
		tCBuffer.fCreateTime = 0.05f;
		tCBuffer.fSpeed = 2.f;
		tCBuffer.fGameTime = m_fParticleTime;
		tCBuffer.vCreateDir = DxVector3((rand() % 10001 - 5000) / 5000.f,
			(rand() % 10001 - 5000) / 5000.f,
			(rand() % 10001 - 5000) / 5000.f).Normalize();

		TRANSFORMCBUFFER	tTransform;

		tTransform.matVP = pCamera->GetViewMatrix() * pCamera->GetProjMatrix();

		tTransform.matVP = XMMatrixTranspose(tTransform.matVP);

		GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Transform", &tTransform, CUT_GEOMETRY);

		SAFE_RELEASE(pCameraTr);
		SAFE_RELEASE(pCamera);

		GET_SINGLE(CShaderManager)->UpdateConstantBuffer("ParticleCBuffer",
			&tCBuffer, CUT_GEOMETRY);

		CONTEXT->IASetInputLayout(m_pInputLayout);
		CONTEXT->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

		UINT	iStride = sizeof(VERTEXPARTICLE);
		UINT	iOffset = 0;

		if (!m_bLight)
		{
			m_fParticleTime += fTime;

			// 스트림 버퍼에 출력한다.(셰이더에서 새로운 파티클들을 생성시켜준다.)
			// 스트림 버퍼 출력시에는 깊이를 끈다.
			m_pDepthDisable->SetState();

			if (m_bFirstRun)
				CONTEXT->IASetVertexBuffers(0, 1, &m_pInitVB, &iStride, &iOffset);
			else
				CONTEXT->IASetVertexBuffers(0, 1, &m_pDrawVB, &iStride, &iOffset);

			m_pStreamShader->SetShader();

			// Stream Buffer를 지정한다.
			CONTEXT->SOSetTargets(1, &m_pStreamVB, &iOffset);

			if (m_bFirstRun)
			{
				CONTEXT->Draw(1, 0);
				m_bFirstRun = false;
			}
			else
				CONTEXT->DrawAuto();

			// 스트림 출력으로 정점 들을 정점버퍼에 모두 기록한 다음에는, 
			// 그 정점들이 정의하는 기본 도형들을 실제로 렌더링에 사용해야한다. 
			// 그런데, 하나의 정점 버퍼를 SO와 IA단계에 모두 묶을수는 없다,
			// 따라서 스트림출력단계에서 정점버퍼를 떼어내야한다. 
			// 어떤 버퍼(NULL)를 정점버퍼가 묶인 슬롯에 다시 묶어준다.

			ID3D11Buffer*	pBuf = NULL;
			CONTEXT->SOSetTargets(1, &pBuf, &iOffset);

			m_pDepthDisable->ResetState();

			// 버퍼 ping-pong
			// 정점버퍼를 OM와 IA단계에 동시에 묶어둘 수는 없다.
			// 이런 상황에서는 두 개의 버퍼를 핑퐁방식으로 갱신하는 기법이 유용하다.
			// 스트림 출력으로 기본 도형들을 정점 버퍼에 기록할 때, 
			// 한 정점 버퍼를 입력대상으로 사용하고 다른 정점버퍼를 출력대상으로 사용한다.
			// 그 다음번의 렌더링 프레임에서는 두 버퍼의 역할을 맞바꾼다.
			// 즉, 방금 스트림 출력이 기록된 정점 버퍼가 새 입력버퍼가 되고, 
			// 기존의 입력 버퍼는 새로운 스트림 출력 대상이 되는 것이다. 
			swap(m_pDrawVB, m_pStreamVB);
		}

		// 실제 출력한다.
		/*GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Transform", &tTransform, CUT_GEOMETRY);
		GET_SINGLE(CShaderManager)->UpdateConstantBuffer("ParticleCBuffer",
		&tCBuffer, CUT_GEOMETRY);*/

		m_pShader->SetShader();

		m_pAlphaBlend->SetState();

		if (m_pTexture)
			m_pTexture->SetTexture(0, CUT_PIXEL);

		CONTEXT->IASetInputLayout(m_pInputLayout);
		CONTEXT->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

		// 방금 스트림 출력된 갱신된 입자시스템을 화면에 그린다. 
		CONTEXT->IASetVertexBuffers(0, 1, &m_pDrawVB, &iStride, &iOffset);

		//자동그리기
		//GS가 스트림으로 정점버퍼에 기록한 기하구조가 매 프레임마다 다를수도있다.
		//심지어 정점들의 개수도 다를 수 있다. 그런 가변적인 정점들을 어떻게그릴까?
		//다행히 D3D는 내부적으로 정점 개수를 관리한다.
		//DrawAuto를 이용하면 스트림출력을 통해 정점버퍼에 기록된 기하구조를 쉽게 그릴수있다.

		//스트림출력된 정점버퍼를 DrawAuto를 이용해 자동으로 그릴 때에도 
		//정점버퍼의 정점들의 입력배치는 여전히 명시적으로 해주어야한다. 

		//DrawAuto메서드는 색인을 사용하지 않으므로, GS는 기본도형들 전부를 
		//정점목록형태로 출력해야한다. 
		CONTEXT->DrawAuto();

		m_pAlphaBlend->ResetState();
}

CParticleSystem * CParticleSystem::Clone()
{
	return new CParticleSystem(*this);
}

void CParticleSystem::RenderLight(float fTime)
{
	PARTICLECBUFFER	tCBuffer = {};

	tCBuffer.vPos = m_pTransform->GetWorldPos();
	tCBuffer.fDeltaTime = fTime;

	CCamera*	pCamera = m_pScene->GetMainCamera();
	CTransform*	pCameraTr = pCamera->GetTransform();

	tCBuffer.vCamAxisY = pCamera->GetAxis(AXIS_Y);
	tCBuffer.vCamAxisX = pCamera->GetAxis(AXIS_X) * -1.f;
	tCBuffer.fCreateTime = 0.1f;
	tCBuffer.fSpeed = 5.f;
	tCBuffer.fGameTime = m_fParticleTime;
	tCBuffer.vCreateDir = DxVector3((rand() % 10001 - 5000) / 5000.f,
		(rand() % 10001 - 5000) / 5000.f,
		(rand() % 10001 - 5000) / 5000.f).Normalize();

	TRANSFORMCBUFFER	tTransform;

	tTransform.matView = pCamera->GetViewMatrix();
	tTransform.matVP = pCamera->GetViewMatrix() * pCamera->GetProjMatrix();
	tTransform.matProj = pCamera->GetProjMatrix();
	tTransform.matInvProj = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matProj),
		tTransform.matProj);

	tTransform.matView = XMMatrixTranspose(tTransform.matView);
	tTransform.matProj = XMMatrixTranspose(tTransform.matProj);
	tTransform.matInvProj = XMMatrixTranspose(tTransform.matInvProj);
	tTransform.matVP = XMMatrixTranspose(tTransform.matVP);

	GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Transform", &tTransform, CUT_GEOMETRY);
	SAFE_RELEASE(pCameraTr);
	SAFE_RELEASE(pCamera);

	GET_SINGLE(CShaderManager)->UpdateConstantBuffer("ParticleCBuffer",
		&tCBuffer, CUT_GEOMETRY);

	CONTEXT->IASetInputLayout(m_pInputLayout);
	CONTEXT->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	UINT	iStride = sizeof(VERTEXPARTICLE);
	UINT	iOffset = 0;

	m_fParticleTime += fTime;
	m_fTotalTime = m_fParticleTime;

	// 스트림 버퍼에 출력한다.(셰이더에서 새로운 파티클들을 생성시켜준다.)
	// 스트림 버퍼 출력시에는 깊이를 끈다.
	m_pDepthDisable->SetState();

	if (m_bFirstRun)
		CONTEXT->IASetVertexBuffers(0, 1, &m_pInitVB, &iStride, &iOffset);

	else
		CONTEXT->IASetVertexBuffers(0, 1, &m_pDrawVB, &iStride, &iOffset);

	m_pStreamShader->SetShader();

	// Stream Buffer를 지정한다.
	CONTEXT->SOSetTargets(1, &m_pStreamVB, &iOffset);

	if (m_bFirstRun)
	{
		CONTEXT->Draw(1, 0);
		m_bFirstRun = false;
	}

	else
		CONTEXT->DrawAuto();

	m_pDepthDisable->ResetState();


	//버퍼 떼어내주기 
	ID3D11Buffer*	pBuf = NULL;

	CONTEXT->SOSetTargets(1, &pBuf, &iOffset);

	// 버퍼 ping-pong
	swap(m_pDrawVB, m_pStreamVB);

	m_pLightShader->SetShader();

	m_pDepthWriteDisable->SetState();

	CONTEXT->IASetInputLayout(m_pInputLayout);
	CONTEXT->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	CONTEXT->IASetVertexBuffers(0, 1, &m_pDrawVB, &iStride, &iOffset);

	CONTEXT->DrawAuto();

	m_pDepthWriteDisable->ResetState();
}
