#include "Effect.h"
#include "Transform.h"
#include "Camera.h"
#include "../Scene/Scene.h"
#include "../Rendering/ShaderManager.h"
#include "../GameObject/GameObject.h"
#include "Renderer.h"
#include "Renderer2D.h"

AR3D_USING

CEffect::CEffect()
{
	SetTag("Effect");
	SetTypeName("CEffect");
	SetTypeID<CEffect>();
	m_eComType = CT_EFFECT;
}

CEffect::CEffect(const CEffect & effect)
	:CComponent(effect)
{
}


CEffect::~CEffect()
{
}

void CEffect::SetEffectCBuffer()
{
	GET_SINGLE(CShaderManager)->UpdateConstantBuffer("BillBoard",
		&m_tEffectCBuffer, CUT_VERTEX | CUT_GEOMETRY);

	/*DxVector4	vUV[2]	=
	{
	DxVector4(0.f, 1.f, 0.f, 0.f),
	DxVector4(1.f, 1.f, 1.f, 0.f)
	};

	GET_SINGLE(CShaderManager)->UpdateConstantBuffer("AnimationUV", vUV, CUT_GEOMETRY);*/
}

bool CEffect::Init()
{
	if (CheckComponentFromTypeID<CRenderer>())
	{
		CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

		pRenderer->AddConstantBuffer("BillBoard", 11, sizeof(EFFECTCBUFFER),
			CUT_VERTEX | CUT_GEOMETRY);

		SAFE_RELEASE(pRenderer);
	}

	else if (CheckComponentFromTypeID<CRenderer2D>())
	{
		CRenderer2D*	pRenderer = FindComponentFromTypeID<CRenderer2D>();

		pRenderer->AddConstantBuffer("BillBoard", 11, sizeof(EFFECTCBUFFER),
			CUT_VERTEX | CUT_GEOMETRY);

		SAFE_RELEASE(pRenderer);
	}

	return true;
}

void CEffect::Input(float fTime)
{
}

void CEffect::Update(float fTime)
{
}

void CEffect::LateUpdate(float fTime)
{
	CCamera* pCamera = m_pScene->GetMainCamera();
	CTransform* pCameraTr = pCamera->GetTransform();

	m_tEffectCBuffer.vCenter = m_pTransform->GetWorldPos();
	m_tEffectCBuffer.vCamPos = pCameraTr->GetWorldPos();
	m_tEffectCBuffer.vCamAxisY = pCamera->GetAxis(AXIS_Y);
	m_tEffectCBuffer.vSize = m_pTransform->GetWorldScale();

	SAFE_RELEASE(pCameraTr);
	SAFE_RELEASE(pCamera);

	if (CheckComponentFromTypeID<CRenderer>())
	{
		CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

		pRenderer->UpdateCBuffer("BillBoard", &m_tEffectCBuffer);

		SAFE_RELEASE(pRenderer);
	}

	else if (CheckComponentFromTypeID<CRenderer2D>())
	{
		CRenderer2D*	pRenderer = FindComponentFromTypeID<CRenderer2D>();

		pRenderer->UpdateCBuffer("BillBoard", &m_tEffectCBuffer);

		SAFE_RELEASE(pRenderer);
	}
}

void CEffect::Collision(float fTime)
{
}

void CEffect::Render(float fTime)
{
}

CEffect * CEffect::Clone()
{
	return new CEffect(*this);
}
