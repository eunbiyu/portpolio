#include "KeyMove.h"
#include "Transform.h"
#include "../Core/Input.h"
#include "../GameObject/GameObject.h"

AR3D_USING

CKeyMove::CKeyMove()
{
	SetTag("KeyMove");
	SetTypeName("CKeyMove");
	SetTypeID<CKeyMove>();
}

CKeyMove::CKeyMove(const CKeyMove & keymove)
{
}


CKeyMove::~CKeyMove()
{
}

bool CKeyMove::Init()
{
	GET_SINGLE(CInput)->CreateKey("MoveKeyFront", VK_UP);
	GET_SINGLE(CInput)->CreateKey("MoveKeyBack", VK_DOWN);
	GET_SINGLE(CInput)->CreateKey("RotKeyYFront", 'Y');
	GET_SINGLE(CInput)->CreateKey("RoKeytYBack", 'H');

	return true;
}

void CKeyMove::Input(float fTime)
{

	if (KEYPRESS("RotKeyYFront") || KEYPUSH("RotKeyYFront"))
	{
		m_pTransform->RotateY(AR3D_PI, 0.05f);
	}

	if (KEYPRESS("RoKeytYBack") || KEYPUSH("RoKeytYBack"))
	{
		m_pTransform->RotateY(-AR3D_PI, 0.05f);
	} 
	if (KEYPRESS("MoveKeyFront") || KEYPUSH("MoveKeyFront"))
	{
		m_pTransform->Forward(5.f, 0.05f);
	}

	if (KEYPRESS("MoveKeyBack") || KEYPUSH("MoveKeyBack"))
	{
		m_pTransform->Forward(-5.f, 0.05f);
	}
}

void CKeyMove::Update(float fTime)
{
}

void CKeyMove::LateUpdate(float fTime)
{
}

void CKeyMove::Collision(float fTime)
{
}

void CKeyMove::Render(float fTime)
{
}

CKeyMove * CKeyMove::Clone()
{
	return new CKeyMove(*this);
}

bool CKeyMove::Collision(CCollider * pCollider)
{
	return false;
}
