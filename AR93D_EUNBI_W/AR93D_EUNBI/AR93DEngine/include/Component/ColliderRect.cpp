#include "ColliderRect.h"
#include "Camera.h"
#include "../Scene/Scene.h"
#include "ColliderPoint.h"

AR3D_USING

CColliderRect::CColliderRect()
{
	m_eCollGroup = CG_UI;
	m_eCollType = CT_RECT;
	SetTag("ColliderRect");
	SetTypeName("CColliderRect");
	SetTypeID<CColliderRect>();
}

CColliderRect::CColliderRect(const CColliderRect & collider)
{
	m_tInfo = collider.m_tInfo;
}


CColliderRect::~CColliderRect()
{
}

RECTINFO CColliderRect::GetRectInfo() const
{
	return m_tInfo;
}

void CColliderRect::SetRectInfo(float l, float t, float r, float b)
{
	m_tInfo = RECTINFO(l, t, r, b);
}

bool CColliderRect::Init()
{
	SetMesh("RectLine");
	SetShader(COLLIDER_COLOR_SHADER);
	SetInputLayout("ColorInputLayout");

	return true;
}

void CColliderRect::Input(float fTime)
{
}

void CColliderRect::Update(float fTime)
{
	CCollider::Update(fTime);

	m_tInfo.MoveRect(m_vMove.x, m_vMove.y);
}

void CColliderRect::LateUpdate(float fTime)
{
}

void CColliderRect::Collision(float fTime)
{
}

void CColliderRect::Render(float fTime)
{
//#ifdef _DEBUG
	//Transform ����
	CCamera* pCamera = m_pScene->FindCamera("UICamera");

	XMMATRIX matScale, matPos;
	matScale = XMMatrixScaling(m_tInfo.fR - m_tInfo.fL, m_tInfo.fB - m_tInfo.fT,
		1.f);
	matPos = XMMatrixTranslation(m_tInfo.fL, m_tInfo.fT, 0.f);
	m_tTCBuffer.matWorld = matScale * matPos;
	m_tTCBuffer.matView = pCamera->GetViewMatrix();
	m_tTCBuffer.matProj = pCamera->GetProjMatrix();
	m_tTCBuffer.matWV = m_tTCBuffer.matWorld * m_tTCBuffer.matView;
	m_tTCBuffer.matWVP = m_tTCBuffer.matWV * m_tTCBuffer.matProj;

	SAFE_RELEASE(pCamera);

	m_tTCBuffer.matWorld = XMMatrixTranspose(m_tTCBuffer.matWorld);
	m_tTCBuffer.matView = XMMatrixTranspose(m_tTCBuffer.matView);
	m_tTCBuffer.matProj = XMMatrixTranspose(m_tTCBuffer.matProj);
	m_tTCBuffer.matWV = XMMatrixTranspose(m_tTCBuffer.matWV);
	m_tTCBuffer.matWVP = XMMatrixTranspose(m_tTCBuffer.matWVP);

//#endif // _DEBUG
	CCollider::Render(fTime);
}

CColliderRect * CColliderRect::Clone()
{
	return new CColliderRect(*this);
}

bool CColliderRect::Collision(CCollider * pCollider)
{
	switch (pCollider->GetColliderType())
	{
	case CT_RECT:
		return CollisionRectToRect(m_tInfo, ((CColliderRect*)pCollider)->m_tInfo);
	case CT_POINT:
		return CollisionRectToPoint(m_tInfo, ((CColliderPoint*)pCollider)->GetPoint());
	}
	return false;
}
