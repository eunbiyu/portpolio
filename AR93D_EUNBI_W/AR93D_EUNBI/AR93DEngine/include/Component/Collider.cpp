#include "Collider.h"
#include "Transform.h"
#include "../Device.h"

//#ifdef _DEBUG
#include "../Scene/SceneManager.h"
#include "../Scene/Scene.h"
#include "../Scene/Layer.h"
#include "../Rendering/Shader.h"
#include "../Rendering/ShaderManager.h"
#include "../Resources/Mesh.h"
#include "../Resources/ResourcesManager.h"
#include "../Rendering/RenderManager.h"
#include "../Rendering/RenderState.h"
#include "../GameObject/GameObject.h"
#include "../Core/Input.h"
//#endif 

AR3D_USING

CCollider::CCollider()
{
	m_eComType = CT_COLLIDER;
	SetColor(0.f, 1.f, 0.f, 1.f);
	SetCollColor(1.f, 0.f, 0.f, 1.f);
	SetTerrainCollColor(0.f, 0.f, 1.f, 1.f);

//#ifdef _DEBUG
	m_pMesh = NULL;
	m_pShader = NULL;
	m_pLayout = NULL;
	m_pWireFrame = NULL;
//#endif 

}

CCollider::CCollider(const CCollider & collider) :
	CComponent(collider)
{
	m_eCollType = collider.m_eCollType;
	m_vPrevPos = collider.m_vPrevPos;
	m_eCollGroup = collider.m_eCollGroup;

//#ifdef _DEBUG
		m_pWireFrame = collider.m_pWireFrame;
		m_pMesh = collider.m_pMesh;
		m_pShader = collider.m_pShader;
		m_pLayout = collider.m_pLayout;

		m_vCollColor = collider.m_vCollColor;
		m_vTerrainCollColor = collider.m_vTerrainCollColor;
		m_vColor = collider.m_vColor;

		if (m_pMesh)
			m_pMesh->AddRef();

		if (m_pShader)
			m_pShader->AddRef();

		if (m_pWireFrame)
			m_pWireFrame->AddRef();
//#endif 
}

CCollider::~CCollider()
{
//#ifdef _DEBUG
	SAFE_RELEASE(m_pWireFrame);
	SAFE_RELEASE(m_pMesh);
	SAFE_RELEASE(m_pShader);
//#endif 
}

void CCollider::AddCollList(CCollider * pColl)
{
	m_CollList.push_back(pColl);
}

void CCollider::EraseCollList(CCollider * pColl)
{
	list<CCollider*>::iterator	iter;
	list<CCollider*>::iterator	iterEnd = m_CollList.end();

	for (iter = m_CollList.begin(); iter != iterEnd; ++iter)
	{
		if (*iter == pColl)
		{
			m_CollList.erase(iter);
			break;
		}
	}
}

bool CCollider::CheckCollList(CCollider * pColl)
{
	list<CCollider*>::iterator	iter;
	list<CCollider*>::iterator	iterEnd = m_CollList.end();

	for (iter = m_CollList.begin(); iter != iterEnd; ++iter)
	{
		if (*iter == pColl)
		{
			return true;
		}
	}

	return false;
}

void CCollider::SetMesh(const string & strKey)
{
//#ifdef _DEBUG
		m_pMesh = GET_SINGLE(CResourcesManager)->FindMesh(strKey);
		m_pWireFrame = GET_SINGLE(CRenderManager)->FindRenderState(WIRE_FRAME);
//#endif 

}

void CCollider::SetShader(const string & strKey)
{
//#ifdef _DEBUG
	m_pShader = GET_SINGLE(CShaderManager)->FindShader(strKey);
//#endif 
}

void CCollider::SetInputLayout(const string & strKey)
{
//#ifdef _DEBUG
	m_pLayout = GET_SINGLE(CShaderManager)->FindInputLayout(strKey);
//#endif 
}

void CCollider::SetColor(const DxVector4 & vColor)
{
//#ifdef _DEBUG
	m_vColor = vColor;
//#endif 

}

void CCollider::SetColor(float r, float g, float b, float a)
{
//#ifdef _DEBUG
	m_vColor.x = r;
	m_vColor.y = g;
	m_vColor.z = b;
	m_vColor.w = a;
//#endif 
}

void CCollider::SetCollColor(const DxVector4 & vColor)
{
//#ifdef _DEBUG
	m_vCollColor = vColor;
//#endif 

}

void CCollider::SetCollColor(float r, float g, float b, float a)
{
//#ifdef _DEBUG
	m_vCollColor.x = r;
	m_vCollColor.y = g;
	m_vCollColor.z = b;
	m_vCollColor.w = a;
//#endif 
}

void CCollider::SetTerrainCollColor(float r, float g, float b, float a)
{
//#ifdef _DEBUG
	m_vTerrainCollColor.x = r;
	m_vTerrainCollColor.y = g;
	m_vTerrainCollColor.z = b;
	m_vTerrainCollColor.w = a;
//#endif 
}

void CCollider::SetRayToTerrainCollPos(const DxVector3 & Pos)
{
	m_vRayToTerrainPickPos = Pos;
}

DxVector3 CCollider::GetRayToTerrainCollPos()
{
	return m_vRayToTerrainPickPos;
}

void CCollider::CollisionEnable()
{
	m_bCollision = true;
}

void CCollider::TerrainCollisionEnable()
{
	m_bTerrainCollision = true;
}

COLLIDER_TYPE CCollider::GetColliderType() const
{
	return m_eCollType;
}

COLLIDER_GROUP CCollider::GetColliderGroup() const
{
	return m_eCollGroup;
}

void CCollider::SetColliderGroup(COLLIDER_GROUP eCollGroup)
{
	m_eCollGroup = eCollGroup;
}

void CCollider::Update(float fTime)
{
	m_bCollision = false;
	m_bTerrainCollision = false;

	DxVector3	vPos = m_pTransform->GetWorldPos();

	m_vMove = vPos - m_vPrevPos;

	m_vPrevPos = vPos;
}

void CCollider::LateUpdate(float fTime)
{
}

void CCollider::Collision(float fTime)
{
}

void CCollider::Render(float fTime)
{
////#ifdef _DEBUG
//	if (strcmp(GET_SINGLE(CSceneManager)->GetCurrentScene()->GetTag().c_str(),
//		"ToolScene") == 0)
//	{
//		if(strcmp(m_pLayer->GetTag().c_str(),
//			"ObjectsLayer") == 0)
//		{
//			COLLIDERCOLORCBUFFER	tCBuffer = {};
//			if (m_bCollision)
//				tCBuffer.vColor = m_vCollColor;
//
//			else if (m_bTerrainCollision)
//				tCBuffer.vColor = m_vTerrainCollColor;
//
//			else
//				tCBuffer.vColor = m_vColor;
//
//			m_pWireFrame->SetState();
//
//			GET_SINGLE(CShaderManager)->UpdateConstantBuffer("ColliderColor", &tCBuffer, CUT_VERTEX);
//			GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Transform", &m_tTCBuffer, CUT_VERTEX);
//			CONTEXT->IASetInputLayout(m_pLayout);
//			m_pShader->SetShader();
//
//			m_pMesh->Render();
//
//			m_pWireFrame->ResetState();
//		}
//		else
//		{
//			//안그림 
//		}
//	}
//	else
//	{
		if (m_pMesh)
		{
			COLLIDERCOLORCBUFFER	tCBuffer = {};
			if (m_bCollision)
				tCBuffer.vColor = m_vCollColor;

			else if (m_bTerrainCollision)
				tCBuffer.vColor = m_vTerrainCollColor;

			else
				tCBuffer.vColor = m_vColor;

			m_pWireFrame->SetState();

			GET_SINGLE(CShaderManager)->UpdateConstantBuffer("ColliderColor", &tCBuffer, CUT_VERTEX);
			GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Transform", &m_tTCBuffer, CUT_VERTEX);
			CONTEXT->IASetInputLayout(m_pLayout);
			m_pShader->SetShader();
			if (GET_SINGLE(CInput)->m_OnOffCollider == true)
			{
			m_pMesh->Render();

			}
			m_pWireFrame->ResetState();
		}
//	}
//#endif 

}

bool CCollider::CollisionSphereToSphere(const SPHEREINFO & tSrc, const SPHEREINFO & tDest)
{
	float	fDist = tSrc.Distance(tDest);

	return fDist <= tSrc.fRadius + tDest.fRadius;
}

bool CCollider::CollisionRectToRect(const RECTINFO & tSrc, const RECTINFO & tDest)
{
	if (tSrc.fL > tDest.fR)
		return false;

	else if (tSrc.fR < tDest.fL)
		return false;

	else if (tSrc.fT > tDest.fB)
		return false;

	else if (tSrc.fB < tDest.fT)
		return false;

	return true;
}

bool CCollider::CollisionRectToPoint(const RECTINFO & tSrc, const DxVector3 & vPos)
{
	if (tSrc.fL > vPos.x)
		return false;

	else if (tSrc.fR < vPos.x)
		return false;

	else if (tSrc.fT > vPos.y)
		return false;

	else if (tSrc.fB < vPos.y)
		return false;

	return true;
}

bool CCollider::CollisionRayToSphere(RAY & tRay, const SPHEREINFO & tSphere)
{
	if (tRay.bIntersect)
		return false;

	DxVector3	vM = tRay.vPos - tSphere.vCenter;

	float	b = 2.f * vM.Dot(tRay.vDir);
	float	c = vM.Dot(vM) - tSphere.fRadius * tSphere.fRadius;

	float	fDet = b * b - 4.f * c;

	if (fDet < 0.f)
		return false;

	fDet = sqrtf(fDet);

	float	t0, t1;

	t0 = (-b + fDet) / 2.f;
	t1 = (-b - fDet) / 2.f;

	if (t0 >= 0.f || t1 >= 0.f)
	{
		if (t0 >= 0.f && t0 < t1)
			tRay.vIntersect = tRay.vPos + tRay.vDir * t0;

		else if (t1 >= 0.f && t1 < t0)
			tRay.vIntersect = tRay.vPos + tRay.vDir * t1;

		tRay.bIntersect = true;

		return true;
	}

	return false;
}

bool CCollider::CollisionTerrainToPosition(
	const TERRAINCOLLINFO& tTerrain, class CTransform* pDestTr,
	const DxVector3& vTerrainScale)
{
	DxVector3	vPos = pDestTr->GetWorldPos();

	// 지형 그리드의 인덱스를 구해준다.
	float	x = (vPos.x / tTerrain.iSizeW) / vTerrainScale.x;
	float	z = (vPos.z / tTerrain.iSizeH) / vTerrainScale.z;

	int	idx = (tTerrain.iNumH - 1 - ((int)z + 1)) * tTerrain.iNumW + (int)x;

	// 사각형을 구성하는 4개의 정점 정보를 구해온다.
	DxVector3	vRcPos[4];
	vRcPos[0] = tTerrain.vecPos[idx];
	vRcPos[1] = tTerrain.vecPos[idx + 1];
	vRcPos[2] = tTerrain.vecPos[idx + tTerrain.iNumW];
	vRcPos[3] = tTerrain.vecPos[idx + tTerrain.iNumW + 1];

	// 우 상단 삼각형인지 체크한다.
	x -= (int)x;
	z = 1.f - (z - (int)z);

	float	fY = 0.f;
	if (x >= z)
		fY = vRcPos[0].y + (vRcPos[1].y - vRcPos[0].y) * x + (vRcPos[3].y - vRcPos[1].y) * z;

	else
		fY = vRcPos[0].y + (vRcPos[3].y - vRcPos[2].y) * x + (vRcPos[2].y - vRcPos[0].y) * z;

	vPos.y = fY;
	pDestTr->SetWorldPos(vPos);

	return true;
}

bool CCollider::CollisionCameraTerrainToPosition(const TERRAINCOLLINFO & tTerrain, CTransform * pDestTr, const DxVector3 & vTerrainScale)
{
	DxVector3	vPos = pDestTr->GetWorldPos();

	// 지형 그리드의 인덱스를 구해준다.
	float	x = (vPos.x / tTerrain.iSizeW) / vTerrainScale.x;
	float	z = (vPos.z / tTerrain.iSizeH) / vTerrainScale.z;

	int	idx = (tTerrain.iNumH - 1 - ((int)z + 1)) * tTerrain.iNumW + (int)x;

	// 사각형을 구성하는 4개의 정점 정보를 구해온다.
	DxVector3	vRcPos[4];
	vRcPos[0] = tTerrain.vecPos[idx];
	vRcPos[1] = tTerrain.vecPos[idx + 1];
	vRcPos[2] = tTerrain.vecPos[idx + tTerrain.iNumW];
	vRcPos[3] = tTerrain.vecPos[idx + tTerrain.iNumW + 1];

	// 우 상단 삼각형인지 체크한다.
	x -= (int)x;
	z = 1.f - (z - (int)z);

	float	fY = 0.f;
	if (x >= z)
		fY = vRcPos[0].y + (vRcPos[1].y - vRcPos[0].y) * x + (vRcPos[3].y - vRcPos[1].y) * z;

	else
		fY = vRcPos[0].y + (vRcPos[3].y - vRcPos[2].y) * x + (vRcPos[2].y - vRcPos[0].y) * z;
	
	// 카메라는 지형에 완전 붙게하지않고, 지형에서 위로 조금 떨어져있도록 만들어준다. 
	fY += 30;

	if (vPos.y <= fY)
	{
		vPos.y = fY;
		pDestTr->SetWorldPos(vPos);
	}
	return true;
}

bool CCollider::CollisionRayToTriangle(RAY & tRay, const DxVector3 & v0, 
	const DxVector3 & v1, const DxVector3 & v2, float & dist)
{
	float det, u, v;
	DxVector3 pvec, tvec, qvec;
	DxVector3 edge1 = v1 - v0;
	DxVector3 edge2 = v2 - v0;

	pvec = XMVector3Cross(tRay.vDir.Convert(), edge2.Convert());
	det = edge1.Dot(edge2);

	if (det < 0.0001f)
		return false;

	tvec = tRay.vPos - v0;
	u = tvec.Dot(pvec);
	if (u < 0.0f || u > det)
		return false;

	
	qvec = tvec.Cross(edge1);
	v = tRay.vDir.Dot(qvec);
	if (v < 0.0f || u + v > det)
		return false;

	dist = edge2.Dot(qvec);
	dist *= (1.0f / det);

	return true;
}

bool CCollider::CollisionAABBToAABB(const AABBBOXINFO & tSrc, const AABBBOXINFO & tDest)
{
	if (tSrc.vCenter.x > tDest.fMinX)
	{
		if (tSrc.vCenter.x < tDest.fMaxX)
			return false;
	}

	if (tSrc.vCenter.y > tDest.fMinY)
	{
		if (tSrc.vCenter.y < tDest.fMaxY)
			return false;
	}

	if (tSrc.vCenter.z > tDest.fMinZ)
	{
		if (tSrc.vCenter.z < tDest.fMaxZ)
			return false;
	}

	return true;
}