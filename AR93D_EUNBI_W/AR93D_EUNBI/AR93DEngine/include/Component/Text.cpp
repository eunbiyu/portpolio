#include "Text.h"
#include "Transform.h"
#include "../Device.h"
#include "../Core/FontManager.h"
#include "../Core/PathManager.h"

AR3D_USING

CText::CText()
{
	SetTag("Text");
	SetTypeName("CText");
	SetTypeID<CText>();
	m_eComType = CT_TEXT;

	m_eAlign = TEXT_LEFT;
	m_ePAlign = TPA_TOP;

	m_tArea = RECTINFO(0.f, 0.f, 100.f, 100.f);

	m_strFontName = "궁서체50";
	m_strBrushName = "Black";
	m_pFont = GET_SINGLE(CFontManager)->FindFont(m_strFontName);
	m_pBrush = GET_SINGLE(CFontManager)->FindBrush(m_strBrushName);
	m_pShadowBrush = NULL;

	m_strText = L"TEXT";
	m_bShadow = false;
}

CText::CText(const CText & text) :
	CComponent(text)
{
	*this = text;
}

CText::~CText()
{
}

void CText::SetShadow(bool bShadow)
{
	m_bShadow = bShadow;

	if (bShadow)
		m_pShadowBrush = GET_SINGLE(CFontManager)->FindBrush("Black");
}

void CText::SetText(const wstring & strText)
{
	m_strText = strText;
}

void CText::SetText(const char * pFileName, const string & strPathKey)
{
	if (!pFileName)
		return;

	string	strPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(DATA_PATH);
	strPath += pFileName;

	m_strText.clear();

	FILE*	pFile = NULL;
	fopen_s(&pFile, strPath.c_str(), "rt");

	if (pFile)
	{
		while (feof(pFile) == 0)
		{
			char	strLine[1024] = {};
			fgets(strLine, 1024, pFile);

			wchar_t	strLineW[1024] = {};
			MultiByteToWideChar(CP_ACP, 0, strLine, -1, strLineW, strlen(strLine));

			m_strText += strLineW;
		}

		fclose(pFile);
	}
}

void CText::AddText(const wstring & strText)
{
	m_strText += strText;
}
void CText::AddText(const string & strText)
{
	wstring wstr = L"";
	wstr.assign(strText.begin(), strText.end());

	m_strText += wstr;
}
void CText::SetAlign(TEXT_ALIGN eAlign)
{
	m_eAlign = eAlign;
}

void CText::SetParagraphAlign(TEXT_PARAGRAPH_ALIGN eAlign)
{
	m_ePAlign = eAlign;
}

void CText::SetFont(const string & strName)
{
	m_strFontName = strName;
	m_pFont = GET_SINGLE(CFontManager)->FindFont(m_strFontName);
}

void CText::SetFont(const string & strKey, const wchar_t * pFontName, int iWeight, int iStyle,
	int iStretch, float fSize, const wchar_t * pLocalName)
{
	m_strFontName = strKey;
	m_pFont = GET_SINGLE(CFontManager)->CreateFontFormat(strKey, pFontName, iWeight, iStyle,
		iStretch, fSize, pLocalName);
}

void CText::SetBrush(const string & strName)
{
	m_strBrushName = strName;
	m_pBrush = GET_SINGLE(CFontManager)->FindBrush(m_strBrushName);
}

void CText::SetBrush(const string & strKey, float r, float g, float b, float a)
{
	m_strBrushName = strKey;
	m_pBrush = GET_SINGLE(CFontManager)->CreateBrush(m_strBrushName, r, g, b, a);
}

void CText::SetArea(const RECTINFO & rc)
{
	m_tArea = rc;
}

void CText::SetArea(float l, float t, float r, float b)
{
	m_tArea = RECTINFO(l, t, r, b);
}

void CText::SetArea(const DxVector2 & vPos, const DxVector2 & vSize)
{
	m_tArea = RECTINFO(vPos.x, vPos.y, vPos.x + vSize.x, vPos.y + vSize.y);
}

void CText::SetArea(const DxVector3 & vPos, const DxVector3 & vSize)
{
	m_tArea = RECTINFO(vPos.x, vPos.y, vPos.x + vSize.x, vPos.y + vSize.y);
}

bool CText::Init()
{
	return true;
}

void CText::Input(float fTime)
{
}

void CText::Update(float fTime)
{
}

void CText::LateUpdate(float fTime)
{
}

void CText::Collision(float fTime)
{
}

void CText::Render(float fTime)
{
	// 텍스트 가로 정렬을 설정한다. 왼쪽 끝에서 출력하게 했다.
	// DWRITE_TEXT_ALIGNMENT_CENTER : 가운데
	// DWRITE_TEXT_ALIGNMENT_TRAILING : 오른쪽
	// DWRITE_TEXT_ALIGNMENT_LEADING : 왼쪽
	switch (m_eAlign)
	{
	case TEXT_LEFT:
		m_pFont->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
		break;
	case TEXT_CENTER:
		m_pFont->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
		break;
	case TEXT_RIGHT:
		m_pFont->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_TRAILING);
		break;
	}

	// 텍스트 세로 정렬을 설정한다.
	// DWRITE_PARAGRAPH_ALIGNMENT_NEAR : 글자가 위 끝에 출력된다.
	// DWRITE_PARAGRAPH_ALIGNMENT_FAR : 글자가 아래 끝에 출력된다.
	// DWRITE_PARAGRAPH_ALIGNMENT_CENTER : 글자가 가운데 출력된다.
	switch (m_ePAlign)
	{
	case TPA_TOP:
		m_pFont->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
		break;
	case TPA_CENTER:
		m_pFont->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
		break;
	case TPA_BOTTOM:
		m_pFont->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_FAR);
		break;
	}

	DxVector3	vPos = m_pTransform->GetWorldPos();
	RECTINFO	rc = m_tArea + vPos;

	D2DTARGET->BeginDraw();

	if (m_bShadow)
	{
		RECTINFO	rcShadow = rc;
		rcShadow.fL += 2;
		rcShadow.fT += 2;
		rcShadow.fR += 2;
		rcShadow.fB += 2;

		D2DTARGET->DrawTextW(m_strText.c_str(), m_strText.length(), m_pFont,
			D2D1::RectF(rcShadow.fL, rcShadow.fT, rcShadow.fR, rcShadow.fB), m_pShadowBrush);
	}
	D2DTARGET->DrawTextW(m_strText.c_str(), m_strText.length(), m_pFont,
		D2D1::RectF(rc.fL, rc.fT, rc.fR, rc.fB), m_pBrush);

	D2DTARGET->EndDraw();
}

CText * CText::Clone()
{
	return new CText(*this);
}
