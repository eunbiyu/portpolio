#pragma once
#include "Light.h"

AR3D_BEGIN

class DLL CLightPoint :
	public CLight
{
private:
	friend class CGameObject;

private:
	CLightPoint();
	CLightPoint(const CLightPoint& light);
	~CLightPoint();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CLightPoint* Clone();
	virtual void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);

public:
	virtual void SetLight();
};

AR3D_END
