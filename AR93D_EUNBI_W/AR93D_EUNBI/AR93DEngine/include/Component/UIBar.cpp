#include "UIBar.h"
#include "Transform.h"

AR3D_USING

CUIBar::CUIBar()
{
	SetTag("UIBar");
	SetTypeName("CUIBar");
	SetTypeID<CUIBar>();
	m_eUIType = UT_BAR;
}

CUIBar::CUIBar(const CUIBar & bar)
	:CUI(bar)
{
	m_fMin = bar.m_fMin;
	m_fMax = bar.m_fMax;
	m_fValue = bar.m_fValue;
	m_fMoveValue = bar.m_fMoveValue;
	m_fCurValue = bar.m_fCurValue;
	m_eBarDir = bar.m_eBarDir;
}


CUIBar::~CUIBar()
{
}

void CUIBar::SetBarDir(BAR_DIR eDir)
{
	m_eBarDir = eDir;

	switch (eDir)
	{
	case BD_LEFT:
		m_pTransform->SetPivot(0.f, 0.f, 0.f);
		break;
	case BD_RIGHT:
		m_pTransform->SetPivot(1.f, 0.f, 0.f);
		break;
	case BD_UP:
		m_pTransform->SetPivot(0.f, 0.f, 0.f);
		break;
	case BD_DOWN:
		m_pTransform->SetPivot(0.f, 1.f, 0.f);
		break;
	}
}

void CUIBar::SetMinMax(float fMin, float fMax)
{
	m_fMin = fMin;
	m_fMax = fMax;
	m_fCurValue = fMax;
}

void CUIBar::SetCurValue(float fValue)
{
	m_fCurValue = fValue;
}

void CUIBar::AddValue(float fValue)
{
	m_fCurValue += fValue;

	// �� ���� 
	if (m_fCurValue <= m_fMin)
		m_fCurValue = m_fMin;
	else if (m_fCurValue >= m_fMax)
		m_fCurValue = m_fMax;
	m_fMoveValue = fValue / (m_fMax - m_fMin); 
}

DxVector3 CUIBar::ComputeScale(DxVector3 vScale)
{
	switch (m_eBarDir)
	{
	case BD_LEFT:
	case BD_RIGHT:
		if (vScale.x != 0)
		{
			vScale.x = vScale.x / (m_fValue * 10.f) * 10.f;
			if (m_fSize == 0.f)
				m_fSize = vScale.x;
		}

		else
			vScale.x = m_fSize;
		break;

	case BD_UP:
	case BD_DOWN:
		if (vScale.y != 0)
		{
			vScale.y = vScale.y / (m_fValue * 10.f) * 10.f;
			if (m_fSize == 0.f)
				m_fSize = vScale.y;
		}

		else
			vScale.y = m_fSize;
		break;
	}

	return vScale;
}

bool CUIBar::Init()
{
	m_fSize = 0.f;
	m_fMin = 0.f;
	m_fMax = 1.f;
	m_fValue = 1.f;
	m_fMoveValue = 0.f;
	m_fCurValue = 1.f;
	m_eBarDir = BD_LEFT;
	m_pTransform->SetPivot(0.f, 0.f, 0.f);

	return true;
}

void CUIBar::Input(float fTime)
{
}

void CUIBar::Update(float fTime)
{
}

void CUIBar::LateUpdate(float fTime)
{
	DxVector3	vScale = m_pTransform->GetWorldScale();

	vScale = ComputeScale(vScale);

	m_fValue += m_fMoveValue;
	m_fMoveValue = 0.f;

	if (m_fValue > 1.f)
		m_fValue = 1.f;

	else if (m_fValue < 0.f)
		m_fValue = 0.f;

	switch (m_eBarDir)
	{
	case BD_LEFT:
		vScale.x *= m_fValue;
		break;
	case BD_RIGHT:
		vScale.x *= m_fValue;
		break;
	case BD_UP:
		vScale.y *= m_fValue;
		break;
	case BD_DOWN:
		vScale.y *= m_fValue;
		break;
	}

	m_pTransform->SetWorldScale(vScale);

}

void CUIBar::Collision(float fTime)
{
}

void CUIBar::Render(float fTime)
{
}

CUI * CUIBar::Clone()
{
	return new CUIBar(*this);
}

void CUIBar::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CUIBar::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CUIBar::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}
