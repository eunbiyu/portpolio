#pragma once
#include "Collider.h"

AR3D_BEGIN

class DLL CColliderTerrain :
	public CCollider
{
private:
	friend class CGameObject;

private:
	CColliderTerrain();
	CColliderTerrain(const CColliderTerrain& collider);
	~CColliderTerrain();


public:
	void* operator new (size_t size)
	{
		return _aligned_malloc(size, 16);
	}

	void operator delete (void* p)
	{
		_aligned_free(p);
	}


private:
	TERRAINCOLLINFO	m_tInfo;
	DxVector3 m_PickPos;

public:
	TERRAINCOLLINFO GetInfo()	const;

public:
	void SetInfo(const vector<DxVector3>& vecPos, unsigned int iNumW, unsigned int iNumH, 
		unsigned int iSizeW, unsigned int iSizeH);
	void SetPickPosInfo(const DxVector3 vPos);
	DxVector3 GetPickInfo(void);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CColliderTerrain* Clone();
	virtual bool Collision(CCollider* pCollider);
};

AR3D_END
