#include "Material.h"
#include "../Resources/Texture.h"
#include "../Resources/Sampler.h"
#include "../Resources/ResourcesManager.h"
#include "../Device.h"
#include "../Rendering/ShaderManager.h"

AR3D_USING

CMaterial::CMaterial()
{
	SetTag("Material");
	SetTypeName("CMaterial");
	SetTypeID<CMaterial>();

	m_tInfo.vDif = DxVector4(1.f, 1.f, 1.f, 1.f);
	m_tInfo.vAmb = DxVector4(1.f, 1.f, 1.f, 1.f);
	m_tInfo.vSpc = DxVector4(1.f, 1.f, 1.f, 1.f);
	m_tInfo.vEmv = DxVector4(1.f, 1.f, 1.f, 1.f);
	m_tInfo.fSpecularPower = 3.2f;
	m_tInfo.iBump = 0;
	m_tInfo.iSpecular = 0;

	m_tBase.pDiffuse = NULL;
	m_tBase.pNormal = NULL;
	m_tBase.pSpecular = NULL;
}

CMaterial::CMaterial(const CMaterial & mtrl)
{
	*this = mtrl;

	if (m_tBase.pDiffuse)
	{
		m_tBase.pDiffuse = new TEXTURE;
		*m_tBase.pDiffuse = *mtrl.m_tBase.pDiffuse;
		if (m_tBase.pDiffuse->pTexture)
			m_tBase.pDiffuse->pTexture->AddRef();

		if (m_tBase.pDiffuse->pSampler)
			m_tBase.pDiffuse->pSampler->AddRef();
	}

	if (m_tBase.pNormal)
	{
		m_tBase.pNormal = new TEXTURE;
		*m_tBase.pNormal = *mtrl.m_tBase.pNormal;
		if (m_tBase.pNormal->pTexture)
			m_tBase.pNormal->pTexture->AddRef();

		if (m_tBase.pNormal->pSampler)
			m_tBase.pNormal->pSampler->AddRef();
	}

	if (m_tBase.pSpecular)
	{
		m_tBase.pSpecular = new TEXTURE;
		*m_tBase.pSpecular = *mtrl.m_tBase.pSpecular;
		if (m_tBase.pSpecular->pTexture)
			m_tBase.pSpecular->pTexture->AddRef();

		if (m_tBase.pSpecular->pSampler)
			m_tBase.pSpecular->pSampler->AddRef();
	}

	if (m_tMultiTex.pDiffuse)
	{
		m_tMultiTex.pDiffuse = new TEXTURE;
		*m_tMultiTex.pDiffuse = *mtrl.m_tMultiTex.pDiffuse;
		if (m_tMultiTex.pDiffuse->pTexture)
			m_tMultiTex.pDiffuse->pTexture->AddRef();

		if (m_tMultiTex.pDiffuse->pSampler)
			m_tMultiTex.pDiffuse->pSampler->AddRef();
	}

	if (m_tMultiTex.pNormal)
	{
		m_tMultiTex.pNormal = new TEXTURE;
		*m_tMultiTex.pNormal = *mtrl.m_tMultiTex.pNormal;
		if (m_tMultiTex.pNormal->pTexture)
			m_tMultiTex.pNormal->pTexture->AddRef();

		if (m_tMultiTex.pNormal->pSampler)
			m_tMultiTex.pNormal->pSampler->AddRef();
	}

	if (m_tMultiTex.pSpecular)
	{
		m_tMultiTex.pSpecular = new TEXTURE;
		*m_tMultiTex.pSpecular = *mtrl.m_tMultiTex.pSpecular;
		if (m_tMultiTex.pSpecular->pTexture)
			m_tMultiTex.pSpecular->pTexture->AddRef();

		if (m_tMultiTex.pSpecular->pSampler)
			m_tMultiTex.pSpecular->pSampler->AddRef();
	}
}

CMaterial::~CMaterial()
{
	if (m_tBase.pDiffuse)
	{
		SAFE_RELEASE(m_tBase.pDiffuse->pTexture);
		SAFE_RELEASE(m_tBase.pDiffuse->pSampler);
		SAFE_DELETE(m_tBase.pDiffuse);
	}

	if (m_tBase.pNormal)
	{
		SAFE_RELEASE(m_tBase.pNormal->pTexture);
		SAFE_RELEASE(m_tBase.pNormal->pSampler);
		SAFE_DELETE(m_tBase.pNormal);
	}

	if (m_tBase.pSpecular)
	{
		SAFE_RELEASE(m_tBase.pSpecular->pTexture);
		SAFE_RELEASE(m_tBase.pSpecular->pSampler);
		SAFE_DELETE(m_tBase.pSpecular);
	}

	if (m_tMultiTex.pDiffuse)
	{
		SAFE_RELEASE(m_tMultiTex.pDiffuse->pTexture);
		SAFE_RELEASE(m_tMultiTex.pDiffuse->pSampler);
		SAFE_DELETE(m_tMultiTex.pDiffuse);
	}

	if (m_tMultiTex.pNormal)
	{
		SAFE_RELEASE(m_tMultiTex.pNormal->pTexture);
		SAFE_RELEASE(m_tMultiTex.pNormal->pSampler);
		SAFE_DELETE(m_tMultiTex.pNormal);
	}

	if (m_tMultiTex.pSpecular)
	{
		SAFE_RELEASE(m_tMultiTex.pSpecular->pTexture);
		SAFE_RELEASE(m_tMultiTex.pSpecular->pSampler);
		SAFE_DELETE(m_tMultiTex.pSpecular);
	}

	if (m_tMultiTex.pAlpha)
	{
		SAFE_RELEASE(m_tMultiTex.pAlpha->pTexture);
		SAFE_RELEASE(m_tMultiTex.pAlpha->pSampler);
		SAFE_DELETE(m_tMultiTex.pAlpha);
	}
}

MATERIALINFO CMaterial::GetMaterialInfo() const
{
	return m_tInfo;
}

SKININFO CMaterial::GetBaseSkin() const
{
	return m_tBase;
}

void CMaterial::SetBaseSkin(const SKININFO & tBaseSkin)
{
	if (m_tBase.pDiffuse)
	{
		SAFE_RELEASE(m_tBase.pDiffuse->pTexture);
		SAFE_RELEASE(m_tBase.pDiffuse->pSampler);
		SAFE_DELETE(m_tBase.pDiffuse);
	}

	if (m_tBase.pNormal)
	{
		SAFE_RELEASE(m_tBase.pNormal->pTexture);
		SAFE_RELEASE(m_tBase.pNormal->pSampler);
		SAFE_DELETE(m_tBase.pNormal);
	}

	if (m_tBase.pSpecular)
	{
		SAFE_RELEASE(m_tBase.pSpecular->pTexture);
		SAFE_RELEASE(m_tBase.pSpecular->pSampler);
		SAFE_DELETE(m_tBase.pSpecular);
	}
	m_tBase = tBaseSkin;
}

void CMaterial::SetMaterialInfo(const MATERIALINFO & tInfo)
{
	m_tInfo = tInfo;
}

void CMaterial::SetMaterialInfo(const DxVector4 & vDif, const DxVector4 & vAmb, const DxVector4 & vSpc,
	const DxVector4 & vEmv, float fSpecularPower)
{
	m_tInfo.vDif = vDif;
	m_tInfo.vAmb = vAmb;
	m_tInfo.vSpc = vSpc;
	m_tInfo.vEmv = vEmv;
	m_tInfo.fSpecularPower = fSpecularPower;
}

void CMaterial::SetMaterialInfo(const DxVector3 & vDif, const DxVector3 & vAmb, const DxVector3 & vSpc, const DxVector3 & vEmv, float fSpecularPower)
{
	m_tInfo.vDif = vDif;
	m_tInfo.vAmb = vAmb;
	m_tInfo.vSpc = vSpc;
	m_tInfo.vEmv = vEmv;
	m_tInfo.fSpecularPower = fSpecularPower;
}

void CMaterial::SetDiffuseColor(const DxVector4 & vDif)
{
	m_tInfo.vDif = vDif;
}

bool CMaterial::SetDiffuseTexture(const string & strSamplerKey,
	const string & strTextureKey, TCHAR * pFileName, const string & strPathKey)
{
	if (!m_tBase.pDiffuse)
		m_tBase.pDiffuse = new TEXTURE;

	SAFE_RELEASE(m_tBase.pDiffuse->pTexture);
	SAFE_RELEASE(m_tBase.pDiffuse->pSampler);

	if (pFileName)
		m_tBase.pDiffuse->pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(strTextureKey, pFileName, strPathKey);

	else
		m_tBase.pDiffuse->pTexture = GET_SINGLE(CResourcesManager)->FindTexture(strTextureKey);

	m_tBase.pDiffuse->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strSamplerKey);

	return true;
}

bool CMaterial::SetDiffuseTextureFromFullPath(const string & strSamplerKey,
	const string & strKey, const char * pFullPath)
{
	if (!m_tBase.pDiffuse)
		m_tBase.pDiffuse = new TEXTURE;

	SAFE_RELEASE(m_tBase.pDiffuse->pTexture);
	SAFE_RELEASE(m_tBase.pDiffuse->pSampler);

	m_tBase.pDiffuse->pTexture = GET_SINGLE(CResourcesManager)->LoadTextureFromFullPath(strKey, pFullPath);

	m_tBase.pDiffuse->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strSamplerKey);

	return true;
}

bool CMaterial::SetDiffuseTexture(CTexture * pTexture, int iTexRegister)
{
	if (!m_tBase.pDiffuse)
		m_tBase.pDiffuse = new TEXTURE;

	SAFE_RELEASE(m_tBase.pDiffuse->pTexture);
	m_tBase.pDiffuse->pTexture = pTexture;
	pTexture->AddRef();
	m_tBase.pDiffuse->iTextureRegister = iTexRegister;

	return true;
}

void CMaterial::SetDiffuseRegister(int iSmpRegister, int iTexRegister)
{
	if (!m_tBase.pDiffuse)
		m_tBase.pDiffuse = new TEXTURE;

	m_tBase.pDiffuse->iTextureRegister = iTexRegister;
	m_tBase.pDiffuse->iSamplerRegister = iSmpRegister;
}

void CMaterial::SetDiffuseShaderConstantType(int iShaderConstantType)
{
	if (!m_tBase.pDiffuse)
		m_tBase.pDiffuse = new TEXTURE;

	m_tBase.pDiffuse->iShaderConstantType = iShaderConstantType;
}

bool CMaterial::SetNormalMapTexture(const string & strSamplerKey, const string & strTextureKey, TCHAR * pFileName, const string & strPathKey)
{
	if (!m_tBase.pNormal)
		m_tBase.pNormal = new TEXTURE;

	SAFE_RELEASE(m_tBase.pNormal->pTexture);
	SAFE_RELEASE(m_tBase.pNormal->pSampler);

	if (pFileName)
		m_tBase.pNormal->pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(strTextureKey, pFileName, strPathKey);

	else
		m_tBase.pNormal->pTexture = GET_SINGLE(CResourcesManager)->FindTexture(strTextureKey);

	m_tBase.pNormal->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strSamplerKey);

	m_tInfo.iBump = 1;
	SetNormalMapRegister(1, 1);


	return true;
}

bool CMaterial::SetNormalMapTextureFromFullPath(const string & strSamplerKey, const string & strKey, const char * pFullPath)
{
	if (!m_tBase.pNormal)
		m_tBase.pNormal = new TEXTURE;

	SAFE_RELEASE(m_tBase.pNormal->pTexture);
	SAFE_RELEASE(m_tBase.pNormal->pSampler);

	m_tBase.pNormal->pTexture = GET_SINGLE(CResourcesManager)->LoadTextureFromFullPath(strKey, pFullPath);

	m_tBase.pNormal->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strSamplerKey);

	m_tInfo.iBump = 1;
	SetNormalMapRegister(1, 1);

	return true;
}

bool CMaterial::SetNormalMapTexture(CTexture * pTexture, int iTexRegister)
{
	if (!m_tBase.pNormal)
		m_tBase.pNormal = new TEXTURE;

	SAFE_RELEASE(m_tBase.pNormal->pTexture);
	m_tBase.pNormal->pTexture = pTexture;
	pTexture->AddRef();
	m_tBase.pNormal->iTextureRegister = iTexRegister;

	m_tInfo.iBump = 1;
	SetNormalMapRegister(1, 1);

	return true;
}

void CMaterial::SetNormalMapRegister(int iSmpRegister, int iTexRegister)
{
	if (!m_tBase.pNormal)
		m_tBase.pNormal = new TEXTURE;

	m_tBase.pNormal->iTextureRegister = iTexRegister;
	m_tBase.pNormal->iSamplerRegister = iSmpRegister;
}

void CMaterial::SetNormalMapShaderConstantType(int iShaderConstantType)
{
	if (!m_tBase.pNormal)
		m_tBase.pNormal = new TEXTURE;

	m_tBase.pNormal->iShaderConstantType = iShaderConstantType;
}

bool CMaterial::SetSpecularTexture(const string & strSamplerKey, const string & strTextureKey, TCHAR * pFileName, const string & strPathKey)
{
	if (!m_tBase.pSpecular)
		m_tBase.pSpecular = new TEXTURE;

	SAFE_RELEASE(m_tBase.pSpecular->pTexture);
	SAFE_RELEASE(m_tBase.pSpecular->pSampler);

	if (pFileName)
		m_tBase.pSpecular->pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(strTextureKey, pFileName, strPathKey);

	else
		m_tBase.pSpecular->pTexture = GET_SINGLE(CResourcesManager)->FindTexture(strTextureKey);

	m_tBase.pSpecular->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strSamplerKey);

	m_tInfo.iSpecular = 1;
	SetSpecularRegister(2, 2);

	return true;
}

bool CMaterial::SetSpecularTextureFromFullPath(const string & strSamplerKey, const string & strKey, const char * pFullPath)
{
	if (!m_tBase.pSpecular)
		m_tBase.pSpecular = new TEXTURE;

	SAFE_RELEASE(m_tBase.pSpecular->pTexture);
	SAFE_RELEASE(m_tBase.pSpecular->pSampler);

	m_tBase.pSpecular->pTexture = GET_SINGLE(CResourcesManager)->LoadTextureFromFullPath(strKey, pFullPath);

	m_tBase.pSpecular->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strSamplerKey);

	m_tInfo.iSpecular = 1;
	SetSpecularRegister(2, 2);

	return true;
}

bool CMaterial::SetSpecularTexture(CTexture * pTexture, int iTexRegister)
{
	if (!m_tBase.pSpecular)
		m_tBase.pSpecular = new TEXTURE;

	SAFE_RELEASE(m_tBase.pSpecular->pTexture);
	m_tBase.pSpecular->pTexture = pTexture;
	pTexture->AddRef();
	m_tBase.pSpecular->iTextureRegister = iTexRegister;

	m_tInfo.iSpecular = 1;
	SetSpecularRegister(2, 2);

	return true;
}

void CMaterial::SetSpecularRegister(int iSmpRegister, int iTexRegister)
{
	if (!m_tBase.pSpecular)
		m_tBase.pSpecular = new TEXTURE;

	m_tBase.pSpecular->iTextureRegister = iTexRegister;
	m_tBase.pSpecular->iSamplerRegister = iSmpRegister;
}

void CMaterial::SetSpecularShaderConstantType(int iShaderConstantType)
{
	if (!m_tBase.pSpecular)
		m_tBase.pSpecular = new TEXTURE;

	m_tBase.pSpecular->iShaderConstantType = iShaderConstantType;
}

void CMaterial::AddDiffuseMultiTexture(const string & strDifTexKey,
	const string & strDifSmpKey, int iDifTexRegister, int iDifSmpRegister,
	int iShaderType,
	vector<wstring> vecFileName, const string& strPathKey)
{
	if (m_tMultiTex.pDiffuse)
	{
		SAFE_RELEASE(m_tMultiTex.pDiffuse->pTexture);
		SAFE_RELEASE(m_tMultiTex.pDiffuse->pSampler);
		SAFE_DELETE(m_tMultiTex.pDiffuse);
	}

	m_tMultiTex.pDiffuse = new TEXTURE;

	m_tMultiTex.pDiffuse->pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(strDifTexKey, vecFileName, strPathKey);
	m_tMultiTex.pDiffuse->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strDifSmpKey);

	m_tMultiTex.pDiffuse->iTextureRegister = iDifTexRegister;
	m_tMultiTex.pDiffuse->iSamplerRegister = iDifSmpRegister;
	m_tMultiTex.pDiffuse->iShaderConstantType = iShaderType;
}

void CMaterial::AddNormalMultiTexture(const string & strNrmTexKey,
	const string & strNrmSmpKey, int iNrmTexRegister, int iNrmSmpRegister,
	int iShaderType,
	vector<wstring> vecFileName, const string& strPathKey)
{
	if (m_tMultiTex.pNormal)
	{
		SAFE_RELEASE(m_tMultiTex.pNormal->pTexture);
		SAFE_RELEASE(m_tMultiTex.pNormal->pSampler);
		SAFE_DELETE(m_tMultiTex.pNormal);
	}

	m_tMultiTex.pNormal = new TEXTURE;
	m_tMultiTex.pNormal->pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(strNrmTexKey, vecFileName, strPathKey);
	m_tMultiTex.pNormal->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strNrmSmpKey);
	m_tMultiTex.pNormal->iTextureRegister = iNrmTexRegister;
	m_tMultiTex.pNormal->iSamplerRegister = iNrmSmpRegister;
	m_tMultiTex.pNormal->iShaderConstantType = iShaderType;
}

void CMaterial::AddSpecularMultiTexture(const string & strSpcTexKey,
	const string & strSpcSmpKey, int iSpcTexRegister, int iSpcSmpRegister,
	int iShaderType,
	vector<wstring> vecFileName, const string& strPathKey)
{
	if (m_tMultiTex.pSpecular)
	{
		SAFE_RELEASE(m_tMultiTex.pSpecular->pTexture);
		SAFE_RELEASE(m_tMultiTex.pSpecular->pSampler);
		SAFE_DELETE(m_tMultiTex.pSpecular);
	}

	m_tMultiTex.pSpecular = new TEXTURE;
	m_tMultiTex.pSpecular->pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(strSpcTexKey, vecFileName, strPathKey);
	m_tMultiTex.pSpecular->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strSpcSmpKey);
	m_tMultiTex.pSpecular->iTextureRegister = iSpcTexRegister;
	m_tMultiTex.pSpecular->iSamplerRegister = iSpcSmpRegister;
	m_tMultiTex.pSpecular->iShaderConstantType = iShaderType;
}

void CMaterial::AddAlphaMultiTexture(const string & strAlphaTexKey,
	const string & strAlphaSmpKey, int iAlphaTexRegister,
	int iAlphaSmpRegister, int iShaderType,
	vector<wstring> vecFileName, const string & strPathKey)
{
	if (m_tMultiTex.pAlpha)
	{
		SAFE_RELEASE(m_tMultiTex.pAlpha->pTexture);
		SAFE_RELEASE(m_tMultiTex.pAlpha->pSampler);
		SAFE_DELETE(m_tMultiTex.pAlpha);
	}

	m_tMultiTex.pAlpha = new TEXTURE;
	m_tMultiTex.pAlpha->pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(strAlphaTexKey, vecFileName, strPathKey);
	m_tMultiTex.pAlpha->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strAlphaSmpKey);
	m_tMultiTex.pAlpha->iTextureRegister = iAlphaTexRegister;
	m_tMultiTex.pAlpha->iSamplerRegister = iAlphaSmpRegister;
	m_tMultiTex.pAlpha->iShaderConstantType = iShaderType;
}

bool CMaterial::Init()
{
	return true;
}

void CMaterial::SetMaterial()
{
	GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Material", &m_tInfo, CUT_VERTEX | CUT_GEOMETRY | CUT_PIXEL);

	if (m_tBase.pDiffuse)
	{
		if (m_tBase.pDiffuse->pTexture)
			m_tBase.pDiffuse->pTexture->SetTexture(m_tBase.pDiffuse->iTextureRegister, m_tBase.pDiffuse->iShaderConstantType);

		if (m_tBase.pDiffuse->pSampler)
			m_tBase.pDiffuse->pSampler->SetSampler(m_tBase.pDiffuse->iSamplerRegister, m_tBase.pDiffuse->iShaderConstantType);
	}

	if (m_tBase.pNormal)
	{
		if (m_tBase.pNormal->pTexture)
			m_tBase.pNormal->pTexture->SetTexture(m_tBase.pNormal->iTextureRegister, m_tBase.pNormal->iShaderConstantType);

		if (m_tBase.pNormal->pSampler)
			m_tBase.pNormal->pSampler->SetSampler(m_tBase.pNormal->iSamplerRegister, m_tBase.pNormal->iShaderConstantType);
	}

	if (m_tBase.pSpecular)
	{
		if (m_tBase.pSpecular->pTexture)
			m_tBase.pSpecular->pTexture->SetTexture(m_tBase.pSpecular->iTextureRegister, m_tBase.pSpecular->iShaderConstantType);

		if (m_tBase.pSpecular->pSampler)
			m_tBase.pSpecular->pSampler->SetSampler(m_tBase.pSpecular->iSamplerRegister, m_tBase.pSpecular->iShaderConstantType);
	}

	if (m_tMultiTex.pDiffuse)
	{
		if (m_tMultiTex.pDiffuse->pTexture)
			m_tMultiTex.pDiffuse->pTexture->SetTexture(m_tMultiTex.pDiffuse->iTextureRegister, m_tMultiTex.pDiffuse->iShaderConstantType);

		if (m_tMultiTex.pDiffuse->pSampler)
			m_tMultiTex.pDiffuse->pSampler->SetSampler(m_tMultiTex.pDiffuse->iSamplerRegister, m_tMultiTex.pDiffuse->iShaderConstantType);
	}

	if (m_tMultiTex.pNormal)
	{
		if (m_tMultiTex.pNormal->pTexture)
			m_tMultiTex.pNormal->pTexture->SetTexture(m_tMultiTex.pNormal->iTextureRegister, m_tMultiTex.pNormal->iShaderConstantType);

		if (m_tMultiTex.pNormal->pSampler)
			m_tMultiTex.pNormal->pSampler->SetSampler(m_tMultiTex.pNormal->iSamplerRegister, m_tMultiTex.pNormal->iShaderConstantType);
	}

	if (m_tMultiTex.pSpecular)
	{
		if (m_tMultiTex.pSpecular->pTexture)
			m_tMultiTex.pSpecular->pTexture->SetTexture(m_tMultiTex.pSpecular->iTextureRegister, m_tMultiTex.pSpecular->iShaderConstantType);

		if (m_tMultiTex.pSpecular->pSampler)
			m_tMultiTex.pSpecular->pSampler->SetSampler(m_tMultiTex.pSpecular->iSamplerRegister, m_tMultiTex.pSpecular->iShaderConstantType);
	}

	if (m_tMultiTex.pAlpha)
	{
		if (m_tMultiTex.pAlpha->pTexture)
			m_tMultiTex.pAlpha->pTexture->SetTexture(m_tMultiTex.pAlpha->iTextureRegister, m_tMultiTex.pAlpha->iShaderConstantType);

		if (m_tMultiTex.pAlpha->pSampler)
			m_tMultiTex.pAlpha->pSampler->SetSampler(m_tMultiTex.pAlpha->iSamplerRegister, m_tMultiTex.pAlpha->iShaderConstantType);
	}
}

CMaterial * CMaterial::Clone()
{
	return new CMaterial(*this);
}

void CMaterial::Save(const char * pFileName, const string & strPathKey)
{
}

void CMaterial::Save(FILE * pFile)
{
	// 재질 색상정보를 저장한다.
	fwrite(&m_tInfo, sizeof(MATERIALINFO), 1, pFile);

	if (m_tBase.pDiffuse)
	{

	}
}

void CMaterial::Load(const char * pFileName, const string & strPathKey)
{
}

void CMaterial::Load(FILE * pFile)
{
}
