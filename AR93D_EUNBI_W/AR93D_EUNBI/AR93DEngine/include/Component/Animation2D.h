#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CAnimation2D :
	public CComponent
{
private:
	friend class CGameObject;

private:
	CAnimation2D();
	CAnimation2D(const CAnimation2D& anim);
	~CAnimation2D();

private:
	vector<PANIMATIONCLIP2D>	m_vecAnimationClip;
	ANIMATION2DCBUFFER			m_tCBuffer;
	int							m_iCurrentAnimation;
	int							m_iDefaultAnimation;

public:
	void AddAnimationClip(const string& strName,
		ANIMATION2D_TYPE eType, ANIMATION_OPTION eOption,
		int iFrameMaxX, int iFrameMaxY, float fLimitTime,
		int iLoopCount = 0, float fLoopTime = 0.f,
		const string& strTexKey = "", int iTexRegister = 0, TCHAR* pFileName = NULL,
		const string& strPathKey = TEXTURE_PATH);
	void AddAnimationClip(const string& strName,
		ANIMATION2D_TYPE eType, ANIMATION_OPTION eOption,
		int iFrameMaxX, int iFrameMaxY, float fLimitTime,
		int iLoopCount = 0, float fLoopTime = 0.f,
		const string& strTexKey = "", int iTexRegister = 0, const vector<wstring>* vecFileName = NULL,
		const string& strPathKey = TEXTURE_PATH);
	void ChangeAnimation(int iAnimation);
	void SetDefaultAnimation(int iAnimation);
	void SetShader();
	void Start();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CAnimation2D* Clone();
};

AR3D_END
