#include "ColliderSphere.h"
#include "Transform.h"
#include "Camera.h"
#include "../Scene/Scene.h"
#include "ColliderRay.h"
#include "ColliderTerrain.h"
#include "../GameObject/GameObject.h"
#include "../Core/GameTotalManager.h"
#include "../Core/Input.h"

AR3D_USING

CColliderSphere::CColliderSphere()
{
	m_eCollGroup = CG_NORMAL;
	m_eCollType = CT_SPHERE;
	SetTag("ColliderSphere");
	SetTypeName("CColliderSphere");
	SetTypeID<CColliderSphere>();
}

CColliderSphere::CColliderSphere(const CColliderSphere & collider) :
	CCollider(collider)
{
	m_tInfo = collider.m_tInfo;
}

CColliderSphere::~CColliderSphere()
{
}

SPHEREINFO CColliderSphere::GetSphereInfo() const
{
	return m_tInfo;
}

void CColliderSphere::SetSphereInfo(const DxVector3 & vCenter, float fRadius)
{
	m_tInfo.vCenter = vCenter;
	m_tInfo.fRadius = fRadius;

	m_vPrevPos = m_pTransform->GetWorldPos();
}

void CColliderSphere::SetSphereInfo(float x, float y, float z, float fRadius)
{
	m_tInfo.vCenter = DxVector3(x, y, z);
	m_tInfo.fRadius = fRadius;

	m_vPrevPos = m_pTransform->GetWorldPos();
}

void CColliderSphere::SetSphereInfo(const SPHEREINFO & tSphere)
{
	m_tInfo = tSphere;

	m_vPrevPos = m_pTransform->GetWorldPos();
}

bool CColliderSphere::Init()
{
	SetMesh("ColorSphere");
	SetShader(COLLIDER_COLOR_SHADER);
	SetInputLayout("ColorInputLayout");

	return true;
}

void CColliderSphere::Input(float fTime)
{
}

void CColliderSphere::Update(float fTime)
{
	CCollider::Update(fTime);

	m_tInfo.vCenter += m_vMove;
}

void CColliderSphere::LateUpdate(float fTime)
{
}

void CColliderSphere::Collision(float fTime)
{
}

void CColliderSphere::Render(float fTime)
{
//#ifdef _DEBUG
	// Transform 구성
	
		CCamera*	pCamera = m_pScene->GetMainCamera();

		XMMATRIX	matScale, matPos;
		matScale = XMMatrixScaling(m_tInfo.fRadius * 2.f, m_tInfo.fRadius * 2.f,
			m_tInfo.fRadius * 2.f);
		matPos = XMMatrixTranslation(m_tInfo.vCenter.x, m_tInfo.vCenter.y,
			m_tInfo.vCenter.z);
		m_tTCBuffer.matWorld = matScale * matPos;
		m_tTCBuffer.matView = pCamera->GetViewMatrix();
		m_tTCBuffer.matProj = pCamera->GetProjMatrix();
		m_tTCBuffer.matWV = m_tTCBuffer.matWorld * m_tTCBuffer.matView;
		m_tTCBuffer.matWVP = m_tTCBuffer.matWV * m_tTCBuffer.matProj;

		SAFE_RELEASE(pCamera);

		m_tTCBuffer.matWorld = XMMatrixTranspose(m_tTCBuffer.matWorld);
		m_tTCBuffer.matView = XMMatrixTranspose(m_tTCBuffer.matView);
		m_tTCBuffer.matProj = XMMatrixTranspose(m_tTCBuffer.matProj);
		m_tTCBuffer.matWV = XMMatrixTranspose(m_tTCBuffer.matWV);
		m_tTCBuffer.matWVP = XMMatrixTranspose(m_tTCBuffer.matWVP);

		//#endif // _DEBUG

		CCollider::Render(fTime);
}

CColliderSphere * CColliderSphere::Clone()
{
	return new CColliderSphere(*this);
}

bool CColliderSphere::Collision(CCollider * pCollider)
{
	switch (pCollider->GetColliderType())
	{
	case CT_SPHERE:
		return CollisionSphereToSphere(m_tInfo, ((CColliderSphere*)pCollider)->m_tInfo);

	case CT_RAY:
		if (KEYPRESS("MouseLButton") || KEYPUSH("MouseLButton"))
		{
			if (GET_SINGLE(CGameTotalManager)->GetPickActor()->GetTag() == 
				m_pGameObject->GetTag()) // Pick된 Actor가 이미 자신일때 
			{
				GET_SINGLE(CGameTotalManager)->ErasePickActor();
				return false;
			}
			if(GET_SINGLE(CGameTotalManager)->GetPickActor() == NULL) // Pick된 Actor가 아무도 없을때 
				GET_SINGLE(CGameTotalManager)->SetPickActor(m_pGameObject);

			return CollisionRayToSphere(((CColliderRay*)pCollider)->GetRay(),
				m_tInfo);
		}
		
	case CT_TERRAIN_COLL:
		if (m_strTag == ("CameraCollider"))
		{
			CTransform*	pTransform = pCollider->GetTransform();
			bool bReturn = CollisionCameraTerrainToPosition(((CColliderTerrain*)pCollider)->GetInfo(),
				m_pTransform, pTransform->GetWorldScale());
			SAFE_RELEASE(pTransform);
			return bReturn;
		}
		else
		{
			CTransform*	pTransform = pCollider->GetTransform();
			bool bReturn = CollisionTerrainToPosition(((CColliderTerrain*)pCollider)->GetInfo(),
				m_pTransform, pTransform->GetWorldScale());
			SAFE_RELEASE(pTransform);
			return bReturn;
		}
	}

	return false;
}
