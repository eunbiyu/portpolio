#include "ColliderPoint.h"
#include "Transform.h"
#include "ColliderRect.h"


AR3D_USING

CColliderPoint::CColliderPoint()
{
	m_eCollType = CT_POINT;
	m_eCollGroup = CG_NORMAL;
	SetTag("ColliderPoint");
	SetTypeName("CColliderPoint");
	SetTypeID<CColliderPoint>();
}

CColliderPoint::CColliderPoint(const CColliderPoint & collider)
{
}


CColliderPoint::~CColliderPoint()
{
}

DxVector3 CColliderPoint::GetPoint() const
{
	return m_vPos;
}

bool CColliderPoint::Init()
{
	return true;
}

void CColliderPoint::Input(float fTime)
{
}

void CColliderPoint::Update(float fTime)
{
	m_vPos = m_pTransform->GetWorldPos();
}

void CColliderPoint::LateUpdate(float fTime)
{
}

void CColliderPoint::Collision(float fTime)
{
}

void CColliderPoint::Render(float fTime)
{
}

CColliderPoint * CColliderPoint::Clone()
{
	return new CColliderPoint(*this);
}

bool CColliderPoint::Collision(CCollider * pCollider)
{
	switch (pCollider->GetColliderType())
	{
	case CT_RECT:
		//if(pCollider->GetTag)
		return CollisionRectToPoint(((CColliderRect*)pCollider)->GetRectInfo(), m_vPos);

	case CT_POINT:
		return m_vPos == ((CColliderPoint*)pCollider)->m_vPos;
	}
	return false;
}
