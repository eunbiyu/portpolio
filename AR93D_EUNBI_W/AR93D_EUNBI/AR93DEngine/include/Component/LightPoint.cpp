#include "LightPoint.h"
#include "Transform.h"
#include "../Rendering/ShaderManager.h"

AR3D_USING

CLightPoint::CLightPoint()
{
	SetTag("LightPoint");
	SetTypeName("CLightPoint");
	SetTypeID<CLightPoint>();
	m_tInfo.eType = LT_POINT;
}


CLightPoint::CLightPoint(const CLightPoint & light) :
	CLight(light)
{
}

CLightPoint::~CLightPoint()
{
}

bool CLightPoint::Init()
{
	return true;
}

void CLightPoint::Input(float fTime)
{
}

void CLightPoint::Update(float fTime)
{
}

void CLightPoint::LateUpdate(float fTime)
{
}

void CLightPoint::Collision(float fTime)
{
}

void CLightPoint::Render(float fTime)
{
}

CLightPoint * CLightPoint::Clone()
{
	return new CLightPoint(*this);
}

void CLightPoint::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CLightPoint::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CLightPoint::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CLightPoint::SetLight()
{
	CLight::SetLight();

	m_tCBuffer.vPos = m_pTransform->GetWorldPos();

	GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Light", &m_tCBuffer, CUT_VERTEX | CUT_PIXEL);
}
