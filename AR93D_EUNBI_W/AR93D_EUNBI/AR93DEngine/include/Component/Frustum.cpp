#include "Frustum.h"
#include "../GameObject/GameObject.h"
#include "Camera.h"

AR3D_USING

CFrustum::CFrustum()
{
	SetTag("Frustum");
	SetTypeName("CFrustum");
	SetTypeID<CFrustum>();
	m_eComType = CT_FRUSTUM;
}


CFrustum::CFrustum(const CFrustum & frustum) :
	CComponent(frustum)
{
	memcpy(m_vPos, frustum.m_vPos, sizeof(DxVector3) * 8);
}

CFrustum::~CFrustum()
{
}

bool CFrustum::FrustumInPoint(const DxVector3 & vPos)
{
	XMVECTOR	vDist;

	for (int i = 0; i < FP_END; ++i)
	{
		vDist = XMPlaneDotCoord(m_Plane[i].Convert(), vPos.Convert());
		DxVector3	v = vDist;
		if (v.x > 0.f)
			return true;
	}

	return false;
}

bool CFrustum::FrustumInSphere(const SPHEREINFO & tSphere)
{
	XMVECTOR	vDist;

	for (int i = 0; i < FP_END; ++i)
	{
		vDist = XMPlaneDotCoord(m_Plane[i].Convert(), tSphere.vCenter.Convert());
		DxVector3	v = vDist;
		if (v.x - tSphere.fRadius > 0.f)
			return true;
	}

	return false;
}

bool CFrustum::FrustumInSphere(const DxVector3 & vCenter, float fRadius)
{
	XMVECTOR	vDist;

	for (int i = 0; i < FP_END; ++i)
	{
		vDist = XMPlaneDotCoord(m_Plane[i].Convert(), vCenter.Convert());
		DxVector3	v = vDist;
		if (v.x - fRadius > 0.f)
			return true;
	}

	return false;
}

bool CFrustum::Init()
{
	m_vPos[0] = DxVector3(-1.f, 1.f, 0.f);
	m_vPos[1] = DxVector3(1.f, 1.f, 0.f);
	m_vPos[2] = DxVector3(-1.f, -1.f, 0.f);
	m_vPos[3] = DxVector3(1.f, -1.f, 0.f);

	m_vPos[4] = DxVector3(-1.f, 1.f, 1.f);
	m_vPos[5] = DxVector3(1.f, 1.f, 1.f);
	m_vPos[6] = DxVector3(-1.f, -1.f, 1.f);
	m_vPos[7] = DxVector3(1.f, -1.f, 1.f);

	return true;
}

void CFrustum::Input(float fTime)
{
}

void CFrustum::Update(float fTime)
{
}

void CFrustum::LateUpdate(float fTime)
{
	// 투영 공간의 8개 정점을 월드공간으로 변환한다.
	// 로컬정점 * 월드행렬 * 뷰행렬 * 투영행렬 = 투영공간정점
	// 투영공간정점을 월드공간으로 변환하려면 투영행렬을 나눠주고
	// 뷰 행렬을 나눠주어야 한다.
	// 행렬은 나누기가 없으므로 역행렬을 곱해줘야 한다.
	DxVector3	vPos[8];

	// 카메라를 얻어온다.
	CCamera*	pCamera = FindComponentFromTypeID<CCamera>();

	// View, Proj를 얻어온다.
	XMMATRIX	matView, matProj;
	matView = pCamera->GetViewMatrix();
	matProj = pCamera->GetProjMatrix();

	XMMATRIX	matVP = matView * matProj;
	XMVECTOR	vDiterminent = XMMatrixDeterminant(matVP);
	matVP = XMMatrixInverse(&vDiterminent, matVP);

	for (int i = 0; i < 8; ++i)
	{
		vPos[i] = m_vPos[i].TransformCoord(matVP);
	}

	// 평면을 구성한다.
	// Left
	XMVECTOR	vPlane;
	vPlane = DirectX::XMPlaneFromPoints(vPos[4].Convert(),
		vPos[0].Convert(), vPos[2].Convert());
	m_Plane[FP_LEFT] = vPlane;

	// Right
	vPlane = DirectX::XMPlaneFromPoints(vPos[1].Convert(),
		vPos[5].Convert(), vPos[7].Convert());
	m_Plane[FP_RIGHT] = vPlane;

	// Top
	vPlane = DirectX::XMPlaneFromPoints(vPos[4].Convert(),
		vPos[5].Convert(), vPos[1].Convert());
	m_Plane[FP_TOP] = vPlane;

	// Bottom
	vPlane = DirectX::XMPlaneFromPoints(vPos[2].Convert(),
		vPos[3].Convert(), vPos[7].Convert());
	m_Plane[FP_BOTTOM] = vPlane;

	// Near
	vPlane = DirectX::XMPlaneFromPoints(vPos[0].Convert(),
		vPos[1].Convert(), vPos[3].Convert());
	m_Plane[FP_NEAR] = vPlane;

	// Far
	vPlane = DirectX::XMPlaneFromPoints(vPos[5].Convert(),
		vPos[4].Convert(), vPos[7].Convert());
	m_Plane[FP_FAR] = vPlane;

	SAFE_RELEASE(pCamera);
}

void CFrustum::Collision(float fTime)
{
}

void CFrustum::Render(float fTime)
{
}

CFrustum * CFrustum::Clone()
{
	return new CFrustum(*this);
}
