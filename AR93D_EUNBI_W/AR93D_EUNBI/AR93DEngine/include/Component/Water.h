#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CWater :
	public CComponent
{
private:
	CWater();
	CWater(const CWater& water);
	~CWater();

private:
	friend class CGameObject;

private:
	unsigned int	m_iVtxSizeX;
	unsigned int	m_iVtxSizeZ;
	vector<DxVector3>	m_vecPos;
	vector<VERTEXNORMAL>	m_vecVtx;
	string				m_strKey;
	CTexture*			m_Texture[MAXTEXNUM];				//스프레팅텍스쳐 객체
	WATERINFO			m_WaterInfo;
	float				WaterTime;

public:
	bool CreateWater(const string& strKey,
		unsigned int iVtxSizeX, unsigned int iVtxSizeZ,
		 const string& strPathKey = TEXTURE_PATH);

	void SetBaseTexture(const string& strKey, TCHAR* pFileName,
		const string& strPathKey = TEXTURE_PATH);
	void SetDetailTexture(const string& strKey, TCHAR* pFileName,
		const string& strPathKey = TEXTURE_PATH);
	void ResetWaterTime() { WaterTime = 0.f; }

public:
	void UpdateTerrainTexture(void);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CWater* Clone();
};

AR3D_END