#include "Terrain.h"
#include "../Resources/ResourcesManager.h"
#include "../Resources/Mesh.h"
#include "Renderer.h"
#include "Material.h"
#include "../GameObject/GameObject.h"
#include "../Core/PathManager.h"
#include "ColliderTerrain.h"
#include "../Scene/Scene.h"
#include "../Scene/SceneManager.h"
#include "../Device.h"
#include "../Resources/Texture.h"

AR3D_USING

CTerrain::CTerrain() :
	m_iVtxNumX(0),
	m_iVtxNumZ(0),
	m_iVtxSizeX(0),
	m_iVtxSizeZ(0)
{
	m_eComType = CT_TERRAIN;
	SetTag("Terrain");
	SetTypeName("CTerrain");
	SetTypeID<CTerrain>();
	m_tTerrainCBuffer.iSplatCount = 0;
	m_tPickTerrainCBuffer.vPos = DxVector3(0.f, 0.f, 0.f);
}

CTerrain::CTerrain(const CTerrain & terrain) :
	CComponent(terrain)
{
	m_iVtxNumX = terrain.m_iVtxNumX;
	m_iVtxNumZ = terrain.m_iVtxNumZ;
	m_iVtxSizeX = terrain.m_iVtxSizeX;
	m_iVtxSizeZ = terrain.m_iVtxSizeZ;
	m_tTerrainCBuffer = terrain.m_tTerrainCBuffer;
	m_tPickTerrainCBuffer.vPos = DxVector3(0.f, 0.f, 0.f);
}

CTerrain::~CTerrain()
{
}

void CTerrain::ToolOn(bool bTool)
{
	m_bTool = bTool;
	m_tPickTerrainCBuffer.fInnerRadius = 0.f;
	m_tPickTerrainCBuffer.fOuterRadius = 0.f;
}

void CTerrain::SetPickRadInfo(float fOuterRad, float fInnerRad)
{
	m_tPickTerrainCBuffer.fOuterRadius = fOuterRad;
	m_tPickTerrainCBuffer.fInnerRadius = fInnerRad;
}

PICKTERRAINCBUFFER CTerrain::GetPickRadInfo(void)
{
	return m_tPickTerrainCBuffer;
}

bool CTerrain::CreateTerrain(const string& strKey, unsigned int iVtxNumX,
	unsigned int iVtxNumZ, unsigned int iVtxSizeX,
	unsigned int iVtxSizeZ,
	char* pHeightMap, const string& strPathKey)
{

	m_strKey = strKey;

	m_vecPos.clear();

	m_iVtxNumX = iVtxNumX;
	m_iVtxNumZ = iVtxNumZ;
	m_iVtxSizeX = iVtxSizeX;
	m_iVtxSizeZ = iVtxSizeZ;

	m_vecPos.reserve(m_iVtxNumX * m_iVtxNumZ);

	unsigned char*	pData = NULL;
	vector<float>	vecY;

	if (pHeightMap)
	{
		// 풀경로를 만들어준다.
		const char* pPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);
		string	strPath;

		if (pPath)
			strPath = pPath;

		strPath += pHeightMap;

		FILE*	pFile = NULL;

		fopen_s(&pFile, strPath.c_str(), "rb");

		if (pFile)
		{
			BITMAPFILEHEADER	tfh;
			BITMAPINFOHEADER	tih;

			fread(&tfh, sizeof(tfh), 1, pFile);
			fread(&tih, sizeof(tih), 1, pFile);

			m_vecPos.reserve(tih.biWidth * tih.biHeight);

			int	iByte = tih.biBitCount / 8;
			pData = new unsigned char[tih.biWidth * tih.biHeight * iByte];

			fread(pData, 1, tih.biWidth * tih.biHeight * iByte, pFile);

			m_iVtxNumX = tih.biWidth;
			m_iVtxNumZ = tih.biHeight;

			for (int i = 0; i < m_iVtxNumZ; ++i)
			{
				for (int j = 0; j < m_iVtxNumX; ++j)
				{
					float	fY = *(pData + (i * m_iVtxNumX + j) * iByte);

					fY *= 0.7f;

					vecY.push_back(fY);
				}
			}

			SAFE_DELETE_ARRAY(pData);

			fclose(pFile);
		}
	}

	m_vecVtx.reserve(m_iVtxNumX * m_iVtxNumZ);

	for (int i = 0; i < m_iVtxNumZ; ++i)
	{
		for (int j = 0; j < m_iVtxNumX; ++j)
		{
			VERTEXBUMP	tVtx = {};

			tVtx.vPos = DxVector3(j * m_iVtxSizeX, 0.f,
				((m_iVtxNumZ - 1) - i) * m_iVtxSizeZ);

			if (!vecY.empty())
				tVtx.vPos.y = vecY[i * m_iVtxNumX + j];

			m_vecPos.push_back(tVtx.vPos);

			tVtx.vNormal = DxVector3(0.f, 0.f, 0.f);
			tVtx.vUV = DxVector2(j / (float)(m_iVtxNumX - 1),
				i / (float)(m_iVtxNumZ - 1));

			tVtx.vTangent = DxVector3(1.f, 0.f, 0.f);
			tVtx.vBinormal = DxVector3(0.f, 0.f, -1.f);

			m_vecVtx.push_back(tVtx);
		}
	}

	vector<UINT>	vecIndex;

	m_vecFaceNormal.reserve((m_iVtxNumX - 1) *(m_iVtxNumZ - 1) * 2); // 삼각형 개수만큼 

	for (int i = 0; i < m_iVtxNumZ - 1; ++i)
	{
		for (int j = 0; j < m_iVtxNumX - 1; ++j)
		{
			int	iAddr = i * m_iVtxNumX + j;

			// 우상단 삼각형
			vecIndex.push_back(iAddr);
			vecIndex.push_back(iAddr + 1);
			vecIndex.push_back(iAddr + m_iVtxNumX + 1);

			//면법선을 구한다.
			DxVector3	vEdge1 = m_vecPos[iAddr + 1] - m_vecPos[iAddr];
			DxVector3	vEdge2 = m_vecPos[iAddr + m_iVtxNumX + 1] - m_vecPos[iAddr];

			vEdge1 = vEdge1.Normalize();
			vEdge2 = vEdge2.Normalize();

			//노말벡터를 구한다. 
			DxVector3 vFaceNormal = vEdge1.Cross(vEdge2);
			vFaceNormal = vFaceNormal.Normalize();

			m_vecFaceNormal.push_back(vFaceNormal); // 한 점에 몇개의 면이 있는지 모르니 벡터에 푸쉬 해준후, 

			// 좌하단 삼각형
			vecIndex.push_back(iAddr);
			vecIndex.push_back(iAddr + m_iVtxNumX + 1);
			vecIndex.push_back(iAddr + m_iVtxNumX);

			vEdge1 = m_vecPos[iAddr + m_iVtxNumX + 1] - m_vecPos[iAddr];
			vEdge2 = m_vecPos[iAddr + m_iVtxNumX] - m_vecPos[iAddr];

			vEdge1 = vEdge1.Normalize();
			vEdge2 = vEdge2.Normalize();

			vFaceNormal = vEdge1.Cross(vEdge2);
			vFaceNormal = vFaceNormal.Normalize();

			m_vecFaceNormal.push_back(vFaceNormal);
		}
	}

	ComputeNormal(m_vecVtx, vecIndex);

	ComputeTangent(m_vecVtx, vecIndex);

	CMesh*	pMesh = GET_SINGLE(CResourcesManager)->CreateMesh(strKey,
		&m_vecVtx[0], m_vecVtx.size(), sizeof(VERTEXBUMP),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		&vecIndex[0], vecIndex.size(), sizeof(UINT));

	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	pRenderer->SetMesh(pMesh);
	pRenderer->SetRenderFlag(RF_CARTOON_ONLY);
	//pRenderer->SetRenderState(ALPHABLEND);

	SAFE_RELEASE(pRenderer);

	SAFE_RELEASE(pMesh);

	CColliderTerrain*	pColl = m_pGameObject->AddComponent<CColliderTerrain>("TerrainCollider");
	pColl->SetInfo(m_vecPos, m_iVtxNumX, m_iVtxNumZ, m_iVtxSizeX, m_iVtxSizeZ);

	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	pScene->SetTerrainColInfo(pColl->GetInfo());

	SAFE_RELEASE(pColl);

	return true;
}

void CTerrain::SetBaseTexture(const string & strKey, TCHAR * pFileName, const string & strPathKey)
{
	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", strKey, pFileName, strPathKey);

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

void CTerrain::SetNormalTexture(const string & strKey, TCHAR * pFileName, const string & strPathKey)
{
	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial();

	pMaterial->SetNormalMapTexture("Point", strKey, pFileName, strPathKey);
	pMaterial->SetNormalMapRegister(1, 1);

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

void CTerrain::SetSpecularTexture(const string & strKey, TCHAR * pFileName, const string & strPathKey)
{
	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial();

	pMaterial->SetSpecularTexture("Linear", strKey, pFileName, strPathKey);
	pMaterial->SetSpecularRegister(2, 2);

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

void CTerrain::SetSplatTexture(const string & strKey,
	vector<wstring> vecFileName, const string & strPathKey)
{
	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial();

	pMaterial->AddDiffuseMultiTexture(strKey, "Linear", 11, 11, CUT_PIXEL, vecFileName, strPathKey);

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

void CTerrain::SetSplatNormalTexture(const string & strKey, vector<wstring> vecFileName, const string & strPathKey)
{
	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial();

	pMaterial->AddNormalMultiTexture(strKey, "Point", 12, 12, CUT_PIXEL, vecFileName, strPathKey);

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

void CTerrain::SetSplatSpecularTexture(const string & strKey, vector<wstring> vecFileName, const string & strPathKey)
{
	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial();

	pMaterial->AddSpecularMultiTexture(strKey, "Linear", 13, 13, CUT_PIXEL, vecFileName, strPathKey);

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

void CTerrain::SetSplatAlphaTexture(const string & strKey, vector<wstring> vecFileName, const string & strPathKey)
{
	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();

	CMaterial*	pMaterial = pRenderer->GetMaterial();

	pMaterial->AddAlphaMultiTexture(strKey, "Point", 14, 14, CUT_PIXEL, vecFileName, strPathKey);

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);
}

void CTerrain::SetSplatCount(int iCount)
{
	m_tTerrainCBuffer.iSplatCount = iCount;
}

void CTerrain::SetDetailLevel(float fLevel)
{
	m_fDetailLevel = fLevel;
}

void CTerrain::UpdateTerrainTexture(void)
{
	//m_Texture
}

vector<DxVector3> CTerrain::GetVecPos()
{
	return m_vecPos;
}

float CTerrain::GetHeight(int idxX, int idxZ)
{
	return m_vecPos[((m_iVtxNumZ - 1) - idxZ) * m_iVtxNumX + idxX].y;
}

void CTerrain::SetNowSetSplatNum(int iNowSplatNumInTool)
{
	m_iNowSplatNumInTool = iNowSplatNumInTool;
}

int CTerrain::GetNowSetSplatNum()
{
	return m_iNowSplatNumInTool;
}

void CTerrain::ComputeNormal(vector<VERTEXBUMP>& vecVtx, const vector<UINT>& vecIndex)
{
	for (size_t i = 0; i < m_vecFaceNormal.size(); i++)
	{
		int idx0 = vecIndex[i * 3]; 
		int idx1 = vecIndex[i * 3 + 1];
		int idx2 = vecIndex[i * 3 + 2];

		vecVtx[idx0].vNormal += m_vecFaceNormal[i];
		vecVtx[idx1].vNormal += m_vecFaceNormal[i]; 
		vecVtx[idx2].vNormal += m_vecFaceNormal[i];
	}

	for (size_t i = 0; i < vecVtx.size(); ++i)
	{
		vecVtx[i].vNormal = vecVtx[i].vNormal.Normalize();
	}
}

void CTerrain::ComputeTangent(vector<VERTEXBUMP>& vecVtx, const vector<UINT>& vecIndex)
{
	for (size_t i = 0; i < m_vecFaceNormal.size(); ++i)
	{
		int	idx0 = vecIndex[i * 3];
		int	idx1 = vecIndex[i * 3 + 1];
		int	idx2 = vecIndex[i * 3 + 2];

		float	fVtx1[3], fVtx2[3];
		fVtx1[0] = vecVtx[idx1].vPos.x - vecVtx[idx0].vPos.x;
		fVtx1[1] = vecVtx[idx1].vPos.y - vecVtx[idx0].vPos.y;
		fVtx1[2] = vecVtx[idx1].vPos.z - vecVtx[idx0].vPos.z;

		fVtx2[0] = vecVtx[idx2].vPos.x - vecVtx[idx0].vPos.x;
		fVtx2[1] = vecVtx[idx2].vPos.y - vecVtx[idx0].vPos.y;
		fVtx2[2] = vecVtx[idx2].vPos.z - vecVtx[idx0].vPos.z;

		float	ftu[2], ftv[2];
		ftu[0] = vecVtx[idx1].vUV.x - vecVtx[idx0].vUV.x;
		ftv[0] = vecVtx[idx1].vUV.y - vecVtx[idx0].vUV.y;

		ftu[1] = vecVtx[idx2].vUV.x - vecVtx[idx0].vUV.x;
		ftv[1] = vecVtx[idx2].vUV.y - vecVtx[idx0].vUV.y;

		//UV좌표 비율 

		float fDen = 1.f / (ftu[0] * ftv[1] - ftu[1] - ftv[0]);
		DxVector3 vTangent = vTangent.Normalize();

		vecVtx[idx0].vTangent = vTangent;
		vecVtx[idx1].vTangent = vTangent;
		vecVtx[idx2].vTangent = vTangent;

		vecVtx[idx0].vBinormal = vecVtx[idx0].vNormal.Cross(vTangent).Normalize();
		vecVtx[idx1].vBinormal = vecVtx[idx1].vNormal.Cross(vTangent).Normalize();
		vecVtx[idx2].vBinormal = vecVtx[idx2].vNormal.Cross(vTangent).Normalize();
	}
}

bool CTerrain::Init()
{
	CRenderer*	pRenderer = m_pGameObject->AddComponent<CRenderer>("Renderer");

	assert(pRenderer);

	pRenderer->AddConstantBuffer("TerrainInfo", 10, sizeof(TERRAINCBUFFER),
		CUT_VERTEX | CUT_PIXEL);

	pRenderer->AddConstantBuffer("PickTerrainInfo", 11, sizeof(PICKTERRAINCBUFFER),
		CUT_VERTEX | CUT_PIXEL);

	pRenderer->SetShader(TERRAIN_SHADER);
	pRenderer->SetInputLayout("BumpInputLayout");

	SAFE_RELEASE(pRenderer);

	m_fDetailLevel = 50.f;

	return true;
}

void CTerrain::Input(float fTime)
{
}

void CTerrain::Update(float fTime)
{
}

void CTerrain::LateUpdate(float fTime)
{
	m_tTerrainCBuffer.fDetailLevel = m_fDetailLevel;

	CRenderer*	pRenderer = FindComponentFromTypeID<CRenderer>();
	CColliderTerrain*	pColl = m_pGameObject->FindComponentFromTypeID<CColliderTerrain>();

	m_tPickTerrainCBuffer.vPos = pColl->GetPickInfo();


	SAFE_RELEASE(pColl);

	assert(pRenderer);

	pRenderer->UpdateCBuffer("TerrainInfo", &m_tTerrainCBuffer);
	if(m_bTool)
	pRenderer->UpdateCBuffer("PickTerrainInfo", &m_tPickTerrainCBuffer);

	SAFE_RELEASE(pRenderer);
}

void CTerrain::Collision(float fTime)
{
}

void CTerrain::Render(float fTime)
{
}

CTerrain * CTerrain::Clone()
{
	return new CTerrain(*this);
}

void CTerrain::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CTerrain::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CTerrain::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}


