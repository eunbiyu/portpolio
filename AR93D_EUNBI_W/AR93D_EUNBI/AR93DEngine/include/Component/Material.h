#pragma once
#include "../BaseObj.h"

AR3D_BEGIN

typedef struct _tagTexture
{
	class CTexture*	pTexture;
	int		iTextureRegister;
	class CSampler*	pSampler;
	int		iSamplerRegister;
	int		iShaderConstantType;

	_tagTexture() :
		pTexture(NULL),
		pSampler(NULL),
		iTextureRegister(-1),
		iSamplerRegister(-1),
		iShaderConstantType(CUT_PIXEL)
	{
	}
}TEXTURE, *PTEXTURE;

typedef struct DLL _tagSkinInfo
{
	PTEXTURE		pDiffuse;
	PTEXTURE		pNormal;
	PTEXTURE		pSpecular;
	PTEXTURE		pAlpha;

	_tagSkinInfo() :
		pDiffuse(NULL),
		pNormal(NULL),
		pSpecular(NULL),
		pAlpha(NULL)
	{
	}
}SKININFO, *PSKININFO;

class DLL CMaterial :
	public CBaseObj
{
private:
	friend class CRenderer2D;
	friend class CRenderer;
	friend class CMesh;

private:
	CMaterial();
	CMaterial(const CMaterial& mtrl);
	~CMaterial();

private:
	MATERIALINFO	m_tInfo;
	SKININFO		m_tBase;
	SKININFO		m_tMultiTex;

public:
	MATERIALINFO GetMaterialInfo()	const;
	SKININFO GetBaseSkin()	const;
	void SetBaseSkin(const SKININFO& tBaseSkin);

public:
	void SetMaterialInfo(const MATERIALINFO& tInfo);
	void SetMaterialInfo(const DxVector4& vDif, const DxVector4& vAmb, const DxVector4& vSpc, const DxVector4& vEmv,
		float fSpecularPower);
	void SetMaterialInfo(const DxVector3& vDif, const DxVector3& vAmb, const DxVector3& vSpc, const DxVector3& vEmv,
		float fSpecularPower);
	void SetDiffuseColor(const DxVector4& vDif);

public:
	bool SetDiffuseTexture(const string& strSamplerKey, const string& strTextureKey,
		TCHAR* pFileName = NULL, const string& strPathKey = TEXTURE_PATH);
	bool SetDiffuseTextureFromFullPath(const string& strSamplerKey, const string& strKey, const char* pFullPath);
	bool SetDiffuseTexture(class CTexture* pTexture, int iTexRegister);
	void SetDiffuseRegister(int iSmpRegister = 0, int iTexRegister = 0);
	void SetDiffuseShaderConstantType(int iShaderConstantType);

	bool SetNormalMapTexture(const string& strSamplerKey, const string& strTextureKey,
		TCHAR* pFileName = NULL, const string& strPathKey = TEXTURE_PATH);
	bool SetNormalMapTextureFromFullPath(const string& strSamplerKey, const string& strKey, const char* pFullPath);
	bool SetNormalMapTexture(class CTexture* pTexture, int iTexRegister);
	void SetNormalMapRegister(int iSmpRegister = 0, int iTexRegister = 0);
	void SetNormalMapShaderConstantType(int iShaderConstantType);

	bool SetSpecularTexture(const string& strSamplerKey, const string& strTextureKey,
		TCHAR* pFileName = NULL, const string& strPathKey = TEXTURE_PATH);
	bool SetSpecularTextureFromFullPath(const string& strSamplerKey, const string& strKey, const char* pFullPath);
	bool SetSpecularTexture(class CTexture* pTexture, int iTexRegister);
	void SetSpecularRegister(int iSmpRegister = 0, int iTexRegister = 0);
	void SetSpecularShaderConstantType(int iShaderConstantType);

public:
	void AddDiffuseMultiTexture(const string& strDifTexKey, const string& strDifSmpKey,
		int iDifTexRegister, int iDifSmpRegister, int iShaderType,
		vector<wstring> vecFileName, const string& strPathKey = TEXTURE_PATH);
	void AddNormalMultiTexture(const string& strNrmTexKey, const string& strNrmSmpKey,
		int iNrmTexRegister, int iNrmSmpRegister, int iShaderType,
		vector<wstring> vecFileName, const string& strPathKey = TEXTURE_PATH);
	void AddSpecularMultiTexture(const string& strSpcTexKey, const string& strSpcSmpKey,
		int iSpcTexRegister, int iSpcSmpRegister, int iShaderType,
		vector<wstring> vecFileName, const string& strPathKey = TEXTURE_PATH);
	void AddAlphaMultiTexture(const string& strAlphaTexKey, const string& strAlphaSmpKey,
		int iAlphaTexRegister, int iAlphaSmpRegister, int iShaderType,
		vector<wstring> vecFileName, const string& strPathKey = TEXTURE_PATH);

public:
	bool Init();
	void SetMaterial();
	CMaterial* Clone();
	void Save(const char* pFileName, const string& strPathKey);
	void Save(FILE* pFile);
	void Load(const char* pFileName, const string& strPathKey);
	void Load(FILE* pFile);
};

AR3D_END
