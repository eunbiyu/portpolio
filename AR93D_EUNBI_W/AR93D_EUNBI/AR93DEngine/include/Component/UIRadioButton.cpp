#include "UIRadioButton.h"
#include "../GameObject/GameObject.h"
#include "Renderer2D.h"
#include "Transform.h"
#include "ColliderSphere.h"
#include "Material.h"
#include "../Scene/Layer.h"
#include "../Scene/Scene.h"
#include "ColliderRect.h"

AR3D_USING

CUIRadioButton::CUIRadioButton()
{
	SetTag("RadioButton");
	SetTypeName("CUIRadioButton");
	SetTypeID<CUIRadioButton>();
	m_eUIType = UT_RADIOBUTTON;
	m_eBtnState = BS_NONE;

	m_bCallback = false;
	m_bClickOn = false;
}


CUIRadioButton::CUIRadioButton(const CUIRadioButton & button) :
	CUIButton(button)
{
}

CUIRadioButton::~CUIRadioButton()
{
}

void CUIRadioButton::SetClick(bool bClick)
{
	m_bClickOn = bClick;
}


void CUIRadioButton::LateUpdate(float fTime)
{
	CRenderer2D*	pRenderer = m_pGameObject->FindComponentFromTypeID<CRenderer2D>();

	CMaterial*	pMaterial = pRenderer->GetMaterial(0, 0);

	switch (m_eBtnState)
	{
	case BS_NONE:
		if (m_bClickOn) 
		{
			pMaterial->SetDiffuseTexture("Linear", "RadioButtonOn", L"UIBox/RadioOn.png");
			pMaterial->SetDiffuseColor(m_vMouseOnCol);
		}
		else 
		{
			pMaterial->SetDiffuseTexture("Linear", "RadioButtonOff", L"UIBox/RadioOff.png");
			pMaterial->SetDiffuseColor(m_vNormalCol);
		}
		break;

	case BS_MOUSEON:
		pMaterial->SetDiffuseColor(m_vClickCol);
		break;
	case BS_CLICK:
		SetClick(true);
		if (m_bCallback)
			m_Callback(m_pGameObject, fTime);
		break;
	}

	SAFE_RELEASE(pMaterial);
	SAFE_RELEASE(pRenderer);
}

void CUIRadioButton::Render(float fTime)
{
}

CUIRadioButton * CUIRadioButton::Clone()
{
	return new	CUIRadioButton(*this);
}