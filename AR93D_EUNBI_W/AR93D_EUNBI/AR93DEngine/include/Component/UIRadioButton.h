#pragma once
#include "UIButton.h"

AR3D_BEGIN

class DLL CUIRadioButton :
	public CUIButton
{
protected:
	friend class CGameObject;

private:
	bool	m_bClickOn;

public:
	CUIRadioButton(); 
	CUIRadioButton(const CUIRadioButton& button);
	~CUIRadioButton();

public:
	void SetClick(bool bClick); // 클릭된지 안된지를 나타내줌.  

public:
	virtual void LateUpdate(float fTime);
	virtual void Render(float fTime);
	virtual CUIRadioButton* Clone();
};

AR3D_END
