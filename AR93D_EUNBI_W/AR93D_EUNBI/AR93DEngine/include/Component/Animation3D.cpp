#include "Animation3D.h"
#include "Animation3DClip.h"
#include "../GameObject/GameObject.h"
#include "../Resources/Texture.h"
#include "Renderer.h"
#include "../Core/PathManager.h"
#include "../Resources/FbxLoader.h"

AR3D_USING

CAnimation3D::CAnimation3D() :
	m_pCurClip(NULL),
	m_pNextClip(NULL),
	m_pBoneTex(NULL),
	m_pLastInsertClip(NULL),
	m_bChange(false),
	m_bEnd(false),
	m_fChangeLimitTime(0.05f),
	m_fChangeTime(0.f),
	m_fAnimationTime(0.f),
	m_fAnimationProgress(0.f),
	m_fFrameTime(1.f / 30.f)
{
	SetTag("Animation3D");
	SetTypeName("CAnimation3D");
	SetTypeID<CAnimation3D>();
	m_eComType = CT_ANIMATION3D;
}

CAnimation3D::CAnimation3D(const CAnimation3D & anim) :
	CComponent(anim)
{
	m_fAnimationProgress = 0.f;
	m_bEnd = false;

	m_vecBones.clear();
	for (size_t i = 0; i < anim.m_vecBones.size(); ++i)
	{
		PBONE	pBone = new BONE;
		pBone->strName = anim.m_vecBones[i]->strName;
		pBone->iDepth = anim.m_vecBones[i]->iDepth;
		pBone->iParentIndex = anim.m_vecBones[i]->iParentIndex;
		pBone->matOffset = new MATRIX;
		pBone->matBone = new MATRIX;
		*pBone->matOffset = *anim.m_vecBones[i]->matOffset;
		*pBone->matBone = *anim.m_vecBones[i]->matBone;

		/*for (size_t j = 0; j < anim.m_vecBones[i]->vecKeyFrame.size(); ++j)
		{
		PKEYFRAME	pFrame = new KEYFRAME;
		*pFrame = *anim.m_vecBones[i]->vecKeyFrame[j];
		pBone->vecKeyFrame.push_back(pFrame);
		}*/

		m_vecBones.push_back(pBone);
	}

	m_mapClip.clear();
	unordered_map<string, class CAnimation3DClip*>::const_iterator	iter;
	unordered_map<string, class CAnimation3DClip*>::const_iterator	iterEnd = anim.m_mapClip.end();

	for (iter = anim.m_mapClip.begin(); iter != iterEnd; ++iter)
	{
		CAnimation3DClip* pClip = iter->second->Clone();

		m_mapClip.insert(make_pair(iter->first, pClip));

		m_pLastInsertClip = pClip;
	}

	m_iAnimationLimitFrame = anim.m_iAnimationLimitFrame;

	m_strDefaultClip = anim.m_strDefaultClip;
	m_strCurClip = anim.m_strCurClip;

	m_pCurClip = FindClip(m_strCurClip);
	m_pNextClip = NULL;

	m_bChange = false;
	m_fChangeTime = 0.f;
	m_fAnimationTime = 0.f;
	m_fChangeLimitTime = anim.m_fChangeLimitTime;
	m_fFrameTime = anim.m_fFrameTime;

	m_pBoneTex = NULL;

	CreateBoneTexture();
}

CAnimation3D::~CAnimation3D()
{
	for (size_t i = 0; i < m_vecBones.size(); ++i)
	{
		SAFE_DELETE(m_vecBones[i]->matBone);
		SAFE_DELETE(m_vecBones[i]->matOffset);
		//Safe_Delete_VecList(m_vecBones[i]->vecKeyFrame);
	}

	SAFE_RELEASE(m_pBoneTex);
	SAFE_RELEASE(m_pNextClip);
	SAFE_RELEASE(m_pCurClip);
	Safe_Release_Map(m_mapClip);
	Safe_Delete_VecList(m_vecBones);
}

const unordered_map<string, class CAnimation3DClip*>* CAnimation3D::GetClips() const
{
	return &m_mapClip;
}

CAnimation3DClip * CAnimation3D::GetLastInsertClip() const
{
	return m_pLastInsertClip;
}

bool CAnimation3D::GetAnimationEnd() const
{
	return m_bEnd;
}

float CAnimation3D::GetAnimationProgress() const
{
	return m_fAnimationProgress;
}

int CAnimation3D::GetAnimationProgressFrame() const
{
	return (m_fAnimationProgress * m_pCurClip->m_tInfo.fTimeLength + m_pCurClip->m_tInfo.fStartTime) *
		m_iAnimationLimitFrame;
}

string CAnimation3D::GetCurrentClipName() const
{
	return m_strCurClip;
}

int CAnimation3D::GetCurrentClipStartFrame() const
{
	return m_pCurClip->m_tInfo.iStartFrame;
}

int CAnimation3D::GetCurrentClipEndFrame() const
{
	return m_pCurClip->m_tInfo.iEndFrame;
}

ANIMATION_OPTION CAnimation3D::GetCurrentClipOption() const
{
	return m_pCurClip->m_tInfo.eOption;
}

int CAnimation3D::GetNextClipStartFrame() const
{
	if (!m_pNextClip)
		return -1;

	return m_pNextClip->m_tInfo.iStartFrame;
}

int CAnimation3D::GetNextClipEndFrame() const
{
	if (!m_pNextClip)
		return -1;

	return m_pNextClip->m_tInfo.iEndFrame;
}

ANIMATION_OPTION CAnimation3D::GetNextClipOption() const
{
	if (!m_pNextClip)
		return AO_NONE;

	return m_pNextClip->m_tInfo.eOption;
}

PBONE CAnimation3D::FindBone(const string & strName) const
{
	for (size_t i = 0; i < m_vecBones.size(); ++i)
	{
		if (m_vecBones[i]->strName == strName)
			return m_vecBones[i];
	}

	return NULL;
}

bool CAnimation3D::LoadFbxClip(const wchar_t * pFileName, const string & strPathKey)
{
	char	strFileName[MAX_PATH] = {};
	WideCharToMultiByte(CP_ACP, 0, pFileName, -1,
		strFileName, lstrlen(pFileName), 0, 0);

	CFbxLoader	loader;
	if (!loader.LoadFBX(pFileName, FLT_ANIMATION, strPathKey))
		return false;

	if (m_vecBones.size() != loader.GetBones()->size())
		return false;

	const vector<PFBXANIMATIONCLIP>* pvecClip = loader.GetClip();

	// 클립을 읽어온다.
	vector<PFBXANIMATIONCLIP>::const_iterator	iterC;
	vector<PFBXANIMATIONCLIP>::const_iterator	iterCEnd = pvecClip->end();

	for (iterC = pvecClip->begin(); iterC != iterCEnd; ++iterC)
	{
		AddClip(AO_LOOP, *iterC);
	}

	return true;
}

bool CAnimation3D::LoadFbxClip(const char * pFileName, const string & strPathKey)
{
	CFbxLoader	loader;
	if (!loader.LoadFBX(pFileName, FLT_ANIMATION, strPathKey))
		return false;

	if (m_vecBones.size() != loader.GetBones()->size())
		return false;

	const vector<PFBXANIMATIONCLIP>* pvecClip = loader.GetClip();

	// 클립을 읽어온다.
	vector<PFBXANIMATIONCLIP>::const_iterator	iterC;
	vector<PFBXANIMATIONCLIP>::const_iterator	iterCEnd = pvecClip->end();

	for (iterC = pvecClip->begin(); iterC != iterCEnd; ++iterC)
	{
		AddClip(AO_LOOP, *iterC);
	}

	return true;
}

bool CAnimation3D::LoadFbxClipFromFullPath(const wchar_t * pFileName)
{
	CFbxLoader	loader;
	if (!loader.LoadFBXFullPath(pFileName, FLT_ANIMATION))
		return false;

	if (m_vecBones.size() != loader.GetBones()->size())
		return false;

	const vector<PFBXANIMATIONCLIP>* pvecClip = loader.GetClip();

	// 클립을 읽어온다.
	vector<PFBXANIMATIONCLIP>::const_iterator	iterC;
	vector<PFBXANIMATIONCLIP>::const_iterator	iterCEnd = pvecClip->end();

	for (iterC = pvecClip->begin(); iterC != iterCEnd; ++iterC)
	{
		AddClip(AO_LOOP, *iterC);
	}

	return true;
}

bool CAnimation3D::LoadFbxClipFromFullPath(const char * pFileName)
{
	CFbxLoader	loader;
	if (!loader.LoadFBXFullPath(pFileName, FLT_ANIMATION))
		return false;

	if (m_vecBones.size() != loader.GetBones()->size())
		return false;

	const vector<PFBXANIMATIONCLIP>* pvecClip = loader.GetClip();

	// 클립을 읽어온다.
	vector<PFBXANIMATIONCLIP>::const_iterator	iterC;
	vector<PFBXANIMATIONCLIP>::const_iterator	iterCEnd = pvecClip->end();

	for (iterC = pvecClip->begin(); iterC != iterCEnd; ++iterC)
	{
		AddClip(AO_LOOP, *iterC);
	}

	return true;
}

void CAnimation3D::AddClipCallback(const string & strName, int iFrame, void(*pFunc)(float))
{
	CAnimation3DClip*	pClip = FindClip(strName);

	if (!pClip)
		return;

	pClip->AddCallback(iFrame, pFunc);
}

void CAnimation3D::AddClipCallback(const string & strName, float fProgress, void(*pFunc)(float))
{
	CAnimation3DClip*	pClip = FindClip(strName);

	if (!pClip)
		return;

	pClip->AddCallback(fProgress, pFunc);
}

void CAnimation3D::ReturnToDefaultClip()
{
	ChangeClip(m_strDefaultClip);
}

void CAnimation3D::ClearClip()
{
	m_strDefaultClip = "";
	m_strCurClip = "";
	SAFE_RELEASE(m_pCurClip);
	Safe_Release_Map(m_mapClip);
	m_fChangeTime = 0.f;
	m_fAnimationTime = 0.f;
}

CTexture * CAnimation3D::GetBoneTexture() const
{
	if (m_pBoneTex)
		m_pBoneTex->AddRef();

	return m_pBoneTex;
}

void CAnimation3D::SetChangeTime(float fTime)
{
	m_fChangeLimitTime = fTime;
}

void CAnimation3D::AddBone(PBONE pBone)
{
	m_vecBones.push_back(pBone);
}

void CAnimation3D::AddClip(ANIMATION_OPTION eOption,
	PFBXANIMATIONCLIP pClip)
{
	CAnimation3DClip*	pAniClip = FindClip(pClip->strName);

	if (pAniClip)
	{
		SAFE_RELEASE(pAniClip);
		return;
	}
	pAniClip = new CAnimation3DClip;

	pAniClip->SetClipInfo(eOption, pClip);

	if (!pAniClip->Init())
	{
		SAFE_RELEASE(pAniClip);
		return;
	}

	switch (pClip->eTimeMode)
	{
	case FbxTime::eFrames24:
		m_iAnimationLimitFrame = 24;
		break;
	case FbxTime::eFrames30:
		m_iAnimationLimitFrame = 30;
		break;
	case FbxTime::eFrames60:
		m_iAnimationLimitFrame = 60;
		break;
	}

	pAniClip->m_iAnimationLimitFrame = m_iAnimationLimitFrame;
	m_fFrameTime = 1.f / m_iAnimationLimitFrame;

	if (m_mapClip.empty())
	{
		SetDefaultClipName(pClip->strName);
		SetCurClipName(pClip->strName);
		m_pCurClip = pAniClip;
		pAniClip->AddRef();
	}

	m_mapClip.insert(make_pair(pClip->strName, pAniClip));

	m_pLastInsertClip = pAniClip;
}

void CAnimation3D::AddClip(const string & strKey,
	ANIMATION_OPTION eOption, int iAnimationLimitFrame, int iStartFrame,
	int iEndFrame, float fStartTime, float fEndTime)
{
	CAnimation3DClip*	pAniClip = FindClip(strKey);

	if (pAniClip)
	{
		SAFE_RELEASE(pAniClip);
		return;
	}

	m_iAnimationLimitFrame = iAnimationLimitFrame;

	pAniClip = new CAnimation3DClip;

	pAniClip->SetClipInfo(strKey, eOption, iAnimationLimitFrame,
		iStartFrame, iEndFrame, fStartTime, fEndTime);

	if (!pAniClip->Init())
	{
		SAFE_RELEASE(pAniClip);
		return;
	}

	pAniClip->m_iAnimationLimitFrame = m_iAnimationLimitFrame;
	m_fFrameTime = 1.f / m_iAnimationLimitFrame;

	if (m_mapClip.empty())
	{
		SetDefaultClipName(strKey);
		SetCurClipName(strKey);
		m_pCurClip = pAniClip;
		pAniClip->AddRef();
	}

	m_mapClip.insert(make_pair(strKey, pAniClip));

	m_pLastInsertClip = pAniClip;
}

void CAnimation3D::AddClip(const string & strKey,
	ANIMATION_OPTION eOption, int iStartFrame, int iEndFrame)
{
	CAnimation3DClip*	pAniClip = FindClip(strKey);

	if (pAniClip)
	{
		SAFE_RELEASE(pAniClip);
		return;
	}

	pAniClip = new CAnimation3DClip;

	pAniClip->m_iAnimationLimitFrame = m_iAnimationLimitFrame;
	m_fFrameTime = 1.f / m_iAnimationLimitFrame;

	float	fStartTime, fEndTime;
	fStartTime = iStartFrame * m_fFrameTime;
	fEndTime = iEndFrame * m_fFrameTime;

	pAniClip->SetClipInfo(strKey, eOption, m_iAnimationLimitFrame,
		iStartFrame, iEndFrame, fStartTime, fEndTime);

	if (!pAniClip->Init())
	{
		SAFE_RELEASE(pAniClip);
		return;
	}


	if (m_mapClip.empty())
	{
		SetDefaultClipName(strKey);
		SetCurClipName(strKey);
		m_pCurClip = pAniClip;
		pAniClip->AddRef();
	}

	m_mapClip.insert(make_pair(strKey, pAniClip));

	m_pLastInsertClip = pAniClip;
}

void CAnimation3D::ChangeClipInfo(const string & strKey, ANIMATION_OPTION eOption, int iStartFrame, int iEndFrame)
{
	CAnimation3DClip*	pAniClip = FindClip(strKey);

	if (!pAniClip)
		return;

	float	fStartTime, fEndTime;
	fStartTime = iStartFrame * m_fFrameTime;
	fEndTime = iEndFrame * m_fFrameTime;

	pAniClip->m_iAnimationLimitFrame = m_iAnimationLimitFrame;
	pAniClip->SetClipInfo(strKey, eOption, m_iAnimationLimitFrame,
		iStartFrame, iEndFrame, fStartTime, fEndTime);
}

void CAnimation3D::ChangeClipInfo(const string & strKey,
	const string & strName, ANIMATION_OPTION eOption, int iStartFrame, int iEndFrame)
{
	CAnimation3DClip*	pAniClip = FindClip(strKey);

	if (!pAniClip)
		return;

	float	fStartTime, fEndTime;
	fStartTime = iStartFrame * m_fFrameTime;
	fEndTime = iEndFrame * m_fFrameTime;

	pAniClip->m_iAnimationLimitFrame = m_iAnimationLimitFrame;
	pAniClip->SetClipInfo(strName, eOption, m_iAnimationLimitFrame,
		iStartFrame, iEndFrame, fStartTime, fEndTime);

	unordered_map<string, class CAnimation3DClip*>::iterator	iter = m_mapClip.find(strKey);

	m_mapClip.erase(iter);

	m_mapClip.insert(make_pair(strName, pAniClip));
}

CAnimation3DClip * CAnimation3D::FindClip(const string & strKey)
{
	unordered_map<string, class CAnimation3DClip*>::iterator	iter = m_mapClip.find(strKey);

	if (iter == m_mapClip.end())
		return NULL;

	iter->second->AddRef();

	return iter->second;
}

void CAnimation3D::SetDefaultClipName(const string & strName)
{
	m_strDefaultClip = strName;
}

void CAnimation3D::SetCurClipName(const string & strName)
{
	m_strCurClip = strName;
	SAFE_RELEASE(m_pCurClip);
	m_pCurClip = FindClip(strName);
}

void CAnimation3D::ChangeClip(const string & strName)
{
	if (!m_bChange && m_strCurClip == strName)
		return;

	else if (m_bChange && m_strNextClip == strName)
		return;

	/*else if (m_strCurClip == strName)
	{
	m_bChange = false;
	m_strNextClip = "";
	m_bChange = false;
	m_fChangeTime = 0.f;
	return;
	}*/

	//SAFE_RELEASE(m_pCurClip);

	//m_pCurClip = FindClip(strName);

	SAFE_RELEASE(m_pNextClip);

	m_strNextClip = strName;

	m_pNextClip = FindClip(strName);

	if (!m_pNextClip)
		return;

	m_bChange = true;
	m_fChangeTime = 0.f;

}

bool CAnimation3D::CreateBoneTexture()
{
	SAFE_RELEASE(m_pBoneTex);

	m_pBoneTex = CTexture::CreateTexture("BoneTex",
		m_vecBones.size() * 4, 1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT,
		D3D11_USAGE_DYNAMIC, D3D11_BIND_SHADER_RESOURCE,
		D3D11_CPU_ACCESS_WRITE);

	if (!m_pBoneTex)
		return false;

	/*CRenderer*	pRenderer = (CRenderer*)FindComponentFromType(CT_RENDERER);

	pRenderer->SetBoneTexture(m_pBoneTex);

	SAFE_RELEASE(pRenderer);*/

	return true;
}

bool CAnimation3D::Save(const char * pFileName, const string & strPathKey)
{
	// 전체 경로를 만들어준다.
	const char*	pPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	string	strPath;
	if (pPath)
		strPath = pPath;

	strPath += pFileName;

	return SaveFromFullPath(strPath.c_str());
}

bool CAnimation3D::Save(FILE * pFile)
{
	// 클립정보를 저장한다.
	int iSize = m_mapClip.size();
	fwrite(&iSize, 4, 1, pFile);

	unordered_map<string, class CAnimation3DClip*>::iterator	iter;
	unordered_map<string, class CAnimation3DClip*>::iterator	iterEnd = m_mapClip.end();

	for (iter = m_mapClip.begin(); iter != iterEnd; ++iter)
	{
		iter->second->Save(pFile);
	}

	fwrite(&m_iAnimationLimitFrame, 4, 1, pFile);

	int	iLength = m_strDefaultClip.length();
	fwrite(&iLength, 4, 1, pFile);
	fwrite(m_strDefaultClip.c_str(), 1, iLength, pFile);

	iLength = m_strCurClip.length();
	fwrite(&iLength, 4, 1, pFile);
	fwrite(m_strCurClip.c_str(), 1, iLength, pFile);

	fwrite(&m_fChangeLimitTime, 4, 1, pFile);
	fwrite(&m_fFrameTime, 4, 1, pFile);

	return true;
}

bool CAnimation3D::SaveFromFullPath(const char * pFileName)
{
	FILE*	pFile = NULL;

	fopen_s(&pFile, pFileName, "wb");

	if (!pFile)
		return false;

	Save(pFile);

	fclose(pFile);

	return true;
}

bool CAnimation3D::SaveClipFromFullPath(const char * pFullPath)
{
	FILE*	pFile = NULL;

	fopen_s(&pFile, pFullPath, "wb");

	if (!pFile)
		return false;

	SaveClip(pFile);

	fclose(pFile);

	return true;
}

bool CAnimation3D::SaveClipFromFullPath(const string & strClipName,
	const char * pFullPath)
{
	CAnimation3DClip*	pClip = FindClip(strClipName);

	if (!pClip)
		return false;

	FILE*	pFile = NULL;

	fopen_s(&pFile, pFullPath, "wb");

	if (!pFile)
		return false;

	pClip->Save(pFile);

	fclose(pFile);

	return true;
}

bool CAnimation3D::SaveClip(FILE * pFile)
{
	// 클립정보를 저장한다.
	size_t iSize = m_mapClip.size();
	fwrite(&iSize, 4, 1, pFile);

	unordered_map<string, class CAnimation3DClip*>::iterator	iter;
	unordered_map<string, class CAnimation3DClip*>::iterator	iterEnd = m_mapClip.end();

	for (iter = m_mapClip.begin(); iter != iterEnd; ++iter)
	{
		iter->second->Save(pFile);
	}

	fwrite(&m_iAnimationLimitFrame, 4, 1, pFile);

	int	iLength = m_strDefaultClip.length();
	fwrite(&iLength, 4, 1, pFile);
	fwrite(m_strDefaultClip.c_str(), 1, iLength, pFile);

	iLength = m_strCurClip.length();
	fwrite(&iLength, 4, 1, pFile);
	fwrite(m_strCurClip.c_str(), 1, iLength, pFile);

	fwrite(&m_fChangeLimitTime, 4, 1, pFile);
	fwrite(&m_fFrameTime, 4, 1, pFile);

	return true;
}

bool CAnimation3D::SaveBoneFromFullPath(const char * pFullPath)
{
	FILE*	pFile = NULL;

	fopen_s(&pFile, pFullPath, "wb");

	if (!pFile)
		return false;

	int	iSize = m_vecBones.size();

	// 본 수를 저장한다.
	fwrite(&iSize, 4, 1, pFile);

	for (int i = 0; i < iSize; ++i)
	{
		// 이름 저장
		int	iLength = m_vecBones[i]->strName.length();

		fwrite(&iLength, 4, 1, pFile);
		fwrite(m_vecBones[i]->strName.c_str(), 1, iLength, pFile);

		// 깊이 저장
		fwrite(&m_vecBones[i]->iDepth, 4, 1, pFile);
		fwrite(&m_vecBones[i]->iParentIndex, 4, 1, pFile);

		for (int j = 0; j < 4; ++j)
		{
			for (int k = 0; k < 4; ++k)
			{
				fwrite(&m_vecBones[i]->matOffset->m[j][k], 4, 1, pFile);
			}
		}

		for (int j = 0; j < 4; ++j)
		{
			for (int k = 0; k < 4; ++k)
			{
				fwrite(&m_vecBones[i]->matBone->m[j][k], 4, 1, pFile);
			}
		}
	}

	fclose(pFile);

	return true;
}

bool CAnimation3D::Load(const char * pFileName, const string & strPathKey)
{
	// 전체 경로를 만들어준다.
	const char*	pPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	string	strPath;
	if (pPath)
		strPath = pPath;

	strPath += pFileName;

	return LoadFromFullPath(strPath.c_str());
}

bool CAnimation3D::Load(FILE * pFile)
{
	// 클립정보를 저장한다.
	Safe_Release_Map(m_mapClip);
	int iSize = 0;
	fread(&iSize, 4, 1, pFile);

	for (int i = 0; i < iSize; ++i)
	{
		CAnimation3DClip*	pClip = new CAnimation3DClip;

		pClip->Load(pFile);

		if (pClip->m_tInfo.strName == "Run")
			pClip->m_tInfo.eOption = AO_LOOP;

		else if (pClip->m_tInfo.strName == "Walk")
			pClip->m_tInfo.eOption = AO_LOOP;

		m_mapClip.insert(make_pair(pClip->m_tInfo.strName, pClip));

		m_pLastInsertClip = pClip;
	}

	fread(&m_iAnimationLimitFrame, 4, 1, pFile);

	unordered_map<string, class CAnimation3DClip*>::iterator	iter;
	unordered_map<string, class CAnimation3DClip*>::iterator	iterEnd = m_mapClip.end();

	for (iter = m_mapClip.begin(); iter != iterEnd; ++iter)
	{
		iter->second->m_iAnimationLimitFrame = m_iAnimationLimitFrame;
	}

	int		iLength;
	char	strName[256] = {};
	fread(&iLength, 4, 1, pFile);
	fread(strName, 1, iLength, pFile);
	m_strDefaultClip = strName;

	iLength = 0;
	fread(&iLength, 4, 1, pFile);
	fread(strName, 1, iLength, pFile);
	m_strCurClip = strName;

	fread(&m_fChangeLimitTime, 4, 1, pFile);
	fread(&m_fFrameTime, 4, 1, pFile);

	SetCurClipName(m_strCurClip);

	m_strNextClip = "";
	//m_pCurClip = FindClip(m_strCurClip);
	m_pNextClip = NULL;
	m_bChange = false;
	m_fChangeTime = 0.f;
	m_fAnimationTime = 0.f;

	//m_fChangeLimitTime = 0.05f;

	//SAFE_RELEASE(m_pBoneTex);

	//CreateBoneTexture();

	return true;
}

bool CAnimation3D::LoadFromFullPath(const char * pFileName)
{
	FILE*	pFile = NULL;

	fopen_s(&pFile, pFileName, "rb");

	if (!pFile)
		return false;

	Load(pFile);

	fclose(pFile);

	return true;
}

bool CAnimation3D::LoadClipFromFullPath(const char * pFullPath)
{

	return false;
}

bool CAnimation3D::LoadClipFromOne(const char * pFullPath)
{
	FILE*	pFile = NULL;

	fopen_s(&pFile, pFullPath, "rb");

	if (!pFile)
		return false;

	CAnimation3DClip*	pClip = new CAnimation3DClip;

	pClip->Init();

	pClip->Load(pFile);

	m_mapClip.insert(make_pair(pClip->GetInfo().strName, pClip));

	fclose(pFile);

	m_pLastInsertClip = pClip;

	return true;
}

bool CAnimation3D::LoadClip(FILE * pFile)
{
	return false;
}

bool CAnimation3D::LoadBoneFromFullPath(const char * pFullPath)
{
	FILE*	pFile = NULL;

	fopen_s(&pFile, pFullPath, "rb");

	if (!pFile)
		return false;

	int	iSize = 0;

	// 본 수를 저장한다.
	fread(&iSize, 4, 1, pFile);

	for (int i = 0; i < m_vecBones.size(); ++i)
	{
		SAFE_DELETE(m_vecBones[i]);
	}

	m_vecBones.clear();

	for (int i = 0; i < iSize; ++i)
	{
		// 이름 저장
		int	iLength = 0;
		char	strName[256] = {};

		PBONE	pBone = new BONE;

		fread(&iLength, 4, 1, pFile);
		fread(strName, 1, iLength, pFile);
		pBone->strName = strName;

		// 깊이 저장
		fread(&pBone->iDepth, 4, 1, pFile);
		fread(&pBone->iParentIndex, 4, 1, pFile);

		pBone->matOffset = new MATRIX;
		pBone->matBone = new MATRIX;

		for (int j = 0; j < 4; ++j)
		{
			for (int k = 0; k < 4; ++k)
			{
				fread(&pBone->matOffset->m[j][k], 4, 1, pFile);
			}
		}

		for (int j = 0; j < 4; ++j)
		{
			for (int k = 0; k < 4; ++k)
			{
				fread(&pBone->matBone->m[j][k], 4, 1, pFile);
			}
		}

		m_vecBones.push_back(pBone);
	}

	CreateBoneTexture();

	fclose(pFile);

	return true;
}

bool CAnimation3D::LoadBone(const char * pFileName, const string & strPathKey)
{// 전체 경로를 만들어준다.
	const char*	pPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	string	strPath;
	if (pPath)
		strPath = pPath;

	strPath += pFileName;

	return LoadBoneFromFullPath(strPath.c_str());
}

bool CAnimation3D::Init()
{
	return true;
}

void CAnimation3D::Input(float fTime)
{
}

void CAnimation3D::Update(float fTime)
{
	if (!m_pCurClip)
		return;

	vector<MATRIX>	vecBones;

	// 모션이 바뀌고 있는 중이라면 기존에 동작되던 모션과
	// 교체될 모션을 보간한다.
	if (m_bChange)
	{
		m_fChangeTime += fTime;

		if (m_fChangeTime >= m_fChangeLimitTime)
		{
			m_fChangeTime = m_fChangeLimitTime;
			m_bChange = false;
		}

		float	fAnimationTime = m_fAnimationTime +
			m_pCurClip->m_tInfo.fStartTime;

		vecBones.reserve(m_vecBones.size());

		// 본 수만큼 반복한다.
		for (size_t i = 0; i < m_vecBones.size(); ++i)
		{
			// 키프레임이 없을 경우
			if (m_pCurClip->IsEmptyKeyFrame(i))
			{
				vecBones.push_back(*m_vecBones[i]->matBone);
				continue;
			}

			int	iFrameIndex = (int)(fAnimationTime * m_iAnimationLimitFrame);
			int	iNextFrameIndex = m_pNextClip->m_tInfo.iStartFrame;

			const PKEYFRAME pCurKey = m_pCurClip->GetKeyFrame(i, iFrameIndex);
			const PKEYFRAME pNextKey = m_pNextClip->GetKeyFrame(i, iNextFrameIndex);

			float	fPercent = m_fChangeTime / m_fChangeLimitTime;

			XMVECTOR	vS = XMVectorLerp(pCurKey->vScale.Convert(),
				pNextKey->vScale.Convert(), fPercent);
			XMVECTOR	vT = XMVectorLerp(pCurKey->vPos.Convert(),
				pNextKey->vPos.Convert(), fPercent);
			XMVECTOR	vR = XMQuaternionSlerp(pCurKey->vRot.Convert(),
				pNextKey->vRot.Convert(), fPercent);

			XMVECTOR	vZero = XMVectorSet(0.f, 0.f, 0.f, 1.f);

			MATRIX	matBone = XMMatrixAffineTransformation(vS, vZero,
				vR, vT);

			matBone = *m_vecBones[i]->matOffset *
				matBone;

			vecBones.push_back(matBone);
		}

		if (!m_bChange)
		{
			m_strCurClip = m_strNextClip;
			SAFE_RELEASE(m_pCurClip);
			m_pCurClip = m_pNextClip;
			m_pCurClip->AddRef();
			SAFE_RELEASE(m_pNextClip);
			m_strNextClip = "";
			m_fAnimationTime = 0.f;
		}
	}

	else
	{
		m_fAnimationTime += fTime;

		m_fAnimationProgress = m_fAnimationTime / m_pCurClip->m_tInfo.fTimeLength;

		m_fAnimationProgress = m_fAnimationProgress > 1.f ? 1.f : m_fAnimationProgress;

		//char	cTest[64] = {};
		//sprintf_s(cTest, "%f\n", m_fAnimationProgress);
		//OutputDebugStringA(cTest);

		// Callback 이 있는지 체크한다.
		for (size_t i = 0; i < m_pCurClip->m_tInfo.vecCallback.size();
			++i)
		{
			if (m_pCurClip->m_tInfo.vecCallback[i]->fAnimationProgress >=
				m_fAnimationProgress && !m_pCurClip->m_tInfo.vecCallback[i]->bCall)
			{
				m_pCurClip->m_tInfo.vecCallback[i]->bCall = true;
				m_pCurClip->m_tInfo.vecCallback[i]->func(fTime);
			}
		}

		while (m_fAnimationTime >= m_pCurClip->m_tInfo.fTimeLength)
		{
			m_fAnimationTime -= m_pCurClip->m_tInfo.fTimeLength;
			m_bEnd = true;

			for (size_t i = 0; i < m_pCurClip->m_tInfo.vecCallback.size();
				++i)
			{
				m_pCurClip->m_tInfo.vecCallback[i]->bCall = false;
			}

			if (m_pCurClip->m_tInfo.eOption == AO_ONCE_RETURN)
			{
				ChangeClip(m_strDefaultClip);
			}
		}

		float	fAnimationTime = m_fAnimationTime +
			m_pCurClip->m_tInfo.fStartTime;

		int	iStartFrame = m_pCurClip->m_tInfo.iStartFrame;
		int	iEndFrame = m_pCurClip->m_tInfo.iEndFrame;

		vecBones.reserve(m_vecBones.size());

		// 본 수만큼 반복한다.
		for (size_t i = 0; i < m_vecBones.size(); ++i)
		{
			// 키프레임이 없을 경우
			if (m_pCurClip->IsEmptyKeyFrame(i))
			{
				vecBones.push_back(*m_vecBones[i]->matBone);
				continue;
			}

			int	iFrameIndex = (int)(fAnimationTime * m_iAnimationLimitFrame);

			if (iFrameIndex >= iEndFrame)
			{
				switch (m_pCurClip->m_tInfo.eOption)
				{
				case AO_LOOP:
					iFrameIndex = iStartFrame;
					break;
				case AO_ONCE_RETURN:
					ChangeClip(m_strDefaultClip);
					break;
				case AO_ONCE_DESTROY:
					m_pGameObject->Death();
					break;
				}
			}

			int	iNextFrameIndex = iFrameIndex + 1;

			if (iNextFrameIndex >= iEndFrame)
				iNextFrameIndex = iStartFrame;

			const PKEYFRAME pCurKey = m_pCurClip->GetKeyFrame(i, iFrameIndex);
			const PKEYFRAME pNextKey = m_pCurClip->GetKeyFrame(i, iNextFrameIndex);

			// 현재 프레임의 시간을 얻어온다.
			double	 dFrameTime = pCurKey->dTime;

			float	fPercent = (fAnimationTime - dFrameTime) / m_fFrameTime;

			XMVECTOR	vS = XMVectorLerp(pCurKey->vScale.Convert(),
				pNextKey->vScale.Convert(), fPercent);
			XMVECTOR	vT = XMVectorLerp(pCurKey->vPos.Convert(),
				pNextKey->vPos.Convert(), fPercent);
			XMVECTOR	vR = XMQuaternionSlerp(pCurKey->vRot.Convert(),
				pNextKey->vRot.Convert(), fPercent);

			XMVECTOR	vZero = XMVectorSet(0.f, 0.f, 0.f, 1.f);

			MATRIX	matBone = XMMatrixAffineTransformation(vS, vZero,
				vR, vT);

			matBone = *m_vecBones[i]->matOffset *
				matBone;

			vecBones.push_back(matBone);
		}
	}

	m_pBoneTex->UpdateData(&vecBones[0], sizeof(MATRIX) *
		vecBones.size());
}

void CAnimation3D::LateUpdate(float fTime)
{
	CRenderer*	pRenderer = (CRenderer*)FindComponentFromType(CT_RENDERER);

	if (!pRenderer->IsBoneTexture())
		pRenderer->SetBoneTexture(m_pBoneTex);

	SAFE_RELEASE(pRenderer);
}

void CAnimation3D::Collision(float fTime)
{
}

void CAnimation3D::Render(float fTime)
{
	m_bEnd = false;
}

CAnimation3D * CAnimation3D::Clone()
{
	return new CAnimation3D(*this);
}
