/*
                              ※※※※※※   통합빌드 유의사항   ※※※※※※

    1. 클래스 추가 시 가끔 한번씩 생각 날 때 마다 UnityBuild.exe 를 실행해서 빌드 활성화를 시켜주세요

    2. 클래스를 삭제시엔 필수로 UnityBuild.exe 를 한 두번 실행해서 UnityBuild (*).cpp에 삭제된 cpp파일이 있는지 확인 하세요

    3. 전역/static 변수 함수들은 이름 충돌 위험이 있으니 사용에 유의 하세요 

    4. 코드(.cpp)에서 Include가 누락되어도 될 때가 있습니다 이전 줄 에서 이미 include 가 되어 있어서 되는 부분입니다
       그렇다고 include를 생략 해도 된다는 말이 아니라 include를 하지 않았는데 어떻게 돌아 가는지에 대한 의문이 들 분을 위한 말입니다

    5. 이 통합 빌드는 안전하지 않을 수 있습니다 
       많은 부분에 예외처리 한다고 노력 했지만 발견하지 못한 예외상황에 대해선 작동이 원활하지 않을 수 있습니다 

    6. 프로젝트 복구가 필요한 사람은 "프로젝트.vcxproj" 파일과 "프로젝트.vcxproj.filters" 파일을 저한테 보내주세요

    이름 : 송지훈
    이메일 : newverka@naver.com
*/

#include "../Scene\SceneScript.cpp"
#include "../Vector2.cpp"
#include "../Matrix.cpp"
#include "../Core\Input.cpp"
#include "../Component\Script.cpp"
#include "../Component\Camera.cpp"
#include "../Component\CameraArm.cpp"
#include "../Component\Collider.cpp"
#include "../Component\ColliderSphere.cpp"
#include "../Core\CollisionManager.cpp"
#include "../Rendering\RenderState.cpp"
#include "../Rendering\RasterizerState.cpp"
#include "../Resources\Sampler.cpp"
#include "../Resources\Texture.cpp"
#include "../Component\Light.cpp"
#include "../Component\LightDir.cpp"
#include "../Component\LightPoint.cpp"
#include "../Rendering\BlendState.cpp"
#include "../Component\Material.cpp"
#include "../Rendering\DepthStencilState.cpp"
