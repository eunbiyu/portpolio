#include "Device.h"

AR3D_USING

DEFINITION_SINGLE(CDevice)

CDevice::CDevice() :
	m_pDevice(NULL),
	m_pContext(NULL),
	m_pSwapChain(NULL),
	m_pRTView(NULL),
	m_pDepthBuffer(NULL),
	m_pDepthView(NULL),
	m_pD2DFactory(NULL),
	m_pD2DTarget(NULL)
{
	memset(m_fClearColor, 0, sizeof(float) * 4);
	m_fClearColor[2] = 1.f;
}

CDevice::~CDevice()
{
	SAFE_RELEASE(m_pD2DTarget);
	SAFE_RELEASE(m_pD2DFactory);

	unordered_map<string, ID3D11DeviceContext*>::iterator	iter;

	for (iter = m_mapDeferredContext.begin(); iter != m_mapDeferredContext.end();
		++iter)
	{
		iter->second->ClearState();
		SAFE_RELEASE(iter->second);
	}

	SAFE_RELEASE(m_pDepthBuffer);
	SAFE_RELEASE(m_pDepthView);

	SAFE_RELEASE(m_pRTView);
	SAFE_RELEASE(m_pSwapChain);

	if (m_pContext)
		m_pContext->ClearState();

	SAFE_RELEASE(m_pContext);
	SAFE_RELEASE(m_pDevice);
}

ID3D11Device * CDevice::GetDevice() const
{
	return m_pDevice;
}

ID3D11DeviceContext * CDevice::GetContext() const
{
	return m_pContext;
}

IDXGISwapChain * CDevice::GetSwapChain() const
{
	return m_pSwapChain;
}

void CDevice::SetClearColor(float fColor[4])
{
	memcpy(m_fClearColor, fColor, sizeof(float) * 4);
}

bool CDevice::CreateDeferredContext(const string & strKey)
{
	ID3D11DeviceContext*	pContext = FindContext(strKey);

	if (pContext)
		return false;

	if (FAILED(m_pDevice->CreateDeferredContext(0, &pContext)))
		return false;

	m_mapDeferredContext.insert(make_pair(strKey, pContext));

	return true;
}

ID3D11DeviceContext * CDevice::FindContext(const string & strKey)
{
	unordered_map<string, ID3D11DeviceContext*>::iterator	iter = m_mapDeferredContext.find(strKey);

	if (iter == m_mapDeferredContext.end())
		return NULL;

	return iter->second;
}

bool CDevice::Init(HWND hWnd, unsigned int iWidth,
	unsigned int iHeight, bool bWindowMode)
{
	unsigned int	iCreateFlag = 0;

#ifdef _DEBUG
	iCreateFlag |= D3D11_CREATE_DEVICE_DEBUG;
#endif // _DEBUG

	iCreateFlag |= D3D11_CREATE_DEVICE_BGRA_SUPPORT;

	D3D_FEATURE_LEVEL	eFeatureLevel = D3D_FEATURE_LEVEL_11_0;

	DXGI_SWAP_CHAIN_DESC	tSwapChain = {};
	tSwapChain.BufferDesc.Width = iWidth;
	tSwapChain.BufferDesc.Height = iHeight;
	tSwapChain.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	tSwapChain.BufferDesc.RefreshRate.Numerator = 60;
	tSwapChain.BufferDesc.RefreshRate.Denominator = 1;
	tSwapChain.BufferCount = 1;
	tSwapChain.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	tSwapChain.OutputWindow = hWnd;
	tSwapChain.SampleDesc.Count = 1;
	tSwapChain.SampleDesc.Quality = 0;
	tSwapChain.Windowed = bWindowMode;

	// Device와 Context를 생성한다.
	if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE,
		0, iCreateFlag, 0, 0, D3D11_SDK_VERSION,
		&tSwapChain, &m_pSwapChain, &m_pDevice, &eFeatureLevel,
		&m_pContext)))
		return false;

	CreateDeferredContext("LoadingContext");

	ID3D11Texture2D*	pBackBuffer = NULL;

	m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D),
		(void**)&pBackBuffer);

	m_pDevice->CreateRenderTargetView(pBackBuffer, NULL, &m_pRTView);

	SAFE_RELEASE(pBackBuffer);

	// 깊이 타겟을 만든다.
	D3D11_TEXTURE2D_DESC	tDesc = {};
	tDesc.Width = iWidth;
	tDesc.Height = iHeight;
	tDesc.MipLevels = 1;
	tDesc.ArraySize = 1;
	tDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	tDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	tDesc.SampleDesc.Count = 1;
	tDesc.SampleDesc.Quality = 0;
	tDesc.Usage = D3D11_USAGE_DEFAULT;

	if (FAILED(m_pDevice->CreateTexture2D(&tDesc, NULL, &m_pDepthBuffer)))
		return false;

	m_pDevice->CreateDepthStencilView(m_pDepthBuffer, NULL, &m_pDepthView);

	m_pContext->OMSetRenderTargets(1, &m_pRTView, m_pDepthView);

	D3D11_VIEWPORT	tVP = {};
	tVP.Width = iWidth;
	tVP.Height = iHeight;
	tVP.MaxDepth = 1;

	m_pContext->RSSetViewports(1, &tVP);

	// ================= 2D 장치 생성 및 렌더타겟 생성 ====================
	// D2D Factory를 생성한다.
	D2D1_FACTORY_OPTIONS	tOption;
	tOption.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
	// 2D 및 텍스트 출력 초기화
	// 2D Factory를 생성한다.
	// D2D1_FACTORY_TYPE_SINGLE_THREADED : 싱글스레드
	// D2D1_FACTORY_TYPE_MULTI_THREADED : 멀티스레드
	if (FAILED(D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, tOption,
		&m_pD2DFactory)))
		return false;

	// Direct11 BackBuffer를 이용하여 2D 렌더링 타겟을 설정해준다.
	IDXGISurface*	pBackBufferSurface = NULL;

	// SwapChain으로부터 Surface를 얻어온다.
	m_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pBackBufferSurface));

	// D2D 렌더타겟을 생성하기 위한 옵션 정보들을 설정한다.
	D2D1_RENDER_TARGET_PROPERTIES	props = D2D1::RenderTargetProperties(
		D2D1_RENDER_TARGET_TYPE_HARDWARE,
		D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED));

	if (FAILED(m_pD2DFactory->CreateDxgiSurfaceRenderTarget(pBackBufferSurface, props,
		&m_pD2DTarget)))
		return false;

	SAFE_RELEASE(pBackBufferSurface);

	return true;
}

void CDevice::Clear()
{
	m_pContext->ClearRenderTargetView(m_pRTView, m_fClearColor);

	m_pContext->ClearDepthStencilView(m_pDepthView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		1.f, 0);
}

void CDevice::Present()
{
	m_pSwapChain->Present(0, 0);
}

ID2D1Factory * CDevice::GetD2DFactory() const
{
	return m_pD2DFactory;
}

ID2D1RenderTarget * CDevice::GetD2DRenderTarget() const
{
	return m_pD2DTarget;
}
