#include "Core.h"
#include "Device.h"
#include "Rendering\RenderManager.h"
#include "Resources\ResourcesManager.h"
#include "Core\PathManager.h"
#include "Resources\Mesh.h"
#include "Rendering\Shader.h"
#include "Rendering\ShaderManager.h"
#include "Scene\SceneManager.h"
#include "Core\TimerManager.h"
#include "Core\Timer.h"
#include "Core\Input.h"
#include "GameObject\GameObject.h"
#include "Core\CollisionManager.h"
#include "Core\Scheduler.h"
#include "Core\GameTotalManager.h"
#include "Core\FontManager.h"
#include "Core\GameTotalManager.h"

AR3D_USING

DEFINITION_SINGLE(CCore)

bool CCore::m_bRun = true;

CCore::CCore() :
	m_hInst(0)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
//	_CrtSetBreakAlloc(1319);

	srand(GetCurrentTime());

//#ifdef _DEBUG
//	AllocConsole();
//#endif // _DEBUG

}

CCore::~CCore()
{
	DESTROY_SINGLE(CSceneManager);
	DESTROY_SINGLE(CResourcesManager);
	DESTROY_SINGLE(CRenderManager);
	DESTROY_SINGLE(CCollisionManager);
	DESTROY_SINGLE(CScheduler);
	DESTROY_SINGLE(CPathManager);
	DESTROY_SINGLE(CInput);
	DESTROY_SINGLE(CFontManager);
	DESTROY_SINGLE(CDevice);
	DESTROY_SINGLE(CTimerManager);
	CGameObject::DestroyPrototype();
#ifdef _DEBUG
	FreeConsole();
#endif // _DEBUG

}

HWND CCore::GetWindowHandle() const
{
	return m_hWnd;
}

void CCore::Exit()
{
	DestroyWindow(m_hWnd);
}

bool CCore::Init(HINSTANCE hInst, unsigned int iWidth, unsigned int iHeight,
	TCHAR* pClass, TCHAR* pTitle, int iIconID, int iSmallIconID,
	bool bWindowMode)
{
	m_hInst = hInst;
	m_tResolution.iWidth = iWidth;
	m_tResolution.iHeight = iHeight;

	// 윈도우 창을 생성한다.
	MyRegisterClass(pClass, iIconID, iSmallIconID);
	Create(pClass, pTitle);

	return Init(m_hWnd, iWidth, iHeight, bWindowMode);
}

bool CCore::Init(HWND hWnd, unsigned int iWidth, unsigned int iHeight,
	bool bWindowMode)
{
	m_hWnd = hWnd;
	m_tResolution.iWidth = iWidth;
	m_tResolution.iHeight = iHeight;

	// 디바이스 초기화
	if (!GET_SINGLE(CDevice)->Init(hWnd, iWidth, iHeight, bWindowMode))
		return false;

	//float	fClear[4] = {1.f, 1.f, 0.f, 0.f};
	//GET_SINGLE(CDevice)->SetClearColor(fClear);

	// 입력 관리자 초기화
	if (!GET_SINGLE(CInput)->Init())
		return false;

	// 시간 관리자 초기화
	if (!GET_SINGLE(CTimerManager)->Init())
		return false;

	if (!GET_SINGLE(CPathManager)->Init())
		return false;

	// 폰트 관리자 초기화
	if (!GET_SINGLE(CFontManager)->Init())
		return false;

	// Scheduler 초기화
	if (!GET_SINGLE(CScheduler)->Init())
		return false;

	// 리소스 관리자 초기화
	if (!GET_SINGLE(CResourcesManager)->Init())
		return false;

	// 렌더링 관리자 초기화
	if (!GET_SINGLE(CRenderManager)->Init())
		return false;

	// 충돌 관리자 초기화
	if (!GET_SINGLE(CCollisionManager)->Init())
		return false;

	// 장면 관리자 초기화
	if (!GET_SINGLE(CSceneManager)->Init())
		return false;


	// 게임 토탈 관리자 초기화
	if (!GET_SINGLE(CGameTotalManager)->Init())
		return false;
	

	return true;
}

int CCore::Run()
{
	MSG msg;

	// 기본 메시지 루프입니다.
	while (m_bRun)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		else
		{
			Logic();
		}
	}

	return (int)msg.wParam;
}

void CCore::RunFromLoop()
{
	Logic();
}

void CCore::Logic()
{
	// MainThread Timer
	CTimer*	pTimer = GET_SINGLE(CTimerManager)->FindTimer("MainThread");

	pTimer->Update();

	float	fDeltaTime = pTimer->GetDeltaTime();

	SAFE_RELEASE(pTimer);

	GET_SINGLE(CInput)->Update(fDeltaTime);

	GET_SINGLE(CGameTotalManager)->Update(fDeltaTime);

	GET_SINGLE(CScheduler)->Update(fDeltaTime);

	if (Input(fDeltaTime) != SC_NONE)
		return;

	if (Update(fDeltaTime) != SC_NONE)
		return;

	Collision(fDeltaTime);

	Render(fDeltaTime);

	if (LateUpdate(fDeltaTime) != SC_NONE)
		return;

	

	GET_SINGLE(CInput)->ClearWheel();
}

int CCore::Input(float fTime)
{
	GET_SINGLE(CInput)->Input(fTime);
	return GET_SINGLE(CSceneManager)->Input(fTime);
}

int CCore::Update(float fTime)
{
	return GET_SINGLE(CSceneManager)->Update(fTime);
}

int CCore::LateUpdate(float fTime)
{
	return GET_SINGLE(CSceneManager)->LateUpdate(fTime);
}

void CCore::Collision(float fTime)
{
	//GET_SINGLE(CInput)->ComputeRay();
	GET_SINGLE(CSceneManager)->Collision(fTime);
	GET_SINGLE(CCollisionManager)->Collision(fTime);
}

void CCore::Render(float fTime)
{
	GET_SINGLE(CDevice)->Clear();

	GET_SINGLE(CSceneManager)->Render(fTime);

	GET_SINGLE(CRenderManager)->Render(fTime);

	GET_SINGLE(CDevice)->Present();
}

ATOM CCore::MyRegisterClass(TCHAR * pClass, int iIconID, int iSmallIconID)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = CCore::WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = m_hInst;
	wcex.hIcon = LoadIcon(m_hInst, MAKEINTRESOURCE(iIconID));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = pClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(iSmallIconID));

	return RegisterClassExW(&wcex);
}

bool CCore::Create(TCHAR * pClass, TCHAR * pTitle)
{
	m_hWnd = CreateWindowW(pClass, pTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, m_hInst, nullptr);

	if (!m_hWnd)
	{
		return false;
	}

	RECT	rc = { 0, 0, m_tResolution.iWidth, m_tResolution.iHeight };

	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	SetWindowPos(m_hWnd, HWND_TOPMOST, 0, 0, rc.right - rc.left,
		rc.bottom - rc.top, SWP_NOZORDER | SWP_NOMOVE);

	ShowWindow(m_hWnd, SW_SHOW);
	UpdateWindow(m_hWnd);

	return true;
}

LRESULT CCore::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_MOUSEWHEEL:
		GET_SINGLE(CInput)->SetWheel(HIWORD(wParam));
		break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		m_bRun = false;
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
