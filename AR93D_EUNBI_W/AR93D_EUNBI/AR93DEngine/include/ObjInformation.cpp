#include "ObjInformation.h"
#include "Core.h"
#include "Device.h"
#include "GameObject\GameObject.h"
#include "Component\Transform.h"
#include "Core\PathManager.h"

AR3D_USING

DEFINITION_SINGLE(CObjInformation)


CObjInformation::CObjInformation()
{
}


CObjInformation::~CObjInformation()
{
	Safe_Release_VecList(m_ObjList);
}

bool CObjInformation::SaveObjInfo(const list<class CGameObject*> ObjList, const char * pFileName, const string & strPathKey)
{
	// 전체 경로를 만들어준다.
	const char*	pPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	string	strPath;
	if (pPath)
		strPath = pPath;

	strPath += pFileName;

	//SAVE FROM FULL PATH

	FILE*	pFile = NULL;

	fopen_s(&pFile, strPath.c_str(), "wb");
	
	if (!pFile)
		return false;

	m_ObjList = ObjList;

	int iSize = m_ObjList.size();

	//저장할 오브젝트의 갯수를 저장한다.
	fwrite(&iSize, 4, 1, pFile);



	list<class CGameObject*>::iterator iter;
	list<class CGameObject*>::iterator iterEnd = m_ObjList.end();

	for (iter = m_ObjList.begin() ; iter !=iterEnd ; ++iter)
	{
		CTransform* pTr = (*iter)->GetTransform();

		//이름 저장
		int iLength = (*iter)->GetTag().length();

		fwrite(&iLength, 4, 1, pFile);
		fwrite((*iter)->GetTag().c_str(), 1, iLength, pFile);

		//Pos 저장
		float x = pTr->GetWorldPos().x;
		float y = pTr->GetWorldPos().y;
		float z = pTr->GetWorldPos().z;
		
		fwrite(&x, 4, 1, pFile);
		fwrite(&y, 4, 1, pFile);
		fwrite(&z, 4, 1, pFile);

		//Scale 저장
		x = pTr->GetWorldScale().x;
		y = pTr->GetWorldScale().y;
		z = pTr->GetWorldScale().z;

		fwrite(&x, 4, 1, pFile);
		fwrite(&y, 4, 1, pFile);
		fwrite(&z, 4, 1, pFile);

		//Rot 저장
		x = pTr->GetWorldRot().x;
		y = pTr->GetWorldRot().y;
		z = pTr->GetWorldRot().z;

		fwrite(&x, 4, 1, pFile);
		fwrite(&y, 4, 1, pFile);
		fwrite(&z, 4, 1, pFile);

		SAFE_RELEASE(pTr);
	}

	fclose(pFile);

	return true;
}

list<OBJINFO> CObjInformation::LoadObjInfo(const char * pFileName, const string & strPathKey)
{
	// 전체 경로를 만들어준다.
	const char*	pPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	string	strPath;
	if (pPath)
		strPath = pPath;

	strPath += pFileName;

	FILE*	pFile = NULL;

	fopen_s(&pFile, strPath.c_str(), "rb");

	if (!pFile)
		return m_ObjInfoList;

	// 오브젝트 수를 저장한다.
	int	iCount = 0;

	fread(&iCount, 4, 1, pFile);

	for (int i = 0; i < iCount; ++i)
	{
		OBJINFO obj;

		int		iLength;
		char	strName[256] = {};

		fread(&iLength, 4, 1, pFile);
		fread(strName, 1, iLength, pFile);

		obj.strTag = strName;

		//Pos 저장
		float x = 0;
		float y = 0;
		float z = 0;

		fread(&x, 4, 1, pFile);
		fread(&y, 4, 1, pFile);
		fread(&z, 4, 1, pFile);

		obj.WorldPos = DxVector3(x, y , z);

		fread(&x, 4, 1, pFile);
		fread(&y, 4, 1, pFile);
		fread(&z, 4, 1, pFile);

		obj.WorldScale = DxVector3(x, y, z);

		fread(&x, 4, 1, pFile);
		fread(&y, 4, 1, pFile);
		fread(&z, 4, 1, pFile);

		obj.WorldRot = DxVector3(x, y, z);

		m_ObjInfoList.push_back(obj);
	}

	fclose(pFile);

	return m_ObjInfoList;
}

