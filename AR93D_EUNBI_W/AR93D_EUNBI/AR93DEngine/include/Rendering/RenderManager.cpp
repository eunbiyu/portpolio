#include "RenderManager.h"
#include "ShaderManager.h"
#include "RasterizerState.h"
#include "DepthStencilState.h"
#include "BlendState.h"
#include "AR3DRenderTarget.h"
#include "AR3DDepthTarget.h"
#include "../Device.h"
#include "../Core.h"
#include "../Component/Renderer.h"
#include "../GameObject/GameObject.h"
#include "../Scene/SceneManager.h"
#include "../Scene/Scene.h"
#include "../Component/Transform.h"
#include "../Component/LightDir.h"
#include "../Resources/ResourcesManager.h"
#include "../Resources/Sampler.h"
#include "Shader.h"
#include "../Scene/Scene.h"
#include "../Scene/SceneManager.h"
#include "../Component/Camera.h"
#include "../Component/ParticleSystem.h"
#include "../Component/LampLight.h"
#include "../Core/GameTotalManager.h"

AR3D_USING

DEFINITION_SINGLE(CRenderManager)

CRenderManager::CRenderManager() :
	m_pLightAcc(NULL),
	m_pLightBlend(NULL),
	m_bShadow(true)
{
	for (int i = 0; i < RG_END; ++i)
	{
		m_vecRender[i].reserve(200);
	}
}

CRenderManager::~CRenderManager()
{
	SAFE_RELEASE(m_pAlphaBlend);
	SAFE_RELEASE(m_pLightBlendTarget);
	SAFE_RELEASE(m_pLightBlend);
	SAFE_RELEASE(m_pLightAcc);
	SAFE_RELEASE(m_pZDisable);
	SAFE_RELEASE(m_pBlendOne);
	SAFE_RELEASE(m_pPointSmp);
	unordered_map<string, PMRT>::iterator	iter;
	unordered_map<string, PMRT>::iterator	iterEnd = m_mapMRT.end();

	for (iter = m_mapMRT.begin(); iter != iterEnd; ++iter)
	{
		for (size_t i = 0; i < iter->second->vecTarget.size(); ++i)
			SAFE_RELEASE(iter->second->vecTarget[i]);

		for (size_t i = 0; i < iter->second->vecOldTarget.size(); ++i)
			SAFE_RELEASE(iter->second->vecOldTarget[i]);

		SAFE_RELEASE(iter->second->pDepth);
		SAFE_RELEASE(iter->second->pOldDepth);
		SAFE_DELETE(iter->second);
	}

	Safe_Release_Map(m_mapRenderTarget);
	Safe_Release_Map(m_mapRenderState);
	Safe_Release_Map(m_mapDepthTarget);
	DESTROY_SINGLE(CShaderManager);
}

void CRenderManager::OnShadow(bool bShadow)
{
	m_bShadow = bShadow;
}

void CRenderManager::AddRenderObject(CGameObject * pObj)
{
	if (pObj->CheckComponentFromType(CT_UI))
	{
		m_vecRender[RG_UI].push_back(pObj);
	}

	else if (pObj->CheckComponentFromType(CT_PARTICLE))
	{
		m_vecRender[RG_ALPHA3].push_back(pObj);

		CParticleSystem*	pParticle = (CParticleSystem*)pObj->FindComponentFromType(CT_PARTICLE);

		if (pParticle->GetParticleLight())
			m_ParticleLightList.push_back(pParticle);

		SAFE_RELEASE(pParticle);
	}

	else if (pObj->CheckComponentFromType(CT_LAMPLIGHT))
	{

		CRenderer*	pRenderer = (CRenderer*)pObj->FindComponentFromType(CT_RENDERER);

		if (pRenderer)
		{
			if (pRenderer->BlendEnable())
				m_vecRender[RG_ALPHA3].push_back(pObj);

			else
				m_vecRender[RG_DEFAULT].push_back(pObj);

			SAFE_RELEASE(pRenderer);
		}

		else
			m_vecRender[RG_ALPHA3].push_back(pObj);

		CLampLight*	pLampLight = (CLampLight*)pObj->FindComponentFromType(CT_LAMPLIGHT);

		//라이트 셋팅 임시
		pLampLight->SetLampLight(true);

		if (pLampLight->GetLampLight())
			m_pLampLight.push_back(pLampLight);

		SAFE_RELEASE(pLampLight);
	}

	else
	{
		CRenderer*	pRenderer = (CRenderer*)pObj->FindComponentFromType(CT_RENDERER);

		if (pRenderer)
		{
			if (pRenderer->BlendEnable())
				m_vecRender[RG_ALPHA3].push_back(pObj);

			else
				m_vecRender[RG_DEFAULT].push_back(pObj);

			SAFE_RELEASE(pRenderer);
		}

		else
			m_vecRender[RG_ALPHA3].push_back(pObj);
	}
}

bool CRenderManager::Init()
{
	if (!GET_SINGLE(CShaderManager)->Init())
		return false;

	CRenderState*	pState = CreateRasterizerState(WIRE_FRAME, D3D11_FILL_WIREFRAME);

	SAFE_RELEASE(pState);

	pState = CreateRasterizerState(CULLING_CW, D3D11_FILL_SOLID, D3D11_CULL_NONE);

	SAFE_RELEASE(pState);

	AddRenderTargetBlendInfo();
	m_pAlphaBlend = CreateBlendState(ALPHABLEND);

	AddRenderTargetBlendInfo(true, D3D11_BLEND_ONE, D3D11_BLEND_ONE);
	m_pBlendOne = CreateBlendState(ACC_BLEND);

	pState = CreateDepthStencilState(DEPTH_LESS_EQUAL, true,
		D3D11_DEPTH_WRITE_MASK_ALL, D3D11_COMPARISON_LESS_EQUAL);

	SAFE_RELEASE(pState);

	m_pZDisable = CreateDepthStencilState(DEPTH_DISABLE, false);

	pState = CreateDepthStencilState(DEPTH_WRITE_DISABLE, true, D3D11_DEPTH_WRITE_MASK_ZERO);

	SAFE_RELEASE(pState);

	// Albedo Target
	CAR3DRenderTarget*	pTarget = CreateTarget("Albedo", _RESOLUTION.iWidth,
		_RESOLUTION.iHeight, DXGI_FORMAT_R8G8B8A8_UNORM);

	pTarget->OnRender(true);
	pTarget->SetScale(100.f, 100.f, 1.f);
	pTarget->SetPos(0.f, 0.f, 0.f);

	SAFE_RELEASE(pTarget);

	// Normal Target
	pTarget = CreateTarget("Normal", _RESOLUTION.iWidth,
		_RESOLUTION.iHeight, DXGI_FORMAT_R32G32B32A32_FLOAT);

	pTarget->OnRender(true);
	pTarget->SetScale(100.f, 100.f, 1.f);
	pTarget->SetPos(0.f, 100.f, 0.f);

	SAFE_RELEASE(pTarget);

	pTarget = CreateTarget("Depth", _RESOLUTION.iWidth,
		_RESOLUTION.iHeight, DXGI_FORMAT_R32G32B32A32_FLOAT);

	pTarget->OnRender(true);
	pTarget->SetScale(100.f, 100.f, 1.f);
	pTarget->SetPos(0.f, 200.f, 0.f);
	pTarget->SetClearColor(1.f, 1.f, 1.f, 0.f);

	SAFE_RELEASE(pTarget);

	pTarget = CreateTarget("Shininess", _RESOLUTION.iWidth,
		_RESOLUTION.iHeight, DXGI_FORMAT_R8G8B8A8_UNORM);

	pTarget->OnRender(true);
	pTarget->SetScale(100.f, 100.f, 1.f);
	pTarget->SetPos(0.f, 300.f, 0.f);

	SAFE_RELEASE(pTarget);

	// 깊이를 생성한다.
	CAR3DDepthTarget*	pDepth = CreateDepthTarget("GBufferDepth",
		_RESOLUTION.iWidth, _RESOLUTION.iHeight,
		DXGI_FORMAT_D24_UNORM_S8_UINT);

	SAFE_RELEASE(pDepth);

	// GBuffer MRT 생성
	AddMRTTarget("GBuffer", "Albedo");
	AddMRTTarget("GBuffer", "Normal");
	AddMRTTarget("GBuffer", "Depth");
	AddMRTTarget("GBuffer", "Shininess");
	//SetMRTDepth("GBuffer", "GBufferDepth");

	// 조명 Diffuse, Ambient 누적버퍼 생성
	pTarget = CreateTarget("LightAccDif", _RESOLUTION.iWidth,
		_RESOLUTION.iHeight, DXGI_FORMAT_R32G32B32A32_FLOAT);

	pTarget->OnRender(true);
	pTarget->SetScale(100.f, 100.f, 1.f);
	pTarget->SetPos(100.f, 0.f, 0.f);

	SAFE_RELEASE(pTarget);

	pTarget = CreateTarget("LightAccSpc", _RESOLUTION.iWidth,
		_RESOLUTION.iHeight, DXGI_FORMAT_R32G32B32A32_FLOAT);

	pTarget->OnRender(true);
	pTarget->SetScale(100.f, 100.f, 1.f);
	pTarget->SetPos(100.f, 100.f, 0.f);

	SAFE_RELEASE(pTarget);

	pDepth = CreateDepthTarget("LightAccDepth",
		_RESOLUTION.iWidth, _RESOLUTION.iHeight,
		DXGI_FORMAT_D24_UNORM_S8_UINT);

	SAFE_RELEASE(pDepth);

	AddMRTTarget("LightAcc", "LightAccDif");
	AddMRTTarget("LightAcc", "LightAccSpc");
	SetMRTDepth("LightAcc", "LightAccDepth");

	m_pPointSmp = GET_SINGLE(CResourcesManager)->FindSampler("Point");

	m_pLightAcc = GET_SINGLE(CShaderManager)->FindShader(LIGHT_ACC_SHADER);

	pTarget = CreateTarget("LightBlend", _RESOLUTION.iWidth,
		_RESOLUTION.iHeight, DXGI_FORMAT_R32G32B32A32_FLOAT);

	pTarget->OnRender(true);
	pTarget->SetScale(100.f, 100.f, 1.f);
	pTarget->SetPos(100.f, 200.f, 0.f);

	SAFE_RELEASE(pTarget);

	// Shader Map Target
	pTarget = CreateTarget("ShadowMap", _RESOLUTION.iWidth, _RESOLUTION.iHeight, DXGI_FORMAT_R32G32B32A32_FLOAT);

	pTarget->OnRender(true);
	pTarget->SetScale(100.f, 100.f, 1.f);
	pTarget->SetPos(100.f, 300.f, 0.f);

	SAFE_RELEASE(pTarget);

	pDepth = CreateDepthTarget("ShadowMapDepth",
		_RESOLUTION.iWidth, _RESOLUTION.iHeight,
		DXGI_FORMAT_D24_UNORM_S8_UINT);

	SAFE_RELEASE(pDepth);

	m_pLightBlend = GET_SINGLE(CShaderManager)->FindShader(LIGHT_BLEND_SHADER);
	m_pLightBlendTarget = GET_SINGLE(CShaderManager)->FindShader(LIGHT_BLEND_OUTPUT_SHADER);

	return true;
}

void CRenderManager::Render(float fTime)
{
	// 출력 오브젝트들을 정렬한다.
	for (int i = 0; i < RG_END; ++i)
	{
		if (i == RG_DEFAULT || i == RG_UI)
		{
			if (m_vecRender[i].size() >= 2)
				sort(m_vecRender[i].begin(), m_vecRender[i].end(), CRenderManager::ObjectZSort);
		}

		else
		{
			if (m_vecRender[i].size() >= 2)
				sort(m_vecRender[i].begin(), m_vecRender[i].end(), CRenderManager::ObjectZSortDescending);
		}
	}

	//  그림자 맵을 만들어준다.
	if (m_bShadow)
		RenderShadowMap(fTime);

	// GBuffer를 만들어준다.
	RenderGBuffer(fTime);

	// 조명 누적 버퍼를 만들어준다.
	RenderLightAcc(fTime);

	// 조명버퍼와 GBuffer를 합성한다.
	RenderLightBlend(fTime);

	// 최종 합성된 버퍼를 화면에 출력한다.
	RenderLightBlendTarget(fTime);


	// 알파오브젝트와 UI를 그린다.
	CAR3DRenderTarget*	pDepth = FindTarget("Depth");

	for (int i = RG_ALPHA1; i < RG_END; ++i)
	{
		for (size_t j = 0; j < m_vecRender[i].size(); ++j)
		{
			m_pPointSmp->SetSampler(11, CUT_PIXEL);
			pDepth->SetTexture(13);
			m_vecRender[i][j]->Render(fTime);
		}
	}

	SAFE_RELEASE(pDepth);

	//if (GetAsyncKeyState(VK_RETURN) & 0x8000)
	//{
	//	SaveTarget("Albedo", L"Albedo.png");
	//	SaveTarget("Normal", L"Normal.png");
	//	SaveTarget("Depth", L"Depth.png");
	//	SaveTarget("Shininess", L"Shininess.png");
	//}

	for (int i = 0; i < RG_END; ++i)
	{
		m_vecRender[i].clear();
	}

	//////// 마지막으로 렌더타겟을 출력해준다.
	//unordered_map<string, CAR3DRenderTarget*>::iterator	iter;
	//unordered_map<string, CAR3DRenderTarget*>::iterator	iterEnd = m_mapRenderTarget.end();

	//for (iter = m_mapRenderTarget.begin(); iter != iterEnd; ++iter)
	//{
	//	iter->second->Render();
	//}
}

void CRenderManager::RenderGBuffer(float fTime)
{
	ChangeMRT("GBuffer");
	ClearMRT("GBuffer");

	for (size_t i = 0; i < m_vecRender[RG_DEFAULT].size(); ++i)
	{
		m_vecRender[RG_DEFAULT][i]->Render(fTime);
	}

	ResetMRT("GBuffer");
}

void CRenderManager::RenderLightAcc(float fTime)
{
	ChangeMRT("LightAcc");
	ClearMRT("LightAcc");

	CScene*	pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

	const list<CGameObject*>* pLightList = pScene->GetLightList();

	list<CGameObject*>::const_iterator	iter;
	list<CGameObject*>::const_iterator	iterEnd = pLightList->end();

	PMRT	pGBuffer = FindMRT("GBuffer");

	for (iter = pLightList->begin(); iter != iterEnd; ++iter)
	{
		UpdateTransform();
		CLight*	pLight = (CLight*)(*iter)->FindComponentFromType(CT_LIGHT);

		pLight->SetLight();

		SAFE_RELEASE(pLight);

		m_pBlendOne->SetState();
		m_pZDisable->SetState();

		CONTEXT->IASetInputLayout(NULL);
		m_pLightAcc->SetShader();

		m_pPointSmp->SetSampler(11, CUT_PIXEL);
		pGBuffer->vecTarget[1]->SetTexture(12);
		pGBuffer->vecTarget[2]->SetTexture(13);
		pGBuffer->vecTarget[3]->SetTexture(14);

		UINT	iOffset = 0;
		CONTEXT->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
		CONTEXT->IASetVertexBuffers(0, 0, NULL, 0, &iOffset);
		CONTEXT->IASetIndexBuffer(0, DXGI_FORMAT_UNKNOWN, 0);
		CONTEXT->Draw(4, 0);

		m_pZDisable->ResetState();
		m_pBlendOne->ResetState();
	}

	// 파티클 조명을 처리한다.
	list<CParticleSystem*>::iterator	iter1;
	list<CParticleSystem*>::iterator	iter1End = m_ParticleLightList.end();

	for (iter1 = m_ParticleLightList.begin(); iter1 != iter1End; ++iter1)
	{
		m_pBlendOne->SetState();

		m_pPointSmp->SetSampler(11, CUT_PIXEL);
		pGBuffer->vecTarget[1]->SetTexture(12);
		pGBuffer->vecTarget[2]->SetTexture(13);
		pGBuffer->vecTarget[3]->SetTexture(14);

		(*iter1)->RenderLight(fTime);

		m_pBlendOne->ResetState();
	}

	m_ParticleLightList.clear();

	//LightLamp m_pLampLight
	// 램프 조명을 처리한다.
	if ((GET_SINGLE(CGameTotalManager)->GetNight()))
	{
		list<CLampLight*>::iterator	iter2;
		list<CLampLight*>::iterator	iter2End = m_pLampLight.end();

		for (iter2 = m_pLampLight.begin(); iter2 != iter2End; ++iter2)
		{
			m_pBlendOne->SetState();

			m_pPointSmp->SetSampler(11, CUT_PIXEL);
			pGBuffer->vecTarget[1]->SetTexture(12);
			pGBuffer->vecTarget[2]->SetTexture(13);
			pGBuffer->vecTarget[3]->SetTexture(14);

			(*iter2)->RenderLight(fTime);

			m_pBlendOne->ResetState();
		}

		m_pLampLight.clear();
	}


	//
	ResetMRT("LightAcc");
}

void CRenderManager::RenderLightBlend(float fTime)
{
	ChangeTarget("LightBlend");
	ClearTarget("LightBlend");

	CAR3DRenderTarget*	pAlbedo = FindTarget("Albedo");
	CAR3DRenderTarget*	pLightDif = FindTarget("LightAccDif");
	CAR3DRenderTarget*	pLightSpc = FindTarget("LightAccSpc");

	m_pZDisable->SetState();

	CONTEXT->IASetInputLayout(NULL);
	m_pLightBlend->SetShader();

	m_pPointSmp->SetSampler(11, CUT_PIXEL);
	pAlbedo->SetTexture(11);
	pLightDif->SetTexture(12);
	pLightSpc->SetTexture(13);

	UINT	iOffset = 0;
	CONTEXT->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	CONTEXT->IASetVertexBuffers(0, 0, NULL, 0, &iOffset);
	CONTEXT->IASetIndexBuffer(0, DXGI_FORMAT_UNKNOWN, 0);
	CONTEXT->Draw(4, 0);

	m_pZDisable->ResetState();

	ResetTarget("LightBlend");

	SAFE_RELEASE(pAlbedo);
	SAFE_RELEASE(pLightDif);
	SAFE_RELEASE(pLightSpc);
}

void CRenderManager::RenderLightBlendTarget(float fTime)
{
	CAR3DRenderTarget*	pAlbedo = FindTarget("LightBlend");

	m_pZDisable->SetState();
	m_pAlphaBlend->SetState();

	CONTEXT->IASetInputLayout(NULL);
	m_pLightBlendTarget->SetShader();

	m_pPointSmp->SetSampler(11, CUT_PIXEL);
	pAlbedo->SetTexture(11);

	UINT	iOffset = 0;
	CONTEXT->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	CONTEXT->IASetVertexBuffers(0, 0, NULL, 0, &iOffset);
	CONTEXT->IASetIndexBuffer(0, DXGI_FORMAT_UNKNOWN, 0);
	CONTEXT->Draw(4, 0);

	m_pAlphaBlend->ResetState();
	m_pZDisable->ResetState();
	SAFE_RELEASE(pAlbedo);
}

void CRenderManager::UpdateTransform()
{
	TRANSFORMCBUFFER	tTransform = {};

	CScene*	pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CCamera*	pCamera = pScene->GetMainCamera();

	tTransform.matWorld = XMMatrixIdentity();
	tTransform.matView = pCamera->GetViewMatrix();
	tTransform.matProj = pCamera->GetProjMatrix();
	tTransform.matWV = tTransform.matWorld * tTransform.matView;
	tTransform.matWVP = tTransform.matWV * tTransform.matProj;
	tTransform.matVP = tTransform.matView * tTransform.matProj;
	tTransform.matInvProj = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matProj),
		tTransform.matProj);
	tTransform.matInvView = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matView),
		tTransform.matView);
	tTransform.matInvVP = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matVP),
		tTransform.matVP);

	SAFE_RELEASE(pCamera);

	tTransform.matWorld = XMMatrixTranspose(tTransform.matWorld);
	tTransform.matView = XMMatrixTranspose(tTransform.matView);
	tTransform.matProj = XMMatrixTranspose(tTransform.matProj);
	tTransform.matWV = XMMatrixTranspose(tTransform.matWV);
	tTransform.matWVP = XMMatrixTranspose(tTransform.matWVP);
	tTransform.matVP = XMMatrixTranspose(tTransform.matVP);
	tTransform.matInvProj = XMMatrixTranspose(tTransform.matInvProj);
	tTransform.matInvView = XMMatrixTranspose(tTransform.matInvView);
	tTransform.matInvVP = XMMatrixTranspose(tTransform.matInvVP);

	GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Transform", &tTransform, CUT_VERTEX | CUT_GEOMETRY | CUT_PIXEL);
}

void CRenderManager::RenderShadowMap(float fTime)
{
	//CAR3DRenderTarget*	pTarget = FindTarget("ShadowMap");
	//CAR3DDepthTarget*	pDepth = FindDepthTarget("ShadowMapDepth");

	//pTarget->ClearTarget();
	//pDepth->ClearTarget();

	//ID3D11RenderTargetView*	pOldTarget;
	//ID3D11DepthStencilView*	pOldDepth;
	//CONTEXT->OMGetRenderTargets(1, &pOldTarget, &pOldDepth);
	//ID3D11RenderTargetView*	pRenderTarget = pTarget->GetTargetView();
	//ID3D11DepthStencilView*	pDepthTarget = pDepth->GetDepthView();

	//CONTEXT->OMSetRenderTargets(1, &pRenderTarget, pDepthTarget);

	//CScene*	pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

	//CGameObject*	pLightObj = pScene->FindLightObj("GlobalLight");

	//CTransform*	pLightTr = pLightObj->GetTransform();

	//DxVector3	vLightDir = pLightTr->GetWorldAxis(AXIS_Z);

	//CCamera*	pCamera = pScene->GetMainCamera();

	//DxVector3	vLookAt = pCamera->GetAttachPos();

	//SAFE_RELEASE(pCamera);

	//DxVector3	vPos = vLookAt - vLightDir * 50.f;

	//// 조명의 방향과 위치를 이용해서 뷰 행렬을 구해준다.
	//MATRIX	matView, matProj;

	//matView = XMMatrixIdentity();

	//DxVector3	vUp = DxVector3(0.f, 1.f, 0.f);

	//DxVector3	vRight = vUp.Cross(vLightDir);
	//vUp = vLightDir.Cross(vRight);

	//memcpy(&matView.m[0][0], &vRight, sizeof(DxVector3));
	//memcpy(&matView.m[1][0], &vUp, sizeof(DxVector3));
	//memcpy(&matView.m[2][0], &vLightDir, sizeof(DxVector3));

	//matView = matView.Transpose();

	//matView.m[3][0] = -vPos.Dot(vRight);
	//matView.m[3][1] = -vPos.Dot(vUp);
	//matView.m[3][2] = -vPos.Dot(vLightDir);

	//matView = XMMatrixPerspectiveFovLH(AR3D_PI / 3.f, 1920.f / 1080.f, 0.3f, 5000.f);

	//for (size_t i = 0; i < m_vecRender[RG_DEFAULT].size(); ++i)
	//{
	//	m_vecRender[RG_DEFAULT][i]->RenderShadow(fTime);
	//}

	//SAFE_RELEASE(pLightTr);
	//SAFE_RELEASE(pLightObj);

	//CONTEXT->OMSetRenderTargets(1, &pOldTarget, pOldDepth);
	//SAFE_RELEASE(pOldTarget);
	//SAFE_RELEASE(pOldDepth);

	//SAFE_RELEASE(pTarget);
	//SAFE_RELEASE(pDepth);
}

bool CRenderManager::AddMRTTarget(const string & strMRTKey,
	const string & strTargetKey)
{
	PMRT	pMRT = FindMRT(strMRTKey);

	if (!pMRT)
	{
		pMRT = new MRT;
		pMRT->pDepth = NULL;
		pMRT->pOldDepth = NULL;
		pMRT->pNullDepth = NULL;

		m_mapMRT.insert(make_pair(strMRTKey, pMRT));
	}

	CAR3DRenderTarget*	pTarget = FindTarget(strTargetKey);

	pMRT->vecTarget.push_back(pTarget);

	return true;
}

bool CRenderManager::SetMRTDepth(const string & strMRTKey,
	const string & strDepthKey)
{
	PMRT	pMRT = FindMRT(strMRTKey);

	if (!pMRT)
	{
		pMRT = new MRT;
		pMRT->pDepth = NULL;
		pMRT->pOldDepth = NULL;
		pMRT->pNullDepth = NULL;

		m_mapMRT.insert(make_pair(strMRTKey, pMRT));
	}

	CAR3DDepthTarget*	pTarget = FindDepthTarget(strDepthKey);

	pMRT->pDepth = pTarget;

	return true;
}

PMRT CRenderManager::FindMRT(const string & strKey)
{
	unordered_map<string, PMRT>::iterator	iter = m_mapMRT.find(strKey);

	if (iter == m_mapMRT.end())
		return NULL;

	return iter->second;
}

void CRenderManager::ChangeMRT(const string & strKey)
{
	PMRT	pMRT = FindMRT(strKey);

	if (!pMRT)
		return;

	// 타겟정보를 만든다.
	vector<ID3D11RenderTargetView*>	vecTarget;

	for (size_t i = 0; i < pMRT->vecTarget.size(); ++i)
	{
		vecTarget.push_back(pMRT->vecTarget[i]->GetTargetView());
	}

	ID3D11DepthStencilView*	pDepthView;

	if (pMRT->pDepth)
		pDepthView = pMRT->pDepth->GetDepthView();

	else
	{
		CONTEXT->OMGetRenderTargets(0, NULL, &pMRT->pNullDepth);
		pDepthView = pMRT->pNullDepth;
	}

	pMRT->vecOldTarget.clear();
	pMRT->vecOldTarget.resize(pMRT->vecTarget.size());

	CONTEXT->OMGetRenderTargets(pMRT->vecOldTarget.size(),
		&pMRT->vecOldTarget[0], &pMRT->pOldDepth);

	CONTEXT->OMSetRenderTargets(vecTarget.size(),
		&vecTarget[0], pDepthView);
}

void CRenderManager::ResetMRT(const string & strKey)
{
	PMRT	pMRT = FindMRT(strKey);

	if (!pMRT)
		return;

	CONTEXT->OMSetRenderTargets(pMRT->vecOldTarget.size(),
		&pMRT->vecOldTarget[0], pMRT->pOldDepth);

	Safe_Release_VecList(pMRT->vecOldTarget);
	SAFE_RELEASE(pMRT->pOldDepth);
	SAFE_RELEASE(pMRT->pNullDepth);
}

void CRenderManager::ClearMRT(const string & strKey)
{
	PMRT	pMRT = FindMRT(strKey);

	if (!pMRT)
		return;

	for (size_t i = 0; i < pMRT->vecTarget.size(); ++i)
	{
		pMRT->vecTarget[i]->ClearTarget();
	}

	if (pMRT->pDepth)
		pMRT->pDepth->ClearTarget();

	else
		CONTEXT->ClearDepthStencilView(pMRT->pNullDepth, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.f, 0);
}

CAR3DRenderTarget * CRenderManager::CreateTarget(const string & strKey,
	int iWidth, int iHeight, DXGI_FORMAT eFmt)
{
	CAR3DRenderTarget*	pTarget = new CAR3DRenderTarget;

	if (!pTarget->CreateTarget(strKey, iWidth, iHeight, eFmt))
	{
		SAFE_RELEASE(pTarget);
		return NULL;
	}

	pTarget->AddRef();

	m_mapRenderTarget.insert(make_pair(strKey, pTarget));

	return pTarget;
}

CAR3DDepthTarget * CRenderManager::CreateDepthTarget(
	const string & strKey, int iWidth, int iHeight,
	DXGI_FORMAT eFmt)
{
	CAR3DDepthTarget*	pTarget = new CAR3DDepthTarget;

	if (!pTarget->CreateDepth(strKey, iWidth, iHeight, eFmt))
	{
		SAFE_RELEASE(pTarget);
		return NULL;
	}

	pTarget->AddRef();

	m_mapDepthTarget.insert(make_pair(strKey, pTarget));

	return pTarget;
}

void CRenderManager::ChangeTarget(const string & strKey)
{
	CAR3DRenderTarget*	pTarget = FindTarget(strKey);

	if (!pTarget)
		return;

	pTarget->ChangeTarget();

	SAFE_RELEASE(pTarget);
}

void CRenderManager::ResetTarget(const string & strKey)
{
	CAR3DRenderTarget*	pTarget = FindTarget(strKey);

	if (!pTarget)
		return;

	pTarget->ResetTarget();

	SAFE_RELEASE(pTarget);
}

void CRenderManager::ClearTarget(const string & strKey)
{
	CAR3DRenderTarget*	pTarget = FindTarget(strKey);

	if (!pTarget)
		return;

	pTarget->ClearTarget();

	SAFE_RELEASE(pTarget);
}

void CRenderManager::ClearDepthTarget(const string & strKey)
{
	CAR3DDepthTarget*	pTarget = FindDepthTarget(strKey);

	if (!pTarget)
		return;

	pTarget->ClearTarget();

	SAFE_RELEASE(pTarget);
}

void CRenderManager::SaveTarget(const string & strKey, const wchar_t * pFileName,
	const string & strPathKey)
{
	CAR3DRenderTarget*	pTarget = FindTarget(strKey);

	if (!pTarget)
		return;

	pTarget->SaveTarget(pFileName, strPathKey);

	SAFE_RELEASE(pTarget);
}

CAR3DRenderTarget * CRenderManager::FindTarget(const string & strKey)
{
	unordered_map<string, class CAR3DRenderTarget*>::iterator	iter = m_mapRenderTarget.find(strKey);

	if (iter == m_mapRenderTarget.end())
		return NULL;

	iter->second->AddRef();

	return iter->second;
}

CAR3DDepthTarget * CRenderManager::FindDepthTarget(const string & strKey)
{
	unordered_map<string, class CAR3DDepthTarget*>::iterator	iter = m_mapDepthTarget.find(strKey);

	if (iter == m_mapDepthTarget.end())
		return NULL;

	iter->second->AddRef();

	return iter->second;
}

CRasterizerState * CRenderManager::CreateRasterizerState(const string & strKey,
	D3D11_FILL_MODE fillMode, D3D11_CULL_MODE cullMode,
	BOOL frontCounterClockwise, INT depthBias, FLOAT depthBiasClamp,
	FLOAT slopeScaledDepthBias, BOOL depthClipEnable, BOOL scissorEnable,
	BOOL multisampleEnable, BOOL antialiasedLineEnable)
{
	CRasterizerState*	pState = (CRasterizerState*)FindRenderState(strKey);

	if (pState)
		return pState;

	pState = new CRasterizerState;

	if (!pState->CreateRasterizerState(strKey, fillMode, cullMode,
		frontCounterClockwise, depthBias, depthBiasClamp,
		slopeScaledDepthBias, depthClipEnable, scissorEnable,
		multisampleEnable, antialiasedLineEnable))
	{
		SAFE_RELEASE(pState);
		return NULL;
	}

	pState->AddRef();

	m_mapRenderState.insert(make_pair(strKey, pState));

	return pState;
}

CBlendState * CRenderManager::CreateBlendState(const string & strKey,
	bool bAlphaCoverage, bool bIndependent)
{
	CBlendState*	pState = (CBlendState*)FindRenderState(strKey);

	if (pState)
		return pState;

	pState = new CBlendState;

	if (!pState->CreateBlendState(strKey, &m_vecRenderTargetBlend,
		bAlphaCoverage, bIndependent))
	{
		SAFE_RELEASE(pState);
		return NULL;
	}

	pState->AddRef();

	m_mapRenderState.insert(make_pair(strKey, pState));

	m_vecRenderTargetBlend.clear();

	return pState;
}

bool CRenderManager::AddRenderTargetBlendInfo(bool bEnable, D3D11_BLEND eSrcBlend,
	D3D11_BLEND eDestBlend, D3D11_BLEND_OP eBlendOp, D3D11_BLEND eSrcBlendAlpha,
	D3D11_BLEND eDestBlendAlpha,
	D3D11_BLEND_OP eBlendOpAlpha, UINT8 iTargetWriteMask)
{
	if (m_vecRenderTargetBlend.size() == 8)
		return false;

	D3D11_RENDER_TARGET_BLEND_DESC	tDesc = {};

	tDesc.BlendEnable = bEnable;
	tDesc.SrcBlend = eSrcBlend;
	tDesc.DestBlend = eDestBlend;
	tDesc.BlendOp = eBlendOp;
	tDesc.SrcBlendAlpha = eSrcBlendAlpha;
	tDesc.DestBlendAlpha = eDestBlendAlpha;
	tDesc.BlendOpAlpha = eBlendOpAlpha;
	tDesc.RenderTargetWriteMask = iTargetWriteMask;

	m_vecRenderTargetBlend.push_back(tDesc);

	return true;
}

CDepthStencilState * CRenderManager::CreateDepthStencilState(const string & strKey,
	bool bDepthEnable, D3D11_DEPTH_WRITE_MASK eDepthMask,
	D3D11_COMPARISON_FUNC eDepthFunc, bool bStencilEnable,
	UINT8 iStencilReadMask, UINT8 iStencilWriteMask,
	D3D11_DEPTH_STENCILOP_DESC tFrontFace,
	D3D11_DEPTH_STENCILOP_DESC tBackFace)
{
	CDepthStencilState*	pState = (CDepthStencilState*)FindRenderState(strKey);

	if (pState)
		return pState;

	pState = new CDepthStencilState;

	if (!pState->CreateDepthStencilState(strKey, bDepthEnable,
		eDepthMask, eDepthFunc, bStencilEnable, iStencilReadMask,
		iStencilWriteMask, tFrontFace, tBackFace))
	{
		SAFE_RELEASE(pState);
		return NULL;
	}

	pState->AddRef();

	m_mapRenderState.insert(make_pair(strKey, pState));

	return pState;
}

CRenderState * CRenderManager::FindRenderState(const string & strKey)
{
	unordered_map<string, CRenderState*>::iterator	iter = m_mapRenderState.find(strKey);

	if (iter == m_mapRenderState.end())
		return NULL;

	iter->second->AddRef();

	return iter->second;
}

bool CRenderManager::ObjectZSort(CGameObject * p1, CGameObject * p2)
{
	CTransform*	pTr1 = p1->GetTransform();
	CTransform*	pTr2 = p2->GetTransform();

	CGameObject*	pCameraObj = GET_SINGLE(CSceneManager)->GetCurrentScene()->GetMainCameraObj();

	CTransform*	pCamTr = pCameraObj->GetTransform();

	float	fDist1, fDist2;

	fDist1 = pTr1->GetWorldPos().Distance(pCamTr->GetWorldPos());
	fDist2 = pTr2->GetWorldPos().Distance(pCamTr->GetWorldPos());

	SAFE_RELEASE(pCamTr);
	SAFE_RELEASE(pTr1);
	SAFE_RELEASE(pTr2);

	SAFE_RELEASE(pCameraObj);


	return fDist1 < fDist2;
}

bool CRenderManager::ObjectZSortDescending(CGameObject * p1, CGameObject * p2)
{
	CTransform*	pTr1 = p1->GetTransform();
	CTransform*	pTr2 = p2->GetTransform();

	CGameObject*	pCameraObj = GET_SINGLE(CSceneManager)->GetCurrentScene()->GetMainCameraObj();

	CTransform*	pCamTr = pCameraObj->GetTransform();

	float	fDist1, fDist2;

	fDist1 = pTr1->GetWorldPos().Distance(pCamTr->GetWorldPos());
	fDist2 = pTr2->GetWorldPos().Distance(pCamTr->GetWorldPos());

	SAFE_RELEASE(pCamTr);
	SAFE_RELEASE(pTr1);
	SAFE_RELEASE(pTr2);

	SAFE_RELEASE(pCameraObj);


	return fDist1 > fDist2;
}
