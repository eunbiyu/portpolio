#include "BlendState.h"
#include "../Device.h"

AR3D_USING

CBlendState::CBlendState() :
	m_iSampleMask(0xffffffff)
{
	SetTag("BlendState");
	SetTypeName("CBlendState");
	SetTypeID<CBlendState>();
	m_eType = RST_BLEND;

	memset(m_fBlendFactor, 0, sizeof(float) * 4);
}


CBlendState::~CBlendState()
{
}

bool CBlendState::CreateBlendState(const string & strKey, 
	const vector<D3D11_RENDER_TARGET_BLEND_DESC>* pTargetBlend,
	bool bAlphaCoverage, bool bIndependent)
{
	D3D11_BLEND_DESC tDesc = {};
	tDesc.AlphaToCoverageEnable = bAlphaCoverage;
	tDesc.IndependentBlendEnable = bIndependent;

	int		iCount = 0;
	vector<D3D11_RENDER_TARGET_BLEND_DESC>::const_iterator iter;

	for (iter = pTargetBlend->begin(); iter != pTargetBlend->end(); ++iter, ++iCount)
	{
		tDesc.RenderTarget[iCount] = *iter;
	}
	if (FAILED(DEVICE->CreateBlendState(&tDesc, (ID3D11BlendState**)&m_pState)))
		return false;
	return true;
}

void CBlendState::SetState()
{
	CONTEXT->OMGetBlendState((ID3D11BlendState**)&m_pDefaultState,
		m_fOldBlendFactor, &m_iOldSampleMask);
	CONTEXT->OMSetBlendState((ID3D11BlendState*)m_pState, m_fBlendFactor, m_iSampleMask);
}

void CBlendState::ResetState()
{
	CONTEXT->OMSetBlendState((ID3D11BlendState*)m_pDefaultState,
		m_fOldBlendFactor, m_iOldSampleMask);
	SAFE_RELEASE(m_pDefaultState);
}

bool CBlendState::CreateAlphablendState()
{
	//투명한 블랜드 상태객체 만들기

	D3D11_BLEND_DESC blendStateDescription = {};

	// Clear the blend state description.
	ZeroMemory(&blendStateDescription, sizeof(D3D11_BLEND_DESC));

	blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
	blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendStateDescription.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	if (FAILED(DEVICE->CreateBlendState(&blendStateDescription, (ID3D11BlendState**)&m_pState)))
		return false;

	return true;
}
