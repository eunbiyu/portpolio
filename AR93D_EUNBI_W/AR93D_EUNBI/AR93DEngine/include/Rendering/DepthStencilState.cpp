#include "DepthStencilState.h"
#include "../Device.h"

AR3D_USING

CDepthStencilState::CDepthStencilState()
{
	SetTag("DepthStencilState");
	SetTypeName("CDepthStencilState");
	SetTypeID<CDepthStencilState>();
	m_eType = RST_DEPTH;
	m_iStencilRef = 0xffffffff;
}

CDepthStencilState::~CDepthStencilState()
{
}

bool CDepthStencilState::CreateDepthStencilState(const string & strKey,
	bool bDepthEnable, D3D11_DEPTH_WRITE_MASK eDepthMask,
	D3D11_COMPARISON_FUNC eDepthFunc, bool bStencilEnable,
	UINT8 iStencilReadMask, UINT8 iStencilWriteMask,
	D3D11_DEPTH_STENCILOP_DESC tFrontFace,
	D3D11_DEPTH_STENCILOP_DESC tBackFace)
{
	/*
	BOOL DepthEnable;
	D3D11_DEPTH_WRITE_MASK DepthWriteMask;
	D3D11_COMPARISON_FUNC DepthFunc;
	BOOL StencilEnable;
	UINT8 StencilReadMask;
	UINT8 StencilWriteMask;
	D3D11_DEPTH_STENCILOP_DESC FrontFace;
	D3D11_DEPTH_STENCILOP_DESC BackFace;
	*/
	D3D11_DEPTH_STENCIL_DESC	tDesc = {};
	tDesc.DepthEnable = bDepthEnable;
	tDesc.DepthWriteMask = eDepthMask;
	tDesc.DepthFunc = eDepthFunc;
	tDesc.StencilEnable = bStencilEnable;
	tDesc.StencilReadMask = iStencilReadMask;
	tDesc.StencilWriteMask = iStencilWriteMask;
	tDesc.FrontFace = tFrontFace;
	tDesc.BackFace = tBackFace;

	if (FAILED(DEVICE->CreateDepthStencilState(&tDesc,
		(ID3D11DepthStencilState**)&m_pState)))
		return false;


	return true;
}

void CDepthStencilState::SetState()
{
	CONTEXT->OMGetDepthStencilState(
		(ID3D11DepthStencilState**)&m_pDefaultState, &m_iOldStencilRef);
	CONTEXT->OMSetDepthStencilState((ID3D11DepthStencilState*)m_pState,
		m_iStencilRef);
}

void CDepthStencilState::ResetState()
{
	CONTEXT->OMSetDepthStencilState((ID3D11DepthStencilState*)m_pDefaultState,
		m_iOldStencilRef);
	SAFE_RELEASE(m_pDefaultState);
}
