#include "RasterizerState.h"
#include "../Device.h"

AR3D_USING

CRasterizerState::CRasterizerState()
{
	SetTag("RasterizerState");
	SetTypeName("CRasterizerState");
	SetTypeID<CRasterizerState>();
	m_eType = RST_RASTERIZER;
}

CRasterizerState::~CRasterizerState()
{
}

bool CRasterizerState::CreateRasterizerState(const string& strKey,
	D3D11_FILL_MODE fillMode,
	D3D11_CULL_MODE cullMode,
	BOOL frontCounterClockwise,
	INT depthBias,
	FLOAT depthBiasClamp,
	FLOAT slopeScaledDepthBias,
	BOOL depthClipEnable,
	BOOL scissorEnable,
	BOOL multisampleEnable,
	BOOL antialiasedLineEnable)
{
	m_strKey = strKey;

	D3D11_RASTERIZER_DESC	tDesc = {};

	tDesc.FillMode = fillMode;
	tDesc.CullMode = D3D11_CULL_NONE;
	tDesc.FrontCounterClockwise = frontCounterClockwise;
	tDesc.DepthBias = depthBias;
	tDesc.DepthBiasClamp = depthBiasClamp;
	tDesc.SlopeScaledDepthBias = slopeScaledDepthBias;
	tDesc.DepthClipEnable = depthClipEnable;
	tDesc.ScissorEnable = scissorEnable;
	tDesc.MultisampleEnable = multisampleEnable;
	tDesc.AntialiasedLineEnable = antialiasedLineEnable;

	if (FAILED(DEVICE->CreateRasterizerState(&tDesc, (ID3D11RasterizerState**)&m_pState)))
		return false;

	return true;
}

void CRasterizerState::SetState()
{
	CONTEXT->RSGetState((ID3D11RasterizerState**)&m_pDefaultState);
	CONTEXT->RSSetState((ID3D11RasterizerState*)m_pState);
}

void CRasterizerState::ResetState()
{
	CONTEXT->RSSetState((ID3D11RasterizerState*)m_pDefaultState);
	SAFE_RELEASE(m_pDefaultState);
}
