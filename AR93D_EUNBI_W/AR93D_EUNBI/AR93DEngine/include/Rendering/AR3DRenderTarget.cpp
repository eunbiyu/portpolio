#include "AR3DRenderTarget.h"
#include "../Device.h"
#include "../Resources/Texture.h"
#include "../Resources/Mesh.h"
#include "../Resources/ResourcesManager.h"
#include "Shader.h"
#include "ShaderManager.h"
#include "../Scene/Scene.h"
#include "../Scene/SceneManager.h"
#include "../Component/Camera.h"
#include "RenderManager.h"
#include "RenderState.h"
#include "../Resources/Sampler.h"

AR3D_USING

CAR3DRenderTarget::CAR3DRenderTarget() :
	m_pTargetTex(NULL),
	m_pTargetView(NULL),
	m_pOldView(NULL),
	m_pOldDepth(NULL)
{
	memset(m_fClearColor, 0, sizeof(float) * 4);
////#ifdef _DEBUG
	m_bIsRender = false;
	m_pMesh = GET_SINGLE(CResourcesManager)->FindMesh("UIMesh");
	m_pShader = GET_SINGLE(CShaderManager)->FindShader(UI_SHADER);
	m_pZDisable = GET_SINGLE(CRenderManager)->FindRenderState(DEPTH_DISABLE);
	m_pSampler = GET_SINGLE(CResourcesManager)->FindSampler("Linear");

	m_tMtrl.vDif = DxVector4(1.f, 1.f, 1.f, 1.f);
////#endif // _DEBUG

}

CAR3DRenderTarget::~CAR3DRenderTarget()
{
//#ifdef _DEBUG
	SAFE_RELEASE(m_pSampler);
	SAFE_RELEASE(m_pMesh);
	SAFE_RELEASE(m_pShader);
	SAFE_RELEASE(m_pZDisable);
//#endif // _DEBUG
	SAFE_RELEASE(m_pOldView);
	SAFE_RELEASE(m_pOldDepth);
	SAFE_RELEASE(m_pTargetTex);
	SAFE_RELEASE(m_pTargetView);
}

ID3D11RenderTargetView * CAR3DRenderTarget::GetTargetView() const
{
	return m_pTargetView;
}

void CAR3DRenderTarget::OnRender(bool bRender)
{
//#ifdef _DEBUG
	m_bIsRender = bRender;
//#endif
}

void CAR3DRenderTarget::SetPos(float x, float y, float z)
{
//#ifdef _DEBUG
	m_vPos = DxVector3(x, y, z);
//#endif // _DEBUG
}

void CAR3DRenderTarget::SetScale(float x, float y, float z)
{
//#ifdef _DEBUG
	m_vScale = DxVector3(x, y, z);
//#endif // _DEBUG
}

void CAR3DRenderTarget::SetClearColor(float r, float g, float b, float a)
{
	m_fClearColor[0] = r;
	m_fClearColor[1] = g;
	m_fClearColor[2] = b;
	m_fClearColor[3] = a;
}

void CAR3DRenderTarget::SetClearColor(float fColor[4])
{
	for (int i = 0; i < 4; ++i)
	{
		m_fClearColor[i] = fColor[i];
	}
}

bool CAR3DRenderTarget::CreateTarget(const string& strName, int iWidth, int iHeight,
	DXGI_FORMAT eFmt)
{
	m_pTargetTex = CTexture::CreateTexture(strName, iWidth, iHeight, 1,
		eFmt, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET,
		0);

	if (FAILED(DEVICE->CreateRenderTargetView(m_pTargetTex->GetTexture(), NULL,
		&m_pTargetView)))
		return false;

	return true;
}

void CAR3DRenderTarget::ChangeTarget()
{
	CONTEXT->OMGetRenderTargets(1, &m_pOldView, &m_pOldDepth);
	CONTEXT->OMSetRenderTargets(1, &m_pTargetView, m_pOldDepth);
}

void CAR3DRenderTarget::ResetTarget()
{
	CONTEXT->OMSetRenderTargets(1, &m_pOldView, m_pOldDepth);
	SAFE_RELEASE(m_pOldView);
	SAFE_RELEASE(m_pOldDepth);
}

void CAR3DRenderTarget::ClearTarget()
{
	CONTEXT->ClearRenderTargetView(m_pTargetView, m_fClearColor);


}

void CAR3DRenderTarget::SaveTarget(const wchar_t * pFileName, const string & strPathKey)
{
	m_pTargetTex->SaveTextureFile(pFileName, strPathKey);
}

void CAR3DRenderTarget::SetTexture(int iRegister)
{
	m_pTargetTex->SetTexture(iRegister, CUT_PIXEL);
}

void CAR3DRenderTarget::Render()
{
//#ifdef _DEBUG
	if (m_bIsRender)
	{
		TRANSFORMCBUFFER	tTransform = {};

		CScene*	pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

		CCamera*	pCamera = pScene->FindCamera("UICamera");

		MATRIX	matScale, matTrans, matWorld;

		matScale = XMMatrixScaling(m_vScale.x, m_vScale.y,
			m_vScale.z);
		matTrans = XMMatrixTranslation(m_vPos.x, m_vPos.y,
			m_vPos.z);

		matWorld = matScale * matTrans;

		tTransform.matWorld = matWorld.mat;
		tTransform.matView = XMMatrixIdentity();
		tTransform.matProj = pCamera->GetProjMatrix();
		tTransform.matWV = tTransform.matWorld * tTransform.matView;
		tTransform.matWVP = tTransform.matWV * tTransform.matProj;
		tTransform.matVP = tTransform.matView * tTransform.matProj;
		tTransform.vPivot = Vec3Zero;
		tTransform.vMeshSize = m_pMesh->GetMeshSize();
		tTransform.vMeshMin = m_pMesh->GetMeshMin();
		tTransform.vMeshMax = m_pMesh->GetMeshMax();
		tTransform.matInvProj = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matProj),
			tTransform.matProj);
		tTransform.matInvView = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matView),
			tTransform.matView);
		tTransform.matInvVP = XMMatrixInverse(&XMMatrixDeterminant(tTransform.matVP),
			tTransform.matVP);

		SAFE_RELEASE(pCamera);

		tTransform.matWorld = XMMatrixTranspose(tTransform.matWorld);
		tTransform.matView = XMMatrixTranspose(tTransform.matView);
		tTransform.matProj = XMMatrixTranspose(tTransform.matProj);
		tTransform.matWV = XMMatrixTranspose(tTransform.matWV);
		tTransform.matWVP = XMMatrixTranspose(tTransform.matWVP);
		tTransform.matVP = XMMatrixTranspose(tTransform.matVP);
		tTransform.matInvProj = XMMatrixTranspose(tTransform.matInvProj);
		tTransform.matInvView = XMMatrixTranspose(tTransform.matInvView);
		tTransform.matInvVP = XMMatrixTranspose(tTransform.matInvVP);

		GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Transform", &tTransform, CUT_VERTEX | CUT_GEOMETRY | CUT_PIXEL);

		m_pZDisable->SetState();
		m_pShader->SetShader();

		GET_SINGLE(CShaderManager)->UpdateConstantBuffer("Material", &m_tMtrl, CUT_VERTEX | CUT_GEOMETRY | CUT_PIXEL);

		GET_SINGLE(CShaderManager)->SetInputLayout("TexInputLayout");
		m_pSampler->SetSampler(0, CUT_PIXEL);
		m_pTargetTex->SetTexture(0, CUT_PIXEL);

		m_pMesh->Render();

		m_pZDisable->ResetState();
	}
//#endif // _DEBUG
}


//void CAR3DRenderTarget::EditTerrainSplattingTexture(const wchar_t * pFileName, const string & strPathKey)
//{
//	m_pTargetTex->EditTerrainSplattingTexture(pFileName, strPathKey);
//}
//
//
//void CAR3DRenderTarget::SaveTargetTerrain(const wchar_t * pFileName, bool breset, const string & strPathKey)
//{
//	m_pTargetTex->SaveTextureFileTerrain(pFileName, breset, strPathKey);
//}