#include "AR3DDepthTarget.h"
#include "../Device.h"
#include "../Resources/Texture.h"

AR3D_USING

CAR3DDepthTarget::CAR3DDepthTarget() :
	m_pDepthView(NULL),
	m_pDepthTex(NULL),
	m_pOldView(NULL)
{
}


CAR3DDepthTarget::~CAR3DDepthTarget()
{
	SAFE_RELEASE(m_pDepthTex);
	SAFE_RELEASE(m_pDepthView);
	SAFE_RELEASE(m_pOldView);
}

ID3D11DepthStencilView * CAR3DDepthTarget::GetDepthView() const
{
	return m_pDepthView;
}

bool CAR3DDepthTarget::CreateDepth(const string& strName,
	int iWidth, int iHeight,
	DXGI_FORMAT eFmt)
{
	// ���� Ÿ���� �����.
	D3D11_TEXTURE2D_DESC	tDesc = {};
	tDesc.Width = iWidth;
	tDesc.Height = iHeight;
	tDesc.MipLevels = 1;
	tDesc.ArraySize = 1;
	tDesc.Format = eFmt;
	tDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	tDesc.SampleDesc.Count = 1;
	tDesc.SampleDesc.Quality = 0;
	tDesc.Usage = D3D11_USAGE_DEFAULT;

	if (FAILED(DEVICE->CreateTexture2D(&tDesc, NULL,
		&m_pDepthTex)))
		return false;

	if (FAILED(DEVICE->CreateDepthStencilView(m_pDepthTex,
		NULL, &m_pDepthView)))
		return false;

	return true;
}

void CAR3DDepthTarget::ClearTarget()
{
	CONTEXT->ClearDepthStencilView(m_pDepthView,
		D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.f, 0);
}
