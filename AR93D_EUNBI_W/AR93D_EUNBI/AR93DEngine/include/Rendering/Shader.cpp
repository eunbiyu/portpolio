#include "Shader.h"
#include "../Core/PathManager.h"
#include "../Device.h"

AR3D_USING

CShader::CShader() :
	m_pVSBlob(NULL),
	m_pVS(NULL),
	m_pPSBlob(NULL),
	m_pPS(NULL),
	m_pGSBlob(NULL),
	m_pGS(NULL)
{
}

CShader::~CShader()
{
	SAFE_RELEASE(m_pVSBlob);
	SAFE_RELEASE(m_pVS);
	SAFE_RELEASE(m_pPSBlob);
	SAFE_RELEASE(m_pPS);
	SAFE_RELEASE(m_pGSBlob);
	SAFE_RELEASE(m_pGS);
}

void * CShader::GetVSCode() const
{
	return m_pVSBlob->GetBufferPointer();
}

void * CShader::GetPSCode() const
{
	return m_pPSBlob->GetBufferPointer();
}

int CShader::GetVSCodeSize() const
{
	return m_pVSBlob->GetBufferSize();
}

int CShader::GetPSCodeSize() const
{
	return m_pPSBlob->GetBufferSize();
}

string CShader::GetKey() const
{
	return m_strKey;
}

void CShader::SetStreamDecl(D3D11_SO_DECLARATION_ENTRY * pStreamDecl, UINT iCount)
{
	m_pStreamDecl = pStreamDecl;
	m_iDeclCount = iCount;
}

bool CShader::LoadShader(const string & strKey, TCHAR * pFileName,
	char* pEntry[ST_END], bool bStreamOut, const string & strPathKey)
{
	if (!LoadVertexShader(strKey, pFileName, pEntry[ST_VERTEX], strPathKey))
		return false;

	if (!LoadPixelShader(strKey, pFileName, pEntry[ST_PIXEL], strPathKey))
		return false;

	if (pEntry[ST_GEOMETRY])
	{
		if (!LoadGeometryShader(strKey, pFileName, pEntry[ST_GEOMETRY], bStreamOut, strPathKey))
			return false;
	}

	return true;
}

bool CShader::LoadVertexShader(const string & strKey, TCHAR * pFileName, char * pEntry,
	const string & strPathKey)
{
	if (!pEntry)
		return true;

	m_strKey = strKey;
	UINT	iFlag = 0;
#ifdef _DEBUG
	iFlag = D3D10_SHADER_DEBUG;
#endif // _DEBUG


	// 전체경로를 만들어준다.
	const wchar_t* pPath = GET_SINGLE(CPathManager)->FindPath(strPathKey);
	wstring	strPath;

	if (pPath)
		strPath = pPath;
	strPath += pFileName;

	ID3DBlob*	pError = NULL;
	if (FAILED(D3DCompileFromFile(strPath.c_str(), NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE,
		pEntry, "vs_5_0", iFlag, 0, &m_pVSBlob, &pError)))
	{
		_cprintf("%s\n", (char*)pError->GetBufferPointer());
		return false;
	}

	if (FAILED(DEVICE->CreateVertexShader(m_pVSBlob->GetBufferPointer(), m_pVSBlob->GetBufferSize(),
		NULL, &m_pVS)))
		return false;

	return true;
}

bool CShader::LoadPixelShader(const string & strKey, TCHAR * pFileName, char * pEntry,
	const string & strPathKey)
{
	if (!pEntry)
		return true;

	m_strKey = strKey;

	UINT	iFlag = 0;
#ifdef _DEBUG
	iFlag = D3D10_SHADER_DEBUG;
#endif // _DEBUG


	// 전체경로를 만들어준다.
	const wchar_t* pPath = GET_SINGLE(CPathManager)->FindPath(strPathKey);
	wstring	strPath;

	if (pPath)
		strPath = pPath;
	strPath += pFileName;

	ID3DBlob*	pError = NULL;
	if (FAILED(D3DCompileFromFile(strPath.c_str(), NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE,
		pEntry, "ps_5_0", iFlag, 0, &m_pPSBlob, &pError)))
	{
		_cprintf((char*)pError->GetBufferPointer());
		return false;
	}

	if (FAILED(DEVICE->CreatePixelShader(m_pPSBlob->GetBufferPointer(), m_pPSBlob->GetBufferSize(),
		NULL, &m_pPS)))
		return false;

	return true;
}

bool CShader::LoadGeometryShader(const string & strKey, TCHAR * pFileName, char * pEntry,
	bool bStreamOut, const string & strPathKey)
{
	if (!pEntry)
		return true;

	m_strKey = strKey;

	UINT	iFlag = 0;
#ifdef _DEBUG
	iFlag = D3D10_SHADER_DEBUG;
#endif // _DEBUG


	// 전체경로를 만들어준다.
	const wchar_t* pPath = GET_SINGLE(CPathManager)->FindPath(strPathKey);
	wstring	strPath;

	if (pPath)
		strPath = pPath;
	strPath += pFileName;

	ID3DBlob*	pError = NULL;
	if (FAILED(D3DCompileFromFile(strPath.c_str(), NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE,
		pEntry, "gs_5_0", iFlag, 0, &m_pGSBlob, &pError)))
	{
		_cprintf((char*)pError->GetBufferPointer());
		return false;
	}

	if (!bStreamOut)
	{
		if (FAILED(DEVICE->CreateGeometryShader(m_pGSBlob->GetBufferPointer(), m_pGSBlob->GetBufferSize(),
			NULL, &m_pGS)))
		{
			return false;
		}
	}

	else
	{
		if (FAILED(DEVICE->CreateGeometryShaderWithStreamOutput(m_pGSBlob->GetBufferPointer(),
			m_pGSBlob->GetBufferSize(), m_pStreamDecl, m_iDeclCount, NULL, 0,
			D3D11_SO_NO_RASTERIZED_STREAM, NULL, &m_pGS)))
			return false;
	}

	return true;
}

void CShader::SetShader()
{
	CONTEXT->VSSetShader(m_pVS, NULL, 0);
	CONTEXT->PSSetShader(m_pPS, NULL, 0);
	CONTEXT->GSSetShader(m_pGS, NULL, 0);
}
