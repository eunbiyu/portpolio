#pragma once
#include "../BaseObj.h"

AR3D_BEGIN

class DLL CRenderState :
	public CBaseObj
{
protected:
	friend class CRenderManager;

protected:
	CRenderState();
	virtual ~CRenderState();

protected:
	ID3D11DeviceChild*		m_pState;
	ID3D11DeviceChild*		m_pDefaultState;
	string					m_strKey;
	RENDER_STATE_TYPE		m_eType;

public:
	RENDER_STATE_TYPE GetType()	const;

public:
	virtual void SetState() = 0;
	virtual void ResetState() = 0;
};

AR3D_END
