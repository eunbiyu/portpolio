#include "RenderState.h"

AR3D_USING

CRenderState::CRenderState() :
	m_pState(NULL),
	m_pDefaultState(NULL)
{
}

CRenderState::~CRenderState()
{
	SAFE_RELEASE(m_pState);
	SAFE_RELEASE(m_pDefaultState);
}

RENDER_STATE_TYPE CRenderState::GetType() const
{
	return m_eType;
}
