#include "ShaderManager.h"
#include "../Device.h"
#include "Shader.h"

AR3D_USING

DEFINITION_SINGLE(CShaderManager)

CShaderManager::CShaderManager() :
	m_iOffsetSize(0)
{
}

CShaderManager::~CShaderManager()
{
	unordered_map<string, PCONSTANTBUFFER>::iterator	iter;
	unordered_map<string, PCONSTANTBUFFER>::iterator	iterEnd = m_mapCBuffer.end();

	for (iter = m_mapCBuffer.begin(); iter != iterEnd; ++iter)
	{
		SAFE_RELEASE(iter->second->pBuffer);
		SAFE_DELETE(iter->second);
	}

	m_mapCBuffer.clear();

	Safe_Release_Map(m_mapInputLayout);
	Safe_Release_Map(m_mapShader);
}

bool CShaderManager::Init()
{
	char*	pEntry[ST_END] = { "StandardColorVS", "StandardColorPS", NULL };

	CShader*	pShader = LoadShader(STANDARD_COLOR_SHADER, L"Standard.fx", pEntry);

	SAFE_RELEASE(pShader);

	pEntry[ST_VERTEX] = "ColliderVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "ColliderPS";

	pShader = LoadShader(COLLIDER_COLOR_SHADER, L"Collider.fx", pEntry);

	SAFE_RELEASE(pShader);

	pEntry[ST_VERTEX] = "StandardTexNormalVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "StandardTexNormalPS";

	pShader = LoadShader(STANDARD_TEXNORMAL_SHADER, L"Standard.fx", pEntry);

	SAFE_RELEASE(pShader);


	pEntry[ST_VERTEX] = "StandardTexNormalSkinningVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "StandardTexNormalPS";

	pShader = LoadShader(STANDARD_TEXNORMAL_ANIM_SHADER, L"Standard.fx", pEntry);

	SAFE_RELEASE(pShader);

	pEntry[ST_VERTEX] = "StandardBumpVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "StandardBumpPS";

	pShader = LoadShader(STANDARD_BUMP_SHADER, L"Standard.fx", pEntry);

	SAFE_RELEASE(pShader);

	pEntry[ST_VERTEX] = "StandardAnimBumpVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "StandardBumpPS";

	pShader = LoadShader(STANDARD_ANIM_BUMP_SHADER, L"Standard.fx", pEntry);

	SAFE_RELEASE(pShader);

	pEntry[ST_VERTEX] = "SkyVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "SkyPS";

	pShader = LoadShader(SKY_SHADER, L"Sky.fx", pEntry);

	SAFE_RELEASE(pShader);

	pEntry[ST_VERTEX] = "EffectVS";
	pEntry[ST_GEOMETRY] = "EffectGS";
	pEntry[ST_PIXEL] = "EffectPS";

	pShader = LoadShader(EFFECT_SHADER, L"Effect.fx", pEntry);

	SAFE_RELEASE(pShader);

	// Particle StreamOut Shader
	pEntry[ST_VERTEX] = "ParticleStreamOutVS";
	pEntry[ST_GEOMETRY] = "ParticleStreamOutGS";
	pEntry[ST_PIXEL] = NULL;

	AddStreamDecl(0, "POSITION", 0, 0, 3, 0);
	AddStreamDecl(0, "VELOCITY", 0, 0, 3, 0);
	AddStreamDecl(0, "SIZE", 0, 0, 2, 0);
	AddStreamDecl(0, "LIFETIME", 0, 0, 1, 0);
	AddStreamDecl(0, "CREATETIME", 0, 0, 1, 0);
	AddStreamDecl(0, "TYPE", 0, 0, 1, 0);
	AddStreamDecl(0, "LIGHTRANGE", 0, 0, 1, 0);

	pShader = LoadShader(PARTICLE_STREAMOUT_SHADER, L"Particle.fx", pEntry, true);

	SAFE_RELEASE(pShader);

	//// Particle Shader
	pEntry[ST_VERTEX] = "ParticleVS";
	pEntry[ST_GEOMETRY] = "ParticleGS";
	pEntry[ST_PIXEL] = "ParticlePS";

	pShader = LoadShader(PARTICLE_SHADER, L"Particle.fx", pEntry);
	SAFE_RELEASE(pShader);

	// Particle Ligth Shader
	pEntry[ST_VERTEX] = "LampLightVS";
	pEntry[ST_GEOMETRY] = "LampLightGS";
	pEntry[ST_PIXEL] = "LampLightAccPS";

	pShader = LoadShader(LAMPLIGHT_SHADER, L"LampLightAcc.fx", pEntry);

	SAFE_RELEASE(pShader);

	// Lamp Ligth Shader
	pEntry[ST_VERTEX] = "ParticleVS";
	pEntry[ST_GEOMETRY] = "ParticleLightGS";
	pEntry[ST_PIXEL] = "ParticleLightAccPS";

	pShader = LoadShader(PARTICLE_LIGHT_SHADER, L"Particle.fx", pEntry);

	SAFE_RELEASE(pShader);

	pEntry[ST_VERTEX] = "UIVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "UIPS";

	pShader = LoadShader(UI_SHADER, L"UI.fx", pEntry);

	SAFE_RELEASE(pShader);

	// Terrain Shader
	pEntry[ST_VERTEX] = "TerrainVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "TerrainPS";

	pShader = LoadShader(TERRAIN_SHADER, L"Terrain.fx", pEntry);

	SAFE_RELEASE(pShader);


	// LightAcc Shader
	pEntry[ST_VERTEX] = "LightAccVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "LightAccPS";

	pShader = LoadShader(LIGHT_ACC_SHADER, L"LightAcc.fx", pEntry);

	SAFE_RELEASE(pShader);

	// LightBlend Shader
	pEntry[ST_VERTEX] = "LightBlendVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "LightBlendPS";

	pShader = LoadShader(LIGHT_BLEND_SHADER, L"LightBlend.fx", pEntry);

	SAFE_RELEASE(pShader);

	//water Shader
	pEntry[ST_VERTEX] = "WaterVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "WaterPS";

	pShader = LoadShader(WATER_SHADER, L"water.fx", pEntry);

	SAFE_RELEASE(pShader);


	// LightBlendTargetOutput Shader
	pEntry[ST_VERTEX] = "LightBlendVS";
	pEntry[ST_GEOMETRY] = NULL;
	pEntry[ST_PIXEL] = "LightBlendTargetOutputPS";

	pShader = LoadShader(LIGHT_BLEND_OUTPUT_SHADER, L"LightBlend.fx", pEntry);

	SAFE_RELEASE(pShader);


	AddElement("POSITION", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("COLOR", 0, 16, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);

	CreateInputLayout("ColorInputLayout", STANDARD_COLOR_SHADER);

	AddElement("POSITION", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("TEXCOORD", 0, 8, DXGI_FORMAT_R32G32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);

	CreateInputLayout("PosInputLayout", SKY_SHADER);

	AddElement("POSITION", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("NORMAL", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("TEXCOORD", 0, 8, DXGI_FORMAT_R32G32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);

	CreateInputLayout("TexNormalInputLayout", STANDARD_TEXNORMAL_SHADER);


	AddElement("POSITION", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("NORMAL", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("TEXCOORD", 0, 8, DXGI_FORMAT_R32G32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("BLENDWEIGHTS", 0, 16, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("BLENDINDICES", 0, 16, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);

	CreateInputLayout("TexNormalAnimInputLayout", STANDARD_TEXNORMAL_ANIM_SHADER);

	AddElement("POSITION", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("TEXCOORD", 0, 8, DXGI_FORMAT_R32G32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);

	CreateInputLayout("TexInputLayout", UI_SHADER);



	AddElement("POSITION", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("NORMAL", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("TEXCOORD", 0, 8, DXGI_FORMAT_R32G32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("TANGENT", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("BINORMAL", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);

	CreateInputLayout("BumpInputLayout", STANDARD_BUMP_SHADER);

	AddElement("POSITION", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("NORMAL", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("TEXCOORD", 0, 8, DXGI_FORMAT_R32G32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("TANGENT", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("BINORMAL", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("BLENDWEIGHTS", 0, 16, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("BLENDINDICES", 0, 16, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);

	CreateInputLayout("AnimBumpInputLayout", STANDARD_ANIM_BUMP_SHADER);

	AddElement("POSITION", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("VELOCITY", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("SIZE", 0, 8, DXGI_FORMAT_R32G32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("LIFETIME", 0, 4, DXGI_FORMAT_R32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("CREATETIME", 0, 4, DXGI_FORMAT_R32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("TYPE", 0, 4, DXGI_FORMAT_R32_UINT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("LIGHTRANGE", 0, 4, DXGI_FORMAT_R32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);

	CreateInputLayout("Particle", PARTICLE_STREAMOUT_SHADER);

	
	AddElement("POSITION", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("LIGHTRANGE", 0, 4, DXGI_FORMAT_R32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);

	CreateInputLayout("LampInputLayout", LAMPLIGHT_SHADER);


	//Water
	AddElement("POSITION", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("NORMAL", 0, 12, DXGI_FORMAT_R32G32B32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);
	AddElement("TEXCOORD", 0, 8, DXGI_FORMAT_R32G32_FLOAT, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0);

	CreateInputLayout("WaterInputLayout", WATER_SHADER);

	// ConstantBuffer
	CreateConstantBuffer("Transform", sizeof(TRANSFORMCBUFFER), 0);

	CreateConstantBuffer("Material", sizeof(MATERIALINFO), 1);

	CreateConstantBuffer("Light", sizeof(LIGHTCBUFFER), 2);

	// Collider Color Buffer
	CreateConstantBuffer("ColliderColor", sizeof(COLLIDERCOLORCBUFFER), 10);

	// Effect Constant Buffer
	CreateConstantBuffer("BillBoard", sizeof(EFFECTCBUFFER), 11);

	CreateConstantBuffer("Animation2D", sizeof(ANIMATION2DCBUFFER), 12);

	// Terrain Buffer
	CreateConstantBuffer("TerrainInfo", sizeof(TERRAINCBUFFER), 10);

	// Terrain Buffer
	CreateConstantBuffer("PickTerrainInfo", sizeof(PICKTERRAINCBUFFER), 11);

	// Water Buffer
	CreateConstantBuffer("WaterInfo", sizeof(WATERINFO), 10);

	// Particle Buffer
	CreateConstantBuffer("ParticleCBuffer", sizeof(PARTICLECBUFFER), 13);

	// LampLight Buffer
	CreateConstantBuffer("LampLightCBuffer", sizeof(LAMPLIGHTCBUFFER), 13);
	
	// Rendering Buffer
	CreateConstantBuffer("Rendering", sizeof(RENDERINGCBUFFER), 3);


	return true;
}

CShader * CShaderManager::LoadShader(const string & strKey, TCHAR * pFileName,
	char* pEntry[ST_END], bool bStreamOut, const string & strPathKey)
{
	CShader*	pShader = FindShader(strKey);

	if (pShader)
		return pShader;

	pShader = new CShader;

	if (bStreamOut)
		pShader->SetStreamDecl(&m_vecStreamDecl[0], m_vecStreamDecl.size());

	if (!pShader->LoadShader(strKey, pFileName, pEntry, bStreamOut, strPathKey))
	{
		if (bStreamOut)
			m_vecStreamDecl.clear();
		SAFE_RELEASE(pShader);
		return NULL;
	}

	if (bStreamOut)
		m_vecStreamDecl.clear();
	pShader->AddRef();

	m_mapShader.insert(make_pair(strKey, pShader));

	return pShader;
}

CShader * CShaderManager::FindShader(const string & strKey)
{
	unordered_map<string, class CShader*>::iterator	iter = m_mapShader.find(strKey);

	if (iter == m_mapShader.end())
		return NULL;

	iter->second->AddRef();

	return iter->second;
}

void CShaderManager::AddElement(char * pSemanticName, int iSemanticIndex, int iSize,
	DXGI_FORMAT eFmt, int iInputSlot, D3D11_INPUT_CLASSIFICATION eSlotClass, int iInstanceStep)
{
	D3D11_INPUT_ELEMENT_DESC	tDesc = {};

	tDesc.SemanticName = pSemanticName;
	tDesc.SemanticIndex = iSemanticIndex;
	tDesc.AlignedByteOffset = m_iOffsetSize;
	tDesc.Format = eFmt;
	tDesc.InputSlot = iInputSlot;
	tDesc.InputSlotClass = eSlotClass;
	tDesc.InstanceDataStepRate = iInstanceStep;

	m_iOffsetSize += iSize;

	m_vecElement.push_back(tDesc);
}

bool CShaderManager::CreateInputLayout(const string & strKey, const string& strShaderKey)
{
	ID3D11InputLayout*	pLayout = FindInputLayout(strKey);

	if (pLayout)
		return false;

	CShader*	pShader = FindShader(strShaderKey);

	if (!pShader)
		return false;

	if (FAILED(DEVICE->CreateInputLayout(&m_vecElement[0], m_vecElement.size(),
		pShader->GetVSCode(), pShader->GetVSCodeSize(), &pLayout)))
	{
		m_vecElement.clear();
		return false;
	}

	m_mapInputLayout.insert(make_pair(strKey, pLayout));

	m_vecElement.clear();
	m_iOffsetSize = 0;

	SAFE_RELEASE(pShader);

	return true;
}

ID3D11InputLayout * CShaderManager::FindInputLayout(const string & strKey)
{
	unordered_map<string, ID3D11InputLayout*>::iterator	iter = m_mapInputLayout.find(strKey);

	if (iter == m_mapInputLayout.end())
		return NULL;

	return iter->second;
}

bool CShaderManager::CreateConstantBuffer(const string & strKey, int iSize, int iRegister)
{
	PCONSTANTBUFFER	pCBuffer = FindConstantBuffer(strKey);

	if (pCBuffer)
		return false;

	pCBuffer = new CONSTANTBUFFER;

	pCBuffer->iSize = iSize;
	pCBuffer->iRegister = iRegister;

	D3D11_BUFFER_DESC	tDesc = {};

	tDesc.ByteWidth = iSize;
	tDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	tDesc.Usage = D3D11_USAGE_DYNAMIC;
	tDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	if (FAILED(DEVICE->CreateBuffer(&tDesc, NULL, &pCBuffer->pBuffer)))
	{
		SAFE_DELETE(pCBuffer);
		return false;
	}

	m_mapCBuffer.insert(make_pair(strKey, pCBuffer));

	return true;
}

PCONSTANTBUFFER CShaderManager::FindConstantBuffer(const string & strKey)
{
	unordered_map<string, PCONSTANTBUFFER>::iterator iter = m_mapCBuffer.find(strKey);

	if (iter == m_mapCBuffer.end())
		return NULL;

	return iter->second;
}

void CShaderManager::UpdateConstantBuffer(const string & strKey, void * pData, int iCut)
{
	PCONSTANTBUFFER	pCBuffer = FindConstantBuffer(strKey);

	if (!pCBuffer)
		return;

	D3D11_MAPPED_SUBRESOURCE	tMap = {};

	CONTEXT->Map(pCBuffer->pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &tMap);

	memcpy(tMap.pData, pData, pCBuffer->iSize);

	CONTEXT->Unmap(pCBuffer->pBuffer, 0);

	if (iCut & CUT_VERTEX)
		CONTEXT->VSSetConstantBuffers(pCBuffer->iRegister, 1, &pCBuffer->pBuffer);

	if (iCut & CUT_PIXEL)
		CONTEXT->PSSetConstantBuffers(pCBuffer->iRegister, 1, &pCBuffer->pBuffer);

	if (iCut & CUT_GEOMETRY)
		CONTEXT->GSSetConstantBuffers(pCBuffer->iRegister, 1, &pCBuffer->pBuffer);
}

void CShaderManager::AddStreamDecl(UINT iStream, const char * pSemanticName, UINT iSemanticIdx, BYTE byStartCom, BYTE byComCount, BYTE byOutSlot)
{
	D3D11_SO_DECLARATION_ENTRY	tDecl = {};

	tDecl.Stream = iStream;
	tDecl.SemanticName = pSemanticName;
	tDecl.SemanticIndex = iSemanticIdx;
	tDecl.StartComponent = byStartCom;
	tDecl.ComponentCount = byComCount;
	tDecl.OutputSlot = byOutSlot;

	m_vecStreamDecl.push_back(tDecl);
}



void CShaderManager::SetInputLayout(const string & strKey)
{
	ID3D11InputLayout*	pInput = FindInputLayout(strKey);

	if (!pInput)
		return;

	CONTEXT->IASetInputLayout(pInput);
}
