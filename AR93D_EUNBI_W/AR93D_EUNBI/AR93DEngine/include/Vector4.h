
#pragma once

#include "Macro.h"
#include "DxMath.h"

AR3D_BEGIN

typedef struct DLL _tagDxVector4 :
	public XMFLOAT4
{
	_tagDxVector4();
	_tagDxVector4(float _x, float _y, float _z, float _w);
	_tagDxVector4(float _f[4]);
	_tagDxVector4(int _i[4]);
	_tagDxVector4(const _tagDxVector4& vec);

	void operator =(const _tagDxVector4& vec);
	void operator =(const struct _tagDxVector3& vec);
	void operator =(float _f[4]);
	void operator =(int _i[4]);
	void operator =(const XMVECTOR& v);

	// +
	_tagDxVector4 operator +(const _tagDxVector4& _v);
	_tagDxVector4 operator +(float _f[4]);
	_tagDxVector4 operator +(float fValue);
	_tagDxVector4 operator +(int iValue);
	_tagDxVector4 operator +(int i[4]);

	// +=
	void operator +=(const _tagDxVector4& _v);
	void operator +=(float _f[4]);
	void operator +=(float fValue);
	void operator +=(int iValue);
	void operator +=(int i[4]);

	// -
	_tagDxVector4 operator -(const _tagDxVector4& _v);
	_tagDxVector4 operator -(float _f[4]);
	_tagDxVector4 operator -(float fValue);
	_tagDxVector4 operator -(int iValue);
	_tagDxVector4 operator -(int i[4]);

	// -=
	void operator -=(const _tagDxVector4& _v);
	void operator -=(float _f[4]);
	void operator -=(float fValue);
	void operator -=(int iValue);
	void operator -=(int i[4]);

	// *
	_tagDxVector4 operator *(const _tagDxVector4& _v);
	_tagDxVector4 operator *(float _f[4]);
	_tagDxVector4 operator *(float fValue);
	_tagDxVector4 operator *(int iValue);
	_tagDxVector4 operator *(int i[4]);

	// *=
	void operator *=(const _tagDxVector4& _v);
	void operator *=(float _f[4]);
	void operator *=(float fValue);
	void operator *=(int iValue);
	void operator *=(int i[4]);

	// /
	_tagDxVector4 operator /(const _tagDxVector4& _v);
	_tagDxVector4 operator /(float _f[4]);
	_tagDxVector4 operator /(float fValue);
	_tagDxVector4 operator /(int iValue);
	_tagDxVector4 operator /(int i[4]);

	// /=
	void operator /=(const _tagDxVector4& _v);
	void operator /=(float _f[4]);
	void operator /=(float fValue);
	void operator /=(int iValue);
	void operator /=(int i[4]);

	// ++
	void operator ++();
	void operator --();

	// ==
	bool operator ==(const _tagDxVector4& _v);
	bool operator ==(float _f[4]);
	bool operator ==(int i[4]);

	// !=
	bool operator !=(const _tagDxVector4& _v);
	bool operator !=(float _f[4]);
	bool operator !=(int i[4]);

	float operator [](int idx);
	XMVECTOR Convert()	const;
}DxVector4, *PDxVector4;


typedef union DLL _tagVector4
{
	XMVECTOR	v;
	struct
	{
		float	x, y, z, w;
	};
	struct
	{
		float	r, g, b, a;
	};
	struct
	{
		float	f[4];
	};

	_tagVector4();
	_tagVector4(float _x, float _y, float _z, float _w);
	_tagVector4(float _f[4]);
	_tagVector4(const _tagDxVector4& vec);
	_tagVector4(const _tagVector4& vec);
	_tagVector4(const XMVECTOR& vec);

	void* operator new(size_t size)
	{
		return _aligned_malloc(size, 16);
	}

	void operator delete(void* p)
	{
		_aligned_free(p);
	}

	void operator =(const _tagDxVector4& vec);
	void operator =(const _tagVector4& vec);
	void operator =(const XMVECTOR& vec);
	void operator =(float _f[4]);

	// +
	_tagVector4 operator +(const _tagDxVector4& _v);
	_tagVector4 operator +(const _tagVector4& _v);
	_tagVector4 operator +(float _f[4]);
	_tagVector4 operator +(float fValue);
	_tagVector4 operator +(int iValue);
	_tagVector4 operator +(int i[4]);

	// +=
	void operator +=(const _tagDxVector4& _v);
	void operator +=(const _tagVector4& _v);
	void operator +=(float _f[4]);
	void operator +=(float fValue);
	void operator +=(int iValue);
	void operator +=(int i[4]);

	// -
	_tagVector4 operator -(const _tagDxVector4& _v);
	_tagVector4 operator -(const _tagVector4& _v);
	_tagVector4 operator -(float _f[4]);
	_tagVector4 operator -(float fValue);
	_tagVector4 operator -(int iValue);
	_tagVector4 operator -(int i[4]);

	// -=
	void operator -=(const _tagDxVector4& _v);
	void operator -=(const _tagVector4& _v);
	void operator -=(float _f[4]);
	void operator -=(float fValue);
	void operator -=(int iValue);
	void operator -=(int i[4]);

	// *
	_tagVector4 operator *(const _tagDxVector4& _v);
	_tagVector4 operator *(const _tagVector4& _v);
	_tagVector4 operator *(float _f[4]);
	_tagVector4 operator *(float fValue);
	_tagVector4 operator *(int iValue);
	_tagVector4 operator *(int i[4]);

	// *=
	void operator *=(const _tagDxVector4& _v);
	void operator *=(const _tagVector4& _v);
	void operator *=(float _f[4]);
	void operator *=(float fValue);
	void operator *=(int iValue);
	void operator *=(int i[4]);

	// /
	_tagVector4 operator /(const _tagDxVector4& _v);
	_tagVector4 operator /(const _tagVector4& _v);
	_tagVector4 operator /(float _f[4]);
	_tagVector4 operator /(float fValue);
	_tagVector4 operator /(int iValue);
	_tagVector4 operator /(int i[4]);

	// /=
	void operator /=(const _tagDxVector4& _v);
	void operator /=(const _tagVector4& _v);
	void operator /=(float _f[4]);
	void operator /=(float fValue);
	void operator /=(int iValue);
	void operator /=(int i[4]);

	// ++
	void operator ++();
	void operator --();

	// ==
	bool operator ==(const _tagDxVector4& _v);
	bool operator ==(const _tagVector4& _v);
	bool operator ==(float _f[4]);
	bool operator ==(int i[4]);

	// !=
	bool operator !=(const _tagDxVector4& _v);
	bool operator !=(const _tagVector4& _v);
	bool operator !=(float _f[4]);
	bool operator !=(int i[4]);

	float operator [](int idx);
	_tagVector4 Transform(const union _tagDxMatrix& mat);
}Vector4, *PVector4;

AR3D_END

