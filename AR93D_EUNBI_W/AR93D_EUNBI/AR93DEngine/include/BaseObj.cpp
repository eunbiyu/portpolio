#include "BaseObj.h"

AR3D_USING

CBaseObj::CBaseObj() :
	m_iRefCount(1),
	m_pTypeID(NULL),
	m_bEnable(true),
	m_bAlive(true)
{
}

CBaseObj::~CBaseObj()
{
}

int CBaseObj::AddRef()
{
	++m_iRefCount;
	return m_iRefCount;
}

int CBaseObj::Release()
{
	--m_iRefCount;

	if (m_iRefCount == 0)
	{
		delete	this;
		return 0;
	}
	return m_iRefCount;
}

int CBaseObj::Remove()
{
	m_iRefCount = 0;
	delete	this;
	return 0;
}

void CBaseObj::SetTag(const string & strTag)
{
	m_strTag = strTag;
}

void CBaseObj::SetTypeName(const string & strTypeName)
{
	m_strTypeName = strTypeName;
}

void CBaseObj::SetEnable(bool bEnable)
{
	m_bEnable = bEnable;
}

void CBaseObj::Death()
{
	m_bAlive = false;
}

const type_info* CBaseObj::GetTypeID() const
{
	return m_pTypeID;
}

string CBaseObj::GetTag() const
{
	return m_strTag.c_str();
}

string CBaseObj::GetTypeName() const
{
	return m_strTypeName.c_str();
}

bool CBaseObj::GetEnable() const
{
	return m_bEnable;
}

bool CBaseObj::GetAlive() const
{
	return m_bAlive;
}
