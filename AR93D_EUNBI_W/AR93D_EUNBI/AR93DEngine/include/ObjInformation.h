#pragma once

#include "Engine.h"
#include "Component\Transform.h"

AR3D_BEGIN

class DLL CObjInformation
{
public:
	//unordered_map<string, class CTransform*> m_mapObjInfo;
	bool m_bChangeInfo;
	list<OBJINFO>	m_ObjInfoList;
	list<class CGameObject*> m_ObjList;
	DxVector3 m_vector;

	bool SaveObjInfo(const list<class CGameObject*> ObjList, const char * pFileName,
		const string & strPathKey = OBJECT_INFO_PATH);
	list<OBJINFO> LoadObjInfo(const char * pFileName, const string & strPathKey = OBJECT_INFO_PATH);


	DECLARE_SINGLE(CObjInformation)
};
AR3D_END