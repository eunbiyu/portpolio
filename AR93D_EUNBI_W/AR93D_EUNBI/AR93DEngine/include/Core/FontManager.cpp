#include "FontManager.h"
#include "../Device.h"

AR3D_USING

DEFINITION_SINGLE(CFontManager)

CFontManager::CFontManager() :
	m_pWriteFactory(NULL)
{
}

CFontManager::~CFontManager()
{
	Safe_Release_Map(m_mapBrush);
	Safe_Release_Map(m_mapFont);
	SAFE_RELEASE(m_pWriteFactory);
}

bool CFontManager::Init()
{
	// IDWriteFactory는 기기 독립적인 리소스이다. 
	if (FAILED(DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(m_pWriteFactory),
		(IUnknown**)&m_pWriteFactory)))
		return false;

	CreateFontFormat("궁서체50", L"궁서체", DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_ULTRA_EXPANDED,
		50.f, L"ko");
	CreateFontFormat("고딕체", L"고딕체", DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_ULTRA_EXPANDED,
		30.f, L"ko");

	CreateBrush("White", 1.f, 1.f, 1.f, 1.f);
	CreateBrush("Black", 0.f, 0.f, 0.f, 1.f);
	CreateBrush("Yellow", 1.f, 1.f, 0.f, 1.f);
	CreateBrush("Blue", 0.f, 0.f, 1.f, 0.5f);

	return true;
}

IDWriteTextFormat * CFontManager::CreateFontFormat(const string & strKey, const wchar_t * pFontName, int iWeight, int iStyle, int iStretch, float fSize, const wchar_t * pLocalName)
{
	IDWriteTextFormat*	pFont = FindFont(strKey);

	if (pFont)
		return pFont;

	// Text Format을 설정한다.
	// 1번인자 : 폰트 이름을 입력한다. 2번인자 : 특정 폰트는 컬렉션을 가지고 있다. 예 Arial 은 Arial Black 이런식
	// 3번인자 : 폰트 굵기 4번인자 : 기울기
	// 5번인자 : 자간 6번인자 : 폰트 크기
	// 7번인자 : 언어 지역 이름을 설정한다. 한국은 ko - KR 미국은 en - us 8번인자 : 텍스트 인터페이스
	if (FAILED(m_pWriteFactory->CreateTextFormat(pFontName, NULL,
		(DWRITE_FONT_WEIGHT)iWeight,
		(DWRITE_FONT_STYLE)iStyle,
		(DWRITE_FONT_STRETCH)iStretch,
		fSize, pLocalName, &pFont)))
		return NULL;

	m_mapFont.insert(make_pair(strKey, pFont));

	return pFont;
}

IDWriteTextFormat * CFontManager::FindFont(const string & strKey)
{
	unordered_map<string, IDWriteTextFormat*>::iterator	iter = m_mapFont.find(strKey);

	if (iter == m_mapFont.end())
		return NULL;

	return iter->second;
}

ID2D1SolidColorBrush * CFontManager::CreateBrush(const string & strKey, float r, float g, float b, float a)
{
	ID2D1SolidColorBrush*	pBrush = FindBrush(strKey);

	if (pBrush)
		return false;

	if (FAILED(D2DTARGET->CreateSolidColorBrush(D2D1::ColorF(r, g, b, a),
		&pBrush)))
		return NULL;

	m_mapBrush.insert(make_pair(strKey, pBrush));

	return pBrush;
}

ID2D1SolidColorBrush * CFontManager::FindBrush(const string & strKey)
{
	unordered_map<string, ID2D1SolidColorBrush*>::iterator	iter = m_mapBrush.find(strKey);

	if (iter == m_mapBrush.end())
		return NULL;

	return iter->second;
}
