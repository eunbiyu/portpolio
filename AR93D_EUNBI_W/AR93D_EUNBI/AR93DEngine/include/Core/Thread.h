#pragma once

#include "../Engine.h"
#include "ThreadQueue.h"

AR3D_BEGIN

class DLL CThread
{
protected:
	friend class CThreadManager;
protected:
	CThread();
	virtual ~CThread() = 0;

protected:
	HANDLE	 m_hThread;
	HANDLE	 m_hStart;
	bool	 m_bLoop;

public:
	bool Init();
	void WaitStart();
	void Start();

public:
	virtual void Run() = 0;

protected:
	static UINT __stdcall ThreadFunc(void* pArg);

};

AR3D_END