#pragma once
#include "../BaseObj.h"

AR3D_BEGIN

class DLL CTimer
	: public CBaseObj
{
private:
	friend class CTimerManager; // 여기서만 생성! 

private:
	CTimer();
	~CTimer();

private:
	//GetTickCount는 1/100초밖에 세지못함.->최대 100프레임만관리
	//쿼리퍼포먼스카운트/프리퀀시는 CPU클럭수준까지 가능하므로 더 정밀하다.
	//얘네는 라지인티저로 8byte longlong타입. 
	LARGE_INTEGER m_tSecond;
	LARGE_INTEGER m_tCount;

private:
	float		m_fDeltaTime;
	float		m_fFPS;
	float		m_fFPSTime;
	int			m_iFrame;
	int			m_iFrameMax;

public:
	float GetDeltaTime()	const;
	float GetFPS()	const;

public:
	bool Init();
	void Update();
};

AR3D_END