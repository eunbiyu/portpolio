#include "Input.h"
#include "../Core.h"
#include "../GameObject/GameObject.h"
#include "../Component/Transform.h"
#include "../Component/ColliderPoint.h"
#include "../Component/ColliderRay.h"
#include "CollisionManager.h"
#include "../Scene/Scene.h"
#include "../Scene/SceneManager.h"
#include "../Component/Camera.h"
#include "../Device.h"
#include "../Core/GameTotalManager.h"
#include "../Component/Terrain.h"
#include "../Scene/Scene.h"
#include "../Core/Input.h"
#include "../ObjInformation.h"
#include "../GameObject/GameObject.h"
#include "../Scene/Scene.h"
#include "../Scene/Layer.h"
#include "../Component/Renderer.h"
#include "../Component/Transform.h"
#include "../Scene/SceneManager.h"
#include "../Component/Terrain.h"
#include "../Rendering/RenderManager.h"
#include "../Engine.h"
#include "../Component/Camera.h"

AR3D_USING

DEFINITION_SINGLE(CInput)

CInput::CInput() :
	m_pCreateKey(NULL),
	m_pMouseTr(NULL),
	m_pRay(NULL),
	m_pMousePoint(NULL),
	m_OnOffCollider(false)
{
	m_tRay.bIntersect = false;
}

CInput::~CInput()
{
	SAFE_RELEASE(m_pMousePoint);
	SAFE_RELEASE(m_pRay);
	SAFE_RELEASE(m_pMouseObj);
	SAFE_RELEASE(m_pMouseTr);
	Safe_Delete_Map(m_mapKey);
}

void CInput::SetWheel(short sWheel)
{
	m_sWheel = sWheel / 120;
}

short CInput::GetWheelDir() const
{
	return m_sWheel;
}

void CInput::ClearWheel()
{
	m_sWheel = 0;
}

CGameObject * CInput::GetMouseObj()
{
	m_pMouseObj->AddRef();
	return m_pMouseObj;
}

CColliderRay * CInput::GetMouseRay()
{
	m_pRay->AddRef();
	return m_pRay;
}

CColliderPoint * CInput::GetMousePoint()
{
	m_pMousePoint->AddRef();
	return m_pMousePoint;
}

void CInput::SetPlayerMovePos(DxVector3 pos)
{
	m_vPlayerMovePos = pos;
	GET_SINGLE(CGameTotalManager)->SetPickedPosition(m_vPlayerMovePos);
}

DxVector3 CInput::GetPlayerMovePos()
{
	return m_vPlayerMovePos;
}

bool CInput::Init()
{
	CreateKey(VK_SPACE, "Fire");
	CreateKey("MoveFront", 'W');
	CreateKey('S', "MoveBack");
	CreateKey("Skill1", VK_CONTROL, '1');
	CreateKey("MouseRButton", VK_RBUTTON);
	CreateKey("MouseLButton", VK_LBUTTON);

	m_sWheel = 0;

	m_pMouseObj = CGameObject::Create("MouseObj", false);

	m_pMouseTr = m_pMouseObj->GetTransform();

	m_pMousePoint = m_pMouseObj->AddComponent<CColliderPoint>("MousePoint");

	m_pRay = m_pMouseObj->AddComponent<CColliderRay>("MouseRay");

	// 마우스 위치를 얻어온다.
	memset(&m_tMousePos, 0, sizeof(POINT));
	memset(&m_tMouseMove, 0, sizeof(POINT));

	ComputeMouse(true);
	m_TerrainInfo.iSizeH = 0.f;

	return true;
}

void CInput::Input(float fTime)
{
	/*if (KEYPRESS("MouseRButton") || KEYPUSH("MouseLButton"))
	{
		if(m_MouseClickOn)
			m_MouseClickOn = false;
		else
			m_MouseClickOn = true;
	}*/
}

void CInput::Update(float fTime)
{
	
	ComputeMouse();
	

	unordered_map<string, PKEYINFO>::iterator	iter;
	unordered_map<string, PKEYINFO>::iterator	iterEnd = m_mapKey.end();

	for (iter = m_mapKey.begin(); iter != iterEnd; ++iter)
	{
		int		iPushCount = 0;
		for (size_t i = 0; i < iter->second->vecKey.size(); ++i)
		{
			if (GetAsyncKeyState(iter->second->vecKey[i]) & 0x8000)
				++iPushCount;
		}

		if (iPushCount == iter->second->vecKey.size())
		{
			if (!iter->second->bPress && !iter->second->bPush)
				iter->second->bPress = true;

			else
			{
				iter->second->bPush = true;
				iter->second->bPress = false;
			}
		}

		else
		{
			if (iter->second->bPress || iter->second->bPush)
			{
				iter->second->bPush = false;
				iter->second->bPress = false;
				iter->second->bUp = true;
			}

			else if (iter->second->bUp)
				iter->second->bUp = false;
		}
	}

	m_pMouseObj->Update(fTime);
	m_pMouseObj->LateUpdate(fTime);
	//GET_SINGLE(CCollisionManager)->AddObject(m_pMouseObj);
	ComputeRay();
	if (m_TerrainInfo.iSizeH > 0)
	{
		DDTPicking();
	}

}

void CInput::ComputeRay()
{
	CScene*	pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

	CCamera*	pCamera = pScene->GetMainCamera();

	MATRIX	tView = pCamera->GetViewMatrix();
	MATRIX	tProj = pCamera->GetProjMatrix();

	// 마우스 위치를 이용해서 뷰공간의 방향을 구해준다.
	DxVector3	vDir;

	vDir.x = (2.f * m_tMousePos.x / _RESOLUTION.iWidth - 1) / tProj._11;
	vDir.y = (-2.f * m_tMousePos.y / _RESOLUTION.iHeight + 1) / tProj._22;
	vDir.z = 1.f;

	vDir = vDir.Normalize();

	DxVector3	vPos = Vec3Zero;

	// 월드공간으로 변환한다.
	tView = tView.Inverse();

	vDir = vDir.TransformNormal(tView);
	vDir = vDir.Normalize();
	vPos = vPos.TransformCoord(tView);

	m_tRay.vDir = vDir;
	m_tRay.vPos = vPos;

	m_pRay->SetRay(vPos, vDir);

	SAFE_RELEASE(pCamera);
}

bool CInput::KeyPress(const string & strKey)
{
	PKEYINFO	pInfo = FindKey(strKey);

	if (!pInfo)
		return false;

	return pInfo->bPress;
}

bool CInput::KeyPush(const string & strKey)
{
	PKEYINFO	pInfo = FindKey(strKey);

	if (!pInfo)
		return false;

	return pInfo->bPush;
}

bool CInput::KeyUp(const string & strKey)
{
	PKEYINFO	pInfo = FindKey(strKey);

	if (!pInfo)
		return false;

	return pInfo->bUp;
}

POINT CInput::GetMousePos() const
{
	return m_tMousePos;
}


POINT CInput::GetMouseMove() const
{
	return m_tMouseMove;
}

PKEYINFO CInput::FindKey(const string & strKey)
{
	unordered_map<string, PKEYINFO>::iterator	iter = m_mapKey.find(strKey);

	if (iter == m_mapKey.end())
		return NULL;

	return iter->second;
}

void CInput::ComputeMouse(bool bStart)
{
	// 윈도우 클라이언트의 크기를 구한다.
	RECT	rcClient = {};
	GetClientRect(WINDOWHANDLE, &rcClient);

	float	x = rcClient.right - rcClient.left;
	float	y = rcClient.bottom - rcClient.top;

	RESOLUTION	tRS = _RESOLUTION;

	x = tRS.iWidth / x;
	y = tRS.iHeight / y;

	POINT	tMousePos;
	GetCursorPos(&tMousePos);
	ScreenToClient(WINDOWHANDLE, &tMousePos);

	tMousePos.x = tMousePos.x * x;
	tMousePos.y = tMousePos.y * y;

	if (!bStart)
	{
		m_tMouseMove.x = tMousePos.x - m_tMousePos.x;
		m_tMouseMove.y = tMousePos.y - m_tMousePos.y;
	}

	m_tMousePos = tMousePos;

	m_pMouseTr->SetWorldPos(tMousePos.x, tMousePos.y, 0.f);
}


//지형 DDT 픽킹 알고리즘 

void CInput::SetTerrainInfo(TERRAINCOLLINFO TerrainInfo)
{
	m_TerrainInfo = TerrainInfo;
}

DxVector3 CInput::GetTerrainPickingPos()
{
	return m_vPickPos;
}

void CInput::DDTPicking()
{
	m_bPick = false;

	//Layer의 카메라 얻어오기
	CCamera* pCamera = GET_SINGLE(CSceneManager)->GetCurrentScene()->GetMainCamera();

	if (NULL == pCamera)
		return;

	//
	DxVector3 vDir = m_tRay.vDir;

	//Start Vector
	DxVector3 vCameraPos = m_tRay.vPos;

	//방향 노말라이즈
	vDir = XMVector3Normalize(vDir.Convert());

	//증가할 방향
	float DX = vDir.x;
	float DZ = vDir.z;

	//시작 기준점
	DxVector3 StartPos = vCameraPos;
	StartPos.y = 0.0f;

	DxVector3 vLength = { 0.f,0.f,0.f };

	//시작점을 계산해 준다
	if (false == IsValidPoint(StartPos))
	{
		while (true)
		{
			StartPos.x += DX*0.05f;
			StartPos.z += DZ*0.05f;

			//시작점을 찾으면 break;
			if (true == IsValidPoint(StartPos))
				break;

			vLength = StartPos - vCameraPos;
			vLength = XMVector3Length(vLength.Convert());

			//1000 을 Max 찾는 거리 변수화 시켜야함
			//이상한 곳을 클릭
			if (vLength.x >= 2000)
				return;
		}
	}

	//이 백터는 내가 찾을 유효한 애들의 좌표를 관리하는 백터
	m_vecIndex.push_back(PostoIndex(StartPos));

	//기울기 많큼 증가하면서 인덱스 저장
	while (true)
	{
		StartPos.x += DX* 0.1f;
		StartPos.z += DZ* 0.1f;

		if (false == IsValidPoint(StartPos))
			break;

		//모든 기준을 통과하면 유효한 인덱스에 좌표들을 넣어줌
		if (false == IsBeforeIdx(StartPos))
			m_vecIndex.push_back(PostoIndex(StartPos));
	}

	int Size = m_vecIndex.size();

	float MinDist = 999999999.f;
	float fDist = 0.0f;

	vector<DxVector3>	pVtx = m_TerrainInfo.vecPos;

	for (int i = 0; i < Size; ++i)
	{
		if (m_vecIndex[i].iRightTop >(m_TerrainInfo.iNumH + 1) * (m_TerrainInfo.iNumW + 1) ||
			m_vecIndex[i].iLeftBot < 0)
			continue;

		if (m_vecIndex[i].iRightBot < 16640 &
			m_vecIndex[i].iLeftTop < 16640 &
			m_vecIndex[i].iRightTop < 16640 &
			m_vecIndex[i].iLeftBot < 16640)
		{
			if (true == DirectX::TriangleTests::Intersects(m_tRay.vPos.Convert()
				, m_tRay.vDir.Convert()
				, pVtx[m_vecIndex[i].iLeftTop].Convert()							//내가 유효하게 모아놓은 애들의 윗 삼각형이 Ray와 충돌하는지 체크
				, pVtx[m_vecIndex[i].iRightTop].Convert()
				, pVtx[m_vecIndex[i].iRightBot].Convert(), fDist))
			{
				if (MinDist >= fDist)
				{
					MinDist = fDist;													//최소 인 애들을 찾아냄
					m_bPick = true;
				}
			}

			if (true == DirectX::TriangleTests::Intersects(m_tRay.vPos.Convert()
				, m_tRay.vDir.Convert()
				, pVtx[m_vecIndex[i].iLeftTop].Convert()
				, pVtx[m_vecIndex[i].iRightBot].Convert()							//내가 유효하게 모아놓은 애들의 아랫 삼각형이 Ray와 충돌하는지 체크
				, pVtx[m_vecIndex[i].iLeftBot].Convert(), fDist))
			{
				if (MinDist >= fDist)
				{
					MinDist = fDist;
					m_bPick = true;
				}
			}
		}
	}

	//PickPos 는 Local Pos 여서 나중에 월드로 쓸려면 지형의 World 행렬을 곱해줘야함
	if (true == m_bPick)
	{
		m_vPickPos = m_tRay.vPos + m_tRay.vDir * MinDist;
	}

	m_vecIndex.clear();

	SAFE_RELEASE(pCamera);
}

bool CInput::IsValidPoint(const DxVector3 & _vPos)
{
	//터레인 안을 잘 픽킹하고 있는 지 찾는 함수
	//현재의 점이 터레인의 밖에 있을 경우 return true
	if (_vPos.x <= (m_TerrainInfo.iNumW* m_TerrainInfo.iSizeW) && _vPos.x >= 0 && _vPos.z <=
		(m_TerrainInfo.iNumH* m_TerrainInfo.iSizeH) && _vPos.z >= 0)
	{
		return true;
	}

	return false;
}

tAreaInfo CInput::PostoIndex(const DxVector3 & _Pos)
{
	//현재 내가 찾은 위치들을 현재 내가 지나는 사각형들로 바꿔줌
	//Terrain의 정점으로 바꿔줌 
	//내가 지나는 Pos 들을 근삿 값으로 바꿈
	DxVector3 TempPos;

	TempPos.x = floorf(_Pos.x);
	TempPos.z = floorf(_Pos.z);

	//현재 내가 지나는 사각형의 네 정점

	int	idx = (m_TerrainInfo.iNumH - 1 - ((int)(TempPos.z / m_TerrainInfo.iSizeH) + 1)) *
		m_TerrainInfo.iNumW + (int)(TempPos.x / m_TerrainInfo.iSizeH);

	tAreaInfo tArea;
	tArea.fRadius = 0.0f;
	tArea.iLeftBot = idx;
	tArea.iLeftTop = idx + 1;
	tArea.iRightTop = idx + m_TerrainInfo.iNumW;
	tArea.iRightBot = idx + m_TerrainInfo.iNumW + 1;

	return tArea;
}


bool CInput::IsBeforeIdx(const DxVector3& _vPos)
{
	//이전의 인덱스와 겹치는지 확인해주는 함수
	//DDT 를 찾을때 근삿값이기 떄문에 같은점이 두번 들어갈 수있는 것을 방지

	UINT Size = m_vecIndex.size();

	if (Size == 0)
		return false;

	tAreaInfo tCurArea = PostoIndex(_vPos);
	tAreaInfo tBeforeArea = m_vecIndex[Size - 1];

	if (tCurArea.iLeftBot == tBeforeArea.iLeftBot &&
		tCurArea.iLeftTop == tBeforeArea.iLeftTop &&
		tCurArea.iRightBot == tBeforeArea.iRightBot &&
		tCurArea.iRightTop == tBeforeArea.iRightTop)
		return true;

	return false;
}
