#include "GameTotalManager.h"
#include "../GameObject/GameObject.h"
#include "../Component/Transform.h"
#include "Input.h"
#include "../Component/Camera.h"
#include "../Scene/SceneManager.h"
#include "../Scene/Scene.h"

AR3D_USING

DEFINITION_SINGLE(CGameTotalManager)

CGameTotalManager::CGameTotalManager()
{
}


CGameTotalManager::~CGameTotalManager()
{
	Safe_Release_Map(m_mapPickedObject);
}

void CGameTotalManager::SetPickActor(CGameObject* pPickedActor)
{
	if (pPickedActor)
	{
		m_pPickedActor = pPickedActor;


		m_pPickedActor->SetPicked(true);

		////카메라 셋팅 
		//CCamera*	pMainCamera = GET_SINGLE(CSceneManager)->GetCurrentScene()
		//	->GetMainCamera();

		//pMainCamera->SetTargetObject(pPickedActor);

		//SAFE_RELEASE(pMainCamera);
	}
	else
	{
		m_pPickedActor = NULL;
	}


}

void CGameTotalManager::ErasePickActor()
{
	if(m_pPickedActor)
		m_pPickedActor->SetPicked(false);
	m_pPickedActor = NULL;
}

CGameObject * CGameTotalManager::GetPickActor()  
{
	return m_pPickedActor;
}

void CGameTotalManager::SetPickedPosition(DxVector3 pos)
{
	m_vPickedPosition = pos;
}

DxVector3 CGameTotalManager::GetPickedPosition() 
{
	return m_vPickedPosition;
}

CROPANDLEVEL CGameTotalManager::GetCropAndNum(int index)
{
	return cropAndlevel[index];
}

bool CGameTotalManager::SetCropAndNum(const string & str, int num)
{
	for (int i = 0; i < 9; ++i)
	{
		if (strcmp(cropAndlevel[i].cropName.c_str(), str.c_str()) == 0)
		{
			cropAndlevel[i].num += num;
			if (cropAndlevel[i].num < 0)
			{
				cropAndlevel[i].num -= num;

				return false; 

			}

			return  true;
		}
	}
	return  false;
}

bool CGameTotalManager::SetCropAndNum(int index, int num)
{
	cropAndlevel[index].num += num;
	if (cropAndlevel[index].num < 0)
	{
		cropAndlevel[index].num -= num;

		return false;

	}
	return true;
}

bool CGameTotalManager::SubtracCropAndNum(int index, int num)
{
	cropAndlevel[index].num -= num;
	if (cropAndlevel[index].num < 0)
	{
		cropAndlevel[index].num += num;

		return false;

	}
	return true;
}

void CGameTotalManager::SetLightPosition(DxVector3 pos)
{
	m_vLightPos.push_back(pos);
}

vector<DxVector3> CGameTotalManager::GetLightPosition()
{
	return m_vLightPos;
}

bool CGameTotalManager::GetNight(void)
{
	return m_bNight;
}

void CGameTotalManager::SetNight(bool bNight)
{
	m_bNight = bNight;
}

bool CGameTotalManager::Init()
{
	cropAndlevel[0]  =  CROPANDLEVEL(0, "Wheat");
	cropAndlevel[1]  =  CROPANDLEVEL(0, "Corn");
	cropAndlevel[2]  =  CROPANDLEVEL(0, "Carrot");
	cropAndlevel[3]  =  CROPANDLEVEL(0, "Potato");
	cropAndlevel[4]  =  CROPANDLEVEL(0, "Eggplant");
	cropAndlevel[5]  =  CROPANDLEVEL(0, "Tomato");
	cropAndlevel[6]  =  CROPANDLEVEL(0, "Pumpkin");
	cropAndlevel[7]  =  CROPANDLEVEL(0, "Onion");
	cropAndlevel[8]  =  CROPANDLEVEL(0, "Cabbage");
	cropAndlevel[9]  =  CROPANDLEVEL(0, "Bread");
	cropAndlevel[10] =  CROPANDLEVEL(0, "water");
	cropAndlevel[11] =  CROPANDLEVEL(0, "ax");

	m_bNight = false;

	return true;
}

void CGameTotalManager::Update(float deltaTime)
{
	
}
