#pragma once

#include "../Engine.h"

AR3D_BEGIN

class DLL CInput
{
private:
	unordered_map<string, PKEYINFO> m_mapKey;
	PKEYINFO m_pCreateKey;
	short		m_sWheel;
	POINT		m_tMousePos;
	POINT		m_tMouseMove;
	class CGameObject*	m_pMouseObj;
	class CTransform*	m_pMouseTr;
	class CColliderRay* m_pRay;
	class CColliderPoint*	m_pMousePoint;
	DxVector3	m_vPlayerMovePos;


	//DDT Pick을위한 
private:
	RAY m_tRay;
	TERRAINCOLLINFO	m_TerrainInfo;
	bool m_bPick;
	//bool m_MouseClickOn
	vector<tAreaInfo>			m_vecIndex;
	DxVector3					m_vPickPos;

public:
	void SetTerrainInfo(TERRAINCOLLINFO	TerrainInfo);
	DxVector3 GetTerrainPickingPos();

	void DDTPicking();
	bool IsValidPoint(const DxVector3& _vPos);
	tAreaInfo PostoIndex(const DxVector3& _Pos);
	bool IsBeforeIdx(const DxVector3& _vPos);
	//================


public:
	bool		m_OnOffCollider; // Collider 켜고 끄고
	bool		m_MouseClickOn; // 마우스로 플레이어들 조절시에 사용하는 변수들

	void SetWheel(short sWheel);
	short GetWheelDir()	const;
	void ClearWheel();
	class CGameObject* GetMouseObj();
	class CColliderRay* GetMouseRay();
	class CColliderPoint* GetMousePoint();

	void SetPlayerMovePos(DxVector3 pos);
	DxVector3 GetPlayerMovePos();

public:
	bool Init();
	void Input(float fTime);
	void Update(float fTime);
	void ComputeRay();

public:
	bool KeyPress(const string& strKey);
	bool KeyPush(const string& strKey);
	bool KeyUp(const string& strKey);
	POINT GetMousePos() const;
	POINT GetMouseMove() const;

private:
	PKEYINFO FindKey(const string& strKey);
	void ComputeMouse(bool bStart = false);

public:
	//종료 함수
	template <typename T>
	bool CreateKey(const T& key)
	{
		if (typeid(T) != typeid(char) && typeid(T) != typeid(int))
		{
			char* pKey = (char*)key;
			m_pCreateKey->strKey = pKey;

			if (FindKey(pKey))
			{
				SAFE_DELETE(m_pCreateKey);
				return false;
			}
			m_mapKey.insert(make_pair(pKey, m_pCreateKey));
		}
		else
			m_pCreateKey->vecKey.push_back((unsigned int)key);

		return true;
	}

	template <typename T, typename ... Types>
	bool CreateKey(const T& key, const Types& ...arg)
	{
		if (!m_pCreateKey)
		{
			m_pCreateKey = new KEYINFO;

			m_pCreateKey->bPress = false;
			m_pCreateKey->bPush = false;
			m_pCreateKey->bUp = false;
		}
		
		if (typeid(T) != typeid(char) && typeid(T) != typeid(int))
		{
			char* pKey = (char*)key;
			m_pCreateKey->strKey = pKey;

			if (FindKey(pKey))
			{
				SAFE_DELETE(m_pCreateKey);
				return false;
			}
			m_mapKey.insert(make_pair(pKey, m_pCreateKey));
		}
		else
			m_pCreateKey->vecKey.push_back((unsigned int)key);

		CreateKey(arg...);

		if (m_pCreateKey)
			m_pCreateKey = NULL;

		return true;
	}



	DECLARE_SINGLE(CInput)
};

AR3D_END