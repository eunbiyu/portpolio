#include "CollisionManager.h"
#include "../GameObject/GameObject.h"
#include "../Component/ColliderRay.h"
#include "../Component/ColliderPoint.h"
#include "Input.h"
#include "../Component/Transform.h"

AR3D_USING

DEFINITION_SINGLE(CCollisionManager)

CCollisionManager::CCollisionManager()
{
}

CCollisionManager::~CCollisionManager()
{
}

void CCollisionManager::AddObject(CGameObject * pObj)
{
	if (!pObj->CheckComponentFromType(CT_COLLIDER))
		return;

	if (!pObj->CheckComponentFromType(CT_UI))
		m_vecCollision.push_back(pObj);

	else
		m_vecUICollision.push_back(pObj);
}

bool CCollisionManager::Init()
{
	m_vecCollision.reserve(1000);
	m_vecUICollision.reserve(1000);

	return true;
}

void CCollisionManager::Collision(float fTime)
{
	// 마우스를 이용한 충돌을 먼저 한다.
	bool	bMouseCollision = false;

	// 마우스 Point와 UI 오브젝트 충돌체들간 충돌처리를 한다.
	bMouseCollision = CollisionMouseUI(fTime);

	// 마우스 Ray와 오브젝트 충돌체들간 충돌처리를 한다.
	if (!bMouseCollision)
	{
		// 충돌 목록을 정렬한다.
		sort(m_vecCollision.begin(), m_vecCollision.end(),
			CCollisionManager::CollisionSort);

		CollisionMouseObject(fTime);
	}
	// 충돌 목록을 정렬한다.
	sort(m_vecCollision.begin(), m_vecCollision.end(),
		CCollisionManager::CollisionSort);

	vector<CGameObject*>::iterator	iter;
	vector<CGameObject*>::iterator	iterEnd;

	if (m_vecUICollision.size() > 1)
	{
		iterEnd = m_vecUICollision.end();
		for (iter = m_vecUICollision.begin(); iter != iterEnd - 1; ++iter)
		{
			for (vector<CGameObject*>::iterator iter1 = iter + 1; iter1 != iterEnd; ++iter1)
			{
				CollisionObject(*iter, *iter1, fTime);
			}
		}
	}

	if (m_vecCollision.size() > 1)
	{
		iterEnd = m_vecCollision.end();
		for (iter = m_vecCollision.begin(); iter != iterEnd - 1; ++iter)
		{
			for (vector<CGameObject*>::iterator iter1 = iter + 1; iter1 != iterEnd; ++iter1)
			{
				CollisionObject(*iter, *iter1, fTime);
			}
		}
	}

	m_vecCollision.clear();
	m_vecUICollision.clear();
}

bool CCollisionManager::CollisionMouseUI(float fTime)
{
	vector<CGameObject*>::iterator	iter;
	vector<CGameObject*>::iterator	iterEnd = m_vecUICollision.end();

	CGameObject*	pMouseObj = GET_SINGLE(CInput)->GetMouseObj();
	CColliderPoint*	pMousePoint = GET_SINGLE(CInput)->GetMousePoint();

	for (iter = m_vecUICollision.begin(); iter != iterEnd; ++iter)
	{
		list<CComponent*>*	pList = (*iter)->FindComponentsFromType(CT_COLLIDER);

		list<CComponent*>::iterator	iter1;
		list<CComponent*>::iterator	iter1End = pList->end();

		for (iter1 = pList->begin(); iter1 != iter1End; ++iter1)
		{
			CCollider*	pColl = (CCollider*)*iter1;

			if (pMousePoint->Collision(pColl))
			{
				pMousePoint->CollisionEnable();
				pColl->CollisionEnable();

				// 처음 충돌할 경우
				if (!pMousePoint->CheckCollList(pColl))
				{
					pMousePoint->AddCollList(pColl);
					pColl->AddCollList(pMousePoint);

					pMouseObj->OnCollisionEnter(pMousePoint, pColl, fTime);
					(*iter)->OnCollisionEnter(pColl, pMousePoint, fTime);
				}

				// 충돌 되고 있는상태일 경우
				else
				{
					pMouseObj->OnCollision(pMousePoint, pColl, fTime);
					(*iter)->OnCollision(pColl, pMousePoint, fTime);
				}

				SAFE_RELEASE(pMousePoint);
				SAFE_RELEASE(pMouseObj);

				return true;
			}

			else if (pMousePoint->CheckCollList(pColl))
			{
				pMousePoint->EraseCollList(pColl);
				pColl->EraseCollList(pMousePoint);

				pMouseObj->OnCollisionLeave(pMousePoint, pColl, fTime);
				(*iter)->OnCollisionLeave(pColl, pMousePoint, fTime);
			}
		}
	}

	SAFE_RELEASE(pMousePoint);
	SAFE_RELEASE(pMouseObj);

	return false;
}

bool CCollisionManager::CollisionMouseObject(float fTime)
{
	CGameObject*	pMouseObj = GET_SINGLE(CInput)->GetMouseObj();
	CColliderRay*	pMouseRay = GET_SINGLE(CInput)->GetMouseRay();

	vector<CGameObject*>::iterator	iter;
	vector<CGameObject*>::iterator	iterEnd = m_vecCollision.end();

	for (iter = m_vecCollision.begin(); iter != iterEnd; ++iter)
	{
		list<CComponent*>*	pList = (*iter)->FindComponentsFromType(CT_COLLIDER);

		list<CComponent*>::iterator	iter1;
		list<CComponent*>::iterator	iter1End = pList->end();

		for (iter1 = pList->begin(); iter1 != iter1End; ++iter1)
		{
			CCollider*	pColl = (CCollider*)*iter1;

			if (pMouseRay->Collision(pColl))
			{
				pMouseRay->CollisionEnable();
				pColl->CollisionEnable();

				// 처음 충돌할 경우
				if (!pMouseRay->CheckCollList(pColl))
				{
					pMouseRay->AddCollList(pColl);
					pColl->AddCollList(pMouseRay);

					pMouseObj->OnCollisionEnter(pMouseRay, pColl, fTime);
					(*iter)->OnCollisionEnter(pColl, pMouseRay, fTime);
				}

				// 충돌 되고 있는상태일 경우
				else
				{
					pMouseObj->OnCollision(pMouseRay, pColl, fTime);
					(*iter)->OnCollision(pColl, pMouseRay, fTime);
				}

				SAFE_RELEASE(pMouseRay);
				SAFE_RELEASE(pMouseObj);

				return true;
			}

			else if (pMouseRay->CheckCollList(pColl))
			{
				pMouseRay->EraseCollList(pColl);
				pColl->EraseCollList(pMouseRay);

				pMouseObj->OnCollisionLeave(pMouseRay, pColl, fTime);
				(*iter)->OnCollisionLeave(pColl, pMouseRay, fTime);
			}
		}
	}

	SAFE_RELEASE(pMouseRay);
	SAFE_RELEASE(pMouseObj);

	return false;
}

void CCollisionManager::CollisionObject(CGameObject * pSrc, CGameObject * pDest,
	float fTime)
{
	list<CComponent*>*	pSrcList = pSrc->FindComponentsFromType(CT_COLLIDER);
	list<CComponent*>*	pDestList = pDest->FindComponentsFromType(CT_COLLIDER);

	list<CComponent*>::iterator	iterSrc;
	list<CComponent*>::iterator	iterSrcEnd = pSrcList->end();

	list<CComponent*>::iterator	iterDest;
	list<CComponent*>::iterator	iterDestEnd = pDestList->end();

	for (iterSrc = pSrcList->begin(); iterSrc != iterSrcEnd; ++iterSrc)
	{
		CCollider*	pSrcColl = (CCollider*)*iterSrc;
		for (iterDest = pDestList->begin(); iterDest != iterDestEnd; ++iterDest)
		{
			CCollider*	pDestColl = (CCollider*)*iterDest;
			if (pSrcColl->GetColliderGroup() != pDestColl->GetColliderGroup())
			{

				if (pSrcColl->Collision(pDestColl))
				{
					if (pSrcColl->GetColliderType() == CT_TERRAIN_COLL ||
						pDestColl->GetColliderType() == CT_TERRAIN_COLL)
					{
						pSrcColl->TerrainCollisionEnable();
						pDestColl->TerrainCollisionEnable();
					}
					else
					{
						pSrcColl->CollisionEnable();
						pDestColl->CollisionEnable();
					}

					// 처음 충돌할 경우
					if (!pSrcColl->CheckCollList(pDestColl))
					{
						pSrcColl->AddCollList(pDestColl);
						pDestColl->AddCollList(pSrcColl);

						pSrc->OnCollisionEnter(pSrcColl, pDestColl, fTime);
						pDest->OnCollisionEnter(pDestColl, pSrcColl, fTime);
					}

					// 충돌 되고 있는상태일 경우
					else
					{
						pSrc->OnCollision(pSrcColl, pDestColl, fTime);
						pDest->OnCollision(pDestColl, pSrcColl, fTime);
					}
				}

				else if (pSrcColl->CheckCollList(pDestColl))
				{
					pSrcColl->EraseCollList(pDestColl);
					pDestColl->EraseCollList(pSrcColl);

					pSrc->OnCollisionLeave(pSrcColl, pDestColl, fTime);
					pDest->OnCollisionLeave(pDestColl, pSrcColl, fTime);
				}
			}
		}
	}
}

bool CCollisionManager::CollisionSort(CGameObject * pObj1,
	CGameObject * pObj2)
{
	CColliderRay*	pRay = GET_SINGLE(CInput)->GetMouseRay();

	CTransform*	pTr1 = pObj1->GetTransform();
	CTransform*	pTr2 = pObj2->GetTransform();

	float	fDist1, fDist2;
	fDist1 = pTr1->GetWorldPos().Distance(pRay->GetRay().vPos);
	fDist2 = pTr2->GetWorldPos().Distance(pRay->GetRay().vPos);

	SAFE_RELEASE(pTr1);
	SAFE_RELEASE(pTr2);
	SAFE_RELEASE(pRay);

	return fDist1 < fDist2;
}
