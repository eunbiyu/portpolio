#include "Scheduler.h"

AR3D_USING

DEFINITION_SINGLE(CScheduler)

CScheduler::CScheduler()
{
}

CScheduler::~CScheduler()
{
	Safe_Delete_VecList(m_Schedule);
}

bool CScheduler::CreateSchedule(const string & strName,
	int(*pFunc)(float), float fLimitTime, SCHEDULE_TYPE eType)
{
	PSCHEDULER		pSchedule = new SCHEDULER;

	pSchedule->eType = eType;
	pSchedule->fLimitTime = fLimitTime;
	pSchedule->fTime = 0.f;
	pSchedule->pObj = NULL;
	pSchedule->strName = strName;
	pSchedule->func = bind(pFunc, placeholders::_1);

	m_Schedule.push_back(pSchedule);

	return true;
}

bool CScheduler::Init()
{
	return true;
}

int CScheduler::Update(float fTime)
{
	list<PSCHEDULER>::iterator	iter;
	list<PSCHEDULER>::iterator	iterEnd = m_Schedule.end();

	for (iter = m_Schedule.begin(); iter != iterEnd;)
	{
		(*iter)->fTime += fTime;

		if ((*iter)->fTime >= (*iter)->fLimitTime)
		{
			(*iter)->fTime -= (*iter)->fLimitTime;

			(*iter)->func(fTime);

			switch ((*iter)->eType)
			{
			case ST_ONCE:
				SAFE_DELETE((*iter));
				iter = m_Schedule.erase(iter);
				break;
			case ST_COUNT:
				break;
			case ST_TIME:
				break;
			default:
				++iter;
				break;
			}
		}

		else
			++iter;
	}

	return 0;
}
