#include "ThreadManager.h"
#include "ThreadQueue.h"
#include "Thread.h"

AR3D_USING

DEFINITION_SINGLE(CThreadManager)

CRITICAL_SECTION CThreadSync::m_Crt;

CThreadManager::CThreadManager()
{
	InitializeCriticalSection(&CThreadSync::m_Crt);
}

CThreadManager::~CThreadManager()
{
	unordered_map<string, CThread*>::iterator	iter;

	for (iter = m_mapThread.begin(); iter != m_mapThread.end(); ++iter)
	{
		SAFE_DELETE(iter->second);
	}

	m_mapThread.clear();

	DeleteCriticalSection(&CThreadSync::m_Crt);
}
