#include "Timer.h"
#include "Debug.h"

AR3D_USING

CTimer::CTimer()
{
}


CTimer::~CTimer()
{
}

float CTimer::GetDeltaTime() const
{
	return m_fDeltaTime;
}

float CTimer::GetFPS() const
{
	return m_fFPS;
}

bool CTimer::Init()
{
	//여기서 시간카운트를 셈 

	//QueryPerformanceFrequency는, 자기 시스템이 최대 
	//어느정도까지 TimerResolution을 지원하는지 판별 
	QueryPerformanceFrequency(&m_tSecond); //프로그램클래스시작에
	//QueryPerformanceCounter는 현재카운터를 알아내는데 쓰임. 
	//사용하고자하는 다음부분에 
	//처리함수도 같은것으로 사용한다. 
	//한번실행하고, 다시 콜함으로써, 각각 얻어진 카운터값의 차이로
	//수행시간을 판단할 수 있습니다. 
	QueryPerformanceCounter(&m_tCount);

	m_fDeltaTime = 0.f;
	m_fFPS = 0.f;
	m_fFPSTime = 0.f;
	m_iFrame = 0;
	m_iFrameMax = 60; //기준치 최대 프레임 

	return true;
}

void CTimer::Update()
{
	LARGE_INTEGER tCount;

	QueryPerformanceCounter(&tCount);

	//DeltaTime :우리가만들어낼것 
	//쿼드파트에 저장한다. 
	m_fDeltaTime = (tCount.QuadPart - m_tCount.QuadPart) / (float)m_tSecond.QuadPart;

	m_tCount = tCount; // 방금잰시간(tCount), 전에있던시간(m_tCount) 이 차이로 시간을잼

	//시간/초를 FPS타임에 넣어준다. 
	m_fFPSTime += m_fDeltaTime;
	++m_iFrame;//인티저 프레임 증가 
	

	if (m_iFrame == m_iFrameMax) // 인티저프레임이 60이되면
	{
		//m_fFPS = m_iFrame / m_fFPSTime;
		//m_iFrame = 0;
		//m_fFPSTime = 0.f;
		//char strFPS[64] = {};
		////sprintf_S를 써야 실수가 먹음 
		//sprintf_s(strFPS, "FPS : %f", m_fFPS);
		//CDebug::OutputTitleBar(strFPS);


	}
	m_fFPS = m_iFrame / m_fFPSTime;
	m_iFrame = 0;
	m_fFPSTime = 0.f;
	char strFPS[64] = {};
	//sprintf_S를 써야 실수가 먹음 
	sprintf_s(strFPS, "FPS : %f", m_fFPS);
	CDebug::OutputTitleBar(strFPS);


}
