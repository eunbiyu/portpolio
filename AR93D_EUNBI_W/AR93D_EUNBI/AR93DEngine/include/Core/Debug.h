#pragma once

#include "../Engine.h"

AR3D_BEGIN

class DLL CDebug
{
public:
	static void OutputConsole(char* pMsg); 
	static void OutputVisualStudio(char* pMsg);
	static void OutputVisualStudio(TCHAR* pMsg);
	static void OutputTitleBar(char* pMsg);
	static void OutputTitleBar(TCHAR* pMsg);
};

AR3D_END