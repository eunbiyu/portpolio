#include "Debug.h"
#include "../Core.h"

AR3D_USING

void CDebug::OutputConsole(char * pMsg)
{
	//콘솔창 출력
	//_cprintf(pMsg);
}

void CDebug::OutputVisualStudio(char * pMsg)
{
	//OutputDebugStringA(pMsg);
}

void CDebug::OutputVisualStudio(TCHAR * pMsg)
{
	//OutputDebugString(pMsg);
}

void CDebug::OutputTitleBar(char * pMsg)
{
//#ifdef _DEBUG
	SetWindowTextA(WINDOWHANDLE, pMsg);
//#endif // _DEBUG
}

void CDebug::OutputTitleBar(TCHAR * pMsg)
{
//#ifdef _DEBUG
	SetWindowText(WINDOWHANDLE, pMsg);
//#endif // _DEBUG
}
