#pragma once

#include "../Engine.h"

AR3D_BEGIN

class DLL CThreadManager
{
private:
	unordered_map<string, class CThread*> m_mapThread;
public:
	template <typename T>
	T* CreateThread(const string strKey)
	{
		T* pThread = new T;
		if (!pThread->Init())
		{
			SAFE_DELETE(pThread);
			return NULL;
		}
		m_mapThread.insert(make_pair(strKey, pThread));

		return pThread;
	}

	DECLARE_SINGLE(CThreadManager)
};

AR3D_END

