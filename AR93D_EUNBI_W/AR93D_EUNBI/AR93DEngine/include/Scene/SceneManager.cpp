#include "SceneManager.h"
#include "Scene.h"

AR3D_USING

DEFINITION_SINGLE(CSceneManager)

CSceneManager::CSceneManager():
	m_pNextScene(NULL),
	m_pCurrentScene(NULL),
	m_pPrevScene(NULL)
{
}

CSceneManager::~CSceneManager()
{
	SAFE_DELETE(m_pNextScene);
	SAFE_DELETE(m_pCurrentScene);
	SAFE_DELETE(m_pPrevScene);
}

CScene * CSceneManager::GetCurrentScene()
{
	return m_pCurrentScene;
}



bool CSceneManager::Init()
{
	// 기본 장면을 생성한다.
	m_pCurrentScene = CreateScene("DefaultScene");

	return true;
}

int CSceneManager::Input(float fTime)
{
	m_pCurrentScene->Input(fTime);

	return ChangeScene();
}

int CSceneManager::Update(float fTime)
{
	m_pCurrentScene->Update(fTime);
	
	return ChangeScene();
}

int CSceneManager::LateUpdate(float fTime)
{
	m_pCurrentScene->LateUpdate(fTime);

	return ChangeScene();
}

void CSceneManager::Collision(float fTime)
{
	m_pCurrentScene->Collision(fTime);
}

void CSceneManager::Render(float fTime)
 {
	m_pCurrentScene->Render(fTime);
}

CScene * CSceneManager::CreateScene(const string & strTag)
{
	CScene*	pScene = new CScene;

	if (!pScene->Init())
	{
		SAFE_DELETE(pScene);
		return NULL;
	}

	pScene->SetTag(strTag);

	return pScene;
}

int CSceneManager::ChangeScene()
{
	if (m_pNextScene)
	{
		if (m_pPrevScene)
			m_pCurrentScene = m_pNextScene;
		else
		{
			SAFE_DELETE(m_pCurrentScene);
			m_pCurrentScene = m_pNextScene;
		}

		m_pNextScene = NULL;
		return SC_NEXT;
	}

	return SC_NONE;
}

void CSceneManager::ReplaceScene(CScene * pScene)
{
	m_pNextScene = pScene;
}

void CSceneManager::PushScene(CScene * pScene)
{
	m_pPrevScene = m_pCurrentScene;
	m_pNextScene = pScene;
}

void CSceneManager::PrevScene()
{
	SAFE_DELETE(m_pCurrentScene);
	m_pCurrentScene = m_pPrevScene;
	m_pPrevScene = NULL;
}
