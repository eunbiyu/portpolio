#pragma once
#include "../BaseObj.h"

AR3D_BEGIN

class DLL CLayer :
	public CBaseObj
{
private:
	friend class CScene;

private:
	CLayer();
	~CLayer();

private:
	list<class CGameObject*>	m_ObjList;
	class CScene*				m_pScene;
	SORT_FLAG	m_eSortFlag;
	int		m_iZOrder;
	bool	m_bZOrder;
	bool	m_bCollisionOn;

public:
	void SetZOrder(int iZOrder);
	void SetScene(class CScene* pScene);
	void DisableZOrder();
	void SetSortFlag(SORT_FLAG eFlag);
	void SetCollisionOn(bool col);
	bool GetCollisionOn(void);

public:
	int GetZOrder()	const;
	bool GetZOrderEnable()	const;
	class CScene* GetScene();

public:
	void AddObject(class CGameObject* pObj);
	void EraseObject(CGameObject * pObj);
	void EraseAllObject();
public:
	static bool ObjectZSort(class CGameObject* p1, class CGameObject* p2);
	static bool ObjectZSortDescending(class CGameObject* p1, class CGameObject* p2);
	class CGameObject* FindObjectFromTag(const string& strTag);
	list<class CGameObject*> GetObjectsList();

public:
	bool Init();
	void Input(float fTime);
	void Update(float fTime);
	void LateUpdate(float fTime);
	void Collision(float fTime);
	void Render(float fTime);
};

AR3D_END
