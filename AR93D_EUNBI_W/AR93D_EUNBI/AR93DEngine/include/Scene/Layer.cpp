#include "Layer.h"
#include "../GameObject/GameObject.h"
#include "Scene.h"
#include "../Component/Transform.h"
#include "SceneManager.h"
#include "../Component/Frustum.h"
#include "../Component/Renderer.h"
#include "../Component/Renderer2D.h"
#include "../Resources/Mesh.h"
#include "../Rendering/RenderManager.h"

AR3D_USING

CLayer::CLayer() :
	m_iZOrder(0),
	m_bZOrder(false),
	m_eSortFlag(SF_ASCENDING), 
	m_bCollisionOn(true)
{
}

CLayer::~CLayer()
{
	Safe_Release_VecList(m_ObjList);
}

void CLayer::SetZOrder(int iZOrder)
{
	m_iZOrder = iZOrder;
	m_bZOrder = true;
}

void CLayer::SetScene(CScene * pScene)
{
	m_pScene = pScene;

	list<CGameObject*>::iterator	iter;
	list<CGameObject*>::iterator	iterEnd = m_ObjList.end();

	for (iter = m_ObjList.begin(); iter != iterEnd; ++iter)
	{
		(*iter)->SetScene(pScene);
	}
}

void CLayer::DisableZOrder()
{
	m_bZOrder = false;
}

void CLayer::SetSortFlag(SORT_FLAG eFlag)
{
	m_eSortFlag = eFlag;
}

void CLayer::SetCollisionOn(bool col)
{
	m_bCollisionOn = col;
}

bool CLayer::GetCollisionOn(void)
{
	return m_bCollisionOn;
}

int CLayer::GetZOrder() const
{
	return m_iZOrder;
}

bool CLayer::GetZOrderEnable() const
{
	return m_bZOrder;
}

CScene * CLayer::GetScene()
{
	return m_pScene;
}

void CLayer::AddObject(CGameObject * pObj)
{
	pObj->AddRef();
	pObj->SetScene(m_pScene);
	pObj->SetLayer(this);


	m_ObjList.push_back(pObj);
}

void CLayer::EraseObject(CGameObject * pObj)
{

	list<CGameObject*>::iterator	iter;
	list<CGameObject*>::iterator	iterEnd = m_ObjList.end();

	for (iter = m_ObjList.begin(); iter != iterEnd; ++iter)
	{

		if ((*iter) == pObj)
		{
			(*iter)->Death();
		}
	}

}

void CLayer::EraseAllObject()
{
	list<CGameObject*>::iterator	iter;
	list<CGameObject*>::iterator	iterEnd = m_ObjList.end();

	for (iter = m_ObjList.begin(); iter != iterEnd; ++iter)
	{
		(*iter)->Death();
	}
}

bool CLayer::ObjectZSort(CGameObject * p1, CGameObject * p2)
{
	CTransform*	pTr1 = p1->GetTransform();
	CTransform*	pTr2 = p2->GetTransform();

	CGameObject*	pCameraObj = GET_SINGLE(CSceneManager)->GetCurrentScene()->GetMainCameraObj();

	CTransform*	pCamTr = pCameraObj->GetTransform();

	float	fDist1, fDist2;

	fDist1 = pTr1->GetWorldPos().Distance(pCamTr->GetWorldPos());
	fDist2 = pTr2->GetWorldPos().Distance(pCamTr->GetWorldPos());

	SAFE_RELEASE(pCamTr);
	SAFE_RELEASE(pTr1);
	SAFE_RELEASE(pTr2);

	SAFE_RELEASE(pCameraObj);


	return fDist1 < fDist2;
}

bool CLayer::ObjectZSortDescending(CGameObject * p1, CGameObject * p2)
{
	CTransform*	pTr1 = p1->GetTransform();
	CTransform*	pTr2 = p2->GetTransform();

	CGameObject*	pCameraObj = GET_SINGLE(CSceneManager)->GetCurrentScene()->GetMainCameraObj();

	CTransform*	pCamTr = pCameraObj->GetTransform();

	float	fDist1, fDist2;

	fDist1 = pTr1->GetWorldPos().Distance(pCamTr->GetWorldPos());
	fDist2 = pTr2->GetWorldPos().Distance(pCamTr->GetWorldPos());

	SAFE_RELEASE(pCamTr);
	SAFE_RELEASE(pTr1);
	SAFE_RELEASE(pTr2);

	SAFE_RELEASE(pCameraObj);


	return fDist1 > fDist2;
}

CGameObject * CLayer::FindObjectFromTag(const string & strTag)
{
	list<class CGameObject*>::iterator	iter;
	list<class CGameObject*>::iterator	iterEnd = m_ObjList.end();

	for (iter = m_ObjList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetTag() == strTag)
		{
			(*iter)->AddRef();
			return *iter;
		}
	}

	return NULL;
}

list<class CGameObject*> CLayer::GetObjectsList()
{
	return m_ObjList;
}

bool CLayer::Init()
{
	return true;
}

void CLayer::Input(float fTime)
{
	list<CGameObject*>::iterator	iter;
	list<CGameObject*>::iterator	iterEnd = m_ObjList.end();

	for (iter = m_ObjList.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		(*iter)->Input(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_ObjList.erase(iter);
		}

		else
			++iter;
	}
}

void CLayer::Update(float fTime)
{
	list<CGameObject*>::iterator	iter;
	list<CGameObject*>::iterator	iterEnd = m_ObjList.end();

	for (iter = m_ObjList.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		(*iter)->Update(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_ObjList.erase(iter);
		}

		else
			++iter;
	}
}

void CLayer::LateUpdate(float fTime)
{
	list<CGameObject*>::iterator	iter;
	list<CGameObject*>::iterator	iterEnd = m_ObjList.end();
	
	if (m_ObjList.size() > 0)
	{
		for (iter = m_ObjList.begin(); iter != iterEnd;)
		{
			if (!(*iter)->GetEnable())
			{
				++iter;
				continue;
			}

			(*iter)->LateUpdate(fTime);

			if (!(*iter)->GetAlive())
			{
				SAFE_RELEASE((*iter));
				iter = m_ObjList.erase(iter);
			}

			else
				++iter;
		}
	}
}

void CLayer::Collision(float fTime)
{
	
		list<CGameObject*>::iterator	iter;
		list<CGameObject*>::iterator	iterEnd = m_ObjList.end();

		for (iter = m_ObjList.begin(); iter != iterEnd;)
		{
			if (!(*iter)->GetEnable())
			{
				++iter;
				continue;
			}
			if(	m_bCollisionOn)
				(*iter)->Collision(fTime);

			if (!(*iter)->GetAlive())
			{
				SAFE_RELEASE((*iter));
				iter = m_ObjList.erase(iter);
			}

			else
				++iter;
		}
}

void CLayer::Render(float fTime)
{
	/*if (m_ObjList.size() >= 2)
	{
	switch (m_eSortFlag)
	{
	case SF_ASCENDING:
	m_ObjList.sort(CLayer::ObjectZSort);
	break;
	case SF_DESCENDING:
	m_ObjList.sort(CLayer::ObjectZSortDescending);
	break;
	}
	}*/

	list<CGameObject*>::iterator	iter;
	list<CGameObject*>::iterator	iterEnd = m_ObjList.end();

	for (iter = m_ObjList.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		CRenderer*	pRenderer = (*iter)->FindComponentFromTypeID<CRenderer>();
		CRenderer2D*	pRenderer2D = (*iter)->FindComponentFromTypeID<CRenderer2D>();

		CTransform*	pTransform = (*iter)->GetTransform();

		bool	bFrustum = false;
		CFrustum*	pFrustum = m_pScene->GetMainCameraFrustum();

		if (pRenderer)
		{
			CMesh*	pMesh = pRenderer->GetMesh();

			SPHEREINFO	tInfo = pMesh->GetSphereInfo();

			DxVector3	vScale = pTransform->GetWorldScale();

			float	fMax = vScale.x > vScale.y ? vScale.x : vScale.y;
			fMax = fMax > vScale.z ? fMax : vScale.z;

			tInfo.vCenter *= vScale;
			tInfo.vCenter += pTransform->GetWorldPos();
			tInfo.fRadius *= fMax;
			tInfo.fRadius *= 2;
			bFrustum = pFrustum->FrustumInSphere(tInfo);
			//bFrustum = false;
			SAFE_RELEASE(pMesh);
		}

		else if (pRenderer2D && !(*iter)->CheckComponentFromType(CT_UI))
		{
			CMesh*	pMesh = pRenderer2D->GetMesh();

			SPHEREINFO	tInfo = pMesh->GetSphereInfo();

			DxVector3	vScale = pTransform->GetWorldScale();

			float	fMax = vScale.x > vScale.y ? vScale.x : vScale.y;
			fMax = fMax > vScale.z ? fMax : vScale.z;

			tInfo.vCenter *= vScale;
			tInfo.vCenter += pTransform->GetWorldPos();
			tInfo.fRadius *= fMax;
			tInfo.fRadius *= 2;
			bFrustum = pFrustum->FrustumInSphere(tInfo);

			//bFrustum = false;
			SAFE_RELEASE(pMesh);
		}

		SAFE_RELEASE(pRenderer);
		SAFE_RELEASE(pRenderer2D);
		SAFE_RELEASE(pFrustum);
		SAFE_RELEASE(pTransform);

		if (!bFrustum)
		{
			GET_SINGLE(CRenderManager)->AddRenderObject(*iter);
			//(*iter)->Render(fTime);
		}

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_ObjList.erase(iter);
		}

		else
			++iter;
	}
}
