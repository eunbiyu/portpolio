#include "Scene.h"
#include "Layer.h"
#include "SceneScript.h"
#include "../GameObject/GameObject.h"
#include "../Component/Camera.h"
#include "../Component/Transform.h"
#include "../Component/LightDir.h"
#include "../Component/Renderer.h"
#include "../Component/Material.h"
#include "../Component/Frustum.h"
#include "../Core.h"
#include "../Component/LightPoint.h"
#include "../Component/Effect.h"
#include "../Core/Input.h"
#include "../Core/GameTotalManager.h"

AR3D_USING

CScene::CScene() :
	m_pCameraObj(NULL),
	m_pCamera(NULL),
	m_pFrustum(NULL),
	m_iDialogType(DT_OBJECT)
{
}

CScene::~CScene()
{
	for (size_t i = 0; i < m_vecSceneScript.size(); ++i)
	{
		SAFE_DELETE(m_vecSceneScript[i]);
	}
	m_vecSceneScript.clear();
	Safe_Release_VecList(m_vecLayer);


	SAFE_RELEASE(m_pEnvironment);
	Safe_Release_Map(m_mapCamera);
	SAFE_RELEASE(m_pFrustum);
	SAFE_RELEASE(m_pCamera);
	SAFE_RELEASE(m_pCameraObj);
	SAFE_RELEASE(m_GlobalLight);
	Safe_Release_VecList(m_LightList);
}

CGameObject * CScene::GetMainCameraObj() const
{
	m_pCameraObj->AddRef();
	return m_pCameraObj;
}

CCamera * CScene::GetMainCamera() const
{
	m_pCamera->AddRef();
	return m_pCamera;
}

CTransform * CScene::GetMainCameraTransform() const
{
	return m_pCameraObj->GetTransform();
}

CFrustum * CScene::GetMainCameraFrustum() const
{
	m_pFrustum->AddRef();
	return m_pFrustum;
}

CGameObject * CScene::FindCameraObj(const string & strKey) const
{
	unordered_map<string, class CGameObject*>::const_iterator	iter = m_mapCamera.find(strKey);

	if (iter == m_mapCamera.end())
		return NULL;

	iter->second->AddRef();

	return iter->second;
}

CCamera * CScene::FindCamera(const string & strKey) const
{
	unordered_map<string, class CGameObject*>::const_iterator	iter = m_mapCamera.find(strKey);

	if (iter == m_mapCamera.end())
		return NULL;

	CCamera*	pCamera = iter->second->FindComponentFromTypeID<CCamera>();

	return pCamera;
}

CFrustum * CScene::FindFrustum(const string & strKey) const
{
	unordered_map<string, class CGameObject*>::const_iterator	iter = m_mapCamera.find(strKey);

	if (iter == m_mapCamera.end())
		return NULL;

	CFrustum*	pFrustum = iter->second->FindComponentFromTypeID<CFrustum>();

	return pFrustum;
}

CTransform * CScene::FindCameraTransform(const string & strKey) const
{
	unordered_map<string, class CGameObject*>::const_iterator	iter = m_mapCamera.find(strKey);

	if (iter == m_mapCamera.end())
		return NULL;

	return iter->second->GetTransform();
}

const list<class CGameObject*>* CScene::GetLightList() const
{
	return &m_LightList;
}

bool CScene::LayerZSort(CLayer * p1, CLayer * p2)
{
	return p1->GetZOrder() < p2->GetZOrder();
}

void CScene::SetTag(const string & strTag)
{
	m_strTag = strTag;
}

string CScene::GetTag() const
{
	return m_strTag;
}

CLayer * CScene::CreateLayer(const string & strTag, int iZOrder)
{

	CLayer*	pLayer = new CLayer;

	if (!pLayer->Init())
	{
		SAFE_DELETE(pLayer);
		return NULL;
	}

	pLayer->SetTag(strTag);
	pLayer->SetZOrder(iZOrder);
	pLayer->SetScene(this);

	pLayer->AddRef();
	m_vecLayer.push_back(pLayer);

	if (m_vecLayer.size() >= 2)
		sort(m_vecLayer.begin(), m_vecLayer.end(), CScene::LayerZSort);

	return pLayer;
}

CGameObject * CScene::CreateCamera(const string & strKey, const DxVector3 & vPos)
{
	CGameObject*	pCameraObj = FindCameraObj(strKey);

	if (pCameraObj)
		return pCameraObj;

	pCameraObj = CGameObject::Create(strKey);

	CTransform*	pTransform = pCameraObj->GetTransform();

	pTransform->SetWorldPos(vPos);

	SAFE_RELEASE(pTransform);

	CCamera*	pCamera = pCameraObj->AddComponent<CCamera>("Camera");

	SAFE_RELEASE(pCamera);

	CFrustum*	pFrustum = pCameraObj->AddComponent<CFrustum>("Frustum");

	SAFE_RELEASE(pFrustum);

	pCameraObj->AddRef();

	pCameraObj->SetScene(this);

	m_mapCamera.insert(make_pair(strKey, pCameraObj));

	return pCameraObj;
}

bool CScene::CreateProjection(const string & strKey, float fAngle, float fWidth,
	float fHeight, float fNear, float fFar)
{
	CCamera*	pCamera = FindCamera(strKey);

	if (!pCamera)
		return false;

	pCamera->CreateProjection(fAngle, fWidth, fHeight, fNear, fFar);

	SAFE_RELEASE(pCamera);

	return true;
}

bool CScene::CreateOrthoProjection(const string & strKey, float fWidth, float fHeight,
	float fNear, float fFar)
{
	CCamera*	pCamera = FindCamera(strKey);

	if (!pCamera)
		return false;

	pCamera->CreateOrthoProjection(fWidth, fHeight, fNear, fFar);

	SAFE_RELEASE(pCamera);

	return true;
}

bool CScene::CreateProjection(float fAngle, float fWidth, float fHeight, float fNear, float fFar)
{
	m_pCamera->CreateProjection(fAngle, fWidth, fHeight, fNear, fFar);

	return true;
}

bool CScene::CreateOrthoProjection(float fWidth, float fHeight, float fNear, float fFar)
{
	m_pCamera->CreateOrthoProjection(fWidth, fHeight, fNear, fFar);

	return true;
}

void CScene::ChangeCamera(const string & strKey)
{
	CGameObject*	pChangeObj = FindCameraObj(strKey);
	CCamera*	pChange = FindCamera(strKey);

	SAFE_RELEASE(m_pCameraObj);
	SAFE_RELEASE(m_pCamera);

	m_pCameraObj = pChangeObj;
	m_pCamera = pChange;
}

CGameObject * CScene::CreateLight(const string & strTag, LIGHT_TYPE eType)
{
	CGameObject*	pLightObj = CGameObject::Create(strTag);

	CLight*	pLight = NULL;

	switch (eType)
	{
	case LT_DIR:
		pLight = pLightObj->AddComponent<CLightDir>(strTag);
		break;
	case LT_POINT:
		pLight = pLightObj->AddComponent<CLightPoint>(strTag);
		break;
	case LT_SPOT:
		break;
	}

	SAFE_RELEASE(pLight);

	pLightObj->AddRef();

	m_LightList.push_back(pLightObj);

	return pLightObj;
}

CGameObject * CScene::FindLight(const string & strTag)
{
	list<class CGameObject*>::iterator iter;
	list<class CGameObject*>::iterator iterEnd = m_LightList.end();

	for (iter = m_LightList.begin(); iter != iterEnd; ++iter)
	{
		if ((*iter)->GetTag() == strTag)
		{
			(*iter)->AddRef();
			return (*iter);
		}
	}

	return  NULL;
}


bool CScene::Init()
{
	CLayer*	pLayer = CreateLayer("DefaultLayer", 1);

	pLayer->SetSortFlag(SF_DESCENDING);

	SAFE_RELEASE(pLayer);

	CLayer*	pUILayer = CreateLayer("UILayer", INT_MAX);

	SAFE_RELEASE(pUILayer);

	CLayer*	pMapLayer = CreateLayer("MapLayer");

	SAFE_RELEASE(pMapLayer);

	// 기본 환경맵 생성
	m_pEnvironment = CGameObject::Create("Environment");

	CTransform*	pEvTransform = m_pEnvironment->GetTransform();

	pEvTransform->SetWorldScale(200000.f, 20000.f, 200000.f);

	SAFE_RELEASE(pEvTransform);

	m_pEnvironment->SetScene(this);

	CRenderer*	pRenderer = m_pEnvironment->AddComponent<CRenderer>("Environment");

	pRenderer->SetMesh("PosCube");
	pRenderer->SetShader(SKY_SHADER);
	pRenderer->SetInputLayout("PosInputLayout");
	pRenderer->SetRenderState(CULLING_CW);
	pRenderer->SetRenderState(DEPTH_LESS_EQUAL);

	CMaterial*	pMaterial = pRenderer->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "DefaultEnvironment", L"SkyBox2.dds");
	
	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	// 메인 카메라 생성
	m_pCameraObj = CreateCamera("MainCamera", DxVector3(0.f, 0.f, -5.f));

	m_pCamera = (CCamera*)m_pCameraObj->FindComponentFromTypeID<CCamera>();

	m_pFrustum = (CFrustum*)m_pCameraObj->FindComponentFromTypeID<CFrustum>();

	// UI 카메라 생성
	CGameObject*	pCameraObj = CreateCamera("UICamera", DxVector3(0.f, 0.f, -5.f));

	CreateOrthoProjection("UICamera", _RESOLUTION.iWidth, _RESOLUTION.iHeight);

	SAFE_RELEASE(pCameraObj);

	// 장면은 기본적으로 전역조명으로 방향성 조명을 하나 무조건 가지고있게 해준다.

	CGameObject*	pLight = CreateLight("GlobalLight", LT_DIR);
	m_GlobalLight = pLight;
	CTransform*	pTransform = pLight->GetTransform();

	pTransform->RotateX(AR3D_PI / 4.f);
	pTransform->RotateY(AR3D_PI / 2.f);

	SAFE_RELEASE(pTransform);
	SAFE_RELEASE(pLight);

	//// Point Light
	//CGameObject* pLight = CreateLight("GlobalLight", LT_POINT);

	//CTransform* pTransform = pLight->GetTransform();

	//pTransform->SetWorldPos(500.f, 500.f, 500.f);

	//SAFE_RELEASE(pTransform);


	//CLightPoint*	pLightCom = (CLightPoint*)pLight->FindComponentFromType(CT_LIGHT);

	//pLightCom->SetLightRange(200.f);
	//pLightCom->SetLightInfo(DxVector4(0.5f, 0.5f, 0.5f, 1.f),
	//	DxVector4(0.5f, 0.5f, 0.5f, 1.f),
	//	DxVector4(0.5f, 0.5f, 0.5f, 1.f));

	//SAFE_RELEASE(pLightCom);

	//SAFE_RELEASE(pLight);


	


	//pLight = CreateLight("LightPoint3", LT_POINT);

	//pTransform = pLight->GetTransform();

	//pTransform->SetWorldPos(200.f, 20.f, 200.f);

	//SAFE_RELEASE(pTransform);

	//pLightCom = (CLightPoint*)pLight->FindComponentFromType(CT_LIGHT);

	//pLightCom->SetLightRange(50.f);
	//pLightCom->SetLightInfo(DxVector4(0.f, 0.f, 1.f, 1.f),
	//	DxVector4(0.f, 0.f, 0.2f, 1.f),
	//	DxVector4(0.f, 0.f, 1.f, 1.f));

	//SAFE_RELEASE(pLightCom);

	//SAFE_RELEASE(pLight);
	m_bNight = false;
	m_bDay = true;
	return true;
}

void CScene::Input(float fTime)
{
	for (size_t i = 0; i < m_vecSceneScript.size(); ++i)
	{
		m_vecSceneScript[i]->Input(fTime);
	}

	list<CGameObject*>::iterator	iterL;
	list<CGameObject*>::iterator	iterLEnd = m_LightList.end();
	for (iterL = m_LightList.begin(); iterL != iterLEnd;)
	{
		if (!(*iterL)->GetEnable())
		{
			++iterL;
			continue;
		}

		(*iterL)->Input(fTime);

		if (!(*iterL)->GetAlive())
		{
			SAFE_RELEASE((*iterL));
			iterL = m_LightList.erase(iterL);
		}

		else
			++iterL;
	}

	m_pCameraObj->Input(fTime);

	vector<CLayer*>::iterator	iter;
	vector<CLayer*>::iterator	iterEnd = m_vecLayer.end();
	for (iter = m_vecLayer.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		(*iter)->Input(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_vecLayer.erase(iter);
		}

		else
			++iter;
	}
}

void CScene::Update(float fTime)
{

	m_pEnvironment->Update(fTime);


	for (size_t i = 0; i < m_vecSceneScript.size(); ++i)
	{
		m_vecSceneScript[i]->Update(fTime);
	}

	list<CGameObject*>::iterator	iterL;
	list<CGameObject*>::iterator	iterLEnd = m_LightList.end();
	for (iterL = m_LightList.begin(); iterL != iterLEnd;)
	{
		if (!(*iterL)->GetEnable())
		{
			++iterL;
			continue;
		}

		(*iterL)->Update(fTime);

		if (!(*iterL)->GetAlive())
		{
			SAFE_RELEASE((*iterL));
			iterL = m_LightList.erase(iterL);
		}

		else
			++iterL;
	}

	vector<CLayer*>::iterator	iter;
	vector<CLayer*>::iterator	iterEnd = m_vecLayer.end();
	for (iter = m_vecLayer.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		(*iter)->Update(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_vecLayer.erase(iter);
		}

		else
			++iter;
	}

	m_pCameraObj->Update(fTime);

	if (GET_SINGLE(CGameTotalManager)->GetNight())
	{
		if (m_bNight == false)
		{
			CTransform*	pTransform = m_GlobalLight->GetTransform();
			pTransform->RotateZ(AR3D_PI / 2.f);
			SAFE_RELEASE(pTransform);
			m_bNight = true;
			m_bDay = false;
			CRenderer*	pRenderer = (CRenderer*)m_pEnvironment->FindComponentFromTag("Environment");

			CMaterial*	pMaterial = pRenderer->GetMaterial();

			pMaterial->SetDiffuseTexture("Linear", "NightEnvironment", L"SkyBox3.dds");

			SAFE_RELEASE(pMaterial);

			SAFE_RELEASE(pRenderer);
		}
	}
	else
	{
		if (m_bDay == false)
		{
			CTransform*	pTransform = m_GlobalLight->GetTransform();
			pTransform->RotateZ(-AR3D_PI / 2.f);
			SAFE_RELEASE(pTransform);
			m_bNight = false;
			m_bDay = true;

			CRenderer*	pRenderer = (CRenderer*)m_pEnvironment->FindComponentFromTag("Environment");

			CMaterial*	pMaterial = pRenderer->GetMaterial();

			pMaterial->SetDiffuseTexture("Linear", "DayEnvironment", L"SkyBox2.dds");

			SAFE_RELEASE(pMaterial);

			SAFE_RELEASE(pRenderer);
		}
	}
}

void CScene::LateUpdate(float fTime)
{

	m_pEnvironment->LateUpdate(fTime);

	for (size_t i = 0; i < m_vecSceneScript.size(); ++i)
	{
		m_vecSceneScript[i]->LateUpdate(fTime);
	}

	list<CGameObject*>::iterator	iterL;
	list<CGameObject*>::iterator	iterLEnd = m_LightList.end();
	for (iterL = m_LightList.begin(); iterL != iterLEnd;)
	{
		if (!(*iterL)->GetEnable())
		{
			++iterL;
			continue;
		}

		(*iterL)->LateUpdate(fTime);

		if (!(*iterL)->GetAlive())
		{
			SAFE_RELEASE((*iterL));
			iterL = m_LightList.erase(iterL);
		}

		else
			++iterL;
	}

	vector<CLayer*>::iterator	iter;
	vector<CLayer*>::iterator	iterEnd = m_vecLayer.end();
	for (iter = m_vecLayer.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		(*iter)->LateUpdate(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_vecLayer.erase(iter);
		}

		else
			++iter;
	}

	m_pCameraObj->LateUpdate(fTime);

	bool	bSort = false;
	for (size_t i = 0; i < m_vecLayer.size(); ++i)
	{
		if (m_vecLayer[i]->GetZOrderEnable())
		{
			bSort = true;
			m_vecLayer[i]->DisableZOrder();
		}
	}

	sort(m_vecLayer.begin(), m_vecLayer.end(), CScene::LayerZSort);
}

void CScene::Collision(float fTime)
{
	for (size_t i = 0; i < m_vecSceneScript.size(); ++i)
	{
		m_vecSceneScript[i]->Collision(fTime);
	}

	vector<CLayer*>::iterator	iter;
	vector<CLayer*>::iterator	iterEnd = m_vecLayer.end();
	for (iter = m_vecLayer.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		if((*iter)->GetCollisionOn())
			(*iter)->Collision(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_vecLayer.erase(iter);
		}

		else
			++iter;
	}

	m_pCameraObj->Collision(fTime);
}

void CScene::Render(float fTime)
{
	for (size_t i = 0; i < m_vecSceneScript.size(); ++i)
	{
		m_vecSceneScript[i]->Render(fTime);
	}


	m_pEnvironment->Render(fTime);


	vector<CLayer*>::iterator	iter;
	vector<CLayer*>::iterator	iterEnd = m_vecLayer.end();
	for (iter = m_vecLayer.begin(); iter != iterEnd;)
	{
		if (!(*iter)->GetEnable())
		{
			++iter;
			continue;
		}

		(*iter)->Render(fTime);

		if (!(*iter)->GetAlive())
		{
			SAFE_RELEASE((*iter));
			iter = m_vecLayer.erase(iter);
		}

		else
			++iter;
	}

	m_pCameraObj->Render(fTime);
}

CLayer * CScene::FindLayer(const string & strTag)
{
	
	for (size_t i = 0; i < m_vecLayer.size(); ++i)
	{
		if (m_vecLayer[i]->GetTag() == strTag)
		{
			m_vecLayer[i]->AddRef();
			return m_vecLayer[i];
		}
	}

	return CreateLayer(strTag);
}

void CScene::SetDialogType(int iDialogType)
{
	m_iDialogType = iDialogType;
}

int CScene::GetDialogType(void)
{
	return m_iDialogType;
}

void CScene::SetTerrainColInfo(TERRAINCOLLINFO pTerrainInfo)
{
	m_pTerrainInfo = pTerrainInfo;
	GET_SINGLE(CInput)->SetTerrainInfo(m_pTerrainInfo);
}

TERRAINCOLLINFO CScene::GetTerrainColInfo(void)
{
	return m_pTerrainInfo;
}

void CScene::SetTerrain(CGameObject * pTerrain)
{
	m_pTerrainObject = pTerrain;
	m_pTerrain = m_pTerrainObject->FindComponentFromTypeID<CTerrain>();
}

CGameObject * CScene::GetTerrainObject()
{
	m_pTerrainObject->AddRef();
	return m_pTerrainObject;
}

CTerrain* CScene::GetTerrain()
{
	m_pTerrain->AddRef();
	return m_pTerrain;
}
