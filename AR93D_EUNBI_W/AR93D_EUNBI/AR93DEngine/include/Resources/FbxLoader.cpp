#include "FbxLoader.h"
#include "../Core/PathManager.h"

/*
애니메이션 관련 정보 : https://www.gamedev.net/articles/programming/graphics/skinned-mesh-animation-using-matrices-r3577
FBX Animation 관련 : https://www.gamedev.net/articles/programming/graphics/how-to-work-with-fbx-sdk-r3582/
*/

AR3D_USING

CFbxLoader::CFbxLoader() :
	m_pManager(NULL),
	m_pScene(NULL),
	m_pImporter(NULL),
	m_bMixamo(false),
	m_eLoadType(FLT_BOTH)
{
}

CFbxLoader::~CFbxLoader()
{
	Safe_Delete_VecList(m_vecMeshContainer);
	Safe_Delete_VecList(m_vecBones);
	Safe_Delete_VecList(m_vecClip);

	for (size_t i = 0; i < m_vecMaterial.size(); ++i)
	{
		Safe_Delete_VecList(m_vecMaterial[i]);
	}

	m_pScene->Destroy();

	m_pManager->Destroy();
}

const vector<PFBXBONE>* CFbxLoader::GetBones() const
{
	return &m_vecBones;
}

const vector<PFBXANIMATIONCLIP>* CFbxLoader::GetClip() const
{
	return &m_vecClip;
}

const vector<PFBXMESHCONTAINER>* CFbxLoader::GetMeshContainer() const
{
	return &m_vecMeshContainer;
}

const vector<vector<PFBXMATERIAL>>* CFbxLoader::GetMaterials() const
{
	return &m_vecMaterial;
}

bool CFbxLoader::LoadFBX(const TCHAR * pFileName, FBX_LOAD_TYPE eLoadType, const string & strPathKey)
{
	char	strFileName[MAX_PATH] = {};
	WideCharToMultiByte(CP_ACP, 0, pFileName, -1, strFileName,
		lstrlen(pFileName), 0, 0);
	string strPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	strPath += strFileName;

	return LoadFBXFullPath(strPath.c_str(), eLoadType);
}

bool CFbxLoader::LoadFBX(const char * pFileName, FBX_LOAD_TYPE eLoadType, const string & strPathKey)
{
	string strPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	strPath += pFileName;

	return LoadFBXFullPath(strPath.c_str(), eLoadType);
}

bool CFbxLoader::LoadFBXFullPath(const TCHAR * pFullPath, FBX_LOAD_TYPE eLoadType)
{
	char	strFileName[MAX_PATH] = {};
	WideCharToMultiByte(CP_ACP, 0, pFullPath, -1, strFileName,
		lstrlen(pFullPath), 0, 0);

	return LoadFBXFullPath(strFileName, eLoadType);
}

bool CFbxLoader::LoadFBXFullPath(const char * pFullPath, FBX_LOAD_TYPE eLoadType)
{
	m_eLoadType = eLoadType;

	// Fbx Manager를 생성한다.
	m_pManager = FbxManager::Create();

	// IO Setting을 만들어준다.
	FbxIOSettings*	pIoSetting = FbxIOSettings::Create(m_pManager, IOSROOT);

	m_pManager->SetIOSettings(pIoSetting);

	// Scene을 생성한다.
	m_pScene = FbxScene::Create(m_pManager, "");

	// Fbx 정보를 읽어올 Importer를 생성한다.
	m_pImporter = FbxImporter::Create(m_pManager, "");

	m_pImporter->Initialize(pFullPath, -1, m_pManager->GetIOSettings());

	m_pImporter->Import(m_pScene);

	if (m_pScene->GetGlobalSettings().GetAxisSystem() != FbxAxisSystem::Max)
		m_pScene->GetGlobalSettings().SetAxisSystem(FbxAxisSystem::Max);

	LoadSkeleton(m_pScene->GetRootNode());

	m_pScene->FillAnimStackNameArray(m_NameArr);

	LoadAnimationClip();

	Triangulate(m_pScene->GetRootNode());

	LoadMesh(m_pScene->GetRootNode());

	m_pImporter->Destroy();

	return true;
}

void CFbxLoader::Triangulate(FbxNode * pNode)
{
	FbxNodeAttribute*	pAttr = pNode->GetNodeAttribute();

	if (pAttr && (pAttr->GetAttributeType() == FbxNodeAttribute::eMesh ||
		pAttr->GetAttributeType() == FbxNodeAttribute::eNurbs ||
		pAttr->GetAttributeType() == FbxNodeAttribute::eNurbsSurface))
	{
		FbxGeometryConverter	converter(m_pManager);

		converter.Triangulate(pAttr, true);
	}

	int	iMtrlCount = pNode->GetMaterialCount();

	if (iMtrlCount > 0)
	{
		vector<PFBXMATERIAL>	vecMtrl;

		m_vecMaterial.push_back(vecMtrl);

		for (int i = 0; i < iMtrlCount; ++i)
		{
			FbxSurfaceMaterial*	pMtrl = pNode->GetMaterial(i);

			if (!pMtrl)
				continue;

			LoadMaterial(pMtrl);
		}
	}

	int	iChildCount = pNode->GetChildCount();

	for (int i = 0; i < iChildCount; ++i)
	{
		Triangulate(pNode->GetChild(i));
	}
}

bool CFbxLoader::LoadMesh(FbxNode * pNode)
{
	FbxNodeAttribute*	pAttr = pNode->GetNodeAttribute();

	if (pAttr && pAttr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		FbxMesh*	pMesh = pNode->GetMesh();

		if (pMesh)
		{
			if (m_eLoadType != FLT_ANIMATION)
				LoadMesh(pMesh);

			else
			{
				int	iSkinCount = pMesh->GetDeformerCount(FbxDeformer::eSkin);

				if (iSkinCount > 0)
				{
					FbxAMatrix	matTransform = GetTransform(pMesh->GetNode());

					for (int i = 0; i < iSkinCount; ++i)
					{
						FbxSkin*	pSkin = (FbxSkin*)pMesh->GetDeformer(i, FbxDeformer::eSkin);

						if (!pSkin)
							continue;

						FbxSkin::EType	eSkinningType = pSkin->GetSkinningType();

						if (eSkinningType == FbxSkin::eRigid ||
							eSkinningType == FbxSkin::eLinear)
						{
							// Cluster : 관절을 의미한다.
							int	iClusterCount = pSkin->GetClusterCount();

							for (int j = 0; j < iClusterCount; ++j)
							{
								FbxCluster*	pCluster = pSkin->GetCluster(j);

								if (!pCluster->GetLink())
									continue;

								int	iBoneIndex = FindBoneFromName(pCluster->GetLink()->GetName());

								LoadTimeTransform(pMesh->GetNode(), pCluster,
									matTransform, iBoneIndex);
							}
						}
					}
				}
			}
		}
	}

	int	iChildCount = pNode->GetChildCount();

	for (int i = 0; i < iChildCount; ++i)
	{
		LoadMesh(pNode->GetChild(i));
	}

	return true;
}

bool CFbxLoader::LoadMesh(FbxMesh * pMesh)
{
	PFBXMESHCONTAINER	pContainer = new FBXMESHCONTAINER;

	pContainer->bBump = false;
	pContainer->bAnimation = false;

	m_vecMeshContainer.push_back(pContainer);

	// ControlPoint 는 위치정보를 담고 있는 배열이다.
	// 이 배열의 개수는 곧 정점의 개수가 된다.
	int	iVtxCount = pMesh->GetControlPointsCount();
	FbxVector4*	pVtxPos = pMesh->GetControlPoints();

	pContainer->vecPos.resize(iVtxCount);
	pContainer->vecNormal.resize(iVtxCount);
	pContainer->vecUV.resize(iVtxCount);
	pContainer->vecTangent.resize(iVtxCount);
	pContainer->vecBinormal.resize(iVtxCount);

	for (int i = 0; i < iVtxCount; ++i)
	{
		pContainer->vecPos[i].x = pVtxPos[i].mData[0];
		pContainer->vecPos[i].y = pVtxPos[i].mData[2];
		pContainer->vecPos[i].z = pVtxPos[i].mData[1];
	}

	int	iPolygonCount = pMesh->GetPolygonCount();

	// 재질 수를 얻어온다.
	int	iMtrlCount = pMesh->GetNode()->GetMaterialCount();

	pContainer->vecIndices.resize(iMtrlCount);

	UINT	iVtxID = 0;

	// 재질 정보를 얻어온다.
	FbxGeometryElementMaterial*	pMaterial = pMesh->GetElementMaterial();
	int iCount = pMesh->GetElementMaterialCount();
	// 삼각형 수만큼 반복한다.
	for (int i = 0; i < iPolygonCount; ++i)
	{
		int	iPolygonSize = pMesh->GetPolygonSize(i);

		int	iIdx[3] = {};

		for (int j = 0; j < iPolygonSize; ++j)
		{
			// 현재 삼각형을 구성하고 있는 버텍스정보 내에서의 인덱스를
			// 구한다.
			int	iControlIndex = pMesh->GetPolygonVertex(i, j);

			iIdx[j] = iControlIndex;

			LoadNormal(pMesh, pContainer, iVtxID, iControlIndex);

			LoadUV(pMesh, pContainer, pMesh->GetTextureUVIndex(i, j),
				iControlIndex);

			LoadTangent(pMesh, pContainer, iVtxID, iControlIndex);

			LoadBinormal(pMesh, pContainer, iVtxID, iControlIndex);

			++iVtxID;
		}

		if (pMaterial)
		{
			int	iMtrlID = pMaterial->GetIndexArray().GetAt(i);

			pContainer->vecIndices[iMtrlID].push_back(iIdx[0]);
			pContainer->vecIndices[iMtrlID].push_back(iIdx[2]);
			pContainer->vecIndices[iMtrlID].push_back(iIdx[1]);
		}
	}


	// 애니메이션 정보가 있는지 판단한다.
	LoadAnimation(pMesh, pContainer);

	return true;
}

void CFbxLoader::LoadNormal(FbxMesh * pMesh,
	PFBXMESHCONTAINER pContainer, int iVtxID, int iControlIndex)
{
	FbxGeometryElementNormal*	pNormal = pMesh->GetElementNormal();

	int	iNormalIndex = iVtxID;

	if (pNormal->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
	{
		switch (pNormal->GetReferenceMode())
		{
		case FbxGeometryElement::eIndexToDirect:
			iNormalIndex = pNormal->GetIndexArray().GetAt(iVtxID);
			break;
		}
	}

	else if (pNormal->GetMappingMode() == FbxGeometryElement::eByControlPoint)
	{
		switch (pNormal->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
			iNormalIndex = iControlIndex;
			break;
		case FbxGeometryElement::eIndexToDirect:
			iNormalIndex = pNormal->GetIndexArray().GetAt(iControlIndex);
			break;
		}
	}

	FbxVector4	vNormal = pNormal->GetDirectArray().GetAt(iNormalIndex);

	pContainer->vecNormal[iControlIndex].x = vNormal.mData[0];
	pContainer->vecNormal[iControlIndex].y = vNormal.mData[2];
	pContainer->vecNormal[iControlIndex].z = vNormal.mData[1];
}

void CFbxLoader::LoadUV(FbxMesh * pMesh, PFBXMESHCONTAINER pContainer,
	int iUVID, int iControlIndex)
{
	FbxGeometryElementUV*	pUV = pMesh->GetElementUV(0);

	if (!pUV)
		return;

	int	iUVIndex = iUVID;

	if (pUV->GetMappingMode() == FbxGeometryElement::eByControlPoint)
	{
		switch (pUV->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
			iUVIndex = iControlIndex;
			break;
		case FbxGeometryElement::eIndexToDirect:
			iUVIndex = pUV->GetIndexArray().GetAt(iControlIndex);
			break;
		}
	}

	FbxVector2	vUV = pUV->GetDirectArray().GetAt(iUVIndex);

	pContainer->vecUV[iControlIndex].x = vUV.mData[0];
	pContainer->vecUV[iControlIndex].y = 1.f - vUV.mData[1];
}

void CFbxLoader::LoadTangent(FbxMesh * pMesh,
	PFBXMESHCONTAINER pContainer, int iVtxID, int iControlIndex)
{
	FbxGeometryElementTangent*	pTangent = pMesh->GetElementTangent();

	if (!pTangent)
		return;

	pContainer->bBump = true;

	int	iTangentIndex = iVtxID;

	if (pTangent->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
	{
		switch (pTangent->GetReferenceMode())
		{
		case FbxGeometryElement::eIndexToDirect:
			iTangentIndex = pTangent->GetIndexArray().GetAt(iVtxID);
			break;
		}
	}

	else if (pTangent->GetMappingMode() == FbxGeometryElement::eByControlPoint)
	{
		switch (pTangent->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
			iTangentIndex = iControlIndex;
			break;
		case FbxGeometryElement::eIndexToDirect:
			iTangentIndex = pTangent->GetIndexArray().GetAt(iControlIndex);
			break;
		}
	}

	FbxVector4	vTangent = pTangent->GetDirectArray().GetAt(iTangentIndex);

	pContainer->vecTangent[iControlIndex].x = vTangent.mData[0];
	pContainer->vecTangent[iControlIndex].y = vTangent.mData[2];
	pContainer->vecTangent[iControlIndex].z = vTangent.mData[1];
}

void CFbxLoader::LoadBinormal(FbxMesh * pMesh, PFBXMESHCONTAINER pContainer, int iVtxID, int iControlIndex)
{
	FbxGeometryElementBinormal*	pBinormal = pMesh->GetElementBinormal();

	if (!pBinormal)
		return;

	pContainer->bBump = true;

	int	iBinormalIndex = iVtxID;

	if (pBinormal->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
	{
		switch (pBinormal->GetReferenceMode())
		{
		case FbxGeometryElement::eIndexToDirect:
			iBinormalIndex = pBinormal->GetIndexArray().GetAt(iVtxID);
			break;
		}
	}

	else if (pBinormal->GetMappingMode() == FbxGeometryElement::eByControlPoint)
	{
		switch (pBinormal->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
			iBinormalIndex = iControlIndex;
			break;
		case FbxGeometryElement::eIndexToDirect:
			iBinormalIndex = pBinormal->GetIndexArray().GetAt(iControlIndex);
			break;
		}
	}

	FbxVector4	vBinormal = pBinormal->GetDirectArray().GetAt(iBinormalIndex);

	pContainer->vecBinormal[iControlIndex].x = vBinormal.mData[0];
	pContainer->vecBinormal[iControlIndex].y = vBinormal.mData[2];
	pContainer->vecBinormal[iControlIndex].z = vBinormal.mData[1];
}

void CFbxLoader::LoadMaterial(FbxSurfaceMaterial * pMtrl)
{
	PFBXMATERIAL	pMtrlInfo = new FBXMATERIAL;

	m_vecMaterial[m_vecMaterial.size() - 1].push_back(pMtrlInfo);

	pMtrlInfo->vDif = GetMaterialColor(pMtrl, FbxSurfaceMaterial::sDiffuse,
		FbxSurfaceMaterial::sDiffuseFactor);

	pMtrlInfo->vAmb = GetMaterialColor(pMtrl, FbxSurfaceMaterial::sAmbient,
		FbxSurfaceMaterial::sAmbientFactor);

	pMtrlInfo->vSpc = GetMaterialColor(pMtrl, FbxSurfaceMaterial::sSpecular,
		FbxSurfaceMaterial::sSpecularFactor);

	pMtrlInfo->vEmv = GetMaterialColor(pMtrl, FbxSurfaceMaterial::sEmissive,
		FbxSurfaceMaterial::sEmissiveFactor);

	pMtrlInfo->fSpecularPower = GetMaterialFactor(pMtrl, FbxSurfaceMaterial::sSpecularFactor);
	pMtrlInfo->fShininess = GetMaterialFactor(pMtrl, FbxSurfaceMaterial::sShininess);
	pMtrlInfo->fTransparencyFactor = GetMaterialFactor(pMtrl, FbxSurfaceMaterial::sTransparencyFactor);

	pMtrlInfo->strDifTex = GetMaterialTexture(pMtrl, FbxSurfaceMaterial::sDiffuse);
	pMtrlInfo->strBumpTex = GetMaterialTexture(pMtrl, FbxSurfaceMaterial::sNormalMap);

	if (pMtrlInfo->strBumpTex.empty())
		pMtrlInfo->strBumpTex = GetMaterialTexture(pMtrl, FbxSurfaceMaterial::sBump);

	pMtrlInfo->strSpcTex = GetMaterialTexture(pMtrl, FbxSurfaceMaterial::sSpecular);
}

DxVector4 CFbxLoader::GetMaterialColor(FbxSurfaceMaterial * pMtrl,
	const char * pPropertyName, const char * pPropertyFactorName)
{
	FbxDouble3	vResult(0, 0, 0);
	double		dFactor = 0;

	FbxProperty	tProperty = pMtrl->FindProperty(pPropertyName);
	FbxProperty	tPropertyFactor = pMtrl->FindProperty(pPropertyFactorName);

	// 유효한지 체크한다.
	if (tProperty.IsValid() && tPropertyFactor.IsValid())
	{
		vResult = tProperty.Get<FbxDouble3>();
		dFactor = tPropertyFactor.Get<FbxDouble>();

		if (dFactor != 1)
		{
			vResult[0] *= dFactor;
			vResult[1] *= dFactor;
			vResult[2] *= dFactor;
		}
	}

	return DxVector4(vResult[0], vResult[1], vResult[2], dFactor);
}

double CFbxLoader::GetMaterialFactor(FbxSurfaceMaterial * pMtrl,
	const char * pPropertyName)
{
	FbxProperty	tProperty = pMtrl->FindProperty(pPropertyName);
	double	dFactor = 0.0;

	if (tProperty.IsValid())
		dFactor = tProperty.Get<FbxDouble>();

	return dFactor;
}

string CFbxLoader::GetMaterialTexture(FbxSurfaceMaterial * pMtrl, const char * pPropertyName)
{
	FbxProperty	tProperty = pMtrl->FindProperty(pPropertyName);

	string	str = "";

	if (tProperty.IsValid())
	{
		int	iTexCount = tProperty.GetSrcObjectCount<FbxFileTexture>();

		if (iTexCount > 0)
		{
			FbxFileTexture*	pFileTex = tProperty.GetSrcObject<FbxFileTexture>(0);

			if (pFileTex)
				str = pFileTex->GetFileName();
		}
	}

	return str;
}

void CFbxLoader::LoadSkeleton(FbxNode * pNode)
{
	int	iChildCount = pNode->GetChildCount();

	for (int i = 0; i < iChildCount; ++i)
	{
		LoadSkeletonRecursive(pNode->GetChild(i), 0, 0, -1);
	}
}

void CFbxLoader::LoadSkeletonRecursive(FbxNode * pNode, int iDepth, int iIndex, int iParentIndex)
{
	FbxNodeAttribute*	pAttr = pNode->GetNodeAttribute();

	if (pAttr && pAttr->GetAttributeType() == FbxNodeAttribute::eSkeleton)
	{
		PFBXBONE	pBone = new FBXBONE;

		pBone->strName = pNode->GetName();
		pBone->iDepth = iDepth;
		pBone->iParentIndex = iParentIndex;

		m_vecBones.push_back(pBone);
	}

	int	iChildCount = pNode->GetChildCount();

	for (int i = 0; i < iChildCount; ++i)
	{
		LoadSkeletonRecursive(pNode->GetChild(i), iDepth + 1, m_vecBones.size(),
			iIndex);
	}
}

void CFbxLoader::LoadAnimationClip()
{
	int	iCount = m_NameArr.GetCount();

	if (iCount <= 0)
		return;

	FbxTime::EMode	eTimeMode = m_pScene->GetGlobalSettings().GetTimeMode();

	for (int i = 0; i < iCount; ++i)
	{
		FbxAnimStack*	pAnimStack = m_pScene->FindMember<FbxAnimStack>(m_NameArr[i]->Buffer());

		if (!pAnimStack)
			continue;

		PFBXANIMATIONCLIP	pClip = new FBXANIMATIONCLIP;

		FbxString	strName = pAnimStack->GetName();
		pClip->strName = pAnimStack->GetName();

		if (pClip->strName == "mixamo.com")
			m_bMixamo = true;

		FbxTakeInfo*	pTake = m_pScene->GetTakeInfo(strName);

		pClip->tStart = pTake->mLocalTimeSpan.GetStart();
		pClip->tEnd = pTake->mLocalTimeSpan.GetStop();
		pClip->lTimeLength = pClip->tEnd.GetFrameCount(eTimeMode) -
			pClip->tStart.GetFrameCount(eTimeMode);
		pClip->eTimeMode = eTimeMode;
		pClip->vecBoneKeyFrame.resize(m_vecBones.size());

		m_vecClip.push_back(pClip);
	}
}

void CFbxLoader::LoadAnimation(FbxMesh * pMesh,
	PFBXMESHCONTAINER pContainer)
{
	int	iSkinCount = pMesh->GetDeformerCount(FbxDeformer::eSkin);

	if (iSkinCount <= 0)
		return;

	else if (m_vecClip.empty())
		return;

	int	iCPCount = pMesh->GetControlPointsCount();

	pContainer->vecBlendWeight.resize(iCPCount);
	pContainer->vecBlendIndex.resize(iCPCount);

	pContainer->bAnimation = true;
	FbxAMatrix	matTransform = GetTransform(pMesh->GetNode());

	for (int i = 0; i < iSkinCount; ++i)
	{
		FbxSkin*	pSkin = (FbxSkin*)pMesh->GetDeformer(i, FbxDeformer::eSkin);

		if (!pSkin)
			continue;

		FbxSkin::EType	eSkinningType = pSkin->GetSkinningType();

		if (eSkinningType == FbxSkin::eRigid ||
			eSkinningType == FbxSkin::eLinear)
		{
			// Cluster : 관절을 의미한다.
			int	iClusterCount = pSkin->GetClusterCount();

			for (int j = 0; j < iClusterCount; ++j)
			{
				FbxCluster*	pCluster = pSkin->GetCluster(j);

				if (!pCluster->GetLink())
					continue;

				int	iBoneIndex = FindBoneFromName(pCluster->GetLink()->GetName());
				LoadWeightAndIndex(pCluster, iBoneIndex, pContainer);

				LoadOffsetMatrix(pCluster, matTransform, iBoneIndex, pContainer);

				m_vecBones[iBoneIndex]->matBone = matTransform;

				if (m_eLoadType == FLT_BOTH)
				{
					LoadTimeTransform(pMesh->GetNode(), pCluster,
						matTransform, iBoneIndex);
				}
			}
		}
	}

	ChangeWeightAndIndices(pContainer);
}

void CFbxLoader::LoadWeightAndIndex(FbxCluster * pCluster, int iBoneIndex, PFBXMESHCONTAINER pContainer)
{
	int	iControlIndicesCount = pCluster->GetControlPointIndicesCount();

	for (int i = 0; i < iControlIndicesCount; ++i)
	{
		FBXWEIGHT	tWeight = {};

		tWeight.iIndex = iBoneIndex;
		tWeight.dWeight = pCluster->GetControlPointWeights()[i];

		int	iClusterIndex = pCluster->GetControlPointIndices()[i];

		pContainer->mapWeights[iClusterIndex].push_back(tWeight);
	}
}

void CFbxLoader::LoadOffsetMatrix(FbxCluster * pCluster, const FbxAMatrix & matTransform, int iBoneIndex, PFBXMESHCONTAINER pContainer)
{
	FbxAMatrix	matCluster;
	FbxAMatrix	matClusterLink;

	pCluster->GetTransformMatrix(matCluster);
	pCluster->GetTransformLinkMatrix(matClusterLink);

	FbxVector4	v1 = { 1.0, 0.0, 0.0, 0.0 };
	FbxVector4	v2 = { 0.0, 0.0, 1.0, 0.0 };
	FbxVector4	v3 = { 0.0, 1.0, 0.0, 0.0 };
	FbxVector4	v4 = { 0.0, 0.0, 0.0, 1.0 };

	FbxAMatrix	matReflect;
	matReflect.mData[0] = v1;
	matReflect.mData[1] = v2;
	matReflect.mData[2] = v3;
	matReflect.mData[3] = v4;

	FbxAMatrix	matOffset;
	matOffset = matClusterLink.Inverse() * matCluster * matTransform;
	matOffset = matReflect * matOffset * matReflect;

	m_vecBones[iBoneIndex]->matOffset = matOffset;
}

void CFbxLoader::LoadTimeTransform(FbxNode * pNode,
	FbxCluster * pCluster, const FbxAMatrix & matTransform,
	int iBoneIndex)
{
	FbxVector4	v1 = { 1.0, 0.0, 0.0, 0.0 };
	FbxVector4	v2 = { 0.0, 0.0, 1.0, 0.0 };
	FbxVector4	v3 = { 0.0, 1.0, 0.0, 0.0 };
	FbxVector4	v4 = { 0.0, 0.0, 0.0, 1.0 };

	FbxAMatrix	matReflect;
	matReflect.mData[0] = v1;
	matReflect.mData[1] = v2;
	matReflect.mData[2] = v3;
	matReflect.mData[3] = v4;

	if (m_bMixamo)
	{
		vector<PFBXANIMATIONCLIP>::iterator	iter;
		vector<PFBXANIMATIONCLIP>::iterator	iterEnd = m_vecClip.end();

		for (iter = m_vecClip.begin(); iter != iterEnd;)
		{
			if ((*iter)->strName != "mixamo.com")
			{
				SAFE_DELETE((*iter));
				iter = m_vecClip.erase(iter);
				iterEnd = m_vecClip.end();
			}

			else
				++iter;
		}

		for (size_t i = 0; i < m_vecClip.size(); ++i)
		{
			FbxLongLong	Start = m_vecClip[i]->tStart.GetFrameCount(m_vecClip[i]->eTimeMode);
			FbxLongLong	End = m_vecClip[i]->tEnd.GetFrameCount(m_vecClip[i]->eTimeMode);

			m_vecClip[i]->vecBoneKeyFrame[iBoneIndex].iBoneIndex = iBoneIndex;

			for (FbxLongLong j = Start; j <= End; ++j)
			{
				FbxTime	tTime = {};

				tTime.SetFrame(j, m_vecClip[i]->eTimeMode);

				// EvaluateGlobalTransform
				FbxAMatrix	matOffset = pNode->EvaluateGlobalTransform(tTime) * matTransform;
				FbxAMatrix	matCur = matOffset.Inverse() * pCluster->GetLink()->EvaluateGlobalTransform(tTime);

				matCur = matReflect * matCur * matReflect;

				FBXKEYFRAME	tKeyFrame = {};

				tKeyFrame.dTime = tTime.GetSecondDouble();
				tKeyFrame.matTransform = matCur;

				m_vecClip[i]->vecBoneKeyFrame[iBoneIndex].vecKeyFrame.push_back(tKeyFrame);
				//m_vecBones[iBoneIndex]->vecKeyFrame.push_back(tKeyFrame);
			}
		}
	}

	else
	{
		for (size_t i = 0; i < m_vecClip.size(); ++i)
		{
			FbxLongLong	Start = m_vecClip[i]->tStart.GetFrameCount(m_vecClip[i]->eTimeMode);
			FbxLongLong	End = m_vecClip[i]->tEnd.GetFrameCount(m_vecClip[i]->eTimeMode);

			m_vecClip[i]->vecBoneKeyFrame[iBoneIndex].iBoneIndex = iBoneIndex;

			for (FbxLongLong j = Start; j <= End; ++j)
			{
				FbxTime	tTime = {};

				tTime.SetFrame(j, m_vecClip[i]->eTimeMode);

				// EvaluateGlobalTransform
				FbxAMatrix	matOffset = pNode->EvaluateGlobalTransform(tTime) * matTransform;
				FbxAMatrix	matCur = matOffset.Inverse() * pCluster->GetLink()->EvaluateGlobalTransform(tTime);

				matCur = matReflect * matCur * matReflect;

				FBXKEYFRAME	tKeyFrame = {};

				tKeyFrame.dTime = tTime.GetSecondDouble();
				tKeyFrame.matTransform = matCur;

				m_vecClip[i]->vecBoneKeyFrame[iBoneIndex].vecKeyFrame.push_back(tKeyFrame);

				//m_vecBones[iBoneIndex]->vecKeyFrame.push_back(tKeyFrame);
			}
		}
	}
}

void CFbxLoader::ChangeWeightAndIndices(
	PFBXMESHCONTAINER pContainer)
{
	unordered_map<int, vector<FBXWEIGHT>>::iterator	iter;
	unordered_map<int, vector<FBXWEIGHT>>::iterator	iterEnd = pContainer->mapWeights.end();

	for (iter = pContainer->mapWeights.begin(); iter != iterEnd; ++iter)
	{
		if (iter->second.size() > 4)
		{
			// 가중치 값에 따라 내림차순 정렬한다.
			sort(iter->second.begin(), iter->second.end(), [](const FBXWEIGHT& lhs, const FBXWEIGHT& rhs)
			{
				return lhs.dWeight > rhs.dWeight;
			});

			double	dSum = 0.0;

			for (int i = 0; i < 4; ++i)
			{
				dSum += iter->second[i].dWeight;
			}

			double	dInterpolate = 1.f - dSum;

			vector<FBXWEIGHT>::iterator	iterErase = iter->second.begin() + 4;

			iter->second.erase(iterErase, iter->second.end());
			iter->second[0].dWeight += dInterpolate;
		}

		float	fWeight[4] = {};
		int		iIndex[4] = {};

		for (int i = 0; i < iter->second.size(); ++i)
		{
			fWeight[i] = iter->second[i].dWeight;
			iIndex[i] = iter->second[i].iIndex;
		}

		DxVector4	vWeight = fWeight;
		DxVector4	vIndex = iIndex;

		pContainer->vecBlendWeight[iter->first] = vWeight;
		pContainer->vecBlendIndex[iter->first] = vIndex;
	}
}

int CFbxLoader::FindBoneFromName(const string & strName)
{
	for (size_t i = 0; i < m_vecBones.size(); ++i)
	{
		if (m_vecBones[i]->strName == strName)
			return i;
	}

	return -1;
}

FbxAMatrix CFbxLoader::GetTransform(FbxNode * pNode)
{
	const FbxVector4	vT = pNode->GetGeometricTranslation(FbxNode::eSourcePivot);
	const FbxVector4	vR = pNode->GetGeometricRotation(FbxNode::eSourcePivot);
	const FbxVector4	vS = pNode->GetGeometricScaling(FbxNode::eSourcePivot);

	return FbxAMatrix(vT, vR, vS);
}
