#pragma once

#include "../BaseObj.h"

AR3D_BEGIN

typedef struct _tagVertexBuffer
{
	ID3D11Buffer*	pBuffer;
	void*			pData;
	unsigned int	iCount;
	unsigned int	iSize;
	D3D11_PRIMITIVE_TOPOLOGY	ePrimitive;
	D3D11_USAGE		eUsage;

	_tagVertexBuffer() :
		pBuffer(NULL),
		pData(NULL),
		iCount(0),
		iSize(0),
		ePrimitive(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST),
		eUsage(D3D11_USAGE_DEFAULT)
	{
	}
}VERTEXBUFFER, *PVERTEXBUFFER;

typedef struct _tagIdx16
{
	WORD _1, _2, _3;
	_tagIdx16()
		:_1(0), _2(0), _3(0)
	{}
	_tagIdx16(WORD __1, WORD __2, WORD __3)
		: _1(__1), _2(__2), _3(__3)
	{}

}INDEX16;

typedef struct _tagIndexBuffer
{
	ID3D11Buffer*	pBuffer;
	void*			pData;
	unsigned int	iCount;
	unsigned int	iSize;
	DXGI_FORMAT		eFormat;
	D3D11_USAGE		eUsage;

	_tagIndexBuffer() :
		pBuffer(NULL),
		pData(NULL),
		iCount(0),
		iSize(0),
		eFormat(DXGI_FORMAT_R32_UINT),
		eUsage(D3D11_USAGE_DEFAULT)
	{
	}
}INDEXBUFFER, *PINDEXBUFFER;

typedef struct _tagMeshContainer
{
	PVERTEXBUFFER				pVtxBuffer;
	vector<PINDEXBUFFER>		vecIndexBuffer;
	vector<class CMaterial*>	vecMaterial;

	_tagMeshContainer() :
		pVtxBuffer(NULL)
	{
	}
}MESHCONTAINER, *PMESHCONTAINER;

class DLL CMesh :
	public CBaseObj
{
private:
	friend class CResourcesManager;

private:
	CMesh();
	~CMesh();

private:
	vector<PMESHCONTAINER>	m_vecMeshContainer;
	string				m_strKey;
	DxVector3			m_vMin;
	DxVector3			m_vMax;
	DxVector3			m_vSize;
	SPHEREINFO			m_tSphere;
	class CAnimation3D*	m_pAnimation;

public:
	DxVector3 GetMeshMin()	const;
	DxVector3 GetMeshMax()	const;
	DxVector3 GetMeshSize()	const;
	SPHEREINFO GetSphereInfo()	const;
	DxVector3 GetCenter()	const;
	float GetRadius();
	UINT GetContainerCount()	const;
	UINT GetSubsetCount(int iContainer = 0)	const;
	class CAnimation3D* CloneAnimation()	const;
	class CAnimation3D* GetAnimation()	const;

public:
	bool CreateMesh(const string& strKey, void* pVertices, unsigned int iVtxCount,
		unsigned int iVtxSize, D3D11_USAGE eVtxUsage,
		D3D11_PRIMITIVE_TOPOLOGY ePrimitive, void* pIndices = NULL,
		unsigned int iIdxCount = 0, unsigned int iIdxSize = 0,
		D3D11_USAGE eIdxUsage = D3D11_USAGE_DEFAULT,
		DXGI_FORMAT eFormat = DXGI_FORMAT_R32_UINT);
	bool CreateSphere(const string& strKey, float fRadius, UINT iNumSub,
		const DxVector4& vColor);
	bool CreateSphere(const string& strKey, float fRadius, UINT iNumSub);
	bool CreateCube(const string& strKey, float fSize);
	bool LoadMesh(const string& strKey, const wchar_t* pFileName,
		FBX_LOAD_TYPE eLoadType = FLT_BOTH, const string& strPathKey = MESH_PATH);
	bool LoadMesh(const string& strKey, const char* pFileName,
		FBX_LOAD_TYPE eLoadType = FLT_BOTH, const string& strPathKey = MESH_PATH);
	bool LoadMeshFromFullPath(const string& strKey, const wchar_t* pFullPath,
		FBX_LOAD_TYPE eLoadType = FLT_BOTH);
	bool LoadMeshFromFullPath(const string& strKey, const char* pFullPath,
		FBX_LOAD_TYPE eLoadType = FLT_BOTH);
	void Render();
	void Render(int iContainer, int iSubset);
	class CMaterial* CloneMaterial(int iContainer = 0, int iSubset = 0);
	void Save(const char* pFileName, const string& strPathKey = MESH_PATH);
	void SaveFromFullPath(const char* pFileName);
	void SaveTexture(struct _tagTexture* pTex, FILE* pFile);
	void Load(const char* pFileName, const string& strPathKey = MESH_PATH);
	void LoadFromFullPath(const char* pFileName);
	void LoadTexture(struct _tagTexture* pTex, FILE* pFile);

private:
	bool CreateVertexBuffer(void* pVertices, unsigned int iVtxCount,
		unsigned int iVtxSize, D3D11_USAGE eVtxUsage,
		D3D11_PRIMITIVE_TOPOLOGY ePrimitive,
		PMESHCONTAINER pContainer);
	bool CreateIndexBuffer(void* pIndices, unsigned int iIdxCount,
		unsigned int iIdxSize, D3D11_USAGE eIdxUsage,
		DXGI_FORMAT eFormat,
		PMESHCONTAINER pContainer);
	void SubDivide(vector<VERTEXCOLOR>* pVtx, vector<UINT>* pIdx);
	void SubDividePos(vector<VERTEXPOS>* pVtx, vector<UINT>* pIdx);
	bool ConvertFbxData(class CFbxLoader* pLoader, FBX_LOAD_TYPE eLoadType);
};

AR3D_END
