#include "ResourcesManager.h"
#include "Mesh.h"
#include "Texture.h"
#include "Sampler.h"

AR3D_USING

DEFINITION_SINGLE(CResourcesManager)

CResourcesManager::CResourcesManager()
{
}

CResourcesManager::~CResourcesManager()
{
	Safe_Release_Map(m_mapSampler);
	Safe_Release_Map(m_mapTexture);
	Safe_Release_Map(m_mapMesh);
}

bool CResourcesManager::Init()
{
	// 샘플 Color Triangle
	VERTEXCOLOR	tVtx[3] =
	{
		VERTEXCOLOR(0.f, 1.f, 0.f, 1.f, 0.f, 0.f, 1.f),
		VERTEXCOLOR(1.f, -1.f, 0.f, 0.f, 1.f, 0.f, 1.f),
		VERTEXCOLOR(-1.f, -1.f, 0.f, 0.f, 0.f, 1.f, 1.f)
	};

	UINT	iIdx[3] = { 0, 1, 2 };

	CMesh*	pMesh = CreateMesh("ColorTriangle", tVtx, 3, sizeof(VERTEXCOLOR),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		iIdx, 3, 4);

	SAFE_RELEASE(pMesh);

	VERTEXCOLOR	tVtxPyramid[5] =
	{
		VERTEXCOLOR(0.f, 0.5f, 0.f, 1.f, 0.f, 0.f, 1.f),
		VERTEXCOLOR(-0.5f, -0.5f, 0.5f, 0.f, 1.f, 0.f, 1.f),
		VERTEXCOLOR(0.5f, -0.5f, 0.5f, 0.f, 0.f, 1.f, 1.f),
		VERTEXCOLOR(-0.5f, -0.5f, -0.5f, 1.f, 0.f, 1.f, 1.f),
		VERTEXCOLOR(0.5f, -0.5f, -0.5f, 0.f, 1.f, 1.f, 1.f)
	};

	UINT	iPyramidIdx[18] =
	{
		0, 4, 3, 0, 2, 4, 0, 1, 2, 0, 3, 1, 3, 4, 1, 4, 2, 1
	};

	pMesh = CreateMesh("ColorPyramid", tVtxPyramid, 5, sizeof(VERTEXCOLOR),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		iPyramidIdx, 18, 4);

	SAFE_RELEASE(pMesh);

	pMesh = CreateSphere("ColorSphere", 0.5f, 2, DxVector4(0.f, 1.f, 0.f, 1.f));

	SAFE_RELEASE(pMesh);

	pMesh = CreateCube("PosCube", 2.f);

	SAFE_RELEASE(pMesh);

	pMesh = CreateSphere("PosSphere", 1.f, 2);

	SAFE_RELEASE(pMesh);

	VERTEXNORMALTEX	tVtxTexPyramid[8] =
	{
		VERTEXNORMALTEX(0.f, 0.5f, 0.f, 0.f, 1.f, 0.f, 0.5f, 0.f),
		VERTEXNORMALTEX(0.f, 0.5f, 0.f, 0.f, 1.f, 0.f, 1.f, 0.5f),
		VERTEXNORMALTEX(0.f, 0.5f, 0.f, 0.f, 1.f, 0.f, 0.5f, 1.f),
		VERTEXNORMALTEX(0.f, 0.5f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.5f),
		VERTEXNORMALTEX(-0.5f, -0.5f, 0.5f, 0.f, 1.f, 0.f, 0.25f, 0.25f),
		VERTEXNORMALTEX(0.5f, -0.5f, 0.5f, 0.f, 1.f, 0.f, 0.75f, 0.25f),
		VERTEXNORMALTEX(-0.5f, -0.5f, -0.5f, 0.f, 1.f, 0.f, 0.25f, 0.75f),
		VERTEXNORMALTEX(0.5f, -0.5f, -0.5f, 0.f, 1.f, 0.f, 0.75f, 0.75f)
	};

	UINT	iPyramidTexIdx[18] =
	{
		2, 7, 6, 3, 6, 4, 0, 4, 5, 1, 5, 7, 6, 7, 4, 7, 5, 4
	};

	pMesh = CreateMesh("TexNormalPyramid", tVtxTexPyramid, 8, sizeof(VERTEXNORMALTEX),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		iPyramidTexIdx, 18, 4);

	SAFE_RELEASE(pMesh);

	VERTEXPOS	tVtxPos = VERTEXPOS(0, 0, 0);

	pMesh = CreateMesh("PosMesh", &tVtxPos, 1, sizeof(VERTEXPOS),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	SAFE_RELEASE(pMesh);

	// UI Mesh
	VERTEXTEX	tUICenterVtx[4] =
	{
		VERTEXTEX(-0.5f, -0.5f, 0.f, 0.f, 0.f),
		VERTEXTEX(0.5f, -0.5f, 0.f, 1.f, 0.f),
		VERTEXTEX(-0.5f, 0.5f, 0.f, 0.f, 1.f),
		VERTEXTEX(0.5f, 0.5f, 0.f, 1.f, 1.f)
	};

	UINT	iRectIdx[6] = { 0, 1, 3, 0, 3, 2 };

	pMesh = CreateMesh("CenterOrthoRect", tUICenterVtx, 4, sizeof(VERTEXTEX),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		iRectIdx, 6, 4);

	SAFE_RELEASE(pMesh);

	VERTEXTEX	tUILTVtx[4] =
	{
		VERTEXTEX(0.f, 0.f, 0.f, 0.f, 0.f),
		VERTEXTEX(1.f, 0.f, 0.f, 1.f, 0.f),
		VERTEXTEX(0.f, 1.f, 0.f, 0.f, 1.f),
		VERTEXTEX(1.f, 1.f, 0.f, 1.f, 1.f)
	};

	pMesh = CreateMesh("UIMesh", tUILTVtx, 4, sizeof(VERTEXTEX),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		iRectIdx, 6, 4);

	SAFE_RELEASE(pMesh);

	VERTEXCOLOR	tRectLine[5] =
	{
		VERTEXCOLOR(0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 1.f),
		VERTEXCOLOR(1.f, 0.f, 0.f, 0.f, 1.f, 0.f, 1.f),
		VERTEXCOLOR(1.f, 1.f, 0.f, 0.f, 1.f, 0.f, 1.f),
		VERTEXCOLOR(0.f, 1.f, 0.f, 0.f, 1.f, 0.f, 1.f),
		VERTEXCOLOR(0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 1.f)
	};

	// 충돌처리용 렉트 LineStrip 메쉬
	pMesh = CreateMesh("RectLine", tRectLine, 5, sizeof(VERTEXCOLOR), D3D11_USAGE_DEFAULT,
		D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP);

	SAFE_RELEASE(pMesh);

	CTexture*	pTexture = LoadTexture("mjh", L"mjh.jpg");

	SAFE_RELEASE(pTexture);

	CSampler*	pSampler = CreateSampler("Linear");

	SAFE_RELEASE(pSampler);

	pSampler = CreateSampler("Point", D3D11_FILTER_MIN_MAG_MIP_POINT);

	SAFE_RELEASE(pSampler);

	return true;
}

CMesh * CResourcesManager::CreateMesh(const string & strKey, void * pVertices,
	unsigned int iVtxCount, unsigned int iVtxSize, D3D11_USAGE eVtxUsage,
	D3D11_PRIMITIVE_TOPOLOGY ePrimitive, void * pIndices,
	unsigned int iIdxCount, unsigned int iIdxSize, D3D11_USAGE eIdxUsage,
	DXGI_FORMAT eFormat)
{
	CMesh*	pMesh = FindMesh(strKey);

	if (pMesh)
		return pMesh;

	pMesh = new CMesh;

	if (!pMesh->CreateMesh(strKey, pVertices, iVtxCount, iVtxSize, eVtxUsage,
		ePrimitive, pIndices, iIdxCount, iIdxSize, eIdxUsage, eFormat))
	{
		SAFE_RELEASE(pMesh);
		return NULL;
	}

	pMesh->AddRef();

	m_mapMesh.insert(make_pair(strKey, pMesh));

	return pMesh;
}

CMesh * CResourcesManager::CreateSphere(const string & strKey, float fRadius,
	UINT iNumSub, const DxVector4& vColor)
{
	CMesh*	pMesh = FindMesh(strKey);

	if (pMesh)
		return pMesh;

	pMesh = new CMesh;

	if (!pMesh->CreateSphere(strKey, fRadius, iNumSub, vColor))
	{
		SAFE_RELEASE(pMesh);
		return NULL;
	}

	pMesh->AddRef();

	m_mapMesh.insert(make_pair(strKey, pMesh));

	return pMesh;
}

CMesh * CResourcesManager::CreateSphere(const string & strKey, float fRadius,
	UINT iNumSub)
{
	CMesh*	pMesh = FindMesh(strKey);

	if (pMesh)
		return pMesh;

	pMesh = new CMesh;

	if (!pMesh->CreateSphere(strKey, fRadius, iNumSub))
	{
		SAFE_RELEASE(pMesh);
		return NULL;
	}

	pMesh->AddRef();

	m_mapMesh.insert(make_pair(strKey, pMesh));

	return pMesh;
}

CMesh * CResourcesManager::CreateCube(const string & strKey, float fSize)
{
	CMesh*	pMesh = FindMesh(strKey);

	if (pMesh)
		return pMesh;

	pMesh = new CMesh;

	if (!pMesh->CreateCube(strKey, fSize))
	{
		SAFE_RELEASE(pMesh);
		return NULL;
	}

	pMesh->AddRef();

	m_mapMesh.insert(make_pair(strKey, pMesh));

	return pMesh;
}

CMesh* CResourcesManager::LoadMesh(const string & strKey,
	const wchar_t * pFileName, FBX_LOAD_TYPE eLoadType, const string & strPathKey)
{
	CMesh*	pMesh = FindMesh(strKey);

	if (pMesh)
		return pMesh;

	pMesh = new CMesh;

	if (!pMesh->LoadMesh(strKey, pFileName, eLoadType, strPathKey))
	{
		SAFE_RELEASE(pMesh);
		return NULL;
	}

	pMesh->AddRef();

	m_mapMesh.insert(make_pair(strKey, pMesh));

	return pMesh;
}

CMesh* CResourcesManager::LoadMesh(const string & strKey,
	const char * pFileName, FBX_LOAD_TYPE eLoadType, const string & strPathKey)
{
	CMesh*	pMesh = FindMesh(strKey);

	if (pMesh)
		return pMesh;

	pMesh = new CMesh;

	if (!pMesh->LoadMesh(strKey, pFileName, eLoadType, strPathKey))
	{
		SAFE_RELEASE(pMesh);
		return NULL;
	}

	pMesh->AddRef();

	m_mapMesh.insert(make_pair(strKey, pMesh));

	return pMesh;
}

CMesh* CResourcesManager::LoadMeshFromFullPath(const string & strKey,
	const wchar_t * pFullPath, FBX_LOAD_TYPE eLoadType)
{
	CMesh*	pMesh = FindMesh(strKey);

	if (pMesh)
		return pMesh;

	pMesh = new CMesh;

	if (!pMesh->LoadMeshFromFullPath(strKey, pFullPath, eLoadType))
	{
		SAFE_RELEASE(pMesh);
		return NULL;
	}

	pMesh->AddRef();

	m_mapMesh.insert(make_pair(strKey, pMesh));

	return pMesh;
}

CMesh* CResourcesManager::LoadMeshFromFullPath(const string & strKey,
	const char * pFullPath, FBX_LOAD_TYPE eLoadType)
{
	CMesh*	pMesh = FindMesh(strKey);

	if (pMesh)
		return pMesh;

	pMesh = new CMesh;

	if (!pMesh->LoadMeshFromFullPath(strKey, pFullPath, eLoadType))
	{
		SAFE_RELEASE(pMesh);
		return NULL;
	}

	pMesh->AddRef();

	m_mapMesh.insert(make_pair(strKey, pMesh));

	return pMesh;
}

bool CResourcesManager::EraseMesh(CMesh * pMesh)
{
	unordered_map<string, class CMesh*>::iterator	iter;
	unordered_map<string, class CMesh*>::iterator	iterEnd = m_mapMesh.end();

	for (iter = m_mapMesh.begin(); iter != iterEnd; ++iter)
	{
		if (iter->second == pMesh)
		{
			SAFE_RELEASE(iter->second);
			m_mapMesh.erase(iter);
			return true;
		}
	}
	return false;
}

CMesh * CResourcesManager::FindMesh(const string & strKey)
{
	unordered_map<string, class CMesh*>::iterator	iter = m_mapMesh.find(strKey);

	if (iter == m_mapMesh.end())
		return NULL;

	iter->second->AddRef();

	return iter->second;
}

CTexture * CResourcesManager::LoadTexture(const string & strKey, TCHAR * pFileName, const string & strPathKey)
{
	CTexture*	pTexture = FindTexture(strKey);

	if (pTexture)
		return pTexture;

	pTexture = new CTexture;

	if (!pTexture->LoadTexture(strKey, pFileName, strPathKey))
	{
		SAFE_RELEASE(pTexture);
		return NULL;
	}

	pTexture->AddRef();

	m_mapTexture.insert(make_pair(strKey, pTexture));

	return pTexture;
}

CTexture * CResourcesManager::LoadTexture(const string & strKey, char * pFileName,
	const string & strPathKey)
{
	CTexture*	pTexture = FindTexture(strKey); // 키값으로 텍스쳐를 찾는다. 

	if (pTexture)
		return pTexture; // 있으면 로드하지않고 리소스를 리턴 

	pTexture = new CTexture; //없다면 생성. 

	if (!pTexture->LoadTexture(strKey, pFileName, strPathKey))
	{
		SAFE_RELEASE(pTexture);
		return NULL;
	}

	pTexture->AddRef();

	m_mapTexture.insert(make_pair(strKey, pTexture)); // Map에 키 값과 함께 넣어준다.  

	return pTexture;
}

CTexture * CResourcesManager::LoadTextureFromFullPath(const string & strKey, const char * pFullPath)
{
	CTexture*	pTexture = FindTexture(strKey);

	if (pTexture)
		return pTexture;

	pTexture = new CTexture;

	if (!pTexture->LoadTextureFromFullPath(strKey, pFullPath))
	{
		SAFE_RELEASE(pTexture);
		return NULL;
	}

	pTexture->AddRef();

	m_mapTexture.insert(make_pair(strKey, pTexture));

	return pTexture;
}

CTexture * CResourcesManager::LoadTexture(const string & strKey, const vector<wstring>& vecFileName, const string & strPathKey)
{
	CTexture*	pTexture = FindTexture(strKey);

	if (pTexture)
		return pTexture;

	pTexture = new CTexture;

	if (!pTexture->LoadTexture(strKey, vecFileName, strPathKey))
	{
		SAFE_RELEASE(pTexture);
		return NULL;
	}

	pTexture->AddRef();

	m_mapTexture.insert(make_pair(strKey, pTexture));

	return pTexture;
}

CTexture * CResourcesManager::LoadTextureFromMultibyte(const string & strKey,
	const vector<string>& vecFileName, const string & strPathKey)
{
	CTexture*	pTexture = FindTexture(strKey);

	if (pTexture)
		return pTexture;

	pTexture = new CTexture;

	if (!pTexture->LoadTextureFromMultibyte(strKey, vecFileName, strPathKey))
	{
		SAFE_RELEASE(pTexture);
		return NULL;
	}

	pTexture->AddRef();

	m_mapTexture.insert(make_pair(strKey, pTexture));

	return pTexture;
}

CTexture * CResourcesManager::LoadTextureFromFullPath(const string & strKey,
	const vector<string>& vecFullPath)
{
	CTexture*	pTexture = FindTexture(strKey);

	if (pTexture)
		return pTexture;

	pTexture = new CTexture;

	if (!pTexture->LoadTextureFromFullPath(strKey, vecFullPath))
	{
		SAFE_RELEASE(pTexture);
		return NULL;
	}

	pTexture->AddRef();

	m_mapTexture.insert(make_pair(strKey, pTexture));

	return pTexture;
}

CTexture * CResourcesManager::CreateTexture(
	const string & strKey, unsigned int iWidth,
	unsigned int iHeight, unsigned int iArrSize,
	DXGI_FORMAT eFmt, D3D11_USAGE eUsage,
	D3D11_BIND_FLAG eBindFlag, int iCpuFlag)
{
	CTexture*	pTexture = FindTexture(strKey);

	if (pTexture)
		return pTexture;

	pTexture = new CTexture;

	if (!pTexture->CreateResource(strKey, iWidth, iHeight,
		iArrSize, eFmt, eUsage, eBindFlag, iCpuFlag))
	{
		SAFE_RELEASE(pTexture);
		return NULL;
	}

	pTexture->AddRef();

	m_mapTexture.insert(make_pair(strKey, pTexture));

	return pTexture;
}

CTexture * CResourcesManager::FindTexture(const string & strKey)
{
	unordered_map<string, class CTexture*>::iterator	iter = m_mapTexture.find(strKey);

	if (iter == m_mapTexture.end())
		return NULL;

	iter->second->AddRef();

	return iter->second;
}

CSampler * CResourcesManager::CreateSampler(const string & strKey, D3D11_FILTER eFilter, D3D11_TEXTURE_ADDRESS_MODE eAddrU, D3D11_TEXTURE_ADDRESS_MODE eAddrV, D3D11_TEXTURE_ADDRESS_MODE eAddrW)
{
	CSampler*	pSampler = FindSampler(strKey);

	if (pSampler)
		return pSampler;

	pSampler = new CSampler;

	if (!pSampler->CreateSampler(strKey, eFilter, eAddrU, eAddrV, eAddrW))
	{
		SAFE_RELEASE(pSampler);
		return NULL;
	}

	pSampler->AddRef();

	m_mapSampler.insert(make_pair(strKey, pSampler));

	return pSampler;
}

CSampler * CResourcesManager::FindSampler(const string & strKey)
{
	unordered_map<string, class CSampler*>::iterator	iter = m_mapSampler.find(strKey);

	if (iter == m_mapSampler.end())
		return NULL;

	iter->second->AddRef();

	return iter->second;
}



CTexture * CResourcesManager::ReloadTexture(const string & strKey, const vector<wstring>& vecFileName, const string & strPathKey)
{
	CTexture*	pTexture = FindTexture(strKey);

	if (pTexture)
	{
		m_mapTexture.erase(strKey);
		SAFE_RELEASE(pTexture);
	}

	pTexture = new CTexture;

	if (!pTexture->LoadTexture(strKey, vecFileName, strPathKey))
	{
		SAFE_RELEASE(pTexture);
		return NULL;
	}

	pTexture->AddRef();

	m_mapTexture.insert(make_pair(strKey, pTexture));

	return pTexture;
}
