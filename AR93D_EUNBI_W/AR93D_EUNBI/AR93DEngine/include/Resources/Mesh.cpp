#include "Mesh.h"
#include "../Device.h"
#include "FbxLoader.h"
#include "../Component/Material.h"
#include "../Component/Animation3D.h"
#include "../Core/PathManager.h"
#include "Texture.h"
#include "Sampler.h"
#include "ResourcesManager.h"

AR3D_USING

CMesh::CMesh() :
	m_pAnimation(NULL)
{
}

CMesh::~CMesh()
{
	SAFE_RELEASE(m_pAnimation);

	for (size_t i = 0; i < m_vecMeshContainer.size(); ++i)
	{
		SAFE_DELETE_ARRAY(m_vecMeshContainer[i]->pVtxBuffer->pData);
		SAFE_RELEASE(m_vecMeshContainer[i]->pVtxBuffer->pBuffer);
		SAFE_DELETE(m_vecMeshContainer[i]->pVtxBuffer);

		Safe_Release_VecList(m_vecMeshContainer[i]->vecMaterial);

		for (size_t j = 0; j < m_vecMeshContainer[i]->vecIndexBuffer.size(); ++j)
		{
			SAFE_DELETE_ARRAY(m_vecMeshContainer[i]->vecIndexBuffer[j]->pData);
			SAFE_RELEASE(m_vecMeshContainer[i]->vecIndexBuffer[j]->pBuffer);
		}

		Safe_Delete_VecList(m_vecMeshContainer[i]->vecIndexBuffer);
	}

	Safe_Delete_VecList(m_vecMeshContainer);
}

DxVector3 CMesh::GetMeshMin() const
{
	return m_vMin;
}

DxVector3 CMesh::GetMeshMax() const
{
	return m_vMax;
}

DxVector3 CMesh::GetMeshSize() const
{
	return m_vSize;
}

SPHEREINFO CMesh::GetSphereInfo() const
{
	return m_tSphere;
}

DxVector3 CMesh::GetCenter() const
{
	return m_tSphere.vCenter;
}

float CMesh::GetRadius()
{
	if (m_tSphere.fRadius <= 0)
	{
		m_vSize = m_vMax - m_vMin;
		m_tSphere.vCenter = (m_vMin + m_vMax) / 2.f;
		m_tSphere.fRadius = m_vSize.Length() / 2.f;
	}
	return m_tSphere.fRadius;
}

UINT CMesh::GetContainerCount() const
{
	return m_vecMeshContainer.size();
}

UINT CMesh::GetSubsetCount(int iContainer) const
{
	return m_vecMeshContainer[iContainer]->vecIndexBuffer.size();
}

CAnimation3D * CMesh::CloneAnimation() const
{
	if (!m_pAnimation)
		return NULL;

	return m_pAnimation->Clone();
}

CAnimation3D * CMesh::GetAnimation() const
{
	if (!m_pAnimation)
		return NULL;

	m_pAnimation->AddRef();
	return m_pAnimation;
}

bool CMesh::CreateMesh(const string & strKey, void * pVertices,
	unsigned int iVtxCount, unsigned int iVtxSize, D3D11_USAGE eVtxUsage,
	D3D11_PRIMITIVE_TOPOLOGY ePrimitive, void * pIndices,
	unsigned int iIdxCount, unsigned int iIdxSize, D3D11_USAGE eIdxUsage,
	DXGI_FORMAT eFormat)
{
	m_strKey = strKey;

	PMESHCONTAINER	pContainer = new MESHCONTAINER;

	m_vecMeshContainer.push_back(pContainer);

	if (!CreateVertexBuffer(pVertices, iVtxCount, iVtxSize, eVtxUsage,
		ePrimitive, pContainer))
		return false;

	if (pIndices)
	{
		if (!CreateIndexBuffer(pIndices, iIdxCount, iIdxSize, eIdxUsage,
			eFormat, pContainer))
			return false;
	}

	m_vSize = m_vMax - m_vMin;
	m_tSphere.vCenter = (m_vMin + m_vMax) / 2.f;
	m_tSphere.fRadius = m_vSize.Length() / 2.f;

	return true;
}

bool CMesh::CreateSphere(const string & strKey, float fRadius, UINT iNumSub,
	const DxVector4& vColor)
{
	vector<VERTEXCOLOR>	vecVtx;
	vector<UINT>		vecIdx;

	// Put a cap on the number of subdivisions.
	iNumSub = min(iNumSub, 5u);

	// Approximate a sphere by tessellating an icosahedron.

	const float X = 0.850651f;
	const float Z = 0.850651f;

	XMFLOAT3 pos[12] =
	{
		XMFLOAT3(-X, 0.0f, Z),  XMFLOAT3(X, 0.0f, Z),
		XMFLOAT3(-X, 0.0f, -Z), XMFLOAT3(X, 0.0f, -Z),
		XMFLOAT3(0.0f, Z, X),   XMFLOAT3(0.0f, Z, -X),
		XMFLOAT3(0.0f, -Z, X),  XMFLOAT3(0.0f, -Z, -X),
		XMFLOAT3(Z, X, 0.0f),   XMFLOAT3(-Z, X, 0.0f),
		XMFLOAT3(Z, -X, 0.0f),  XMFLOAT3(-Z, -X, 0.0f)
	};

	DWORD k[60] =
	{
		1,4,0,  4,9,0,  4,5,9,  8,5,4,  1,8,4,
		1,10,8, 10,3,8, 8,3,5,  3,2,5,  3,7,2,
		3,10,7, 10,6,7, 6,11,7, 6,0,11, 6,1,0,
		10,1,6, 11,0,9, 2,11,9, 5,2,9,  11,2,7
	};

	vecVtx.resize(12);
	vecIdx.resize(60);

	for (UINT i = 0; i < 12; ++i)
		vecVtx[i].vPos = pos[i];

	for (UINT i = 0; i < 60; ++i)
		vecIdx[i] = k[i];

	for (UINT i = 0; i < iNumSub; ++i)
		SubDivide(&vecVtx, &vecIdx);

	// Project vertices onto sphere and scale.
	for (UINT i = 0; i < vecVtx.size(); ++i)
	{
		// Project onto unit sphere.
		DxVector3 n = vecVtx[i].vPos.Normalize();

		// Project onto sphere.
		DxVector3 p = n * fRadius;

		vecVtx[i].vPos = p;
		vecVtx[i].vColor = vColor;
	}

	PMESHCONTAINER	pContainer = new MESHCONTAINER;

	m_vecMeshContainer.push_back(pContainer);

	if (!CreateVertexBuffer(&vecVtx[0], vecVtx.size(), sizeof(VERTEXCOLOR),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		pContainer))
		return false;

	if (!CreateIndexBuffer(&vecIdx[0], vecIdx.size(), sizeof(UINT),
		D3D11_USAGE_DEFAULT, DXGI_FORMAT_R32_UINT,
		pContainer))
		return false;

	m_vSize = m_vMax - m_vMin;
	m_tSphere.vCenter = (m_vMin + m_vMax) / 2.f;
	m_tSphere.fRadius = m_vSize.Length() / 2.f;

	return true;
}

bool CMesh::CreateSphere(const string & strKey, float fRadius, UINT iNumSub)
{
	vector<VERTEXPOS>	vecVtx;
	vector<UINT>		vecIdx;

	// Put a cap on the number of subdivisions.
	iNumSub = min(iNumSub, 5u);

	// Approximate a sphere by tessellating an icosahedron.

	const float X = 0.525731f;
	const float Z = 0.850651f;

	XMFLOAT3 pos[12] =
	{
		XMFLOAT3(-X, 0.0f, Z),  XMFLOAT3(X, 0.0f, Z),
		XMFLOAT3(-X, 0.0f, -Z), XMFLOAT3(X, 0.0f, -Z),
		XMFLOAT3(0.0f, Z, X),   XMFLOAT3(0.0f, Z, -X),
		XMFLOAT3(0.0f, -Z, X),  XMFLOAT3(0.0f, -Z, -X),
		XMFLOAT3(Z, X, 0.0f),   XMFLOAT3(-Z, X, 0.0f),
		XMFLOAT3(Z, -X, 0.0f),  XMFLOAT3(-Z, -X, 0.0f)
	};

	DWORD k[60] =
	{
		1,4,0,  4,9,0,  4,5,9,  8,5,4,  1,8,4,
		1,10,8, 10,3,8, 8,3,5,  3,2,5,  3,7,2,
		3,10,7, 10,6,7, 6,11,7, 6,0,11, 6,1,0,
		10,1,6, 11,0,9, 2,11,9, 5,2,9,  11,2,7
	};

	vecVtx.resize(12);
	vecIdx.resize(60);

	for (UINT i = 0; i < 12; ++i)
		vecVtx[i].vPos = pos[i];

	for (UINT i = 0; i < 60; ++i)
		vecIdx[i] = k[i];

	for (UINT i = 0; i < iNumSub; ++i)
		SubDividePos(&vecVtx, &vecIdx);

	// Project vertices onto sphere and scale.
	for (UINT i = 0; i < vecVtx.size(); ++i)
	{
		// Project onto unit sphere.
		DxVector3 n = vecVtx[i].vPos.Normalize();

		// Project onto sphere.
		DxVector3 p = n * fRadius;

		vecVtx[i].vPos = p;
	}

	PMESHCONTAINER	pContainer = new MESHCONTAINER;

	m_vecMeshContainer.push_back(pContainer);

	if (!CreateVertexBuffer(&vecVtx[0], vecVtx.size(), sizeof(VERTEXPOS),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		pContainer))
		return false;

	if (!CreateIndexBuffer(&vecIdx[0], vecIdx.size(), sizeof(UINT),
		D3D11_USAGE_DEFAULT, DXGI_FORMAT_R32_UINT,
		pContainer))
		return false;

	m_vSize = m_vMax - m_vMin;
	m_tSphere.vCenter = (m_vMin + m_vMax) / 2.f;
	m_tSphere.fRadius = m_vSize.Length() / 2.f;

	return true;
}

bool CMesh::CreateCube(const string & strKey, float fSize)
{
	vector<VERTEXTEX>	vecVtx;
	vector<INDEX16>		vecIdx;

	vecVtx.resize(24);
	vecIdx.resize(12);

	// Approximate a sphere by tessellating an icosahedron.
	vecVtx[0].vPos = DxVector3(-fSize, fSize, -fSize);
	vecVtx[0].vUV = DxVector2(0.f, 0.f);

	vecVtx[1].vPos = DxVector3(-fSize, fSize, fSize);
	vecVtx[1].vUV = DxVector2(0.f, 0.f);

	vecVtx[2].vPos = DxVector3(fSize, fSize, fSize);
	vecVtx[2].vUV = DxVector2(0.f, 0.f);

	vecVtx[3].vPos = DxVector3(fSize, fSize, -fSize);
	vecVtx[3].vUV = DxVector2(0.f, 0.f);

	vecIdx[0]._1 = 0; vecIdx[0]._2 = 1; vecIdx[0]._3 = 3;
	vecIdx[1]._1 = 1; vecIdx[1]._2 = 2; vecIdx[1]._3 = 3;

	 //아랫 면
	vecVtx[4].vPos = DxVector3(-fSize, -fSize, -fSize);
	vecVtx[4].vUV = DxVector2(0.f, 0.f);

	vecVtx[5].vPos = DxVector3(-fSize, -fSize, fSize);
	vecVtx[5].vUV = DxVector2(0.f, 0.f);

	vecVtx[6].vPos = DxVector3(fSize, -fSize, fSize);
	vecVtx[6].vUV = DxVector2(0.f, 0.f);

	vecVtx[7].vPos = DxVector3(fSize, -fSize, -fSize);
	vecVtx[7].vUV = DxVector2(0.f, 0.f);

	vecIdx[2]._1 = 4; vecIdx[2]._2 = 5; vecIdx[2]._3 = 7;
	vecIdx[3]._1 = 5; vecIdx[3]._2 = 6; vecIdx[3]._3 = 7;

	 //왼쪽 면
	vecVtx[8].vPos = DxVector3(-fSize, -fSize, fSize);
	vecVtx[8].vUV = DxVector2(0.f, 0.f);

	vecVtx[9].vPos = DxVector3(-fSize, fSize, fSize);
	vecVtx[9].vUV = DxVector2(0.f, 0.f);

	vecVtx[10].vPos = DxVector3(-fSize, fSize, -fSize);
	vecVtx[10].vUV = DxVector2(0.f, 0.f);

	vecVtx[11].vPos = DxVector3(-fSize, -fSize, -fSize);
	vecVtx[11].vUV = DxVector2(0.f, 0.f);

	vecIdx[4]._1 = 9; vecIdx[4]._2 = 10; vecIdx[4]._3 = 11;
	vecIdx[5]._1 = 9; vecIdx[5]._2 = 11; vecIdx[5]._3 = 8;

	// 오른쪽 면
	vecVtx[12].vPos = DxVector3(fSize, -fSize, fSize);
	vecVtx[12].vUV = DxVector2(0.f, 0.f);

	vecVtx[13].vPos = DxVector3(fSize, fSize, fSize);
	vecVtx[13].vUV = DxVector2(0.f, 0.f);

	vecVtx[14].vPos = DxVector3(fSize, fSize, -fSize);
	vecVtx[14].vUV = DxVector2(0.f, 0.f);

	vecVtx[15].vPos = DxVector3(fSize, -fSize, -fSize);
	vecVtx[15].vUV = DxVector2(0.f, 0.f);

	vecIdx[6]._1 = 12; vecIdx[6]._2 = 14; vecIdx[6]._3 = 13;
	vecIdx[7]._1 = 12; vecIdx[7]._2 = 15; vecIdx[7]._3 = 14;

	// 전면
	vecVtx[16].vPos = DxVector3(-fSize, -fSize, -fSize);
	vecVtx[16].vUV = DxVector2(0.f, 0.f);

	vecVtx[17].vPos = DxVector3(-fSize, fSize, -fSize);
	vecVtx[17].vUV = DxVector2(0.f, 0.f);

	vecVtx[18].vPos = DxVector3(fSize, fSize, -fSize);
	vecVtx[18].vUV = DxVector2(0.f, 0.f);

	vecVtx[19].vPos = DxVector3(fSize, -fSize, -fSize);
	vecVtx[19].vUV = DxVector2(0.f, 0.f);

	vecIdx[8]._1 = 16; vecIdx[8]._2 = 17; vecIdx[8]._3 = 19;
	vecIdx[9]._1 = 17; vecIdx[9]._2 = 18; vecIdx[9]._3 = 19;

	// 후면
	vecVtx[20].vPos = DxVector3(-fSize, -fSize, fSize);
	vecVtx[20].vUV = DxVector2(0.f, 0.f);

	vecVtx[21].vPos = DxVector3(-fSize, fSize, fSize);
	vecVtx[21].vUV = DxVector2(0.f, 0.f);

	vecVtx[22].vPos = DxVector3(fSize, fSize, fSize);
	vecVtx[22].vUV = DxVector2(0.f, 0.f);

	vecVtx[23].vPos = DxVector3(fSize, -fSize, fSize);
	vecVtx[23].vUV = DxVector2(0.f, 0.f);

	vecIdx[10]._1 = 20; vecIdx[10]._2 = 23; vecIdx[10]._3 = 22;
	vecIdx[11]._1 = 20; vecIdx[11]._2 = 22; vecIdx[11]._3 = 21;

	PMESHCONTAINER	pContainer = new MESHCONTAINER;

	m_vecMeshContainer.push_back(pContainer);

	if (!CreateVertexBuffer(&vecVtx[0], vecVtx.size(), sizeof(VERTEXTEX),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		pContainer))
		return false;

	if (!CreateIndexBuffer(&vecIdx[0], vecIdx.size()*3, sizeof(WORD),
		D3D11_USAGE_DEFAULT, DXGI_FORMAT_R16_UINT,
		pContainer))
		return false;

	m_vSize = DxVector3( fSize, fSize, fSize);

	return true;
}

bool CMesh::LoadMesh(const string & strKey, const wchar_t * pFileName,
	FBX_LOAD_TYPE eLoadType, const string & strPathKey)
{
	char	strFileName[MAX_PATH] = {};
	WideCharToMultiByte(CP_ACP, 0, pFileName, -1,
		strFileName, lstrlen(pFileName), 0, 0);

	char	strExt[_MAX_EXT] = {};

	_splitpath_s(strFileName, 0, 0, 0, 0, 0, 0, strExt, _MAX_EXT);

	_strupr(strExt);

	if (strcmp(strExt, ".FBX") == 0)
	{
		CFbxLoader	loader;
		if (!loader.LoadFBX(pFileName, eLoadType, strPathKey))
			return false;

		return ConvertFbxData(&loader, eLoadType);
	}

	Load(strFileName);

	return true;
}

bool CMesh::LoadMesh(const string & strKey, const char * pFileName,
	FBX_LOAD_TYPE eLoadType, const string & strPathKey)
{
	char	strExt[_MAX_EXT] = {};

	_splitpath_s(pFileName, 0, 0, 0, 0, 0, 0, strExt, _MAX_EXT);

	_strupr(strExt);

	if (strcmp(strExt, ".FBX") == 0)
	{
		CFbxLoader	loader;
		if (!loader.LoadFBX(pFileName, eLoadType, strPathKey))
			return false;

		return ConvertFbxData(&loader, eLoadType);
	}

	Load(pFileName);

	return true;
}

bool CMesh::LoadMeshFromFullPath(const string & strfuvKey,
	const wchar_t * pFullPath, FBX_LOAD_TYPE eLoadType)
{
	char	strFullPath[MAX_PATH] = {};
	WideCharToMultiByte(CP_ACP, 0, pFullPath, -1,
		strFullPath, lstrlen(pFullPath), 0, 0);

	char	strExt[_MAX_EXT] = {};

	_splitpath_s(strFullPath, 0, 0, 0, 0, 0, 0, strExt, _MAX_EXT);

	_strupr(strExt);

	if (strcmp(strExt, ".FBX") == 0)
	{
		CFbxLoader	loader;
		if (!loader.LoadFBXFullPath(pFullPath, eLoadType))
			return false;

		return ConvertFbxData(&loader, eLoadType);
	}

	//Load(strFullPath);
	LoadFromFullPath(strFullPath);

	return true;
}

bool CMesh::LoadMeshFromFullPath(const string & strKey,
	const char * pFullPath, FBX_LOAD_TYPE eLoadType)
{
	char	strExt[_MAX_EXT] = {};

	_splitpath_s(pFullPath, 0, 0, 0, 0, 0, 0, strExt, _MAX_EXT);

	_strupr(strExt);

	if (strcmp(strExt, ".FBX") == 0)
	{
		CFbxLoader	loader;
		if (!loader.LoadFBXFullPath(pFullPath, eLoadType))
			return false;

		return ConvertFbxData(&loader, eLoadType);
	}

	Load(pFullPath);

	return true;
}

void CMesh::Render()
{
	for (size_t i = 0; i < m_vecMeshContainer.size(); ++i)
	{
		PVERTEXBUFFER	pVB = m_vecMeshContainer[i]->pVtxBuffer;
		UINT	iStride = pVB->iSize;
		UINT	iOffset = 0;
		CONTEXT->IASetVertexBuffers(0, 1, &pVB->pBuffer, &iStride, &iOffset);
		CONTEXT->IASetPrimitiveTopology(pVB->ePrimitive);

		if (!m_vecMeshContainer[i]->vecIndexBuffer.empty())
		{
			for (size_t j = 0; j < m_vecMeshContainer[i]->vecIndexBuffer.size(); ++j)
			{
				PINDEXBUFFER	pIB = m_vecMeshContainer[i]->vecIndexBuffer[j];
				CONTEXT->IASetIndexBuffer(pIB->pBuffer, pIB->eFormat, 0);
				CONTEXT->DrawIndexed(pIB->iCount, 0, 0);
			}
		}

		else
		{
			CONTEXT->Draw(pVB->iCount, 0);
		}
	}
}

void CMesh::Render(int iContainer, int iSubset)
{
	PVERTEXBUFFER	pVB = m_vecMeshContainer[iContainer]->pVtxBuffer;
	UINT	iStride = pVB->iSize;
	UINT	iOffset = 0;
	CONTEXT->IASetVertexBuffers(0, 1, &pVB->pBuffer, &iStride, &iOffset);
	CONTEXT->IASetPrimitiveTopology(pVB->ePrimitive);

	if (!m_vecMeshContainer[iContainer]->vecIndexBuffer.empty())
	{
		PINDEXBUFFER	pIB = m_vecMeshContainer[iContainer]->vecIndexBuffer[iSubset];
		CONTEXT->IASetIndexBuffer(pIB->pBuffer, pIB->eFormat, 0);
		CONTEXT->DrawIndexed(pIB->iCount, 0, 0);
	}

	else
	{
		CONTEXT->Draw(pVB->iCount, 0);
	}
}

CMaterial * CMesh::CloneMaterial(int iContainer, int iSubset)
{
	if (iContainer < 0 || iContainer >= m_vecMeshContainer.size())
		return NULL;

	PMESHCONTAINER	pContainer = m_vecMeshContainer[iContainer];

	if (iSubset < 0 || iSubset >= pContainer->vecMaterial.size())
		return NULL;

	return pContainer->vecMaterial[iSubset]->Clone();
}

void CMesh::Save(const char * pFileName, const string & strPathKey)
{
	// 전체 경로를 만들어준다.
	const char*	pPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	string	strPath;
	if (pPath)
		strPath = pPath;

	strPath += pFileName;

	return SaveFromFullPath(strPath.c_str());
}

void CMesh::SaveFromFullPath(const char * pFileName)
{
	FILE*	pFile = NULL;

	fopen_s(&pFile, pFileName, "wb");

	if (!pFile)
		return;

	// 컨테이너 수를 저장한다.
	int	iCount = m_vecMeshContainer.size();

	fwrite(&iCount, 4, 1, pFile);

	for (int i = 0; i < iCount; ++i)
	{
		// 정점 수를 저장한다.
		fwrite(&m_vecMeshContainer[i]->pVtxBuffer->iCount,
			4, 1, pFile);
		// 정점 하나의 크기를 저장한다.
		fwrite(&m_vecMeshContainer[i]->pVtxBuffer->iSize,
			4, 1, pFile);
		// 위상구조를 저장한다.
		fwrite(&m_vecMeshContainer[i]->pVtxBuffer->ePrimitive,
			4, 1, pFile);
		// 용도를 저장한다.
		fwrite(&m_vecMeshContainer[i]->pVtxBuffer->eUsage,
			4, 1, pFile);

		// 정점정보를 저장한다.
		fwrite(m_vecMeshContainer[i]->pVtxBuffer->pData,
			m_vecMeshContainer[i]->pVtxBuffer->iSize,
			m_vecMeshContainer[i]->pVtxBuffer->iCount,
			pFile);

		// 서브셋 수를 저장한다.
		int	iSubset = m_vecMeshContainer[i]->vecIndexBuffer.size();
		fwrite(&iSubset, 4, 1, pFile);

		for (int j = 0; j < iSubset; ++j)
		{
			// 인덱스 수를 저장한다.
			fwrite(&m_vecMeshContainer[i]->vecIndexBuffer[j]->iCount,
				4, 1, pFile);
			// 인덱스 크기를 저장한다.
			fwrite(&m_vecMeshContainer[i]->vecIndexBuffer[j]->iSize,
				4, 1, pFile);
			// 인덱스 포멧을 저장한다.
			fwrite(&m_vecMeshContainer[i]->vecIndexBuffer[j]->eFormat,
				4, 1, pFile);
			// 인덱스 용도를 저장한다.
			fwrite(&m_vecMeshContainer[i]->vecIndexBuffer[j]->eUsage,
				4, 1, pFile);

			// 현재 서브셋의 인덱스 정보를 저장한다.
			fwrite(m_vecMeshContainer[i]->vecIndexBuffer[j]->pData,
				m_vecMeshContainer[i]->vecIndexBuffer[j]->iSize,
				m_vecMeshContainer[i]->vecIndexBuffer[j]->iCount,
				pFile);
		}

		// 재질 정보를 저장한다.
		int	iMtrlCount = m_vecMeshContainer[i]->vecMaterial.size();
		fwrite(&iMtrlCount, 4, 1, pFile);

		for (int j = 0; j < iMtrlCount; ++j)
		{
			MATERIALINFO	tMtrlInfo = m_vecMeshContainer[i]->vecMaterial[j]->GetMaterialInfo();
			SKININFO	tBaseSkin = m_vecMeshContainer[i]->vecMaterial[j]->GetBaseSkin();

			// 재질 색상정보를 저장한다.
			fwrite(&tMtrlInfo, sizeof(MATERIALINFO), 1, pFile);

			bool	bTexture = false;

			if (tBaseSkin.pDiffuse)
			{
				bTexture = true;
				fwrite(&bTexture, 1, 1, pFile);
				SaveTexture(tBaseSkin.pDiffuse, pFile);
			}

			else
				fwrite(&bTexture, 1, 1, pFile);

			bTexture = false;

			if (tBaseSkin.pNormal)
			{
				bTexture = true;
				fwrite(&bTexture, 1, 1, pFile);
				SaveTexture(tBaseSkin.pNormal, pFile);
			}

			else
				fwrite(&bTexture, 1, 1, pFile);

			bTexture = false;

			if (tBaseSkin.pSpecular)
			{
				bTexture = true;
				fwrite(&bTexture, 1, 1, pFile);
				SaveTexture(tBaseSkin.pSpecular, pFile);
			}

			else
				fwrite(&bTexture, 1, 1, pFile);
			//m_vecMeshContainer[i]->vecMaterial[j]->Save(pFile);
		}
	}

	fclose(pFile);

	/*if (m_pAnimation)
	{
	char	strAniPath[MAX_PATH] = {};
	memcpy(strAniPath, pFileName, strlen(pFileName));

	memset(strAniPath + (strlen(pFileName) - 3), 0, 3);
	strcat_s(strAniPath, "anm");

	m_pAnimation->SaveFromFullPath(strAniPath);
	}*/

}

void CMesh::SaveTexture(_tagTexture* pTex, FILE* pFile)
{
	string	strKey = pTex->pTexture->GetKey();
	wstring pFullPath = pTex->pTexture->GetFullPath().c_str();

	wchar_t	strPath[MAX_PATH] = {};

	int	iCount = 0;
	for (int i = pFullPath.length() - 1; i >= 0; --i)
	{
		if (pFullPath[i] == '\\' || pFullPath[i] == '/')
			++iCount;

		if (iCount == 2)
		{
			memcpy(strPath, pFullPath.c_str() + (i + 1),
				sizeof(wchar_t) * (MAX_PATH - (i + 1)));
			break;
		}
	}

	int	iLength = strKey.length();

	fwrite(&iLength, 4, 1, pFile);
	fwrite(strKey.c_str(), 1, iLength, pFile);

	iLength = lstrlen(strPath);
	fwrite(&iLength, 4, 1, pFile);
	fwrite(strPath, 2, iLength, pFile);

	// 패스 키 저장
	iLength = strlen(MESH_PATH);
	fwrite(&iLength, 4, 1, pFile);
	fwrite(MESH_PATH, 1, iLength, pFile);

	// 레지스터 저장
	fwrite(&pTex->iTextureRegister, 4, 1, pFile);

	strKey = pTex->pSampler->GetKey();
	iLength = strKey.length();

	fwrite(&iLength, 4, 1, pFile);
	fwrite(strKey.c_str(), 1, iLength, pFile);

	fwrite(&pTex->iSamplerRegister, 4, 1, pFile);
	fwrite(&pTex->iShaderConstantType, 4, 1, pFile);
}

void CMesh::Load(const char * pFileName, const string & strPathKey)
{
	// 전체 경로를 만들어준다.
	const char*	pPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	string	strPath;
	if (pPath)
		strPath = pPath;

	strPath += pFileName;

	return LoadFromFullPath(strPath.c_str());
}

void CMesh::LoadFromFullPath(const char * pFileName)
{
	FILE*	pFile = NULL;

	fopen_s(&pFile, pFileName, "rb");

	if (!pFile)
		return;

	for (size_t i = 0; i < m_vecMeshContainer.size(); ++i)
	{
		SAFE_DELETE_ARRAY(m_vecMeshContainer[i]->pVtxBuffer->pData);
		SAFE_RELEASE(m_vecMeshContainer[i]->pVtxBuffer->pBuffer);
		SAFE_DELETE(m_vecMeshContainer[i]->pVtxBuffer);

		Safe_Release_VecList(m_vecMeshContainer[i]->vecMaterial);

		for (size_t j = 0; j < m_vecMeshContainer[i]->vecIndexBuffer.size(); ++j)
		{
			SAFE_DELETE_ARRAY(m_vecMeshContainer[i]->vecIndexBuffer[j]->pData);
			SAFE_RELEASE(m_vecMeshContainer[i]->vecIndexBuffer[j]->pBuffer);
		}

		Safe_Delete_VecList(m_vecMeshContainer[i]->vecIndexBuffer);
	}

	Safe_Delete_VecList(m_vecMeshContainer);

	// 컨테이너 수를 저장한다.
	int	iCount = 0;

	fread(&iCount, 4, 1, pFile);

	for (int i = 0; i < iCount; ++i)
	{
		PMESHCONTAINER	pContainer = new MESHCONTAINER;

		int	iSize, iVtxCount;
		D3D11_USAGE	eUsage;
		D3D11_PRIMITIVE_TOPOLOGY	ePrimitive;

		// 정점 수를 저장한다.
		fread(&iVtxCount,
			4, 1, pFile);
		// 정점 하나의 크기를 저장한다.
		fread(&iSize,
			4, 1, pFile);
		// 위상구조를 저장한다.
		fread(&ePrimitive,
			4, 1, pFile);
		// 용도를 저장한다.
		fread(&eUsage,
			4, 1, pFile);

		void*	pData = new char[iSize * iVtxCount];

		// 정점정보를 저장한다.
		fread(pData, iSize, iVtxCount, pFile);

		// 버텍스 버퍼를 만든다.
		CreateVertexBuffer(pData, iVtxCount, iSize, eUsage, ePrimitive,
			pContainer);

		SAFE_DELETE_ARRAY(pData);

		// 서브셋 수를 저장한다.
		int	iSubset = 0, iIdxCount = 0;
		fread(&iSubset, 4, 1, pFile);

		for (int j = 0; j < iSubset; ++j)
		{
			// 인덱스 수를 저장한다.
			fread(&iIdxCount, 4, 1, pFile);
			// 인덱스 크기를 저장한다.
			fread(&iSize, 4, 1, pFile);
			// 인덱스 포멧을 저장한다.
			DXGI_FORMAT	eFormat;
			fread(&eFormat, 4, 1, pFile);
			// 인덱스 용도를 저장한다.
			fread(&eUsage, 4, 1, pFile);

			void*	pData = new char[iSize * iIdxCount];

			// 현재 서브셋의 인덱스 정보를 저장한다.
			fread(pData, iSize, iIdxCount, pFile);

			CreateIndexBuffer(pData, iIdxCount, iSize, eUsage, eFormat,
				pContainer);

			SAFE_DELETE_ARRAY(pData);
		}

		// 재질 정보를 저장한다.
		int	iMtrlCount = 0;
		fread(&iMtrlCount, 4, 1, pFile);

		for (int j = 0; j < iMtrlCount; ++j)
		{
			CMaterial*	pMaterial = new CMaterial;
			pMaterial->Init();

			MATERIALINFO	tMtrlInfo;
			SKININFO	tBaseSkin = {};

			// 재질 색상정보를 저장한다.
			fread(&tMtrlInfo, sizeof(MATERIALINFO), 1, pFile);

			pMaterial->SetMaterialInfo(tMtrlInfo);

			bool	bTexture = false;

			fread(&bTexture, 1, 1, pFile);

			if (bTexture)
			{
				tBaseSkin.pDiffuse = new TEXTURE;

				LoadTexture(tBaseSkin.pDiffuse, pFile);
			}

			fread(&bTexture, 1, 1, pFile);
			if (bTexture)
			{
				tBaseSkin.pNormal = new TEXTURE;
				LoadTexture(tBaseSkin.pNormal, pFile);
			}

			fread(&bTexture, 1, 1, pFile);
			if (bTexture)
			{
				tBaseSkin.pSpecular = new TEXTURE;
				LoadTexture(tBaseSkin.pSpecular, pFile);
			}

			pMaterial->SetBaseSkin(tBaseSkin);

			pContainer->vecMaterial.push_back(pMaterial);
			//m_vecMeshContainer[i]->vecMaterial[j]->Save(pFile);
		}

		m_vecMeshContainer.push_back(pContainer);
	}

	fclose(pFile);

	SAFE_RELEASE(m_pAnimation);

	m_pAnimation = new CAnimation3D;

	m_pAnimation->Init();

	char	strAniPath[MAX_PATH] = {};
	memcpy(strAniPath, pFileName, strlen(pFileName));

	memset(strAniPath + (strlen(pFileName) - 3), 0, 3);
	strcat_s(strAniPath, "anm");
	if (!m_pAnimation->LoadFromFullPath(strAniPath))
	{
		SAFE_RELEASE(m_pAnimation);
	}
}

void CMesh::LoadTexture(_tagTexture * pTex, FILE * pFile)
{
	char	strKey[256] = {};

	int	iLength = 0;

	fread(&iLength, 4, 1, pFile);
	fread(strKey, 1, iLength, pFile);

	wchar_t	strPath[MAX_PATH] = {};
	fread(&iLength, 4, 1, pFile);
	fread(strPath, 2, iLength, pFile);

	// 패스 키 저장
	char	strPathKey[256] = {};
	fread(&iLength, 4, 1, pFile);
	fread(strPathKey, 1, iLength, pFile);

	// 텍스쳐 로딩
	pTex->pTexture = GET_SINGLE(CResourcesManager)->LoadTexture(
		strKey, strPath, strPathKey);

	// 레지스터 저장
	fread(&pTex->iTextureRegister, 4, 1, pFile);

	iLength = 0;
	memset(strKey, 0, 256);

	fread(&iLength, 4, 1, pFile);
	fread(strKey, 1, iLength, pFile);

	pTex->pSampler = GET_SINGLE(CResourcesManager)->FindSampler(strKey);

	fread(&pTex->iSamplerRegister, 4, 1, pFile);
	fread(&pTex->iShaderConstantType, 4, 1, pFile);
}

bool CMesh::CreateVertexBuffer(void * pVertices, unsigned int iVtxCount,
	unsigned int iVtxSize, D3D11_USAGE eVtxUsage,
	D3D11_PRIMITIVE_TOPOLOGY ePrimitive,
	PMESHCONTAINER pContainer)
{
	PVERTEXBUFFER	pVB = new VERTEXBUFFER;

	// 정점 수만큼 반복하며 Min과 Max값을 구한다.
	for (int i = 0; i < iVtxCount; ++i)
	{
		DxVector3	vPos;
		memcpy(&vPos, ((char*)pVertices) + i * iVtxSize,
			sizeof(DxVector3));
		if (m_vMin.x > vPos.x)
			m_vMin.x = vPos.x;

		if (m_vMax.x < vPos.x)
			m_vMax.x = vPos.x;

		if (m_vMin.y > vPos.y)
			m_vMin.y = vPos.y;

		if (m_vMax.y < vPos.y)
			m_vMax.y = vPos.y;

		if (m_vMin.z > vPos.z)
			m_vMin.z = vPos.z;

		if (m_vMax.z < vPos.z)
			m_vMax.z = vPos.z;
	}

	pContainer->pVtxBuffer = pVB;

	pVB->iCount = iVtxCount;
	pVB->iSize = iVtxSize;
	pVB->eUsage = eVtxUsage;
	pVB->ePrimitive = ePrimitive;

	SAFE_DELETE_ARRAY(pVB->pData);

	D3D11_BUFFER_DESC	tDesc = {};
	tDesc.ByteWidth = iVtxSize * iVtxCount;
	tDesc.Usage = eVtxUsage;
	if (eVtxUsage == D3D11_USAGE_DYNAMIC)
		tDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	tDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	tDesc.StructureByteStride = 0;

	pVB->pData = new char[iVtxSize * iVtxCount];

	memcpy(pVB->pData, pVertices, iVtxSize * iVtxCount);

	D3D11_SUBRESOURCE_DATA	tData = {};
	tData.pSysMem = pVB->pData;

	if (DEVICE->CreateBuffer(&tDesc, &tData, &pVB->pBuffer))
		return false;

	return true;
}

bool CMesh::CreateIndexBuffer(void * pIndices, unsigned int iIdxCount,
	unsigned int iIdxSize, D3D11_USAGE eIdxUsage, DXGI_FORMAT eFormat,
	PMESHCONTAINER pContainer)
{
	PINDEXBUFFER	pIB = new INDEXBUFFER;

	pContainer->vecIndexBuffer.push_back(pIB);

	pIB->iCount = iIdxCount;
	pIB->iSize = iIdxSize;
	pIB->eUsage = eIdxUsage;
	pIB->eFormat = eFormat;

	SAFE_DELETE_ARRAY(pIB->pData);

	D3D11_BUFFER_DESC	tDesc = {};
	tDesc.ByteWidth = iIdxSize * iIdxCount;
	tDesc.Usage = eIdxUsage;
	if (eIdxUsage == D3D11_USAGE_DYNAMIC)
		tDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	tDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	tDesc.StructureByteStride = 0;

	pIB->pData = new char[iIdxSize * iIdxCount];

	memcpy(pIB->pData, pIndices, iIdxSize * iIdxCount);

	D3D11_SUBRESOURCE_DATA	tData = {};
	tData.pSysMem = pIB->pData;

	if (DEVICE->CreateBuffer(&tDesc, &tData, &pIB->pBuffer))
		return false;

	return true;
}

void CMesh::SubDivide(vector<VERTEXCOLOR>* pVtx, vector<UINT>* pIdx)
{
	vector<VERTEXCOLOR>	vecVtx = *pVtx;
	vector<UINT>	vecIdx = *pIdx;
	pVtx->resize(0);
	pIdx->resize(0);

	//       v1
	//       *
	//      / \
			//     /   \
	//  m0*-----*m1
//   / \   / \
	//  /   \ /   \
	// *-----*-----*
// v0    m2     v2

	UINT numTris = vecIdx.size() / 3;
	for (UINT i = 0; i < numTris; ++i)
	{
		VERTEXCOLOR v0 = vecVtx[vecIdx[i * 3 + 0]];
		VERTEXCOLOR v1 = vecVtx[vecIdx[i * 3 + 1]];
		VERTEXCOLOR v2 = vecVtx[vecIdx[i * 3 + 2]];

		//
		// Generate the midpoints.
		//

		VERTEXCOLOR m0, m1, m2;

		// For subdivision, we just care about the position component.  We derive the other
		// vertex components in CreateGeosphere.

		m0.vPos = DxVector3(
			0.5f*(v0.vPos.x + v1.vPos.x),
			0.5f*(v0.vPos.y + v1.vPos.y),
			0.5f*(v0.vPos.z + v1.vPos.z));

		m1.vPos = DxVector3(
			0.5f*(v1.vPos.x + v2.vPos.x),
			0.5f*(v1.vPos.y + v2.vPos.y),
			0.5f*(v1.vPos.z + v2.vPos.z));

		m2.vPos = XMFLOAT3(
			0.5f*(v0.vPos.x + v2.vPos.x),
			0.5f*(v0.vPos.y + v2.vPos.y),
			0.5f*(v0.vPos.z + v2.vPos.z));

		//
		// Add new geometry.
		//

		pVtx->push_back(v0); // 0
		pVtx->push_back(v1); // 1
		pVtx->push_back(v2); // 2
		pVtx->push_back(m0); // 3
		pVtx->push_back(m1); // 4
		pVtx->push_back(m2); // 5

		pIdx->push_back(i * 6 + 0);
		pIdx->push_back(i * 6 + 3);
		pIdx->push_back(i * 6 + 5);

		pIdx->push_back(i * 6 + 3);
		pIdx->push_back(i * 6 + 4);
		pIdx->push_back(i * 6 + 5);

		pIdx->push_back(i * 6 + 5);
		pIdx->push_back(i * 6 + 4);
		pIdx->push_back(i * 6 + 2);

		pIdx->push_back(i * 6 + 3);
		pIdx->push_back(i * 6 + 1);
		pIdx->push_back(i * 6 + 4);
	}
}

void CMesh::SubDividePos(vector<VERTEXPOS>* pVtx, vector<UINT>* pIdx)
{
	vector<VERTEXPOS>	vecVtx = *pVtx;
	vector<UINT>	vecIdx = *pIdx;
	pVtx->resize(0);
	pIdx->resize(0);

	//       v1
	//       *
	//      / \
				//     /   \
	//  m0*-----*m1
//   / \   / \
	//  /   \ /   \
	// *-----*-----*
// v0    m2     v2

	UINT numTris = vecIdx.size() / 3;
	for (UINT i = 0; i < numTris; ++i)
	{
		VERTEXPOS v0 = vecVtx[vecIdx[i * 3 + 0]];
		VERTEXPOS v1 = vecVtx[vecIdx[i * 3 + 1]];
		VERTEXPOS v2 = vecVtx[vecIdx[i * 3 + 2]];

		//
		// Generate the midpoints.
		//

		VERTEXPOS m0, m1, m2;

		// For subdivision, we just care about the position component.  We derive the other
		// vertex components in CreateGeosphere.

		m0.vPos = DxVector3(
			0.5f*(v0.vPos.x + v1.vPos.x),
			0.5f*(v0.vPos.y + v1.vPos.y),
			0.5f*(v0.vPos.z + v1.vPos.z));

		m1.vPos = DxVector3(
			0.5f*(v1.vPos.x + v2.vPos.x),
			0.5f*(v1.vPos.y + v2.vPos.y),
			0.5f*(v1.vPos.z + v2.vPos.z));

		m2.vPos = XMFLOAT3(
			0.5f*(v0.vPos.x + v2.vPos.x),
			0.5f*(v0.vPos.y + v2.vPos.y),
			0.5f*(v0.vPos.z + v2.vPos.z));

		//
		// Add new geometry.
		//

		pVtx->push_back(v0); // 0
		pVtx->push_back(v1); // 1
		pVtx->push_back(v2); // 2
		pVtx->push_back(m0); // 3
		pVtx->push_back(m1); // 4
		pVtx->push_back(m2); // 5

		pIdx->push_back(i * 6 + 0);
		pIdx->push_back(i * 6 + 3);
		pIdx->push_back(i * 6 + 5);

		pIdx->push_back(i * 6 + 3);
		pIdx->push_back(i * 6 + 4);
		pIdx->push_back(i * 6 + 5);

		pIdx->push_back(i * 6 + 5);
		pIdx->push_back(i * 6 + 4);
		pIdx->push_back(i * 6 + 2);

		pIdx->push_back(i * 6 + 3);
		pIdx->push_back(i * 6 + 1);
		pIdx->push_back(i * 6 + 4);
	}
}

bool CMesh::ConvertFbxData(CFbxLoader * pLoader, FBX_LOAD_TYPE eLoadType)
{
 	if (eLoadType != FLT_ANIMATION)
	{
		const vector<PFBXMESHCONTAINER>*	pMeshContainer = pLoader->GetMeshContainer();

		vector<PFBXMESHCONTAINER>::const_iterator	iter;
		vector<PFBXMESHCONTAINER>::const_iterator	iterEnd = pMeshContainer->end();

		for (iter = pMeshContainer->begin(); iter != iterEnd; ++iter)
		{
			PMESHCONTAINER	pContainer = new MESHCONTAINER;

			m_vecMeshContainer.push_back(pContainer);

			int		iVtxSize = 0;

			// 정점정보를 얻어와서 버텍스버퍼를 만든다.
			if ((*iter)->bAnimation)
			{
				if ((*iter)->bBump)
				{
					iVtxSize = sizeof(VERTEXANIMBUMP);

					vector<VERTEXANIMBUMP>	vecVtx;

					for (size_t i = 0; i < (*iter)->vecPos.size(); ++i)
					{
						VERTEXANIMBUMP	tVtx = {};

						tVtx.vPos = (*iter)->vecPos[i];
						tVtx.vNormal = (*iter)->vecNormal[i];
						tVtx.vUV = (*iter)->vecUV[i];
						tVtx.vTangent = (*iter)->vecTangent[i];
						tVtx.vBinormal = (*iter)->vecBinormal[i];
						tVtx.vWeight = (*iter)->vecBlendWeight[i];
						tVtx.vIndices = (*iter)->vecBlendIndex[i];

						vecVtx.push_back(tVtx);
					}

					if (!CreateVertexBuffer(&vecVtx[0], vecVtx.size(),
						iVtxSize, D3D11_USAGE_DEFAULT,
						D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
						pContainer))
						return false;
				}

				else
				{
					iVtxSize = sizeof(VERTEXANIM);
					vector<VERTEXANIM>	vecVtx;

					for (size_t i = 0; i < (*iter)->vecPos.size(); ++i)
					{
						VERTEXANIM	tVtx = {};

						tVtx.vPos = (*iter)->vecPos[i];
						tVtx.vNormal = (*iter)->vecNormal[i];
						tVtx.vUV = (*iter)->vecUV[i];
						tVtx.vWeight = (*iter)->vecBlendWeight[i];
						tVtx.vIndices = (*iter)->vecBlendIndex[i];

						vecVtx.push_back(tVtx);
					}

					if (!CreateVertexBuffer(&vecVtx[0], vecVtx.size(),
						iVtxSize, D3D11_USAGE_DEFAULT,
						D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
						pContainer))
						return false;
				}
			}

			else
			{
				if ((*iter)->bBump)
				{
					iVtxSize = sizeof(VERTEXBUMP);

					vector<VERTEXBUMP>	vecVtx;

					for (size_t i = 0; i < (*iter)->vecPos.size(); ++i)
					{
						VERTEXBUMP	tVtx = {};

						tVtx.vPos = (*iter)->vecPos[i];
						tVtx.vNormal = (*iter)->vecNormal[i];
						tVtx.vUV = (*iter)->vecUV[i];
						tVtx.vTangent = (*iter)->vecTangent[i];
						tVtx.vBinormal = (*iter)->vecBinormal[i];

						vecVtx.push_back(tVtx);
					}

					if (!CreateVertexBuffer(&vecVtx[0], vecVtx.size(),
						iVtxSize, D3D11_USAGE_DEFAULT,
						D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
						pContainer))
						return false;
				}

				else
				{
					iVtxSize = sizeof(VERTEXNORMALTEX);
					vector<VERTEXNORMALTEX>	vecVtx;

					for (size_t i = 0; i < (*iter)->vecPos.size(); ++i)
					{
						VERTEXNORMALTEX	tVtx = {};

						tVtx.vPos = (*iter)->vecPos[i];
						tVtx.vNormal = (*iter)->vecNormal[i];
						tVtx.vUV = (*iter)->vecUV[i];

						vecVtx.push_back(tVtx);
					}

					if (!CreateVertexBuffer(&vecVtx[0], vecVtx.size(),
						iVtxSize, D3D11_USAGE_DEFAULT,
						D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
						pContainer))
						return false;
				}
			}

			// 인덱스버퍼 생성
			for (size_t i = 0; i < (*iter)->vecIndices.size(); ++i)
			{
				if (!CreateIndexBuffer(&(*iter)->vecIndices[i][0],
					(*iter)->vecIndices[i].size(), 4,
					D3D11_USAGE_DEFAULT, DXGI_FORMAT_R32_UINT,
					pContainer))
					return false;
			}
		}

		// 재질 정보를 읽어온다.
		const vector<vector<PFBXMATERIAL>>*	pMaterials = pLoader->GetMaterials();

		vector<vector<PFBXMATERIAL>>::const_iterator	iterM;
		vector<vector<PFBXMATERIAL>>::const_iterator	iterMEnd = pMaterials->end();

		int	iContainer = 0;
		for (iterM = pMaterials->begin(); iterM != iterMEnd; ++iterM, ++iContainer)
		{
			PMESHCONTAINER	pContainer = m_vecMeshContainer[iContainer];

			for (size_t i = 0; i < (*iterM).size(); ++i)
			{
				PFBXMATERIAL	pMtrl = (*iterM)[i];

				CMaterial*	pMaterial = new CMaterial;

				if (!pMaterial->Init())
				{
					SAFE_RELEASE(pMaterial);
					return NULL;
				}

				pMaterial->SetMaterialInfo(pMtrl->vDif, pMtrl->vAmb,
					pMtrl->vSpc, pMtrl->vEmv, pMtrl->fShininess);

				// 이름을 불러온다.
				char	strName[MAX_PATH] = {};
				_splitpath_s(pMtrl->strDifTex.c_str(), NULL, 0, NULL, 0,
					strName, MAX_PATH, NULL, 0);

				pMaterial->SetDiffuseTextureFromFullPath("Linear", strName,
					pMtrl->strDifTex.c_str());
				pMaterial->SetDiffuseRegister(0, 0);

				if (!pMtrl->strBumpTex.empty())
				{
					memset(strName, 0, MAX_PATH);
					_splitpath_s(pMtrl->strBumpTex.c_str(), NULL, 0, NULL, 0,
						strName, MAX_PATH, NULL, 0);

					pMaterial->SetNormalMapTextureFromFullPath("Point", strName,
						pMtrl->strBumpTex.c_str());
					pMaterial->SetNormalMapRegister(1, 1);
				}

				if (!pMtrl->strSpcTex.empty())
				{
					memset(strName, 0, MAX_PATH);
					_splitpath_s(pMtrl->strSpcTex.c_str(), NULL, 0, NULL, 0,
						strName, MAX_PATH, NULL, 0);

					pMaterial->SetSpecularTextureFromFullPath("Point", strName,
						pMtrl->strSpcTex.c_str());
					pMaterial->SetSpecularRegister(2, 2);
				}

				pContainer->vecMaterial.push_back(pMaterial);
			}
		}

		m_vSize = m_vMax - m_vMin;
		m_tSphere.vCenter = (m_vMin + m_vMax) / 2.f;
		m_tSphere.fRadius = m_vSize.Length() / 2.f;

		// 애니메이션
		const vector<PFBXBONE>*	pvecBone = pLoader->GetBones();

		if (pvecBone->empty())
			return true;

		SAFE_RELEASE(m_pAnimation);

		m_pAnimation = new CAnimation3D;

		if (!m_pAnimation->Init())
		{
			SAFE_RELEASE(m_pAnimation);
			return false;
		}

		// 본 수만큼 반복한다.
		vector<PFBXBONE>::const_iterator	iterB;
		vector<PFBXBONE>::const_iterator	iterBEnd = pvecBone->end();

		for (iterB = pvecBone->begin(); iterB != iterBEnd; ++iterB)
		{
			PBONE	pBone = new BONE;

			pBone->strName = (*iterB)->strName;
			pBone->iDepth = (*iterB)->iDepth;
			pBone->iParentIndex = (*iterB)->iParentIndex;

			float	fMat[4][4];

			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					fMat[i][j] = (*iterB)->matOffset.mData[i].mData[j];
				}
			}

			pBone->matOffset = new MATRIX;
			*pBone->matOffset = fMat;

			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					fMat[i][j] = (*iterB)->matBone.mData[i].mData[j];
				}
			}

			pBone->matBone = new MATRIX;
			*pBone->matBone = fMat;

			/*pBone->vecKeyFrame.reserve((*iterB)->vecKeyFrame.size());

			for (int i = 0; i < (*iterB)->vecKeyFrame.size(); ++i)
			{
			PKEYFRAME	pKeyFrame = new KEYFRAME;

			pKeyFrame->dTime = (*iterB)->vecKeyFrame[i].dTime;

			FbxAMatrix	mat = (*iterB)->vecKeyFrame[i].matTransform;

			FbxVector4	vPos, vScale;
			FbxQuaternion	qRot;

			vPos = mat.GetT();
			vScale = mat.GetS();
			qRot = mat.GetQ();

			pKeyFrame->vScale = DxVector3(vScale.mData[0], vScale.mData[1],
			vScale.mData[2]);
			pKeyFrame->vPos = DxVector3(vPos.mData[0], vPos.mData[1],
			vPos.mData[2]);
			pKeyFrame->vRot = DxVector4(qRot.mData[0], qRot.mData[1],
			qRot.mData[2], qRot.mData[3]);

			pBone->vecKeyFrame.push_back(pKeyFrame);
			}*/

			m_pAnimation->AddBone(pBone);
		}

		m_pAnimation->CreateBoneTexture();
	}

	if (eLoadType != FLT_MESH)
	{
		const vector<PFBXANIMATIONCLIP>* pvecClip = pLoader->GetClip();

		// 클립을 읽어온다.
		vector<PFBXANIMATIONCLIP>::const_iterator	iterC;
		vector<PFBXANIMATIONCLIP>::const_iterator	iterCEnd = pvecClip->end();

		for (iterC = pvecClip->begin(); iterC != iterCEnd; ++iterC)
		{
			m_pAnimation->AddClip(AO_LOOP, *iterC);
		}
	}

	return true;
}
