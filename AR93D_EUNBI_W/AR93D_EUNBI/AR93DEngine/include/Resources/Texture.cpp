#include "Texture.h"
#include "../Core/PathManager.h"
#include "../Device.h"
#include "DirectXTex.h"
#include "../Core/Input.h"
#include "../Component/ColliderRay.h"
#include "../Scene/SceneManager.h"
#include "../Scene/Scene.h"
#include "../Component/Terrain.h"

AR3D_USING

CTexture::CTexture() :
	m_pSRView(NULL),
	m_pTexture(NULL)
{
}

CTexture::~CTexture()
{
	SAFE_RELEASE(m_pTexture);
	SAFE_RELEASE(m_pSRView);
	Safe_Release_VecList(m_vecScratchImage);
	Safe_Delete_VecList(m_vecScratchImage);
}

CTexture * CTexture::CreateTexture(const string & strKey,
	unsigned int iWidth, unsigned int iHeight,
	unsigned int iArrSize, DXGI_FORMAT eFmt,
	D3D11_USAGE eUsage, int iBindFlag,
	int iCpuFlag)
{
	CTexture*	pTex = new CTexture;

	if (!pTex->CreateResource(strKey, iWidth, iHeight,
		iArrSize, eFmt, eUsage, iBindFlag, iCpuFlag))
	{
		SAFE_RELEASE(pTex);
		return NULL;
	}

	return pTex;
}

ID3D11Texture2D * CTexture::GetTexture() const
{
	return m_pTexture;
}

string CTexture::GetKey() const
{
	return m_strKey;
}

string CTexture::GetPathKey() const
{
	return m_strPathKey;
}

wstring CTexture::GetFullPath() const
{
	return m_strFullPath;
}

wstring CTexture::GetFileName() const
{
	return m_strFileName;
}

void CTexture::SetPathKey(const string & strPathKey)
{
	m_strPathKey = strPathKey;
}

void CTexture::Save(FILE * pFile)
{
}

void CTexture::Load(FILE * pFile)
{
}

bool CTexture::LoadTexture(const string & strKey, TCHAR * pFileName,
	const string & strPathKey)
{
	// 유니코드 문자열을 멀티바이트 문자열로 만든다.
	char	strPath[MAX_PATH] = {};

	WideCharToMultiByte(CP_ACP, 0, pFileName, -1, strPath, lstrlen(pFileName),
		NULL, NULL);

	m_strKey = strKey;
	m_strPathKey = strPathKey;
	m_strFileName = pFileName;

	return LoadTexture(strKey, strPath, strPathKey);
}

bool CTexture::LoadTexture(const string & strKey, char * pFileName,
	const string & strPathKey)
{
	const char*	pPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	string	strPath;
	if (pPath)
		strPath = pPath;

	strPath += pFileName;

	m_strKey = strKey;
	m_strPathKey = strPathKey;

	TCHAR	strFileName[MAX_PATH] = {};

	MultiByteToWideChar(CP_ACP, 0, pFileName, -1, strFileName,
		strlen(pFileName));

	m_strFileName = strFileName;

	return LoadTextureFromFullPath(strKey, strPath.c_str());
}

bool CTexture::LoadTexture(const string & strKey)
{
	m_strKey = strKey;

	char	strExt[_MAX_EXT] = {};

	::DirectX::ScratchImage*	pImage = new ::DirectX::ScratchImage;

	return true;
}

bool CTexture::LoadTextureFromFullPath(const string & strKey,
	const char * pFullPath)
{
	m_strKey = strKey;

	char	strExt[_MAX_EXT] = {};

	_splitpath_s(pFullPath, NULL, 0, NULL, 0, NULL, 0, strExt, _MAX_EXT);

	_strupr_s(strExt);

	::DirectX::ScratchImage*	pImage = new ::DirectX::ScratchImage;

	// 전체 경로를 유니코드로 변환한다.
	TCHAR	strFullPath[MAX_PATH] = {};
	MultiByteToWideChar(CP_ACP, 0, pFullPath, -1, strFullPath, strlen(pFullPath));

	m_strFullPath = strFullPath;
	m_strKey = strKey;

	if (m_strPathKey.empty())
		m_strPathKey = TEXTURE_PATH;

	if (strcmp(strExt, ".DDS") == 0)
	{
		if (FAILED(LoadFromDDSFile(strFullPath, DDS_FLAGS_NONE, NULL, *pImage)))
		{
			SAFE_DELETE(pImage);
			return false;
		}
	}

	else if (strcmp(strExt, ".TGA") == 0)
	{
		if (FAILED(LoadFromTGAFile(strFullPath, NULL, *pImage)))
		{
			SAFE_DELETE(pImage);
			return false;
		}
	}

	else
	{
		if (FAILED(LoadFromWICFile(strFullPath, WIC_FLAGS_NONE, NULL,
			*pImage)))
		{
			SAFE_DELETE(pImage);
			return false;
		}
	}

	m_vecScratchImage.push_back(pImage);

	if (FAILED(CreateShaderResourceView(DEVICE, pImage->GetImages(),
		pImage->GetImageCount(), pImage->GetMetadata(), &m_pSRView)))
		return false;

	return true;
}

bool CTexture::LoadTexture(const string & strKey, const vector<wstring>& vecFileName,
	const string & strPathKey)
{
	vector<string>	vecPath;

	for (size_t i = 0; i < vecFileName.size(); ++i)
	{
		// 유니코드 문자열을 멀티바이트 문자열로 만든다.
		char	strPath[MAX_PATH] = {};

		WideCharToMultiByte(CP_ACP, 0, vecFileName[i].c_str(), -1, strPath, vecFileName[i].length(),
			NULL, NULL);

		vecPath.push_back(strPath);
	}

	m_strPathKey = strPathKey;
	m_strKey = strKey;

	return LoadTextureFromMultibyte(strKey, vecPath, strPathKey);
}

bool CTexture::LoadTextureFromMultibyte(const string & strKey, const vector<string>& vecFileName,
	const string & strPathKey)
{
	const char*	pPath = GET_SINGLE(CPathManager)->FindPathToMultiByte(strPathKey);

	string	strPath;
	if (pPath)
		strPath = pPath;

	vector<string>	vecFullPath;

	for (size_t i = 0; i < vecFileName.size(); ++i)
	{
		string	strFullPath = strPath;
		strFullPath += vecFileName[i];

		vecFullPath.push_back(strFullPath);
	}

	m_strPathKey = strPathKey;
	m_strKey = strKey;

	return LoadTextureFromFullPath(strKey, vecFullPath);
}

bool CTexture::LoadTextureFromFullPath(const string & strKey, const vector<string>& vecFullPath)
{
	m_strKey = strKey;

	for (size_t i = 0; i < vecFullPath.size(); ++i)
	{
		char	strExt[_MAX_EXT] = {};

		_splitpath_s(vecFullPath[i].c_str(), NULL, 0, NULL, 0, NULL, 0, strExt, _MAX_EXT);

		_strupr_s(strExt);

		::DirectX::ScratchImage*	pImage = new ::DirectX::ScratchImage;

		// 전체 경로를 유니코드로 변환한다.
		TCHAR	strFullPath[MAX_PATH] = {};
		MultiByteToWideChar(CP_ACP, 0, vecFullPath[i].c_str(), -1, strFullPath,
			vecFullPath[i].length());

		if (strcmp(strExt, ".DDS") == 0)
		{
			if (FAILED(LoadFromDDSFile(strFullPath, DDS_FLAGS_NONE, NULL, *pImage)))
			{
				SAFE_DELETE(pImage);
				return false;
			}
		}

		else if (strcmp(strExt, ".TGA") == 0)
		{
			if (FAILED(LoadFromTGAFile(strFullPath, NULL, *pImage)))
			{
				SAFE_DELETE(pImage);
				return false;
			}
		}

		else
		{ // Load가 한번에 되지않는다 ..........

			if (FAILED(LoadFromWICFile(strFullPath, WIC_FLAGS_NONE, NULL,
				*pImage)))
			{
				while (!FAILED(LoadFromWICFile(strFullPath, WIC_FLAGS_NONE, NULL,
					*pImage)))
				{
					LoadFromWICFile(strFullPath, WIC_FLAGS_NONE, NULL,
						*pImage);
					/*SAFE_DELETE(pImage);
					return false;*/
				}
			}
		}

		m_vecScratchImage.push_back(pImage);
	}

	vector<ID3D11Texture2D*>	vecTextureArray;

	for (size_t i = 0; i < m_vecScratchImage.size(); ++i)
	{
		ID3D11Texture2D*	pTex = NULL;
		if (FAILED(CreateTextureEx(DEVICE, m_vecScratchImage[i]->GetImages(),
			m_vecScratchImage[i]->GetImageCount(), m_vecScratchImage[i]->GetMetadata(),
			D3D11_USAGE_STAGING, 0, D3D11_CPU_ACCESS_READ, 0, false,
			(ID3D11Resource**)&pTex)))
		{
			Safe_Release_VecList(vecTextureArray);
			return false;
		}

		vecTextureArray.push_back(pTex);
	}

	D3D11_TEXTURE2D_DESC	tTex2DDesc = {};
	vecTextureArray[0]->GetDesc(&tTex2DDesc);

	D3D11_TEXTURE2D_DESC	tTexArrayDesc = {};

	tTexArrayDesc.Width = tTex2DDesc.Width;
	tTexArrayDesc.Height = tTex2DDesc.Height;
	tTexArrayDesc.MipLevels = tTex2DDesc.MipLevels;
	tTexArrayDesc.ArraySize = vecTextureArray.size();
	tTexArrayDesc.Format = tTex2DDesc.Format;
	tTexArrayDesc.SampleDesc.Count = 1;
	tTexArrayDesc.SampleDesc.Quality = 0;
	tTexArrayDesc.Usage = D3D11_USAGE_DEFAULT;
	tTexArrayDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

	ID3D11Texture2D*	pTexArray = NULL;
	if (FAILED(DEVICE->CreateTexture2D(&tTexArrayDesc, NULL, &pTexArray)))
	{
		Safe_Release_VecList(vecTextureArray);
		return false;
	}

	for (size_t i = 0; i < vecTextureArray.size(); ++i)
	{
		for (UINT iMipLevel = 0; iMipLevel < tTex2DDesc.MipLevels; ++iMipLevel)
		{
			D3D11_MAPPED_SUBRESOURCE	tMap = {};

			CONTEXT->Map(vecTextureArray[i], iMipLevel, D3D11_MAP_READ, 0, &tMap);

			CONTEXT->UpdateSubresource(pTexArray, D3D11CalcSubresource(iMipLevel, i,
				tTex2DDesc.MipLevels), NULL, tMap.pData, tMap.RowPitch, tMap.DepthPitch);

			CONTEXT->Unmap(vecTextureArray[i], iMipLevel);
		}
	}

	// Shader ResourceView Desc
	D3D11_SHADER_RESOURCE_VIEW_DESC	tViewDesc = {};

	tViewDesc.Format = tTexArrayDesc.Format;
	tViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	tViewDesc.Texture2DArray.MipLevels = tTexArrayDesc.MipLevels;
	tViewDesc.Texture2DArray.ArraySize = vecTextureArray.size();

	if (FAILED(DEVICE->CreateShaderResourceView(pTexArray, &tViewDesc, &m_pSRView)))
	{
		SAFE_RELEASE(pTexArray);
		Safe_Release_VecList(vecTextureArray);
		return false;
	}

	SAFE_RELEASE(pTexArray);
	Safe_Release_VecList(vecTextureArray);

	return true;
}

bool CTexture::CreateResource(const string & strKey,
	unsigned int iWidth, unsigned int iHeight,
	unsigned int iArrSize, DXGI_FORMAT eFmt,
	D3D11_USAGE eUsage, int iBindFlag,
	int iCpuFlag)
{
	::DirectX::ScratchImage*	pImage = new ::DirectX::ScratchImage;

	if (FAILED(pImage->Initialize2D(eFmt, iWidth, iHeight,
		iArrSize, 1)))
	{
		SAFE_DELETE(pImage);
		return false;
	}

	m_vecScratchImage.push_back(pImage);

	if (FAILED(CreateShaderResourceViewEx(DEVICE, pImage->GetImages(),
		pImage->GetImageCount(), pImage->GetMetadata(), eUsage,
		iBindFlag, iCpuFlag, 0, false, &m_pSRView)))
		return false;

	m_pSRView->GetResource((ID3D11Resource**)&m_pTexture);

	return true;
}

void CTexture::UpdateData(void * pData, int iSize)
{
	ID3D11Texture2D*	pTexture = NULL;

	m_pSRView->GetResource((ID3D11Resource**)&pTexture);

	D3D11_MAPPED_SUBRESOURCE	tMap;

	CONTEXT->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &tMap);

	memcpy(tMap.pData, pData, iSize);

	CONTEXT->Unmap(pTexture, 0);

	SAFE_RELEASE(pTexture);
}

void CTexture::SetTexture(int iRegister, int iShaderType)
{
	if (iShaderType & CUT_VERTEX)
		CONTEXT->VSSetShaderResources(iRegister, 1, &m_pSRView);

	if (iShaderType & CUT_PIXEL)
		CONTEXT->PSSetShaderResources(iRegister, 1, &m_pSRView);
}

void CTexture::SaveTextureFile(const wchar_t * pFileName, const string & strPathKey)
{
	const wchar_t*	pPath = GET_SINGLE(CPathManager)->FindPath(strPathKey);

	wstring	strFullPath;
	if (pPath)
		strFullPath = pPath;
	strFullPath += pFileName;

	char	strFileName[MAX_PATH] = {};
	WideCharToMultiByte(CP_ACP, 0, pFileName, -1, strFileName, lstrlen(pFileName),
		0, 0);

	char	strExt[_MAX_EXT] = {};

	_splitpath_s(strFileName, NULL, 0, NULL, 0, NULL, 0, strExt, _MAX_EXT);

	_strupr_s(strExt);

	if (strcmp(strExt, ".DDS") == 0)
	{
		ScratchImage	img;
		CaptureTexture(DEVICE, CONTEXT, m_pTexture, img);
		SaveToDDSFile(img.GetImages(), img.GetImageCount(),
			img.GetMetadata(), DDS_FLAGS_NONE, strFullPath.c_str());
	}

	else if (strcmp(strExt, ".TGA") == 0)
	{
		ScratchImage	img;
		CaptureTexture(DEVICE, CONTEXT, m_pTexture, img);
		SaveToTGAFile(img.GetImages()[0], strFullPath.c_str());
	}
	else
	{
		ScratchImage	img;
		CaptureTexture(DEVICE, CONTEXT, m_pTexture, img);
		SaveToWICFile(img.GetImages(), img.GetImageCount(),
			WIC_FLAGS_NONE, GetWICCodec(WIC_CODEC_PNG), strFullPath.c_str());
	}
}

//
//void CTexture::SaveTextureFileTerrain(const wchar_t * pFileName, bool breset, const string & strPathKey)
//{
//	const wchar_t*	pPath = GET_SINGLE(CPathManager)->FindPath(strPathKey);
//
//	wstring	strFullPath;
//	if (pPath)
//		strFullPath = pPath;
//	strFullPath += pFileName;
//
//	char	strFileName[MAX_PATH] = {};
//	WideCharToMultiByte(CP_ACP, 0, pFileName, -1, strFileName, lstrlen(pFileName),
//		0, 0);
//
//	char	strExt[_MAX_EXT] = {};
//
//	_splitpath_s(strFileName, NULL, 0, NULL, 0, NULL, 0, strExt, _MAX_EXT);
//
//	_strupr_s(strExt);
//
//	
//
//	//초기화
//	if (breset) 
//	{
//		//저장 기능 
//
//		ScratchImage	img;
//
//		CaptureTexture(DEVICE, CONTEXT, m_pTexture, img);
//
//		m_splattingPxl = (PSPLAT_TEX)img.GetPixels();
//
//		m_splattingImgSize = img.GetPixelsSize();
//		memset(m_splattingPxl, 0, m_splattingImgSize);
//		SaveToWICFile(img.GetImages(), img.GetImageCount(),
//		WIC_FLAGS_NONE, GetWICCodec(WIC_CODEC_BMP), strFullPath.c_str());
//	}
//
//	
//}
//
//void CTexture::EditTerrainSplattingTexture(const wchar_t * pFileName, const string & strPathKey)
//{
//	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
//	CTerrain* pTerrain = pScene->GetTerrain();
//	float OuterRad = pTerrain->GetPickRadInfo().fOuterRadius ;
//	float InnerRad = pTerrain->GetPickRadInfo().fInnerRadius ; 
//
//	SAFE_RELEASE(pTerrain);
//	const wchar_t*	pPath = GET_SINGLE(CPathManager)->FindPath(strPathKey);
//
//	wstring	strFullPath;
//	if (pPath)
//		strFullPath = pPath;
//	strFullPath += pFileName;
//
//
//	//저장 기능 
//	ScratchImage	img;
//
//	DirectX::LoadFromWICFile(strFullPath.c_str(), DirectX::WIC_FLAGS_NONE, nullptr, img);
//
//	m_splattingPxl = (PSPLAT_TEX)img.GetPixels();
//	m_splattingImgSize = img.GetPixelsSize();
//
//	DxVector3 MousePos = GET_SINGLE(CInput)->GetMouseRay()->GetRayToTerrainCollPos();
//
//	//마우스 찍힌 점 인덱스
//	//MousePos.z /= 5;
//	//MousePos.x /= 5;
//
//	int realSize = m_splattingImgSize / 4; // 129*129 = 16641  pixel
//
//	for (int i = 0; i < realSize; i++) // 4픽셀
//	{
//		int x = i % (645); 
//		int z = i / (645);
//		x = x;
//		z = 645 - z;
//
//		float fDist = ((MousePos.x - x) * (MousePos.x - x)) +
//			((MousePos.z - z) * (MousePos.z - z));
//
//		if (fDist < 0.f)
//		{
//			if (OuterRad  * OuterRad < fDist)
//			{
//				float temp = ((OuterRad * OuterRad) + fDist) / (OuterRad * OuterRad);
//					//if (InnerRad  * InnerRad < fDist)
//					//{
//					//	m_splattingPxl[i].r = 255; //a
//					//	m_splattingPxl[i].g = 255; //g
//					//	m_splattingPxl[i].b = 255;
//					//	m_splattingPxl[i].a = 255;
//					//}
//					//else
//					//{
//						m_splattingPxl[i].r = 255; //a
//						m_splattingPxl[i].g = 255; //g
//						m_splattingPxl[i].b = 255;
//						m_splattingPxl[i].a = 255;
//					//}
//			}
//		}
//
//		else if (fDist >= 0.f)
//		{
//			float temp = ((OuterRad * OuterRad) - fDist) / (OuterRad * OuterRad);
//			if (OuterRad  * OuterRad > fDist)
//			{
//				//if (InnerRad  * InnerRad > fDist)
//				//{
//				//	m_splattingPxl[i].r = 255; //a
//				//	m_splattingPxl[i].g = 255; //g
//				//	m_splattingPxl[i].b = 255;
//				//	m_splattingPxl[i].a = 255;
//				//}
//				//else
//				//{
//					m_splattingPxl[i].r = 255; //a
//					m_splattingPxl[i].g = 255; //g
//					m_splattingPxl[i].b = 255;
//					m_splattingPxl[i].a = 255;
//				//}
//			}
//
//		}
//	}
//	
//
//		if (FAILED(SaveToWICFile(img.GetImages(), img.GetImageCount(),
//			WIC_FLAGS_NONE, GetWICCodec(WIC_CODEC_BMP), strFullPath.c_str())))
//		{
//			int a = 10;
//		}
//}
//
