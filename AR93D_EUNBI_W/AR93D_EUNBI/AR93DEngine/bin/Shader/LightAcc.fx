
#include "Share.fx"

SamplerState g_GBufferSmp : register(s11);

texture2D g_GBufferAlbedo : register(t11);
texture2D g_GBufferNormal : register(t12);
texture2D g_GBufferDepth : register(t13);
texture2D g_GBufferSpecular : register(t14);


struct PS_OUTPUT_LIGHTACC
{
    float4 vTarget0 : SV_Target;
    float4 vTarget1 : SV_Target1;
};

static const float2 vPos[4] =
{
    float2(-1.f, 1.f),
	float2(1.f, 1.f),
	float2(-1.f, -1.f),
	float2(1.f, -1.f)
};

static const float2 vUV[4] =
{
    float2(0.f, 0.f),
	float2(1.f, 0.f),
	float2(0.f, 1.f),
	float2(1.f, 1.f)
};

VS_TEX_OUTPUT LightAccVS(uint iVertexID : SV_VertexID)
{
    VS_TEX_OUTPUT output = (VS_TEX_OUTPUT) 0;

    output.vPos = float4(vPos[iVertexID], 0.f, 1.f);
    output.vUV = vUV[iVertexID];

    return output;
}


_tagMtrl ComputeLightAcc(float3 vNormal, float3 vViewPos,
	float2 vUV, float fSpcPower, float4 vMtrlDif, float4 vMtrlAmb)
{
    float4 vSpc;

    vSpc = g_GBufferSpecular.Sample(g_GBufferSmp, vUV);
	//vSpc.a = 1.f;

	// 조명 타입에 따라 조명의 방향을 구하고 뷰공간으로 변환한다.
    float3 vLightDir = (float3) 0;
    float fIntensity = 1.f;
	// 방향성 조명일 경우
    if (g_iLightType == 0)
    {
        vLightDir = mul(float4(g_vLightDir, 0.f), g_matView);
        vLightDir = normalize(vLightDir);
    }

	// 점 조명일 경우
    else if (g_iLightType == 1)
    {
        vLightDir = mul(float4(g_vLightPos, 1.f), g_matView);
        vLightDir = vLightDir - vViewPos;

        float fDist = length(vLightDir);
        vLightDir = normalize(vLightDir);

        fIntensity = (1.f - fDist / g_fLightRange) * 0.7f + 0.1f;
		if (fDist > g_fLightRange)
			fIntensity = 0.f;
    }

	// Spot
    else if (g_iLightType == 2)
    {
    }

    _tagMtrl tMtrl = (_tagMtrl) 0;

	// 휘도를 구해준다. 
    float fDot = max(0, dot(vNormal, vLightDir));

    //툰 쉐이딩을 적용해야하는 오브젝트는 휘도를 3등분으로 분배해준다. 
   // if (vSpc.a == 0 || vSpc.a == 1)
    //    fDot = (ceil(fDot * 3) / 3.f) + 0.1f;
    
    //Dif, Amb값 처리 
    tMtrl.vDif = g_vLightDif * vMtrlDif * fDot * fIntensity;
    tMtrl.vAmb = g_vLightAmb * vMtrlAmb * fDot * fIntensity;

	// 정반사광을 구한다.
	// 반사벡터를 구해준다.
    float3 vReflect = 2.f * vNormal * dot(vNormal, vLightDir) - vLightDir;
    vReflect = normalize(vReflect);

   
	// 정점에서 카메라를 향하는 벡터를 만든다.
    float3 vView = -normalize(vViewPos);

    float4 SpecularPower = float4(50.f, 50.f, 50.f, 1.f);

    
    //아웃라인 or
    //림라이트
    int rimpower = 30.f;
    //float3 RimColor = float3(-2.f, -2.f, -2.f);
    float3 RimColor = vSpc.xyz;
    if(RimColor. x <= 0.f)
    {
        RimColor = float3(-2.f, -2.f, -2.f);
        rimpower = 5.f;
    }

    if (vSpc.a == 1 || vSpc.a == 2)
    {
        float rim = saturate(dot(vNormal, vView));
        if (rim > 0.3)
            rim = 1;
        else
        {
            rim = -1;
        }
       
        tMtrl.vEmiss = float4(pow(1 - rim, rimpower) * RimColor, 1.f);
        
    }

    vSpc.a = 1.f;
 
    tMtrl.vSpc = g_vLightSpc * vSpc * pow(max(0, dot(vReflect, vView)), SpecularPower) * fIntensity ;
	
    return tMtrl;
}

PS_OUTPUT_LIGHTACC LightAccPS(VS_TEX_OUTPUT input)
{
    PS_OUTPUT_LIGHTACC output = (PS_OUTPUT_LIGHTACC) 0;

    float4 vNormalCol = g_GBufferNormal.Sample(g_GBufferSmp, input.vUV);

    if (vNormalCol.a == 0.f)
        clip(-1);

    float3 vNormal = vNormalCol * 2.f - 1.f;

    float4 vDepth = g_GBufferDepth.Sample(g_GBufferSmp, input.vUV);

	// uv좌표를 -1 ~ 1 사이로 만든다.
    float4 vProjPos;
    vProjPos.x = (input.vUV.x * 2.f - 1.f) * vDepth.w;
    vProjPos.y = (input.vUV.y * -2.f + 1.f) * vDepth.w;
    vProjPos.z = vDepth.x * vDepth.w;
    vProjPos.w = vDepth.w;
	
    float3 vViewPos = mul(vProjPos, g_matInvProj).xyz;

    float4 vDif = (float4) vDepth.y;
    float4 vAmb = (float4) vDepth.z;

    _tagMtrl tMtrl = ComputeLightAcc(vNormal, vViewPos, input.vUV,
		vNormalCol.a, vDif, vAmb);
    
    output.vTarget0 = tMtrl.vDif + tMtrl.vAmb  + 0.05f;

    output.vTarget1 = tMtrl.vSpc + tMtrl.vEmiss ;

    return output;
}
