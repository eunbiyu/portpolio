#include "FreeCameraPlayer.h"
#include "Component/Transform.h"
#include "Core/Input.h"
#include "Scene/Layer.h"
#include "GameObject/GameObject.h"
#include "Scene/Scene.h"
#include "Component/Camera.h"

CFreeCameraPlayer::CFreeCameraPlayer()
{
}


CFreeCameraPlayer::~CFreeCameraPlayer()
{
}

bool CFreeCameraPlayer::Init()
{
	GET_SINGLE(CInput)->CreateKey("MoveLeft", 'A');
	GET_SINGLE(CInput)->CreateKey("MoveRight", 'D');
	GET_SINGLE(CInput)->CreateKey("MoveUp", 'W');
	GET_SINGLE(CInput)->CreateKey("MoveDown", 'S');

	return true;
}

void CFreeCameraPlayer::Input(float fTime)
{
	//FREE CAMERA ���� 

	CCamera*	pMainCamera = m_pScene->GetMainCamera();
	CTransform*	pTransform = pMainCamera->GetTransform();


	DxVector3 forward = pMainCamera->GetAxis(AXIS_Z);
	DxVector3 back = DxVector3(forward.x * -1, forward.y * -1, forward.z * -1);

	DxVector3 right = pMainCamera->GetAxis(AXIS_X);
	DxVector3 left = DxVector3(right.x * -1, right.y * -1, right.z * -1);

	if (KEYPRESS("MoveLeft") || KEYPUSH("MoveLeft"))
	{
		m_pTransform->Move(left, 2.f);
	}

	if (KEYPRESS("MoveRight") || KEYPUSH("MoveRight"))
	{
		m_pTransform->Move(right, 2.f);
	}

	if (KEYPRESS("MoveUp") || KEYPUSH("MoveUp"))
	{
		m_pTransform->Move(forward, 2.f);
	}

	if (KEYPRESS("MoveDown") || KEYPUSH("MoveDown"))
	{
		m_pTransform->Move(back, 2.f);
	}
	SAFE_RELEASE(pMainCamera);
	SAFE_RELEASE(pTransform);
}

void CFreeCameraPlayer::Update(float fTime)
{
}

void CFreeCameraPlayer::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CFreeCameraPlayer::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CFreeCameraPlayer::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}
