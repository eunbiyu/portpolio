
#include "Core.h"
#include "Client.h"
#include "resource.h"
#include "Scene/SceneManager.h"
#include "Scene/Scene.h"
#include "SceneScript\MainScene.h"
#include "SceneScript\StartScene.h"

AR3D_USING

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	/*if (!GET_SINGLE(CCore)->Init(hInstance, g_tRS[RT_FHD].iWidth,
		g_tRS[RT_FHD].iHeight, L"AR9Client", L"AR9Client", IDI_ICON1,
		IDI_ICON1, true))*/

	if (!GET_SINGLE(CCore)->Init(hInstance, g_tRS[RT_FHD].iWidth,
		g_tRS[RT_FHD].iHeight, L"AR9Client", L"AR9Client_EUNBI", IDI_ICON1,
		IDI_ICON1, true))
	{
		DESTROY_SINGLE(CCore);
		return 0;
	}

	CScene*	pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

	//CMainScene*	pStartScene = pScene->CreateScript<CMainScene>();
	CStartScene*	pStartScene = pScene->CreateScript<CStartScene>();

 	int	iRet = GET_SINGLE(CCore)->Run();

	DESTROY_SINGLE(CCore);

	return iRet;
}