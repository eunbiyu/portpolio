#pragma once

#include "Component/Script.h"
#include "GameObject/GameObject.h"

AR3D_USING

class CFreeCameraPlayer :
	public CScript
{
public:
	CFreeCameraPlayer();
	~CFreeCameraPlayer();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void OnCollisionEnter(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollision(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollisionLeave(CCollider* pSrc, CCollider* pDest, float fTime);
};



