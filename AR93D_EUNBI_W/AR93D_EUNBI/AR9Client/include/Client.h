#pragma once

#include "Engine.h"

#ifdef _DEBUG
#pragma comment(lib, "Engine_Debug")
#else
#pragma comment(lib, "Engine")
#endif // _DEBUG

AR3D_USING

enum RESOLUTION_TYPE
{
	RT_SMALL,
	RT_HD,
	RT_FHD,
	RT_MAX
};


static RESOLUTION g_tRS[RT_MAX] =
{
	RESOLUTION(800, 600),
	RESOLUTION(1280, 720),
	RESOLUTION(1920, 1280)
};



// Resolution
