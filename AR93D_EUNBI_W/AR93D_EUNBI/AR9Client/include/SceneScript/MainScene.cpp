#include "MainScene.h"
#include "GameObject/GameObject.h"
#include "Component/Renderer.h"
#include "Component/Transform.h"
#include "Component/FreeCamera.h"
#include "Scene/Scene.h"
#include "Scene/Layer.h"
#include "../ObjectScript/Player.h"
#include "../ObjectScript/Bullet.h"
#include "../ObjectScript/Minion.h"
#include "Component/Camera.h"
#include "Component/CameraArm.h"
#include "Component/ColliderSphere.h"
#include "Component/ColliderRect.h"
#include "Component/Material.h"
#include "Component/Effect.h"
#include "Component/Animation2D.h"
#include "Resources/ResourcesManager.h"
#include "Resources/Texture.h"
#include "Resources/Mesh.h"
#include "Core/Scheduler.h"
#include "Component/UIBar.h"
#include "Component/UIBack.h"
#include "Component/Renderer2D.h"
#include "Component/Terrain.h"
#include "ObjInformation.h"
#include "../FreeCameraPlayer.h"
#include "Core/Input.h"
#include "Core/GameTotalManager.h"
#include "Component/Water.h"
#include "../ObjectScript/FarmObject.h"
#include "Component/UIButton.h"
#include "Scene/SceneManager.h"
#include "../Client.h"
#include "Component/ParticleSystem.h"
#include "Component/Text.h"
#include "Component/Light.h"
#include "../ObjectScript/BuildingScript.h"
#include "Component/LightPoint.h"
#include "Component/LampLight.h"

CMainScene::CMainScene() :
	m_pPlayerObj(NULL)
{
	m_LampCount = 0;
}


CMainScene::~CMainScene()
{
	SAFE_RELEASE(m_pPlayerObj);
	Safe_Release_Map(m_mapLandDirt);
}


bool CMainScene::Init()
{

	//조명 생성
	
	//키 생성 
	m_LampCount = 0;
	CreateKey();
	CLayer*	pLayer = m_pScene->CreateLayer("MakeCropMakerLayer");
	SAFE_RELEASE(pLayer);
	pLayer = m_pScene->FindLayer("DefaultLayer");

	//------------CAMERA------------//
	

	CGameObject*	pMainCameraObj = m_pScene->GetMainCameraObj();

	CFreeCamera*	pArm = pMainCameraObj->AddComponent<CFreeCamera>("FreeCamera");

	SAFE_RELEASE(pArm);
	//CColliderSphere* pCameraCollider = pMainCameraObj->AddComponent<CColliderSphere>("CameraCollider");
	//pCameraCollider->SetSphereInfo(pMainCameraObj->GetTransform()->GetWorldPos() + 100,
	//	100.f);
	//pCameraCollider->SetColliderGroup(CG_NORMAL);
	//SAFE_RELEASE(pCameraCollider);

	SAFE_RELEASE(pMainCameraObj);
//	SAFE_RELEASE(pCameraObj);

	//-------------PLAYERETC---------//

	CGameObject*	pPlayerObj = CGameObject::Create("PlayerObj");
	pPlayerObj->AddRef();
	CTransform*	pTransform = pPlayerObj->GetTransform();
	pTransform->SetWorldPos(390.f, 30.f, 210.f);
	pTransform->SetWorldScale(0.1f, 0.1f, 0.1f);
	pTransform->SetLocalRotX(80.12f);

	SAFE_RELEASE(pTransform);

	CRenderer*	pRenderer = pPlayerObj->AddComponent<CRenderer>("Renderer");
	pRenderer->SetMesh("WorkerMesh", L"Worker.msh", FLT_MESH);
	pRenderer->SetShader(STANDARD_TEXNORMAL_ANIM_SHADER);
	pRenderer->SetInputLayout(TEXNORMAL_ANIM_INPUT);
	// 0 : 카툰렌더링 1 : 카툰&아웃라인, 2: 아웃라인만, 3: 일반 렌더링 , 
	pRenderer->SetRenderFlag(RF_NORMAL);
	//pRenderer->SetRenderState(ALPHABLEND);

	CAnimation3D*	pPlayerAni = pPlayerObj->AddComponent<CAnimation3D>("PlayerAni");
	pPlayerAni->LoadBone("Worker.bne");
	pPlayerAni->Load("Worker.anm"); // Idle, Walk, Plant, Dig And Seed
	pPlayerAni->SetCurClipName("Idle");
	pPlayerAni->SetDefaultClipName("Idle");
	SAFE_RELEASE(pPlayerAni);

	CMaterial*	pPlayerMaterial = pRenderer->GetMaterial();
	//pPlayerMaterial->SetNormalMapTexture("Linear", "PlayerMeshNormalTex", L"Test.fbm/OccupyGuy_normal.jpg", MESH_PATH);
	//pPlayerMaterial->SetSpecularTexture("Linear", "PlayerMeshSpecularTex", L"Test.fbm/OccupyGuy_spec.jpg", MESH_PATH);
	SAFE_RELEASE(pPlayerMaterial);
	SAFE_RELEASE(pRenderer);

	// 플레이어 스크립트 추가
	CPlayer*	pPlayer = pPlayerObj->AddComponent<CPlayer>("Player");
	SAFE_RELEASE(pPlayer);
	pLayer->AddObject(pPlayerObj);

	/*CCamera*	pMainCamera = m_pScene->GetMainCamera();

	pMainCamera->Attach(pPlayerObj, DxVector3(0.f, 0.f, -5.f));

	SAFE_RELEASE(pMainCamera);*/
	SAFE_RELEASE(pPlayerObj);

	//////// 물
	CLayer*	pMapLayer = m_pScene->FindLayer("MapLayer");

	CGameObject*	pWaterObj = CGameObject::Create("pWaterObj");

	CWater*	pWater = pWaterObj->AddComponent<CWater>("Water");

	pWater->CreateWater("Water", 1280, 1280);
	pWater->SetBaseTexture("Water",
		L"Terrain/Water_Base_Texture_0.dds");
	pWater->SetDetailTexture("WaterDetail",
		L"Terrain/Water_Detail_Texture_0.dds");

	pMapLayer->AddObject(pWaterObj);
	SAFE_RELEASE(pWater);
	SAFE_RELEASE(pWaterObj);

	// 지형

	CGameObject*	pTerrainObj = CGameObject::Create("Terrain");

	CTerrain*	pTerrain = pTerrainObj->AddComponent<CTerrain>("Terrain");

	pTerrain->CreateTerrain("Terrain", 129, 129, 5, 5, "Terrain/terrain.bmp");
	pTerrain->SetBaseTexture("TerrainDiffuse",
		L"Terrain/TerrainTexture1.bmp");
	/*pTerrain->SetNormalTexture("TerrainNormal",
	L"Terrain/TerrainTexture1_NRM.bmp");
	pTerrain->SetSpecularTexture("TerrainSpc",
	L"Terrain/TerrainTexture1_SPEC.bmp");*/

	vector<wstring>	vecMultiTex;

	vecMultiTex.push_back(L"Terrain/SAND_00.bmp");
	vecMultiTex.push_back(L"Terrain/farmfloor.bmp");
	vecMultiTex.push_back(L"Terrain/TerrainTexture3.bmp");

	pTerrain->SetSplatTexture("TerrainSplat", vecMultiTex);

	// Normal 세팅
	vecMultiTex.clear();

	//vecMultiTex.push_back(L"Terrain/ROCK_01+MOSS_NRM.bmp");

	//pTerrain->SetSplatNormalTexture("TerrainSplatNormal", vecMultiTex);

	//vecMultiTex.clear();

	//vecMultiTex.push_back(L"Terrain/ROCK_01+MOSS_SPEC.bmp");

	//pTerrain->SetSplatSpecularTexture("TerrainSplatSpecular", vecMultiTex);

	//Splat Alpha Setting
	vecMultiTex.clear();

	vecMultiTex.push_back(L"Terrain/Terrain1_real.bmp");
	vecMultiTex.push_back(L"Terrain/Terrain2_r.bmp");
	vecMultiTex.push_back(L"Terrain/Terrain3.bmp");

	pTerrain->SetSplatAlphaTexture("TerrainSplatAlpha", vecMultiTex);
	pTerrain->SetSplatCount(vecMultiTex.size());
	vecMultiTex.clear();

	SAFE_RELEASE(pTerrain);

	pMapLayer->AddObject(pTerrainObj);

	SAFE_RELEASE(pTerrainObj);

	SAFE_RELEASE(pMapLayer);


	//
	SAFE_RELEASE(pLayer);
	//---------------------<OBJECT , UI 로딩>----------------------------//
	GetInfoFromTool("ObjInfoList.txt");
	MakeUI();
	return true;
}

void CMainScene::CreateKey()
{
	GET_SINGLE(CInput)->CreateKey("OnOffCollider", VK_F1);
	GET_SINGLE(CInput)->CreateKey("SetNight", VK_F2);
}

void CMainScene::Input(float fTime)
{
	if (KEYPRESS("OnOffCollider"))
	{
		if (GET_SINGLE(CInput)->m_OnOffCollider == true)
			GET_SINGLE(CInput)->m_OnOffCollider = false;
		else
			GET_SINGLE(CInput)->m_OnOffCollider = true;
	}
	if (KEYPRESS("SetNight"))
	{
		CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
		CLight*	pGlobalLight = (CLight*)pScene->FindLight("GlobalLight");

		CTransform*	pTransform = pGlobalLight->GetTransform();

		if (GET_SINGLE(CGameTotalManager)->GetNight())
		{
			GET_SINGLE(CGameTotalManager)->SetNight(false);

		}
		else
		{
			GET_SINGLE(CGameTotalManager)->SetNight(true);
		}


		SAFE_RELEASE(pTransform);

		SAFE_RELEASE(pGlobalLight);
	}

}

void CMainScene::Update(float fTime)
{
	static bool	bEnable = true;
	static bool	bPush;

	if (GetAsyncKeyState(VK_RETURN) & 0x8000)
	{
		bPush = true;
	}

	else if (bPush)
	{
		bPush = false;
		bEnable = !bEnable;
		CLayer*	pLayer = m_pScene->FindLayer("DefaultLayer");

		pLayer->SetEnable(bEnable);

		SAFE_RELEASE(pLayer);
	}

	//if (GetAsyncKeyState(VK_F1) & 0x8000)
	//{
	//	m_pBar->AddValue(-300.f * fTime);
	//}

	//if (GetAsyncKeyState(VK_F2) & 0x8000)
	//{
	//	m_pBar->AddValue(300.f * fTime);
	//}

	/*if (GET_SINGLE(CInput)->m_MouseClickOn == true)
	{
	MakeFarmObject(CF_TOMATO, ROW, "Tomato");
	}*/
}

int CMainScene::MonsterRespawn(float fTime)
{
	static int iCount = 0;

	if (iCount == 15)
		return 0;

	++iCount;

	DxVector3	vPos = DxVector3(rand() % 6 - 2.5f, rand() % 4 - 1.5f, rand() % 8 + 3.f);

	CGameObject*	pMinionObj = CGameObject::CreateClone("Minion");

	CTransform*	pTransform = pMinionObj->GetTransform();

	pTransform->SetWorldPos(vPos);

	SAFE_RELEASE(pTransform);

	CMinion*	pMinion = pMinionObj->FindComponentFromTypeID<CMinion>();

	pMinion->SetTarget(m_pPlayerObj);

	SAFE_RELEASE(pMinion);

	CLayer*	pLayer = m_pScene->FindLayer("DefaultLayer");

	pLayer->AddObject(pMinionObj);

	SAFE_RELEASE(pMinionObj);

	SAFE_RELEASE(pLayer);

	return 0;
}

bool CMainScene::GetInfoFromTool(const char * pFileName)
{
	m_ObjInfoFromTool = GET_SINGLE(CObjInformation)->LoadObjInfo(pFileName);
	if (m_ObjInfoFromTool.size() <= 0)
		return false;

	return LoadObjects(m_ObjInfoFromTool);
}

bool CMainScene::LoadObjects(list<OBJINFO> ObjInfoFromTool)
{
	CLayer*	pLayer = m_pScene->FindLayer("DefaultLayer");

	list<OBJINFO>::iterator iter;
	list<OBJINFO>::iterator iterEnd = ObjInfoFromTool.end();

	for (iter = ObjInfoFromTool.begin(); iter != iterEnd; ++iter)
	{
		CGameObject*	pObj = CGameObject::Create((*iter).strTag);

		pObj->AddRef();

		CRenderer* pRenderer = pObj->AddComponent<CRenderer>("Renderer");

		string temp = (*iter).strTag;
		temp += ".FBX";
		if (strcmp((*iter).strTag.c_str(), "Fence") == 0)
		{

			pRenderer->SetMesh((*iter).strTag, L"Fence.FBX", FLT_BOTH, OBJECT_MESH_PATH);
		}
		else
			pRenderer->SetMesh((*iter).strTag, StringToTCHAR(temp), FLT_BOTH, OBJECT_MESH_PATH);
		pRenderer->SetShader(STANDARD_BUMP_SHADER);
		pRenderer->SetInputLayout("BumpInputLayout");
		pRenderer->SetRenderFlag(RF_CARTOON_ONLY);
		pRenderer->SetRenderState(CULLING_CW);

		string Temptreestring = (*iter).strTag.c_str();
		Temptreestring.pop_back();
		Temptreestring.pop_back();

		CTransform* pTransform = pObj->GetTransform();
		
		pTransform->SetWorldScale((*iter).WorldScale.x, (*iter).WorldScale.y, (*iter).WorldScale.z);
		pTransform->SetWorldPos((*iter).WorldPos.x, (*iter).WorldPos.y, (*iter).WorldPos.z);
		pTransform->SetWorldRot((*iter).WorldRot.x, (*iter).WorldRot.y, (*iter).WorldRot.z);

		if (strcmp((*iter).strTag.c_str(), "Fence") == 0)
		{
			pTransform->SetWorldScale(3.f, 3.f, 3.f);
		}
		
		else if (strcmp(Temptreestring.c_str(), "tree ") == 0)
		{
			pTransform->SetWorldScale(0.05f, 0.05f, 0.05f);
		}
		else if (strcmp((*iter).strTag.c_str(), "Medieval_Windmill") == 0)
		{
			pTransform->SetWorldScale(0.2f, 0.2f, 0.2f);
			pTransform->SetLocalRotZ(80.12f);
			pTransform->SetLocalRotY(-80.12f);
		}
		else if (strcmp((*iter).strTag.c_str(), "lamp") == 0)
		{
			pTransform->SetLocalRotZ(80.12f);
			pTransform->SetLocalRotY(-80.12f);

			pTransform->SetWorldScale(0.1f, 0.1f, 0.1f);

			CLampLight* buildingScript = pObj->AddComponent<CLampLight>("LampLight");
			//
			SAFE_RELEASE(buildingScript);
			GET_SINGLE(CGameTotalManager)->SetLightPosition(pTransform->GetWorldPos());
	
		}

		else
			pTransform->SetWorldScale(0.01f, 0.01f, 0.01f);

		pTransform->SetLocalRotX(80.12f);

		if (strcmp((*iter).strTag.c_str(), "Medieval_Windmill") == 0 || 
			strcmp((*iter).strTag.c_str(), "BarnHouseA1") == 0 ||
			strcmp((*iter).strTag.c_str(), "BarnHouseA2") == 0 || 
			strcmp((*iter).strTag.c_str(), "WaterTowerA1") == 0)
		{
			CBuildingScript* buildingScript = pObj->AddComponent<CBuildingScript>("BuildingScript");
			SAFE_RELEASE(buildingScript);

		}

		else
		{

			//충돌체 추가
			CColliderSphere*	pSphere = pObj->AddComponent<CColliderSphere>("Collider");

			if (strcmp((*iter).strTag.c_str(), "Fence") == 0)
			{
				pSphere->SetSphereInfo(pTransform->GetWorldPos(),
					pRenderer->GetMesh()->GetRadius() * 0.5);
			}
			
			else
				pSphere->SetSphereInfo(pTransform->GetWorldPos(),
					pRenderer->GetMesh()->GetRadius() * 0.01);

			
			SAFE_RELEASE(pSphere);
		}
		//// 재질 정보 변경
		CMaterial* pMaterial = pRenderer->GetMaterial();
		if (strcmp((*iter).strTag.c_str(), "Fence") == 0)
		{
			pMaterial->SetDiffuseTexture("Linear", "Fence", L"fence_texture.png");
		}
		else if (strcmp(Temptreestring.c_str(), "tree ") == 0)
		{
			pMaterial->SetDiffuseTexture("Linear", "TreeTexture", L"Treeficus.png");
		}
		else if (strcmp((*iter).strTag.c_str(), "Medieval_Windmill") == 0)
		{
			pMaterial->SetDiffuseTexture("Linear", "Medieval_Windmill", L"Medieval_Windmill.png");
		}
		else if (strcmp((*iter).strTag.c_str(), "lamp") == 0)
		{
			pMaterial->SetDiffuseTexture("Linear", "lamp", L"lamp.png");
		}
		else
		{
			pMaterial->SetDiffuseTexture("Linear", "Atlas", L"FarmA1_Atlas_col.jpg");
			//pMaterial->SetSpecularTexture("Linear", "AtlasSpec", L"FarmA1_Atlas_col_SPEC.bmp");
			//pMaterial->SetNormalMapTexture("Linear", "AtlasNormal", L"FarmA1_Atlas_col_NRM.bmp");

		}

		if (strcmp((*iter).strTag.c_str(), "lamp") == 0)
		{

		////Light추가 
		//CGameObject*	pLight = m_pScene->CreateLight((*iter).strTag.c_str(), LT_POINT);

		//pTransform = pLight->GetTransform();

		//pTransform->SetWorldPos(pTransform->GetWorldPos().x, pTransform->GetWorldPos().y, pTransform->GetWorldPos().z);

		//SAFE_RELEASE(pTransform);
		}

		//

		SAFE_RELEASE(pMaterial);

		SAFE_RELEASE(pRenderer);

		pLayer->AddObject(pObj);

		SAFE_RELEASE(pTransform);
		SAFE_RELEASE(pObj);

	}
	SAFE_RELEASE(pLayer);
	return true;
}


TCHAR* CMainScene::StringToTCHAR(const string& s)
{
	tstring tstr;
	const char* all = s.c_str();
	int len = 1 + strlen(all);
	wchar_t* t = new wchar_t[len];
	if (NULL == t) throw std::bad_alloc();
	mbstowcs(t, all, len);
	return (TCHAR*)t;
}

void CMainScene::MakeEffect(const DxVector3 & Pos)
{
	CGameObject*	pEffectObj = CGameObject::Create("MouseEffect", false);

	CRenderer* pRenderer = pEffectObj->AddComponent<CRenderer>("MouseEffectRenderer");

	pRenderer->SetMesh("PosMesh");
	pRenderer->SetShader(EFFECT_SHADER);
	pRenderer->SetInputLayout("PosInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	// 재질 정보 변경
	CMaterial* pMaterial = pRenderer->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "allow", L"allow.png");
	/*pMaterial->SetMaterialInfo(DxVector3(1.f, 0.f, 0.f), DxVector3(0.2f, 0.f, 0.f), DxVector3(1.f, 0.f, 0.f),
	DxVector3(1.f, 0.f, 0.f), 3.2f);*/

	SAFE_RELEASE(pMaterial);


	CEffect*	pEffect = pEffectObj->AddComponent<CEffect>("Effect");

	SAFE_RELEASE(pEffect);

	CAnimation2D*	pAnimation2D = pEffectObj->AddComponent<CAnimation2D>("EffectAnimation");

	vector<wstring>	vecExplosion;

	for (int i = 0; i <= 2; ++i)
	{
		TCHAR	strExplosion[MAX_PATH] = {};
		wsprintf(strExplosion, L"Effect/arrow/arrow%d.png", i);

		vecExplosion.push_back(strExplosion);
	}
	//GET_SINGLE(CResourcesManager)->LoadTexture("Explosion", vecExplosion);
	/*pAnimation2D->AddAnimationClip("HPPotion", A2D_ATLAS,
	AO_ONCE_DESTROY, 5, 4, 1.f, 0, 0.f, "HPPotion");*/
	pAnimation2D->AddAnimationClip("Allow", A2D_FRAME,
		AO_ONCE_DESTROY, vecExplosion.size(), 1, 0.3f, 0, 0.f, "arrow", 11, &vecExplosion);

	pAnimation2D->Start();

	SAFE_RELEASE(pAnimation2D);


	CTransform*	pTransform = pEffectObj->GetTransform();

	pTransform->SetWorldScale(10.f, 10.f, 10.f);
	pTransform->SetWorldPos(Pos.x, Pos.y + 5.f, Pos.z);

	SAFE_RELEASE(pTransform);

	CLayer*	pLayer = m_pScene->FindLayer("DefaultLayer");

	pLayer->AddObject(pEffectObj);


	SAFE_RELEASE(pLayer);
	SAFE_RELEASE(pRenderer);
	SAFE_RELEASE(pEffectObj);
}

void CMainScene::MakeFarmObject(CGameObject * pObj, float fTime)
{

	CGameObject*	pFarmObject = CGameObject::Create("FarmObject");
	pFarmObject->AddRef();

	CFarmObject* pLandDirt = pFarmObject->AddComponent<CFarmObject>("LandDitrt");
	pLandDirt->SetCropFlag(pObj->GetTag());

	// 플레이어 스크립트 추가
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer* pLayer = pScene->FindLayer("DefaultLayer");
	pLayer->AddObject(pFarmObject);

	//MainCamera
	CTransform*	pMainCamera = m_pScene->GetMainCamera()->GetTransform();
	
	pMainCamera->SetLookAt(pFarmObject);

	pMainCamera->SetWorldPos(pFarmObject->GetTransform()->GetWorldPos().x + 20,
		pFarmObject->GetTransform()->GetWorldPos().y + 30,
		pFarmObject->GetTransform()->GetWorldPos().z + 40);

	SAFE_RELEASE(pMainCamera); 

	SAFE_RELEASE(pLandDirt);

	m_mapLandDirt.insert(make_pair(pObj->GetTag(), pFarmObject));

	SAFE_RELEASE(pLayer);
	SAFE_RELEASE(pFarmObject);
}

void CMainScene::MakeUI()
{
	//------------------ < Inventory Button 생성 > ----------------------

	CGameObject* pInventoryBtn = CGameObject::Create("InventoryButton");

	CTransform*	pTransform = pInventoryBtn->GetTransform();

	pTransform->SetWorldScale(150.f, 150.f, 1.f);
	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth -450, g_tRS[RT_FHD].iHeight - 100.f, 0.f);

	CRenderer2D*	pRenderer = pInventoryBtn->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	CMaterial* pMaterial = pRenderer->GetMaterial();
	pMaterial->SetDiffuseTexture("Linear", "StartButton", L"UI/InventoryUI.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	CUIButton* pButton = pInventoryBtn->AddComponent<CUIButton>("Button");
	pButton->SetCallback(this, &CMainScene::MakeInventory);

	SAFE_RELEASE(pButton);
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	pLayer = m_pScene->FindLayer("UILayer");
	pLayer->AddObject(pInventoryBtn);

	SAFE_RELEASE(pInventoryBtn);

	SAFE_RELEASE(pTransform);

	//------------------ < Crops Button 생성 > ----------------------

	CGameObject* pCropsBtn = CGameObject::Create("InventoryButton");

	pTransform = pCropsBtn->GetTransform();

	pTransform->SetWorldScale(150.f, 150.f, 1.f);
	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth - 300, g_tRS[RT_FHD].iHeight - 100.f, 0.f);

	pRenderer = pCropsBtn->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	pMaterial = pRenderer->GetMaterial();
	pMaterial->SetDiffuseTexture("Linear", "CropsButton", L"UI/CropsUI.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	pButton = pCropsBtn->AddComponent<CUIButton>("Button");

	pButton->SetCallback(this, &CMainScene::MakeCropMaker);

	SAFE_RELEASE(pButton);
	pLayer->AddObject(pCropsBtn);

	SAFE_RELEASE(pCropsBtn);

	SAFE_RELEASE(pTransform);

	//------------------ < Inventory Button 생성 > ----------------------

	CGameObject* pAXBtn = CGameObject::Create("InventoryButton");

	pTransform = pAXBtn->GetTransform();

	pTransform->SetWorldScale(150.f, 150.f, 1.f);
	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth - 150, g_tRS[RT_FHD].iHeight - 100.f, 0.f);

	pRenderer = pAXBtn->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	pMaterial = pRenderer->GetMaterial();
	pMaterial->SetDiffuseTexture("Linear", "AXButton", L"UI/AXUI.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	pButton = pAXBtn->AddComponent<CUIButton>("Button");
	SAFE_RELEASE(pButton)

	pLayer->AddObject(pAXBtn);

	SAFE_RELEASE(pAXBtn);

	SAFE_RELEASE(pLayer);
	SAFE_RELEASE(pTransform);

	
	
}

void CMainScene::MakeInventory(CGameObject * pObj, float fTime)
{
	MakeInventory();
	MakeCloseButton();
	//------------------ < Inventory 생성 > ----------------------
	CGameObject*	pInventory = CGameObject::Create("Inventory");

	CRenderer2D*	pRenderer2D = pInventory->AddComponent<CRenderer2D>("Renderer");

	pRenderer2D->SetMesh("UIMesh");
	pRenderer2D->SetShader(UI_SHADER);
	pRenderer2D->SetInputLayout("TexInputLayout");
	pRenderer2D->SetRenderState(ALPHABLEND);

	// 재질 정보 변경
	CMaterial* pMaterial = pRenderer2D->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "Inventory", L"UI/Inventory2.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer2D);

	CColliderRect*	pInvenColl = pInventory->AddComponent<CColliderRect>("Collider");

	pInvenColl->SetRectInfo(0.f, 0.f, g_tRS[RT_FHD].iWidth / 3, g_tRS[RT_FHD].iHeight);

	SAFE_RELEASE(pInvenColl);

	CTransform* pTransform = pInventory->GetTransform();

	pTransform->SetWorldScale(g_tRS[RT_FHD].iWidth / 3, g_tRS[RT_FHD].iHeight, 0.f);

	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth / 3 * 2, 0.f, 0.f);
	pTransform->SetPivot(0.f, 0.f, 0.f);

	SAFE_RELEASE(pTransform);

	CUIBack*	pInvenBack = pInventory->AddComponent<CUIBack>("UIBack");

	SAFE_RELEASE(pInvenBack);

	CLayer*	pLayer = m_pScene->FindLayer("MakeCropMakerLayer");

	pLayer->AddObject(pInventory);
	pLayer->SetCollisionOn(true);

	SAFE_RELEASE(pTransform);
	SAFE_RELEASE(pLayer);
	SAFE_RELEASE(pInventory);
}

void CMainScene::MakeInventory()
{
		for (int i = 0; i < 9; ++i)
		{

			string temp = "UI/CropsUI/";
			temp += GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName;
			temp += ".png";

			CGameObject* pUI = CGameObject::Create(GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName);

			CTransform*	pTransform = pUI->GetTransform();

			pTransform->SetWorldScale(80.f, 80.f, 1.f);
			pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth - 550 , i * 100 + 250, 0.f);
			pTransform->SetPivot(0.f, 0.f, 0.f);

			CRenderer2D*	pRenderer = pUI->AddComponent<CRenderer2D>("Renderer");
			pRenderer->SetMesh("UIMesh");
			pRenderer->SetShader(UI_SHADER);
			pRenderer->SetInputLayout("TexInputLayout");
			pRenderer->SetRenderState(ALPHABLEND);

			CMaterial* pMaterial = pRenderer->GetMaterial();
			pMaterial->SetDiffuseTexture("Linear", GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName, StringToTCHAR(temp));

			SAFE_RELEASE(pMaterial);

			SAFE_RELEASE(pRenderer); 
			CUIBack* pButton = pUI->AddComponent<CUIBack>(GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName);

			SAFE_RELEASE(pButton);

			CText*	pText = pUI->AddComponent<CText>("Text");

			pText->SetArea(100.f, 0.f, 400.f, 100.f);
			pText->SetShadow(true);
			pText->SetText(intToWstring(GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).num));

			CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
			CLayer*	pLayer = m_pScene->FindLayer("MakeCropMakerLayer");
			pLayer->AddObject(pUI);

			SAFE_RELEASE(pUI);

			SAFE_RELEASE(pTransform);

		}
		for (int i = 9; i < 12; ++i)
		{

			string temp = "UI/CropsUI/";
			temp += GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName;
			temp += ".png";

			CGameObject* pUI = CGameObject::Create(GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName);

			CTransform*	pTransform = pUI->GetTransform();

			pTransform->SetWorldScale(80.f, 80.f, 1.f);
			pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth - 300 , (i - 9)* 100 + 250, 0.f);
			pTransform->SetPivot(0.f, 0.f, 0.f);

			CRenderer2D*	pRenderer = pUI->AddComponent<CRenderer2D>("Renderer");
			pRenderer->SetMesh("UIMesh");
			pRenderer->SetShader(UI_SHADER);
			pRenderer->SetInputLayout("TexInputLayout");
			pRenderer->SetRenderState(ALPHABLEND);

			CMaterial* pMaterial = pRenderer->GetMaterial();
			pMaterial->SetDiffuseTexture("Linear", GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName, StringToTCHAR(temp));

			SAFE_RELEASE(pMaterial);

			SAFE_RELEASE(pRenderer);
			CUIBack* pButton = pUI->AddComponent<CUIBack>(GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName);

			SAFE_RELEASE(pButton);

			CText*	pText = pUI->AddComponent<CText>("Text");

			pText->SetArea(100.f, 0.f, 400.f, 100.f);
			pText->SetShadow(true);
			pText->SetText(intToWstring(GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).num));

			CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
			CLayer*	pLayer = m_pScene->FindLayer("MakeCropMakerLayer");
			pLayer->AddObject(pUI);

			SAFE_RELEASE(pUI);

			SAFE_RELEASE(pTransform);

		}
}

void CMainScene::MakeCropMaker(CGameObject * pObj, float fTime)
{
	MakeCropMaker();
	MakeCloseButton();
	//------------------ < Inventory 생성 > ----------------------
	CGameObject*	pInventory = CGameObject::Create("CropMaker");

	CRenderer2D*	pRenderer2D = pInventory->AddComponent<CRenderer2D>("CropMakerRenderer");

	pRenderer2D->SetMesh("UIMesh");
	pRenderer2D->SetShader(UI_SHADER);
	pRenderer2D->SetInputLayout("TexInputLayout");
	pRenderer2D->SetRenderState(ALPHABLEND);


	// 재질 정보 변경
	CMaterial* pMaterial = pRenderer2D->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "CropMaker", L"UI/Inventory.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer2D);

	CColliderRect*	pInvenColl = pInventory->AddComponent<CColliderRect>("InvenColl");

	pInvenColl->SetRectInfo(0.f, 0.f, g_tRS[RT_FHD].iWidth / 3, g_tRS[RT_FHD].iHeight);

	SAFE_RELEASE(pInvenColl);

	CTransform* pTransform = pInventory->GetTransform();

	pTransform->SetWorldScale(g_tRS[RT_FHD].iWidth / 3, g_tRS[RT_FHD].iHeight, 0.f);

	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth /3 *2, 0.f, 0.f);
	pTransform->SetPivot(0.f, 0.f, 0.f);

	SAFE_RELEASE(pTransform);

	CUIBack*	pInvenBack = pInventory->AddComponent<CUIBack>("UIBack");

	SAFE_RELEASE(pInvenBack); 
	
	CLayer*	pLayer = m_pScene->FindLayer("MakeCropMakerLayer");

	pLayer->AddObject(pInventory);
	pLayer->SetCollisionOn(true);

	SAFE_RELEASE(pTransform);
	SAFE_RELEASE(pLayer);
	SAFE_RELEASE(pInventory);

	
}

void CMainScene::MakeCropMaker()
{
	for (int i = 0; i < 9; ++i)
	{

		string temp = "UI/CropsUI/";
		temp += GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName;
		temp += ".png";


		CGameObject* pUI = CGameObject::Create(GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName);

		CTransform*	pTransform = pUI->GetTransform();

		pTransform->SetWorldScale(150.f, 150.f, 1.f);
		pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth - 550 + (i % 3 * 150), (i / 3) * 200 + 300, 0.f);
		pTransform->SetPivot(0.f, 0.f, 0.f);

		CRenderer2D*	pRenderer = pUI->AddComponent<CRenderer2D>("Renderer");
		pRenderer->SetMesh("UIMesh");
		pRenderer->SetShader(UI_SHADER);
		pRenderer->SetInputLayout("TexInputLayout");
		pRenderer->SetRenderState(ALPHABLEND);

		CMaterial* pMaterial = pRenderer->GetMaterial();
		pMaterial->SetDiffuseTexture("Linear", GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName, StringToTCHAR(temp));

		SAFE_RELEASE(pMaterial);

		SAFE_RELEASE(pRenderer);

		CUIButton* pButton = pUI->AddComponent<CUIButton>(GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName);

		//pButton->SetCallback(this, &CStartScene::StartButton);

		pButton->SetCallback(this, &CMainScene::MakeFarmObject);

		SAFE_RELEASE(pButton);
		CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
		CLayer*	pLayer = m_pScene->FindLayer("MakeCropMakerLayer");
		pLayer->AddObject(pUI);

		SAFE_RELEASE(pUI);

		SAFE_RELEASE(pTransform);

	}
}

void CMainScene::MakeCloseButton()
{

	//==========================< close button >======================//

	CGameObject* pUI = CGameObject::Create("CloseButton");

	CTransform*	pTransform = pUI->GetTransform();

	pTransform->SetWorldScale(100.f, 100.f, 1.f);
	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth - 100, 20, 0.f);
	pTransform->SetPivot(0.f, 0.f, 0.f);

	CRenderer2D*	pRenderer = pUI->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	CMaterial* pMaterial = pRenderer->GetMaterial();
	pMaterial->SetDiffuseTexture("Linear", "CloseButton", L"UI/CloseButton.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	CUIButton* pButton = pUI->AddComponent<CUIButton>("CloseButton");

	//pButton->SetCallback(this, &CStartScene::StartButton);

	pButton->SetCallback(this, &CMainScene::CloseLayer);

	SAFE_RELEASE(pButton);


	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	pLayer = m_pScene->FindLayer("MakeCropMakerLayer");
	pLayer->AddObject(pUI);

	SAFE_RELEASE(pUI);
	SAFE_RELEASE(pTransform);
}

void CMainScene::MakeParticle(DxVector3 Position, float fLifeTime)
{

	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	pLayer = m_pScene->FindLayer("MakeCropMakerLayer");
	CGameObject*	pParticleObj = CGameObject::Create("Particle", true);

	CTransform*	pParticleTr = pParticleObj->GetTransform();

	pParticleTr->SetWorldPos(0.f, 0.f, 3.f);

	SAFE_RELEASE(pParticleTr);

	CParticleSystem*	pParticle = pParticleObj->AddComponent<CParticleSystem>("Particle");

	pParticle->SetParticleInfo();
	pParticle->SetParticleTexture("ParticleSample",
		L"Effect/butterfly.png");
	pParticle->SetParticleLight(true);

	SAFE_RELEASE(pParticle);

	SAFE_RELEASE(pParticleObj);

	// 파티클 생성
	for (int i = 0; i < 5; ++i)
	{
		pParticleObj = CGameObject::CreateClone("Particle");

		CTransform*	pParticleTr = pParticleObj->GetTransform();
		pParticleTr->SetWorldPos(Position);
		SAFE_RELEASE(pParticleTr);

		pLayer->AddObject(pParticleObj);

		SAFE_RELEASE(pParticleObj);
	}
	
	SAFE_RELEASE(pLayer);

}

void CMainScene::CloseLayer(CGameObject * pObj, float fTime)
{
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	pLayer = m_pScene->FindLayer("MakeCropMakerLayer");
	
	pLayer->EraseAllObject();
	pLayer->SetCollisionOn(false);

	SAFE_RELEASE(pLayer);

}

wstring CMainScene::intToWstring(int inum)
{
	char tmp[10];
	char* ch;

	ch = _itoa(inum, tmp, 10);    // 정수형 -> 문자열 변환

	CString str = CString(ch);

	std::wstring wstr = (LPCTSTR)str;
	return wstr;
}
