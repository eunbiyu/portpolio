#include "LoadingScene.h"
#include "GameObject/GameObject.h"
#include "Component/Renderer.h"
#include "Component/Transform.h"
#include "Scene/Scene.h"
#include "Scene/SceneManager.h"
#include "Scene/Layer.h"
#include "Component/Renderer2D.h"
#include "Component/Material.h"
#include "LoadingThread.h"
#include "Core/ThreadManager.h"

CLoadingScene::CLoadingScene()
{
}

CLoadingScene::~CLoadingScene()
{
}

bool CLoadingScene::Init()
{
	CLayer*	pLayer = m_pScene->FindLayer("UILayer");

	CGameObject*	pBack = CGameObject::Create("BackObj");

	CTransform*	pTransform = pBack->GetTransform();

	pTransform->SetWorldScale(1280.f, 720.f, 1.f);

	SAFE_RELEASE(pTransform);

	CRenderer2D*	pRenderer = pBack->AddComponent<CRenderer2D>("Renderer");

	pRenderer->SetMesh("LTOrthoRect");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	// 재질 정보 변경
	CMaterial* pMaterial = pRenderer->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "LoadingSceneBack", L"StartScene/Lineage1.bmp");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	pLayer->AddObject(pBack);

	SAFE_RELEASE(pBack);

	SAFE_RELEASE(pLayer);

	m_pThread = GET_SINGLE(CThreadManager)->CreateThread<CLoadingThread>("LoadingThread");

	m_pThread->Start();

	return true;
}

void CLoadingScene::Update(float fTime)
{
	if (m_pThread->GetLoadComplete())
		m_pThread->Change();
}
