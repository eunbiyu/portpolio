#pragma once

#include "Core/Thread.h"
#include "Scene/Scene.h"

AR3D_USING

class CLoadingThread :
	public CThread
{
public:
	CLoadingThread();
	~CLoadingThread();

private:
	bool	m_bLoadComplete;
	CScene*	m_pScene;

public:
	bool GetLoadComplete()	const;
	void Change();

public:
	virtual void Run();
};
