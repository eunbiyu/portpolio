#include "StartScene.h"
#include "GameObject/GameObject.h"
#include "Component/Renderer.h"
#include "Component/Transform.h"
#include "Scene/Scene.h"
#include "Scene/SceneManager.h"
#include "Scene/Layer.h"
#include "../ObjectScript/Player.h"
#include "../ObjectScript/Bullet.h"
#include "../ObjectScript/Minion.h"
#include "Component/Camera.h"
#include "Component/CameraArm.h"
#include "Component/ColliderSphere.h"
#include "Component/Material.h"
#include "Component/Effect.h"
#include "Component/Animation2D.h"
#include "Resources/ResourcesManager.h"
#include "Resources/Texture.h"
#include "Component/Renderer2D.h"
#include "Component/UIButton.h"
#include "Core.h"
#include "Scene/SceneManager.h"
#include "Scene/Scene.h"
#include "MainScene.h"
#include "Core/ThreadManager.h"
#include "Core/Thread.h"
#include "LoadingThread.h"
#include "LoadingScene.h"
#include "Component/UIRadioButton.h"
#include "Component/UICheckBox.h"
#include "../Client.h"

CStartScene::CStartScene()
{
}


CStartScene::~CStartScene()
{
}

bool CStartScene::Init()
{
	CLayer*	pLayer = m_pScene->FindLayer("UILayer");

	CGameObject*	pBack = CGameObject::Create("BackObj");

	CTransform*	pTransform = pBack->GetTransform();
	pTransform->SetWorldScale(g_tRS[RT_FHD].iWidth, g_tRS[RT_FHD].iHeight, 1.f);
	pTransform->SetPivot(0.f, 0.f, 0.f);
	SAFE_RELEASE(pTransform);

	CRenderer2D*	pRenderer = pBack->AddComponent<CRenderer2D>("Renderer");

	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	// 재질 정보 변경
	CMaterial* pMaterial = pRenderer->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "StartSceneBack", L"StartScene/farm.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	pLayer->AddObject(pBack);

	SAFE_RELEASE(pBack);

	//------------------ < CHECKBOX 프로토타입 생성 > ----------------------

	CGameObject* pCheckBox = CGameObject::Create("CheckBox", true);

	pTransform = pCheckBox->GetTransform();

	pTransform->SetWorldScale(100.f, 100.f, 1.f);

	SAFE_RELEASE(pTransform);

	pRenderer = pCheckBox->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	pMaterial = pRenderer->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "CheckBoxOff", L"UIBox/CheckBox_Off.png");
	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);


	SAFE_RELEASE(pCheckBox);

	//CreateCheckBox(3);


	//------------------ < RADIOBUTTON 프로토타입 생성 > ----------------------

	CGameObject* pRadioBtn = CGameObject::Create("RadioButton", true);

	pTransform = pRadioBtn->GetTransform();

	pTransform->SetWorldScale(100.f, 100.f, 1.f);

	SAFE_RELEASE(pTransform);

	pRenderer = pRadioBtn->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	pMaterial = pRenderer->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "RadioButton", L"UIBox/RadioOff.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	
	SAFE_RELEASE(pRadioBtn);

	//CreateRadioButton(4);

	//------------------ < 시작버튼 생성 > ----------------------

	CGameObject* pStartBtn = CGameObject::Create("StartButton");

	pTransform = pStartBtn->GetTransform();

	pTransform->SetWorldScale(200.f, 100.f, 1.f);
	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth / 2.f , (g_tRS[RT_FHD].iHeight / 2.f )* 2.f - 200.f, 0.f);

	pRenderer = pStartBtn->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	pMaterial = pRenderer->GetMaterial();
	pMaterial->SetDiffuseTexture("Linear", "btn", L"StartScene/Menu_Start.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	CUIButton* pButton = pStartBtn->AddComponent<CUIButton>("Button");
	
	pButton->SetCallback(this, &CStartScene::StartButton);

	SAFE_RELEASE(pButton);
	pLayer->AddObject(pStartBtn);

	SAFE_RELEASE(pStartBtn);

	SAFE_RELEASE(pTransform);

	//------------------ < End버튼 만들기 > -------------------

	CGameObject* pEndBtn = CGameObject::Create("EndButton");

	pTransform = pEndBtn->GetTransform();

	pTransform->SetWorldScale(200.f, 100.f, 1.f);
	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth / 2.f, (g_tRS[RT_FHD].iHeight / 2.f)* 2.f - 100.f, 0.f);

	pRenderer = pEndBtn->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	pMaterial = pRenderer->GetMaterial();
	//재질정보 변경
	pMaterial->SetDiffuseTexture("Linear", "EndButton", L"StartScene/Menu_End.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	pButton = pEndBtn->AddComponent<CUIButton>("Button");

	pButton->SetCallback(this, &CStartScene::ExitButton);

	SAFE_RELEASE(pButton);

	pLayer->AddObject(pEndBtn);

	SAFE_RELEASE(pEndBtn);

	SAFE_RELEASE(pTransform);


	SAFE_RELEASE(pLayer);

	return true;
}

void CStartScene::Update(float fTime)
{
}

void CStartScene::ExitButton(CGameObject * pObj, float fTime)
{
	GET_SINGLE(CCore)->Exit();
}

void CStartScene::StartButton(CGameObject * pObj, float fTime)
{
	CScene* pScene = GET_SINGLE(CSceneManager)->CreateScene("MainScene");

	CMainScene* pLoad = pScene->CreateScript<CMainScene>();

	GET_SINGLE(CSceneManager)->ReplaceScene(pScene);
}

void CStartScene::SelectRadioButton(CGameObject * pObj, float fTime)
{
	//CLayer*	pLayer = m_pScene->FindLayer("UILayer");
	//list<CGameObject*>*	pRadioBtnObjList = pLayer->FindObjectsFromTag("RadioButton");
	//
	//list<CGameObject*>::iterator	iter;

	//for (iter = pRadioBtnObjList->begin(); iter != pRadioBtnObjList->end(); ++iter)
	//{
	//		//?? 왜 TypeID로는 검색이안되는지 .. >>	(*iter)->FindComponentFromTypeID<CUIRadioButton>();
	//	if ((*iter) != pObj) 
	//	{ 
	//		//메모리주소때문에 TypeID비교가안되고 Tag비교만됨. DLL에서 셋팅해준것이기때문에 
	//		CUIRadioButton*	pRadioBtn = (CUIRadioButton*)(*iter)->FindComponentFromTag("BasicRadio");
	//		//CUIRadioButton*	pRadioBtn = (CUIRadioButton*)(*iter)->FindComponentFromTypeID<CUIRadioButton>();
	//		pRadioBtn->SetClick(false);
	//		SAFE_RELEASE(pRadioBtn);
	//	}
	//} 

	//SAFE_RELEASE(pLayer);

}

void CStartScene::SelectCheckBox(CGameObject * pObj, float fTime)
{
}

void CStartScene::CreateRadioButton(int num)
{
	for (int i = 0; i < num; ++i)
	{
		DxVector3	vPos = DxVector3(100 * (i+1), 100.f, 0.f);

		CGameObject*	RadioBtnObj = CGameObject::CreateClone("RadioButton");

		CTransform*	pTransform = RadioBtnObj->GetTransform();

		pTransform->SetWorldPos(vPos);

		SAFE_RELEASE(pTransform);

		CUIRadioButton* pRadioButton = RadioBtnObj->AddComponent<CUIRadioButton>("RadioButton");
		
		pRadioButton->SetTag("BasicRadio");
		pRadioButton->SetCallback(this, &CStartScene::SelectRadioButton);

		SAFE_RELEASE(pRadioButton);

		CLayer*	pLayer = m_pScene->FindLayer("UILayer");

		pLayer->AddObject(RadioBtnObj);

		SAFE_RELEASE(RadioBtnObj);

		SAFE_RELEASE(pLayer);

	}
}

void CStartScene::CreateCheckBox(int num)
{
	for (int i = 0; i < num; ++i)
	{
		DxVector3	vPos = DxVector3(100 * (i + 1), 300.f, 0.f);

		CGameObject*	CheckBoxObj = CGameObject::CreateClone("CheckBox");

		CTransform*	pTransform = CheckBoxObj->GetTransform();

		pTransform->SetWorldPos(vPos);

		SAFE_RELEASE(pTransform);

		CUICheckBox* pCheckBox = CheckBoxObj->AddComponent<CUICheckBox>("CheckBox");

		pCheckBox->SetTag("BasicCheckBox");
		pCheckBox->SetCallback(this, &CStartScene::SelectCheckBox);
		 
		SAFE_RELEASE(pCheckBox);

		CLayer*	pLayer = m_pScene->FindLayer("UILayer");

		pLayer->AddObject(CheckBoxObj);

		SAFE_RELEASE(CheckBoxObj);

		SAFE_RELEASE(pLayer);

	}
}
