#include "LoadingThread.h"
#include "Core/Scheduler.h"
#include "Scene/SceneManager.h"
#include "Scene/Scene.h"
#include "MainScene.h"
#include "Scene/SceneManager.h"
#include "MainScene.h"


CLoadingThread::CLoadingThread() :
	m_bLoadComplete(false)
{
	
}

CLoadingThread::~CLoadingThread()
{
}

bool CLoadingThread::GetLoadComplete() const
{
	return m_bLoadComplete;
}

void CLoadingThread::Change()
{
	GET_SINGLE(CSceneManager)->ReplaceScene(m_pScene);
}

void CLoadingThread::Run()
{
	m_pScene = GET_SINGLE(CSceneManager)->CreateScene("MainScene");

	CMainScene* pMainScene = m_pScene->CreateScript<CMainScene>();

	m_bLoop = false;
	m_bLoadComplete = true;
}
