#pragma once

#include "Scene/SceneScript.h"
#include "GameObject/GameObject.h"
#include "Component/UIBar.h"

typedef std::basic_string<TCHAR> tstring;

AR3D_USING

class CMainScene :
	public CSceneScript
{
public:
	CMainScene();
	~CMainScene();

private:
	float		m_fMonsterRespawnTime;
	float		m_fMonsterRespawnLimitTime;
	CGameObject*	m_pPlayerObj;
	list<OBJINFO>	m_ObjInfoFromTool;
	unordered_map<string, class CGameObject*>	m_mapLandDirt;
	int				m_LampCount;

	//UIButton

public:
	virtual bool Init();
	
	void CreateKey();
	virtual void Input(float fTime);
	virtual void Update(float fTime);

public:
	int MonsterRespawn(float fTime);
	bool GetInfoFromTool(const char * pFileName);
	bool LoadObjects(list<OBJINFO> ObjInfoFromTool);
	TCHAR* StringToTCHAR(const string& s);

	//======================== < MAKER METHOD >  ========================// 
	virtual void MakeEffect(const DxVector3& Pos);
	void MakeFarmObject(CGameObject * pObj, float fTime);
	void MakeUI();
	void MakeInventory(CGameObject * pObj, float fTime);
	void MakeInventory();

	void MakeCropMaker(CGameObject * pObj, float fTime);
	void MakeCropMaker();

	void MakeCloseButton();
	void MakeParticle(DxVector3 Position, float fLifeTime = 10.f);
	void CloseLayer(CGameObject * pObj, float fTime);
	wstring intToWstring(int inum);
};

