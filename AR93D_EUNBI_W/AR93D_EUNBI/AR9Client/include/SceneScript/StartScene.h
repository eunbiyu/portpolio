#pragma once

#include "Scene/SceneScript.h"
#include "GameObject/GameObject.h"

AR3D_USING

class CStartScene :
	public CSceneScript
{
public:
	CStartScene();
	~CStartScene();

public:
	virtual bool Init();
	virtual void Update(float fTime);

public:
	void ExitButton(CGameObject* pObj, float fTime);
	void StartButton(CGameObject* pObj, float fTime);
	void SelectRadioButton(CGameObject* pObj, float fTime);
	void SelectCheckBox(CGameObject* pObj, float fTime);
	void CreateRadioButton(int num);
	void CreateCheckBox(int num);
	virtual void MakeEffect(const DxVector3& Pos) {};
};

