#pragma once

#include "Scene/SceneScript.h"

AR3D_USING

class CLoadingScene :
	public CSceneScript
{
public:
	CLoadingScene();
	~CLoadingScene();

private:
	class CLoadingThread*	m_pThread;

public:
	virtual bool Init();
	virtual void Update(float fTime);
};

