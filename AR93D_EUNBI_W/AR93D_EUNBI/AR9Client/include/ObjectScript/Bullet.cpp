#include "Bullet.h"
#include "GameObject/GameObject.h"
#include "Component/Renderer.h"
#include "Component/Transform.h"
#include "Scene/Scene.h"
#include "Component/Camera.h"
#include "Component/ColliderSphere.h"

CBullet::CBullet()
{
	SetTypeID<CBullet>();
}

CBullet::CBullet(const CBullet & bullet) :
	CScript(bullet)
{
	m_fSpeed = bullet.m_fSpeed;
	m_fDist = bullet.m_fDist;
}

CBullet::~CBullet()
{
}

bool CBullet::Init()
{
	m_fSpeed = 7.f;
	m_fDist = 3.f;

	m_pTransform->SetWorldScale(0.5f, 0.5f, 0.5f);

	// ������ ����
	CRenderer*	pRenderer = m_pGameObject->AddComponent<CRenderer>("Renderer");

	pRenderer->SetMesh("ColorPyramid");
	pRenderer->SetShader(STANDARD_COLOR_SHADER);
	pRenderer->SetInputLayout("ColorInputLayout");
	pRenderer->SetRenderState(WIRE_FRAME);

	SAFE_RELEASE(pRenderer);

	CColliderSphere*	pSphere = m_pGameObject->AddComponent<CColliderSphere>("Bullet");

	//pSphere->SetEnable(false);

	pSphere->SetSphereInfo(Vec3Zero, 0.5f);

	SAFE_RELEASE(pSphere);

	return true;
}

void CBullet::Input(float fTime)
{
}

void CBullet::Update(float fTime)
{
	m_pTransform->Up(m_fSpeed, fTime);
	m_pTransform->Rotate(0.f, AR3D_PI * 4.f, 0.f, fTime, true);

	m_fDist -= m_fSpeed * fTime;

	if (m_fDist <= 0.f)
	{
		CCamera*	pMainCamera = m_pScene->GetMainCamera();
		CGameObject*	pAttachObj = pMainCamera->GetAttachObject();
		SAFE_RELEASE(pMainCamera);

		if (pAttachObj && pAttachObj->CheckComponentFromTypeID<CBullet>())
		{
			m_pScene->ChangeCamera("MainCamera");
		}
		SAFE_RELEASE(pAttachObj);

		m_pGameObject->Death();
	}
}

CBullet * CBullet::Clone()
{
	return new CBullet(*this);
}

void CBullet::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
	if (pSrc->GetTag() == "PlayerBullet" && pDest->GetTag() == "Minion")
		m_pGameObject->Death();


	else if (pSrc->GetTag() == "MonsterBullet" && pDest->GetTag() == "PlayerBody")
		m_pGameObject->Death();
	//MessageBox(NULL, L"�Ѿ��浹", L"�Ѿ��浹", MB_OK);
}
