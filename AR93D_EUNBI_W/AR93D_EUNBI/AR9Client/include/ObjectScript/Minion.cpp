#include "Minion.h"
#include "Component/Renderer.h"
#include "Component/Transform.h"
#include "Component/ColliderSphere.h"
#include "Component/Material.h"

CMinion::CMinion() :
	m_pTarget(NULL)
{
	SetTypeID<CMinion>();
}

CMinion::CMinion(const CMinion & minion) :
	CScript(minion)
{
	m_pTarget = NULL;
}

CMinion::~CMinion()
{
	SAFE_RELEASE(m_pTarget);
}

void CMinion::SetTarget(CGameObject * pTarget)
{
	SAFE_RELEASE(m_pTarget);
	pTarget->AddRef();
	m_pTarget = pTarget;
}

bool CMinion::Init()
{

	m_pTransform->SetWorldScale(0.8f, 0.8f, 0.8f);

	// 렌더러 생성
	CRenderer*	pRenderer = m_pGameObject->AddComponent<CRenderer>("Renderer");

	pRenderer->SetMesh("TexNormalPyramid");
	pRenderer->SetShader(STANDARD_TEXNORMAL_SHADER);
	pRenderer->SetInputLayout("TexNormalInputLayout");

	//재질정보 설정
	CMaterial* pMaterial = pRenderer->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "Minion", L"minion.jpg");

	SAFE_RELEASE(pMaterial);
	SAFE_RELEASE(pRenderer);

	CColliderSphere*	pSphere = m_pGameObject->AddComponent<CColliderSphere>("Minion");

	pSphere->SetSphereInfo(Vec3Zero, 0.5f);

	SAFE_RELEASE(pSphere);

	return true;
}

void CMinion::Input(float fTime)
{
}

void CMinion::Update(float fTime)
{
	m_pTransform->LookAt(m_pTarget, AXIS_Y);
}

CMinion * CMinion::Clone()
{
	return new CMinion(*this);
}

void CMinion::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
	if (pDest->GetTag() == "PlayerBullet")
		m_pGameObject->Death();
}
