#pragma once
#include "Component\Script.h"
#include "GameObject/GameObject.h"

AR3D_USING

class CPlant :
	public CScript
{
public:
	CPlant();
	~CPlant();

private:
	CGameObject*	m_pChild;

};

