#pragma once

#include "Component/Script.h"
#include "GameObject/GameObject.h"

AR3D_USING

class CMinion	:
	public CScript
{
public:
	CMinion();
	CMinion(const CMinion& minion);
	~CMinion();

private:
	CGameObject*	m_pTarget;

public:
	void SetTarget(CGameObject* pTarget);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual CMinion* Clone();
	virtual void OnCollisionEnter(CCollider* pSrc, CCollider* pDest, float fTime);
};

