#include "Player.h"
#include "Component/Transform.h"
#include "Core/Input.h"
#include "Scene/Layer.h"
#include "Component/Renderer.h"
#include "Component/Camera.h"
#include "Scene/Scene.h"
#include "Bullet.h"
#include "Component/ColliderSphere.h"
#include "GameObject/GameObject.h"
#include "Component/Material.h"
#include "Component/Effect.h"
#include "Component/Animation2D.h"
#include "Resources/Mesh.h"
#include "Core/GameTotalManager.h"
#include "FarmObject.h"
#include "Component/UIBar.h"
#include "Component/Renderer2D.h"
#include "Scene/SceneManager.h"
#include "../Client.h"

CPlayer::CPlayer() :
	m_pChild(NULL),
	m_bAttack(false),
	m_bPlant(false),
	m_pBar(NULL)
{
	SetTypeID<CPlayer>();
}

CPlayer::~CPlayer()
{
	SAFE_RELEASE(m_pAniController);
	SAFE_RELEASE(m_pChild);
	SAFE_RELEASE(m_pBar);
}

void CPlayer::MoveEffect(float fTime)
{
	CGameObject*	pEffectObj = CGameObject::Create("Effect", false);

	CRenderer* pRenderer = pEffectObj->AddComponent<CRenderer>("EffectRenderer");

	pRenderer->SetMesh("PosMesh");
	pRenderer->SetShader(EFFECT_SHADER);
	pRenderer->SetInputLayout("PosInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	// 재질 정보 변경
	CMaterial* pMaterial = pRenderer->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "HPPotion", L"HPPotion.png");
	/*pMaterial->SetMaterialInfo(DxVector3(1.f, 0.f, 0.f), DxVector3(0.2f, 0.f, 0.f), DxVector3(1.f, 0.f, 0.f),
	DxVector3(1.f, 0.f, 0.f), 3.2f);*/

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	CEffect*	pEffect = pEffectObj->AddComponent<CEffect>("Effect");

	SAFE_RELEASE(pEffect);

	CAnimation2D*	pAnimation2D = pEffectObj->AddComponent<CAnimation2D>("EffectAnimation");

	vector<wstring>	vecExplosion;

	for (int i = 1; i <= 89; ++i)
	{
		TCHAR	strExplosion[MAX_PATH] = {};
		wsprintf(strExplosion, L"Effect/Explosion/160x120/Explosion%d.png", i);

		vecExplosion.push_back(strExplosion);
	}
	//GET_SINGLE(CResourcesManager)->LoadTexture("Explosion", vecExplosion);
	/*pAnimation2D->AddAnimationClip("HPPotion", A2D_ATLAS,
	AO_ONCE_DESTROY, 5, 4, 1.f, 0, 0.f, "HPPotion");*/
	pAnimation2D->AddAnimationClip("Explosion", A2D_FRAME,
		AO_ONCE_DESTROY, vecExplosion.size(), 1, 1.f, 0, 0.f, "Explosion", 11, &vecExplosion);

	pAnimation2D->Start();

	SAFE_RELEASE(pAnimation2D);

	CTransform*	pTransform = pEffectObj->GetTransform();

	pTransform->SetWorldPos(m_pTransform->GetWorldPos());

	SAFE_RELEASE(pTransform);

	m_pLayer->AddObject(pEffectObj);

	SAFE_RELEASE(pEffectObj);
}

bool CPlayer::Init()
{
	m_bPlant = false;

	// 충돌체 추가
	CColliderSphere*	pSphere = m_pGameObject->AddComponent<CColliderSphere>("PlayerBody");

	CRenderer* pRenderer = (CRenderer*)m_pGameObject->FindComponentFromTag("Renderer");
	
	pSphere->SetColliderGroup(CG_PLAYER);
	pSphere->SetSphereInfo(m_pTransform->GetWorldPos(),
		pRenderer->GetMesh()->GetRadius() * 0.05);

	SAFE_RELEASE(pSphere);
	SAFE_RELEASE(pRenderer);

	m_pAniController = (CAnimation3D*)FindComponentFromType(CT_ANIMATION3D);
/*
	m_pAniController->AddClipCallback<CPlayer>("Run", 2.0f, this,
		&CPlayer::MoveEffect);*/

	m_pAniController->SetCurClipName("Idle");
	m_pGameObject->SetPicked(false);
	//MakeUIBar();
	return true;
}


void CPlayer::Input(float fTime)
{

	//CCamera*	pMainCamera = m_pScene->GetMainCamera();
	//CTransform*	pTransform = pMainCamera->GetTransform();


	//DxVector3 forward = pMainCamera->GetAxis(AXIS_Z);
	//DxVector3 back = DxVector3(forward.x * -1, forward.y * -1, forward.z * -1);

	//DxVector3 right = pMainCamera->GetAxis(AXIS_X);
	//DxVector3 left = DxVector3(right.x * -1, right.y * -1, right.z * -1);

	//m_pTransform->SetWorldRot(pTransform->GetWorldRot());/*

	//if (KEYPRESS("WALK") || KEYPUSH("WALK"))
	//{
	//	m_pTransform->Move(left, 1.f);
	//	m_pAniController->ChangeClip("Walk");
	//}*/


	//SAFE_RELEASE(pTransform);
	//SAFE_RELEASE(pMainCamera);
}

void CPlayer::Update(float fTime)
{
	if (m_pTransform->GetMove())
		m_bAttack = false;


	if (m_bAttack)
	{
	}
	if (m_pGameObject->GetPicked())
	{
		//빨강색으로, 그리고 걷게만들기 

		CRenderer* pRenderer = (CRenderer*)m_pGameObject->FindComponentFromTag("Renderer");

		pRenderer->SetOutlineColor(DxVector3(1.f, 0.f, 0.f));

		SAFE_RELEASE(pRenderer);
		m_pAniController->ChangeClip("Walk");
		//if (m_pPickedActor)
		//{
		m_pGameObject->GetTransform()->SetWorldPos(
		GET_SINGLE(CInput)->GetTerrainPickingPos());
		//}
	}
	else
	{
		CRenderer* pRenderer = (CRenderer*)m_pGameObject->FindComponentFromTag("Renderer");

		pRenderer->SetOutlineColor(DxVector3(0.f, 0.f, 0.f));

		SAFE_RELEASE(pRenderer);
		if (m_bPlant)
			m_pAniController->ChangeClip("Plant");
		else
			m_pAniController->ChangeClip("Idle");
	}

}

void CPlayer::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
	if (pDest->GetTag() == "MouseRay")
	{
		if (GET_SINGLE(CGameTotalManager)->GetPickActor() != NULL)
		{
			if (GET_SINGLE(CGameTotalManager)->GetPickActor()->GetTag() ==
				m_pGameObject->GetTag()) // Pick된 Actor가 이미 자신일때 
			{
				GET_SINGLE(CGameTotalManager)->ErasePickActor();
			}
			else
			{
				GET_SINGLE(CGameTotalManager)->ErasePickActor();
				GET_SINGLE(CGameTotalManager)->SetPickActor(m_pGameObject);
			}
		}
		else// Pick된 Actor가 아무도 없을때 
		{
			GET_SINGLE(CGameTotalManager)->SetPickActor(m_pGameObject);
			m_bPlant = false;
		}

	}

	if (pDest->GetTag() == "FarmObject")
	{
		if (m_bPlant == false)
		{
			CFarmObject* FarmObj = (CFarmObject*)pDest->GetGameObject()->FindComponentFromTag("LandDitrt");
			FarmObj->ChangeLevel();
			SAFE_RELEASE(FarmObj); 
			m_bPlant = true;
		}

	}

	
}

void CPlayer::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
	
}

void CPlayer::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CPlayer::MakeUIBar()
{
	CGameObject*	pTestBar = CGameObject::Create("UIBar");

	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	pUILayer = pScene->FindLayer("UILayer");

	CRenderer2D*	pRenderer2D = pTestBar->AddComponent<CRenderer2D>("BarRenderer");

	pRenderer2D->SetMesh("UIMesh");
	pRenderer2D->SetShader(UI_SHADER);
	pRenderer2D->SetInputLayout("TexInputLayout");
	pRenderer2D->SetRenderState(ALPHABLEND);

	// 재질 정보 변경
	CMaterial* pMaterial = pRenderer2D->GetMaterial();

	pMaterial->SetDiffuseTexture("Linear", "TestBar", L"HpBar.bmp");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer2D);


	CUIBar*	pUIBar = pTestBar->AddComponent<CUIBar>("UIBar");

	pUIBar->SetMinMax(0.f, 1000.f);
	pUIBar->SetBarDir(BD_RIGHT);

	m_pBar = pUIBar;
	//SAFE_RELEASE(pUIBar);

	CTransform* pTransform = pTestBar->GetTransform();

	pTransform->SetWorldScale(300.f, 50.f, 1.f);

	DxVector3	vPos = m_pTransform->GetWorldPos();

	//tView = tView.Inverse();
	//tProj = tProj.Inverse();
	CCamera*	pCamera = pScene->GetMainCamera();

	
	MATRIX	tView = pCamera->GetViewMatrix();
	MATRIX	tProj = pCamera->GetProjMatrix();

	vPos = vPos.TransformCoord(tView);
	vPos = vPos.TransformCoord(tProj);
	//vPos.y = g_tRS[RT_FHD].iWidth - vPos.y;
	vPos.z = 0.f;

	//CTransform* pTransform = m_pBar->GetTransform();
	pTransform->SetWorldPos(vPos);

	//pTransform->SetWorldPos(400.f, 30.f, 0.f);

	SAFE_RELEASE(pTransform);

	pUILayer->AddObject(pTestBar);

	SAFE_RELEASE(pTestBar);

	//CUIBar*	pUIBar = pTestBar->AddComponent<CUIBar>("UIBar");

	//pUIBar->SetMinMax(0.f, 1000.f);
	//pUIBar->SetBarDir(BD_RIGHT);

	//m_pBar = pUIBar;
	////SAFE_RELEASE(pUIBar);

	//CTransform* pTransform = pTestBar->GetTransform();

	//pTransform->SetWorldScale(300.f, 50.f, 1.f);

	//CCamera*	pCamera = pScene->GetMainCamera();

	//MATRIX	tView = pCamera->GetViewMatrix();
	//MATRIX	tProj = pCamera->GetProjMatrix();

	//// 월드공간으로 변환한다. 
	//DxVector3	vPos = m_pTransform->GetWorldPos();

	//vPos = vPos.TransformCoord(tProj);
	//vPos.z = 0.f;

	//pTransform->SetWorldPos(vPos);
	//pTransform->SetWorldPos(300, 300, 0.f);

	//SAFE_RELEASE(pCamera);
	//SAFE_RELEASE(pTransform);

	//pUILayer->AddObject(pTestBar);

	//SAFE_RELEASE(pTestBar);
}
