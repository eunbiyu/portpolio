#include "BuildingScript.h"
#include "GameObject/GameObject.h"
#include "Component/Renderer.h"
#include "Component/Transform.h"
#include "Scene/Scene.h"
#include "Scene/Layer.h"
#include "Component/ColliderSphere.h"
#include "Resources/ResourcesManager.h"
#include "Resources/Texture.h"
#include "Resources/Mesh.h"
#include "Component/Material.h"
#include "Scene/SceneManager.h"
#include "Core/GameTotalManager.h"
#include "../Client.h"
#include "Scene/SceneManager.h"
#include "Component/ParticleSystem.h"
#include "Component/Light.h"
#include "Core/GameTotalManager.h"
#include "Component/ColliderRect.h"
#include "Component/UIBack.h"
#include "Component/UIButton.h"
#include "Component/Renderer2D.h"
#include "Component/Text.h"
#include "Component/Camera.h"

CBuildingScript::CBuildingScript()
{
	SetTypeID<CBuildingScript>();
}


CBuildingScript::~CBuildingScript()
{
}

bool CBuildingScript::Init()
{
	//충돌체 추가

	CTransform*	pTransform = m_pGameObject->GetTransform();
	CRenderer*	pRenderer = (CRenderer*)m_pGameObject->FindComponentFromTag("Renderer");

	CColliderSphere*	pSphere = m_pGameObject->AddComponent<CColliderSphere>("FarmObject");


	if (strcmp(m_pGameObject->GetTag().c_str(), "Medieval_Windmill") == 0)
	{
		pSphere->SetSphereInfo(pTransform->GetWorldPos(),
			pRenderer->GetMesh()->GetRadius() * 0.05);
	}
	else if (strcmp(m_pGameObject->GetTag().c_str(), "WaterTowerA1") == 0)
	{
		pSphere->SetSphereInfo(pTransform->GetWorldPos(),
			pRenderer->GetMesh()->GetRadius() * 0.03);
	}
	else
	{
		pSphere->SetSphereInfo(pTransform->GetWorldPos(),
			pRenderer->GetMesh()->GetRadius() * 0.01);
	}


	SAFE_RELEASE(pSphere);

	SAFE_RELEASE(pTransform);
	SAFE_RELEASE(pRenderer);

	return true;
}

void CBuildingScript::Input(float fTime)
{
}

void CBuildingScript::Update(float fTime)
{
}

void CBuildingScript::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
	if (pDest->GetTag() == "MouseRay")
	{
		if (GET_SINGLE(CGameTotalManager)->GetPickActor() != NULL)
		{
			if (GET_SINGLE(CGameTotalManager)->GetPickActor()->GetTag() ==
				m_pGameObject->GetTag()) // Pick된 Actor가 이미 자신일때 
			{
				GET_SINGLE(CGameTotalManager)->ErasePickActor();
			}
			else
			{
				GET_SINGLE(CGameTotalManager)->ErasePickActor();
				GET_SINGLE(CGameTotalManager)->SetPickActor(m_pGameObject);
				MakeInfoUIMaker();
				CCamera*	pMainCamera = GET_SINGLE(CSceneManager)->GetCurrentScene()
					->GetMainCamera();

				pMainCamera->SetTargetObject(m_pGameObject);

				SAFE_RELEASE(pMainCamera);
			}
		}
		else// Pick된 Actor가 아무도 없을때 
		{
			GET_SINGLE(CGameTotalManager)->SetPickActor(m_pGameObject);
			MakeInfoUIMaker();

			CCamera*	pMainCamera = GET_SINGLE(CSceneManager)->GetCurrentScene()
				->GetMainCamera();

			pMainCamera->SetTargetObject(m_pGameObject);

			SAFE_RELEASE(pMainCamera);

		}
	}
}

void CBuildingScript::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CBuildingScript::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CBuildingScript::MakeInfoUIMaker()
{
	
	//------------------ < buy Button 생성 > ----------------------
	CGameObject* pUI = CGameObject::Create("BuyButton");

	CTransform*	pTransform = pUI->GetTransform();

	pTransform->SetWorldScale(200.f, 100.f, 1.f);
	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth / 2 -50, g_tRS[RT_FHD].iHeight  - 200, 0.f);
	pTransform->SetPivot(0.f, 0.f, 0.f);

	CRenderer2D*	pRenderer = pUI->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	CMaterial* pMaterial = pRenderer->GetMaterial();
	pMaterial->SetDiffuseTexture("Linear", "Buy", L"UI/buy.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	CUIButton* pButton = pUI->AddComponent<CUIButton>("BuyButton");

	pButton->SetCallback(this, &CBuildingScript::Buy);

	//pButton->SetCallback(this, &CMainScene::CloseLayer);

	SAFE_RELEASE(pButton);



	CLayer*	pLayer = m_pScene->FindLayer("BuildingUILayer");
	pLayer->AddObject(pUI);

	SAFE_RELEASE(pUI);
	SAFE_RELEASE(pTransform);

	//------------------ < cancel Button 생성 > ----------------------
	pUI = CGameObject::Create("closeButton");

	pTransform = pUI->GetTransform();

	pTransform->SetWorldScale(200.f, 100.f, 1.f);
	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth / 2 + 200, g_tRS[RT_FHD].iHeight  - 200, 0.f);
	pTransform->SetPivot(0.f, 0.f, 0.f);

	pRenderer = pUI->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	pMaterial = pRenderer->GetMaterial();
	pMaterial->SetDiffuseTexture("Linear", "close", L"UI/close.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	pButton = pUI->AddComponent<CUIButton>("closeBtn");

	pButton->SetCallback(this, &CBuildingScript::CloseLayer);

	SAFE_RELEASE(pButton);

	pLayer->AddObject(pUI);

	SAFE_RELEASE(pUI);
	SAFE_RELEASE(pTransform);

	//------------------ < INFO UI 생성 > ----------------------
	CGameObject*	pInventory = CGameObject::Create("InfoUI");

	CRenderer2D*	pRenderer2D = pInventory->AddComponent<CRenderer2D>("Renderer");

	pRenderer2D->SetMesh("UIMesh");
	pRenderer2D->SetShader(UI_SHADER);
	pRenderer2D->SetInputLayout("TexInputLayout");
	pRenderer2D->SetRenderState(ALPHABLEND);


	// 재질 정보 변경
	pMaterial = pRenderer2D->GetMaterial();

	if (strcmp(m_pGameObject->GetTag().c_str(), "WaterTowerA1") == 0)
	{
		pMaterial->SetDiffuseTexture("Linear", "watertower", L"UI/watertower.png");
	}
	else if (strcmp(m_pGameObject->GetTag().c_str(), "Medieval_Windmill") == 0)
	{
		pMaterial->SetDiffuseTexture("Linear", "bakery", L"UI/bakery.png");
	}
	else
	{
		pMaterial->SetDiffuseTexture("Linear", "bakery", L"UI/bakery.png");
	}


	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer2D);

	CColliderRect*	pInvenColl = pInventory->AddComponent<CColliderRect>("InvenColl");

	pInvenColl->SetRectInfo(0.f, 0.f, g_tRS[RT_FHD].iWidth / 2, g_tRS[RT_FHD].iHeight / 3 * 2);

	SAFE_RELEASE(pInvenColl);

	pTransform = pInventory->GetTransform();

	pTransform->SetWorldScale(g_tRS[RT_FHD].iWidth / 2, g_tRS[RT_FHD].iHeight / 3, 0.f);

	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth / 3 - 100, g_tRS[RT_FHD].iHeight / 3 * 2, 0.f);
	pTransform->SetPivot(0.f, 0.f, 0.f);

	SAFE_RELEASE(pTransform);

	CUIBack*	pInvenBack = pInventory->AddComponent<CUIBack>("UIBack");

	SAFE_RELEASE(pInvenBack);

	pLayer->AddObject(pInventory);
	pLayer->SetCollisionOn(true);

	SAFE_RELEASE(pTransform);
	SAFE_RELEASE(pLayer);
	SAFE_RELEASE(pInventory);

	//MakeCloseButton();

}

void CBuildingScript::MakeCloseButton()
{

	//==========================< close button >======================//

	CGameObject* pUI = CGameObject::Create("CloseButton");

	CTransform*	pTransform = pUI->GetTransform();

	pTransform->SetWorldScale(100.f, 100.f, 1.f);
	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth / 3 * 2 + 100, g_tRS[RT_FHD].iHeight / 3 * 2 + 10, 0.f);
	pTransform->SetPivot(0.f, 0.f, 0.f);

	CRenderer2D*	pRenderer = pUI->AddComponent<CRenderer2D>("Renderer");
	pRenderer->SetMesh("UIMesh");
	pRenderer->SetShader(UI_SHADER);
	pRenderer->SetInputLayout("TexInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);

	CMaterial* pMaterial = pRenderer->GetMaterial();
	pMaterial->SetDiffuseTexture("Linear", "CloseButton", L"UI/CloseButton.png");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	CUIButton* pButton = pUI->AddComponent<CUIButton>("CloseButton");

	//pButton->SetCallback(this, &CStartScene::StartButton);

	pButton->SetCallback(this, &CBuildingScript::CloseLayer);

	SAFE_RELEASE(pButton);


	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	pLayer = m_pScene->FindLayer("BuildingUILayer");
	pLayer->AddObject(pUI);

	SAFE_RELEASE(pUI);
	SAFE_RELEASE(pTransform);
}

void CBuildingScript::MakeErrorButton()
{
	CloseLayer(m_pGameObject, 10);
	MakeCloseButton();
	//==========================< close button >======================//

	CGameObject* pUI = CGameObject::Create("ErrorMessage");

	CRenderer2D*	pRenderer2D = pUI->AddComponent<CRenderer2D>("Renderer");

	pRenderer2D->SetMesh("UIMesh");
	pRenderer2D->SetShader(UI_SHADER);
	pRenderer2D->SetInputLayout("TexInputLayout");
	pRenderer2D->SetRenderState(ALPHABLEND);


	// 재질 정보 변경
	CMaterial* pMaterial = pRenderer2D->GetMaterial();
	pMaterial->SetDiffuseTexture("Linear", "error", L"UI/error.png");
	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer2D);

	CColliderRect*	pInvenColl = pUI->AddComponent<CColliderRect>("InvenColl");

	pInvenColl->SetRectInfo(0.f, 0.f, g_tRS[RT_FHD].iWidth / 2, g_tRS[RT_FHD].iHeight / 3 * 2);

	SAFE_RELEASE(pInvenColl);

	CTransform* pTransform = pUI->GetTransform();

	pTransform->SetWorldScale(g_tRS[RT_FHD].iWidth / 2, g_tRS[RT_FHD].iHeight / 3, 0.f);

	pTransform->SetWorldPos(g_tRS[RT_FHD].iWidth / 3 - 100, g_tRS[RT_FHD].iHeight / 3 * 2, 0.f);
	pTransform->SetPivot(0.f, 0.f, 0.f);

	SAFE_RELEASE(pTransform);

	CUIBack*	pInvenBack = pUI->AddComponent<CUIBack>("UIBack");

	SAFE_RELEASE(pInvenBack);

	CLayer*	pLayer = m_pScene->FindLayer("BuildingUILayer");
	pLayer->AddObject(pUI);
	pLayer->SetCollisionOn(true);

	SAFE_RELEASE(pTransform);
	SAFE_RELEASE(pLayer);
	SAFE_RELEASE(pUI);


}

void CBuildingScript::CloseLayer(CGameObject * pObj, float fTime)
{
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	pLayer = m_pScene->FindLayer("BuildingUILayer");

	pLayer->EraseAllObject();
	pLayer->SetCollisionOn(false);

	SAFE_RELEASE(pLayer);
}

void CBuildingScript::Buy(CGameObject * pObj, float fTime)
{
	if (strcmp(m_pGameObject->GetTag().c_str(), "WaterTowerA1") == 0)
	{
		if (GET_SINGLE(CGameTotalManager)->SubtracCropAndNum(3, 3))
		{

			if (GET_SINGLE(CGameTotalManager)->SubtracCropAndNum(5, 3))
			{
				CloseLayer(pObj, fTime);
			}
			else //  여기서 경고창 생성
			{
				MakeErrorButton();
				return;
			}

		}
		else //  여기서 경고창 생성
		{
			MakeErrorButton();
			return;
		}

		GET_SINGLE(CGameTotalManager)->SetCropAndNum(10, 1);

	}
	else if (strcmp(m_pGameObject->GetTag().c_str(), "Medieval_Windmill") == 0)
	{
		if (GET_SINGLE(CGameTotalManager)->SubtracCropAndNum(0, 6))
		{

			if (GET_SINGLE(CGameTotalManager)->SubtracCropAndNum(1, 3))
			{
				CloseLayer(pObj, fTime);
			}
			else //  여기서 경고창 생성
			{
				MakeErrorButton();
				return;
			}

		}
		else //  여기서 경고창 생성
		{
			MakeErrorButton();
			return;
		}

		GET_SINGLE(CGameTotalManager)->SetCropAndNum(9, 1);
	}
	else
	{
	}
}

wstring CBuildingScript::intToWstring(int inum)
{
	char tmp[10];
	char* ch;

	ch = _itoa(inum, tmp, 10);    // 정수형 -> 문자열 변환

	CString str = CString(ch);

	std::wstring wstr = (LPCTSTR)str;
	return wstr;
}
