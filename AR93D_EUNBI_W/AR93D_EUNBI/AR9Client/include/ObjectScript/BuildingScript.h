#pragma once
#include "Component/Script.h"
#include "GameObject/GameObject.h"

typedef std::basic_string<TCHAR> tstring;

AR3D_USING

class CBuildingScript :
	public CScript
{
public:
	CBuildingScript();
	~CBuildingScript();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void OnCollisionEnter(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollision(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollisionLeave(CCollider* pSrc, CCollider* pDest, float fTime);

	void MakeInfoUIMaker();
	void MakeCloseButton();
	void MakeErrorButton();
	void CloseLayer(CGameObject * pObj, float fTime);
	void Buy(CGameObject * pObj, float fTime);
	wstring	intToWstring(int inum);

};

