#pragma once

#include "Component/Script.h"
#include "Component/Collider.h"

AR3D_USING

class CBullet	:
	public CScript
{
public:
	CBullet();
	CBullet(const CBullet& bullet);
	~CBullet();

private:
	float	m_fSpeed;
	float	m_fDist;

public:
	void SetDist(float fDist)
	{
		m_fDist = fDist;
	}

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual CBullet* Clone();
	virtual void OnCollisionEnter(CCollider* pSrc, CCollider* pDest, float fTime);
};
