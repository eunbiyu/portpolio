#pragma once
#include "Component/Script.h"
#include "GameObject/GameObject.h"

typedef std::basic_string<TCHAR> tstring;

AR3D_USING

class CLandDirt;

class CFarmObject :
	public CScript
{
private:
	CROPS_FLAG  m_cropFlag;
	ANGLE_FLAG	m_bDirtDir; //ROW, COL ���� ����
	int			ObjectLevel;
	int			ObjectChangeLevel;
	string		m_strFarmObjTag;

	list<CGameObject*>	m_ChildList;

public:
	CFarmObject();
	~CFarmObject();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void OnCollisionEnter(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollision(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollisionLeave(CCollider* pSrc, CCollider* pDest, float fTime);

	void CreateFarmObject(CROPS_FLAG cropType, ANGLE_FLAG dirtDir,
		string strTag);
	class CLandDirt* LoadLandDirt(const string& strKey);
	class CLandDirt* FindLandDirt(const string& strKey);
	void SetCropFlag(string Tag);
	CROPS_FLAG GetCropFlag();
	TCHAR* CFarmObject::StringToTCHAR(const string& s);
	void ChangeLevel();
	void MakeParticle(DxVector3 Position, float fLifeTime);
};

