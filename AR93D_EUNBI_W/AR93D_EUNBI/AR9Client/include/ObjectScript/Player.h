#pragma once

#include "Component/Script.h"
#include "GameObject/GameObject.h"
#include "Component/Animation3D.h"
#include "Component/UIBar.h"

AR3D_USING

class CPlayer	:
	public CScript
{
public:
	CPlayer();
	~CPlayer();

private:
	CGameObject*	m_pChild;
	CAnimation3D*	m_pAniController;
	bool			m_bAttack;
	float			m_fSpeed;
	DxVector3		m_vTargetPosition; //타겟포지션이 있을때 
	bool			m_bPlant;
	CUIBar*			m_pBar;

public:
	void MoveEffect(float fTime);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void OnCollisionEnter(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollision(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollisionLeave(CCollider* pSrc, CCollider* pDest, float fTime);
	void MakeUIBar();
};

