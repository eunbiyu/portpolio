#include "FarmObject.h"
#include "Plant.h"
#include "GameObject/GameObject.h"
#include "Component/Renderer.h"
#include "Component/Transform.h"
#include "Scene/Scene.h"
#include "Scene/Layer.h"
#include "Component/ColliderSphere.h"
#include "Resources/ResourcesManager.h"
#include "Resources/Texture.h"
#include "Resources/Mesh.h"
#include "Component/Material.h"
#include "Scene/SceneManager.h"
#include "Core/GameTotalManager.h"
#include "../Client.h"
#include "Scene/SceneManager.h"
#include "Component/ParticleSystem.h"
#include "Component/Light.h"
#include "../Client.h"
#include "Core/GameTotalManager.h"
#include "Core/Input.h"

CFarmObject::CFarmObject()
{
	ObjectLevel = 1;
	SetTypeID<CFarmObject>();
}


CFarmObject::~CFarmObject()
{
}

bool CFarmObject::Init()
{
	ObjectLevel = 1;
	CTransform*	pTransform = m_pGameObject->GetTransform();
	pTransform->SetWorldPos(390.f, 30.f, 210.f);
	pTransform->SetWorldScale(0.01f, 0.01f, 0.01f);
	pTransform->SetLocalRotX(80.12f);

	CRenderer*	pRenderer = m_pGameObject->AddComponent<CRenderer>("PlayerRenderer");
	pRenderer->SetMesh("FarmMesh", L"LandA1_DirtA2.fbx", FLT_BOTH, OBJECT_MESH_PATH);
	pRenderer->SetShader(STANDARD_BUMP_SHADER);
	pRenderer->SetInputLayout("BumpInputLayout");
	pRenderer->SetRenderFlag(RF_CARTOON_ONLY);

	//충돌체 추가
	CColliderSphere*	pSphere = m_pGameObject->AddComponent<CColliderSphere>("FarmObject");

	pSphere->SetSphereInfo(pTransform->GetWorldPos(),
		pRenderer->GetMesh()->GetRadius() * 0.001);


	SAFE_RELEASE(pSphere);

	SAFE_RELEASE(pTransform);
	//// 재질 정보 변경
	CMaterial* pMaterial = pRenderer->GetMaterial();
	pMaterial->SetDiffuseTexture("Linear", "Atlas", L"FarmA1_Atlas_col.jpg");

	SAFE_RELEASE(pMaterial);
	SAFE_RELEASE(pRenderer);

	//타이머 만들어주기 

	return true;
}

void CFarmObject::Input(float fTime)
{
}

void CFarmObject::Update(float fTime)
{
	if (m_pGameObject->GetPicked())
	{
	
		m_pGameObject->GetTransform()->SetWorldPos(
			GET_SINGLE(CInput)->GetTerrainPickingPos());
	}
}

void CFarmObject::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
	if (pDest->GetTag() == "MouseRay")
	{
		if (GET_SINGLE(CGameTotalManager)->GetPickActor() != NULL)
		{
			if (GET_SINGLE(CGameTotalManager)->GetPickActor()->GetTag() ==
				m_pGameObject->GetTag()) // Pick된 Actor가 이미 자신일때 
			{
				GET_SINGLE(CGameTotalManager)->ErasePickActor();
			}
			else
			{
				GET_SINGLE(CGameTotalManager)->ErasePickActor();
				GET_SINGLE(CGameTotalManager)->SetPickActor(m_pGameObject);
			}
		}
		else// Pick된 Actor가 아무도 없을때 
			GET_SINGLE(CGameTotalManager)->SetPickActor(m_pGameObject);

	}
}

void CFarmObject::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CFarmObject::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CFarmObject::CreateFarmObject(CROPS_FLAG cropType,
	ANGLE_FLAG dirtDir, string strTag)
{

}

CLandDirt * CFarmObject::LoadLandDirt(const string & strKey)
{
	return nullptr;
}

CLandDirt * CFarmObject::FindLandDirt(const string & strKey)
{
	return nullptr;
}

void CFarmObject::SetCropFlag(string Tag)
{
	for (int i = 0; i < 3; ++i)
	{

		CGameObject*	pChild = CGameObject::Create("PlayerChild");
		m_strFarmObjTag = Tag;

		CTransform*	pTransform = pChild->GetTransform();

		pTransform->SetLocalRotX(80.12f);
		pTransform->SetWorldPos(0.f, 0.f, -300 + i*300.f);

		SAFE_RELEASE(pTransform);

		CRenderer* pRenderer = pChild->AddComponent<CRenderer>("PlayerRenderer");
		string temp = Tag;
		string leveltemp = std::to_string(ObjectLevel);
		temp += leveltemp;
		temp += ".FBX";

		pRenderer->SetMesh(Tag+"Mesh" + leveltemp, StringToTCHAR(temp), FLT_BOTH, OBJECT_MESH_PATH);
		pRenderer->SetShader(STANDARD_BUMP_SHADER);
		pRenderer->SetInputLayout("BumpInputLayout");
		// 0 : 카툰렌더링 1 : 카툰&아웃라인, 2: 아웃라인만, 3: 일반 렌더링 , 
		pRenderer->SetRenderFlag(RF_CARTOON_ONLY);
		//pRenderer->SetRenderState(ALPHABLEND);
		CMaterial* pMaterial = pRenderer->GetMaterial();
		pMaterial->SetDiffuseTexture("Linear", "Atlas", L"FarmA1_Atlas_col.jpg");

		SAFE_RELEASE(pMaterial);
		SAFE_RELEASE(pRenderer);

		m_pGameObject->AddChild(pChild);

		m_ChildList.push_back(pChild);

	}


}

CROPS_FLAG CFarmObject::GetCropFlag()
{
	return m_cropFlag;
}


TCHAR* CFarmObject::StringToTCHAR(const string& s)
{
	tstring tstr;
	const char* all = s.c_str();
	int len = 1 + strlen(all);
	wchar_t* t = new wchar_t[len];
	if (NULL == t) throw std::bad_alloc();
	mbstowcs(t, all, len);
	return (TCHAR*)t;
}

void CFarmObject::ChangeLevel()
{
	if (ObjectLevel < 3)
	{
		ObjectChangeLevel = ObjectLevel + 1;

		MakeParticle(m_pTransform->GetWorldPos(), 0.f);
	}
	else
	{
		MakeParticle(m_pTransform->GetWorldPos(), 2.f);
		m_pGameObject->Death();
		for (int i = 0; i < 9; ++i)
		{
			if (strcmp(GET_SINGLE(CGameTotalManager)->GetCropAndNum(i).cropName.c_str(),
				m_strFarmObjTag.c_str()) == 0)
				GET_SINGLE(CGameTotalManager)->SetCropAndNum(i, 3);
		}
	}

	if (ObjectChangeLevel != ObjectLevel)
	{
		ObjectLevel = ObjectChangeLevel;
		SetCropFlag(m_strFarmObjTag);
	}
}

void CFarmObject::MakeParticle(DxVector3 Position, float fLifeTime)
{

	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	pLayer = m_pScene->FindLayer("MakeCropMakerLayer");
	CGameObject*	pParticleObj = CGameObject::Create("Particle", true);

	CTransform*	pParticleTr = pParticleObj->GetTransform();

	pParticleTr->SetWorldPos(0.f, 0.f, 3.f);

	SAFE_RELEASE(pParticleTr);

	CParticleSystem*	pParticle = pParticleObj->AddComponent<CParticleSystem>("Particle");

	pParticle->SetParticleInfo();
	if(fLifeTime < 1.f)
		pParticle->SetParticleTexture("ParticleSample", L"Effect/butterfly4.png");
	else
		pParticle->SetParticleTexture("ParticleSample2", L"Effect/butterfly.png");
	pParticle->SetParticleLight(true);

	SAFE_RELEASE(pParticle);

	SAFE_RELEASE(pParticleObj);

	// 파티클 생성
	for (int i = 0; i < 3; ++i)
	{
		pParticleObj = CGameObject::CreateClone("Particle");

		CTransform*	pParticleTr = pParticleObj->GetTransform();
		pParticleTr->SetWorldPos(Position);
		SAFE_RELEASE(pParticleTr);

		pLayer->AddObject(pParticleObj);

		SAFE_RELEASE(pParticleObj);
	}

	SAFE_RELEASE(pLayer);

}
