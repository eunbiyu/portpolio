
#include "Share.fx"

SamplerState g_LightBlendSmp : register(s11);

texture2D g_GBufferAlbedo : register(t11);
texture2D g_LightDif : register(t12);
texture2D g_LightSpc : register(t13);

static const float2 vPos[4] =
{
    float2(-1.f, 1.f),
	float2(1.f, 1.f),
	float2(-1.f, -1.f),
	float2(1.f, -1.f)
};

static const float2 vUV[4] =
{
    float2(0.f, 0.f),
	float2(1.f, 0.f),
	float2(0.f, 1.f),
	float2(1.f, 1.f)
};

VS_TEX_OUTPUT LightBlendVS(uint iVertexID : SV_VertexID)
{
    VS_TEX_OUTPUT output = (VS_TEX_OUTPUT) 0;

    output.vPos = float4(vPos[iVertexID], 0.f, 1.f);
    output.vUV = vUV[iVertexID];

    return output;
}

PS_OUTPUT LightBlendPS(VS_TEX_OUTPUT input)
{
    PS_OUTPUT output = (PS_OUTPUT) 0;

	// Albedo와 Diffuse, Specular  합성을 해야 한다.
    float4 vAlbedo = g_GBufferAlbedo.Sample(g_LightBlendSmp, input.vUV);

    if (vAlbedo.a == 0.f)
        clip(-1);

    float4 vDif = g_LightDif.Sample(g_LightBlendSmp, input.vUV);
    float4 vSpc = g_LightSpc.Sample(g_LightBlendSmp, input.vUV);

    vDif += 0.3f;

    //여기서 Diffuse빼버림... 넣으면 이상
	output.vTarget0 = (vAlbedo * 1.1) * vDif + vSpc; +0.1f;
    output.vTarget0.a = 1.f;

    return output;
}

PS_OUTPUT LightBlendTargetOutputPS(VS_TEX_OUTPUT input)
{
    PS_OUTPUT output = (PS_OUTPUT) 0;

    float4 vAlbedo = g_GBufferAlbedo.Sample(g_LightBlendSmp, input.vUV);

    if (vAlbedo.a == 0.f)
        clip(-1);

    output.vTarget0 = vAlbedo;

    return output;
}
