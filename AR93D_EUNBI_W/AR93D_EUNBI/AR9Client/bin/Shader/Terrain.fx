
#include "Share.fx"

struct VS_TERRAIN_OUTPUT
{
	float4	vPos	: SV_POSITION;
	float3	vNormal	: NORMAL;
	float2	vUV	: TEXCOORD;
    float4  vProjPos : POSITION;
    float3  vWorldPos   : POSITION1;
	float3	vTangent	: TANGENT;
	float3	vBinormal	: BINORMAL;
	int		iSplatCount : TEXCOORD1;
};

cbuffer TerrainInfo	: register(b10)
{
	float	g_fDetailLevel;
	int		g_iSplatCount;
	float2	g_vTerrainEmpty1;
}
cbuffer PickTerrainInfo	: register(b11)
{
    float3 g_vPos;
    float g_fOuterRadius;
	float3	g_Empty;
	float	g_fInnerRadius;
}

Texture2DArray	g_SplatTexArray		: register(t11);
SamplerState	g_SplatSmp			: register(s11);

Texture2DArray	g_SplatNTexArray	: register(t12);
SamplerState	g_SplatNSmp			: register(s12);

Texture2DArray	g_SplatSTexArray	: register(t13);
SamplerState	g_SplatSSmp			: register(s13);

Texture2DArray	g_AlphaTexArray		: register(t14);
SamplerState	g_AlphaSmp			: register(s14);


VS_TERRAIN_OUTPUT TerrainVS(VS_BUMP_INPUT input)
{
    VS_TERRAIN_OUTPUT output = (VS_TERRAIN_OUTPUT) 0;

    output.vProjPos = mul(float4(input.vPos, 1.f), g_matWVP);
    output.vUV = input.vUV;
    output.vNormal = normalize(mul(float4(input.vNormal, 0.f), g_matWV).xyz);
	//output.vViewPos = mul(float4(input.vPos, 1.f), g_matWV);
    output.vPos = output.vProjPos;

    if (g_iBump == 1)
    {
        output.vTangent = normalize(mul(float4(input.vTangent, 0.f), g_matWV).xyz);
        output.vBinormal = normalize(mul(float4(input.vBinormal, 0.f), g_matWV).xyz);
    }

    output.iSplatCount = g_iSplatCount;

    return output;
}


PS_OUTPUT_GBUFFER TerrainPS(VS_TERRAIN_OUTPUT input)
{
    PS_OUTPUT_GBUFFER output = (PS_OUTPUT_GBUFFER) 0;


    //스플래팅 툴을 위한 값 
    float fOuterDist = ((g_vPos.x - input.vWorldPos.x) * (g_vPos.x - input.vWorldPos.x)) +
	((g_vPos.z - input.vWorldPos.z) * (g_vPos.z - input.vWorldPos.z));

    float fInnerDist = ((g_vPos.x - input.vWorldPos.x) * (g_vPos.x - input.vWorldPos.x)) +
	((g_vPos.z - input.vWorldPos.z) * (g_vPos.z - input.vWorldPos.z));

	// 법선맵이 있을 경우 법선을 새로 구한다.
    float3 vNormal = input.vNormal;
    float2 vUV = input.vUV * g_fDetailLevel;
    float2 vEntireUV = input.vUV;
    float3 vUVW = float3(0.f, 0.f, 0.f);

    if (g_iBump == 1)
    {
        float4 vNormalMap = g_NormalTex.Sample(g_NormalSmp, vUV);

        for (int i = 0; i < 8; i++)
        {
            vUVW = float3(input.vUV, i);
            float fAlpha = g_AlphaTexArray.Sample(g_AlphaSmp, vUVW).r;

            if (fAlpha == 0.f)
                continue;

            vUVW = float3(vUV, i);
            
                vNormalMap = vNormalMap * (1.f - fAlpha) +
				g_SplatNTexArray.Sample(g_SplatNSmp, vUVW) * fAlpha;
        }

        float3 vNormal = vNormalMap * 2.f - 1.f;
        vNormal = normalize(vNormal);

		// 기저벡터를 이용하여 탄젠트공간의 법선을 View 공간으로 만들어준다.
		// Tangent, Binormal, Normal은 버텍스 셰이더에서 View공간으로 변환된
		// 축정보들이다. 이 축정보를 이용해서 행렬을 아래처럼 구성하면
		// Tx Bx Nx
		// Ty By Ny
		// Tz Bz Nz
		// 이렇게 구성하면 뷰 공간에 있는 어느 벡터를 탄젠트 공간으로 변환하는
		// 행렬이 만들어진다. 
		// NormalMap에 저장된 법선 정보는 탄젠트공간에 존재하는 법선들이다.
		// 이 법선정보를 뷰공간으로 바꾸려면 위 행렬의 역행렬이 필요하다.
		// Tx Ty Tz
		// Bx By Bz
		// Nx Ny Nz
		// 그래서 NormalMap에서 뽑아온 벡터를 위 행렬에 곱하게 되면 탄젠트공간에 있던
		// 법선 정보가 뷰공간으로 변환된다.
        float3x3 mat = float3x3(input.vTangent, input.vBinormal, input.vNormal);

        vNormal = mul(vNormal, mat);
        vNormal = normalize(vNormal);
    }

  //  _tagMtrl tMtrl = ComputeTerrainLight(vNormal, input.vViewPos, vUV,
		//input.vUV, input.iSplatCount);

    float4 vColor = g_DiffuseTex.Sample(g_DiffuseSmp, vUV);
    float4 vEntireColor = g_DiffuseTex.Sample(g_DiffuseSmp, vEntireUV);
    
    vColor = saturate((vColor * 0.3f) + (vEntireColor * 0.7f)) * 1.0;


	// clip 함수는 인자가 0보다 작을 경우 현재 픽셀을 폐기한다.
	// 깊이버퍼에도 깊이값이 쓰이지 않는다.
    if (vColor.a == 0.f)
        clip(-1);

    for (int i = 0; i < 8; i++)
    {
        vUVW = float3(input.vUV, i);
        float fAlpha = g_AlphaTexArray.Sample(g_AlphaSmp, vUVW).r;

        if (fAlpha == 0.f)
            continue;

        vUVW = float3(vUV, i);
        
        //for Smooth Splatting
        if (fOuterDist < 0)
        {
            if (g_fOuterRadius * g_fOuterRadius < -fOuterDist)
            {
                if (g_fInnerRadius * g_fInnerRadius < -fInnerDist)
                {

                }
                else
                {
                }
            }
        }

        if (fOuterDist > 0)
        {
            if (g_fOuterRadius * g_fOuterRadius > fOuterDist)
            {
                if (g_fInnerRadius * g_fInnerRadius > fInnerDist)
                {
                }
                else
                {
                   
                }
            }
        }
        vColor.rgb = vColor.rgb * (1.f - fAlpha) +
		g_SplatTexArray.Sample(g_SplatSmp, vUVW).rgb * fAlpha;
    }
    
    output.vTarget0.rgb = vColor.xyz; // *(tMtrl.vDif.xyz + tMtrl.vAmb.xyz) + tMtrl.vSpc.xyz;
    

    output.vTarget0.a = vColor.a;

    output.vTarget1.rgb = vNormal * 0.5f + 0.5f;
    output.vTarget1.a = 3.2f;

    float fDiffuse = ColorToPixel(float4(1.f, 1.f, 1.f, 1.f));
    float fAmbient = ColorToPixel(float4(1.f, 1.f, 1.f, 1.f));

    output.vTarget2.r = input.vProjPos.z / input.vProjPos.w;
    output.vTarget2.g = g_vMtrlDif.r*0.8;
    output.vTarget2.b = g_vMtrlAmb.r * 0.8;
	//output.vTarget2.g = fDiffuse;
	//output.vTarget2.b = fAmbient;
    output.vTarget2.a = input.vProjPos.w;

    float4 vSpc = g_vMtrlSpc;

    //if (g_iSpecular == 1)
    //{
    //    vSpc = g_SpecularTex.Sample(g_SpecularSmp, vUV);

    //    float3 vUVW;

    //    for (int i = 0; i < 8; i++)
    //    {
    //        vUVW = float3(input.vUV, i);
    //        float fAlpha = g_AlphaTexArray.Sample(g_AlphaSmp, vUVW).r;

    //        if (fAlpha == 0.f)
    //            continue;

    //        vUVW = float3(vUV, i);
    //        vSpc = vSpc * (1.f - fAlpha) +
				//g_SplatSTexArray.Sample(g_SplatSSmp, vUVW) * fAlpha;
    //    }
    //}
    //툰으로 하면서 스펙큘러 계산을 없앰.. 

    output.vTarget3 = vSpc;

	// 1. 브러쉬 효과 Outer
    if (fOuterDist < 0 )
    {
        if (g_fOuterRadius * g_fOuterRadius < -fOuterDist)
        {
            if (g_fOuterRadius * g_fOuterRadius > -fOuterDist + 1)
                output.vTarget0 = float4(1.f, 0.f, 0.f, 1.f);
        }
    }	

    if (fOuterDist > 0 )							   		 
    {		
        if (g_fOuterRadius * g_fOuterRadius > fOuterDist)
        {
            if (g_fOuterRadius * g_fOuterRadius < fOuterDist + 1)
                output.vTarget0 = float4(1.f, 0.f, 0.f, 1.f);
        }
    }

    // 2.  브러쉬 효과 Inner
    if (fInnerDist < 0)
    {
        if (g_fInnerRadius * g_fInnerRadius < -fInnerDist)
        {
            if (g_fInnerRadius * g_fInnerRadius > -fInnerDist + 2)
                output.vTarget0 = float4(0.f, 0.f, 1.f, 1.f);
        }
    }

    if (fInnerDist > 0)
    {
        if (g_fInnerRadius * g_fInnerRadius > fInnerDist)
        {
            if (g_fInnerRadius * g_fInnerRadius < fInnerDist + 2)
                output.vTarget0 = float4(0.f, 0.f, 1.f, 1.f);
        }
    }
    
	// 0 : 카툰렌더링 1 : , 
    output.vTarget3.a = g_iRenderFlag;


    return output;
}
