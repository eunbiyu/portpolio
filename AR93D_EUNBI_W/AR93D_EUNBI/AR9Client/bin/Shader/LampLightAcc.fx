
#include "Share.fx"

// ======================= Particle Light ===============================
struct GS_OUTPUT_LAMP_LIGHT
{
    float4 vPos : SV_POSITION;
    float2 vUV : TEXCOORD;
    float3 vLightPos : POSITION;
    float fLightRange : LIGHTRANGE;
};

struct VS_INPUT_LAMP_LIGHT
{
    float3 vPos : POSITION;
    float fLightRange : LIGHTRANGE;
};



cbuffer LampCBuffer : register(b13)
{
    float3 g_vCamAxisX;
    float Empty1;
    float3 g_vCamAxisY;
    float Empty2;
}


SamplerState g_GBufferSmp : register(s11);

texture2D g_GBufferNormal : register(t12);
texture2D g_GBufferDepth : register(t13);
texture2D g_GBufferSpecular : register(t14);


VS_INPUT_LAMP_LIGHT LampLightVS(VS_INPUT_LAMP_LIGHT input)
{
    return input;
}

[maxvertexcount(4)]
void LampLightGS(point VS_INPUT_LAMP_LIGHT input[1],
	inout TriangleStream<GS_OUTPUT_LAMP_LIGHT> triStream)
{
    float fHalfX = input[0].fLightRange * 1.5f;
    float fHalfY = input[0].fLightRange * 1.5f;

    float4 vPos[4];
    vPos[0] = float4(input[0].vPos + fHalfX * g_vCamAxisX - fHalfY * g_vCamAxisY, 1.f);
    vPos[1] = float4(input[0].vPos + fHalfX * g_vCamAxisX + fHalfY * g_vCamAxisY, 1.f);
    vPos[2] = float4(input[0].vPos - fHalfX * g_vCamAxisX - fHalfY * g_vCamAxisY, 1.f);
    vPos[3] = float4(input[0].vPos - fHalfX * g_vCamAxisX + fHalfY * g_vCamAxisY, 1.f);

    float2 vUV[4] =
    {
        float2(0.f, 1.f),
		float2(0.f, 0.f),
		float2(1.f, 1.f),
		float2(1.f, 0.f)
    };

    GS_OUTPUT_LAMP_LIGHT output;

	[unroll]
    for (int i = 0; i < 4; ++i)
    {
        float4 vProjPos = mul(vPos[i], g_matVP);
        float2 vProjUV = (float2) 0;
        vProjUV.x = vProjPos.x / vProjPos.w * 0.5f + 0.5f;
        vProjUV.y = vProjPos.y / vProjPos.w * -0.5f + 0.5f;
        output.vPos = vProjPos;
        output.vUV = vProjUV;
        output.vLightPos = mul(float4(input[0].vPos, 1.f), g_matView);
        output.fLightRange = input[0].fLightRange;

        triStream.Append(output);
    }
}

_tagMtrl ComputeLampLightAcc(float3 vLightPos, float fLightRange, float3 vNormal, float3 vViewPos,
	float2 vUV, float fSpcPower, float4 vMtrlDif, float4 vMtrlAmb)
{
    _tagMtrl tMtrl = (_tagMtrl) 0;
    float4 vSpc;

    vSpc = g_GBufferSpecular.Sample(g_GBufferSmp, vUV);
	//vSpc.a = 1.f;

	// 조명 타입에 따라 조명의 방향을 구하고 뷰공간으로 변환한다.
    float3 vLightDir = (float3) 0;
    float fIntensity = 1.f;

	// 점 조명일 경우
    vLightDir = vLightPos - vViewPos;

    float fDist = length(vLightDir);
    vLightDir = normalize(vLightDir);

    fIntensity = (1.f - fDist / fLightRange);
    if (fDist > fLightRange)
        fIntensity = 0.f;

    fIntensity = pow(fIntensity, 1.5f);

	// Diffuse를 구한다.
    float fDot = max(0, dot(vNormal, vLightDir));
	//if (vSpc.a == 0)
	//	fDot = (ceil(fDot * 3) / 3.f);

	//fDot = (ceil(fDot * 3) / 3.f);

    tMtrl.vDif = float4(1.0f, 1.0f, 1.0f, 1.f) * vMtrlDif * fIntensity;
    tMtrl.vAmb = float4(0.f, 0.f, 0.f, 1.f) * vMtrlAmb * fIntensity;

	// 정반사광을 구한다.
	// 반사벡터를 구해준다.
    float3 vReflect = 2.f * vNormal * dot(vNormal, vLightDir) - vLightDir;
    vReflect = normalize(vReflect);

    vSpc.a = 1.f;

	// 정점에서 카메라를 향하는 벡터를 만든다.
    float3 vView = -normalize(vViewPos);

    tMtrl.vSpc = float4(0.1f, 0.1f, 0.1f, 0.1f) * vSpc * pow(max(0, dot(vReflect, vView)), g_fSpecularPower) * fIntensity;
    return tMtrl;
}

struct PS_OUTPUT_LIGHTACC
{
    float4 vTarget0 : SV_Target;
    float4 vTarget1 : SV_Target1;
};

PS_OUTPUT_LIGHTACC LampLightAccPS(GS_OUTPUT_LAMP_LIGHT input)
{
    PS_OUTPUT_LIGHTACC output = (PS_OUTPUT_LIGHTACC) 0;

    float4 vDepth = g_GBufferDepth.Sample(g_GBufferSmp, input.vUV);

    if (vDepth.w == 0.f)
        clip(-1);

	/*if (vDepth.w < input.vLightPos.z)
	clip(-1);*/

	// uv좌표를 -1 ~ 1 사이로 만든다.
    float4 vProjPos;
    vProjPos.x = (input.vUV.x * 2.f - 1.f) * vDepth.w;
    vProjPos.y = (input.vUV.y * -2.f + 1.f) * vDepth.w;
    vProjPos.z = vDepth.x * vDepth.w;
    vProjPos.w = vDepth.w;

    float3 vViewPos = mul(vProjPos, g_matInvProj).xyz;

    float3 vLightDir = input.vLightPos - vViewPos;

    float fDist = length(vLightDir);

    if (fDist > input.fLightRange)
        clip(-1);

    float4 vNormalCol = g_GBufferNormal.Sample(g_GBufferSmp, input.vUV);

    if (vNormalCol.a == 0.f)
        clip(-1);

    float3 vNormal = vNormalCol * 2.f - 1.f;

    float4 vDif = (float4) vDepth.y;
    float4 vAmb = (float4) vDepth.z;

    _tagMtrl tMtrl = ComputeLampLightAcc(input.vLightPos, input.fLightRange, vNormal, vViewPos, input.vUV,
		vNormalCol.a, vDif, vAmb);

    output.vTarget0 = tMtrl.vDif*1.3f;
    output.vTarget1 = tMtrl.vSpc;

    return output;
}
