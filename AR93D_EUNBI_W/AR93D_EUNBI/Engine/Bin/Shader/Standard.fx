
#include "Share.fx"

VS_COLOR_OUTPUT StandardColorVS(VS_COLOR_INPUT input)
{
    VS_COLOR_OUTPUT output = (VS_COLOR_OUTPUT) 0;

    output.vPos = mul(float4(input.vPos, 1.f), g_matWVP);
    output.vColor = input.vColor;

    return output;
}

PS_OUTPUT StandardColorPS(VS_COLOR_OUTPUT input)
{
    PS_OUTPUT output = (PS_OUTPUT) 0;

    output.vTarget0 = input.vColor;

    return output;
}


VS_TEXNORMAL_OUTPUT StandardTexNormalVS(VS_TEXNORMAL_INPUT input)
{
    VS_TEXNORMAL_OUTPUT output = (VS_TEXNORMAL_OUTPUT) 0;

    output.vProjPos = mul(float4(input.vPos, 1.f), g_matWVP);
    output.vUV = input.vUV;
    output.vNormal = normalize(mul(float4(input.vNormal, 0.f), g_matWV).xyz);
	//output.vViewPos = mul(float4(input.vPos, 1.f), g_matWV);
    output.vPos = output.vProjPos;

    return output;
}


VS_TEXNORMAL_OUTPUT StandardTexNormalSkinningVS(VS_TEXNORMAL_ANIM_INPUT input)
{
    VS_TEXNORMAL_OUTPUT output = (VS_TEXNORMAL_OUTPUT) 0;

	// 스키닝
    _tagSkinning tSkinning = Skinning(input.vPos, input.vNormal,
		input.vWeights, input.vIndices);

    output.vProjPos = mul(float4(tSkinning.vPos, 1.f), g_matWVP);
    output.vUV = input.vUV;
    output.vNormal = normalize(mul(float4(tSkinning.vNormal, 0.f), g_matWV).xyz);
	//output.vViewPos = mul(float4(input.vPos, 1.f), g_matWV);
    output.vPos = output.vProjPos;

    return output;
}

PS_OUTPUT_GBUFFER StandardTexNormalPS(VS_TEXNORMAL_OUTPUT input)
{
    PS_OUTPUT_GBUFFER output = (PS_OUTPUT_GBUFFER) 0;

	//_tagMtrl	tMtrl = ComputeLight(input.vNormal, input.vViewPos, input.vUV);

    float4 vColor = g_DiffuseTex.Sample(g_DiffuseSmp, input.vUV);

	// clip 함수는 인자가 0보다 작을 경우 현재 픽셀을 폐기한다.
	// 깊이버퍼에도 깊이값이 쓰이지 않는다.
    if (vColor.a == 0.f)
        clip(-1);


    output.vTarget0.rgb = vColor.xyz; // *(tMtrl.vDif.xyz + tMtrl.vAmb.xyz) + tMtrl.vSpc.xyz;
   
    output.vTarget0.a = vColor.a;
	//output.vTarget0.a = 1.f;

    float fDiffuse = ColorToPixel(g_vMtrlDif);
    float fAmbient = ColorToPixel(g_vMtrlAmb);

    output.vTarget1.rgb = input.vNormal * 0.5f + 0.5f;
    output.vTarget1.a = g_fSpecularPower;

    output.vTarget2.r = input.vProjPos.z / input.vProjPos.w;
	//output.vTarget2.r = g_vMtrlDif.r;
    output.vTarget2.g = g_vMtrlDif.r;
    output.vTarget2.b = g_vMtrlAmb.r;
    output.vTarget2.a = input.vProjPos.w;

    float4 vSpc = g_vMtrlSpc;

    if (g_iSpecular == 1)
    {
        vSpc = g_SpecularTex.Sample(g_SpecularSmp, input.vUV);
    }
    //vSpc = float4(1.f, 1.f, 1.f, 1.f);

    //output.vTarget3 = vSpc;
	// 0 : 카툰렌더링 1 : 일반렌더링
    output.vTarget3.xyz = g_vOutlineColor;
    output.vTarget3.a = g_iRenderFlag;

    return output;
}

VS_BUMP_OUTPUT StandardBumpVS(VS_BUMP_INPUT input)
{
    VS_BUMP_OUTPUT output = (VS_BUMP_OUTPUT) 0;

    output.vProjPos = mul(float4(input.vPos, 1.f), g_matWVP);
    output.vUV = input.vUV;
    output.vNormal = normalize(mul(float4(input.vNormal, 0.f), g_matWV).xyz);
	//output.vViewPos = mul(float4(input.vPos, 1.f), g_matWV);
    output.vPos = output.vProjPos;

    if (g_iBump == 1)
    {
        output.vTangent = normalize(mul(float4(input.vTangent, 0.f), g_matWV).xyz);
        output.vBinormal = normalize(mul(float4(input.vBinormal, 0.f), g_matWV).xyz);
    }

    return output;
}

VS_BUMP_OUTPUT StandardAnimBumpVS(VS_ANIM_BUMP_INPUT input)
{
    VS_BUMP_OUTPUT output = (VS_BUMP_OUTPUT) 0;

	// 스키닝
    _tagSkinning tSkinning = Skinning(input.vPos, input.vNormal,
		input.vTangent, input.vBinormal, input.vWeights, input.vIndices);

    output.vProjPos = mul(float4(tSkinning.vPos, 1.f), g_matWVP);
    output.vUV = input.vUV;
    output.vNormal = normalize(mul(float4(tSkinning.vNormal, 0.f), g_matWV).xyz);
	//output.vViewPos = mul(float4(tSkinning.vPos, 1.f), g_matWV);
    output.vPos = output.vProjPos;

    if (g_iBump == 1)
    {
        output.vTangent = normalize(mul(float4(tSkinning.vTangent, 0.f), g_matWV).xyz);
        output.vBinormal = normalize(mul(float4(tSkinning.vBinormal, 0.f), g_matWV).xyz);
    }

    return output;
}

PS_OUTPUT_GBUFFER StandardBumpPS(VS_BUMP_OUTPUT input)
{
    PS_OUTPUT_GBUFFER output = (PS_OUTPUT_GBUFFER) 0;

	// 법선맵이 있을 경우 법선을 새로 구한다.
    float3 vNormal = input.vNormal;

    if (g_iBump == 1)
    {
        vNormal = ComputeNormal(input.vTangent, input.vBinormal, input.vNormal,
			input.vUV);
    }

	//_tagMtrl	tMtrl = ComputeLight(vNormal, input.vViewPos, input.vUV);

    float4 vColor = g_DiffuseTex.Sample(g_DiffuseSmp, input.vUV);

	// clip 함수는 인자가 0보다 작을 경우 현재 픽셀을 폐기한다.
	// 깊이버퍼에도 깊이값이 쓰이지 않는다.
    if (vColor.a == 0.f)
        clip(-1);

    output.vTarget0.rgb = vColor.xyz; // *(tMtrl.vDif.xyz + tMtrl.vAmb.xyz) + tMtrl.vSpc.xyz;
    output.vTarget0.a = vColor.a;

    float fDiffuse = ColorToPixel(g_vMtrlDif);
    float fAmbient = ColorToPixel(g_vMtrlAmb);

    output.vTarget1.rgb = vNormal * 0.5f + 0.5f;
    output.vTarget1.a = g_fSpecularPower;

    output.vTarget2.r = input.vProjPos.z / input.vProjPos.w;
	//output.vTarget2.r = g_vMtrlDif.r;
    output.vTarget2.g = g_vMtrlDif.r;
    output.vTarget2.b = g_vMtrlAmb.r;
    output.vTarget2.a = input.vProjPos.w;

    float4 vSpc = g_vMtrlSpc;

    //if (g_iSpecular == 1)
    //{
    //    vSpc = g_SpecularTex.Sample(g_SpecularSmp, input.vUV);
    //}

    output.vTarget3.xyz = g_vOutlineColor;
	// 0 : 카툰렌더링 1 : 일반렌더링
    output.vTarget3.a = g_iRenderFlag;

    return output;
}
