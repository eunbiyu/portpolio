
// Color Vertex Input Structure
struct VS_COLOR_INPUT
{
    float3 vPos : POSITION;
    float4 vColor : COLOR;
};

// Color Vertex Output Structure
struct VS_COLOR_OUTPUT
{
	// SV : System Value
    float4 vPos : SV_POSITION;
    float4 vColor : COLOR;
};

// Tex Vertex Input Structure
struct VS_TEX_INPUT
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
};

struct VS_TEX_OUTPUT
{
    float4 vPos : SV_POSITION;
    float2 vUV : TEXCOORD;
};

// Tex Normal Vertex Input Structure
struct VS_TEXNORMAL_INPUT
{
    float3 vPos : POSITION;
    float3 vNormal : NORMAL;
    float2 vUV : TEXCOORD;
};

// Tex Normal Vertex Input Structure
struct VS_TEXNORMAL_ANIM_INPUT
{
    float3 vPos : POSITION;
    float3 vNormal : NORMAL;
    float2 vUV : TEXCOORD;
    float4 vWeights : BLENDWEIGHTS;
    float4 vIndices : BLENDINDICES;
};

struct VS_TEXNORMAL_OUTPUT
{
    float4 vPos : SV_POSITION;
    float3 vNormal : NORMAL;
    float2 vUV : TEXCOORD;
    float4 vProjPos : POSITION;
};

struct VS_BUMP_INPUT
{
    float3 vPos : POSITION;
    float3 vNormal : NORMAL;
    float2 vUV : TEXCOORD;
    float3 vTangent : TANGENT;
    float3 vBinormal : BINORMAL;
};

struct VS_ANIM_BUMP_INPUT
{
    float3 vPos : POSITION;
    float3 vNormal : NORMAL;
    float2 vUV : TEXCOORD;
    float3 vTangent : TANGENT;
    float3 vBinormal : BINORMAL;
    float4 vWeights : BLENDWEIGHTS;
    float4 vIndices : BLENDINDICES;
};

struct VS_BUMP_OUTPUT
{
    float4 vPos : SV_POSITION;
    float3 vNormal : NORMAL;
    float2 vUV : TEXCOORD;
    float4 vProjPos : POSITION;
    float3 vTangent : TANGENT;
    float3 vBinormal : BINORMAL;
};


// Single Target Output Structure
struct PS_OUTPUT
{
    float4 vTarget0 : SV_Target;
};

struct PS_OUTPUT_GBUFFER
{
    float4 vTarget0 : SV_Target;
    float4 vTarget1 : SV_Target1;
    float4 vTarget2 : SV_Target2;
    float4 vTarget3 : SV_Target3;
};

/*
상수버퍼 : 쉐이더 외부에서 값을 넘겨줄때 사용한다.
*/
cbuffer Transform : register(b0)
{
    matrix g_matWorld;
    matrix g_matView;
    matrix g_matProj;
    matrix g_matWV;
    matrix g_matWVP;
    matrix g_matVP;
    matrix g_matInvProj;
    matrix g_matInvView;
    matrix g_matInvVP;
    float3 g_vTrPivot;
    float g_vTrEmpty;
    float3 g_vMeshSize;
    float g_vTrEmpty1;
    float3 g_vMeshMin;
    float g_vTrEmpty2;
    float3 g_vMeshMax;
    float g_vTrEmpty3;
}

cbuffer Material : register(b1)
{
    float4 g_vMtrlDif;
    float4 g_vMtrlAmb;
    float4 g_vMtrlSpc;
    float4 g_vMtrlEmv;
    int g_iBump;
    int g_iSpecular;
    float g_fSpecularPower;
    int g_iMtrlEmpty;
}

cbuffer Light : register(b2)
{
    float4 g_vLightDif;
    float4 g_vLightAmb;
    float4 g_vLightSpc;
    int g_iLightType;
    float3 g_vLightDir;
    float3 g_vLightPos;
    float g_fLightRange;
}

cbuffer Rendering : register(b3)
{
    int g_iRenderFlag;
    float3 g_vOutlineColor;
}


// Diffuse Texture
texture2D g_DiffuseTex : register(t0);
SamplerState g_DiffuseSmp : register(s0);

// Normal Texture
texture2D g_NormalTex : register(t1);
SamplerState g_NormalSmp : register(s1);

// Specular Texture
texture2D g_SpecularTex : register(t2);
SamplerState g_SpecularSmp : register(s2);

// Bone Texture
texture2D g_BoneTex : register(t3);
SamplerState g_BoneSmp : register(s3);

struct _tagMtrl
{
    float4 vDif;
    float4 vAmb;
    float4 vSpc;
    float4 vEmiss;
};

float3 ComputeNormal(float3 vTangent, float3 vBinormal, float3 vVtxNormal,
	float2 vUV)
{
    float4 vNormalMap = g_NormalTex.Sample(g_NormalSmp, vUV);
    float3 vNormal = vNormalMap.xyz * 2.f - 1.f;

	// 기저벡터를 이용하여 탄젠트공간의 법선을 View 공간으로 만들어준다.
	// Tangent, Binormal, Normal은 버텍스 셰이더에서 View공간으로 변환된
	// 축정보들이다. 이 축정보를 이용해서 행렬을 아래처럼 구성하면
	// Tx Bx Nx
	// Ty By Ny
	// Tz Bz Nz
	// 이렇게 구성하면 뷰 공간에 있는 어느 벡터를 탄젠트 공간으로 변환하는
	// 행렬이 만들어진다. 
	// NormalMap에 저장된 법선 정보는 탄젠트공간에 존재하는 법선들이다.
	// 이 법선정보를 뷰공간으로 바꾸려면 위 행렬의 역행렬이 필요하다.
	// Tx Ty Tz
	// Bx By Bz
	// Nx Ny Nz
	// 그래서 NormalMap에서 뽑아온 벡터를 위 행렬에 곱하게 되면 탄젠트공간에 있던
	// 법선 정보가 뷰공간으로 변환된다.
    float3x3 mat = float3x3(vTangent, vBinormal, vVtxNormal);

    vNormal = mul(vNormal, mat);
    vNormal = normalize(vNormal);

    return vNormal;
}

_tagMtrl ComputeLight(float3 vNormal, float3 vViewPos, float2 vUV)
{
	// 조명 타입에 따라 조명의 방향을 구하고 뷰공간으로 변환한다.
    float3 vLightDir = (float3) 0;
    float fIntensity = 1.f;
	// 방향성 조명일 경우
    if (g_iLightType == 0)
    {
        vLightDir = mul(float4(g_vLightDir, 0.f), g_matView).xyz;
        vLightDir = normalize(vLightDir);
    }

	// 점 조명일 경우
    if (g_iLightType == 1)
    {
        vLightDir = mul(float4(g_vLightPos, 1.f), g_matView).xyz;
        vLightDir = vLightDir - vViewPos;

        float fDist = length(vLightDir);
        vLightDir = normalize(vLightDir);

        fIntensity = 1.f / fDist;
        if (fDist > g_fLightRange)
            fIntensity = 0.f;
    }

	// Spot
    if (g_iLightType == 2)
    {
    }

    _tagMtrl tMtrl = (_tagMtrl) 0;

	// Diffuse를 구한다.
    tMtrl.vDif = g_vLightDif * g_vMtrlDif * max(0, dot(vNormal, vLightDir)) * fIntensity;
    tMtrl.vAmb = g_vLightAmb * g_vMtrlAmb * max(0, dot(vNormal, vLightDir)) * fIntensity;

	// 정반사광을 구한다.
	// 반사벡터를 구해준다.
    float3 vReflect = 2.f * vNormal * dot(vNormal, vLightDir) - vLightDir;
    vReflect = normalize(vReflect);

	// 정점에서 카메라를 향하는 벡터를 만든다.
    float3 vView = -normalize(vViewPos);

    float4 vSpc = g_vMtrlSpc;

    if (g_iSpecular == 1)
    {
        vSpc.rgb = g_SpecularTex.Sample(g_SpecularSmp, vUV).rgb;
    }

    tMtrl.vSpc = g_vLightSpc * vSpc * pow(max(0, dot(vReflect, vView)), g_fSpecularPower) * fIntensity;

    return tMtrl;
}

struct _tagSkinning
{
    float3 vPos;
    float3 vNormal;
    float3 vTangent;
    float3 vBinormal;
};

matrix GetBoneMatrix(int idx)
{
    matrix matBone =
    {
        g_BoneTex.Load(int3(idx * 4, 0, 0)),
		g_BoneTex.Load(int3(idx * 4 + 1, 0, 0)),
		g_BoneTex.Load(int3(idx * 4 + 2, 0, 0)),
		g_BoneTex.Load(int3(idx * 4 + 3, 0, 0))
    };

    return matBone;
}

_tagSkinning Skinning(float3 vPos, float3 vNormal, float3 vTangent,
	float3 vBinormal, float4 vWeights, float4 vIndices)
{
    _tagSkinning tSkinning = (_tagSkinning) 0;

	/*if (g_BoneTex == null)
	{
		tSkinning.vPos = vPos;
		tSkinning.vNormal = vNormal;
		tSkinning.vTangent = vTangent;
		tSkinning.vBinormal = vBinormal;
		return tSkinning;
	}*/

    float fWeights[4];
    fWeights[0] = vWeights.x;
    fWeights[1] = vWeights.y;
    fWeights[2] = vWeights.z;
    fWeights[3] = 1.f - vWeights.x - vWeights.y - vWeights.z;

    for (int i = 0; i < 4; ++i)
    {
        matrix matBone = GetBoneMatrix((int) vIndices[i]);

        tSkinning.vPos += fWeights[i] * mul(float4(vPos, 1.f), matBone).xyz;
        tSkinning.vNormal += fWeights[i] * mul(float4(vNormal, 0.f), matBone).xyz;
        tSkinning.vTangent += fWeights[i] * mul(float4(vTangent, 0.f), matBone).xyz;
        tSkinning.vBinormal += fWeights[i] * mul(float4(vBinormal, 0.f), matBone).xyz;
    }

    tSkinning.vNormal = normalize(tSkinning.vNormal);
    tSkinning.vTangent = normalize(tSkinning.vTangent);
    tSkinning.vBinormal = normalize(tSkinning.vBinormal);
	
    return tSkinning;
}

_tagSkinning Skinning(float3 vPos, float3 vNormal, float4 vWeights, float4 vIndices)
{
    _tagSkinning tSkinning = (_tagSkinning) 0;

    float fWeights[4];
    fWeights[0] = vWeights.x;
    fWeights[1] = vWeights.y;
    fWeights[2] = vWeights.z;
    fWeights[3] = 1.f - vWeights.x - vWeights.y - vWeights.z;

    for (int i = 0; i < 4; ++i)
    {
        matrix matBone = GetBoneMatrix((int) vIndices[i]);

        tSkinning.vPos += fWeights[i] * mul(float4(vPos, 1.f), matBone).xyz;
        tSkinning.vNormal += fWeights[i] * mul(float4(vNormal, 0.f), matBone).xyz;
    }

    tSkinning.vNormal = normalize(tSkinning.vNormal);

    return tSkinning;
}

int ColorToPixel(float4 vColor)
{
    int r, g, b, a;
    r = vColor.r * 255;
    g = vColor.g * 255;
    b = vColor.b * 255;
    a = vColor.a * 255;
	
    int iPixel = (r & 0xff000000) >> 24;
    iPixel <<= 8;
    iPixel |= ((g & 0xff000000) >> 24);
    iPixel <<= 8;
    iPixel |= ((b & 0xff000000) >> 24);
    iPixel <<= 8;
    iPixel |= ((a & 0xff000000) >> 24);

    return iPixel;
}

float4 PixelToColor(float fPixel)
{
    float4 vColor;
    int iPixel = (int) fPixel;

    vColor.a = iPixel & 0x000000ff;
    iPixel >>= 8;
    vColor.b = iPixel & 0x000000ff;
    iPixel >>= 8;
    vColor.g = iPixel & 0x000000ff;
    iPixel >>= 8;
    vColor.r = iPixel & 0x000000ff;

    return vColor;
}
