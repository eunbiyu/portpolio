
#include "Share.fx"

cbuffer Effect : register(b11)
{
    float3 g_vCenter;
    float g_fEmpty1;
    float3 g_vCamAxisY;
    float g_fEmpty2;
    float3 g_vCamPos;
    float g_fEmpty3;
    float2 g_vSize;
    float2 g_fEmpty4;
}

cbuffer Animation2D : register(b12)
{
    int g_iAnimType;
    int g_iAnimFrameX;
    int g_iAnimFrameY;
    int g_iAnimFrameMaxX;
    int g_iAnimFrameMaxY;
    float3 vEmpty;
}

struct VS_INPUT_POS
{
    float3 vPos : POSITION;
};

struct VS_OUTPUT_POS
{
    float3 vPos : POSITION;
};

struct GS_OUTPUT
{
    float4 vPos : SV_Position;
    float2 vUV : TEXCOORD;
    float4 vProjPos : POSITION;
    uint iPrimID : SV_PrimitiveID;
};

Texture2DArray g_EffectTexArray : register(t11);
texture2D g_GBufferDepth : register(t12);

SamplerState g_GBufferDepthSmp : register(s12);

float2 ComputeAtlasUV(float2 vCurUV)
{
    float2 vUV = (float2) 0;

    vUV.x = (g_iAnimFrameX + vCurUV.x) / g_iAnimFrameMaxX;
    vUV.y = (g_iAnimFrameY + vCurUV.y) / g_iAnimFrameMaxY;

    return vUV;
}

VS_OUTPUT_POS EffectVS(VS_INPUT_POS input)
{
    VS_OUTPUT_POS output = (VS_OUTPUT_POS) 0;

    output.vPos = input.vPos;

    return output;
}

[maxvertexcount(4)]
void EffectGS(point VS_OUTPUT_POS input[1],
	uint iPrimID : SV_PrimitiveID,
	inout TriangleStream<GS_OUTPUT> TriStream)
{
    float3 vView = g_vCamPos - g_vCenter;
    vView = normalize(vView);

    float3 vUp = float3(0.f, 1.f, 0.f);
    float3 vRight = cross(vUp, vView);
    vRight = normalize(vRight);
	//vUp = g_vCamAxisY;
    vUp = cross(vView, vRight);
    vUp = normalize(vUp);

    float fHalfX = g_vSize.x * 0.5f;
    float fHalfY = g_vSize.y * 0.5f;
	//fHalfX = 1.f;
	//fHalfY = 1.f;

    float4 vPos[4];
    vPos[0] = float4(g_vCenter + fHalfX * vRight - fHalfY * vUp, 1.f);
    vPos[1] = float4(g_vCenter + fHalfX * vRight + fHalfY * vUp, 1.f);
    vPos[2] = float4(g_vCenter - fHalfX * vRight - fHalfY * vUp, 1.f);
    vPos[3] = float4(g_vCenter - fHalfX * vRight + fHalfY * vUp, 1.f);
	/*vPos[0] = float4(-g_vSize.x, -g_vSize.y, 0.f, 1.f);
	vPos[1] = float4(-g_vSize.x, g_vSize.y, 0.f, 1.f);
	vPos[2] = float4(g_vSize.x, -g_vSize.y, 0.f, 1.f);
	vPos[3] = float4(g_vSize.x, g_vSize.y, 0.f, 1.f);*/
	/*vPos[0] = float4(g_vCamPos.x - 0.5f, g_vCamPos.y - 0.5f, 0.f, 1.f);
	vPos[1] = float4(g_vCamPos.x - 0.5f, g_vCamPos.y + 0.5f, 0.f, 1.f);
	vPos[2] = float4(g_vCamPos.x + 0.5f, g_vCamPos.y - 0.5f, 0.f, 1.f);
	vPos[3] = float4(g_vCamPos.x + 0.5f, g_vCamPos.y + 0.5f, 0.f, 1.f);*/

    float2 vUV[4] =
    {
        float2(0.f, 1.f),
		float2(0.f, 0.f),
		float2(1.f, 1.f),
		float2(1.f, 0.f),
    };

	// Atlas
    if (g_iAnimType == 1)
    {
        for (int i = 0; i < 4; ++i)
        {
            vUV[i] = ComputeAtlasUV(vUV[i]);
        }
    }

    GS_OUTPUT output;

	[unroll]
    for (int i = 0; i < 4; ++i)
    {
        output.vProjPos = mul(vPos[i], g_matVP);
        output.vUV = vUV[i];
        output.iPrimID = iPrimID;
        output.vPos = output.vProjPos;

        TriStream.Append(output);
    }
}


PS_OUTPUT EffectPS(GS_OUTPUT input)
{
    PS_OUTPUT output = (PS_OUTPUT) 0;

    if (g_iAnimType == 2)
    {
        float3 vUV = float3(input.vUV, g_iAnimFrameX);
        output.vTarget0 = g_EffectTexArray.Sample(g_DiffuseSmp, vUV);
    }
    else
        output.vTarget0 = g_DiffuseTex.Sample(g_DiffuseSmp, input.vUV);

    if (output.vTarget0.a == 0.f)
        clip(-1);

	// 깊이값을 얻어온다.
	// GBufferDepth에서 깊이를 얻어오기 위한 투영좌표를 이용한 UV 좌표를 구해준다.
    float2 vDepthUV;
    vDepthUV.x = input.vProjPos.x / input.vProjPos.w * 0.5f + 0.5f;
    vDepthUV.y = input.vProjPos.y / input.vProjPos.w * -0.5f + 0.5f;
    float fDepth = g_GBufferDepth.Sample(g_GBufferDepthSmp, vDepthUV).w;
    float fSrcDepth = input.vProjPos.w;

    float fAlpha = (fDepth - fSrcDepth) / 0.5f;

    if (fAlpha > 1.f)
        fAlpha = 1.f;
    else if (fAlpha < 0.f)
        fAlpha = 0.f;

    if (fDepth == 0.f)
        fAlpha = 1.f;

    output.vTarget0.a *= fAlpha;

    return output;
}

// ======================= Particle Shader ============================
struct VS_INPUT_PARTICLE
{
    float3 vPos : POSITION;
    float3 vVelocity : VELOCITY;
    float2 vSize : SIZE;
    float fAge : AGE;
    unsigned int iType : TYPE;
};

struct VS_OUTPUT_PARTICLE
{
    float3 vPos : SV_POSITION;
    float3 vVelocity : VELOCITY;
    float2 vSize : SIZE;
    float fAge : AGE;
    unsigned int iType : TYPE;
};

cbuffer ParticleCBuffer : register(b13)
{
    float3 g_vParticlePos;
    float g_fParticleEmpty;
}

VS_INPUT_PARTICLE ParticleStreamOutVS(VS_INPUT_PARTICLE input)
{
    return input;
}

[maxvertexcount(20)]
void ParticleStreamOutGS(point VS_INPUT_PARTICLE input[1],
	inout PointStream<VS_INPUT_PARTICLE> pointStream)
{
}

VS_INPUT_PARTICLE ParticleVS(VS_INPUT_PARTICLE input)
{
    return input;
}

struct GS_OUTPUT_PARTICLE
{
    float4 vPos : SV_POSITION;
    float2 vUV : TEXCOORD;
    float4 vProjPos : POSITION;
};

[maxvertexcount(4)]
void ParticleGS(point VS_INPUT_PARTICLE input[1],
	inout TriangleStream<GS_OUTPUT_PARTICLE> triStream)
{
}

PS_OUTPUT ParticlePS(GS_OUTPUT_PARTICLE input)
{
    PS_OUTPUT output = (PS_OUTPUT) 0;

    if (g_iAnimType == 2)
    {
        float3 vUV = float3(input.vUV, g_iAnimFrameX);
        output.vTarget0 = g_EffectTexArray.Sample(g_DiffuseSmp, vUV);
    }
    else
        output.vTarget0 = g_DiffuseTex.Sample(g_DiffuseSmp, input.vUV);

    if (output.vTarget0.a == 0.f)
        clip(-1);

	// 깊이값을 얻어온다.
	// GBufferDepth에서 깊이를 얻어오기 위한 투영좌표를 이용한 UV 좌표를 구해준다.
    float2 vDepthUV;
    vDepthUV.x = input.vProjPos.x / input.vProjPos.w * 0.5f + 0.5f;
    vDepthUV.y = input.vProjPos.y / input.vProjPos.w * -0.5f + 0.5f;
    float fDepth = g_GBufferDepth.Sample(g_GBufferDepthSmp, vDepthUV).w;
    float fSrcDepth = input.vProjPos.w;

    float fAlpha = (fDepth - fSrcDepth) / 0.5f;

    if (fAlpha > 1.f)
        fAlpha = 1.f;
    else if (fAlpha < 0.f)
        fAlpha = 0.f;

    if (fDepth == 0.f)
        fAlpha = 1.f;

    output.vTarget0.a *= fAlpha;

    return output;
}
