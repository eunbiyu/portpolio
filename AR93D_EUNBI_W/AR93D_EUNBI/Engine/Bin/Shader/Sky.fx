
cbuffer Transform	: register(b0)
{
	matrix	g_matWorld;
	matrix	g_matView;
	matrix	g_matProj;
	matrix	g_matWV;
	matrix	g_matWVP;
}

struct VS_INPUT
{
	float3	vPos	: POSITION;
	float2	vUV		: TEXCOORD;
};

struct VS_OUTPUT
{
	float4	vPos		: SV_POSITION;
	float3  vCubeDir		: POSITION;
	float2	vUV		    :  TEXCOORD;
};

struct PS_OUTPUT
{
	float4	vTarget	: SV_Target;
};

// Diffuse Texture
TextureCube		g_DiffuseTex	: register(t0);
SamplerState	g_DiffuseSmp	: register(s0);

VS_OUTPUT SkyVS(VS_INPUT input)
{
	VS_OUTPUT	output = (VS_OUTPUT)0;

	output.vPos = mul(float4(input.vPos, 0.f), g_matView);
	output.vPos = mul(output.vPos, g_matProj);

	output.vUV = input.vUV;

	output.vPos.z = output.vPos.w - 0.01f;

	output.vCubeDir = normalize(input.vPos);


	return output;
}

PS_OUTPUT SkyPS(VS_OUTPUT input)
{
	PS_OUTPUT	output = (PS_OUTPUT)0;

	output.vTarget = g_DiffuseTex.Sample(g_DiffuseSmp, input.vCubeDir);

	//output.vTarget = float4(0.f, 1.f, 0.f, 0.f);

	return output;
}
