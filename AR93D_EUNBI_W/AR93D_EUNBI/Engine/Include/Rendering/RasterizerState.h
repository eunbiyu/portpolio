#pragma once
#include "RenderState.h"

AR3D_BEGIN

class DLL CRasterizerState :
	public CRenderState
{
private:
	friend class CRenderManager;

private:
	CRasterizerState();
	~CRasterizerState();

public:
	bool CreateRasterizerState(const string& strKey,
		D3D11_FILL_MODE fillMode = D3D11_FILL_SOLID,
		D3D11_CULL_MODE cullMode = D3D11_CULL_BACK,
		BOOL frontCounterClockwise = FALSE,
		INT depthBias = D3D11_DEFAULT_DEPTH_BIAS,
		FLOAT depthBiasClamp = D3D11_DEFAULT_DEPTH_BIAS_CLAMP,
		FLOAT slopeScaledDepthBias = D3D11_DEFAULT_SLOPE_SCALED_DEPTH_BIAS,
		BOOL depthClipEnable = TRUE,
		BOOL scissorEnable = FALSE,
		BOOL multisampleEnable = FALSE,
		BOOL antialiasedLineEnable = FALSE);

public:
	virtual void SetState();
	virtual void ResetState();
};

AR3D_END
