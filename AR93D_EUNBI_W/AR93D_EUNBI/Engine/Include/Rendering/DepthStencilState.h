#pragma once
#include "RenderState.h"

AR3D_BEGIN

class DLL CDepthStencilState :
	public CRenderState
{
private:
	friend class CRenderManager;

private:
	CDepthStencilState();
	~CDepthStencilState();

protected:
	UINT	m_iStencilRef;
	UINT	m_iOldStencilRef;

public:
	bool CreateDepthStencilState(const string& strKey, bool bDepthEnable = true,
		D3D11_DEPTH_WRITE_MASK eDepthMask = D3D11_DEPTH_WRITE_MASK_ALL,
		D3D11_COMPARISON_FUNC eDepthFunc = D3D11_COMPARISON_LESS,
		bool bStencilEnable = false, UINT8 iStencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK,
		UINT8 iStencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK,
		D3D11_DEPTH_STENCILOP_DESC tFrontFace = { D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_COMPARISON_ALWAYS },
		D3D11_DEPTH_STENCILOP_DESC tBackFace = { D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_COMPARISON_ALWAYS });

public:
	virtual void SetState();
	virtual void ResetState();
};

AR3D_END


