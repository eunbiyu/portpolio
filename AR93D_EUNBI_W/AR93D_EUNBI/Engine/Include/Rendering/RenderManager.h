#pragma once

#include "../Component/LampLight.h"
#include "../Engine.h"

AR3D_BEGIN

typedef struct DLL _tagMRT
{
	vector<class CAR3DRenderTarget*>	vecTarget;
	class CAR3DDepthTarget*		pDepth;
	ID3D11DepthStencilView*		pNullDepth;
	vector<ID3D11RenderTargetView*>		vecOldTarget;
	ID3D11DepthStencilView*		pOldDepth;
}MRT, *PMRT;

class DLL CRenderManager
{
private:
	unordered_map<string, class CRenderState*>	m_mapRenderState;
	vector<D3D11_RENDER_TARGET_BLEND_DESC>	m_vecRenderTargetBlend;
	unordered_map<string, class CAR3DRenderTarget*>	m_mapRenderTarget;
	unordered_map<string, class CAR3DDepthTarget*>	m_mapDepthTarget;
	vector<class CGameObject*>	m_vecRender[RG_END];
	list<class CParticleSystem*>	m_ParticleLightList;
	list<class CLampLight*>			m_pLampLight;
	unordered_map<string, PMRT>		m_mapMRT;
	class CSampler*	m_pPointSmp;
	class CRenderState*	m_pBlendOne;
	class CRenderState*	m_pZDisable;
	class CRenderState*	m_pAlphaBlend;
	class CShader*		m_pLightAcc;
	class CShader*		m_pLightBlend;
	class CShader*		m_pLightBlendTarget;
	bool				m_bShadow;

public:
	void OnShadow(bool bShadow);
	void AddRenderObject(class CGameObject* pObj);

public:
	bool Init();
	void Render(float fTime);

private:
	void RenderGBuffer(float fTime);
	void RenderLightAcc(float fTime);
	void RenderLightBlend(float fTime);
	void RenderLightBlendTarget(float fTime);
	void UpdateTransform();
	void RenderShadowMap(float fTime);

public:
	bool AddMRTTarget(const string& strMRTKey, const string& strTargetKey);
	bool SetMRTDepth(const string& strMRTKey, const string& strDepthKey);
	PMRT FindMRT(const string& strKey);
	void ChangeMRT(const string& strKey);
	void ResetMRT(const string& strKey);
	void ClearMRT(const string& strKey);
	class CAR3DRenderTarget* CreateTarget(const string& strKey,
		int iWidth, int iHeight, DXGI_FORMAT eFmt);
	class CAR3DDepthTarget* CreateDepthTarget(const string& strKey,
		int iWidth, int iHeight, DXGI_FORMAT eFmt);
	void ChangeTarget(const string& strKey);
	void ResetTarget(const string& strKey);
	void ClearTarget(const string& strKey);
	void ClearDepthTarget(const string& strKey);
	void SaveTarget(const string& strKey, const wchar_t* pFileName,
		const string& strPathKey = TEXTURE_PATH);
	class CAR3DRenderTarget* FindTarget(const string& strKey);
	class CAR3DDepthTarget* FindDepthTarget(const string& strKey);

	class CRasterizerState* CreateRasterizerState(const string& strKey,
		D3D11_FILL_MODE fillMode = D3D11_FILL_SOLID,
		D3D11_CULL_MODE cullMode = D3D11_CULL_NONE,
		BOOL frontCounterClockwise = FALSE,
		INT depthBias = D3D11_DEFAULT_DEPTH_BIAS,
		FLOAT depthBiasClamp = D3D11_DEFAULT_DEPTH_BIAS_CLAMP,
		FLOAT slopeScaledDepthBias = D3D11_DEFAULT_SLOPE_SCALED_DEPTH_BIAS,
		BOOL depthClipEnable = TRUE,
		BOOL scissorEnable = FALSE,
		BOOL multisampleEnable = FALSE,
		BOOL antialiasedLineEnable = FALSE);

	class CBlendState* CreateBlendState(const string& strKey,
		bool bAlphaCoverage = false, bool bIndependent = false);

	bool AddRenderTargetBlendInfo(bool bEnable = true, D3D11_BLEND eSrcBlend = D3D11_BLEND_SRC_ALPHA,
		D3D11_BLEND eDestBlend = D3D11_BLEND_INV_SRC_ALPHA,
		D3D11_BLEND_OP eBlendOp = D3D11_BLEND_OP_ADD, D3D11_BLEND eSrcBlendAlpha = D3D11_BLEND_ONE,
		D3D11_BLEND eDestBlendAlpha = D3D11_BLEND_ZERO,
		D3D11_BLEND_OP eBlendOpAlpha = D3D11_BLEND_OP_ADD,
		UINT8 iTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL);

	class CDepthStencilState* CreateDepthStencilState(const string& strKey,
		bool bDepthEnable = true,
		D3D11_DEPTH_WRITE_MASK eDepthMask = D3D11_DEPTH_WRITE_MASK_ALL,
		D3D11_COMPARISON_FUNC eDepthFunc = D3D11_COMPARISON_LESS,
		bool bStencilEnable = false, UINT8 iStencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK,
		UINT8 iStencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK,
		D3D11_DEPTH_STENCILOP_DESC tFrontFace = { D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_COMPARISON_ALWAYS },
		D3D11_DEPTH_STENCILOP_DESC tBackFace = { D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_COMPARISON_ALWAYS });

	class CRenderState* FindRenderState(const string& strKey);

public:
	static bool ObjectZSort(class CGameObject* p1, class CGameObject* p2);
	static bool ObjectZSortDescending(class CGameObject* p1, class CGameObject* p2);

	DECLARE_SINGLE(CRenderManager)
};

AR3D_END
