#pragma once
#include "../BaseObj.h"

AR3D_BEGIN

class DLL CAR3DRenderTarget :
	public CBaseObj
{
private:
	friend class CRenderManager;

private:
	CAR3DRenderTarget();
	~CAR3DRenderTarget();

private:
	class CTexture*			m_pTargetTex;
	ID3D11RenderTargetView*	m_pTargetView;
	ID3D11RenderTargetView*	m_pOldView;
	ID3D11DepthStencilView*	m_pOldDepth;
	float		m_fClearColor[4];
//#ifdef _DEBUG
	bool			m_bIsRender;
	class CMesh*	m_pMesh;
	class CShader*	m_pShader;
	MATERIALINFO	m_tMtrl;
	DxVector3		m_vScale;
	DxVector3		m_vPos;
	class CSampler*	m_pSampler;
	class CRenderState*	m_pZDisable;
//#endif // _DEBUG


public:
	ID3D11RenderTargetView* GetTargetView()	const;

public:
	void OnRender(bool bRender);
	void SetPos(float x, float y, float z);
	void SetScale(float x, float y, float z);
	void SetClearColor(float r, float g, float b, float a);
	void SetClearColor(float fColor[4]);

public:
	bool CreateTarget(const string& strName, int iWidth, int iHeight, DXGI_FORMAT eFmt);
	void ChangeTarget();
	void ResetTarget();
	void ClearTarget();
	void SaveTarget(const wchar_t * pFileName,
		const string & strPathKey = TEXTURE_PATH);
	void SetTexture(int iRegister);
	void Render();

public: // For Terrain
	//void SaveTargetTerrain(const wchar_t * pFileName, bool breset = false, const string & strPathKey = TEXTURE_PATH);
	//void EditTerrainSplattingTexture(const wchar_t * pFileName, const string & strPathKey = TEXTURE_PATH);
};

AR3D_END
