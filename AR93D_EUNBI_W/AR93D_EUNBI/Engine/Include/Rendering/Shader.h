#pragma once
#include "../BaseObj.h"

AR3D_BEGIN

class DLL CShader :
	public CBaseObj
{
private:
	friend class CShaderManager;

private:
	CShader();
	~CShader();

private:
	ID3DBlob*			m_pVSBlob;
	ID3D11VertexShader*	m_pVS;

	ID3DBlob*			m_pPSBlob;
	ID3D11PixelShader*	m_pPS;

	ID3DBlob*				m_pGSBlob;
	ID3D11GeometryShader*	m_pGS;

	string		m_strKey;

	D3D11_SO_DECLARATION_ENTRY*		m_pStreamDecl;
	UINT							m_iDeclCount;

public:
	void* GetVSCode()	const;
	void* GetPSCode()	const;
	int GetVSCodeSize()	const;
	int GetPSCodeSize()	const;
	string GetKey()	const;

private:
	void SetStreamDecl(D3D11_SO_DECLARATION_ENTRY* pStreamDecl, UINT iCount);

public:
	bool LoadShader(const string& strKey, TCHAR* pFileName,
		char* pEntry[ST_END], bool bStreamOut = false, const string& strPathKey = SHADER_PATH);
	bool LoadVertexShader(const string& strKey, TCHAR* pFileName, char* pEntry,
		const string& strPathKey);
	bool LoadPixelShader(const string& strKey, TCHAR* pFileName, char* pEntry,
		const string& strPathKey);
	bool LoadGeometryShader(const string& strKey, TCHAR* pFileName, char* pEntry,
		bool bStreamOut, const string& strPathKey);
	void SetShader();
};

AR3D_END
