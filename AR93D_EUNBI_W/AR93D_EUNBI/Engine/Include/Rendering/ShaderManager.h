#pragma once

#include "../Engine.h"

AR3D_BEGIN

class DLL CShaderManager
{
private:
	unordered_map<string, class CShader*>	m_mapShader;
	unordered_map<string, ID3D11InputLayout*>	m_mapInputLayout;
	unordered_map<string, PCONSTANTBUFFER>		m_mapCBuffer;
	vector<D3D11_INPUT_ELEMENT_DESC>	m_vecElement;
	vector<D3D11_SO_DECLARATION_ENTRY>	m_vecStreamDecl;
	UINT		m_iOffsetSize;

public:
	bool Init();
	class CShader* LoadShader(const string& strKey, TCHAR* pFileName,
		char* pEntry[ST_END], bool bStreamOut = false, const string& strPathKey = SHADER_PATH);

	class CShader* FindShader(const string& strKey);

	void AddElement(char* pSemanticName, int iSemanticIndex, int iSize,
		DXGI_FORMAT eFmt, int iInputSlot, D3D11_INPUT_CLASSIFICATION eSlotClass,
		int iInstanceStep);
	bool CreateInputLayout(const string& strKey, const string& strShaderKey);
	void SetInputLayout(const string& strKey);

	ID3D11InputLayout* FindInputLayout(const string& strKey);

	bool CreateConstantBuffer(const string& strKey, int iSize, int iRegister);
	PCONSTANTBUFFER FindConstantBuffer(const string& strKey);
	void UpdateConstantBuffer(const string& strKey, void* pData, int iCut);

	void AddStreamDecl(UINT iStream, const char* pSemanticName, UINT iSemanticIdx,
		BYTE byStartCom, BYTE byComCount, BYTE byOutSlot);

	DECLARE_SINGLE(CShaderManager)
};

AR3D_END
