#pragma once
#include "../BaseObj.h"

AR3D_BEGIN

class DLL CAR3DDepthTarget :
	public CBaseObj
{
private:
	friend class CRenderManager;

private:
	CAR3DDepthTarget();
	~CAR3DDepthTarget();

private:
	ID3D11Texture2D*		m_pDepthTex;
	ID3D11DepthStencilView*	m_pDepthView;
	ID3D11DepthStencilView*	m_pOldView;

public:
	ID3D11DepthStencilView* GetDepthView()	const;

public:
	bool CreateDepth(const string& strName, int iWidth, int iHeight, DXGI_FORMAT eFmt);
	void ClearTarget();
};

AR3D_END
