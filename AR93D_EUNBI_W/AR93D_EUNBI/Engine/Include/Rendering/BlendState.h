#pragma once
#include "RenderState.h"

AR3D_BEGIN

class CBlendState :
	public CRenderState
{
private: 
	friend class CRenderManager;

private:
	CBlendState();
	~CBlendState();

private:
	UINT m_iSampleMask;
	UINT m_iOldSampleMask;
	float m_fBlendFactor[4];
	float m_fOldBlendFactor[4];

public:
	bool CreateBlendState(const string & strKey,
		const vector<D3D11_RENDER_TARGET_BLEND_DESC>* pTargetBlend,
		bool bAlphaCoverage = false, bool bIndependent = false);
	bool CreateAlphablendState();

public:
	virtual void SetState();
	virtual void ResetState();
};

AR3D_END