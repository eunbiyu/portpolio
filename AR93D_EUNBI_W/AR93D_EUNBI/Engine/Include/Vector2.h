
#pragma once

#include "Macro.h"
#include "DxMath.h"

AR3D_BEGIN

typedef struct DLL _tagDxVector2 :
	public XMFLOAT2
{
	_tagDxVector2();
	_tagDxVector2(float _x, float _y);
	_tagDxVector2(float _f[2]);
	_tagDxVector2(const _tagDxVector2& vec);
	
	void operator =(const _tagDxVector2& vec);
	void operator =(const struct _tagDxVector3& vec);
	void operator =(float _f[2]);

	// +
	_tagDxVector2 operator +(const _tagDxVector2& _v);
	_tagDxVector2 operator +(float _f[2]);
	_tagDxVector2 operator +(float fValue);
	_tagDxVector2 operator +(int iValue);
	_tagDxVector2 operator +(int i[2]);

	// +=
	void operator +=(const _tagDxVector2& _v);
	void operator +=(float _f[2]);
	void operator +=(float fValue);
	void operator +=(int iValue);
	void operator +=(int i[2]);

	// -
	_tagDxVector2 operator -(const _tagDxVector2& _v);
	_tagDxVector2 operator -(float _f[2]);
	_tagDxVector2 operator -(float fValue);
	_tagDxVector2 operator -(int iValue);
	_tagDxVector2 operator -(int i[2]);

	// -=
	void operator -=(const _tagDxVector2& _v);
	void operator -=(float _f[2]);
	void operator -=(float fValue);
	void operator -=(int iValue);
	void operator -=(int i[2]);

	// *
	_tagDxVector2 operator *(const _tagDxVector2& _v);
	_tagDxVector2 operator *(float _f[2]);
	_tagDxVector2 operator *(float fValue);
	_tagDxVector2 operator *(int iValue);
	_tagDxVector2 operator *(int i[2]);

	// *=
	void operator *=(const _tagDxVector2& _v);
	void operator *=(float _f[2]);
	void operator *=(float fValue);
	void operator *=(int iValue);
	void operator *=(int i[2]);

	// /
	_tagDxVector2 operator /(const _tagDxVector2& _v);
	_tagDxVector2 operator /(float _f[2]);
	_tagDxVector2 operator /(float fValue);
	_tagDxVector2 operator /(int iValue);
	_tagDxVector2 operator /(int i[2]);

	// /=
	void operator /=(const _tagDxVector2& _v);
	void operator /=(float _f[2]);
	void operator /=(float fValue);
	void operator /=(int iValue);
	void operator /=(int i[2]);

	// ++
	void operator ++();
	void operator --();

	// ==
	bool operator ==(const _tagDxVector2& _v);
	bool operator ==(float _f[2]);
	bool operator ==(int i[2]);

	// !=
	bool operator !=(const _tagDxVector2& _v);
	bool operator !=(float _f[2]);
	bool operator !=(int i[2]);

	float operator [](int idx);

	float Length();
	_tagDxVector2 Normalize();
}DxVector2, *PDxVector2;


typedef union DLL _tagVector2
{
	XMVECTOR	v;
	struct
	{
		float	x, y, z, w;
	};

	struct
	{
		float	f[4];
	};

	_tagVector2();
	_tagVector2(float _x, float _y);
	_tagVector2(float _f[2]);
	_tagVector2(const _tagDxVector2& vec);
	_tagVector2(const _tagVector2& vec);

	void* operator new(size_t size)
	{
		return _aligned_malloc(size, 16);
	}

	void operator delete(void* p)
	{
		_aligned_free(p);
	}

	void operator =(const _tagDxVector2& vec);
	void operator =(const _tagVector2& vec);
	void operator =(float _f[2]);

	// +
	_tagVector2 operator +(const _tagDxVector2& _v);
	_tagVector2 operator +(const _tagVector2& _v);
	_tagVector2 operator +(float _f[2]);
	_tagVector2 operator +(float fValue);
	_tagVector2 operator +(int iValue);
	_tagVector2 operator +(int i[2]);

	// +=
	void operator +=(const _tagDxVector2& _v);
	void operator +=(const _tagVector2& _v);
	void operator +=(float _f[2]);
	void operator +=(float fValue);
	void operator +=(int iValue);
	void operator +=(int i[2]);

	// -
	_tagVector2 operator -(const _tagDxVector2& _v);
	_tagVector2 operator -(const _tagVector2& _v);
	_tagVector2 operator -(float _f[2]);
	_tagVector2 operator -(float fValue);
	_tagVector2 operator -(int iValue);
	_tagVector2 operator -(int i[2]);

	// -=
	void operator -=(const _tagDxVector2& _v);
	void operator -=(const _tagVector2& _v);
	void operator -=(float _f[2]);
	void operator -=(float fValue);
	void operator -=(int iValue);
	void operator -=(int i[2]);

	// *
	_tagVector2 operator *(const _tagDxVector2& _v);
	_tagVector2 operator *(const _tagVector2& _v);
	_tagVector2 operator *(float _f[2]);
	_tagVector2 operator *(float fValue);
	_tagVector2 operator *(int iValue);
	_tagVector2 operator *(int i[2]);

	// *=
	void operator *=(const _tagDxVector2& _v);
	void operator *=(const _tagVector2& _v);
	void operator *=(float _f[2]);
	void operator *=(float fValue);
	void operator *=(int iValue);
	void operator *=(int i[2]);

	// /
	_tagVector2 operator /(const _tagDxVector2& _v);
	_tagVector2 operator /(const _tagVector2& _v);
	_tagVector2 operator /(float _f[2]);
	_tagVector2 operator /(float fValue);
	_tagVector2 operator /(int iValue);
	_tagVector2 operator /(int i[2]);

	// /=
	void operator /=(const _tagDxVector2& _v);
	void operator /=(const _tagVector2& _v);
	void operator /=(float _f[2]);
	void operator /=(float fValue);
	void operator /=(int iValue);
	void operator /=(int i[2]);

	// ++
	void operator ++();
	void operator --();

	// ==
	bool operator ==(const _tagDxVector2& _v);
	bool operator ==(const _tagVector2& _v);
	bool operator ==(float _f[2]);
	bool operator ==(int i[2]);

	// !=
	bool operator !=(const _tagDxVector2& _v);
	bool operator !=(const _tagVector2& _v);
	bool operator !=(float _f[2]);
	bool operator !=(int i[2]);

	float operator [](int idx);

	float Length();
	_tagVector2 Normalize();
}Vector2, *PVector2;

AR3D_END

