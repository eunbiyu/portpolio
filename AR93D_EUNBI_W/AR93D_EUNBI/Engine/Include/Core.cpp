#include "Core.h"

AR3D_USING

DEFINITION_SINGLE(CCore)

bool CCore::m_bRun = true;

CCore::CCore() :
	m_hInst(0)
{
}

CCore::~CCore()
{
}

bool CCore::Init(HINSTANCE hInst, unsigned int iWidth, unsigned int iHeight,
	TCHAR* pClass, TCHAR* pTitle, int iIconID, int iSmallIconID,
	bool bWindowMode)
{
	m_hInst = hInst;
	m_tResolution.iWidth = iWidth;
	m_tResolution.iHeight = iHeight;

	// 윈도우 창을 생성한다.
	MyRegisterClass(pClass, iIconID, iSmallIconID);
	Create(pClass, pTitle);

	return Init(m_hWnd, iWidth, iHeight, bWindowMode);
}

bool CCore::Init(HWND hWnd, unsigned int iWidth, unsigned int iHeight,
	bool bWindowMode)
{
	return true;
}

int CCore::Run()
{
	MSG msg;

	// 기본 메시지 루프입니다.
	while (m_bRun)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		else
		{
		}
	}

	return (int)msg.wParam;
}

ATOM CCore::MyRegisterClass(TCHAR * pClass, int iIconID, int iSmallIconID)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = CCore::WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = m_hInst;
	wcex.hIcon = LoadIcon(m_hInst, MAKEINTRESOURCE(iIconID));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = pClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(iSmallIconID));

	return RegisterClassExW(&wcex);
}

bool CCore::Create(TCHAR * pClass, TCHAR * pTitle)
{
	m_hWnd = CreateWindowW(pClass, pTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, m_hInst, nullptr);

	if (!m_hWnd)
	{
		return false;
	}

	RECT	rc = { 0, 0, m_tResolution.iWidth, m_tResolution.iHeight };

	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	SetWindowPos(m_hWnd, HWND_TOPMOST, 0, 0, rc.right - rc.left,
		rc.bottom - rc.top, SWP_NOZORDER | SWP_NOMOVE);

	ShowWindow(m_hWnd, SW_SHOW);
	UpdateWindow(m_hWnd);

	return true;
}

LRESULT CCore::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		m_bRun = false;
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
