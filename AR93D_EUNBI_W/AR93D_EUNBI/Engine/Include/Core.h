#pragma once

#include "Engine.h"
#include "BaseObj.h"

AR3D_BEGIN

class DLL CCore
{
private:
	RESOLUTION		m_tResolution;
	HINSTANCE		m_hInst;
	HWND			m_hWnd;
	static bool		m_bRun;

public:
	HWND GetWindowHandle()	const;
	RESOLUTION GetResolution()	const
	{
		return m_tResolution;
	}

	void Exit();

public:
	bool Init(HINSTANCE hInst, unsigned int iWidth, unsigned int iHeight,
		TCHAR* pClass, TCHAR* pTitle, int iIconID, int iSmallIconID,
		bool bWindowMode);
	bool Init(HWND hWnd, unsigned int iWidth, unsigned int iHeight,
		bool bWindowMode);
	int Run();
	void RunFromLoop();

private:
	void Logic();
	int Input(float fTime);
	int Update(float fTime);
	int LateUpdate(float fTime);
	void Collision(float fTime);
	void Render(float fTime);

private:
	ATOM MyRegisterClass(TCHAR* pClass, int iIconID, int iSmallIconID);
	bool Create(TCHAR* pClass, TCHAR* pTitle);
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	DECLARE_SINGLE(CCore)
};

AR3D_END
 