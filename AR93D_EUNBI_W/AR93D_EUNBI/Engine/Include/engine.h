
#pragma once

#include <Windows.h>
#include <list>
#include <conio.h>
#include <vector>
#include <unordered_map>
#include <crtdbg.h>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <string>
#include <algorithm>
#include <functional>
#include <process.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXCollision.h>
#include <dxgi.h>
#include <limits>
#include <d2d1.h>
#include <dwrite.h>
#include <atlstr.h>

#pragma comment(lib, "d2d1")
#pragma comment(lib, "dwrite")


using namespace std;

#pragma comment(lib, "d3d11")
#pragma comment(lib, "d3dcompiler")

// 사용자 정의 헤더파일
#include "Flag.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Matrix.h"

#ifdef _DEBUG
#pragma comment(lib, "DirectXTex_Debug")
#else
#pragma comment(lib, "DirectXTex")
#endif // _DEBUG

AR3D_BEGIN

#define	AR3D_PI	3.141592f
#define MAXTEXNUM		4
#define TEXALPHASIZE	128
#define MINIALPHASIZE	128

static Vector3	WORLDAXIS[AXIS_MAX] =
{
	Vector3(1.f, 0.f, 0.f),
	Vector3(0.f, 1.f, 0.f),
	Vector3(0.f, 0.f, 1.f)
};


#define	MAX_PARTICLES	10000


// Path Key
#define	BASE_PATH	"BasePath"
#define	SHADER_PATH	"ShaderPath"
#define	TEXTURE_PATH	"TexturePath"
#define	MESH_PATH		"MeshPath"
#define	OBJECT_MESH_PATH "ObjectMeshPath"
#define	OBJECT_INFO_PATH "ObjectInfoPath"
#define	DATA_PATH		"DataPath"


// Input Layout Key
#define	COLOR_INPUT		"ColorInputLayout"
#define	POS_INPUT		"PosInputLayout"
#define	TEXNORMAL_INPUT	"TexNormalInputLayout"
#define	TEX_INPUT		"TexInputLayout"
#define	TEXNORMAL_ANIM_INPUT	"TexNormalAnimInputLayout"
#define	BUMP_INPUT		"BumpInputLayout"
#define	ANIMBUMP_INPUT	"AnimBumpInputLayout"


//----------------------------SHADER KEY------------------------------//

// Default Shader Key
#define	STANDARD_COLOR_SHADER	"StandardColorShader"
#define	STANDARD_TEXNORMAL_SHADER	"StandardTexNormalShader"
#define	STANDARD_TEXNORMAL_ANIM_SHADER	"StandardTexNormalAnimShader"
#define	STANDARD_BUMP_SHADER	"StandardBumpShader"
#define	STANDARD_ANIM_BUMP_SHADER	"StandardAnimBumpShader"
#define	WATER_SHADER	"WaterShader"


// Terrain Shader Key
#define	TERRAIN_SHADER			"TerrainShader"

// Light Shader Key
#define	LIGHT_ACC_SHADER		"LightAcc"
#define	LIGHT_BLEND_SHADER		"LightBlend"
#define	LIGHT_BLEND_OUTPUT_SHADER		"LightBlendOutput"

// sky shader key
#define	SKY_SHADER	"SkyShader"

#define	EFFECT_SHADER	"EffectShader"
#define	PARTICLE_STREAMOUT_SHADER	"ParticleStreamOutShader"
#define	LAMPLIGHT_SHADER	"LampLightShader"
#define	PARTICLE_SHADER	"ParticleShader"
#define	PARTICLE_LIGHT_SHADER	"ParticleLightShader"

#define	COLLIDER_COLOR_SHADER	"ColliderColorShader"

// UI Shader Key
#define	UI_SHADER		"UIShader"


// OutLine Shader Key
#define	OUTLINE_TEXNORMAL_SHADER	"OutLineTexNormal"
#define	OUTLINE_TEXNORMAL_ANIM_SHADER	"OutLineTexNormalAnim"
#define	OUTLINE_BUMP_SHADER			"OutLineBump"
#define	OUTLINE_BUMP_ANIM_SHADER	"OutLineBumpAnim"


//----------------------------------------------------------------------------//

// RenderState Key
#define	WIRE_FRAME				"WireFrame"
#define	CULLING_CW				"CWCulling"
#define	ALPHABLEND				"AlphaBlend"
#define	DEPTH_LESS_EQUAL		"DepthLessEqual"
#define	DEPTH_DISABLE			"DepthDisable"
#define	ACC_BLEND				"AccBlend"
#define	ALPHAOBJ				"AlphaObj"
#define	DEPTH_WRITE_DISABLE		"DepthWriteDisable"

template <typename T>
void Safe_Release_VecList(T& p)
{
	T::iterator	iter;
	T::iterator	iterEnd = p.end();

	for (iter = p.begin(); iter != iterEnd; ++iter)
	{
		SAFE_RELEASE((*iter));
	}

	p.clear();
}

template <typename T>
void Safe_Delete_VecList(T& p)
{
	T::iterator	iter;
	T::iterator	iterEnd = p.end();

	for (iter = p.begin(); iter != iterEnd; ++iter)
	{
		SAFE_DELETE((*iter));
	}

	p.clear();
}

template <typename T>
void Safe_Release_Map(T& p)
{
	T::iterator	iter;
	T::iterator	iterEnd = p.end();

	for (iter = p.begin(); iter != iterEnd; ++iter)
	{
		SAFE_RELEASE(iter->second);
	}

	p.clear();
}

template <typename T>
void Safe_Delete_Map(T& p)
{
	T::iterator	iter;
	T::iterator	iterEnd = p.end();

	for (iter = p.begin(); iter != iterEnd; ++iter)
	{
		SAFE_DELETE(iter->second);
	}

	p.clear();
}

// Resolution
typedef struct DLL _tagResolution
{
	unsigned int		iWidth;
	unsigned int		iHeight;

	_tagResolution() :
		iWidth(0),
		iHeight(0)
	{
	}

	_tagResolution(unsigned int iW, unsigned int iH) :
		iWidth(iW),
		iHeight(iH)
	{
	}

	_tagResolution(const _tagResolution& t)
	{
		*this = t;
	}

	void operator =(const _tagResolution& t)
	{
		iWidth = t.iWidth;
		iHeight = t.iHeight;
	}
}RESOLUTION, *PRESOLUTION;

// Color Vertex
typedef struct DLL _tagVertexColor
{
	DxVector3	vPos;
	DxVector4	vColor;

	_tagVertexColor() :
		vPos(0.f, 0.f, 0.f),
		vColor(0.f, 0.f, 0.f, 0.f)
	{
	}

	_tagVertexColor(const _tagVertexColor& vtx)
	{
		*this = vtx;
	}

	_tagVertexColor(float x, float y, float z, float r, float g, float b,
		float a) :
		vPos(x, y, z),
		vColor(r, g, b, a)
	{
	}

	void operator =(const _tagVertexColor& vtx)
	{
		vPos = vtx.vPos;
		vColor = vtx.vColor;
	}
}VERTEXCOLOR, *PVERTEXCOLOR;

// Tex Vertex
typedef struct DLL _tagVertexTex
{
	DxVector3	vPos;
	DxVector2	vUV;

	_tagVertexTex() :
		vPos(0.f, 0.f, 0.f),
		vUV(0.f, 0.f)
	{
	}

	_tagVertexTex(const _tagVertexTex& vtx)
	{
		*this = vtx;
	}

	_tagVertexTex(float x, float y, float z, float u, float v) :
		vPos(x, y, z),
		vUV(u, v)
	{
	}

	void operator =(const _tagVertexTex& vtx)
	{
		vPos = vtx.vPos;
		vUV = vtx.vUV;
	}
}VERTEXTEX, *PVERTEXTEX;

typedef struct DLL _tagVertexNormalTex
{
	DxVector3	vPos;
	DxVector3	vNormal;
	DxVector2	vUV;

	_tagVertexNormalTex() :
		vPos(0.f, 0.f, 0.f),
		vNormal(0.f, 0.f, 0.f),
		vUV(0.f, 0.f)
	{
	}

	_tagVertexNormalTex(const _tagVertexNormalTex& vtx)
	{
		*this = vtx;
	}

	_tagVertexNormalTex(float x, float y, float z, float nx, float ny,
		float nz, float u, float v) :
		vPos(x, y, z),
		vNormal(nx, ny, nz),
		vUV(u, v)
	{
	}

	void operator =(const _tagVertexNormalTex& vtx)
	{
		vPos = vtx.vPos;
		vNormal = vtx.vNormal;
		vUV = vtx.vUV;
	}
}VERTEXNORMALTEX, *PVERTEXNORMALTEX;

// Position Vertex
typedef struct DLL _tagVertexPos
{
	DxVector3	vPos;

	_tagVertexPos() :
		vPos(0.f, 0.f, 0.f)
	{
	}

	_tagVertexPos(const _tagVertexPos& vtx)
	{
		*this = vtx;
	}

	_tagVertexPos(float x, float y, float z) :
		vPos(x, y, z)
	{
	}

	void operator =(const _tagVertexPos& vtx)
	{
		vPos = vtx.vPos;
	}
}VERTEXPOS, *PVERTEXPOS;


// Normal Vertex
typedef struct DLL _tagVertexNormal
{
	DxVector3	vPos;
	DxVector3	vNormal;
	DxVector2	vUV;

	_tagVertexNormal() :
		vPos(0.f, 0.f, 0.f),
		vNormal(0.f, 0.f, 0.f),
		vUV(0.f, 0.f)
	{
	}

	_tagVertexNormal(const _tagVertexNormal& vtx)
	{
		*this = vtx;
	}

	_tagVertexNormal(float x, float y, float z,
		float nx, float ny, float nz, 
		float u, float v) 
		: vPos(x, y, z),
		vNormal(nx, ny, nz),
		vUV(u, v)
	{
	}

	void operator =(const _tagVertexNormal& vtx)
	{
		vPos = vtx.vPos;
		vNormal = vtx.vNormal;
		vUV = vtx.vUV;
	}
}VERTEXNORMAL, *PVERTEXNORMAL;


// Bump Vertex
typedef struct DLL _tagVertexBump
{
	DxVector3	vPos;
	DxVector3	vNormal;
	DxVector2	vUV;
	DxVector3	vTangent;
	DxVector3	vBinormal;

	_tagVertexBump() :
		vPos(0.f, 0.f, 0.f),
		vNormal(0.f, 0.f, 0.f),
		vUV(0.f, 0.f),
		vTangent(0.f, 0.f, 0.f),
		vBinormal(0.f, 0.f, 0.f)
	{
	}

	_tagVertexBump(const _tagVertexBump& vtx)
	{
		*this = vtx;
	}

	_tagVertexBump(float x, float y, float z,
		float nx, float ny, float nz, float u, float v,
		float tx, float ty, float tz, float bx, float by, float bz) :
		vPos(x, y, z),
		vNormal(nx, ny, nz),
		vUV(u, v),
		vTangent(tx, ty, tz),
		vBinormal(bx, by, bz)
	{
	}

	void operator =(const _tagVertexBump& vtx)
	{
		vPos = vtx.vPos;
		vNormal = vtx.vNormal;
		vUV = vtx.vUV;
		vTangent = vtx.vTangent;
		vBinormal = vtx.vBinormal;
	}
}VERTEXBUMP, *PVERTEXBUMP;


// Animation Bump Vertex
typedef struct DLL _tagVertexAnimBump
{
	DxVector3	vPos;
	DxVector3	vNormal;
	DxVector2	vUV;
	DxVector3	vTangent;
	DxVector3	vBinormal;
	DxVector4	vWeight;
	DxVector4	vIndices;

	_tagVertexAnimBump() :
		vPos(0.f, 0.f, 0.f),
		vNormal(0.f, 0.f, 0.f),
		vUV(0.f, 0.f),
		vTangent(0.f, 0.f, 0.f),
		vBinormal(0.f, 0.f, 0.f),
		vWeight(0.f, 0.f, 0.f, 0.f),
		vIndices(0.f, 0.f, 0.f, 0.f)
	{
	}

	_tagVertexAnimBump(const _tagVertexAnimBump& vtx)
	{
		*this = vtx;
	}

	_tagVertexAnimBump(float x, float y, float z,
		float nx, float ny, float nz, float u, float v,
		float tx, float ty, float tz, float bx, float by, float bz,
		float wx, float wy, float wz, float ww,
		float ix, float iy, float iz, float iw) :
		vPos(x, y, z),
		vNormal(nx, ny, nz),
		vUV(u, v),
		vTangent(tx, ty, tz),
		vBinormal(bx, by, bz),
		vWeight(wx, wy, wz, ww),
		vIndices(ix, iy, iz, iw)
	{
	}

	void operator =(const _tagVertexAnimBump& vtx)
	{
		vPos = vtx.vPos;
		vNormal = vtx.vNormal;
		vUV = vtx.vUV;
		vTangent = vtx.vTangent;
		vBinormal = vtx.vBinormal;
		vWeight = vtx.vWeight;
		vIndices = vtx.vIndices;
	}
}VERTEXANIMBUMP, *PVERTEXANIMBUMP;

// Animation Vertex
typedef struct DLL _tagVertexAnim
{
	DxVector3	vPos;
	DxVector3	vNormal;
	DxVector2	vUV;
	DxVector4	vWeight;
	DxVector4	vIndices;

	_tagVertexAnim() :
		vPos(0.f, 0.f, 0.f),
		vNormal(0.f, 0.f, 0.f),
		vUV(0.f, 0.f),
		vWeight(0.f, 0.f, 0.f, 0.f),
		vIndices(0.f, 0.f, 0.f, 0.f)
	{
	}

	_tagVertexAnim(const _tagVertexAnim& vtx)
	{
		*this = vtx;
	}

	_tagVertexAnim(float x, float y, float z,
		float nx, float ny, float nz, float u, float v,
		float wx, float wy, float wz, float ww,
		float ix, float iy, float iz, float iw) :
		vPos(x, y, z),
		vNormal(nx, ny, nz),
		vUV(u, v),
		vWeight(wx, wy, wz, ww),
		vIndices(ix, iy, iz, iw)
	{
	}

	void operator =(const _tagVertexAnim& vtx)
	{
		vPos = vtx.vPos;
		vNormal = vtx.vNormal;
		vUV = vtx.vUV;
		vWeight = vtx.vWeight;
		vIndices = vtx.vIndices;
	}
}VERTEXANIM, *PVERTEXANIM;

// Particle Vertex
typedef	struct _tagVertexParticle
{
	DxVector3 vPos;
	DxVector3 vVelocity;
	DxVector2 vSize;
	float	fLifeTime;
	float	fCreateTime;
	unsigned int iType;
	float	fLightRange;
}VERTEXPARTICLE, *PVERTEXPARTICLE;

typedef struct DLL __declspec(align(16)) _tagTransformCBuffer
{
	MATRIX			matWorld;
	MATRIX			matView;
	MATRIX			matProj;
	MATRIX			matWV;
	MATRIX			matWVP;
	MATRIX			matVP;
	MATRIX			matInvProj;
	MATRIX			matInvView;
	MATRIX			matInvVP;
	DxVector3		vPivot;
	float			fEmpty;
	DxVector3		vMeshSize;
	float			fEmpty1;
	DxVector3		vMeshMin;
	float			fEmpty2;
	DxVector3		vMeshMax;
	float			fEmpty3;
}TRANSFORMCBUFFER, *PTRANSFORMCBUFFER;


typedef struct DLL _tagConstantBuffer
{
	ID3D11Buffer*	pBuffer;
	int		iSize;
	int		iRegister;
}CONSTANTBUFFER, *PCONSTANTBUFFER;

typedef struct DLL _tagKeyInfo
{
	vector<unsigned int>	vecKey;
	string	strKey;
	bool	bPress;
	bool	bPush;
	bool	bUp;
}KEYINFO, *PKEYINFO;

typedef struct DLL _tagSphereInfo
{
	DxVector3	vCenter;
	float		fRadius;

	_tagSphereInfo() :
		vCenter(0.f, 0.f, 0.f),
		fRadius(0.f)
	{
	}

	_tagSphereInfo(const _tagSphereInfo& sphere)
	{
		*this = sphere;
	}

	_tagSphereInfo(const DxVector3& vC, float fR) :
		vCenter(vC),
		fRadius(fR)
	{
	}

	_tagSphereInfo(float x, float y, float z, float r) :
		vCenter(x, y, z),
		fRadius(r)
	{
	}

	float Distance(const _tagSphereInfo& tSphere)	const
	{
		return vCenter.Distance(tSphere.vCenter);
	}

	float Distance(const _tagSphereInfo* pSphere)	const
	{
		return Distance(*pSphere);
	}
}SPHEREINFO, *PSPHEREINFO;

typedef struct DLL _tagRectInfo
{
	float	fL;
	float	fT;
	float	fR;
	float	fB;

	_tagRectInfo() :
		fL(0.f),
		fT(0.f),
		fR(0.f),
		fB(0.f)
	{
	}

	_tagRectInfo(float l, float t, float r, float b) :
		fL(l),
		fT(t),
		fR(r),
		fB(b)
	{
	}

	_tagRectInfo(const _tagRectInfo& rc)
	{
		fL = rc.fL;
		fT = rc.fT;
		fR = rc.fR;
		fB = rc.fB;
	}

	void MoveRect(float x, float y)
	{
		fL += x;
		fR += x;
		fT += y;
		fB += y;
	}
	_tagRectInfo operator +(const DxVector3& vPos)
	{
		_tagRectInfo	tInfo;
		tInfo.fL = fL + vPos.x;
		tInfo.fR = fR + vPos.x;
		tInfo.fT = fT + vPos.y;
		tInfo.fB = fB + vPos.y;
		return tInfo;
	}
}RECTINFO, *PRECTINFO;


typedef struct DLL _tagAABBboxInfo
{
	DxVector3	vCenter;

	float		fXSize;
	float		fYSize;
	float		fZSize;

	float		fMinX;
	float		fMaxX;

	float		fMinY;
	float		fMaxY;

	float		fMinZ;
	float		fMaxZ;

	_tagAABBboxInfo() :
		vCenter(0.f, 0.f, 0.f),
		fXSize(0.f),
		fYSize(0.f),
		fZSize(0.f)
	{
	}

	_tagAABBboxInfo(const _tagAABBboxInfo& aabb)
	{
		*this = aabb;
	}

	_tagAABBboxInfo(const DxVector3& vC, float fX, float fY, float fZ) :
		vCenter(vC),
		fXSize(fX), 
		fYSize(fY),
		fZSize(fZ)
	{
		fMinX = vCenter.x - fXSize;
		fMaxX = vCenter.x + fXSize;

		fMinY = vCenter.y - fYSize;
		fMaxY = vCenter.y + fYSize;

		fMinZ = vCenter.z - fZSize;
		fMaxZ = vCenter.z + fZSize;

	}

	float Distance(const _tagAABBboxInfo& tRect)	const
	{
		return vCenter.Distance(tRect.vCenter);
	}

	float Distance(const _tagAABBboxInfo* tRect)	const
	{
		return Distance(*tRect);
	}

}AABBBOXINFO, *PAABBBOXINFO;



typedef struct DLL _tagColliderColorCBuffer
{
	DxVector4	vColor;
}COLLIDERCOLORCBUFFER, *PCOLLIDERCOLORCBUFFER;

typedef struct DLL _tagTerrainCBuffer
{
	float		fDetailLevel;
	int			iSplatCount;
	DxVector2	vEmpty;
}TERRAINCBUFFER, *PTERRAINCBUFFER;

typedef struct DLL _tagPickTerrainCBuffer
{
	DxVector3	vPos;
	float		fOuterRadius;
	DxVector3	vEmpty;
	float		fInnerRadius;
}PICKTERRAINCBUFFER, *PICKPTERRAINCBUFFER;

typedef struct DLL _tagRendererCBuffer
{
	int		iRegister;
	int		iSize;
	void*	pData;
	int		iShaderType;
}RENDERERCBUFFER, *PRENDERERCBUFFER;

typedef struct DLL _tagLightInfo
{
	DxVector4	vDif;
	DxVector4	vAmb;
	DxVector4	vSpc;
	LIGHT_TYPE	eType;
}LIGHTINFO, *PLIGHTINFO;

typedef struct DLL _tagLightCBuffer
{
	DxVector4	vDif;
	DxVector4	vAmb;
	DxVector4	vSpc;
	LIGHT_TYPE	eType;
	DxVector3	vDir;
	DxVector3	vPos;
	float		fRange;
}LIGHTCBUFFER, *PLIGHTCBUFFER;

typedef struct DLL _tagMaterialInfo
{
	DxVector4	vDif;
	DxVector4	vAmb;
	DxVector4	vSpc;
	DxVector4	vEmv;
	int			iBump;
	int			iSpecular;
	float		fSpecularPower;
	int			iEmpty;
}MATERIALINFO, *PMATERIALINFO;

typedef struct DLL _tagEffectCBuffer
{
	DxVector3	vCenter;
	float		fEmpty1;
	DxVector3	vCamAxisY;
	float		fEmpty2;
	DxVector3	vCamPos;
	float		fEmpty3;
	DxVector2	vSize;
	DxVector2	vEmpty4;
}EFFECTCBUFFER, *PEFFECTCBUFFER;

// AnimationClip2D
typedef struct DLL _tagAnimationClip2D
{
	string				strName;
	class CTexture*		pTexture;
	int					iTexRegister;
	ANIMATION2D_TYPE	eType;
	ANIMATION_OPTION	eOption;
	int					iFrameX;
	int					iFrameY;
	int					iFrameMaxX;
	int					iFrameMaxY;
	float				fFrameTime;
	float				fLimitTime;
	int					iLoopCount;
	float				fLoopTime;
}ANIMATIONCLIP2D, *PANIMATIONCLIP2D;

// Animation2D CBuffer
typedef struct DLL _tagAnimation2DCBuffer
{
	ANIMATION2D_TYPE	eType;
	int					iFrameX;
	int					iFrameY;
	int					iFrameMaxX;
	int					iFrameMaxY;
	DxVector3			vEmpty;
}ANIMATION2DCBUFFER, *PANIMATION2DCBUFFER;

// Ray
typedef struct DLL _tagRay
{
	DxVector3	vPos;
	DxVector3	vDir;
	DxVector3	vIntersect;
	bool		bIntersect;
}RAY, *PRAY;

typedef struct DLL _tagTerrainCollInfo
{
	vector<DxVector3>	vecPos;
	unsigned int		iNumH;
	unsigned int		iNumW;
	unsigned int		iSizeH;
	unsigned int		iSizeW;
}TERRAINCOLLINFO, *PTERRAINCOLLINFO;

typedef struct DLL _tagParticleCBuffer
{
	DxVector3	vPos;
	float		fDeltaTime;
	DxVector3	vCamAxisX;
	float		fCreateTime;
	DxVector3	vCamAxisY;
	float		fSpeed;
	float		fGameTime;
	DxVector3	vCreateDir;
}PARTICLECBUFFER, *PPARTICLECBUFFER;


typedef struct DLL _tagWaterInfo
{
	DxVector3			vEmpty;
	float				fWaterTime;
}WATERINFO, *PWATERINFO;

typedef struct DLL ObjInfo
{
	string		strTag;
	DxVector3	WorldPos;
	DxVector3	WorldRot;
	DxVector3	WorldScale;
}OBJINFO, *POBJINFO;

//Edit Splatting Image Value
typedef struct DLL splatting_texture
{
	BYTE a;
	BYTE r;
	BYTE g;
	BYTE b;
}SPLAT_TEX, *PSPLAT_TEX;


enum FBX_LOAD_TYPE
{
	FLT_MESH,
	FLT_ANIMATION,
	FLT_BOTH,
	FLT_END
};

typedef struct DLL _tagRenderingCBuffer
{
	int			iCartoonRenderFlag;
	DxVector3	vEdgeColor;
}RENDERINGCBUFFER, *PRENDERINGCBUFFER;

typedef struct DLL tAreaInfo
{
	float fRadius;
	int iLeftBot;
	int iLeftTop;
	int iRightTop;
	int iRightBot;
};

typedef struct _CropsAndLevel
{
	string cropName;
	int		num;

	_CropsAndLevel() :
		num(0)
	{
	}

	_CropsAndLevel( int num, string name) :
		num(num)
	{
		cropName = name;
	}

	_CropsAndLevel(const _CropsAndLevel& t)
	{
		*this = t;
	}

	void operator =(const _CropsAndLevel& t)
	{
		num = t.num;
		cropName = t.cropName;
	}
}CROPANDLEVEL, *PCROPANDLEVEL;

//Lamp Light 버텍스버퍼
typedef	struct _tagVertexLight
{
	DxVector3 vPos;
	float	fLightRange;
}VERTEXLAMPLIGHT, *PVERTEXLAMPLIGHT;

//Lamp Light 상수버퍼
typedef struct DLL _tagLampLightCBuffer
{
	DxVector3	vCamAxisX;
	float		Empty1;
	DxVector3	vCamAxisY;
	float		Empty2;
}LAMPLIGHTCBUFFER, *PLAMPLIGHTCBUFFER;


AR3D_END
