#pragma once

#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <limits.h>

using namespace DirectX;
using namespace DirectX::PackedVector;