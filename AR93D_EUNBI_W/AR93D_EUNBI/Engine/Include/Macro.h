
#pragma once

#ifdef DLL_EXPORT
#define	DLL	__declspec(dllexport)
#else
#define	DLL	__declspec(dllimport)
#endif // DLL_EXPORT

#define	AR3D_BEGIN	namespace AR3D {
#define	AR3D_END	}
#define	AR3D_USING	using namespace AR3D;

#define	SAFE_DELETE(p)	if(p)	{ delete p; p = NULL; }
#define	SAFE_DELETE_ARRAY(p)	if(p)	{ delete[] p; p = NULL; }
#define	SAFE_RELEASE(p)	if(p)	{ p->Release(); p = NULL; }

#define	DECLARE_SINGLE(Type)	\
	private:\
		static Type*	m_pInst;\
	public:\
		static Type* GetInst()\
		{\
			if(!m_pInst)\
				m_pInst	= new Type;\
			return m_pInst;\
		}\
		static void DestroyInst()\
		{\
			SAFE_DELETE(m_pInst);\
		}\
	public:\
		Type();\
		~Type();
#define	DEFINITION_SINGLE(Type)	Type* Type::m_pInst	= NULL;
#define	GET_SINGLE(Type)	Type::GetInst()
#define	DESTROY_SINGLE(Type)	Type::DestroyInst()

#define	_RESOLUTION		CCore::GetInst()->GetResolution()

#define	DEVICE	CDevice::GetInst()->GetDevice()
#define	CONTEXT	CDevice::GetInst()->GetContext()
#define	SWAPCHAIN	CDevice::GetInst()->GetSwapChain()
#define	WINDOWHANDLE	CCore::GetInst()->GetWindowHandle()


#define	KEYPRESS(key)	CInput::GetInst()->KeyPress(key)
#define	KEYPUSH(key)	CInput::GetInst()->KeyPush(key)
#define	KEYUP(key)	CInput::GetInst()->KeyUp(key)
#define   WHEELDIR   CInput::GetInst()->GetWheelDir()
#define   MOUSEPOS   CInput::GetInst()->GetMousePos()
#define   MOUSEMOVE   CInput::GetInst()->GetMouseMove()
#define	D2DTARGET		CDevice::GetInst()->GetD2DRenderTarget()
