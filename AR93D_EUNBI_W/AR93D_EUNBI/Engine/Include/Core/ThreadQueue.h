#pragma once

AR3D_BEGIN

class DLL CThreadSync
{
private:
	friend class CThreadManager;

private:
	static CRITICAL_SECTION m_Crt;

public:
	CThreadSync()
	{
		EnterCriticalSection(&m_Crt); // 자동으로 생성될때 임계영역에 들어가게 만들어줌. 
	}
	~CThreadSync()
	{
		LeaveCriticalSection(&m_Crt); // 소멸될때 자동으로 임계영역에서 나오도록 만들어줌. 
	}

};

template <typename T, int SIZE = 100>
class DLL CThreadQueue
{
public:

	CThreadQueue()
	{
		m_iHead = 0;
		m_iTain = 0;
	}

	~CThreadQueue()
	{
	}

private:
	T m_Data[SIZE];
	int m_iHead;
	int m_iTail;

public:
	void push(const T& data) 
	{
		CThreadSync sync;
		
		int iTail = (m_iTail + 1) % SIZE;

		if (iTail == m_iHead)
			return;

		m_Data[iTail] = data;
		m_iTail = iTail;
	}

	bool pop(T& data)
	{
		CThreadSync sync;

		if (m_iHead == m_iTail)
			return false;
		int iHead = (m_iHead + 1) % SIZE;

		data = m_Data[iHead];
		m_iHead = iHead;
	}
};

AR3D_END