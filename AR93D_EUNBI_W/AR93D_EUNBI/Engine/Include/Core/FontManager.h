#pragma once

#include "../Engine.h"

AR3D_BEGIN

class DLL CFontManager
{
private:
	IDWriteFactory*		m_pWriteFactory;
	unordered_map<string, IDWriteTextFormat*>		m_mapFont;
	unordered_map<string, ID2D1SolidColorBrush*>	m_mapBrush;

public:
	bool Init();
	IDWriteTextFormat* CreateFontFormat(const string& strKey,
		const wchar_t* pFontName, int iWeight, int iStyle,
		int iStretch, float fSize, const wchar_t* pLocalName);
	IDWriteTextFormat* FindFont(const string& strKey);

	ID2D1SolidColorBrush* CreateBrush(const string& strKey,
		float r, float g, float b, float a);
	ID2D1SolidColorBrush* FindBrush(const string& strKey);

	DECLARE_SINGLE(CFontManager)
};

AR3D_END
