#pragma once

#include "../Engine.h"

AR3D_BEGIN

typedef struct _tagScheduler
{
	string strName;
	function<int(float)> func;
	float fTime;
	float fLimitTime;
	void* pObj;
	SCHEDULE_TYPE eType;
}SCHEDULER, *PSCHEDULER;

class DLL CScheduler
{
private:
	list<PSCHEDULER> m_Schedule;

public:
	bool CreateSchedule(const string& strName, int(*pFunc)(float),
		float fLimitTime, SCHEDULE_TYPE eType);
	template <typename T>
	bool CreateSchedule(const string& strName, T* pObj,
		int (T::*pFunc)(float), float fLimitTime, SCHEDULE_TYPE eType)
	{
		PSCHEDULER pSchedule = new SCHEDULER;

		pSchedule->eType = eType;
		pSchedule->fLimitTime = fLimitTime;
		pSchedule->fTime = 0.f;
		pSchedule->pObj = pObj;
		pSchedule->strName = strName;
		pSchedule->func = bind(pFunc, pObj, placeholders::_1);

		m_Schedule.push_back(pSchedule);

		return true;
	}
	
public:
	bool Init();
	int Update(float fTime);

	DECLARE_SINGLE(CScheduler)
};

AR3D_END