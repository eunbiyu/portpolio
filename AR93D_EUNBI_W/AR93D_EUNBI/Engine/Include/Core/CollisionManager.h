#pragma once

#include "../Engine.h"

AR3D_BEGIN

class DLL CCollisionManager
{
private:
	vector<class CGameObject*> m_vecCollision;
	vector<class CGameObject*> m_vecUICollision;

public:
	void AddObject(class CGameObject* pObj);

public:
	bool Init();
	void Collision(float fTime);

private: 
	bool CollisionMouseUI(float fTime);
	bool CollisionMouseObject(float fTime);
	void CollisionObject(class CGameObject* pSrc, class CGameObject* pDest, float fTime);

public:
	static bool CollisionSort(class CGameObject* pObj1, class CGameObject* pObj2);

	DECLARE_SINGLE(CCollisionManager)
};

AR3D_END
