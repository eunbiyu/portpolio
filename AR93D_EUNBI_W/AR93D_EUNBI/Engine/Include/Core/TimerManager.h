#pragma once

#include "../Engine.h"

AR3D_BEGIN

class DLL CTimerManager
{
private:
	unordered_map<string, class CTimer*> m_mapTimer; 

public:
	bool Init();
	class CTimer* CreateTimer(const string& strKey);
	class CTimer* FindTimer(const string& strKey);

	DECLARE_SINGLE(CTimerManager)
};

AR3D_END