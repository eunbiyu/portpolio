#pragma once

#include "../Engine.h"

AR3D_BEGIN

class CGameObject;


class DLL CGameTotalManager
{
private:
	CGameObject*	m_pPickedActor;
	unordered_map<string, class CGameObject*>	m_mapPickedObject;
	DxVector3		m_vPickedPosition;
	vector<DxVector3> m_vLightPos;
	bool			 m_bNight;

public:
	CROPANDLEVEL	cropAndlevel[12];

public:
	void SetPickActor(CGameObject* pPickedActor);
	void ErasePickActor();
	CGameObject* GetPickActor() ;
	void SetPickedPosition(DxVector3 pos);
	DxVector3 GetPickedPosition();
	CROPANDLEVEL GetCropAndNum(int index);
	bool SetCropAndNum(const string& str, int num); 
	bool SetCropAndNum(int index, int num);
	bool SubtracCropAndNum(int index, int num); // �����Լ�
	void SetLightPosition(DxVector3 pos);
	vector<DxVector3> GetLightPosition();


	bool GetNight(void);
	void SetNight(bool bNight);

	bool Init();
	void Update(float deltaTime);


	DECLARE_SINGLE(CGameTotalManager)
};

AR3D_END