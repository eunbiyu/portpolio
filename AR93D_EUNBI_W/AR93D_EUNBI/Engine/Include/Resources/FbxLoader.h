#pragma once

#ifdef _DEBUG
#pragma comment(lib, "libfbxsdk-md_Debug")
#else
#pragma comment(lib, "libfbxsdk-md")
#endif

#include <fbxsdk.h>
#include "../Engine.h"

AR3D_BEGIN

// Key Frame
typedef	struct DLL _tagFbxKeyFrame
{
	FbxAMatrix	matTransform;
	double		dTime;
}FBXKEYFRAME, *PFBXKEYFRAME;

// Bone Key Frame
typedef	struct DLL _tagFbxBoneKeyFrame
{
	int		iBoneIndex;
	vector<FBXKEYFRAME>	vecKeyFrame;
}FBXBONEKEYFRAME, *PFBXBONEKEYFRAME;

// Fbx Animation Clip
typedef struct DLL _tagFbxAnimationClip
{
	string			strName;
	FbxTime			tStart;
	FbxTime			tEnd;
	FbxLongLong		lTimeLength;
	FbxTime::EMode	eTimeMode;
	vector<FBXBONEKEYFRAME>	vecBoneKeyFrame;
}FBXANIMATIONCLIP, *PFBXANIMATIONCLIP;

// Bone
typedef struct DLL _tagFbxBone
{
	string	strName;
	int		iDepth;
	int		iParentIndex;
	FbxAMatrix	matOffset;
	FbxAMatrix	matBone;
	//vector<FBXKEYFRAME>	vecKeyFrame;
}FBXBONE, *PFBXBONE;

typedef struct DLL _tagFbxMaterial
{
	DxVector4	vDif;
	DxVector4	vAmb;
	DxVector4	vSpc;
	DxVector4	vEmv;
	float		fSpecularPower;
	float		fTransparencyFactor;
	float		fShininess;
	string		strDifTex;
	string		strBumpTex;
	string		strSpcTex;
}FBXMATERIAL, *PFBXMATERIAL;

typedef struct DLL _tagFbxWeight
{
	int		iIndex;
	double	dWeight;
}FBXWEIGHT, *PFBXWEIGHT;

typedef struct DLL _tagFBXMeshContainer
{
	vector<DxVector3>	vecPos;
	vector<DxVector3>	vecNormal;
	vector<DxVector2>	vecUV;
	vector<DxVector3>	vecTangent;
	vector<DxVector3>	vecBinormal;
	vector<DxVector4>	vecBlendWeight;
	vector<DxVector4>	vecBlendIndex;
	vector<vector<UINT>>	vecIndices;
	unordered_map<int, vector<FBXWEIGHT>>	mapWeights;
	bool				bBump;
	bool				bAnimation;
}FBXMESHCONTAINER, *PFBXMESHCONTAINER;

class DLL CFbxLoader
{
private:
	friend class CMesh;
	friend class CAnimation3D;

private:
	CFbxLoader();
	~CFbxLoader();

private:
	FbxManager*		m_pManager;
	FbxScene*		m_pScene;
	FbxImporter*	m_pImporter;
	vector<PFBXMESHCONTAINER>	m_vecMeshContainer;
	vector<vector<PFBXMATERIAL>>	m_vecMaterial;
	vector<PFBXBONE>			m_vecBones;
	vector<PFBXANIMATIONCLIP>	m_vecClip;
	FbxArray<FbxString*>		m_NameArr;
	bool		m_bMixamo;
	FBX_LOAD_TYPE	m_eLoadType;

public:
	const vector<PFBXBONE>* GetBones()	const;
	const vector<PFBXANIMATIONCLIP>* GetClip()	const;
	const vector<PFBXMESHCONTAINER>* GetMeshContainer()	const;
	const vector<vector<PFBXMATERIAL>>* GetMaterials()	const;

public:
	bool LoadFBX(const TCHAR* pFileName, FBX_LOAD_TYPE eLoadType, const string& strPathKey = MESH_PATH);
	bool LoadFBX(const char* pFileName, FBX_LOAD_TYPE eLoadType, const string& strPathKey = MESH_PATH);
	bool LoadFBXFullPath(const TCHAR* pFullPath, FBX_LOAD_TYPE eLoadType);
	bool LoadFBXFullPath(const char* pFullPath, FBX_LOAD_TYPE eLoadType);

private:
	void Triangulate(FbxNode* pNode);
	bool LoadMesh(FbxNode* pNode);
	bool LoadMesh(FbxMesh* pMesh);
	void LoadNormal(FbxMesh* pMesh, PFBXMESHCONTAINER pContainer,
		int iVtxID, int iControlIndex);
	void LoadUV(FbxMesh* pMesh, PFBXMESHCONTAINER pContainer,
		int iUVID, int iControlIndex);
	void LoadTangent(FbxMesh* pMesh, PFBXMESHCONTAINER pContainer,
		int iVtxID, int iControlIndex);
	void LoadBinormal(FbxMesh* pMesh, PFBXMESHCONTAINER pContainer,
		int iVtxID, int iControlIndex);
	void LoadMaterial(FbxSurfaceMaterial* pMtrl);
	DxVector4 GetMaterialColor(FbxSurfaceMaterial* pMtrl,
		const char* pPropertyName, const char* pPropertyFactorName);
	double GetMaterialFactor(FbxSurfaceMaterial* pMtrl,
		const char* pPropertyName);
	string GetMaterialTexture(FbxSurfaceMaterial* pMtrl,
		const char* pPropertyName);

private:	// Animation
	void LoadSkeleton(FbxNode* pNode);
	void LoadSkeletonRecursive(FbxNode* pNode, int iDepth,
		int iIndex, int iParentIndex);
	void LoadAnimationClip();
	void LoadAnimation(FbxMesh* pMesh, PFBXMESHCONTAINER pContainer);
	void LoadWeightAndIndex(FbxCluster* pCluster, int iBoneIndex, PFBXMESHCONTAINER pContainer);
	void LoadOffsetMatrix(FbxCluster* pCluster, const FbxAMatrix& matTransform,
		int iBoneIndex, PFBXMESHCONTAINER pContainer);
	void LoadTimeTransform(FbxNode* pNode, FbxCluster* pCluster,
		const FbxAMatrix& matTransform, int iBoneIndex);
	void ChangeWeightAndIndices(PFBXMESHCONTAINER pContainer);
	int FindBoneFromName(const string& strName);
	FbxAMatrix GetTransform(FbxNode* pNode);
};

AR3D_END
