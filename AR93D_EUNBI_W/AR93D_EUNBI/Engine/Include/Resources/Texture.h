#pragma once
#include "../BaseObj.h"
#include "DirectXTex.h"

AR3D_BEGIN



class DLL CTexture :
	public CBaseObj
{ 
private:
	friend class CResourcesManager;
	friend class CAR3DRenderTarget;

private:
	CTexture();
	~CTexture();

public:
	static CTexture* CreateTexture(const string& strKey, unsigned int iWidth,
		unsigned int iHeight, unsigned int iArrSize,
		DXGI_FORMAT eFmt, D3D11_USAGE eUsage, int iBindFlag,
		int iCpuFlag);

private:
	vector<::DirectX::ScratchImage*>	m_vecScratchImage;
	ID3D11ShaderResourceView*			m_pSRView;
	ID3D11Texture2D*					m_pTexture;
	string								m_strKey;
	string								m_strPathKey;
	wstring								m_strFullPath;
	wstring								m_strFileName;/*
	PSPLAT_TEX							m_splattingPxl;
	int									m_splattingImgSize;*/

public:
	ID3D11Texture2D* GetTexture()	const;
	string GetKey()	const;
	string GetPathKey()	const;
	wstring GetFullPath()	const;
	wstring GetFileName()	const;

public:
	void SetPathKey(const string& strPathKey);
	void Save(FILE* pFile);
	void Load(FILE* pFile);

public:
	bool LoadTexture(const string& strKey, TCHAR* pFileName, const string& strPathKey = TEXTURE_PATH);
	bool LoadTexture(const string& strKey, char* pFileName, const string& strPathKey = TEXTURE_PATH);
	bool LoadTexture(const string& strKey);
	bool LoadTextureFromFullPath(const string& strKey, const char* pFullPath);
	bool LoadTexture(const string& strKey, const vector<wstring>& vecFileName,
		const string& strPathKey = TEXTURE_PATH);
	bool LoadTextureFromMultibyte(const string& strKey, const vector<string>& vecFileName,
		const string& strPathKey = TEXTURE_PATH);
	bool LoadTextureFromFullPath(const string& strKey, const vector<string>& vecFullPath);
	bool CreateResource(const string& strKey, unsigned int iWidth,
		unsigned int iHeight, unsigned int iArrSize,
		DXGI_FORMAT eFmt, D3D11_USAGE eUsage, int iBindFlag,
		int iCpuFlag);
	void UpdateData(void* pData, int iSize);
	void SetTexture(int iRegister, int iShaderType);
	void SaveTextureFile(const wchar_t* pFileName, const string& strPathKey = TEXTURE_PATH);
	//void SaveTextureFileTerrain(const wchar_t* pFileName, bool breset, const string& strPathKey = TEXTURE_PATH);

	//void EditTerrainSplattingTexture(const wchar_t* pFileName, const string & strPathKey = TEXTURE_PATH);

};

AR3D_END
