#pragma once

#include "../Engine.h"

AR3D_BEGIN

class DLL CResourcesManager
{
private:
	unordered_map<string, class CMesh*>		m_mapMesh;
	unordered_map<string, class CTexture*>	m_mapTexture;
	unordered_map<string, class CSampler*>	m_mapSampler;

public:
	bool Init();
	class CMesh* CreateMesh(const string& strKey, void* pVertices, unsigned int iVtxCount,
		unsigned int iVtxSize, D3D11_USAGE eVtxUsage,
		D3D11_PRIMITIVE_TOPOLOGY ePrimitive, void* pIndices = NULL,
		unsigned int iIdxCount = 0, unsigned int iIdxSize = 0,
		D3D11_USAGE eIdxUsage = D3D11_USAGE_DEFAULT,
		DXGI_FORMAT eFormat = DXGI_FORMAT_R32_UINT);
	class CMesh* CreateSphere(const string& strKey, float fRadius,
		UINT iNumSub, const DxVector4& vColor);
	class CMesh* CreateSphere(const string& strKey, float fRadius,
		UINT iNumSub); 
	class CMesh* CreateCube(const string& strKey, float fSize);
	class CMesh* LoadMesh(const string& strKey, const wchar_t* pFileName,
		FBX_LOAD_TYPE eLoadType = FLT_BOTH, const string& strPathKey = MESH_PATH);
	class CMesh* LoadMesh(const string& strKey, const char* pFileName,
		FBX_LOAD_TYPE eLoadType = FLT_BOTH, const string& strPathKey = MESH_PATH);
	class CMesh* LoadMeshFromFullPath(const string& strKey, const wchar_t* pFullPath,
		FBX_LOAD_TYPE eLoadType = FLT_BOTH);
	class CMesh* LoadMeshFromFullPath(const string& strKey, const char* pFullPath,
		FBX_LOAD_TYPE eLoadType = FLT_BOTH);
	bool EraseMesh(class CMesh* pMesh);

	class CMesh* FindMesh(const string& strKey);

	class CTexture* LoadTexture(const string& strKey, TCHAR* pFileName,
		const string& strPathKey = TEXTURE_PATH);
	class CTexture*  LoadTexture(const string& strKey, char* pFileName, const string& strPathKey = TEXTURE_PATH);
	class CTexture*  LoadTextureFromFullPath(const string& strKey, const char* pFullPath);
	class CTexture*  LoadTexture(const string& strKey, const vector<wstring>& vecFileName,
		const string& strPathKey = TEXTURE_PATH);
	class CTexture*  LoadTextureFromMultibyte(const string& strKey, const vector<string>& vecFileName,
		const string& strPathKey = TEXTURE_PATH);
	class CTexture*  LoadTextureFromFullPath(const string& strKey, const vector<string>& vecFullPath);
	class CTexture* CreateTexture(const string& strKey, unsigned int iWidth,
		unsigned int iHeight, unsigned int iArrSize,
		DXGI_FORMAT eFmt, D3D11_USAGE eUsage, D3D11_BIND_FLAG eBindFlag,
		int iCpuFlag);
	class CTexture* FindTexture(const string& strKey);
	class CTexture*  ReloadTexture(const string& strKey, const vector<wstring>& vecFileName,
		const string& strPathKey = TEXTURE_PATH);


	class CSampler* CreateSampler(const string& strKey, D3D11_FILTER eFilter = D3D11_FILTER_MIN_MAG_MIP_LINEAR,
		D3D11_TEXTURE_ADDRESS_MODE eAddrU = D3D11_TEXTURE_ADDRESS_WRAP,
		D3D11_TEXTURE_ADDRESS_MODE eAddrV = D3D11_TEXTURE_ADDRESS_WRAP,
		D3D11_TEXTURE_ADDRESS_MODE eAddrW = D3D11_TEXTURE_ADDRESS_WRAP);
	class CSampler* FindSampler(const string& strKey);

	DECLARE_SINGLE(CResourcesManager)
};

AR3D_END
