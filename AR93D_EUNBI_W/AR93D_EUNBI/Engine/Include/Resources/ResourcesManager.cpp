#include "ResourcesManager.h"
#include "Mesh.h"

AR3D_USING

DEFINITION_SINGLE(CResourcesManager)

CResourcesManager::CResourcesManager()
{
}

CResourcesManager::~CResourcesManager()
{
	Safe_Release_Map(m_mapMesh);
}

bool CResourcesManager::Init()
{
	// ���� Color Triangle
	VERTEXCOLOR	tVtx[3] =
	{
		VERTEXCOLOR(0.f, 1.f, 0.f, 1.f, 0.f, 0.f, 1.f),
		VERTEXCOLOR(1.f, -1.f, 0.f, 0.f, 1.f, 0.f, 1.f),
		VERTEXCOLOR(-1.f, -1.f, 0.f, 0.f, 0.f, 1.f, 1.f)
	};

	UINT	iIdx[3] = { 0, 1, 2 };

	CMesh*	pMesh = CreateMesh("ColorTriangle", tVtx, 3, sizeof(VERTEXCOLOR),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		iIdx, 3, 4);

	SAFE_RELEASE(pMesh);

	VERTEXCOLOR	tVtxPyramid[5] =
	{
		VERTEXCOLOR(0.f, 0.5f, 0.f, 1.f, 0.f, 0.f, 1.f),
		VERTEXCOLOR(-0.5f, -0.5f, 0.5f, 0.f, 1.f, 0.f, 1.f),
		VERTEXCOLOR(0.5f, -0.5f, 0.5f, 0.f, 0.f, 1.f, 1.f),
		VERTEXCOLOR(-0.5f, -0.5f, -0.5f, 1.f, 0.f, 1.f, 1.f),
		VERTEXCOLOR(0.5f, -0.5f, -0.5f, 0.f, 1.f, 1.f, 1.f)
	};

	UINT	iPyramidIdx[18]	=
	{
		0, 4, 3, 0, 2, 4, 0, 1, 2, 0, 3, 1, 3, 4, 1, 4, 2, 1
	};

	pMesh = CreateMesh("ColorPyramid", tVtxPyramid, 5, sizeof(VERTEXCOLOR),
		D3D11_USAGE_DEFAULT, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		iPyramidIdx, 18, 4);

	SAFE_RELEASE(pMesh);

	pMesh = CreateSphere("ColorSphere", 0.5f, 2, DxVector4(0.f, 1.f, 0.f, 1.f));

	SAFE_RELEASE(pMesh);

	return true;
}

CMesh * CResourcesManager::CreateMesh(const string & strKey, void * pVertices,
	unsigned int iVtxCount, unsigned int iVtxSize, D3D11_USAGE eVtxUsage,
	D3D11_PRIMITIVE_TOPOLOGY ePrimitive, void * pIndices, 
	unsigned int iIdxCount, unsigned int iIdxSize, D3D11_USAGE eIdxUsage,
	DXGI_FORMAT eFormat)
{
	CMesh*	pMesh = FindMesh(strKey);

	if (pMesh)
		return pMesh;

	pMesh = new CMesh;

	if (!pMesh->CreateMesh(strKey, pVertices, iVtxCount, iVtxSize, eVtxUsage,
		ePrimitive, pIndices, iIdxCount, iIdxSize, eIdxUsage, eFormat))
	{
		SAFE_RELEASE(pMesh);
		return NULL;
	}

	pMesh->AddRef();

	m_mapMesh.insert(make_pair(strKey, pMesh));

	return pMesh;
}

CMesh * CResourcesManager::CreateSphere(const string & strKey, float fRadius, 
	UINT iNumSub, const DxVector4& vColor)
{
	CMesh*	pMesh = FindMesh(strKey);

	if (pMesh)
		return pMesh;

	pMesh = new CMesh;

	if (!pMesh->CreateSphere(strKey, fRadius, iNumSub, vColor))
	{
		SAFE_RELEASE(pMesh);
		return NULL;
	}

	pMesh->AddRef();

	m_mapMesh.insert(make_pair(strKey, pMesh));

	return pMesh;
}

CMesh * CResourcesManager::FindMesh(const string & strKey)
{
	unordered_map<string, class CMesh*>::iterator	iter = m_mapMesh.find(strKey);

	if (iter == m_mapMesh.end())
		return NULL;

	iter->second->AddRef();

	return iter->second;
}
