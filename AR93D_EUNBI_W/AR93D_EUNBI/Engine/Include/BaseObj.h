#pragma once

#include "Engine.h"

AR3D_BEGIN

class DLL CBaseObj
{
protected:
	CBaseObj();
	virtual ~CBaseObj() = 0;

protected:
	int			m_iRefCount;
	const type_info*	m_pTypeID;
	string		m_strTag;
	string		m_strTypeName;
	bool		m_bEnable;
	bool		m_bAlive;

public:
	int AddRef();
	int Release();
	int Remove();

public:
	template <typename T>
	void SetTypeID()
	{
		m_pTypeID = &typeid(T);
	}

	void SetTag(const string& strTag);
	void SetTypeName(const string& strTypeName);
	void SetEnable(bool bEnable);
	void Death();

public:
	const type_info* GetTypeID()	const;
	string GetTag()	const;
	string GetTypeName()	const;
	bool GetEnable()	const;
	bool GetAlive()	const;
};

AR3D_END
