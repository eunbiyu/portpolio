#pragma once
#include "../BaseObj.h"

AR3D_BEGIN

class DLL CGameObject :
	public CBaseObj
{
private:
	CGameObject();
	CGameObject(const CGameObject& obj);
	~CGameObject();

private:
	static unordered_map<string, CGameObject*>	m_mapPrototype;

public:
	static CGameObject* Create(const string& strTag, bool bPrototype = false);
	static CGameObject* CreateClone(const string& strKey);
	static void DestroyPrototype();

private:
	class CScene*	m_pScene;
	class CLayer*	m_pLayer;
	class CTransform*	m_pTransform;
	list<class CComponent*>	m_ComList;
	list<class CComponent*>	m_FindComList;
	list<CGameObject*>	m_ChildList;
	CGameObject*		m_pParent;

public:
	CTransform* GetTransform();
	CGameObject* GetParent();
	CTransform* GetParentTransform();

public:
	void SetParent(CGameObject* pParent);
	void SetScene(class CScene* pScene);
	void SetLayer(class CLayer* pLayer);
	void ChildTransformActiveUpdate();

public:
	void AddChild(CGameObject* pChild);

public:
	list<CGameObject*>* GetChildList();

public:
	void AddComponent(class CComponent* pCom);

	template <typename T>
	T* AddComponent(const string& strTag)
	{
		T*	pCom = new T;

		pCom->SetScene(m_pScene);
		pCom->SetLayer(m_pLayer);
		pCom->SetGameObject(this);
		pCom->SetTransform(m_pTransform);

		if (!pCom->Init())
		{
			SAFE_RELEASE(pCom);
			return NULL;
		}

		pCom->SetTag(strTag);

		pCom->AddRef();
		m_ComList.push_back(pCom);

		return pCom;
	}

public:
	class CComponent* FindComponentFromTag(const string& strTag);
	class CComponent* FindComponentFromTypeName(const string& strTypeName);
	class CComponent* FindComponentFromType(COMPONENT_TYPE eType);
	bool CheckComponentFromTag(const string& strTag);
	bool CheckComponentFromTypeName(const string& strTypeName);
	bool CheckComponentFromType(COMPONENT_TYPE eType);
	template <typename T>
	bool CheckComponentFromTypeID()
	{
		list<class CComponent*>::iterator	iter;
		list<class CComponent*>::iterator	iterEnd = m_ComList.end();

		for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
		{
			if ((*iter)->GetTypeID() == &typeid(T))
			{
				return true;
			}
		}

		return false;
	}

	list<class CComponent*>* FindComponentsFromTag(const string& strTag);
	list<class CComponent*>* FindComponentsFromTypeName(const string& strTypeName);
	list<class CComponent*>* FindComponentsFromType(COMPONENT_TYPE eType);

	template <typename T>
	T* FindComponentFromTypeID()
	{
		list<class CComponent*>::iterator	iter;
		list<class CComponent*>::iterator	iterEnd = m_ComList.end();

		for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
		{
			if ((*iter)->GetTypeID() == &typeid(T))
			{
				(*iter)->AddRef();
				return (T*)*iter;
			}
		}

		return NULL;
	}

	template <typename T>
	list<class CComponent*>* FindComponentsFromTypeID()
	{
		Safe_Release_VecList(m_FindComList);
		list<class CComponent*>::iterator	iter;
		list<class CComponent*>::iterator	iterEnd = m_ComList.end();

		for (iter = m_ComList.begin(); iter != iterEnd; ++iter)
		{
			if (*(*iter)->GetTypeID() == typeid(T))
			{
				(*iter)->AddRef();
				m_FindComList.push_back(*iter);
			}
		}

		return &m_FindComList;
	}

public:
	bool Init();
	void Input(float fTime);
	void Update(float fTime);
	void LateUpdate(float fTime);
	void Collision(float fTime);
	void Render(float fTime);
	CGameObject* Clone();
	void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);

private:
	bool				m_bPicked;

public:
	bool GetPicked();
	void SetPicked(bool bpicked);
	void EraseComponentFromTag(const string& strTypeName);

};

AR3D_END
