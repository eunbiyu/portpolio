#pragma once

#include "../Engine.h"

AR3D_BEGIN

class DLL CSceneManager
{
public:
	class CScene*	m_pCurrentScene;
	class CScene*	m_pNextScene;
	class CScene*	m_pPrevScene;


public:
	class CScene* GetCurrentScene();

public:
	bool Init();
	int Input(float fTime);
	int Update(float fTime);
	int LateUpdate(float fTime);
	void Collision(float fTime);
	void Render(float fTime);
	class CScene* CreateScene(const string& strTag);
	int ChangeScene();
	void ReplaceScene(class CScene* pScene);
	void PushScene(class CScene* pScene);
	void PrevScene();

public:
	void ChangeScene(CScene* Scene);

	DECLARE_SINGLE(CSceneManager)
};


AR3D_END