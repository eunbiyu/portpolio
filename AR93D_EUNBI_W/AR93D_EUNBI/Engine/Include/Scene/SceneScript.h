#pragma once

#include "../Engine.h"

AR3D_BEGIN

class DLL CSceneScript
{
protected:
	friend class CScene;

protected:
	CSceneScript();
	virtual ~CSceneScript() = 0;

protected:
	class CScene* m_pScene;

public:
	virtual bool Init() = 0;
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
};

AR3D_END
