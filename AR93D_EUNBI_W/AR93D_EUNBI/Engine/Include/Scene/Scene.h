#pragma once

#include "../Engine.h"
#include "../Component/Terrain.h"
AR3D_BEGIN

class DLL CScene
{
private:
	friend class CSceneManager;

private:
	CScene();
	~CScene();

private:
	string	m_strTag;
	vector<class CLayer*>	m_vecLayer;
	vector<class CSceneScript*>	m_vecSceneScript;
	unordered_map<string, class CGameObject*> m_mapCamera;
	class CGameObject* m_pCameraObj;
	class CCamera* m_pCamera;
	class CFrustum* m_pFrustum;
	class CGameObject* m_GlobalLight;
	list <class CGameObject*> m_LightList;
	class CGameObject*	m_pEnvironment;
	bool		m_bNight;
	bool		m_bDay;

	//Tool
	int				m_iDialogType;
	TERRAINCOLLINFO		m_pTerrainInfo;
	CGameObject*			m_pTerrainObject;
	CTerrain*			m_pTerrain;

public:
	static bool LayerZSort(class CLayer* p1, class CLayer* p2);

public:
	class CGameObject* GetMainCameraObj()	const;
	class CCamera* GetMainCamera()	const;
	class CTransform* GetMainCameraTransform()	const;
	class CFrustum* GetMainCameraFrustum() const;
	class CGameObject* FindCameraObj(const string& strKey)	const;
	class CCamera* FindCamera(const string& strKey)	const;
	class CFrustum* FindFrustum(const string& strKey)	const;
	class CTransform* FindCameraTransform(const string& strKey)	const;
	const list<class CGameObject*>* GetLightList()	const;

	

public:
	void SetTag(const string& strTag);
	string GetTag() const;
	class CLayer* CreateLayer(const string& strTag, int iZOrder = 0);
	template <typename T>
	T* CreateScript()
	{
		T*	pScript = new T;

		pScript->m_pScene = this;

		if (!pScript->Init())
		{
			SAFE_DELETE(pScript);
			return NULL;
		}

		m_vecSceneScript.push_back(pScript);

		return pScript;
	}

	class CGameObject* CreateCamera(const string& strKey, const DxVector3& vPos = Vec3Zero);
	bool CreateProjection(const string& strKey, float fAngle, float fWidth,
		float fHeight, float fNear = 0.3f, float fFar = 1000.f);
	bool CreateOrthoProjection(const string& strKey, float fWidth, float fHeight,
		float fNear = 0.f, float fFar = 1000.f);
	bool CreateProjection(float fAngle, float fWidth,
		float fHeight, float fNear = 0.3f, float fFar = 1000.f);
	bool CreateOrthoProjection(float fWidth, float fHeight,
		float fNear = 0.f, float fFar = 1000.f);
	void ChangeCamera(const string& strKey);

	class CGameObject* CreateLight(const string& strTag, LIGHT_TYPE eType);
	class CGameObject* FindLight(const string& strTag);

public:
	bool Init();
	void Input(float fTime);
	void Update(float fTime);
	void LateUpdate(float fTime);
	void Collision(float fTime);
	void Render(float fTime);
	void SetDialogType(int iDialogType);
	int GetDialogType(void);
	void SetTerrainColInfo(TERRAINCOLLINFO pTerrainInfo);
	TERRAINCOLLINFO  GetTerrainColInfo(void);
	void SetTerrain(CGameObject* pTerrain);
	class CGameObject* GetTerrainObject();
	class CTerrain* GetTerrain();

public:
	class CLayer* FindLayer(const string& strTag);
	virtual void MakeEffect(const DxVector3& Pos) {};

};

AR3D_END
