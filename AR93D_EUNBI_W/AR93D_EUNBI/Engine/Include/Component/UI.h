#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CUI :
	public CComponent
{
protected:
	friend class CGameObject;

protected:
	CUI();
	CUI(const CUI& ui);
	~CUI();

protected:
	UI_TYPE		m_eUIType;

public:
	UI_TYPE GetUIType()	const;

public:
	virtual bool Init() = 0;
	virtual void Input(float fTime) = 0;
	virtual void Update(float fTime) = 0;
	virtual void LateUpdate(float fTime) = 0;
	virtual void Collision(float fTime) = 0;
	virtual void Render(float fTime) = 0;
	virtual CUI* Clone() = 0;
	virtual void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
};

AR3D_END
