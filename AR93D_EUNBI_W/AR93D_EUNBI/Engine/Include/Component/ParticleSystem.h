#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CParticleSystem :
	public CComponent
{
private:
	friend class CGameObject;

private:
	CParticleSystem();
	CParticleSystem(const CParticleSystem& particle);
	~CParticleSystem();

private:
	ID3D11Buffer*	m_pInitVB;
	ID3D11Buffer*	m_pStreamVB;
	ID3D11Buffer*	m_pDrawVB;
	int				m_iMaxParticles;
	bool			m_bFirstRun;
	class CShader*	m_pStreamShader;
	class CShader*	m_pShader;
	class CShader*	m_pLightShader;
	ID3D11InputLayout*	m_pInputLayout;
	class CTexture*	m_pTexture;
	class CRenderState*	m_pDepthDisable;
	class CRenderState*	m_pAlphaBlend;
	class CRenderState*	m_pDepthWriteDisable;
	float			m_fParticleTime;
	bool			m_bLight;
	float			m_fLightRange;
	float			m_fTotalTime;

public:
	bool GetParticleLight()	const;

public:
	void SetLightRange(float fRange);
	void SetShader(const string& strKey);
	void SetStreamShader(const string& strKey);
	void SetInputLayout(const string& strKey);
	void SetParticleInfo(int iMaxParticles = MAX_PARTICLES);
	void SetParticleTexture(const string& strKey, TCHAR* pFileName,
		const string& strPathKey = TEXTURE_PATH);
	void SetParticleLight(bool bParticleLight);
	float GetParticleTime();


public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CParticleSystem* Clone();

public:
	void RenderLight(float fTime);
};

AR3D_END
