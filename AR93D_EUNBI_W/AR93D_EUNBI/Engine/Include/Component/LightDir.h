#pragma once
#include "Light.h"

AR3D_BEGIN

class DLL CLightDir :
	public CLight
{
private:
	friend class CGameObject;
	AXIS m_axis;

public:
	CLightDir();
	CLightDir(const CLightDir& light);
	~CLightDir();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CLightDir* Clone();
	virtual void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);

public:
	virtual void SetLight();
	virtual void SetLightDir(AXIS axis);
};

AR3D_END

