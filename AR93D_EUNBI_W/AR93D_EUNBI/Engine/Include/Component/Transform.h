#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CTransform :
	public CComponent
{
private:
	friend class CGameObject;

private:
	CTransform();
	CTransform(const CTransform& transform);
	~CTransform();

private:
	DxVector3		m_vLocalScale;
	DxVector3		m_vLocalRot;
	DxVector3		m_vLocalPos;
	DxVector3		m_vLocalAxis[AXIS_MAX];

	DxVector3		m_vPivot;

	DxVector3		m_vWorldScale;
	DxVector3		m_vWorldRot;
	DxVector3		m_vWorldPos;
	DxVector3		m_vWorldAxis[AXIS_MAX];

	bool			m_bUpdate;
	bool			m_bMove;
	CTransform*		m_pParent;

public:
	bool GetMove()	const;
	DxVector3 GetLocalScale()	const;
	DxVector3 GetLocalRot()	const;
	DxVector3 GetLocalPos()	const;

	DxVector3 GetPivot()	const;

	DxVector3 GetWorldScale()	const;
	DxVector3 GetWorldRot()	const;
	DxVector3 GetWorldPos()	const;
	DxVector3 GetWorldHierarchyRot()	const;
	CTransform* GetParentTransform()	const;
	DxVector3 GetWorldAxis(AXIS axis)	const;

public:
	void SetParentTransform(CTransform* pParent);
	void ActiveUpdate();
	void ComputeWorldAxis();

public:
	// ============= Local Scale ==============
	void SetLocalScale(const Vector3& v);
	void SetLocalScale(const DxVector3& v);
	void SetLocalScale(const XMVECTOR& v);
	void SetLocalScale(float x, float y, float z);
	void SetLocalScale(float f[3]);
	void SetLocalScaleX(float x);
	void SetLocalScaleY(float y);
	void SetLocalScaleZ(float z);

	// ============= Local Rot ==============
	void SetLocalRot(const Vector3& v);
	void SetLocalRot(const DxVector3& v);
	void SetLocalRot(const XMVECTOR& v);
	void SetLocalRot(float x, float y, float z);
	void SetLocalRot(float f[3]);
	void SetLocalRotX(float x);
	void SetLocalRotY(float y);
	void SetLocalRotZ(float z);

	// ============= Local Pos ==============
	void SetLocalPos(const Vector3& v);
	void SetLocalPos(const DxVector3& v);
	void SetLocalPos(const XMVECTOR& v);
	void SetLocalPos(float x, float y, float z);
	void SetLocalPos(float f[3]);
	void SetLocalPosX(float x);
	void SetLocalPosY(float y);
	void SetLocalPosZ(float z);

	void SetPivot(const DxVector3& vPivot);
	void SetPivot(float x, float y, float z);

	// ============= World Scale ==============
	void SetWorldScale(const Vector3& v);
	void SetWorldScale(const DxVector3& v);
	void SetWorldScale(const XMVECTOR& v);
	void SetWorldScale(float x, float y, float z);
	void SetWorldScale(float f[3]);
	void SetWorldScaleX(float x);
	void SetWorldScaleY(float y);
	void SetWorldScaleZ(float z);

	// ============= World Rot ==============
	void SetWorldRot(const Vector3& v);
	void SetWorldRot(const DxVector3& v);
	void SetWorldRot(const XMVECTOR& v);
	void SetWorldRot(float x, float y, float z);
	void SetWorldRot(float f[3]);
	void SetWorldRotX(float x);
	void SetWorldRotY(float y);
	void SetWorldRotZ(float z);

	// ============= World Pos ==============
	void SetWorldPos(const Vector3& v);
	void SetWorldPos(const DxVector3& v);
	void SetWorldPos(const XMVECTOR& v);
	void SetWorldPos(float x, float y, float z);
	void SetWorldPos(float f[3]);
	void SetWorldPosX(float x);
	void SetWorldPosY(float y);
	void SetWorldPosZ(float z);

private:
	PMATRIX		m_matLocalScale;
	PMATRIX		m_matLocalRotX;
	PMATRIX		m_matLocalRotY;
	PMATRIX		m_matLocalRotZ;
	PMATRIX		m_matLocalRot;
	PMATRIX		m_matLocalPos;
	PMATRIX		m_matLocal;

	PMATRIX		m_matWorldScale;
	PMATRIX		m_matWorldRotX;
	PMATRIX		m_matWorldRotY;
	PMATRIX		m_matWorldRotZ;
	PMATRIX		m_matWorldRot;
	PMATRIX		m_matWorldPos;
	PMATRIX		m_matParent;
	PMATRIX		m_matWorld;

public:
	MATRIX GetLocalMatrix()	const;
	MATRIX GetWorldRotMatrix()	const;
	MATRIX GetWorldMatrix()	const;
	MATRIX GetParentMatrix()	const;
	MATRIX GetParentRotMatrix()	const;

public:
	void Forward(float fSpeed, float fTime);
	void Forward(float fSpeed);
	void Right(float fSpeed, float fTime);
	void Right(float fSpeed);
	void Up(float fSpeed, float fTime);
	void Up(float fSpeed);
	void Move(const DxVector3& vDir, float fSpeed);
	void Move(const DxVector3& vDir, float fSpeed, float fTime);
	void Move(const DxVector3& vMove);
	void Rotate(const DxVector3& vRot, bool bLocal = false);
	void Rotate(float x, float y, float z, bool bLocal = false);
	void RotateX(float x, bool bLocal = false);
	void RotateY(float y, bool bLocal = false);
	void RotateZ(float z, bool bLocal = false);
	void Rotate(const DxVector3& vRot, float fTime, bool bLocal = false);
	void Rotate(float x, float y, float z, float fTime, bool bLocal = false);
	void RotateX(float x, float fTime, bool bLocal = false);
	void RotateY(float y, float fTime, bool bLocal = false);
	void RotateZ(float z, float fTime, bool bLocal = false);
	void LookAt(CTransform* pTransform, AXIS axis = AXIS_Z);
	void LookAt(class CGameObject* pObject, AXIS axis = AXIS_Z);
	void CopyTransform(CTransform* pTransform);
	void ComputeWorldMatrix();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CTransform* Clone();
	
public:
	bool closeEnough(const float& a, const float& b, 
		const float& epsilon = std::numeric_limits<float>::epsilon());
	DxVector3 DecomposeRatationAxis(const DxVector3 & _vRight, const DxVector3 & _vUp, const DxVector3 & _vForward);
	void SetLookAt( class CGameObject* TargetObj);
};

AR3D_END
