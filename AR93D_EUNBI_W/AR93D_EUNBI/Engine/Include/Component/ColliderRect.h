#pragma once
#include "Collider.h"

AR3D_BEGIN

class DLL CColliderRect :
	public CCollider
{
private:
	friend class CGameObject;

private:
	CColliderRect();
	CColliderRect(const CColliderRect& collider);
	virtual ~CColliderRect();


public:
	void* operator new (size_t size)
	{
		return _aligned_malloc(size, 16);
	}

	void operator delete (void* p)
	{
		_aligned_free(p);
	}


private:
	RECTINFO	m_tInfo;

public:
	RECTINFO GetRectInfo()	const;

public:
	void SetRectInfo(float l, float t, float r, float b);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CColliderRect* Clone();
	virtual bool Collision(CCollider* pCollider);
};

AR3D_END
