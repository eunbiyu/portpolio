#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CCameraArm :
	public CComponent
{
private:
	friend class CGameObject;

private:
	CCameraArm();
	CCameraArm(const CCameraArm& camera);
	~CCameraArm();

private:
	float	m_fMinDist;
	float	m_fMaxDist;
	float	m_fZoomSpeed;

public:
	void SetZoomDistance(float fMin, float fMax);
	void SetZoomSpeed(float fSpeed);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CCameraArm* Clone();

private:
	void Zoom(float fTime);
	void RotationDrag(float fTime);
};

AR3D_END
