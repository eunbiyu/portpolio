#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CKeyMove :
	public CComponent
{
public:
	CKeyMove();
	CKeyMove(const CKeyMove& keymove);
	~CKeyMove();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CKeyMove* Clone();
	virtual bool Collision(CCollider* pCollider);
};

AR3D_END
