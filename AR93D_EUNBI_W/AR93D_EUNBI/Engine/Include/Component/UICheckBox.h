#pragma once
#include "UIButton.h"

AR3D_BEGIN
class DLL CUICheckBox :
	public CUIButton
{
protected:
	friend class CGameObject;

private:
	bool	m_bClickOn;
	bool	m_Check;

public:
	CUICheckBox();
	CUICheckBox(const CUICheckBox& button);
	~CUICheckBox();

public:
	void SetClick(bool bClick); // 클릭된지 안된지를 나타내줌.  

public:
	virtual void LateUpdate(float fTime);
	virtual void Render(float fTime);
	virtual CUICheckBox* Clone();
};

AR3D_END 

