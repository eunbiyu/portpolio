#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL CScript :
	public CComponent
{
protected:
	CScript();
	CScript(const CScript& script);
	virtual ~CScript() = 0;

public:
	virtual bool Init() = 0;
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CScript* Clone();

	virtual void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
};

AR3D_END
