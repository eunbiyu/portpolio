#pragma once
#include "Component.h"

AR3D_BEGIN


class CGameObject;

class DLL CCamera :
	public CComponent
{
private:
	friend class CGameObject;

private:
	CCamera();
	CCamera(const CCamera& camera);
	~CCamera();

private:
	PMATRIX		m_matView;
	PMATRIX		m_matProj;
	class CGameObject*	m_pAttachObj;
	class CGameObject*	m_pTargetObj; // �ٰ��� Ÿ�� 
	class CTransform*	m_pAttachTr;
	DxVector3	m_vPrevPos;
	DxVector3	m_vAxis[AXIS_MAX];
	bool		inTarget;

public:
	DxVector3 GetAxis(AXIS axis)	const;
	void SetTargetObject(CGameObject* Target);
	CGameObject* GetTargetObject();

public:
	void SetAxis(DxVector3 vAxis[AXIS_MAX]);
	void SetAxis(const DxVector3& vAxis, AXIS axis);

public:
	void Attach(class CGameObject* pObj, const DxVector3& vDist = DxVector3(0.f, 0.f, -5.f));
	void Dettach();

public:
	class CGameObject* GetAttachObject();

public:
	XMMATRIX GetViewMatrix()	const;
	XMMATRIX GetProjMatrix()	const;

public:
	void CreateProjection(float fAngle, float fWidth, float fHeight,
		float fNear = 0.1f, float fFar = 1000.f);
	void CreateOrthoProjection(float fWidth, float fHeight, float fNear = 0.f,
		float fFar = 1000.f);

private:
	void ComputeViewMatrix();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CCamera* Clone();
};

AR3D_END
