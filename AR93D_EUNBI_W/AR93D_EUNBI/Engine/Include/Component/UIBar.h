#pragma once
#include "UI.h"

AR3D_BEGIN

class DLL CUIBar :
	public CUI
{
protected:
	friend class CGameObject;

protected:
	CUIBar();
	CUIBar(const CUIBar& bar);
	~CUIBar();

private:
	float	m_fMin;
	float	m_fMax;
	float	m_fCurValue;
	float	m_fValue; 
	float	m_fMoveValue;
	float	m_fSize;
	BAR_DIR	 m_eBarDir;

public:
	void SetBarDir(BAR_DIR eDir);
	void SetMinMax(float fMin, float fMax);
	void SetCurValue(float fValue);
	void AddValue(float fValue);

private:
	DxVector3 ComputeScale(DxVector3 vScale);


public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime) ;
	virtual CUI* Clone() ;
	virtual void OnCollisionEnter(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollision(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
	virtual void OnCollisionLeave(class CCollider* pSrc, class CCollider* pDest,
		float fTime);
};

AR3D_END