#pragma once
#include "Collider.h"

AR3D_BEGIN

class DLL CColliderPoint :
	public CCollider
{
private:
	friend class CGameObject;

private:
	CColliderPoint();
	CColliderPoint(const CColliderPoint& collider);
	virtual ~CColliderPoint();


public:
	void* operator new (size_t size)
	{
		return _aligned_malloc(size, 16);
	}

	void operator delete (void* p)
	{
		_aligned_free(p);
	}


private:
	DxVector3		m_vPos;

public:
	DxVector3 GetPoint()	const;

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CColliderPoint* Clone();
	virtual bool Collision(CCollider* pCollider);
};

AR3D_END
