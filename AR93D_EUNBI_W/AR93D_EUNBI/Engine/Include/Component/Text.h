#pragma once
#include "Component.h"

AR3D_BEGIN

enum TEXT_ALIGN
{
	TEXT_LEFT,
	TEXT_CENTER,
	TEXT_RIGHT
};

enum TEXT_PARAGRAPH_ALIGN
{
	TPA_TOP,
	TPA_CENTER,
	TPA_BOTTOM
};

class DLL CText :
	public CComponent
{
protected:
	friend class CGameObject;

protected:
	CText();
	CText(const CText& text);
	~CText();

private:
	RECTINFO	m_tArea;
	string		m_strFontName;
	string		m_strBrushName;
	IDWriteTextFormat*		m_pFont;
	ID2D1SolidColorBrush*	m_pBrush;
	ID2D1SolidColorBrush*	m_pShadowBrush;

	TEXT_ALIGN	m_eAlign;
	TEXT_PARAGRAPH_ALIGN	m_ePAlign;

	wstring		m_strText;
	bool		m_bShadow;

public:
	void SetShadow(bool bShadow);
	void SetText(const wstring& strText);
	void SetText(const char* pFileName, const string& strPathKey = DATA_PATH);
	void AddText(const wstring& strText);
	void AddText(const string& strText);
	void SetAlign(TEXT_ALIGN eAlign);
	void SetParagraphAlign(TEXT_PARAGRAPH_ALIGN eAlign);
	void SetFont(const string& strName);
	void SetFont(const string& strKey,
		const wchar_t* pFontName, int iWeight, int iStyle,
		int iStretch, float fSize, const wchar_t* pLocalName);
	void SetBrush(const string& strName);
	void SetBrush(const string& strKey, float r, float g, float b, float a);
	void SetArea(const RECTINFO& rc);
	void SetArea(float l, float t, float r, float b);
	void SetArea(const DxVector2& vPos, const DxVector2& vSize);
	void SetArea(const DxVector3& vPos, const DxVector3& vSize);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CText* Clone();
};

AR3D_END
