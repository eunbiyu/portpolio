#pragma once
#include "Collider.h"

AR3D_BEGIN

class CColliderAABB :
	public CCollider
{
public:
	CColliderAABB();
	CColliderAABB(const CColliderAABB& collider);
	~CColliderAABB();

private:
	AABBBOXINFO m_tInfo;

public:
	AABBBOXINFO GetBoxInfo() const;


public:
	void SetSphereInfo(const DxVector3& vCenter, float fX, float fY, float fZ);
	void SetSphereInfo(const AABBBOXINFO& tSphere);


public:
	void* operator new (size_t size)
	{
		return _aligned_malloc(size, 16);
	}

	void operator delete (void* p)
	{
		_aligned_free(p);
	}


public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CColliderAABB* Clone();
	virtual bool Collision(CCollider* pCollider);


};

AR3D_END

