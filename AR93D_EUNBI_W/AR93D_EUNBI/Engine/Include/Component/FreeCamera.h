#pragma once
#include "Component.h"

AR3D_BEGIN

class DLL  CFreeCamera :
	public CComponent
{
private:
	friend class CGameObject;

public:
	CFreeCamera();
	~CFreeCamera();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void LateUpdate(float fTime);
	virtual void Collision(float fTime);
	virtual void Render(float fTime);
	virtual CFreeCamera* Clone();

private:
	void RotationDrag(float fTime);
};

AR3D_END

