#include "share.fx"

struct VS_WATER_INPUT
{
    float3 vPos : POSITION;
    float3 vNormal : NORMAL;
    float2 vUV : TEXCOORD;
};


struct VS_WATER_OUTPUT
{
    float4 vPos : SV_POSITION;
    float3 vNormal : NORMAL;
    float2 vUV : TEXCOORD;
    float4 vProjPos : POSITION;
};

cbuffer TerrainInfo : register(b10)
{
    float3 g_fEmpty;
    float  g_fWaterTime;
}

VS_TEXNORMAL_OUTPUT WaterVS(VS_TEXNORMAL_INPUT input)
{
    VS_TEXNORMAL_OUTPUT output = (VS_TEXNORMAL_OUTPUT) 0;

    output.vProjPos = mul(float4(input.vPos, 1.f), g_matWVP);
    output.vUV = input.vUV;
    output.vNormal = normalize(mul(float4(input.vNormal, 0.f), g_matWV).xyz);
	//output.vViewPos = mul(float4(input.vPos, 1.f), g_matWV);
    output.vPos = output.vProjPos;

    
    float4 positionW = mul(float4(input.vPos, 1.f), g_matWorld);
    
    return output;
}


PS_OUTPUT_GBUFFER WaterPS(VS_WATER_OUTPUT input)
{
    PS_OUTPUT_GBUFFER output = (PS_OUTPUT_GBUFFER) 0;
    
    float g_fDetailLevel = 20.f; // 디테일 임의지정 
    float2 vDetailUV = (input.vUV * g_fDetailLevel);

    vDetailUV.y += (g_fWaterTime)/10;

    float4 vColor = g_DiffuseTex.Sample(g_DiffuseSmp, input.vUV);
    float4 cDetailTexColor = g_NormalTex.Sample(g_DiffuseSmp, vDetailUV);

    vColor = saturate((vColor * 0.5f) + (cDetailTexColor * 0.5f));

	// clip 함수는 인자가 0보다 작을 경우 현재 픽셀을 폐기한다.
	// 깊이버퍼에도 깊이값이 쓰이지 않는다.
        if (vColor.a == 0.f)
            clip(-1);

        output.vTarget0.rgb = vColor.xyz; // *(tMtrl.vDif.xyz + tMtrl.vAmb.xyz) + tMtrl.vSpc.xyz;

    
    output.vTarget0.r = ceil(output.vTarget0.r * 10.f) * 0.1f; //
    output.vTarget0.g = ceil(output.vTarget0.g * 10.f) * 0.1f; //
    output.vTarget0.b = ceil(output.vTarget0.b * 10.f) * 0.1f; //

        output.vTarget0.a = vColor.a;
	//output.vTarget0.a = 1.f;

        float fDiffuse = ColorToPixel(g_vMtrlDif);
        float fAmbient = ColorToPixel(g_vMtrlAmb);

        output.vTarget1.rgb = input.vNormal * 0.5f + 0.5f;
        output.vTarget1.a = 0.5f;

        output.vTarget2.r = input.vProjPos.z / input.vProjPos.w;
	//output.vTarget2.r = g_vMtrlDif.r;
        output.vTarget2.g = g_vMtrlDif.r;
        output.vTarget2.b = g_vMtrlAmb.r;
        output.vTarget2.a = input.vProjPos.w;

        float4 vSpc = g_vMtrlSpc;

        if (g_iSpecular == 1)
        {
            vSpc = g_SpecularTex.Sample(g_SpecularSmp, input.vUV);
        }

        output.vTarget3 = vSpc;
    output.vTarget3.a = 0.5f;
  

        return output;
    }