#pragma once

#include "ToolsValue.h"
// CObjectDialog 대화 상자입니다.

class CObjectDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CObjectDialog)

public:
	CObjectDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CObjectDialog();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	//리스트박스 선택을 위한 변수 선언 

private:
	CListBox m_ObjectListBox;
	int m_iListCurSel;
	string m_Clickedstr;

	//TRANSFORM< 
	float m_fScale;
	//

	CEdit    m_EditX;
	CEdit    m_EditY;
	CEdit    m_EditZ;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnLbnSelchangeList1();
	afx_msg void OnEnChangeRotx();
	afx_msg void OnEnChangeRoty();
	afx_msg void OnEnChangeRotz();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedButton13();
	afx_msg void OnBnClickedButton14();
};
