#pragma once
#include "afxwin.h"
#include <string>
#include "MenuDialog.h"


// CEditForm 폼 뷰입니다.
using namespace std;
class CEditForm : public CFormView
{
	DECLARE_DYNCREATE(CEditForm)
private:
	CMenuDialog		m_MenuDialog;

protected:
	CEditForm();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CEditForm();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_EDITFORM };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	//afx_msg void OnBnClickedButton2();
	//afx_msg void OnLbnSelchangeList1();
	CSliderCtrl m_ctrSlider;

	//리스트박스 선택을 위한 변수 선언 
public:
	CListBox m_ObjectListBox;
	int m_iListCurSel;
private:
	string m_Clickedstr;

	//afx_msg void OnNMCustomdrawSlider1(NMHDR *pNMHDR, LRESULT *pResult);
	//afx_msg void OnBnClickedButton3();
public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
};


