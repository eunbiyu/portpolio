#pragma once

#include "Component/Script.h"
#include "GameObject/GameObject.h"

AR3D_USING

class CObjectScript :
	public CScript
{
public:
	CObjectScript();
	~CObjectScript();

public:
	string	m_strName;
	bool	m_bSelected;

public:
	void SetStrName(const string& strName);

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void OnCollisionEnter(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollision(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollisionLeave(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual CObjectScript * Clone();
};


