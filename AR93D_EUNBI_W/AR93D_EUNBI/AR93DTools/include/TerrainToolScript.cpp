#include "stdafx.h"
#include "TerrainToolScript.h"

#include "stdafx.h"
#include "Component/ColliderTerrain.h"
#include "Component/ColliderRay.h"
#include "GameObject/GameObject.h"
#include "Component/Material.h"
#include "Component/Transform.h"
#include "Core/Input.h"
#include "Scene/Layer.h"
#include "Component/Renderer.h"
#include "Component/Camera.h"
#include "Scene/Scene.h"
#include "Component/Renderer.h"
#include "Resources/Mesh.h"
#include "Scene/SceneManager.h"
#include "Rendering/RenderManager.h"


CTerrainToolScript::CTerrainToolScript()
{
	SetTypeID<CTerrainToolScript>();
	m_bTerrainUpdate = false;
}

CTerrainToolScript::~CTerrainToolScript()
{
}

void CTerrainToolScript::SetStrName(const string & strName)
{
	m_strName = strName;
}

bool CTerrainToolScript::Init()
{
	m_bTerrainUpdate = false;
	return true;
}

bool CTerrainToolScript::InitTerrainTexture()
{
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	
	CGameObject*	pTerrainObj = pScene->GetTerrainObject();

	CTerrain* pTerrain = (CTerrain*)pTerrainObj->FindComponentFromTag("Terrain");


	vector<wstring>	vecMultiTex;

	vecMultiTex.push_back(L"Terrain/SAND_00.bmp");
	vecMultiTex.push_back(L"Terrain/farmfloor.bmp");
	vecMultiTex.push_back(L"Terrain/TerrainTexture3.bmp");

	pTerrain->SetSplatTexture("TerrainSplat", vecMultiTex);

	// Normal ����
	vecMultiTex.clear();

	vecMultiTex.push_back(L"Terrain/ROCK_01+MOSS_NRM.bmp");

	pTerrain->SetSplatNormalTexture("TerrainSplatNormal", vecMultiTex);

	vecMultiTex.clear();

	vecMultiTex.push_back(L"Terrain/ROCK_01+MOSS_SPEC.bmp");

	pTerrain->SetSplatSpecularTexture("TerrainSplatSpecular", vecMultiTex);

	//Splat Alpha Setting
	vecMultiTex.clear();

	vecMultiTex.push_back(L"Terrain1_real.bmp");
	vecMultiTex.push_back(L"Terrain2_r.bmp");
	vecMultiTex.push_back(L"Terrain3.bmp");

	pTerrain->SetSplatAlphaTexture("TerrainSplatAlpha", vecMultiTex);
	pTerrain->SetSplatCount(vecMultiTex.size());
	vecMultiTex.clear();

	SAFE_RELEASE(pTerrain);
	SAFE_RELEASE(pTerrainObj);

	return true;
}

void CTerrainToolScript::Input(float fTime)
{
}



void CTerrainToolScript::Update(float fTime)
{
	if (m_bTerrainUpdate)
	{
		CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
		CTerrain* pTerrain = pScene->GetTerrain();

		vector<wstring>	vecMultiTex;

		vecMultiTex.clear();

		vecMultiTex.push_back(L"Terrain1_real.bmp");
		vecMultiTex.push_back(L"Terrain2_r.bmp");
		vecMultiTex.push_back(L"Terrain3.bmp");

		pTerrain->SetSplatAlphaTexture("TerrainSplatAlpha", vecMultiTex);
		pTerrain->SetSplatCount(vecMultiTex.size());
		vecMultiTex.clear();

		SAFE_RELEASE(pTerrain);
		m_bTerrainUpdate = false;
	}
}

void CTerrainToolScript::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CTerrainToolScript::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
	if (pDest->GetTag() == "MouseRay")
	{
		CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

		CTerrain*	pTerrain = pScene->GetTerrain();

		//if (pTerrain)
		//{
		//	if (pTerrain->GetNowSetSplatNum() == 1)
		//		int a = 0;
		//		//GET_SINGLE(CRenderManager)->EditTerrainSplattingTexture("Terrain1", L"Terrain1.bmp");
		//	else if (pTerrain->GetNowSetSplatNum() == 2)
		//		GET_SINGLE(CRenderManager)->EditTerrainSplattingTexture("Terrain2", L"Terrain2.bmp");
		//	else if (pTerrain->GetNowSetSplatNum() == 3)
		//		GET_SINGLE(CRenderManager)->EditTerrainSplattingTexture("Terrain3", L"Terrain3.bmp");
		//	//m_bTerrainUpdate = true;
		//	SAFE_RELEASE(pTerrain);
		//}
		//else
		//{
		//	int a = 10;
		//}
	}
}

void CTerrainToolScript::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{

}

CTerrainToolScript * CTerrainToolScript::Clone()
{
	return new CTerrainToolScript(*this);
}
