#pragma once
#include "afxwin.h"

#include "Resources/Texture.h"
// CTerrainDialog dialog

class CTerrainDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CTerrainDialog)

public:
	CTerrainDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTerrainDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG3 };
#endif

protected:
	CSliderCtrl m_ctrSliderInner;
	CSliderCtrl m_ctrSliderOuter;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	int m_InnerRad;
	int m_OuterRad;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMCustomdrawSlider1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSlider2(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL OnInitDialog();
	CBitmapButton m_btnImage1;
	CBitmapButton m_btnImage2;
	CBitmapButton m_btnImage3;
	CBitmapButton m_btnImage4;
	CBitmapButton m_btnImage5;
	afx_msg void OnBnClickedImage1();
	afx_msg void OnBnClickedImage2();

private:
	class CTexture*		m_pSplattingTexture1;  
	class CTexture*		m_pSplattingTexture2;
	class CTexture*		m_pSplattingTexture3;
	class CTexture*		m_pSplattingTexture4;

public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton9();
	afx_msg void OnBnClickedButton10();
	afx_msg void OnBnClickedButton11();
	afx_msg void OnBnClickedImage3();
	afx_msg void OnBnClickedImage4();
	afx_msg void OnBnClickedImage5();
};
