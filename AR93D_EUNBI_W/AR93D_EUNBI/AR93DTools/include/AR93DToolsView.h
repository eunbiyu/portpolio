
// AR93DToolsView.h : CAR93DToolsView 클래스의 인터페이스
//

#pragma once

#include "AR93DToolsDoc.h"

class CAR93DToolsView : public CView
{
protected: // serialization에서만 만들어집니다.
	CAR93DToolsView();
	DECLARE_DYNCREATE(CAR93DToolsView)

// 특성입니다.
public:
	CAR93DToolsDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual void OnDraw(CDC* pDC);  // 이 뷰를 그리기 위해 재정의되었습니다.
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 구현입니다.
public:
	virtual ~CAR93DToolsView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	afx_msg void OnDropFiles(HDROP hDropInfo);
	virtual void OnInitialUpdate();
};

#ifndef _DEBUG  // AR93DToolsView.cpp의 디버그 버전
inline CAR93DToolsDoc* CAR93DToolsView::GetDocument() const
   { return reinterpret_cast<CAR93DToolsDoc*>(m_pDocument); }
#endif

