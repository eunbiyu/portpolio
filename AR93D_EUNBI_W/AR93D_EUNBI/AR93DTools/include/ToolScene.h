#pragma once

#include "Scene/SceneScript.h"
#include "GameObject/GameObject.h"
#include "Component/UIBar.h"

AR3D_USING

class CToolScene :
	public CSceneScript
{
public:
	CToolScene();
	~CToolScene();

private:
	CGameObject*	m_pPlayerObj;
	list<OBJINFO> m_ObjInfoFromTool;

public:
	virtual bool Init();
	void Input(float fTime);
	
	virtual void Update(float fTime);

public:
	bool LoadFBX(const string& str);
	TCHAR* StringToTCHAR(const string& s);
	bool LoadObjects(list<OBJINFO> ObjInfoFromTool);
	virtual void MakeEffect(const DxVector3& Pos) {};
	
};

