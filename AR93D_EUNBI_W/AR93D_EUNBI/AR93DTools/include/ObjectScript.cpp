#include "stdafx.h"
#include "ObjectScript.h"
#include "Component/ColliderSphere.h"
#include "Component/ColliderRay.h"
#include "GameObject/GameObject.h"
#include "Component/Material.h"
#include "Component/Transform.h"
#include "Core/Input.h"
#include "Scene/Layer.h"
#include "Component/Renderer.h"
#include "Component/Camera.h"
#include "Scene/Scene.h"
#include "Component/Renderer.h"
#include "Resources/Mesh.h"
#include "Scene/SceneManager.h"
#include "Core/GameTotalManager.h"

CObjectScript::CObjectScript()
{
	SetTypeID<CObjectScript>();
	m_bSelected = false;
}



CObjectScript::~CObjectScript()
{
}

void CObjectScript::SetStrName(const string & strName)
{
	m_strName = strName;

	string Temptreestring = strName.c_str();
	Temptreestring.pop_back();
	Temptreestring.pop_back();

	if (strcmp(Temptreestring.c_str(), "tree ") == 0)
	{
		CTransform*	pTransform = m_pGameObject->GetTransform();
		pTransform->SetWorldScale(0.05f, 0.05f, 0.05f);
		SAFE_RELEASE(pTransform);
	}

	if (strcmp(strName.c_str(), "Fence") == 0)
	{
		CTransform*	pTransform = m_pGameObject->GetTransform();

		pTransform->SetWorldScale(2.f, 2.f, 2.f);
		CRenderer* renderer = (CRenderer*)m_pGameObject->FindComponentFromTag("Renderer");
		CMesh* mesh = renderer->GetMesh();

		//pTransform->SetWorldPos(300.f, 30.f, 300.f);

		//// 충돌체 추가
		//m_pGameObject->EraseComponentFromTag("Collider");

		//CColliderSphere*	pSphere = m_pGameObject->AddComponent<CColliderSphere>("Collider");

		//
		//pSphere->SetSphereInfo(DxVector3(m_pGameObject->GetTransform()->GetWorldPos().x,
		//	m_pGameObject->GetTransform()->GetWorldPos().y - 1.f,
		//	m_pGameObject->GetTransform()->GetWorldPos().z),
		//	mesh->GetMeshSize().y  *m_pGameObject->GetTransform()->GetWorldScale().y * 0.8);

		//SAFE_RELEASE(pSphere);
		SAFE_RELEASE(renderer);
		SAFE_RELEASE(mesh);
		SAFE_RELEASE(pTransform);
	}
	else if (strcmp(strName.c_str(), "Medieval_Windmill") == 0)
	{
		CTransform*	pTransform = m_pGameObject->GetTransform();

		//pTransform->SetWorldScale(0.005f, 0.005f, 0.005f);
		//pTransform->SetLocalRotY(80.12f);
		pTransform->SetWorldScale(0.2f, 0.2f, 0.2f);
		pTransform->SetLocalRotZ(80.12f);
		pTransform->SetLocalRotY(-80.12f);

		CRenderer* renderer = (CRenderer*)m_pGameObject->FindComponentFromTag("Renderer");
		CMesh* mesh = renderer->GetMesh();
		CColliderSphere*	pSphere = (CColliderSphere*)m_pGameObject->FindComponentFromTag("Collider");
		pSphere->SetSphereInfo(pTransform->GetWorldPos(),
			mesh->GetRadius() * 0.02);
		renderer->GetMaterial()->
			SetDiffuseTexture("Linear", strName.c_str(), L"Medieval_Windmill.png");

		SAFE_RELEASE(pTransform);
		SAFE_RELEASE(pSphere);
		SAFE_RELEASE(mesh);
	}

	else if (strcmp(strName.c_str(), "lamp") == 0)
	{
		CTransform*	pTransform = m_pGameObject->GetTransform();

		pTransform->SetLocalRotZ(80.12f);
		pTransform->SetLocalRotY(-80.12f);

		CRenderer* renderer = (CRenderer*)m_pGameObject->FindComponentFromTag("Renderer");
		CMesh* mesh = renderer->GetMesh();
		CColliderSphere*	pSphere = (CColliderSphere*)m_pGameObject->FindComponentFromTag("Collider");
		pSphere->SetSphereInfo(pTransform->GetWorldPos(),
			mesh->GetRadius() * 0.02);
		renderer->GetMaterial()->
			SetDiffuseTexture("Linear", strName.c_str(), L"lamp.png");

		SAFE_RELEASE(pTransform);
		SAFE_RELEASE(pSphere);
		SAFE_RELEASE(mesh);
	}
}

bool CObjectScript::Init()
{
	m_bSelected = false;

	CTransform*	pTransform = m_pGameObject->GetTransform();

	string Temptreestring = m_pGameObject->GetTag().c_str();
	Temptreestring.pop_back();
	Temptreestring.pop_back();

	if (strcmp(Temptreestring.c_str(), "tree ") == 0 ||
		strcmp(Temptreestring.c_str(), "Little_House") == 0)
	{
		pTransform->SetWorldScale(0.1f, 0.1f, 0.1f);
	}

	 if (strcmp(m_pGameObject->GetTag().c_str(), "Medieval_Windmill") == 0 ||
		strcmp(m_pGameObject->GetTag().c_str(), "lamp") == 0)
	{
		pTransform->SetWorldScale(0.1f, 0.1f, 0.1f);/*
		pTransform->SetLocalRotY(80.12f);
		pTransform->SetLocalRotZ(80.12f);*/

	}
	//else
	//{
	//}
	pTransform->SetLocalRotX(80.12f);
	CRenderer* renderer = (CRenderer*)m_pGameObject->FindComponentFromTag("Renderer");
	CMesh* mesh = renderer->GetMesh();

	pTransform->SetWorldPos(300.f, 30.f, 300.f);
	
	// 충돌체 추가
	CColliderSphere*	pSphere = m_pGameObject->AddComponent<CColliderSphere>("Collider");

	if (strcmp(m_pGameObject->GetTag().c_str(), "BarnHouse" ) == 0 || 
		strcmp(m_pGameObject->GetTag().c_str(), "test1") == 0 )
	{
		pSphere->SetSphereInfo(DxVector3(m_pGameObject->GetTransform()->GetWorldPos().x -180.f,
			m_pGameObject->GetTransform()->GetWorldPos().y - 1.f,
			m_pGameObject->GetTransform()->GetWorldPos().z +180.f),
			mesh->GetMeshSize().y  *m_pGameObject->GetTransform()->GetWorldScale().y * 0.1);
	}
	else if (strcmp(m_pGameObject->GetTag().c_str(), "Fence") == 0 ||
		strcmp(m_pGameObject->GetTag().c_str(), "Olive_Tree") == 0 ||
		strcmp(m_pGameObject->GetTag().c_str(), "Medieval_Windmill") == 0 ||
		strcmp(m_pGameObject->GetTag().c_str(), "Little_House") == 0 ||
		strcmp(m_pGameObject->GetTag().c_str(), "lamp") == 0)
	{
		pSphere->SetSphereInfo(pTransform->GetWorldPos(),
			mesh->GetRadius() * 0.5);
	}
	else {

		pSphere->SetSphereInfo(pTransform->GetWorldPos(),
			mesh->GetRadius() * 0.01);
	}
	SAFE_RELEASE(pSphere);
	SAFE_RELEASE(renderer);
	SAFE_RELEASE(mesh);
	SAFE_RELEASE(pTransform);
	
	return true;
}

void CObjectScript::Input(float fTime)
{

}

void CObjectScript::Update(float fTime)
{
	/*if (strcmp(m_pLayer->GetTag().c_str(), "ObjectsLayer") == 0 &&  m_bSelected)
	{
		m_pTransform->SetWorldPos(GET_SINGLE(CInput)->GetTerrainPickingPos());
	}*/
}

void CObjectScript::OnCollisionEnter(CCollider * pSrc, CCollider * pDest, float fTime)
{
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer* pObjectLayer = pScene->FindLayer("ModifyLayer");

	if (pDest->GetTag() == "MouseRay")
	{
		if (GET_SINGLE(CGameTotalManager)->GetPickActor() != m_pGameObject)
		{
			if (GET_SINGLE(CGameTotalManager)->GetPickActor() == NULL &&
				pObjectLayer->GetObjectsList().size() ==0)
			{
				GET_SINGLE(CGameTotalManager)->SetPickActor(m_pGameObject);


					if (strcmp(m_pLayer->GetTag().c_str(), "DefaultLayer") == 0)
					{
						CLayer*	pDefaultLayer = pScene->FindLayer("DefaultLayer");
						pDefaultLayer->EraseObject(m_pGameObject);
						SAFE_RELEASE(pDefaultLayer);
					}

					if (strcmp(m_pLayer->GetTag().c_str(), "ModifyLayer") != 0)
					{
						pObjectLayer->AddObject(m_pGameObject);
					}

				CLayer*	pDefaultLayer = pScene->FindLayer("DefaultLayer");
				pDefaultLayer->SetCollisionOn(false);


				SAFE_RELEASE(pDefaultLayer);
				SAFE_RELEASE(pObjectLayer);
			}
		}
		else
		{
			

			CLayer*	pDefaultLayer = pScene->FindLayer("DefaultLayer");
			GET_SINGLE(CGameTotalManager)->ErasePickActor();
			pDefaultLayer->SetCollisionOn(true);
			SAFE_RELEASE(pDefaultLayer);
		}
	}
	
}

void CObjectScript::OnCollision(CCollider * pSrc, CCollider * pDest, float fTime)
{
}

void CObjectScript::OnCollisionLeave(CCollider * pSrc, CCollider * pDest, float fTime)
{
	
}

CObjectScript * CObjectScript::Clone()
{
	return new CObjectScript(*this);
}