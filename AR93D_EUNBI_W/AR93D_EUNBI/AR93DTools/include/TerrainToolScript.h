#pragma once

#include "Component/Script.h"
#include "GameObject/GameObject.h"

AR3D_USING

class CTerrainToolScript :
	public CScript
{
public:
	CTerrainToolScript();
	~CTerrainToolScript();

public:
	string	m_strName;
	bool	m_bTerrainUpdate;

public:
	void SetStrName(const string& strName);
	bool InitTerrainTexture();

public:
	virtual bool Init();
	virtual void Input(float fTime);
	virtual void Update(float fTime);
	virtual void OnCollisionEnter(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollision(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual void OnCollisionLeave(CCollider* pSrc, CCollider* pDest, float fTime);
	virtual CTerrainToolScript * Clone();
};



