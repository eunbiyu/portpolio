// EditForm.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AR93DTools.h"
#include "EditForm.h"
#include "ToolsValue.h"
#include "Component/Transform.h"

#include "AR93DToolsDoc.h"
#include "AR93DToolsView.h"
AR3D_USING
// CEditForm

IMPLEMENT_DYNCREATE(CEditForm, CFormView)

CEditForm::CEditForm()
	: CFormView(IDD_DIALOG_EDITFORM)
	, m_iListCurSel(0)
{

}

CEditForm::~CEditForm()
{
}

void CEditForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_LIST1, m_ObjectListBox);
	//DDX_Control(pDX, IDC_SLIDER1, m_ctrSlider);

}

BEGIN_MESSAGE_MAP(CEditForm, CFormView)
	//ON_BN_CLICKED(IDC_BUTTON2, &CEditForm::OnBnClickedButton2)
	//ON_LBN_SELCHANGE(IDC_LIST1, &CEditForm::OnLbnSelchangeList1)
	//ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER1, &CEditForm::OnNMCustomdrawSlider1)
	//ON_BN_CLICKED(IDC_BUTTON3, &CEditForm::OnBnClickedButton3)
END_MESSAGE_MAP()


// CEditForm 진단입니다.

#ifdef _DEBUG
void CEditForm::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CEditForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


BOOL CEditForm::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	bool Temp = CFormView::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
	if (false == m_MenuDialog.Create(IDD_DIALOG_MENU, this))
	{
		AfxMessageBox(_T("Error : Create Menu Dialog Failed"), MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	m_MenuDialog.ShowWindow(SW_SHOW);

	return true;
}
