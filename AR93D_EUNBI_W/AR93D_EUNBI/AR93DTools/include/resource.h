//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AR93DTools.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_AR93DToolsTYPE              130
#define IDR_THEME_MENU                  200
#define ID_SET_STYLE                    201
#define ID_VIEW_APPLOOK_WIN_2000        205
#define ID_VIEW_APPLOOK_OFF_XP          206
#define ID_VIEW_APPLOOK_WIN_XP          207
#define ID_VIEW_APPLOOK_OFF_2003        208
#define ID_VIEW_APPLOOK_VS_2005         209
#define ID_VIEW_APPLOOK_VS_2008         210
#define ID_VIEW_APPLOOK_OFF_2007_BLUE   215
#define ID_VIEW_APPLOOK_OFF_2007_BLACK  216
#define ID_VIEW_APPLOOK_OFF_2007_SILVER 217
#define ID_VIEW_APPLOOK_OFF_2007_AQUA   218
#define ID_VIEW_APPLOOK_WINDOWS_7       219
#define IDD_DIALOG_EDITFORM             310
#define IDD_DIALOG_MENU                 312
#define IDD_DIALOG1                     314
#define IDD_DIALOG2                     316
#define IDD_DIALOG3                     318
#define IDB_TILE                        350
#define IDB_GRASSFR                     351
#define IDB_GRASS                       352
#define IDB_FLOWERS                     353
#define IDB_DIRT_CLICKED                354
#define IDB_BITMAP6                     355
#define IDB_FLOWER_CLICKED              356
#define IDB_GRASS_CLICKED               357
#define IDB_GRASSFR_CLICKED             358
#define IDB_TILE_CLICKED                359
#define IDC_EDIT_MapName                1000
#define IDC_BUTTON1                     1001
#define IDC_BUTTON_SAVE                 1001
#define IDC_BUTTON2                     1002
#define IDC_LIST1                       1003
#define IDC_BUTTON5                     1003
#define IDC_BUTTON_CLIP_MODIFY          1003
#define IDC_IMAGE2                      1003
#define IDC_BUTTON3                     1004
#define IDC_SLIDER1                     1005
#define IDC_BUTTON4                     1005
#define IDC_IMAGE5                      1006
#define IDC_BUTTON13                    1006
#define IDC_SLIDER2                     1007
#define IDC_BUTTON9                     1008
#define IDC_TAB1                        1009
#define IDC_BUTTON10                    1009
#define IDC_BUTTON11                    1010
#define IDC_EDIT_ENDFRAME               1011
#define IDC_BUTTON12                    1011
#define IDC_EDIT_CLIPNAME               1013
#define IDC_EDIT_STARTFRAME             1014
#define IDC_LIST_CLIP                   1015
#define IDC_COMBO_ANIOPTION             1016
#define IDC_BUTTON_ADD_CLIP             1017
#define IDC_BUTTON_SAVE_CLIP            1018
#define IDC_BUTTON_DEFAULTCLIP          1019
#define IDC_ROTZ                        1020
#define IDC_ROTX                        1021
#define IDC_ROTY                        1022
#define IDC_BUTTON6                     1023
#define IDC_IMAGE1                      1024
#define IDC_BUTTON14                    1024
#define IDC_IMAGE3                      1025
#define IDC_IMAGE4                      1026
#define IDC_SCALE                       1029
#define IDC_BUTTON7                     1031
#define IDC_BUTTON8                     1032

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        360
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
