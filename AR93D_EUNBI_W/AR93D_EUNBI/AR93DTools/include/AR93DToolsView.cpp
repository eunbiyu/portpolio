
// AR93DToolsView.cpp : CAR93DToolsView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "AR93DTools.h"
#endif

#include "AR93DToolsDoc.h"
#include "AR93DToolsView.h"
#include "GameObject/GameObject.h"
#include "Component/Renderer.h"
#include "Component/Material.h"
#include "Component/Transform.h"
#include "Scene/Scene.h"
#include "Scene/Layer.h"
#include "Scene/SceneManager.h"
#include "Core/Input.h"

#include "ObjInformation.h"

AR3D_USING

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAR93DToolsView

IMPLEMENT_DYNCREATE(CAR93DToolsView, CView)

BEGIN_MESSAGE_MAP(CAR93DToolsView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_DROPFILES()
END_MESSAGE_MAP()

// CAR93DToolsView 생성/소멸

CAR93DToolsView::CAR93DToolsView()
{
}

CAR93DToolsView::~CAR93DToolsView()
{
}

BOOL CAR93DToolsView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CAR93DToolsView 그리기

void CAR93DToolsView::OnDraw(CDC* /*pDC*/)
{
	CAR93DToolsDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
}


// CAR93DToolsView 인쇄

BOOL CAR93DToolsView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CAR93DToolsView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CAR93DToolsView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CAR93DToolsView 진단

#ifdef _DEBUG
void CAR93DToolsView::AssertValid() const
{
	CView::AssertValid();
}

void CAR93DToolsView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CAR93DToolsDoc* CAR93DToolsView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CAR93DToolsDoc)));
	return (CAR93DToolsDoc*)m_pDocument;
}
#endif //_DEBUG


// CAR93DToolsView 메시지 처리기


BOOL CAR93DToolsView::OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CView::OnDrop(pDataObject, dropEffect, point);
}


void CAR93DToolsView::OnDropFiles(HDROP hDropInfo)
{
	//GET_SINGLE(CObjInformation)->Init();
	int	iFileCount = DragQueryFile(hDropInfo, 0xffffffff, NULL, 0);

	for (int i = 0; i < iFileCount; ++i)
	{
		int	iLength = DragQueryFile(hDropInfo, i, NULL, 0);

		TCHAR*	pPath = new TCHAR[iLength + 1];
		memset(pPath, 0, sizeof(TCHAR) * (iLength + 1));

		DragQueryFile(hDropInfo, i, pPath, iLength + 1);

		TCHAR	strExt[_MAX_EXT] = {};

		_wsplitpath_s(pPath, 0, 0, 0, 0, 0, 0, strExt, _MAX_EXT);

		char	strExt1[_MAX_EXT] = {};
		WideCharToMultiByte(CP_ACP, 0, strExt, -1, strExt1,
			lstrlen(strExt), 0, 0);

		_strupr_s(strExt1);

		if (strcmp(strExt1, ".FBX") == 0)
		{
			CGameObject*	pObj = CGameObject::Create("Obj");

			CScene*	pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

			CLayer*	pLayer = pScene->FindLayer("DefaultLayer");

			pLayer->AddObject(pObj);

			CTransform*	pTransform = pObj->GetTransform();

			pTransform->SetWorldScale(0.05f, 0.05f, 0.05f);
			
			pTransform->SetWorldPos(GET_SINGLE(CInput)->GetMousePos().x, 0.f, GET_SINGLE(CInput)->GetMousePos().y);

			SAFE_RELEASE(pTransform);

			CRenderer*	pRenderer = pObj->AddComponent<CRenderer>("Renderer");

			string	strKey = "TestMesh";
			strKey += (i + 1);

			pRenderer->SetMeshFromFullPath(strKey, pPath);
			pRenderer->SetShader(STANDARD_BUMP_SHADER);
			pRenderer->SetInputLayout("BumpInputLayout");
			pRenderer->SetRenderState(ALPHABLEND);

			SAFE_RELEASE(pRenderer);

			SAFE_RELEASE(pLayer);

			SAFE_RELEASE(pObj);
		}

		SAFE_DELETE_ARRAY(pPath);
	}

	DragFinish(hDropInfo);
	//CView::OnDropFiles(hDropInfo);
}


void CAR93DToolsView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	DragAcceptFiles(TRUE);
}
