// TerrainDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AR93DTools.h"
#include "TerrainDialog.h"
#include "afxdialogex.h"

#include "GameObject/GameObject.h"
#include "Component/Renderer.h"
#include "Component/Material.h"
#include "Component/Transform.h"
#include "Scene/Scene.h"
#include "Scene/Layer.h"
#include "Scene/SceneManager.h"
#include "Component/Terrain.h"
#include "Rendering/RenderManager.h"

#include "Core/PathManager.h"
#include "Core/Input.h"
#include "ObjInformation.h"
#include "ToolScene.h"


// CTerrainDialog dialog

IMPLEMENT_DYNAMIC(CTerrainDialog, CDialogEx)

CTerrainDialog::CTerrainDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DIALOG3, pParent)
{
}

CTerrainDialog::~CTerrainDialog()
{
}

void CTerrainDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_SLIDER1, m_ctrSliderInner);
	DDX_Control(pDX, IDC_SLIDER2, m_ctrSliderOuter);

	DDX_Control(pDX, IDC_IMAGE1, m_btnImage1);
	DDX_Control(pDX, IDC_IMAGE2, m_btnImage2);
	DDX_Control(pDX, IDC_IMAGE3, m_btnImage3);
	DDX_Control(pDX, IDC_IMAGE4, m_btnImage4);
	DDX_Control(pDX, IDC_IMAGE5, m_btnImage5);
}


BEGIN_MESSAGE_MAP(CTerrainDialog, CDialogEx)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER1, &CTerrainDialog::OnNMCustomdrawSlider1)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER2, &CTerrainDialog::OnNMCustomdrawSlider2)

	ON_BN_CLICKED(IDC_IMAGE1, &CTerrainDialog::OnBnClickedImage1)
	ON_BN_CLICKED(IDC_IMAGE2, &CTerrainDialog::OnBnClickedImage2)
	ON_BN_CLICKED(IDC_BUTTON1, &CTerrainDialog::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON3, &CTerrainDialog::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON9, &CTerrainDialog::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON10, &CTerrainDialog::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON11, &CTerrainDialog::OnBnClickedButton11)
	ON_BN_CLICKED(IDC_IMAGE3, &CTerrainDialog::OnBnClickedImage3)
	ON_BN_CLICKED(IDC_IMAGE4, &CTerrainDialog::OnBnClickedImage4)
	ON_BN_CLICKED(IDC_IMAGE5, &CTerrainDialog::OnBnClickedImage5)
END_MESSAGE_MAP()


// CTerrainDialog message handlers


void CTerrainDialog::OnNMCustomdrawSlider1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	*pResult = 0;

	m_ctrSliderInner.SetRange(0, 30);

	m_InnerRad = m_ctrSliderInner.GetPos();

	if (m_OuterRad < m_InnerRad + 2.f)
	{
		m_OuterRad = m_InnerRad + 2.f;
	}

	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	pMapLayer = pScene->FindLayer("MapLayer");

	CGameObject*	pTerrainObj = pMapLayer->FindObjectFromTag("Terrain");

	CTerrain*	pTerrain = (CTerrain*)pTerrainObj->FindComponentFromTag("Terrain");
	pTerrain->SetPickRadInfo(m_OuterRad, m_InnerRad);

	SAFE_RELEASE(pMapLayer);
	SAFE_RELEASE(pTerrainObj);
	SAFE_RELEASE(pTerrain);
}


void CTerrainDialog::OnNMCustomdrawSlider2(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	*pResult = 0;

	m_ctrSliderOuter.SetRange(0, 30);

	m_OuterRad = m_ctrSliderOuter.GetPos();
	if (m_OuterRad < m_InnerRad + 2.f)
	{
		m_OuterRad = m_InnerRad + 2.f;
	}

	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	pMapLayer = pScene->FindLayer("MapLayer");

	CGameObject*	pTerrainObj = pMapLayer->FindObjectFromTag("Terrain");

	CTerrain*	pTerrain = (CTerrain*)pTerrainObj->FindComponentFromTag("Terrain");
	pTerrain->SetPickRadInfo(m_OuterRad , m_InnerRad);

	SAFE_RELEASE(pMapLayer);
	SAFE_RELEASE(pTerrainObj);
	SAFE_RELEASE(pTerrain);
}

BOOL CTerrainDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  Add extra initialization here
	m_btnImage1.LoadBitmaps(IDB_GRASS, IDB_GRASS_CLICKED, IDB_GRASS_CLICKED, NULL);
	m_btnImage2.LoadBitmaps(IDB_GRASSFR, IDB_GRASSFR_CLICKED, IDB_GRASSFR_CLICKED, NULL);
	m_btnImage3.LoadBitmaps(IDB_FLOWERS, IDB_FLOWER_CLICKED, IDB_FLOWER_CLICKED, NULL);
	m_btnImage4.LoadBitmaps(IDB_TILE, IDB_TILE_CLICKED, IDB_TILE_CLICKED, NULL);
	m_btnImage5.LoadBitmaps(IDB_GRASS, IDB_GRASS_CLICKED, IDB_GRASS_CLICKED, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}


void CTerrainDialog::OnBnClickedImage1()
{
	// TODO: TEXTURE1 SETTING
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

	CTerrain*	pTerrain = pScene->GetTerrain();

	pTerrain->SetNowSetSplatNum(1);

	SAFE_RELEASE(pTerrain);
}


void CTerrainDialog::OnBnClickedImage2()
{
	// TODO: TEXTURE2 SETTING
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

	CTerrain*	pTerrain = pScene->GetTerrain();

	pTerrain->SetNowSetSplatNum(2);

	SAFE_RELEASE(pTerrain);
}


void CTerrainDialog::OnBnClickedImage3()
{
	// TODO: TEXTURE3 SETTING
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

	CTerrain*	pTerrain = pScene->GetTerrain();

	pTerrain->SetNowSetSplatNum(3);

	SAFE_RELEASE(pTerrain);
}

void CTerrainDialog::OnBnClickedImage4()
{
	// TODO: TEXTURE4 SETTING

}


void CTerrainDialog::OnBnClickedImage5()
{
	// TODO: TEXTURE5 SETTING
}

//=================RESET TEXTURE========================//

void CTerrainDialog::OnBnClickedButton1()
{
	// TODO: RESET TEXTURE1
	//GET_SINGLE(CRenderManager)->SaveTargetTerrain("Terrain1", L"Terrain1.bmp", true);
}


void CTerrainDialog::OnBnClickedButton3()
{
	// TODO: RESET TEXTURE2
	//GET_SINGLE(CRenderManager)->SaveTargetTerrain("Terrain2", L"Terrain2.bmp", true);
}


void CTerrainDialog::OnBnClickedButton9()
{
	// TODO: RESET TEXTURE3
	//GET_SINGLE(CRenderManager)->SaveTargetTerrain("Terrain3", L"Terrain3.bmp", true);
}


void CTerrainDialog::OnBnClickedButton10()
{
	// TODO: RESET TEXTURE4
	//GET_SINGLE(CRenderManager)->SaveTargetTerrain("Terrain4", L"Terrain4.bmp", true);
}


void CTerrainDialog::OnBnClickedButton11()
{
	// TODO: RESET TEXTURE5
//	GET_SINGLE(CRenderManager)->SaveTargetTerrain("Terrain5", L"Terrain5.bmp", true);
}

