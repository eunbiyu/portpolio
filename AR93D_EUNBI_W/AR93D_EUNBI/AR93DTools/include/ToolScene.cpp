#include "stdafx.h"
#include "ToolScene.h"

#include "GameObject/GameObject.h"
#include "Component/Renderer.h"
#include "Component/Transform.h"
#include "Scene/Scene.h"
#include "Scene/Layer.h"
#include "Component/Camera.h"
#include "Component/CameraArm.h"
#include "Component/ColliderSphere.h"
#include "Component/ColliderRect.h"
#include "Component/Material.h"
#include "Component/Effect.h"
#include "Component/Animation2D.h"
#include "Resources/ResourcesManager.h"
#include "Resources/Texture.h"
#include "Core/Scheduler.h"
#include "Core/Input.h"
#include "Component/UIBar.h"
#include "Component/UIBack.h"
#include "Component/Renderer2D.h"
#include "Component/FreeCamera.h"
#include "Component/Terrain.h"
#include "Scene/SceneManager.h"
#include "Resources/Mesh.h"
#include "Device.h"
#include "Rendering/RenderManager.h"

#include "ObjInformation.h"
#include "EmptyPlayer.h"
#include "Component/KeyMove.h"
#include "ObjectScript.h"
#include "TerrainToolScript.h"


AR3D_USING

typedef std::basic_string<TCHAR> tstring;

CToolScene::CToolScene() :
	m_pPlayerObj(NULL)
{
}


CToolScene::~CToolScene()
{
	SAFE_RELEASE(m_pPlayerObj);
}

bool CToolScene::Init()
{
	//키 생성 
	GET_SINGLE(CInput)->CreateKey("OnOffCollider", VK_F1);

	CLayer*	pLayer = m_pScene->FindLayer("DefaultLayer");

	CGameObject*	pMainCameraObj = m_pScene->GetMainCameraObj();

	CFreeCamera*	pArm = pMainCameraObj->AddComponent<CFreeCamera>("CameraArm");

	//pArm->SetZoomSpeed(100.f);

	SAFE_RELEASE(pArm);

	SAFE_RELEASE(pArm);

	SAFE_RELEASE(pMainCameraObj);

	CGameObject*	pCameraObj = m_pScene->CreateCamera("BulletAction");

	CCamera*	pCamera = (CCamera*)pCameraObj->FindComponentFromType(CT_CAMERA);

	CTransform*	pCameraTr = pCameraObj->GetTransform();

	CGameObject*	pPlayerObj = CGameObject::Create("PlayerObj");

	pPlayerObj->AddRef();
	m_pPlayerObj = pPlayerObj;
	CTransform*	pTransform = pPlayerObj->GetTransform();

	pTransform->SetWorldPosY(10.f);
	pTransform->SetWorldRotX(-90.f);
	SAFE_RELEASE(pTransform);


	// 지형
	CLayer*	pMapLayer = m_pScene->FindLayer("MapLayer");

	CGameObject*	pTerrainObj = CGameObject::Create("Terrain");

	CTerrain*	pTerrain = pTerrainObj->AddComponent<CTerrain>("Terrain");
	
	pTerrain->ToolOn(true); // 툴이라고해준다 

	//텍스쳐 4개 셋팅
	//GET_SINGLE(CRenderManager)->SaveTargetTerrain("Terrain1", L"Terrain1.bmp", true);
	//GET_SINGLE(CRenderManager)->SaveTargetTerrain("Terrain2", L"Terrain2.bmp", true);
	//GET_SINGLE(CRenderManager)->SaveTargetTerrain("Terrain3", L"Terrain3.bmp", true);
	
	pTerrain->CreateTerrain("Terrain", 129, 129, 5, 5, "Terrain/terrain.bmp");
	pTerrain->SetBaseTexture("TerrainDiffuse",
		L"Terrain/TerrainTexture1.bmp");
	pTerrain->SetNormalTexture("TerrainNormal",
		L"Terrain/TerrainTexture1_NRM.bmp");
	pTerrain->SetSpecularTexture("TerrainSpc",
		L"Terrain/TerrainTexture1_SPEC.bmp");

	vector<wstring>	vecMultiTex;

	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	pScene->SetTerrain(pTerrainObj);

	// Terrain 스크립트 추가
	CTerrainToolScript*	pObjectScript = pTerrainObj->AddComponent<CTerrainToolScript>("TerrainScript");
	
	//// 이전에 저장되어있던 애들 불러오기
	pObjectScript->InitTerrainTexture();

	m_ObjInfoFromTool = GET_SINGLE(CObjInformation)->LoadObjInfo("ObjInfoList.txt");
	if (m_ObjInfoFromTool.size() > 0)
	{
		LoadObjects(m_ObjInfoFromTool);
	}

	SAFE_RELEASE(pTerrain);
	SAFE_RELEASE(pObjectScript);

	pMapLayer->AddObject(pTerrainObj);

	SAFE_RELEASE(pTerrainObj);

	SAFE_RELEASE(pMapLayer); 
	SAFE_RELEASE(pLayer);


	GET_SINGLE(CObjInformation)->m_bChangeInfo = false;

	return true;
}

void CToolScene::Input(float fTime)
{
	if (KEYPRESS("OnOffCollider"))
	{
		if (GET_SINGLE(CInput)->m_OnOffCollider == true)
			GET_SINGLE(CInput)->m_OnOffCollider = false;
		else
			GET_SINGLE(CInput)->m_OnOffCollider = true;

	}

}

void CToolScene::Update(float fTime)
{

	//CLayer*	pLayer = m_pScene->FindLayer("DefaultLayer");
	//list<CGameObject*>*	pRadioBtnObjList = pLayer->FindAllObjects();

	//list<CGameObject*>::iterator	iter;

	//for (iter = pRadioBtnObjList->begin(); iter != pRadioBtnObjList->end(); ++iter)
	//{
	//	if ((*iter)->GetSelected())
	//	{
	//		//CKeyMove*	pKeymove = (*iter)->AddComponent<CKeyMove>("KeyMove");
	//		//
	//		//SAFE_RELEASE(pKeymove);


	//	}
	//}
	//SAFE_RELEASE(pLayer);
}

bool CToolScene::LoadFBX(const string & str)
{
	//이름 저장하기 위해 확장자를 뺀 나머지를 만들어준다. 
	string Namestr = str;

	if (Namestr.size() <= 0)
		return false;

	for (int i = 0; i < 4; ++i)
	{
		Namestr.pop_back();
	}

	CGameObject*	pObj = CGameObject::Create(Namestr);
	

	CScene*	pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();

	pObj->AddRef();

	CRenderer*	pRenderer = pObj->AddComponent<CRenderer>("Renderer");
 	pRenderer->SetMesh(Namestr, StringToTCHAR(str), FLT_BOTH, OBJECT_MESH_PATH);
	pRenderer->SetShader(STANDARD_TEXNORMAL_SHADER);
	pRenderer->SetInputLayout("TexNormalInputLayout");
	pRenderer->SetRenderState(ALPHABLEND);
	pRenderer->SetRenderFlag(RF_CARTOON_ONLY);

	// 플레이어 스크립트 추가
	CObjectScript*	pObjectScript = pObj->AddComponent<CObjectScript>("Object");
	pObjectScript->SetStrName(Namestr);

	SAFE_RELEASE(pObjectScript);

	// 재질 정보 변경
	CMaterial* pMaterial = pRenderer->GetMaterial();

	string Temptreestring = Namestr.c_str();
	Temptreestring.pop_back();
	Temptreestring.pop_back();

	if (strcmp(Namestr.c_str(), "Fence") == 0)
	{
		pMaterial->SetDiffuseTexture("Linear", "Fence", L"fence_texture.png");
	}
	else if (strcmp(Namestr.c_str(), "Medieval_Windmill") == 0)
	{
		pMaterial->SetDiffuseTexture("Linear", Namestr.c_str(), L"Medieval_Windmill.png");
		CTransform* pTransform  = pObj->GetTransform();
		pTransform->SetWorldScale(0.2f, 0.2f, 0.2f);
		SAFE_RELEASE(pTransform);
		pRenderer->SetRenderState(ALPHABLEND);

	}
	else if (strcmp(Namestr.c_str(), "Treenew") == 0)
	{
		pMaterial->SetDiffuseTexture("Linear", Namestr.c_str(), L"Treenew.png");
	}
	else if (strcmp(Namestr.c_str(), "Little_House") == 0)
	{
		pMaterial->SetDiffuseTexture("Linear", Namestr.c_str(), L"Little_House.png");
	}
	else if (strcmp(Namestr.c_str(), "lamp") == 0)
	{
		pMaterial->SetDiffuseTexture("Linear", Namestr.c_str(), L"lamp.png");
		CTransform* pTransform = pObj->GetTransform();
		pTransform->SetWorldScale(0.2f, 0.2f, 0.2f);
		SAFE_RELEASE(pTransform);
	}
	else if (strcmp(Temptreestring.c_str(), "tree ") == 0)
	{
		pMaterial->SetDiffuseTexture("Linear", "TreeTexture", L"Treeficus.png");
	}
	else
	{
		CTransform* pTransform = pObj->GetTransform();
		pTransform->SetWorldScale(0.01f, 0.01f, 0.01f);
		SAFE_RELEASE(pTransform);
		pMaterial->SetDiffuseTexture("Linear", "Atlas", L"FarmA1_Atlas_col.png");
	}
	//pMaterial->SetNormalMapTexture("Linear", "AtlasNRM", L"FarmA1_Atlas_col_NRM.bmp");
	//pMaterial->SetSpecularTexture("Linear", "AtlasSPEC", L"FarmA1_Atlas_col_SPEC.bmp");

	SAFE_RELEASE(pMaterial);

	SAFE_RELEASE(pRenderer);

	CLayer*	pLayer = pScene->FindLayer("DefaultLayer");

	pLayer->AddObject(pObj);

	SAFE_RELEASE(pLayer);

	SAFE_RELEASE(pObj);

	return true;
}



TCHAR* CToolScene::StringToTCHAR(const string& s)
{
	tstring tstr;
	const char* all = s.c_str();
	int len = 1 + strlen(all);
	wchar_t* t = new wchar_t[len];
	if (NULL == t) throw std::bad_alloc();
	mbstowcs(t, all, len);
	return (TCHAR*)t;
}

bool CToolScene::LoadObjects(list<OBJINFO> ObjInfoFromTool)
{
	CLayer*	pLayer = m_pScene->FindLayer("DefaultLayer");

	list<OBJINFO>::iterator iter;
	list<OBJINFO>::iterator iterEnd = ObjInfoFromTool.end();

	for (iter = ObjInfoFromTool.begin(); iter != iterEnd; ++iter)
	{
		CGameObject*	pObj = CGameObject::Create((*iter).strTag);

		pObj->AddRef();

		CRenderer* pRenderer = pObj->AddComponent<CRenderer>("Renderer");

		string temp = (*iter).strTag;
		temp += ".FBX";

		pRenderer->SetMesh((*iter).strTag, StringToTCHAR(temp), FLT_BOTH, OBJECT_MESH_PATH);
		pRenderer->SetShader(STANDARD_BUMP_SHADER);
		pRenderer->SetInputLayout("BumpInputLayout");
		pRenderer->SetRenderState(ALPHABLEND);

		// 플레이어 스크립트 추가
		CObjectScript*	pObjectScript = pObj->AddComponent<CObjectScript>("Object");
		

		CTransform* pTransform = pObj->GetTransform();

		pTransform->SetWorldScale((*iter).WorldScale.x, (*iter).WorldScale.y, (*iter).WorldScale.z);
		pTransform->SetWorldPos((*iter).WorldPos.x, (*iter).WorldPos.y, (*iter).WorldPos.z);
		pTransform->SetWorldRot((*iter).WorldRot.x, (*iter).WorldRot.y, (*iter).WorldRot.z);
		pTransform->SetWorldScale(0.01f, 0.01f, 0.01f);
		pTransform->SetLocalRotX(80.12f);

		pObjectScript->SetStrName((*iter).strTag);

		SAFE_RELEASE(pObjectScript);


		//// 재질 정보 변경
		CMaterial* pMaterial = pRenderer->GetMaterial();

		string Temptreestring = (*iter).strTag.c_str();
		Temptreestring.pop_back();
		Temptreestring.pop_back();

		if (strcmp((*iter).strTag.c_str(), "Fence") == 0)
		{
			pMaterial->SetDiffuseTexture("Linear", "Fence", L"fence_texture.png");
		}
		else if (strcmp(Temptreestring.c_str(), "tree ") == 0)
		{
			pMaterial->SetDiffuseTexture("Linear", "TreeTexture", L"Treeficus.png");
		}
		else if (strcmp((*iter).strTag.c_str(), "Medieval_Windmill") == 0)
		{
			pMaterial->SetDiffuseTexture("Linear", Temptreestring.c_str(), L"Medieval_Windmill.png");
			CRenderer* renderer = (CRenderer*)pRenderer->FindComponentFromTag("Renderer");
			CMesh* mesh = renderer->GetMesh();
			SAFE_RELEASE(mesh);
		}

		else if (strcmp((*iter).strTag.c_str(), "lamp") == 0)
		{
			pMaterial->SetDiffuseTexture("Linear", Temptreestring.c_str(), L"lamp.png");
			CTransform* pTransform = pObj->GetTransform();
			pTransform->SetWorldScale(0.1f, 0.1f, 0.1f);
			SAFE_RELEASE(pTransform);
		}
		else
		{
			CTransform* pTransform = pObj->GetTransform(); 
			pTransform->SetWorldScale(0.01f, 0.01f, 0.01f);
			SAFE_RELEASE(pTransform);
			pMaterial->SetDiffuseTexture("Linear", "Atlas", L"FarmA1_Atlas_col.png");
		}
		SAFE_RELEASE(pMaterial);

		SAFE_RELEASE(pRenderer);

		pLayer->AddObject(pObj);

		SAFE_RELEASE(pObj);
		SAFE_RELEASE(pTransform);
	}
	SAFE_RELEASE(pLayer);

	return true;
}