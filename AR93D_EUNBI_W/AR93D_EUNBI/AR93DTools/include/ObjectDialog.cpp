// ObjectDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AR93DTools.h"
#include "ObjectDialog.h"
#include "afxdialogex.h"
#include "Engine.h"

#include "AR93DToolsDoc.h"
#include "AR93DToolsView.h"
#include "GameObject/GameObject.h"
#include "Component/Renderer.h"
#include "Component/Material.h"
#include "Component/Transform.h"
#include "Scene/Scene.h"
#include "Scene/Layer.h"
#include "Scene/SceneManager.h"
#include "Component/Terrain.h"
#include "Core/PathManager.h"
#include "Core/Input.h"
#include "ObjInformation.h"
#include "ToolScene.h"
#include "ObjectScript.h"
#include "ObjInformation.h"
#include "Core/GameTotalManager.h"

// CObjectDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CObjectDialog, CDialogEx)

CObjectDialog::CObjectDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DIALOG1, pParent), 
	m_fScale(0)
{

}

CObjectDialog::~CObjectDialog()
{
}

void CObjectDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ObjectListBox);
	//TEXT CONTROL
	DDX_Text(pDX, IDC_SCALE, m_fScale);
}


BEGIN_MESSAGE_MAP(CObjectDialog, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON2, &CObjectDialog::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON1, &CObjectDialog::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON3, &CObjectDialog::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CObjectDialog::OnBnClickedButton4)
	ON_LBN_SELCHANGE(IDC_LIST1, &CObjectDialog::OnLbnSelchangeList1)
	ON_BN_CLICKED(IDC_BUTTON6, &CObjectDialog::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CObjectDialog::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CObjectDialog::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON13, &CObjectDialog::OnBnClickedButton13)
	ON_BN_CLICKED(IDC_BUTTON14, &CObjectDialog::OnBnClickedButton14)
END_MESSAGE_MAP()


// CObjectDialog 메시지 처리기입니다.


void CObjectDialog::OnBnClickedButton2()
{
	// TODO: GET LIST


	CFileFind FileFind;

	wstring TempPath = GET_SINGLE(CPathManager)->FindPath(OBJECT_MESH_PATH);
	TempPath += L"*.fbx";

	string strKey;

	// 디렉터리가 존재하면 TRUE를 반환한다.
	bool IsFile = FileFind.FindFile(TempPath.c_str());
	while (IsFile)
	{
		// 다음 파일 혹은 폴더가 존재한다면 TRUE를 반환한다.
		IsFile = FileFind.FindNextFileW();

		if (false == FileFind.IsDots())
		{
			CString FileName = FileFind.GetFileName();
			m_ObjectListBox.AddString(FileName);

			//CString->string 변환하는 함수 만들기 
			//CString FilePath = FileFind.GetFilePath();
			//strKey = FileName;

			///*CMesh* pMesh = GET_SINGLE(CResMgr)->LoadMeshFromFullPath(strKey, FilePath);
			//SAFE_RELEASE(pMesh);*/
		}
	}
}


void CObjectDialog::OnBnClickedButton1()
{
	// TODO: LOAD FBX
	

	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CToolScene* pToolScene = (CToolScene*)pScene;
	bool LoadOK = pToolScene->LoadFBX(m_Clickedstr);


}


void CObjectDialog::OnBnClickedButton3()
{
	// TODO: SAVE ALL
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer*	SaveObjList = pScene->FindLayer("DefaultLayer");

	list<class CGameObject*> pObjlist; // 새로 저장할 애들 

	pObjlist = SaveObjList->GetObjectsList();

	GET_SINGLE(CObjInformation)->SaveObjInfo(pObjlist, "ObjInfoList.txt");

	SAFE_RELEASE(SaveObjList);
}


void CObjectDialog::OnBnClickedButton4()
{
	// TODO: CANCEL
	list<OBJINFO> list =GET_SINGLE(CObjInformation)->LoadObjInfo("ObjInfoList.txt");
}


void CObjectDialog::OnLbnSelchangeList1()
{
	// TODO: Add your control notification handler code here

	m_iListCurSel = m_ObjectListBox.GetCurSel();

	CString cStr;

	m_ObjectListBox.GetText(m_iListCurSel, cStr);

	m_Clickedstr = std::string(CT2CA(cStr.operator LPCWSTR()));
}


void CObjectDialog::OnEnChangeRotx()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}


void CObjectDialog::OnEnChangeRoty()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}


void CObjectDialog::OnEnChangeRotz()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}


void CObjectDialog::OnBnClickedButton6()
{
	// TODO: SAVE ROT
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	list<class CGameObject*> pObjlist;
	CLayer* pObjectLayer = pScene->FindLayer("ModifyLayer");
	//CLayer*	SaveObjList = pScene->FindLayer("SaveObjectLayer");
	pObjlist = pObjectLayer->GetObjectsList();
	list<class CGameObject*>::iterator iter;
	

	list<CGameObject*>::iterator	iterEnd = pObjlist.end();
	for (iter = pObjlist.begin(); iter != iterEnd; ++iter)
	{
		(*iter)->GetTransform()->RotateY(AR3D_PI, 0.05f);
		//pObjectLayer->EraseObject(*iter);
		//SaveObjList->AddObject(*iter);
		
	}

	SAFE_RELEASE(pObjectLayer);
	//SAFE_RELEASE(SaveObjList);
}


void CObjectDialog::OnBnClickedButton7()
{
	// TODO: SAVE SCALE
	UpdateData(TRUE);
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	list<class CGameObject*> pObjlist;
	CLayer* pObjectLayer = pScene->FindLayer("ObjectsLayer");
	CLayer*	SaveObjList = pScene->FindLayer("SaveObjectLayer");
	pObjlist = pObjectLayer->GetObjectsList();

	list<class CGameObject*>::iterator iter;

	list<CGameObject*>::iterator	iterEnd = pObjlist.end();
	for (iter = pObjlist.begin(); iter != iterEnd; ++iter)
	{
		(*iter)->GetTransform()->SetLocalScale(DxVector3(m_fScale, m_fScale, m_fScale));
		pObjectLayer->EraseObject(*iter);
		SaveObjList->AddObject(*iter);
	}

	SAFE_RELEASE(pObjectLayer);
	SAFE_RELEASE(SaveObjList);
}


void CObjectDialog::OnBnClickedButton8()
{
	//FBX FIX
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	CLayer* pModifyLayer = pScene->FindLayer("ModifyLayer");

	list<class CGameObject*> pObjlist = pModifyLayer->GetObjectsList();

	list<class CGameObject*>::iterator iter;
	list<CGameObject*>::iterator	iterEnd = pObjlist.end();

	for (iter = pObjlist.begin(); iter != iterEnd; ++iter)
	{
		CLayer* pDefaultLayer = pScene->FindLayer("DefaultLayer");
		pDefaultLayer->AddObject(*iter);

		pModifyLayer->EraseObject(*iter);
		SAFE_RELEASE(pDefaultLayer);
		
	}
}


void CObjectDialog::OnBnClickedButton13()
{
	// TODO: REMOVE FBX
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	list<class CGameObject*> pObjlist;
	CLayer* pObjectLayer = pScene->FindLayer("ModifyLayer");
	pObjlist = pObjectLayer->GetObjectsList();
	list<class CGameObject*>::iterator iter;

	if (pObjlist.size() >= 1) 
	{
	list<CGameObject*>::iterator	iterEnd = pObjlist.end();
	for (iter = pObjlist.begin(); iter != iterEnd; ++iter)
	{
		pObjectLayer->EraseObject(*iter);
		(*iter)->Remove();
	}
	}
	GET_SINGLE(CGameTotalManager)->ErasePickActor();
	SAFE_RELEASE(pObjectLayer);
	//SAFE_RELEASE(SaveObjList);
}


void CObjectDialog::OnBnClickedButton14()
{
	// TODO: SAVE ROT
	CScene* pScene = GET_SINGLE(CSceneManager)->GetCurrentScene();
	list<class CGameObject*> pObjlist;
	CLayer* pObjectLayer = pScene->FindLayer("ObjectsLayer");
	//CLayer*	SaveObjList = pScene->FindLayer("SaveObjectLayer");
	pObjlist = pObjectLayer->GetObjectsList();
	list<class CGameObject*>::iterator iter;


	list<CGameObject*>::iterator	iterEnd = pObjlist.end();
	for (iter = pObjlist.begin(); iter != iterEnd; ++iter)
	{
		(*iter)->GetTransform()->RotateY(AR3D_PI, -0.05f);
		//pObjectLayer->EraseObject(*iter);
		//SaveObjList->AddObject(*iter);

	}

	SAFE_RELEASE(pObjectLayer);
	//SAFE_RELEASE(SaveObjList);
}